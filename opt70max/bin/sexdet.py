#!/usr/bin/env python
import sys
from optparse import OptionParser
from cgiutils import bundle


def main():
    parser =  OptionParser('usage: %prog [options]')
    parser.add_option('-o', '--output',
                      help='output sex prediction file')
    parser.add_option('-i', '--input',
                      help='input *contigs.csv file from sampler')
    parser.add_option('-r', '--rules', 
                      help='read rules from file')
    parser.add_option('-m', '--manifest', default=None,
                      help='gender from manifest')

    (options, args) = parser.parse_args()

    if len(args) != 0:
        raise Exception('unexpected arguments')

    if not options.output or not options.input:
        raise Exception('output file and input file must be specified')

    outf = open(options.output, 'w')
    inf = open(options.input, 'r')
    rf = open(options.rules, 'r')

    ruleKeys = []
    rules = {}
    for line in rf:
        fields = line.strip().split(',')
        ruleKeys.append(int(fields[0]))
        rules[ruleKeys[-1]] = bundle( maxF=float(fields[1]), minM=float(fields[2]), dnbCount=None, length=None )

    totalDnbs = 0
    totalLength = 0
    ignore = inf.readline() # header
    for line in inf:
        (ctg, chr, offset, length, dnbCount) = line.split(',')
        (ctg, offset, length, dnbCount) = map(int, (ctg, offset, length, dnbCount))
        totalDnbs += dnbCount
        totalLength += length
        if ctg in rules:
            rules[ctg].dnbCount = dnbCount
            rules[ctg].length = length

    print 'contig,maxFRate,minMRate,rate,call'
    calls = { 'male':0, 'female':0 }
    for key in ruleKeys:
        rule = rules[key]
        call = None
        rate = None
        if rule.dnbCount is not None:
            rate = rule.dnbCount / float(totalDnbs) / ( rule.length / float(totalLength) )
            if rate < rule.maxF:
                call = 'female'
            elif rate > rule.minM:
                call = 'male'
        print '%d,%s,%s,%s,%s' % (key, rule.maxF, rule.minM, rate, call)
        if call is not None:
            calls[call] += 1

    M = calls['male']
    F = calls['female']
    print
    print 'sex contigs = %s, male contigs = %s, female contigs = %s' % (len(rules), M, F)

    if( M>0 and F==0 ):
        print >>outf, 'male'
    elif ( F>0 and M==0 ):
        print >>outf, 'female'
    else:
        print 'ambiguous gender'
        options.manifest = options.manifest.lower()
        if options.manifest in [ 'male', 'female' ]:
            print 'using manifest gender:', options.manifest
            print >>outf, options.manifest
        elif M > F:
            print >>outf, 'male'
        elif F > M:
            print >> outf, 'female'
        else:
            print 'no manifest gender, defaulting to female'
            print >>outf, 'female'
        
    inf.close()
    outf.close()

if __name__ == '__main__':
    main()
