#!/usr/bin/python
import os,sys
import optparse

class CNVCall:
    def __init__(self,chr,beg,end,ploidy):
        self.chr=chr
        self.beg=beg
        self.end=end
        self.ploidy=ploidy

def callcompare(x,y):
    if (x.chr != y.chr):
        if(x.chr<y.chr):
            return -1
        else:
            return 1
    if (x.beg != y.beg):
        if(x.beg<y.beg):
            return -1
        else:
            return 1
    if (x.end != y.end):
        if(x.end<y.end):
            return -1
        else:
            return 1
    if (x.ploidy != y.ploidy):
        if(x.ploidy<y.ploidy):
            return -1
        else:
            return 1
    return 0

def readCalls(callFileName,minLen,maxLen,minScore,minTailScore,maxScore,maxTailScore,countNoCalls,ignoreSexChr):
    
    calls = []

    callfile=open(callFileName,'r')
    for l in callfile:
        lw = l.split('\t')
        
        if lw[0][0] != '\n' and lw[0][0] != "#" and lw[0][0] != ">":
            ploidy = lw[5]
            ploidyType = lw[6]
            len=int(lw[2])-int(lw[1])
            score=int(lw[7])
            tailscore=int(lw[8])
                
            chr = lw[0]
            isIgnoreChr = 0
            if( chr == "chrM" or ( ignoreSexChr and ( chr == "chrX" or chr == "chrY" ) ) ):
                isIgnoreChr=1
            
            if( (ploidy == "X" or ploidy == "V" or ploidy == "I" or ploidy == "N" or ploidyType != "=" )
                and isIgnoreChr != 1
                and minLen <= len and  maxLen > len
                and minScore <= score and maxScore > score
                and minTailScore <= tailscore and maxTailScore > score):

                p=-2
                if ( ploidy != "X" and ploidy != "V" and ploidy != "I" and ploidy != "N" and ploidyType != "=" ):
                    p=int(ploidy)
                else:
                    if ( countNoCalls ):
                        p=-1
                if p > -2 :
                    calls += [ CNVCall(lw[0],int(lw[1]),int(lw[2]),p) ]
                    
    callfile.close()
    calls.sort(callcompare)
    return calls

def readKnowns(knownFileName,minLen,maxLen,ignoreSexChr):
    knowns = []
    knownfile=open(knownFileName,'r')
    knowns = []
    for l in knownfile:
        lw = l.split()
        if lw[2][0:5] == "start":
            continue
        chr = lw[1]
        if( lw[1][0:3] != "chr"):
            chr = "chr" + lw[1]
        isIgnoreChr = 0
        ploidy=-1
        if(len(lw)>4):
            ploidy=int(lw[4])
        if( chr == "chrM" or ( ignoreSexChr and ( chr == "chrX" or chr == "chrY" ) ) ):
            isIgnoreChr=1
        if(isIgnoreChr != 1 and int(lw[3])-int(lw[2]) >= minLen and int(lw[3])-int(lw[2]) < maxLen ):
            knowns += [ CNVCall(chr,int(lw[2]),int(lw[3]),ploidy) ]
    knownfile.close()
    knowns.sort(callcompare)
    return knowns

def whichtail(ploidy):
    if(ploidy==2):
        return 1
    if(ploidy==-1):
        return -1
    if(ploidy>2):
        return 2
    if(ploidy<2):
        return 0


def core_compare(calls,numcalls,other,numother,outFileName,verbose):

    if outFileName != '': 
        outfile=open(outFileName,'w')
    
    overlapbp = 0
    overlapsametailbp = 0
    overlapsameploidybp = 0
    overlappedSegs = 0
    overlapSamePloidySegs = 0
    overlapSamePloidyTailSegs = 0
    identicalExtentSegs = 0
    identicalInclPloidySegs = 0
    novelbp = 0
    totalbp = 0
    j=0

    for i in range(0,numcalls):

        chr = calls[i].chr
        beg = calls[i].beg
        end = calls[i].end
        callploidy = calls[i].ploidy
        hasOverlap = 0
        hasOverlapSamePloidy = 0
        hasOverlapSamePloidyTail = 0
        isIdenticalExtent = 0
        isIdenticalInclPloidy = 0
        mybp = (end-beg)
        myoverlapbp=0
        mynovelbp=0
        novel = 0
        
        while(j<numother and other[j].chr < chr):
            j += 1
        while(j<numother and other[j].chr == chr and other[j].end <= beg):
            j += 1

        #if(j<numother):
            #print chr + "," + str(beg) + "," + str(end) + ",start=" + str(j) + other[j].chr + "," + str(other[j].beg) + "," + str(other[j].end)

        k = j
        while(k< numother and other[k].chr == chr and other[k].beg < end and beg < end):

            if(other[k].beg == calls[i].beg and other[k].end == calls[i].end):
                isIdenticalExtent = 1
                if(other[k].ploidy == calls[i].ploidy):
                    isIdenticalInclPloidy = 1
                    
            if(other[k].beg > beg):
                #print "novel from " + str(beg) + " to " + str(other[k].beg)
                mynovelbp += other[k].beg-beg
                beg = other[k].beg
            if(other[k].end > end):
                myoverlapbp += end-beg
                if whichtail(callploidy) == whichtail(other[k].ploidy):
                    overlapsametailbp += end-beg
                    if callploidy == other[k].ploidy:
                        overlapsameploidybp += end-beg
                #print "known1 from " + calls[i].chr + " " + str(beg) + " to " + str(end)
                beg = end
                hasOverlap = 1
                if callploidy == other[k].ploidy:
                    hasOverlapSamePloidy += 1
                if whichtail(callploidy) == whichtail(other[k].ploidy):
                    hasOverlapSamePloidyTail += 1
            else:
                myoverlapbp += other[k].end - beg
                if whichtail(callploidy) == whichtail(other[k].ploidy):
                    overlapsametailbp += other[k].end - beg
                    if callploidy == other[k].ploidy:
                        overlapsameploidybp += other[k].end-beg
                #print "known2 from " + calls[i].chr + " " + str(beg) + " to " + str(other[k].end)
                beg = other[k].end
                hasOverlap = 1
                if callploidy == other[k].ploidy:
                    hasOverlapSamePloidy += 1
                if whichtail(callploidy) == whichtail(other[k].ploidy):
                    hasOverlapSamePloidyTail += 1
            if ( other[k].end <= end ):
                k += 1

        if(beg < end):
            #print "novel from " + str(beg) + " to " + str(end)
            mynovelbp += end-beg

        #print "\t" + str(novelbp) + "," + str(overlapbp)

        totalbp += mybp
        overlapbp += myoverlapbp
        novelbp += mynovelbp
        if ( novelbp + overlapbp != totalbp ):
            print "UHOH: " + str(novelbp+overlapbp) + " not equal " + str(totalbp)
            sys.exit(-1)

        if hasOverlap:
            overlappedSegs += 1
        if hasOverlapSamePloidy:
            overlapSamePloidySegs += 1
        if hasOverlapSamePloidyTail:
            overlapSamePloidyTailSegs += 1
        if isIdenticalExtent:
            identicalExtentSegs += 1
            if isIdenticalInclPloidy:
                identicalInclPloidySegs += 1

        if verbose:
            if hasOverlap:
                print "Overlapped " + calls[i].chr + " " + str(calls[i].beg) + " " + str(calls[i].end) + " " + str(calls[i].ploidy) + " " + str(myoverlapbp) + " " + str(mynovelbp)
            else:
                print "Unoverlapped " + calls[i].chr + " " + str(calls[i].beg) + " " + str(calls[i].end) + " " + str(calls[i].ploidy) + " " + str(myoverlapbp) + " " + str(mynovelbp)



    if outFileName != '':
        print >> outfile, "novel %d known %d\n" % ( novelbp, overlapbp )
        return ""
    else:
        return "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d" % ( overlapbp, novelbp, overlapsametailbp, overlappedSegs, numcalls-overlappedSegs , overlapSamePloidySegs, overlapSamePloidyTailSegs , identicalExtentSegs, identicalInclPloidySegs, overlapsameploidybp )

    
def overlapLists(fileAName, fileBName,outFileName,isKnownA,isKnownB,minLen,maxLen,minScore,minTailScore,maxScore,maxTailScore,swap,countNoCallsA,countNoCallsB,verbose,ignoreSexChr):

    calls = []
    if(isKnownA):
        calls = readKnowns(fileAName,minLen,maxLen,ignoreSexChr)
    else:
        calls = readCalls(fileAName,minLen,maxLen,minScore,minTailScore,maxScore,maxTailScore,countNoCallsA,ignoreSexChr)

    other = []
    if isKnownB:
        other = readKnowns(fileBName,minLen,maxLen,ignoreSexChr)
    else:
        other = readCalls(fileBName,0,9999999999,0,0,9999999999,9999999999,countNoCallsB,ignoreSexChr)

    numcalls = len(calls)
    numother = len(other)

    fwdcompare = core_compare(calls,numcalls,other,numother,outFileName,verbose)

    revcompare = ""
    if ( outFileName == "" ):
        tmp = other
        other = calls
        calls = tmp
        numcalls = len(calls)
        numother = len(other)
        revcompare = core_compare(calls,numcalls,other,numother,outFileName,False)
        print "FileA,FileB,minLen,maxLen,minScore,minTailScore,maxScore,maxTailScore,AandB,Aonly,AandBsameTail,Aoverlapped,Anot-overlapped,Aoverlapped-same-ploidy,Aoverlapped-same-ploidy-tail,Aidentical-extents,Aidentical-incl-ploidy,AandBsamePloidy,BandA,Bonly,BandAsameTail,Boverlapped,Bnot-overlapped,Boverlapped-same-ploidy,Boverlapped-same-ploidy-tail,Bidentical-extents,Bidentical-incl-ploidy,AandBsamePloidy\n" + fileAName + "," + fileBName + "," + str(minLen) + "," + str(maxLen) + "," + str(minScore) + "," + str(minTailScore) + "," + str(maxScore) + "," + str(maxTailScore) + "," + fwdcompare + "," + revcompare
        

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(outFile='',fileA='',fileB='',isKnownA=False,isKnownB=False,
                        minLen="0",maxLen="999999999",minScore="0",minTailScore="0",
                        maxScore="999999999",maxTailScore="999999999",swap=False,
                        countNoCallsA=False,countNoCallsB=False,verbose=False,
                        ignoreSexChr=False)
    parser.add_option('-a', '--fileA', dest='fileA',
                      help='file of called CNV intervals')
    parser.add_option('-A', '--isKnownA', dest='isKnownA', action='store_true',
                      help='file A calls are in "known" format (id,chr,beg,end,ploidy,comments), rather than cnv Segments file format')
    parser.add_option('-B', '--isKnownB', dest='isKnownB', action='store_true',
                      help='file B calls are in "known" format (id,chr,beg,end,ploidy,comments), rather than cnv Segments file format')
    parser.add_option('-b', '--fileB', dest='fileB',
                      help='file of alternative CNV intervals')
    parser.add_option('-o', '--output', dest='outFile',
                      help='output file, used only for pipeline stats (reduces amount of info)')
    parser.add_option('--minLen', dest='minLen',
                      help='minimum length (inclusive) of call (in file A; file B not restricted)')
    parser.add_option('--maxLen', dest='maxLen',
                      help='maximum length (exclusive) of call (in file A; file B not restricted)')
    parser.add_option('--minScore', dest='minScore',
                      help='minimum score of call (in file A; file B not restricted)')
    parser.add_option('--minTailScore', dest='minTailScore',
                      help='minimum tail score of call (in file A; file B not restricted)')
    parser.add_option('--maxScore', dest='maxScore',
                      help='maximum score of call (in file A; file B not restricted)')
    parser.add_option('--maxTailScore', dest='maxTailScore',
                      help='maximum tail score of call (in file A; file B not restricted)')
    parser.add_option('--Xa', dest='countNoCallsA',action='store_true',
                      help='No-called intervals in file A, if cnvSegments format, are to be counted for determining overlaps with calls in other file')
    parser.add_option('--Xb', dest='countNoCallsB',action='store_true',
                      help='No-called intervals in file B, if cnvSegments format, are to be counted for determining overlaps with calls in other file')
    parser.add_option('--verbose', dest='verbose',action='store_true',
                      help='Output overlap details for A segments')
    parser.add_option('--ignore-sex-chr', dest='ignoreSexChr',action='store_true',
                      help='Ignore segments on sex chromosomes')
    
    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')
    if not options.fileA or not options.fileB:
        parser.error('two CNV call files must be specified (with --fileA and --fileB')
        
    overlapLists(options.fileA,options.fileB,options.outFile,options.isKnownA,options.isKnownB,
                 int(options.minLen),int(options.maxLen),int(options.minScore),int(options.minTailScore),
                 int(options.maxScore),int(options.maxTailScore),options.swap,options.countNoCallsA,options.countNoCallsB,options.verbose,
                 options.ignoreSexChr)
        


if __name__ == '__main__':
    main()
