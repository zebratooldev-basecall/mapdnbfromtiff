#!/usr/bin/python

from driverlib import *

Params( 'compare', [ ('copygold', '0'),] )
copyGold = Params.get('compare', 'copygold')

class Compare(object):
    tests = {}
    copyGoldMode = False
    
    def __init__(self, name, stepsToCompare, workDir):
        self.compareName = name 
        self.stepsToCompare = stepsToCompare
        self.steps = []
        self.workDir = workDir
        
    def Test(self, fileName, goldFileName, method="bindiff", mem=1):
        testName = '_'.join(["test",self.compareName, method, os.path.basename(fileName)])
        if Compare.tests.has_key(testName):
            testNum = Compare.tests[testName]+1
            testName = testName+str(testNum)
            Compare.tests[testName] = testNum
        else:
            Compare.tests[testName] = 0
              
        testParamsName = '_'.join(["params",testName])

        additionalParams=[]
        if Compare.copyGoldMode:
            additionalParams=[('copy-gold','1')]
            
        Params(testParamsName,
               [
                   ('input-file', fileName),
                   ('input-gold', goldFileName),
                   ('output', os.path.join(self.workDir,testName)),
                   ('method', method),
                   ('no-error', '1'),
               ] + additionalParams)

        Step(testName, 
             commands=createCommand('diff.py', [testParamsName]), 
             depends=self.stepsToCompare, 
             memory=mem)
        return testName


def checkTestResults(workDir,emails = []):
    checkResultsScript = """#!/usr/bin/env python
import sys, os
workDir = "%s"
emails = %s
filesToCheck = %s
failedTests = []

for file in filesToCheck:

    if os.path.exists(os.path.join(workDir,file+"-FAILED")):
        failedTests.append(file)
    else:
        if not os.path.exists(os.path.join(workDir,file+"-OK")):
            raise Exception('test result not found: '+file)
            
failedTestNo = len(failedTests)

if failedTestNo>0:
    failedTestList="\\n".join(failedTests)
    fileName = os.path.join(workDir,"FAILED")
    outFile = open(fileName,"w+")
    print >>outFile, failedTestList
    #send emails is tests failed
    if len(emails)>0:
        os.system('cat '+fileName+' | mail -s "tests failed in '+workDir+'s" '+",".join(emails))
        
else:
    outFile = open(os.path.join(workDir,"SUCCESS"),"w+")
    print >>outFile, "all the tests executed successfully"
    if len(emails)>0:
        cmd = 'echo all the tests executed successfully | mail -s "tests succeeded in '+workDir+'" '+",".join(emails)
        os.system(cmd)
    
outFile.close()
"""
    checkResultsScript = checkResultsScript%(workDir,emails,Compare.tests.keys())
    scriptName = os.path.join(workDir,'checkTestResults.py')
    File(scriptName,checkResultsScript)
    Step("checkTestResults", 
         commands=["python "+scriptName],
         depends=Compare.tests.keys(), 
         memory=1)
    return "checkTestResults"

#use to append parameters from command line to a set
#fixes missing functionality in driver.py
#use syntax -p paramList.name=$ to set bool flags 
def appendParamSets(paramLists = Params.overrides.keys()):
    for ovName in paramLists:
        if Params.paramSets.has_key(ovName) and Params.overrides.has_key(ovName):
            ovList = Params.overrides[ovName]
            paramSet = Params.paramSets[ovName]
            for p in ovList:
                found = False
                for i in xrange(len(paramSet)):
                    (pn,pv) = paramSet[i]
                    if pn==p:
                        found = True
                        break
                        #paramSet[i] = (pn,ovList[p])
                        #print "overriding parameter: ",ovName+'.'+p,"to",ovList[p]
                if not found:
                    val = ovList[p]
                    if val=='$':
                        val = ''
                    paramSet.append((p,val))
                    print "append parameter: ",ovName+'.'+p,"as",val

if copyGold==str(1):
    print "WARNING: running in copy gold mode"
    Compare.copyGoldMode = True
