#!/bin/env python
import os,sys
import optparse
from cgatools.util import DelimitedFile
from cgatools.reference import CrrFile

def addMeiFields(meiDF):
    meiDF.addField('Chromosome')
    meiDF.addField('InsertRangeBegin',conv=int)
    meiDF.addField('InsertRangeEnd',conv=int)
    meiDF.addField('Strand')
    meiDF.addField('ElementType')
    meiDF.addField('ElementTypeScore')
    meiDF.addField('ElementSequenceBegin',conv=int)
    meiDF.addField('ElementSequenceEnd',conv=int)
    meiDF.addField('NextBestElementType')
    meiDF.addField('InsertionScore')
    meiDF.addField('InsertionDnbCount')
    meiDF.addField('InsertionLeftDnbCount')
    meiDF.addField('InsertionRightDnbCount')
    meiDF.addField('ReferenceDnbCount',sub={'N':'.'})
    meiDF.addField('KnownEventSensitivityForInsertionScore')
    #meiDF.addField('GeneOverlap')
    #meiDF.addField('XRef')
    #meiDF.addField('FrequencyInBaseline')
    #meiDF.addField('NovelEventCountForInsertionScore')


def getSuperType(elementType):
    if elementType[:3].upper() == 'ALU':
        return( 'ALU' )
    if elementType[:2].upper() == 'L1':
        return( 'L1' )
    if elementType[:3].upper() == 'SVA':
        return( 'SVA' )
    if elementType[:3].upper() == 'MER':
        return( 'MER' )
    if elementType[:3].upper() == 'LTR':
        return( 'LTR' )
    if elementType[:5].upper() == 'POLYA':
        return( 'POLYA' )
    if elementType[:4].upper() == 'HERV':
        return( 'HERV' )
    raise Exception('unexpected elementType')
    
    
def write_single_mei_vcf_line(out,
                              chr,insertRangeBegin,insertRangeEnd,
                              strand,elementType,elementTypeScore,
                              elementSequenceBegin,elementSequenceEnd,
                              nextBestElementType,
                              insertionScore,insertionDnbCount,
                              insertionLeftDnbCount,insertionRightDnbCount,
                              referenceDnbCount,knownEventSensitivity,
                              crrFile):

    pos = int( (insertRangeBegin+insertRangeEnd)/2 )

    if pos == 0:
        refBase = '.'
    else:
        refBase = crrFile.getSequence(chr,pos-1,pos)
    
    if chr[0:3] == 'chr':
        chr = chr[3:]

    superType = getSuperType(elementType)

    if elementSequenceEnd <= elementSequenceBegin:
        raise Exception("logic error regarding elementSequence positions")

    
    infoStr = 'IMPRECISE;SVTYPE=INS;END=%d;SVLEN=%d;CIPOS=%d,%d;MEINFO=%s,%d,%d,%s;NS=1' %  (
                 pos,
                 elementSequenceEnd-elementSequenceBegin,
                 insertRangeBegin-pos,
                 insertRangeEnd-pos,
                 elementType,
                 elementSequenceBegin+1,
                 elementSequenceEnd,
                 strand
                 )

    filterStatus = 'PASS'
    if float(knownEventSensitivity) > .75:
        filterStatus = 'sns75'
        if float(knownEventSensitivity) > .95:
            filterStatus = 'sns95'

    if nextBestElementType == '':
        nextBestElementType = '.'
        
    formatStr = '%s:%s:%s:%s:%s:%s:%s:%s:%s:%s' % ( '.',
                                                    filterStatus,
                                                    insertionScore,
                                                    insertionDnbCount,
                                                    insertionLeftDnbCount,
                                                    insertionRightDnbCount,
                                                    referenceDnbCount,
                                                    nextBestElementType,
                                                    elementTypeScore,
                                                    knownEventSensitivity)

    formatSpec = 'GT:FT:CGA_IS:CGA_IDC:CGA_IDCL:CGA_IDCR:CGA_RDC:CGA_NBET:CGA_ETS:CGA_NES'
    
    out.write('%s\t%d\t.\t%s\t<INS:ME:%s>\t.\t.\t%s\t%s\t%s\n' %
       ( chr, pos, refBase, superType, infoStr, formatSpec, formatStr) )

              
def write_mei_vcf_header(out,sampleId):
    out.write('##fileformat=VCFv4.1\n' +
              '##ALT=<ID=INS:ME:ALU,Description="Insertion of ALU element">\n' +
              '##ALT=<ID=INS:ME:L1,Description="Insertion of L1 element">\n' +
              '##ALT=<ID=INS:ME:SVA,Description="Insertion of SVA element">\n' +
              '##ALT=<ID=INS:ME:MER,Description="Insertion of MER element">\n' +
              '##ALT=<ID=INS:ME:LTR,Description="Insertion of LTR element">\n' +
              '##ALT=<ID=INS:ME:PolyA,Description="Insertion of PolyA element">\n' +
              '##ALT=<ID=INS:ME:HERV,Description="Insertion of HERV element">\n' +
              '##FILTER=<ID=sns75,Description="Sensitivity to known MEI calls in range (.75,.95] i.e. medium FDR">\n' +
              '##FILTER=<ID=sns95,Description="Sensitivity to known MEI calls in range (.95,1.00] i.e. high to very high FDR">\n' +
              '##INFO=<ID=NS,Number=1,Type=Integer,Description="Number of Samples With Data">\n' +
              '##INFO=<ID=CIPOS,Number=2,Type=Integer,Description="Confidence interval around POS for imprecise variants">\n' +
              '##INFO=<ID=END,Number=1,Type=Integer,Description="End position of the variant described in this record">\n' +
              '##INFO=<ID=IMPRECISE,Number=0,Type=Flag,Description="Imprecise structural variation">\n' +
              '##INFO=<ID=MEINFO,Number=4,Type=String,Description="Mobile element info of the form NAME,START,END,POLARITY">\n' +
              '##INFO=<ID=SVLEN,Number=.,Type=Integer,Description="Difference in length between REF and ALT alleles">\n' +
              '##INFO=<ID=SVTYPE,Number=1,Type=String,Description="Type of structural variant">\n' +
              '##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">\n' +
              '##FORMAT=<ID=FT,Number=1,Type=String,Description="Genotype filters">\n' +
              '##FORMAT=<ID=CGA_IS,Number=1,Type=Float,Description="MEI InsertionScore: confidence in occurrence of an insertion">\n' +
              '##FORMAT=<ID=CGA_IDC,Number=1,Type=Float,Description="MEI InsertionDnbCount: count of paired ends supporting insertion">\n' +
              '##FORMAT=<ID=CGA_IDCL,Number=1,Type=Float,Description="MEI InsertionLeftDnbCount: count of paired ends supporting insertion on 5\' end of insertion point">\n' +
              '##FORMAT=<ID=CGA_IDCR,Number=1,Type=Float,Description="MEI InsertionRightDnbCount: count of paired ends supporting insertion on 3\' end of insertion point">\n' +
              '##FORMAT=<ID=CGA_RDC,Number=1,Type=Float,Description="MEI ReferenceDnbCount: count of paired ends supporting reference allele">\n' +
              '##FORMAT=<ID=CGA_NBET,Number=1,Type=String,Description="MEI NextBestElementType: (sub)type of second-most-likely inserted mobile element">\n' +
              '##FORMAT=<ID=CGA_ETS,Number=1,Type=Float,Description="MEI ElementTypeScore: confidence that insertion is of type indicated by CGA_ET/ElementType">\n' +
              '##FORMAT=<ID=CGA_NES,Number=1,Type=Float,Description="MEI KnownEventSensitivityForInsertionScore: fraction of known MEI insertion polymorphisms called for this sample with CGA_IS at least as high as for the current call">\n' +
              ('#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t%s\n' % (sampleId) ) )


def makeMeiVCF(sampleId,
            meiFile,
            outFile,crrFile):

    mei = DelimitedFile(meiFile)
    addMeiFields(mei)

    out = open(outFile,'w')
    write_mei_vcf_header(out,sampleId)
    
    for (chr,insertRangeBegin,insertRangeEnd,
         strand,elementType,elementTypeScore,
         elementSequenceBegin,elementSequenceEnd,
         nextBestElementType,
         insertionScore,insertionDnbCount,
         insertionLeftDnbCount,insertionRightDnbCount,
         referenceDnbCount,knownEventSensitivity ) in mei:

        write_single_mei_vcf_line(out,
                                  chr,insertRangeBegin,insertRangeEnd,
                                  strand,elementType,elementTypeScore,
                                  elementSequenceBegin,elementSequenceEnd,
                                  nextBestElementType,
                                  insertionScore,insertionDnbCount,
                                  insertionLeftDnbCount,insertionRightDnbCount,
                                  referenceDnbCount,knownEventSensitivity,
                                  crrFile)

        

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(sampleId='',meiFile='',
                        outFile='', crrFile='')
    parser.add_option('--mei-file',dest='meiFile',
                      help='File containing mobile element insertion calls')
    parser.add_option('--sample-id',dest='sampleId',
                      help='Sample ID')
    parser.add_option('--output-file',dest='outFile',
                      help='name of file to contain output')
    parser.add_option('--crr-file',dest='crrFile',
                      help='Reference crr file')
    
    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.meiFile:
        parser.error('mei-file specification required')
    if not options.sampleId:
        parser.error('sample-id specification required')
    if not options.outFile:
        parser.error('output-file specification required')
    if not options.crrFile:
        parser.error('crr-file specification required')

    crrFile = CrrFile(options.crrFile)
    
    makeMeiVCF(options.sampleId,options.meiFile,
               options.outFile,crrFile)


if __name__ == '__main__':
    main()
