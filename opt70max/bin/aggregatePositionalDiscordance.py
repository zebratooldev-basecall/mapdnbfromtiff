#!/usr/bin/env python

from reportReader import CgiReport
from os.path import join as pjoin
from optparse import OptionParser
import copy
import glob
import sys
import numpy as np

results = {}

TABLE_LIST = ['discordancePerPosition',
              'referenceErrorCounts',
              'callBaseErrorCounts',
              'correlatedDiscPerPosition',
              'correlatedBaseCallsForDiscPositions',
              'positionalConfustionMatrices']

def load_reports(inputDirectory):
    reports = {}

    # loop through discordance pattern files.
    for fn in glob.glob('%s/map-*' % inputDirectory):
        # load report.
        cr = CgiReport(fn)

        # loop through tables and add them to the results.
        for name,table in cr.arrayIdx.items():
            if not reports.has_key(name):
                reports[name] = []
            reports[name].append(table)

    return reports

def zero_out_counts(record,keys):
    recordCopy = copy.deepcopy(record)
    for key in keys:
        recordCopy[key] = 0

    return recordCopy

def aggregate_positional_discordance(tables):
    results = {}
    keys = ['totalBaseCount','discordantBaseCount']

    # initialize results.
    for record in tables[0]:
        rc = zero_out_counts(record, keys)
        results[record['position']] = copy.deepcopy(rc)

    # loop through tables.
    for n in range(len(tables)):
        for record in tables[n]:
            pos = record['position']
            for key in keys:
                results[pos][key] += int(record[key])

    # add rate.
    for record in results.values():
        try:
            record['discordanceRate'] = float(record['discordantBaseCount'])/float(record['totalBaseCount'])
        except ZeroDivisionError:
            record['discordanceRate'] = float('Inf')
            

    return results.values()

def write_positional_discordance(records, fn):
    HEADER = ['position','totalBaseCount','discordantBaseCount','discordanceRate']

    ff = open(fn, 'w')
    ff.write(','.join(HEADER) + '\n')
    for record in records:
        ff.write('%s,%s,%s,%f\n' % (record['position'],
                                    record['totalBaseCount'],
                                    record['discordantBaseCount'],
                                    record['discordanceRate']))
    ff.close()
            
def aggregate_reference_errors(tables):
    results = []
    indexedResults = {}
    keys = ['totalBaseCount','discordantBaseCount']

    # initialize results.
    for record in tables[0]:
        rc = zero_out_counts(record, keys)
        results.append(rc)
        if not indexedResults.has_key(record['position']):
            indexedResults[record['position']] = {}
        indexedResults[record['position']][record['refBase']] = rc

    # loop through tables.
    for n in range(len(tables)):
        for record in tables[n]:
            pos = record['position']
            refBase = record['refBase']
            for key in keys:
                indexedResults[pos][refBase][key] += int(record[key])

    # add an error rate.
    for record in results:
        try:
            record['referenceErrorRate'] = float(record['discordantBaseCount'])/float(record['totalBaseCount'])
        except ZeroDivisionError:
            record['referenceErrorRate'] = float('Inf')

    # normalize error rate.
    for pos in indexedResults.keys():
        records = indexedResults[pos].values()
        if records[0]['referenceErrorRate'] != float('Inf'):
            sumRERates = sum([r['referenceErrorRate'] for r in records])
        for record in records:
            if record['referenceErrorRate'] == float('Inf'):
                record['normReferenceErrorRate'] = float('Inf')
            else:
                record['normReferenceErrorRate'] = record['referenceErrorRate']/sumRERates

    return results

def write_reference_error_rates(records, fn):
    HEADER = ['position','refBase','totalBaseCount','discordantBaseCount','referenceErrorRate','normReferenceErrorRate']

    ff = open(fn, 'w')
    ff.write(','.join(HEADER) + '\n')
    for record in records:
        ff.write('%s,%s,%s,%s,%f,%f\n' % (record['position'],
                                          record['refBase'],
                                          record['totalBaseCount'],
                                          record['discordantBaseCount'],
                                          record['referenceErrorRate'],
                                          record['normReferenceErrorRate']))
    ff.close()

def aggregate_base_call_errors(tables):
    results = []
    indexedResults = {}
    keys = ['totalBaseCount','discordantBaseCount']

    # initialize results.
    for record in tables[0]:
        rc = zero_out_counts(record, keys)
        results.append(rc)
        if not indexedResults.has_key(record['position']):
            indexedResults[record['position']] = {}
        indexedResults[record['position']][record['callBase']] = rc

    # loop through tables.
    for n in range(len(tables)):
        for record in tables[n]:
            pos = record['position']
            refBase = record['callBase']
            for key in keys:
                indexedResults[pos][refBase][key] += int(record[key])

    # add an error rate.
    for record in results:
        try:
            record['callBaseErrorRate'] = float(record['discordantBaseCount'])/float(record['totalBaseCount'])
        except ZeroDivisionError:
            record['callBaseErrorRate'] = float('Inf')

    # normalize error rate.
    for pos in indexedResults.keys():
        records = indexedResults[pos].values()
        if records[0]['callBaseErrorRate'] != float('Inf'):
            sumRERates = sum([r['callBaseErrorRate'] for r in records])
        for record in records:
            if record['callBaseErrorRate'] == float('Inf'):
                record['normCallBaseErrorRate'] = float('Inf')
            else:
                record['normCallBaseErrorRate'] = record['callBaseErrorRate']/sumRERates

    return results

def write_base_call_error_rates(records, fn):
    HEADER = ['position','callBase','totalBaseCount','discordantBaseCount','callBaseErrorRate','normCallBaseErrorRate']

    ff = open(fn, 'w')
    ff.write(','.join(HEADER) + '\n')
    for record in records:
        ff.write('%s,%s,%s,%s,%f,%f\n' % (record['position'],
                                          record['callBase'],
                                          record['totalBaseCount'],
                                          record['discordantBaseCount'],
                                          record['callBaseErrorRate'],
                                          record['normCallBaseErrorRate']))
    ff.close()

def aggregate_corr_discordance(tables):
    results = []
    indexedResults = {}
    keys = ['totalDiscBaseCount','correlatedDiscBaseCount']

    # initialize results.
    for record in tables[0]:
        rc = zero_out_counts(record, keys)
        results.append(rc)
        if not indexedResults.has_key(rc['position']):
            indexedResults[rc['position']] = {}
        indexedResults[rc['position']][rc['otherPosition']] = rc

    # loop through tables.
    for n in range(len(tables)):
        for record in tables[n]:
            pos = record['position']
            oPos = record['otherPosition']
            for key in keys:
                indexedResults[pos][oPos][key] += int(record[key])

    # add an correlation rate.
    for record in results:
        try:
            record['correlatedDiscordanceFraction'] = float(record['correlatedDiscBaseCount'])/float(record['totalDiscBaseCount'])
        except ZeroDivisionError:
            record['correlatedDiscordanceFraction'] = float('Inf')

    return results

def write_correlated_discordance(records, fn):
    HEADER = ['queryPosition','comparisonPosition','totalDiscBaseCount','correlatedDiscBaseCount','correlatedDiscordanceFraction']

    ff = open(fn, 'w')
    ff.write(','.join(HEADER) + '\n')
    for record in records:
        ff.write('%s,%s,%s,%s,%f\n' % (record['position'],
                                       record['otherPosition'],
                                       record['totalDiscBaseCount'],
                                       record['correlatedDiscBaseCount'],
                                       record['correlatedDiscordanceFraction']))
    ff.close()

def aggregate_correlated_calls(tables):
    results = []
    indexedResults = {}
    keys = ['totalDiscBaseCount','correlatedBaseCalls']

    # initialize results.
    for record in tables[0]:
        rc = zero_out_counts(record, keys)
        results.append(rc)
        if not indexedResults.has_key(rc['position']):
            indexedResults[rc['position']] = {}
        indexedResults[rc['position']][rc['otherPosition']] = rc

    # loop through tables.
    for n in range(len(tables)):
        for record in tables[n]:
            pos = record['position']
            oPos = record['otherPosition']
            for key in keys:
                indexedResults[pos][oPos][key] += int(record[key])

    # compute over abundance of correlated calls.
    #   - (observed - (AVG(correlatedBaseCallFraction) * totalDiscBaseCount))
    #   - most likely there is some double counting going on here!
    corrBaseCalls = np.array([np.float(r['correlatedBaseCalls']) for r in results if r['totalDiscBaseCount']>0])
    totBaseCalls = np.array([np.float(r['totalDiscBaseCount']) for r in results if r['totalDiscBaseCount']>0])
    avgCorrBaseCallFraction = np.average(corrBaseCalls / totBaseCalls)

    # add computed variables.
    for record in results:
        record['overAbundantCorrBaseCalls'] = int(record['correlatedBaseCalls']) - int(float(record['totalDiscBaseCount']) * avgCorrBaseCallFraction)

        # zero safe division.
        if record['totalDiscBaseCount'] > 0:
            record['correlatedBaseCallFraction'] = float(record['correlatedBaseCalls'])/float(record['totalDiscBaseCount'])
            record['fracOverAbundantCorrBaseCalls'] = float(record['overAbundantCorrBaseCalls'])/float(record['totalDiscBaseCount'])
        else:
            record['correlatedBaseCallFraction'] = float('Inf')
            record['fracOverAbundantCorrBaseCalls'] = float('Inf')

    return results

def write_correlated_base_calls(records, fn):
    HEADER = ['queryPosition','comparisonPosition','totalDiscBaseCount','correlatedBaseCalls','correlatedBaseCallFraction','overAbundantCorrBaseCalls','fracOverAbundantCorrBaseCalls']

    ff = open(fn, 'w')
    ff.write(','.join(HEADER) + '\n')
    for record in records:
        ff.write('%s,%s,%s,%s,%f,%d,%f\n' % (record['position'],
                                             record['otherPosition'],
                                             record['totalDiscBaseCount'],
                                             record['correlatedBaseCalls'],
                                             record['correlatedBaseCallFraction'],
                                             record['overAbundantCorrBaseCalls'],
                                             record['fracOverAbundantCorrBaseCalls']))
    ff.close()

def aggregate_confusion_matrices(tables):
    results = []
    indexedResults = {}
    keys = ['count']
    countsByPos = {}

    # initialize results.
    for record in tables[0]:
        rc = zero_out_counts(record, keys)
        results.append(rc)
        if not indexedResults.has_key(rc['position']):
            indexedResults[rc['position']] = {}
        if not indexedResults[rc['position']].has_key(rc['callBase']):
            indexedResults[rc['position']][rc['callBase']] = {}
        indexedResults[rc['position']][rc['callBase']][rc['refBase']] = rc

    # loop through tables.
    for n in range(len(tables)):
        for record in tables[n]:
            pos = record['position']
            cB = record['callBase']
            rB = record['refBase']
            for key in keys:
                indexedResults[pos][cB][rB][key] += int(record[key])
            if not countsByPos.has_key(pos):
                countsByPos[pos] = 0
            countsByPos[pos] += int(record['count'])

    # add computed variables (fraction).
    for record in results:
        pos = record['position']
        if countsByPos[pos] > 0:
            record['fraction'] = float(record['count']) / float(countsByPos[pos])
        else:
            record['fraction'] = float('Inf')

    return results

def write_positional_confusion_matrices(records, fn):
    HEADER = ['position','callBase','refBase','count','fraction']

    ff = open(fn, 'w')
    ff.write(','.join(HEADER) + '\n')
    for record in records:
        ff.write('%s,%s,%s,%s,%f\n' % (record['position'],
                                       record['callBase'],
                                       record['refBase'],
                                       record['count'],
                                       record['fraction']))
    ff.close()
        

def aggregate_reports(reports):
    # positional discordance.
    if not reports.has_key('discordancePerPosition'):
        raise Exception('discordancePerPosition table missing from reports!')

    posDisc = aggregate_positional_discordance(reports['discordancePerPosition'])

    # reference error counts.
    if not reports.has_key('referenceErrorCounts'):
        raise Exception('referenceErrorCounts table missing from reports!')

    refErrorRates = aggregate_reference_errors(reports['referenceErrorCounts'])

    # call base error counts.
    if not reports.has_key('callBaseErrorCounts'):
        raise Exception('callBaseErrorCounts table missing from reports!')

    callBaseErrorRates = aggregate_base_call_errors(reports['callBaseErrorCounts'])

    # correlated discordance per position.
    if not reports.has_key('correlatedDiscPerPosition'):
        raise Exception('correlatedDiscPerPosition table missing from reports!')

    correlatedDiscordance = aggregate_corr_discordance(reports['correlatedDiscPerPosition'])

    # correlated base calls per position.
    if not reports.has_key('correlatedBaseCallsForDiscPositions'):
        raise Exception('correlatedBaseCallsForDiscPositions table missing from reports!')

    correlatedBaseCalls = aggregate_correlated_calls(reports['correlatedBaseCallsForDiscPositions'])

    # positional confusion matrices.
    if not reports.has_key('positionalConfustionMatrices'):
        raise Exception('positionalConfustionMatrices table missing from reports!')

    posConfusionMatrices = aggregate_confusion_matrices(reports['positionalConfustionMatrices'])

    return posDisc, refErrorRates, callBaseErrorRates, correlatedDiscordance, correlatedBaseCalls, posConfusionMatrices 
    

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-d', '--input-directory', help='top level discordance directory in disc pattern workflow', dest='input_directory')
    parser.add_option('-l', '--lane',            help='lane to run aggregation on',                               dest='lane')

    # parse command line arguments.
    options, args = parser.parse_args(arguments[1:])

    return options.input_directory, options.lane

def main(arguments):
    # parse command line arguments.
    inputDirectory, lane = parse_arguments(arguments)

    # create directory for lane.
    laneDir = pjoin(inputDirectory, lane)

    # load reports.
    reports = load_reports(laneDir)

    # aggregate tables.
    posDisc, refErrorRates, callBaseErrorRates, correlatedDiscordance, correlatedBaseCalls, posConfusionMatrices = aggregate_reports(reports)

    # write aggregated reports to disk.
    write_positional_discordance(posDisc, pjoin(inputDirectory, lane, 'positional_discordance.csv'))
    write_reference_error_rates(refErrorRates, pjoin(inputDirectory, lane, 'reference_error_rates.csv'))
    write_base_call_error_rates(callBaseErrorRates, pjoin(inputDirectory, lane, 'call_base_error_rates.csv'))
    write_correlated_discordance(correlatedDiscordance, pjoin(inputDirectory, lane, 'correlated_discordance.csv'))
    write_correlated_base_calls(correlatedBaseCalls, pjoin(inputDirectory, lane, 'correlated_base_calls.csv'))
    write_positional_confusion_matrices(posConfusionMatrices, pjoin(inputDirectory, lane, 'positional_confusion_matrices.csv'))

main(sys.argv)
