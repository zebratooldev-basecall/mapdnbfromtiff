#! /usr/bin/env python

import sys
from optparse import OptionParser


def varChar(genotype, refG):
    if genotype == 'NO-CALL':
        return 'N'
    if genotype == 'NO-MATCH':
        return '0'
    if genotype == 'NO-ALLELE':
        return 'N'
    if genotype == refG:
        return '0'
    return '1'


def writeDbsnptool(fields, out, msg):
    chrom = fields[2]
    begin = fields[3]
    end = fields[4]
    vars = map(lambda x:x.replace('-',''), fields[1].split('/'))
    out.write('%s,%s,F,%s,%s,B,%s,%s\n' % (chrom,begin,chrom,end,msg,','.join(vars)))


def doDbsnptool(dsfn, msg, zygosityByVariant, statsff, ncff, called0ff):
    (nc, called0, match1, match) = (0,0,0,0)
    ff = open(dsfn)
    while True:
        header = ff.readline()
        if '' == header:
            raise Exception('header not found')
        if len(header) > 1 and header[0] == '>':
            break
    zyg2 = {}
    for line in ff:
        fields = line.rstrip('\r\n').split('\t')
        vars = fields[1].split('/')
        for var in vars:
            if var != fields[5]:
                break
        if var == '-':
            var = ''
        key = '%s,%s,%s,%s' % (fields[2], fields[3], fields[4], var)
        if key not in zygosityByVariant or key in zyg2:
            continue
        zyg1k = zygosityByVariant[key]
        zygcg = ''.join(sorted([ varChar(field, fields[5]) for field in fields[6:8] ]))
        zyg2[key] = zygcg
        if zygcg == zyg1k:
            match += 1
        elif zygcg.find('1') != -1:
            match1 += 1
        elif zygcg.find('N') == -1:
            called0 += 1
            writeDbsnptool(fields, called0ff, 'called0')
        else:
            nc += 1
            writeDbsnptool(fields, ncff, 'nc')
    ff.close()
    statsff.write('%s,%d,%d,%d,%d\n' % (msg, nc, called0, match1, match))


def main():
    parser = OptionParser('usage: %prog [--task=TASKID] PARAMS')
    parser.disable_interspersed_args()
    parser.add_option('--kg-file', default=None,
                      help='Path to 1kg tv.tsv file.')
    parser.add_option('--ds-file', default=None,
                      help='Path to dbsnptool2 output file.')
    parser.add_option('--output-prefix', default='',
                      help='Prefix for output files.')

    (options, args) = parser.parse_args()

    # Read in 1kg data.
    zygosityByVariant = {}
    ff = open(options.kg_file)
    header = ff.readline()
    for line in ff:
        fields = line.rstrip('\r\n').split('\t')
        zygosityByVariant['%s,%s,%s,%s' % (fields[1], fields[2], fields[3], fields[6])] = fields[8]
    ff.close()

    statsff = open(options.output_prefix+'1kgstats.csv', 'w')
    statsff.write('compare1kgindel\n')
    statsff.write('method,nc,called0,match1,match\n')
    ncff = open(options.output_prefix+'nc.tsv', 'w')
    called0ff = open(options.output_prefix+'called0.tsv', 'w')

    doDbsnptool(options.ds_file, 'dbsnptool-relaxed', zygosityByVariant, statsff, ncff, called0ff)


if __name__ == '__main__':
    main()
