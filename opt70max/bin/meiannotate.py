#!/usr/bin/python
import os, sys, optparse
import matplotlib
matplotlib.use('Agg')
import pylab as P

from cgiutils import bundle
from gbifile import GbiFile
from quicksect import IntervalNode

GRAPH_COLOR1 = '#3A6F8F' # 58.111.143
GRAPH_COLOR2 = '#F08E4F' # 240.142.79

DEFAULT_EXTEND_LEN = 50 # uncertainty around insertion interval

class Gene(object):
    def __init__(self, name, chrom, exon_starts, exon_ends, strand):
        self.name = name
        self.chrom = chrom
        self.start = exon_starts[0]
        self.end = exon_ends[-1]
        self.exons = [ x for x in zip(exon_starts, exon_ends) ]
        self.strand = strand

    def intersects_exon(self, chrom, start, end):
        if chrom != self.chrom:
            return False
        for s, e in self.exons:
            if start < e and s < end:
                return True
        return False

class AnnotationStore(object):
    def __init__(self):
        self.chroms = {}

    def insert(self, ann):
        chrom = ann.chrom
        if chrom in self.chroms:
            self.chroms[chrom] = self.chroms[chrom].insert(ann.start, ann.end, 0, ann)
        else:
            self.chroms[chrom] = IntervalNode(ann.start, ann.end, 0, ann)

    def intersect(self, chrom, start, end):
        r = []
        if chrom in self.chroms:
            self.chroms[chrom].intersect(start, end, lambda x: r.append(x.other))
        return r

def split_line(s, delim):
    return [ x.strip() for x in s.split(delim) ]

def read_gene_file(fn):
    '''Read gene_annotations.csv file and return the interval tree of genes.
    '''
    f = open(fn)
    for line in f:
        if line.startswith('mrnaAcc,'):
            break
    store = AnnotationStore()
    for line in f:
        data = split_line(line, ',')
        exon_starts = [ int(x) - 1 for x in data[7].split(';') ]
        exon_ends = [ int(x) for x in data[8].split(';') ]
        if not exon_starts or len(exon_starts) != len(exon_ends):
            continue
        chrom = data[6].strip()
        if chrom == 'MT':
            continue
        chrom = 'chr' + chrom
        name = data[3].strip()
        if not name:
            continue
        if data[4] not in '-+':
            continue
        gene = Gene(name, chrom, exon_starts, exon_ends, data[4])
        store.insert(gene)
    f.close()
    return store

def convert_1k_vcf_file(input_fn, output_fn):
    '''Read 1000 genomes VCF file describing MEI events and write the
    events out in BED-compatible format.

    BED format is chosen for compatibility with LiftOver tool for conversion
    to build 37. See http://genome.ucsc.edu/cgi-bin/hgLiftOver .
    '''
    f = open(input_fn)
    out = open(output_fn, 'w')
    for line in f:
        if not line.strip() or line[0] == '#':
            continue
        tokens = split_line(line, '\t')
        chrom, pos, xid = tokens[0], int(tokens[1]), tokens[2]
        # some of the IDs are of the form ID1&ID2. One ID is enough for me
        xid = xid.split('&')[0]
        alt = tokens[4].split(':')[-1][:-1] # parse 'ALU' out of '<INS:ME:ALU>'
        print >>out, 'chr%s\t%d\t%d\t%s:%s' % (chrom, pos, pos+1, alt, xid)
    f.close()
    out.close()

def read_1k_insertions_bed(fn, extend_len):
    '''Read 1000 genome MEI data from a BED file, turn each event into and
    interval 2*extend_len bases long, and return a tree of these intervals.'''
    store = AnnotationStore()
    f = open(fn)
    for line in f:
        if line[0] in '#>':
            continue
        tokens = split_line(line, '\t')
        chrom, pos, xid = tokens[0], int(tokens[1]), tokens[3]
        mei = bundle(chrom=chrom, start=(pos-extend_len), end=(pos+extend_len), xid=xid)
        store.insert(mei)
    f.close()
    return store

def read_mei_baseline_freqs(fn, extend_len):
    '''Read frequency in the baseline genome set data and return a tree
    with intervals'''
    store = AnnotationStore()
    f = open(fn)
    for line in f:
        if line[0] in '#>':
            continue
        tokens = split_line(line, '\t')
        chrom, s, e, freq = tokens[0], int(tokens[1]), int(tokens[2]), float(tokens[3])
        evt = bundle(chrom=chrom, start=(s-extend_len), end=(e+extend_len), freq=freq)
        store.insert(evt)
    f.close()
    return store

def load_mei_ranges(gid, fn):
    '''Returns the list of all MEI ranges in the given file as bundles of
    chrom, start, end, gid.'''
    result = []
    f = open(fn)
    for line in f:
        if line[0] in '#>':
            continue
        tokens = split_line(line, '\t')
        chrom, s, e = tokens[0], int(tokens[1]), int(tokens[2])
        mei = bundle(chrom=chrom, start=s, end=e, gid=gid)
        result.append(mei)
    f.close()
    return result

def create_mei_baseline(inputs, extend_len, output_fn):
    '''Read events from all baseline files and create a baseline output file
    with frequencies for events.

    The first parameter must be a list fo tuples (genome_id, filename),
    where genome_id is some kind of unique and user-friendly identifier for
    the baseline genome (e.g. 'NA19240').

    Chromosome order must be a map (string -> integer) containing all
    interesting chromosome names.
    '''
    chroms = [ 'chr' + str(x) for x in range(1, 23) ] + [ 'chrX', 'chrY' ]
    chrom_order = {}
    for i, x in enumerate(chroms):
        chrom_order[x] = i

    females = set()
    males = set()
    points = []
    for gid, fn in inputs:
        ranges = load_mei_ranges(gid, fn)
        hasY = False
        for x in ranges:
            if x.chrom not in chrom_order:
                continue
            if x.chrom == 'chrY':
                hasY = True
            points.append( (x.chrom, x.start - extend_len, -1, x) )
            points.append( (x.chrom, x.end + extend_len, 1, x) )
        if hasY:
            males.add(gid)
        else:
            females.add(gid)
    points.sort()

    print 'loaded %d genomes, %d male, %d female' % \
        (len(inputs), len(males), len(females))

    results = []
    depth = 0
    for _, coord, depth_change, x in points:
        if depth == 0:
            start = coord
            chrom = x.chrom
            gid_set = set()
        assert chrom == x.chrom
        gid_set.add(x.gid)
        depth += depth_change
        if depth == 0:
            end = coord
            if chrom == 'chrY':
                denominator = float(len(males))
            else:
                denominator = float(len(males) + len(females))
            freq = float(len(gid_set)) / denominator
            results.append( (chrom, max(0, start), end, freq) )
    assert depth == 0

    results.sort(key=lambda x: (chrom_order[x[0]], x[1]))

    f = open(output_fn, 'w')
    print >>f, '>chrom\tstart\tend\tfrequency'
    for x in results:
        print >>f, '%s\t%d\t%d\t%.3f' % x
    f.close()

def process_mei_files(dir_name, chroms, batchCount):
    expectedHeader = [ '>chromosome',
                       'begin',
                       'end',
                       'overflowCountLeft',
                       'overflowCountRight',
                       'mobileElement',
                       'strand',
                       'totalScore',
                       'leftScore',
                       'rightScore',
                       'totalDnbCount',
                       'leftDnbCount',
                       'rightDnbCount',
                       'nextBestElement',
                       'nextBestElementStrand',
                       'nextBestScore',
                       'elementBegin',
                       'elementEnd',
                       'hetToHomScore',
                       'refDnbCount',
                       'totalSupport',
                       'forwardHasMatches',
                       'backwardHasMatches',
                       ]
    result = []
    for chrom in chroms:
        highWatermark = 0
        for ii in xrange(batchCount):
            batchNum = ii+1
            fn = os.path.join(dir_name, chrom + '-repasm',
                              chrom + '-repasm-%dof%d.csv' % (batchNum,batchCount))
            if not os.path.exists(fn):
                continue
            f = open(fn)
            header = split_line(f.readline(), '\t')
            if header != expectedHeader:
                raise Exception(
                    'bad header in ' + fn + ':\n' + str(header) + '\n!=\n' + str(expectedHeader))
            for line in f:
                # Parse the input line
                data = split_line(line, '\t')
                chrom, start, end = data[0], int(data[1]), int(data[2])
                element_type, strand, insertion_score = data[5], data[6], int(data[7])
                element_type_score = insertion_score - int(data[15] or '0')
                next_best = data[13]
                element_sequence_begin = int(data[16])
                element_sequence_end = int(data[17])
                dnb_count_total, dnb_count_left, dnb_count_right = [ int(x) for x in data[10:13] ]
                dnb_count_ref = data[19] or 'N'
                total_support = float(data[20])
                forwardHasMatches = data[21] == '1'
                backwardHasMatches = data[22] == '1'

                # In the following, I attempt to faithfully follow the
                # filtering steps that previously existed in the C++
                # code (OptimizeVariations_RepeatAsm.cpp), so that I
                # get the exact same answer. Note that the
                # highWatermark filtering will filter out strong MEIs
                # if they overlap MEIs that are very weak. At some
                # point, it might be good to fix this. It happens very
                # rarely (<5x for the NA19240 test genome I ran).

                skipDuplicateEvent = not (highWatermark < start)
                highWatermark = end
                if skipDuplicateEvent:
                    continue

                if total_support < 1.0:
                    continue

                if (not forwardHasMatches) and (not backwardHasMatches):
                    # Skip events where the support consists only of very weak
                    # DNB mappings, so that we don't have any single mapping
                    # of quality greater than alpha.
                    continue

                if 0 == dnb_count_total:
                    # Skip events where the support consists only of many weak
                    # DNBs, so that no DNB supports the event by more than 3dB
                    continue

                result.append(bundle(
                    chromosome=chrom,
                    insert_range_begin=start,
                    insert_range_end=end,
                    strand=strand,
                    element_type=element_type,
                    element_type_score=element_type_score,
                    element_sequence_begin=element_sequence_begin,
                    element_sequence_end=element_sequence_end,
                    next_best_element_type=next_best,
                    insertion_score=insertion_score,
                    insertion_dnb_count=dnb_count_total,
                    insertion_left_dnb_count=dnb_count_left,
                    insertion_right_dnb_count=dnb_count_right,
                    reference_dnb_count=dnb_count_ref))
            f.close()
    return result

def find_affected_genes(gene_store, chrom, start, end):
    genes = gene_store.intersect(chrom, start, end)
    affected_genes = {}
    for g in genes:
        if g.intersects_exon(chrom, start, end):
            affected_genes[g.name] = (g.strand, 'EXON')
        elif g.name not in affected_genes:
            affected_genes[g.name] = (g.strand, 'INTRON')
    gene_text = []
    for k in sorted(affected_genes.keys()):
        strand, exon_intron = affected_genes[k]
        gene_text.append('%s:%s:%s' % (k, strand, exon_intron))
    return gene_text

def annotate_mei_events(data, gene_store, xref_store, freq_store):
    for x in data:
        gene_text = find_affected_genes(
                        gene_store,
                        x.chromosome, x.insert_range_begin, x.insert_range_end)
        xref_all = xref_store.intersect(
                        x.chromosome, x.insert_range_begin, x.insert_range_end)
        freq_all = freq_store.intersect(
                        x.chromosome, x.insert_range_begin, x.insert_range_end)
        x.x_ref = xref_all and xref_all[0].xid or ''
        x.gene_overlap = ';'.join(gene_text)
        if freq_all:
            x.frequency_in_baseline = max([f.freq for f in freq_all])
        else:
            x.frequency_in_baseline = .0

def get_roc_data(known_novel, column):
    data = sorted(known_novel, key=lambda x: -x[column])
    known = 0
    novel = 0
    known_list = []
    novel_list = []
    score_list = []
    for t in data:
        if t.x_ref:
            known += 1
        else:
            novel += 1
        known_list.append(known)
        novel_list.append(novel)
        score_list.append(t[column])
    ascore = P.array(score_list, dtype=float)
    aknown = P.array(known_list, dtype=float)
    anovel = P.array(novel_list, dtype=float)
    return ascore, anovel, aknown

def create_roc_graph(data, fn=None):
    dnbt, dnbx, dnby = get_roc_data(data, 'insertion_dnb_count')
    sct, scx, scy =  get_roc_data(data, 'insertion_score')

    if len(dnby) > 0: dnby /= dnby[-1]
    if len(scy) > 0: scy /= scy[-1]

    P.plot(scx, scy, color=GRAPH_COLOR2, label='InsertionScore', lw=2)
    P.plot(dnbx, dnby, color=GRAPH_COLOR1, label='InsertionDnbCount', lw=1)
    P.title('MEI ROC')
    P.xlabel('Count of novel (not in 1000 genomes dataset) events')
    P.ylabel('Fraction of known (in 1000 genomes dataset) events')

    sensitivity = [.5, .75, .9, .95, .98, .995]
    squeue = sensitivity[:]
    P.ylim(0, 1.12)

    for sc, x, y in zip(sct, scx, scy):
        if y > squeue[0]:
            text_offset = (30, -30)
            if y > .91:
                text_offset = (-20, 20)
            del squeue[0]
            if not squeue:
                P.xlim(0, x)
                break
            P.annotate('score: %d' % sc,
                       (x, y), xytext=text_offset, textcoords='offset points',
                       arrowprops=dict(arrowstyle="->"))
    P.legend(loc='lower right')
    P.subplots_adjust(left=0.08, right=0.98, top=0.92, bottom=0.08)
    if fn:
        P.savefig(fn)
    else:
        P.show()
    P.cla()

def create_ref_count_hist(data, fn=None):
    rc = P.array([ float(x.reference_dnb_count)
                        for x in data
                        if x.reference_dnb_count != 'N' and x.x_ref ])
    P.hist(rc, bins=[x for x in range(0, 170, 1)], normed=True, fc=GRAPH_COLOR1)
    P.title('MEI reference DNB count distribution\n(based on events also found in 1000 genomes dataset)')
    P.xlabel('Count of DNBs supporting reference')
    P.ylabel('Fraction of events')
    P.subplots_adjust(left=0.1, right=0.98, top=0.9, bottom=0.08)
    if fn:
        P.savefig(fn)
    else:
        P.show()
    P.cla()

def create_roc_lookup(data):
    '''Returns dictionaries score->novel-count and score->sensitivity'''
    score, novel, known =  get_roc_data(data, 'insertion_score')
    if len(known) > 0 and known[-1] > 0: known /= known[-1]

    min_sensitivity = {}
    max_novel_count = {}
    for sc, x, y in zip(score, novel, known):
        t = int(sc)
        if t in min_sensitivity:
            min_sensitivity[t] = min(min_sensitivity[t], y)
            max_novel_count[t] = max(max_novel_count[t], x)
        else:
            min_sensitivity[t] = y
            max_novel_count[t] = x
    return max_novel_count, min_sensitivity

def create_format_lines(columns):
    headers = [''.join([x.capitalize() for x in col.split('_')]) for col, _ in columns]
    formatters = [ '%(' + x + ')' + fmt for x, fmt in columns ]
    return '>' + '\t'.join(headers), '\t'.join(formatters)

def store_data(data, fn=None):
    max_novel_count, min_sensitivity = create_roc_lookup(data)
    column_order = [
        ('chromosome', 's'),
        ('insert_range_begin', 'd'),
        ('insert_range_end', 'd'),
        ('strand', 's'),
        ('element_type', 's'),
        ('element_type_score', 'd'),
        ('element_sequence_begin', 'd'),
        ('element_sequence_end', 'd'),
        ('next_best_element_type', 's'),
        ('insertion_score', 'd'),
        ('insertion_dnb_count', 'd'),
        ('insertion_left_dnb_count', 'd'),
        ('insertion_right_dnb_count', 'd'),
        ('reference_dnb_count', 's'),
        ('gene_overlap', 's'),
        ('x_ref', 's'),
        ('frequency_in_baseline', '.3f'),
        ('novel_event_count_for_insertion_score', '.0f'),
        ('known_event_sensitivity_for_insertion_score', '.3f'),
    ]
    header, format = create_format_lines(column_order)
    if fn:
        f = open(fn, 'w')
    else:
        f = sys.stdout
    print >>f, header
    for x in data:
        score = x.insertion_score
        x.novel_event_count_for_insertion_score = max_novel_count[score]
        x.known_event_sensitivity_for_insertion_score = min_sensitivity[score]
        print >>f, format % x
    if fn:
        f.close()

def write_summary_stats(data, fn):
    known = at_95 = known_at_95 = 0
    total = len(data)
    for x in data:
        if x.x_ref:
            known += 1
        if x.known_event_sensitivity_for_insertion_score <= .95:
            at_95 += 1
            if x.x_ref:
                known_at_95 += 1
    f = open(fn, 'w')
    print >>f, 'dict,mei'
    print >>f, 'totalEventCount,%d' % total
    print >>f, 'knownEventCount,%d' % known
    print >>f, 'eventCountAtSensitivity95,%d' % at_95
    print >>f, 'knownEventCountAtSensitivity95,%d' % known_at_95
    f.close()

def make_out_fn(opts, base_name):
    if opts.asm_id:
        n, e = base_name.split('.')
        name = n + '-' + opts.asm_id + '.' + e
    else:
        name = base_name
    return os.path.join(opts.pipeline_base_dir, name)

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.add_option('--annotation-output',
                      help='output file name for VCF conversion and baseline build')
    parser.add_option('--vcf-file-name',
                      help='convert 1000 genome MEI data from VCF to BED')
    parser.add_option('--baseline-input-ids',
                      help='comma-separated list of genome IDs to use as baseline')
    parser.add_option('--baseline-path-template',
                      help='template to create the path to the MEI files to be used '+
                           'as baseline; MEI filename for each genome is assumed to be '+
                           'template % genome-id')
    parser.add_option('--pipeline-base-dir',
                      help='repeatins directory that contains chrN-repasm.csv files; the '+
                           'output files will also be created here')
    parser.add_option('--chromosome-list',
                      help='comma-separated list of chromosomes to include in the output file')
    parser.add_option('--batch-count', type=int, default=None,
                      help='batch count for RepAsm step')
    parser.add_option('--gene-annotations',
                      help='gene annotation file')
    parser.add_option('--xref-annotations',
                      help='file name for 1000 genomes MEI data in BED format')
    parser.add_option('--frequency-annotations',
                      help='frequency in the baseline genome set data file')
    parser.add_option('--asm-id',
                      help='assembly ID to embed in the output file names')
    parser.add_option('--self-test', action='store_true',
                      help='run basic self test with empty data; doesn\'t test much')
    (opts, args) = parser.parse_args()
    if args:
        parser.error('unexpected arguments')

    if opts.vcf_file_name:
        convert_1k_vcf_file(opts.vcf_file_name, opts.annotation_output)
    elif opts.baseline_input_ids:
        genome_ids = opts.baseline_input_ids.split(',')
        genomes = [ (x, opts.baseline_path_template % x) for x in genome_ids ]
        create_mei_baseline(genomes, DEFAULT_EXTEND_LEN, opts.annotation_output)
    elif opts.self_test:
        gene_store = xref_store = freq_store = AnnotationStore()
        data = []
        annotate_mei_events(data, gene_store, xref_store, freq_store)
        store_data(data)
        create_ref_count_hist(data)
        create_roc_graph(data)
    elif opts.pipeline_base_dir:
        gene_store = read_gene_file(opts.gene_annotations)
        xref_store = read_1k_insertions_bed(opts.xref_annotations, DEFAULT_EXTEND_LEN)
        freq_store = read_mei_baseline_freqs(opts.frequency_annotations, DEFAULT_EXTEND_LEN)
        chroms = opts.chromosome_list.split(',')
        data = process_mei_files(opts.pipeline_base_dir, chroms, opts.batch_count)
        annotate_mei_events(data, gene_store, xref_store, freq_store)
        out_fn = make_out_fn(opts, 'mobileElementInsertionsBeta.tsv')
        store_data(data, out_fn)
        rc_out_fn = make_out_fn(opts, 'mobileElementInsertionsRefCountsBeta.png')
        create_ref_count_hist(data, rc_out_fn)
        roc_out_fn = make_out_fn(opts, 'mobileElementInsertionsROCBeta.png')
        create_roc_graph(data, roc_out_fn)
        write_summary_stats(data, os.path.join(opts.pipeline_base_dir, 'mei-summary.csv'))
    else:
        parser.error('cannot determine which function to run')

if __name__ == '__main__':
    main()
