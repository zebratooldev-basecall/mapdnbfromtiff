#!/usr/bin/python -u

# Driver for the dnbQuality program
import os
import sys
from subprocess import call


def findOutputDir(argv):
    # scan input args for the -o flag and return its
    # value.  Raise an exception is none found.

    foundIt = False
    outDir = '.'
    for arg in argv:
        if (arg == '-o'):
            foundIt = True
        elif foundIt:
            outDir = arg
            break
    return outDir

    

def main(argv):
    print 'Running dnbQuality...'
    call(["dnbQuality"] + argv)

    outputDir = findOutputDir(argv)
    if (outputDir == ''):
        print >>sys.stderr, "output dir not specified - use -o option"
        sys.exit(2)
        
    resultFile = 'dnbQuality.htm'
    templateFile = os.environ['CGI_HOME'] + '/etc/dnbQuality.tmpl'
    
    # this is a bit of a hack, but lets the templates reference
    # files without knowning "where" they are located.
    os.chdir(outputDir)
    print 'Generating HTML and Image files...'
    call(["instantiateTemplate.py", '-o', resultFile, templateFile])

if __name__ == "__main__":
    main(sys.argv[1:])
