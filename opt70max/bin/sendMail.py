#!/usr/bin/python
#coding: utf8
import os, sys
import smtplib
import email.MIMEMultipart
import email.MIMEText
import email.MIMEBase

############################# Class Define ##########################
class SendMail(object):
    ''' This class is used for send mail '''

    def sendMail(self):
        server = smtplib.SMTP("localhost")
        #server.login("username","password") #仅smtp服务器需要验证时

        # 构造MIMEMultipart对象做为根容器
        main_msg = email.MIMEMultipart.MIMEMultipart()

        # 构造MIMEText对象做为邮件显示内容并附加到根容器
        #text_msg = email.MIMEText.MIMEText("this is a test text to text mime")
        text_msg = email.MIMEText.MIMEText("<html><b><p>%s</p></b></html>" % self.content, 'html', 'utf-8')
        main_msg.attach(text_msg)

        # 构造MIMEBase对象做为文件附件内容并附加到根容器
        if self.attachment is not None:
            contype = 'application/octet-stream'
            maintype, subtype = contype.split('/', 1)

            ## 读入文件内容并格式化
            data = open(self.attachment, 'rb')
            file_msg = email.MIMEBase.MIMEBase(maintype, subtype)
            file_msg.set_payload(data.read( ))
            data.close( )
            email.Encoders.encode_base64(file_msg)

            ## 设置附件头
            basename = os.path.basename(self.attachment)
            file_msg.add_header('Content-Disposition',
                     'attachment', filename = basename)
            main_msg.attach(file_msg)

        # 设置根容器属性
        main_msg['From'] = self.from_addr
        main_msg['To'] = self.to_addr
        main_msg['Subject'] = self.subject
        main_msg['Date'] = email.Utils.formatdate( )

        # 得到格式化后的完整文本
        fullText = main_msg.as_string( )

        # 用smtp发送邮件
        try:
            server.sendmail(self.from_addr, self.to_addr, fullText)
        finally:
            server.quit()

    def __init__(self, from_addr, to_addr, subject, content, attachment):
        self.from_addr = from_addr
        self.to_addr = to_addr
        self.subject= subject
        self.content = content
        self.attachment = attachment

def parse_arguments(args):
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('--from-addr',help='the mail from who',default=None,dest='from_addr')
    parser.add_option('--to-addr',help='the mail to who',default=None,dest='to_addr')
    parser.add_option('--subject',help="the mail's subject",default='Test',dest='subject')
    parser.add_option('--content',help="the mail's content",default='Test',dest='content')
    parser.add_option('--attachment',help="the mail's attament",default=None,dest='attachment')

    options, args = parser.parse_args(args)
    if options.from_addr is None or options.to_addr is None:
        print "You need point out the email address."
        sys.exit(-1)

    return options.from_addr, options.to_addr, options.subject, options.content, options.attachment

def main(argv):
    from_addr, to_addr, subject, content, attachment = parse_arguments(argv)
    #print from_addr, to_addr, subject, content, attachment
    sendmail = SendMail(from_addr, to_addr, subject, content, attachment)
    sendmail.sendMail()

if __name__ == '__main__':
    main(sys.argv)
