#!/usr/bin/env python
import numpy
from csvparser import Data
from copy import deepcopy

##----------------------------------------------------------------------------
## General utility functions
##----------------------------------------------------------------------------

def system(cmd):
    print '-'*70
    print 'executing:'
    print cmd
    print '-'*70
    sys.stdout.flush()
    sys.stderr.flush()
    sts = os.system(cmd)
    if sts != 0:
        raise RuntimeError('command execution failed with status: %d' % sts)

def pct(x):
    return "%.02f%%" % (100*x)

def pct_precise(x,digits):
    fmtString = "%." + str(digits) + "f%%"
    return fmtString % (100*x)

def gb(x):
    gbVal = 1e9
    if x < (gbVal / 100):
        format = "%.04f GB"
    else:
        format = "%.02f GB"
    return format % (float(x)/gbVal)

def tsep(x):
    s = "%d" % int(x)
    t = []
    while len(s) > 3:
        t.append(s[-3:])
        s = s[:-3]
    t.append(s)
    t.reverse()
    return ','.join(t)

def floatDiv(num, denom, div0Val = 99999.0, div0_0Val = 1.0):
    """
    Safe for 0/0 division.
    """
    num = float(num)
    denom = float(denom)
    if denom == 0.0:
        if num == 0.0:
            return div0_0Val
        else:
            return div0Val
    return num / denom

def combineDictData(children, attrName):
    """Create an object with numpy arrays that combine values from children objects"""

    # hack to allow missing data
    children = [ x for x in children if hasattr(x, attrName) ]

    size = len(children)
    if size == 0:
        return Data()

    keys = [ii for ii in getattr(children[0], attrName).__dict__.keys() if ii != 'tupleList']

    r = Data()
    for k in keys:
        r.__dict__[k] = numpy.zeros(size, dtype=float)
    for ii, x in zip(range(len(children)), children):
        for k, v in getattr(x, attrName).__dict__.items():
            if k in r.__dict__:
                r.__dict__[k][ii] = v
    for k in keys:
        r.__dict__[k+'Sum'] = numpy.sum(r.__dict__[k])
    return r

def setDef(o, prop, value):
    if not hasattr(o, prop) or not getattr(o, prop):
        setattr(o, prop, value)

def getTableValue(data, name, index):
    """
    Finds and returns a value from a list by its first field value.
    Throws NameError if name is not found or index is out of bounds.
    """
    for x in data:
        if x[0] == name and index < len(x):
            return x[index]
    raise NameError

def getTableInt(data, name, index):
    return int(getTableValue(data, name, index))

def getTableFloat(data, name, index):
    return float(getTableValue(data, name, index))

def findByName(lst, name):
    r = [ x for x in lst if x.name == name ]
    assert len(r) < 2
    if len(r) == 0:
        raise RuntimeError('object not found: ' + name)
    elif len(r) == 1:
        return r[0]
    else:
        print [x.name for x in lst]
        assert False

def getArrayValueOr0(data, name, index):
    """
    Finds and returns a value form an array object.
    Returns 0 if name is not found or index is out of bounds.
    """
    try:
        val = data.__dict__[name][index]
    except:
        val = 0
    return val

def getDictValueOr0(data, name):
    """
    Finds and returns a value form a dict object.
    Returns 0 if name is not found or index is out of bounds.
    """
    try:
        val = data[name]
    except:
        val = 0
    return val


##----------------------------------------------------------------------------
## CGI-specific functions
##----------------------------------------------------------------------------

def listGaps(lanes):
    """Returns the list of (gapNo, gapName) pairs for the small gaps
       present for the given lane, and 'mateGap' string if the mate
       gap is present and None otherwise"""
    gs = set()
    mateGap = None
    for x in lanes:
        gs.update([g for g in x.__dict__.keys() if g.startswith('gap')])
        if hasattr(x, 'mateGap'):
            mateGap = 'mateGap'

    return sorted([ (int(x[3:]), x) for x in gs ]), mateGap

def prepareGapData(lanes, gapName):
    lanes = [ x for x in lanes if hasattr(x, gapName) ]
    assert len(lanes) > 0

    d = None
    for lane in lanes:
        ld = lane.__dict__[gapName]
        count = deepcopy(ld.count)
        total = sum(count)
        freq = count / total
        if d is None:
            d = Data()
            d.x = deepcopy(ld.value)
            d.total = count
            d.minFreq = freq
            d.maxFreq = freq
        else:
            assert (d.x == ld.value).all()
            d.total += count
            d.minFreq = numpy.minimum(d.minFreq, freq)
            d.maxFreq = numpy.maximum(d.maxFreq, freq)

    # trim the range to non-zero values only
    nzpositions = numpy.squeeze(numpy.indices(d.total.shape))[d.total!=0]

    if not nzpositions.any():
        return None

    nzmin, nzmax = ( min(nzpositions), max(nzpositions)+1 )
    d.x = d.x[nzmin:nzmax]
    d.total = d.total[nzmin:nzmax]
    d.minFreq = d.minFreq[nzmin:nzmax]
    d.maxFreq = d.maxFreq[nzmin:nzmax]

    d.freq = d.total / sum(d.total)

    # error array has two elements, each fo the same length as
    # the main data; the for x[i] is from y[i] - e[0][i] to y[i] + e[1][i]
    error = numpy.empty((2, len(d.x)), dtype=float)
    error[0] = d.freq - d.minFreq
    error[1] = d.maxFreq - d.freq
    d.error = error
    return d

def prepareDiscData(lanes):
    d = None
    for lane in lanes:
        try:
            ld = lane.discordance
            if d is None:
                d = Data()
                d.score = deepcopy(ld.score)
                d.total = deepcopy(ld.totalBaseCount)
                d.disc = deepcopy(ld.discordantBaseCount)
                d.ratio = d.disc / d.total
                d.minRatio = d.ratio[:]
                d.maxRatio = d.ratio[:]
            else:
                d.total += ld.totalBaseCount
                d.disc += ld.discordantBaseCount
                d.ratio += ld.discordantBaseCount / ld.totalBaseCount
                d.minRatio = numpy.minimum(d.minRatio, d.ratio)
                d.maxRatio = numpy.maximum(d.maxRatio, d.ratio)
        except AttributeError:
            pass
    if d is not None:
        d.discordantBaseCount = 0
        d.totalBaseCount = 0
        for score in d.score:
            d.discordantBaseCount += d.disc[score]
            d.totalBaseCount += d.total[score]
    return d

def combineAdfReportData(adfLaneMetrics):
    totalADFedDNBs = 0
    totalMirages = 0
    for adfRptLine in adfLaneMetrics:
        try:
            totalADFedDNBs += int(adfRptLine[1])
            totalMirages += int(adfRptLine[2])
        except:
            if totalADFedDNBs > 0:
                raise "invalid line in adfrpt::adfLaneMetrics: "+adfRptLine
    return (totalADFedDNBs,totalMirages)
