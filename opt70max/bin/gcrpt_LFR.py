#! /usr/bin/env python

import sys
import os
from os.path import basename
from glob import glob
# This code is for LFR
genomeBasesGcWin500 = [ 1994, 2275, 3383, 3559, 3447,
                        3932, 4998, 4909, 5360, 6668,
                        7444, 7402, 7106, 8507, 9147,
                        9767, 9626, 9996, 11648, 11808,
                        11483, 11835, 11658, 12013, 12391,
                        12365, 12540, 13198, 11876, 12071,
                        12598, 11947, 12142, 12096, 12487,
                        13353, 13544, 13700, 13820, 13321,
                        13357, 13762, 13967, 14656, 15979,
                        16923, 15697, 15304, 15468, 15943,
                        16410, 17168, 17470, 17231, 16809,
                        16691, 16959, 17543, 18117, 18179,
                        18119, 18278, 18305, 18627, 19717,
                        19766, 20352, 20428, 20993, 22143,
                        22349, 22838, 22709, 23318, 23430,
                        24119, 25818, 26170, 26016, 26609,
                        28044, 28734, 29502, 30277, 31548,
                        33344, 35101, 35277, 37110, 38131,
                        39682, 40736, 43328, 45477, 47462,
                        50667, 54705, 59265, 63387, 66798,
                        73285, 81582, 91523, 101881, 114590,
                        129684, 147415, 167988, 192487, 219781,
                        252546, 292148, 337313, 389558, 445633,
                        515344, 596295, 687010, 786441, 902583,
                        1032748, 1176837, 1334047, 1502901, 1696090,
                        1908582, 2138609, 2389329, 2665179, 2963844,
                        3287225, 3627610, 3982165, 4377264, 4788828,
                        5228447, 5664356, 6144331, 6631620, 7131439,
                        7658019, 8194036, 8740410, 9304526, 9870216,
                        10458911, 11042667, 11642694, 12242218, 12842356,
                        13446675, 14030493, 14619321, 15213997, 15807703,
                        16366145, 16940548, 17496952, 18063673, 18612541,
                        19156453, 19707882, 20233488, 20735324, 21240462,
                        21698356, 22148923, 22599705, 23031485, 23453040,
                        23861917, 24276683, 24673462, 25046998, 25404424,
                        25762287, 26085715, 26415612, 26721588, 27005899,
                        27261565, 27494293, 27723991, 27939386, 28128815,
                        28288343, 28418316, 28510700, 28599143, 28724065,
                        28837064, 28983927, 29093819, 29167656, 29223999,
                        29268317, 29250170, 29156506, 28990655, 28709484,
                        28383731, 28007418, 27602674, 27222509, 26903210,
                        26638342, 26436333, 26267781, 26129694, 25998129,
                        25860592, 25721914, 25576939, 25421689, 25241052,
                        25041016, 24815499, 24574618, 24299402, 24019162,
                        23710684, 23412021, 23105334, 22783809, 22461357,
                        22137163, 21797357, 21431843, 21071730, 20705894,
                        20316209, 19929983, 19548364, 19152637, 18756778,
                        18345982, 17947187, 17572288, 17152948, 16759338,
                        16378760, 15987108, 15589831, 15216871, 14839188,
                        14458047, 14097939, 13733890, 13370112, 13005445,
                        12618844, 12249502, 11883375, 11512948, 11153765,
                        10808237, 10444116, 10089410, 9759081, 9431451,
                        9101389, 8776308, 8460875, 8150827, 7851400,
                        7575886, 7297013, 7030686, 6777746, 6532543,
                        6296690, 6077642, 5867945, 5667512, 5468339,
                        5279245, 5093899, 4920038, 4748428, 4584475,
                        4435063, 4281679, 4137389, 3994396, 3853557,
                        3719421, 3607225, 3492399, 3384957, 3281961,
                        3180989, 3090736, 3000254, 2914465, 2829994,
                        2748422, 2674182, 2599853, 2526352, 2450934,
                        2380654, 2313070, 2242080, 2169265, 2096182,
                        2030551, 1974084, 1913380, 1855371, 1802734,
                        1756090, 1705715, 1654715, 1599710, 1548260,
                        1501664, 1453005, 1407471, 1360784, 1318783,
                        1277709, 1229728, 1185982, 1142238, 1095092,
                        1051634, 1006359, 965015, 929967, 891620,
                        857252, 821455, 784357, 748677, 717688,
                        684193, 650944, 623505, 594172, 565888,
                        539512, 513777, 488030, 465782, 445877,
                        428412, 412428, 398736, 382812, 371805,
                        360429, 348166, 333925, 319537, 306888,
                        295462, 286611, 277624, 269808, 260671,
                        253282, 245683, 238770, 232363, 224146,
                        214543, 205429, 199240, 191736, 187364,
                        180080, 175060, 169773, 164645, 160881,
                        157093, 151535, 147065, 142931, 138231,
                        134449, 130624, 125862, 121596, 118236,
                        113149, 109238, 104637, 100089, 95543,
                        92333, 88468, 84985, 80672, 77943,
                        76844, 74084, 70678, 69734, 69316,
                        66626, 63651, 61182, 58670, 54918,
                        51864, 48760, 43878, 38945, 34895,
                        31203, 27244, 23948, 20778, 19095,
                        17591, 15396, 13756, 12584, 11000,
                        9603, 8468, 6502, 6440, 5638,
                        4964, 4240, 3720, 3310, 2843,
                        2290, 1960, 1442, 1166, 1087,
                        906, 701, 635, 393, 326,
                        334, 305, 374, 283, 265,
                        223, 192, 260, 170, 157,
                        151, 93, 79, 62, 63,
                        81, 63, 107, 53, 49,
                        49, 41, 55, 115, 108,
                        99, 39, 30, 21, 73,
                        50, 47, 13, 47, 16,
                        25, 26, 9, 5, 37,
                        19, 7, 17, 6, 0,
                        0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0,
                        0,
                        ]

exomeBasesGcWin500 = [ 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 1,
                       22, 18, 29, 21, 69,
                       94, 65, 62, 70, 121,
                       185, 215, 218, 369, 373,
                       328, 285, 298, 332, 462,
                       625, 749, 710, 918, 1100,
                       1250, 1574, 2201, 2530, 2853,
                       3518, 4065, 4830, 5853, 6856,
                       8013, 9042, 10593, 12762, 14447,
                       15973, 18068, 21040, 24005, 26915,
                       29793, 33713, 36988, 41680, 45528,
                       50120, 54001, 58438, 62861, 66964,
                       71991, 76304, 82117, 88371, 93889,
                       99451, 103921, 109401, 115887, 120956,
                       124549, 129403, 134271, 139160, 143303,
                       146594, 150751, 154939, 157458, 159386,
                       163467, 166867, 169151, 170870, 172557,
                       174638, 176566, 178724, 181471, 184532,
                       186033, 186819, 189817, 190195, 189696,
                       191353, 192555, 193992, 193365, 195028,
                       195961, 197698, 198464, 199438, 198680,
                       198536, 198443, 198120, 196397, 196732,
                       196030, 194160, 194614, 193323, 192351,
                       192364, 189879, 187682, 188241, 186289,
                       183806, 182370, 181987, 181330, 179205,
                       179864, 178301, 175871, 175603, 175599,
                       176691, 175560, 172638, 170085, 168749,
                       169041, 167660, 166922, 166886, 166706,
                       164712, 163188, 163195, 161801, 161642,
                       161804, 161861, 162207, 160171, 158512,
                       155803, 155131, 155248, 154588, 154747,
                       156909, 157258, 159039, 159530, 158488,
                       158423, 157262, 156397, 154985, 152379,
                       153348, 154860, 153835, 152975, 152629,
                       153060, 153864, 153892, 153647, 155108,
                       156218, 156471, 157048, 158839, 159576,
                       161859, 162732, 162534, 162788, 162467,
                       164745, 164850, 164306, 165120, 165647,
                       168205, 170200, 172743, 173228, 174322,
                       173657, 174360, 174260, 175735, 178466,
                       178878, 179832, 181158, 181497, 182373,
                       182004, 182051, 182299, 182246, 182946,
                       184753, 184283, 182875, 182060, 182020,
                       179913, 179177, 177419, 176069, 174230,
                       172952, 171534, 170659, 170700, 169140,
                       166565, 162321, 158248, 156077, 154487,
                       152683, 148079, 145074, 142826, 139694,
                       136340, 131187, 127960, 124840, 122108,
                       119802, 117028, 113874, 111074, 108723,
                       105504, 103234, 99800, 96300, 93053,
                       89514, 86224, 82563, 78307, 74983,
                       72681, 71248, 69369, 66333, 63800,
                       61996, 59326, 56347, 54376, 51615,
                       51775, 50297, 47381, 46018, 44463,
                       43685, 42292, 41287, 39972, 38192,
                       37119, 36007, 35433, 34772, 34673,
                       32756, 32329, 31295, 30213, 30242,
                       29629, 28051, 26595, 25364, 24738,
                       23575, 22854, 22476, 21831, 20999,
                       20431, 19255, 18343, 17172, 16435,
                       15655, 14712, 13988, 12941, 11772,
                       10765, 10288, 9285, 8594, 7930,
                       7284, 6618, 6394, 5740, 5195,
                       4566, 4122, 3575, 3251, 2927,
                       2936, 2328, 2051, 1669, 1401,
                       1342, 1391, 1281, 998, 762,
                       636, 519, 369, 350, 339,
                       316, 277, 220, 161, 134,
                       112, 96, 147, 118, 142,
                       126, 135, 127, 44, 49,
                       34, 52, 63, 26, 13,
                       21, 39, 33, 8, 8,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0,
                       0,
                       ]

# countBaselin is from an old data, no longer used
countsBaseline = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 3, 9, 32, 182, 830, 2644, 7827, 19078,
                   43948, 82347, 139927, 216561, 300853, 400765, 483762,
                   571746, 629424, 674799, 709033, 722056, 719210, 705862,
                   673543, 670991, 628732, 592881, 554230, 510364, 459324,
                   404730, 352063, 302878, 245533, 228130, 182607, 156714,
                   133320, 113381, 97372, 83725, 71763, 60618, 50607, 42798,
                   34397, 27465, 21509, 16249, 12033, 8557, 5899, 4008, 2680,
                   2026, 1500, 1144, 895, 606, 407, 274, 131, 84, 38, 8, 1,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

countsSim = [11,37,78,129,191,299,405,614,745,924,1276,1533,1935,2290,3017,
            3998,5446,8344,13819,25622,55316,111827,222778,414485,701602,1216185,
            1791739,2505816,3336075,4099547,5016642,5668942,6339608,6796128,7124275,7371722,
            7475259,7413630,7321618,6950547,7076142,6611647,6340628,5985600,5662848,5328748,
            4919697,4467876,4070953,3384375,3409102,2746249,2385110,2029399,1736622,1503982,
            1304991,1132806,996131,851063,786450,665548,572855,480938,399701,325487,257210,
            201741,157558,123000,103485,85571,74359,64731,54774,51063,42248,35603,28943,
            22429,17780,12113,8133,5224,3453,1756,1011,418,186,84,34,9,3,4,3,1,0,0,1,0,0]

def readCounts(fn):
    ff = open(fn)
    gcpct = False
    counts = []
    for line in ff:
        if line.startswith('gcpercent,count'):
            gcpct = True
            continue
        if not gcpct:
            continue
        fields = line.split(',')
        if len(fields) != 2:
            continue
        counts.append(int(fields[1]))
    ff.close()
    if len(counts) != 101:
        raise 'counts len bad: %d' % len(counts) 
    return counts

def computeFactor(valLowCount, dnbCount, ce):
    predicted = ce[0] + ce[1]*valLowCount + ce[2]*(valLowCount * valLowCount) + ce[3]*dnbCount
    return predicted/valLowCount

gcToGenome = []
gcToExome = []
for ii in xrange(0, 101):
    gcToGenome.append(sum(genomeBasesGcWin500[0:ii*5+1]) / float(sum(genomeBasesGcWin500)))
    gcToExome .append(sum( exomeBasesGcWin500[0:ii*5+1]) / float(sum( exomeBasesGcWin500)))

laneToLib = {}
libToCounts = {}

## added to get CLS information from xml file
def getCLS(xml = ''):
    fh = open(xml)
    for ln in fh:
        ln = ln.strip()
        if "CLS" in ln:
            cls = ln.split("=")[-1].strip(">").strip("\"")
            break
	else:
	    cls = ""
    return cls

for workdir in sys.argv[1:]:
    libdirs = glob('%s/MAP/GS*'%(workdir,))

    if len(libdirs) < 1:
        continue
    libs = [ basename(lib) for lib in libdirs ]
    
    # lane and lib are same thing
    # hack to get it to run

    if len(libs) == 1:
        lane = libs[0]
    else:
        sys.exit('More than one lane found.  Is this contam screen?')
    
    laneToLib[lane] = []
    for lib in libs:
        fn = glob('%s/MAP/%s/reports/MappingStats.csv' % (workdir,lib))
        xml = glob('%s/collection.xml'%(workdir,))

        ## changes to record CLS instead of slide_lane information again
	if len(xml)!=0: lib = getCLS(xml[0])
 
        if len(fn) != 1:
            continue
        print ">>>", lib
        laneToLib[lane].append(lib)

        libToCounts[(lane,lib)] = readCounts(fn[0])

#lanes = [ '00SIM', '01GS199-ASM'] + sorted(laneToLib.keys())
lanes = [ '00SIM'] + sorted(laneToLib.keys())
libToCounts[('00SIM','SIM')] = countsSim
laneToLib['00SIM'] = [ 'SIM' ]
#libToCounts[('01GS199-ASM','GS00433-CLS')] = countsBaseline
#laneToLib['01GS199-ASM'] = [ 'GS00433-CLS' ]

out = open('%s/gcrpt.csv' % workdir, 'w') 
# The first line of gcrpt.csv
vals = []
vals.append('PctGc')
vals.append('GenomeFraction')
vals.append('ExomeFraction')
for lane in lanes:
    for lib in laneToLib[lane]:
        if lane.startswith('0'):
            vals.append(lane[2:])
        else:
            vals.append(lane)
    
out.write(','.join(map(str, vals))+'\n')

# The second line of the gcrpt.csv
vals = []
vals.append('PctGc')
vals.append('GenomeFraction')
vals.append('ExomeFraction')
for lane in lanes:
    for lib in laneToLib[lane]:
        vals.append(lib)
out.write(','.join(map(str, vals))+'\n')

simCounts = libToCounts[('00SIM','SIM')]

lowGenomeCount_L = {}
lowGenomeCount_R = {}
lowExomeCount_L = {}
lowExomeCount_R = {}

for ii in xrange(0, 101):
    vals = []
    vals.append(ii)
    vals.append(gcToGenome[ii])
    vals.append(gcToExome[ii])
    for lane in lanes:
        for lib in laneToLib[lane]:
            if (lane,lib) not in lowGenomeCount_L:
                lowGenomeCount_L[(lane,lib)] = 0.0
                lowGenomeCount_R[(lane,lib)] = 0.0
                lowExomeCount_L[(lane,lib)] = 0.0
                lowExomeCount_R[(lane,lib)] = 0.0
            counts = libToCounts[(lane,lib)]
            if 0 == simCounts[ii]:
                vals.append(1)
            else:
                val = counts[ii]/float(sum(counts)) / (simCounts[ii]/float(sum(simCounts)))
                vals.append( val )
                if val < .6:
                    if 0 == ii:
                        lowGenomeCount_L[(lane,lib)] += gcToGenome[ii]
                        lowExomeCount_L[(lane,lib)] += gcToExome[ii]
                    else:
                        if ii <= 40:    # Divider is 40 for genome
                            lowGenomeCount_L[(lane,lib)] += gcToGenome[ii] - gcToGenome[ii-1]
                        else:
                            lowGenomeCount_R[(lane,lib)] += gcToGenome[ii] - gcToGenome[ii-1]
                        if ii <= 49:    # 49 for exome
                            lowExomeCount_L[(lane,lib)] += gcToExome[ii] - gcToExome[ii-1]
                        else:
                            lowExomeCount_R[(lane,lib)] += gcToExome[ii] - gcToExome[ii-1]
    out.write(','.join(map(str, vals))+'\n')
out.close()

# Computation for GC roll up report
# Calculte the factor using a model
valGenome_Original = 100.0*(lowGenomeCount_L[(lane,lib)] + lowGenomeCount_R[(lane,lib)])
valExome_Original = 100.0*(lowExomeCount_L[(lane,lib)] + lowExomeCount_R[(lane,lib)])
dnbCount = sum(counts)

# coefficients for regression model for NON-LFR
#ceG = [4.665E-1, -5.192E-2, 9.823E-2, -9.496E-10]
#ceE = [1.038, 1.792E-1, -1.770E-3, -6.247E-9]
# coefficients for regression model for LFR
ceG = [1.206, 7.144e-1, 8.004e-3, -4.286e-9]
ceE = [12.35, 2.257e-2, 1.431e-2, -4.215e-8]

factorG = computeFactor(valGenome_Original, dnbCount, ceG)
factorE = computeFactor(valExome_Original, dnbCount, ceE)

ff = open('%s/gcrpt-rollup.csv'%(workdir,), 'w') 
strG = 'GenomeFractionLt.6AvgCvgByGc'
strE = 'ExomeFractionLt.6AvgCvgByGc'
ff.write('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n' % ('Lane','CLS',
                                              strG,strE, strG + '_Left', strE + '_Left',
                                              strG + '_Right', strE + '_Right',
                                              strG + '_L/R', strE + '_L/R'))
for lane in lanes:
    for lib in laneToLib[lane]:
        prettylane = lane
        if lane.startswith('0'):
            prettylane = lane[2:]
        ff.write('%s,%s,%.1f%%,%.1f%%,%.1f%%,%.1f%%,%.1f%%,%.1f%%' % (
                 prettylane,lib,
                 100.0*(lowGenomeCount_L[(lane,lib)] + lowGenomeCount_R[(lane,lib)]) * factorG,
                 100.0*(lowExomeCount_L[(lane,lib)] + lowExomeCount_R[(lane,lib)]) * factorE,
                 100.0*lowGenomeCount_L[(lane,lib)] * factorG,
                 100.0*lowExomeCount_L[(lane,lib)] * factorE,
                 100.0*lowGenomeCount_R[(lane,lib)] * factorG,
                 100.0*lowExomeCount_R[(lane,lib)] * factorE
                 ))
        if lane.startswith('0'):
            ff.write(',1,1\n')
        else:
            ff.write(',%.3f,%.3f\n' % (
                     (lowGenomeCount_L[(lane,lib)] / lowGenomeCount_R[(lane,lib)]),
                     (lowExomeCount_L[(lane,lib)]  / lowExomeCount_R[(lane,lib)])
                     ))
ff.close()
