#!/usr/bin/env python
import os
import pymssql
import accessionlib, driverlib
from driverlib import Error

class LimsLane:
    pass

def getLanesFromLims():
    conn = pymssql.connect(host='ws-psqldb03:1433',
                           user='limsrptdbuser', password='L1msrptdbus3r',
                           database='LIMSRPTDB')
    cur = conn.cursor()
    query = \
"""SELECT DISTINCT sd.SlideBC,
                Lane,
                lib.CLSBC,
                RDL
FROM pub_Library lib, pub_SlideLaneDNB sd
WHERE
    lib.Circular_Library_structure_Id = sd.Circular_Library_structure_Id AND
    sd.SlideBC IN (
        SELECT DISTINCT SlideBC FROM pub_LigateSummary
    )
ORDER BY 1, 2"""

    cur.execute(query)
    rows = cur.fetchall()
    cur.close()
    conn.close()

    lanes = []
    for r in rows:
        lane = LimsLane()
        lane.name = "%s-L%02d" % (r[0], r[1])
        lane.slide = r[0]
        lane.library, lane.path = r[2], r[3]
        lanes.append(lane)
        #print lane.name, lane.library
    return lanes

def getUnixLocation(limsLane):
    p = limsLane.path.replace('\\', '/')
    if p.startswith('//hoard'):
        p = p.replace('//hoard', '/Proj')
        return os.path.join(p, 'Slides', limsLane.slide)
    else:
        raise RuntimeError('bad RDL path in LIMS: ' + limsLane.path)

def main():
    from optparse import OptionParser
    parser = OptionParser('usage: %prog [options]')
    parser.set_defaults(rootDir='/Proj/Assembly/inazarenko/FakeRoot',
                        libraries=[], output='dirlist.txt')
    parser.add_option('-r', '--root-dir', dest='rootDir',
                      help='use ROOTDIR as the base directory for the pipeline '
                           'object locations')
    parser.add_option('--library', dest='libraries', action='append',
                      help='include all objects related to LIBRARY into the list')
    parser.add_option('-o', '--output', dest='output',
                      help='name of the file to write the list of directories to')
    (options, args) = parser.parse_args()
    if len(args) != 0:
        raise RuntimeError('unexpected arguments')
    cgiHome = driverlib.getCgiHome()

    if len(options.libraries) == 0:
        raise RuntimeError('expected at least one library argument')

    print 'loading tree of assembly objects...'
    om = accessionlib.ObjectManager(options.rootDir, cgiHome)

    librarySet = set(options.libraries)
    print 'fetching list of lanes from LIMS...'
    limsLanes = getLanesFromLims()
    dirtySlides = set()
    for x in limsLanes:
        if x.library in librarySet:
            dirtySlides.add(x.slide)
    limsLanes = [ x for x in limsLanes if x.slide in dirtySlides ]
    dirtyLanes = set([x.name for x in limsLanes])

    if len(limsLanes) == 0:
        raise RuntimeError('no lanes for the specified library found in LIMS')

    laneCountByLibrary = {}
    laneCountForSpecifiedLibs = 0
    for x in limsLanes:
        if x.library in laneCountByLibrary:
            laneCountByLibrary[x.library] += 1
        else:
            laneCountByLibrary[x.library] = 1
        if x.library in librarySet:
            laneCountForSpecifiedLibs += 1

    print 'found %d related slides (%d lanes)' % (len(dirtySlides), len(dirtyLanes))
    print 'lane count per library (* marks collateral damage):'
    for x in sorted(laneCountByLibrary.keys()):
        if x in librarySet:
            flag = ' '
        else:
            flag = '*'
        print flag, x, laneCountByLibrary[x]
    if len(limsLanes) - laneCountForSpecifiedLibs >= laneCountForSpecifiedLibs:
        raise RuntimeError('too many "control" lanes on the deletion list')

    print 'searching assembly object tree...'
    dirtyObjects = []
    #import pdb; pdb.set_trace()
    for otype in ['ADF', 'FDF', 'MAP']:
        print 'looking for %s objects...' % otype
        count = 0
        idlist = sorted(om.usedIds[otype])
        for x in idlist:
            try:
                obj = om.findObject(x)
            except Error:
                continue
            if obj.lane in dirtyLanes:
                dirtyObjects.append(obj.location)
                count += 1
        print 'affected %s objects: %d' % (otype, count)
    print 'looking for GAPS objects...'
    idlist = sorted(om.usedIds['GAPS'])
    count = 0
    for x in idlist:
        try:
            obj = om.findObject(x)
        except Error:
            continue
        if obj.library in librarySet:
            dirtyObjects.append(obj.location)
            count += 1
    print 'affected GAPS objects: %d' % count

    if not dirtyObjects:
        raise RuntimeError('no related objects found under: ' + options.rootDir)

    print 'looking for ASM objects...'
    affectedAssemblies = []
    idlist = sorted(om.usedIds['ASM'])
    for x in idlist:
        try:
            asmobj = om.findObject(x)
        except Error:
            continue
        for m in asmobj.mapList:
            try:
                mobj = om.findObject(m)
            except Error:
                continue
            if mobj.lane in dirtyLanes:
                affectedAssemblies.append(asmobj)
                break
    dirtyAssemblyNames = set([x.name for x in affectedAssemblies])

    print 'looking for EXP objects...'
    affectedExps = []
    idlist = sorted(om.usedIds['EXP'])
    for x in idlist:
        try:
            obj = om.findObject(x)
        except Error:
            continue
        if obj.asm in dirtyAssemblyNames:
            affectedExps.append(obj)

    if affectedAssemblies or affectedExps:
        print 'assemblies and export objects that use these libraries:'
        for x in affectedAssemblies:
            print x.location
        for x in affectedExps:
            print x.location

    slideLocations = set([getUnixLocation(x) for x in limsLanes])

    dirs = dirtyObjects + sorted(slideLocations)
    print 'total directories in the list:', len(dirs)

    print 'checking if the directories exist...'
    count = 0
    for x in dirs:
        if (not os.path.isdir(x) and os.path.lexists(x)) or os.path.islink(x):
            raise RuntimeError('exists, but not a directory: ' + x)
        elif os.path.isdir(x):
            count += 1
    print 'count of existing directories in the list:', count

    print 'writing the list to the output file...'
    out = open(options.output, 'w')
    for x in dirs:
        print >>out, x
    print 'done.'

if __name__ == '__main__':
    main()
