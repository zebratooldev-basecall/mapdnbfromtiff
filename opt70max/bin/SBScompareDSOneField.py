#!/usr/bin/env python

import sys
import itertools
#from os.path import join as pjoin
from optparse import OptionParser
from random import sample
import numpy as np
import subprocess


def set_up_translation(cpalbases,cpalcolors,sbsbases,sbscolors):
    global cpalTranslate
    global sbsTranslate
    cpalTranslate = dict((cpalbases[n].upper(),cpalcolors[n].upper()) for n in range(len(cpalbases)))
    sbsTranslate = dict((sbscolors[n].upper(),sbsbases[n].upper()) for n in range(len(sbsbases)))

def rollup_conc_adj_by_pct(pct,rpa):
    maxindex = int( float(pct) * len(rpa) )
    values = {}
    
    # roll up data based on tuple of concRef, concPrior, concAfter
    for entry in rpa[:maxindex]:
        concPattern = entry[1]
        values[concPattern] = values.get(concPattern,0) + 1

    # calculate totals and disc/conc
    conctotal = 0; disctotal = 0
    for patterns, count in values.iteritems():
        if patterns[0] == 0:
            disctotal += count
        else:
            conctotal += count
    total = conctotal + disctotal

    return conctotal, disctotal, total, values

def write_field_data(ho,aso,pct,field,results,slidelane):
    print >> sys.stderr, "Writing out data"
    conctotal, disctotal, total, values = rollup_conc_adj_by_pct(pct,results) ##values[pattern] = count
    # write out adjacent base comparison for given dnb percent
    # parse lane, row and column numbers from field name
    lane = field[:3]; column = field[4:7]; row = field[-3:]
    slide = slidelane.split('-')[0]
    for pattern, count in values.iteritems():
        if pattern[0] == 0:
            catdenom = disctotal
        else:
            catdenom = conctotal
        catfrac = float(count) / float(catdenom)
        totalfrac = float(count) / float(total)
        aso.write('%s,%s,%s,%s,%s,%s,%s,%s,%s\n'%(slidelane,pct,pattern,catfrac,totalfrac,slide,lane,column,row))

    # write out discordance info
    if int(total) != 0:
        discfrac = float(disctotal) / float(total)
        concfrac = float(conctotal) / float(total)
    else:
        discfrac = 'inf'; concfrac = 'inf'
    
    ho.write('%s,%s,%s,%s,%s,%s,%s,%s,%s\n'%(pct,slide,lane,column,row,conctotal,disctotal,concfrac,discfrac))

def check_adjacent_bases(calls,refpos,sbspos):
    concRef = 0; concPrior = 0; concAfter = 0; adjN = 0
    
    # translate test base from cPal to SBS if necessary
    cpalcolor = cpalTranslate[calls[sbspos]]
    testbase = sbsTranslate[cpalcolor]

    if calls[refpos] == testbase:
        concRef = 1
    if calls[refpos - 1] == testbase:
        concPrior = 1
    if calls[refpos + 1] == testbase:
        concAfter = 1
    
    return concRef, concPrior, concAfter
    

def get_avg_adjacent_scores(scores, refpos):
    adjscores = [int(i) for i in scores[refpos-1:refpos+2] ]
    avgscore = sum(adjscores) / float(len(adjscores))

    return avgscore

def get_field_from_file(callsin):
    basename = callsin.split('/')[-1]
    field = basename.split('_')[1]

    # Check field name
    if field[0] != 'L' or field[3] != 'C' or field[7] != 'R':
       raise Exception('%s is not a valid field entry' % (field,)) 

    return field

def parse_calls_scores(callsin, scoresin, refpos, testpos, samplefrac):
    refpos = int(refpos); testpos = int(testpos)
    ci = open(callsin);  si = open(scoresin)
    rpa = [] ## rpa[[avgscore,(concRef, concPrior, concAfter)],[],...]
    try:
        # get file length and counts of random values for sampling
        fileLength = file_len(callsin)
        useLineCount = int(float(samplefrac) * fileLength)
        uselines = sample(xrange(fileLength),useLineCount)
        uselines = set(uselines)
        
        linecount = 0
        for calls, scores in itertools.izip(ci,si):
            linecount += 1
            scores = scores.rstrip().split(',') 
            if linecount in uselines:
                # throw out sequences with Ns
                if calls[refpos] != 'N' and calls[testpos] != 'N':
                    if calls[refpos - 1] != 'N' and calls[refpos + 1] != 'N':
                        # check adjacent bases
                        concRef, concPrior, concAfter = check_adjacent_bases(calls, refpos, testpos)
                        
                        # get scores of adjacent
                        avgscore = get_avg_adjacent_scores(scores, refpos)
                        rpa.append([avgscore,(concRef,concPrior,concAfter)])
    finally:
        ci.close()
        si.close()

    rpa.sort()
    rpa.reverse()

    return rpa

def file_len(fname):
    p = subprocess.Popen(['wc','-l',fname],stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE)
    result,err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-f', '--input-file-base', help='basename from dumpdnbs output (files should end in _scores.csv and _calls.csv)', dest='input_file')
    parser.add_option('-o', '--output-base', help='basename of output files', dest='outbase')
    parser.add_option('-r', '--reference-position', help='0-based position of reference base in dnb string', dest='refpos')
    parser.add_option('-t', '--test-position', help='0-based position of test base (ie SBS) in dnb string', dest='testpos')
    parser.add_option('-l', '--slidelane', help='slidelane', dest='slidelane')
    parser.add_option('-s', '--sampling-fraction', help='fraction of DNBs to sample', dest='samplefrac', default=0.1)
    parser.add_option('-p', '--dnb-percents', help='comma-separated list of percentage cut-offs of dnb base scores', dest='dnbpcts', default='0.8,0.9,1')
    parser.add_option('-1', '--cpal-bases', help='comma-separated list of bases for cPal cycles', dest='cpalbases')
    parser.add_option('-2', '--cpal-colors', help='comma-separated list of fluorophore colors for cPal cycles', dest='cpalcolors')
    parser.add_option('-3', '--sbs-bases', help='comma-separated list of bases for SBS cycles', dest='sbsbases')
    parser.add_option('-4', '--sbs-colors', help='comma-separated list of fluorophore colors for SBS cycles', dest='sbscolors')

    # parse command line arguments.
    options, args = parser.parse_args(arguments[1:])

    dnbpcts = options.dnbpcts.split(',')
    cpalbases = options.cpalbases.split(',')
    cpalcolors = options.cpalcolors.split(',')
    sbsbases = options.sbsbases.split(',')
    sbscolors = options.sbscolors.split(',')

    return options.input_file, options.outbase, options.refpos, options.testpos, options.samplefrac, dnbpcts, options.slidelane, cpalbases, cpalcolors, sbsbases, sbscolors

def main(arguments):
    # parse command line arguments
    print 'parsing arguments'
    filebase, outbase, refpos, testpos, samplefrac, pcts, slidelane,cpalbases, cpalcolors, sbsbases, sbscolors = parse_arguments(arguments)
    refpos = int(refpos); testpos = int(testpos)
    pcts = [float(i) for i in pcts]
    # construct dictionaries to translate cPal bases to SBS
    set_up_translation(cpalbases,cpalcolors,sbsbases,sbscolors)
    
    # get field from file name
    scoresin = filebase + '_scores.csv'
    callsin = filebase + '_calls.csv'
    field = get_field_from_file(callsin)

    # go through data by field
    print >> sys.stderr, "Processing files %s, %s"%(scoresin, callsin)
    results = parse_calls_scores(callsin, scoresin, refpos, testpos, samplefrac)

    # get overall discordance for field
    conctotal, disctotal, total, values = rollup_conc_adj_by_pct(1,results) ##values[pattern] = count
    if int(total) != 0:
        discscore = float(disctotal) / float(total)
    else:
        discscore = 'inf'

    # write out summary stats for field for each pct
    # open file for heatmap out 
    heatout = '%s_%s_conc_disc_heatmap.csv'%(outbase,field)
    # open file for adj_summary out
    adj_summ_out = '%s_%s_adjacent_summary.csv'%(outbase,field)
    try:
        ho = open(heatout,'w')
        aso = open(adj_summ_out,'w')

        aso.write('discordance_rate_for_field,%s\n'%(discscore,))
        aso.write('slidelane,pctdata,concordance_pattern,cat_fraction,total_fraction,slide,lane,column,row\n')
        ho.write('pctdata,slide,lane,column,row,concordant_dnbs,discordant_dnbs,concordant_fraction,discordant_fraction\n')

        for pct in pcts:
            write_field_data(ho,aso,pct,field,results,slidelane)

    finally:
        ho.close(); aso.close()

if __name__ == '__main__':
    main(sys.argv)
