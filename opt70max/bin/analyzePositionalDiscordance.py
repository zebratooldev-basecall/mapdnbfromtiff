#!/usr/bin/env python

import sys
import sequence
import datetime
import numpy as np
import glob

now = datetime.datetime.now()

from optparse import OptionParser
from os.path import join as pjoin
from dnbLib import *
from sequence import ChrSequence

CHROM_DIR = '/rnd/home/sgiles/data/REF'
BASES = 'ACGT'

def initializeContainers(dnbLength):
    global maxDnbLength
    global posDiscSink
    global corrDiscSink
    global corrBaseCallsSink
    global posConfMatrix
    global posRefErrorSink
    global posCallErrorSink

    maxDnbLength = dnbLength
    # initialize containers for metric counts [x occurrences in y observations].
    posDiscSink = {}
    corrDiscSink = {}
    corrBaseCallsSink = {}
    posConfMatrix = {}
    posRefErrorSink = {}
    posCallErrorSink = {}
    for i in range(dnbLength):
        posDiscSink[i+1] = [0,0]
        corrDiscSink[i+1] = {}
        corrBaseCallsSink[i+1] = {}
        posConfMatrix[i+1] = {}
        posRefErrorSink[i+1] = {}
        posCallErrorSink[i+1] = {}
        for j in range(dnbLength):
            corrDiscSink[i+1][j+1] = [0,0]
            corrBaseCallsSink[i+1][j+1] = [0,0]
        for callBase in list(BASES):
            posConfMatrix[i+1][callBase] = {}
            posRefErrorSink[i+1][callBase] = [0,0]
            posCallErrorSink[i+1][callBase] = [0,0]
            for refBase in list(BASES):
                posConfMatrix[i+1][callBase][refBase] = 0


def log(string):
    now = datetime.datetime.now()

    print('%s: %s' % (now.strftime('%Y-%m-%d %H:%M:%S'),string))

def find_discordant_bases(seq, mapping, ref, side):
    posDisc = [False] * len(seq)
    expRefBases = [''] * len(seq)

    offsets = mapping.dnbArm.dnb.dnbArchitecture.getOffsets(side, mapping.gaps)

    for i in range(len(seq)):
        expRefBases[i] = ref[offsets[i]]
        if seq[i] != 'N' and seq[i] != ref[offsets[i]]:
            posDisc[i] = True

    return posDisc, expRefBases

def find_discordances(leftArm, rightArm, chrSeq):
    # this function will use dnb sequence and corresponding reference to find discordant positions.
    bases = list(leftArm.sequence) + list(rightArm.sequence)

    # get discordances and expanded reference bases.
    #   left Arm.
    mapping = leftArm.mappings[0]

    leftArmRef = chrSeq.get_sequence(mapping.chromosome, mapping.begin, 
                                    mapping.begin + len(leftArm.sequence) + sum(mapping.gaps))
    if mapping.strand == '-':
        leftArmRef = sequence.reverse_complement(leftArmRef)
    leftArmDiscPos, leftArmExpRefBases = find_discordant_bases(leftArm.sequence,
                                                             mapping, leftArmRef, leftArm.side)

    #   right Arm.
    mapping = rightArm.mappings[0]

    rightArmRef = chrSeq.get_sequence(mapping.chromosome, mapping.begin,
                                    mapping.begin + len(rightArm.sequence) + sum(mapping.gaps))
    if mapping.strand == '-':
        rightArmRef = sequence.reverse_complement(rightArmRef)
    rightArmDiscPos, rightArmExpRefBases = find_discordant_bases(rightArm.sequence, 
                                                               mapping, rightArmRef, rightArm.side)

    discordances = leftArmDiscPos + rightArmDiscPos
    expRefBases = leftArmExpRefBases + rightArmExpRefBases
    
    return bases, expRefBases, discordances

def process_disc_positions(bases, refBases, posDisc):
    # loop through positions and increment counters when appropriate.
    for i in range(len(bases)):
        iPos = i + 1
        if bases[i] == 'N': continue
        if not posConfMatrix[1].has_key(refBases[i]): continue

        # positional discordance
        posDiscSink[iPos][1] += 1
        posRefErrorSink[iPos][refBases[i]][1] += 1
        posCallErrorSink[iPos][bases[i]][1] += 1

        if posDisc[i]: 
            posDiscSink[iPos][0] += 1
            posRefErrorSink[iPos][refBases[i]][0] += 1
            posCallErrorSink[iPos][bases[i]][0] += 1

        # correlated discordance.
        if posDisc[i]:
            for j in range(len(bases)):
                if i == j: continue
                if bases[j] == 'N': continue
                jPos = j + 1
                corrDiscSink[iPos][jPos][1] += 1
                if posDisc[j] and posDisc.count(True)>1:
                    corrDiscSink[iPos][jPos][0] += 1

                # correlated base calls when base[i] is discordant.
                corrBaseCallsSink[iPos][jPos][1] += 1
                if bases[i] == bases[j]:
                    corrBaseCallsSink[iPos][jPos][0] += 1

        # confusion matrices.
        posConfMatrix[iPos][bases[i]][refBases[i]] += 1

def write_report(fn):
    out = open(fn, 'w')

    # positional discordance.
    out.write('array,discordancePerPosition\n')
    out.write('position,totalBaseCount,discordantBaseCount\n')
    for i in range(maxDnbLength):
        out.write('%d,%d,%d\n' % (i+1, posDiscSink[i+1][1], posDiscSink[i+1][0]))
    out.write('\n')

    # reference error counts.
    out.write('array,referenceErrorCounts\n')
    out.write('position,refBase,totalBaseCount,discordantBaseCount\n')
    for i in range(maxDnbLength):
        for refBase in list(BASES):
            out.write('%d,%s,%d,%d\n' % (i+1, refBase, posRefErrorSink[i+1][refBase][1], posRefErrorSink[i+1][refBase][0]))
    out.write('\n')

    # call base error counts.
    out.write('array,callBaseErrorCounts\n')
    out.write('position,callBase,totalBaseCount,discordantBaseCount\n')
    for i in range(maxDnbLength):
        for callBase in list(BASES):
            out.write('%d,%s,%d,%d\n' % (i+1, callBase, posCallErrorSink[i+1][callBase][1], posCallErrorSink[i+1][callBase][0]))
    out.write('\n')

    # correlated discordance.
    out.write('array,correlatedDiscPerPosition\n')
    out.write('position,otherPosition,totalDiscBaseCount,correlatedDiscBaseCount\n')
    for i in range(maxDnbLength):
        for j in range(maxDnbLength):
            out.write('%d,%d,%d,%d\n' % (i+1, j+1, corrDiscSink[i+1][j+1][1], corrDiscSink[i+1][j+1][0]))
    out.write('\n')

    # correlated Base Calls for discordant positions.
    out.write('array,correlatedBaseCallsForDiscPositions\n')
    out.write('position,otherPosition,totalDiscBaseCount,correlatedBaseCalls\n')
    for i in range(maxDnbLength):
        for j in range(maxDnbLength):
            out.write('%d,%d,%d,%d\n' % (i+1, j+1, corrBaseCallsSink[i+1][j+1][1], corrBaseCallsSink[i+1][j+1][0]))
    out.write('\n')

    # confusion matrices.
    out.write('array,positionalConfustionMatrices\n')
    out.write('position,callBase,refBase,count\n')
    for i in range(maxDnbLength):
        for callBase in list(BASES):
            for refBase in list(BASES):
                out.write('%d,%s,%s,%d\n' % (i+1, callBase, refBase, posConfMatrix[i+1][callBase][refBase]))
    out.write('\n')
    out.close()

def find_map_files(mappingDir):
    mapFile = ''
    result = []
    log('searching for input files')

    for mapFile in glob.glob(pjoin(mappingDir, 'map','map-*.txt')):
        result.append(mapFile)

    if len(mapFile) == 0:
        raise Exception('No input files found! %s' % (pjoin(mappingDir, 'map','map-*.txt')))

    return result

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-i','--input-file',        help='text output file from TeraMap',               dest='input_file')
    parser.add_option('-o','--output-filename',   help='output report file name',                     dest='output_fn')
    parser.add_option('-d','--dnb-architecture',  help='string representation of dnb architecture',   dest='dnb_architecture', default='ad2_29_29')
    parser.add_option('-r','--reference-build',   help='UCSC name of the human reference build used', dest='reference_build', default='hg18')

    # parse command line arguments.
    options,args = parser.parse_args(arguments[1:])

    # check reference build compatibility.
    supportedBuilds = set(['hg18','hg19'])
    if not options.reference_build in supportedBuilds:
        raise Exception('Reference build %s not supported!' % options.reference_build)

    return options.input_file, options.output_fn, options.dnb_architecture, options.reference_build

def main(arguments):
    # parse arguments.
    inputFileName, outputFileName, dnbArchitecture, referenceBuild = parse_arguments(arguments)

    # create dnb architecture object to get dnb length.
    da = DnbArchitecture(DnbArchitectureStr = dnbArchitecture)

    # initialize module level counters.
    initializeContainers(da.leftArmLength() + da.rightArmLength())

    # load TeraMap output file.
    #   Currently the DnbLibCollection by default filters all non-unique & non-fully mapped dnbs.
    log('loading TeraMap file: %(inputFileName)s' % vars())
    dc = MappedDnbCollection(inputFileName)
    dc.loadDnbMappings(DnbArchitectureParam=dnbArchitecture, FullUniqueMappingsOnly=True)
    log('  - loaded dnb count: %d' % (len(dc.dnbs)))

    # index dnbs by chromosome.
    log('  - indexing dnbs by chromosome...')
    dc.indexByChromosome()

    # loop through chromosomes with dnb mappings.
    for chrName in dc.dnbsByChrom.keys():
        # skip chrM.
        if chrName == 'chrM': continue

        log('    - loading chromosome: %s' % chrName)
        chrFn = pjoin(CHROM_DIR, referenceBuild, chrName + '.fa')
        chrSeq = ChrSequence(chrFn)

        # loop through fully mapped unique dnbs.
        for dnb in dc.dnbsByChrom[chrName]:
            # process arms to get full dnb sequence, discordanct positions, and reference bases.
            bases, refBases, posDisc = find_discordances(dnb.leftArm, dnb.rightArm, chrSeq)

            # increment counters for positional discordance, correlated discordance, etc.
            process_disc_positions(bases, refBases, posDisc)

    # write out report.
    write_report(outputFileName)

main(sys.argv)
