#!/bin/bash

modeldatadir=$1
outputfile=$2

cd $modeldatadir

R --no-save <<EOF
d<-read.table('covmodelinputs.txt',header=TRUE)
library(splines)
library(stats)
b<-bs(d\$dnbGCpcnt,df=20,degree=1,Boundary.knots=c(0,1))
mfull<-glm(isReal~bs(dnbGCpcnt,df=20,degree=1,Boundary.knots=c(0,1))+armsGC+armsAG+leftA+leftC+leftG+leftT+leftN+rightA+rightC+rightG+rightT+rightN+leftend+rightend+leftAcuI+rightAcuI+leftEcoP15+rightEcoP15+p1base+p2base+p3base+p4base+p5base+p6base+p7base+p8base+p9base+p10base+p11base+p12base+p13base+p14base+p15base+p16base+p17base+p18base+p19base+p20base+p21base+p22base+p23base+p24base+p25base+p26base+p27base+p28base+p29base+p30base+p31base+p32base+p33base+p34base+p35base+p36base+p37base+p38base+p39base+p40base+p41base+p42base+p43base+p44base+p45base+p46base+p47base+p48base+p49base+p50base+p51base+p52base+p53base+p54base+p55base+p56base+p57base+p58base+p59base+p60base+p61base+p62base+p63base+p64base+p65base+p66base+p67base+p68base+p69base+p70base,data=d,family=binomial)
coef<-mfull\$coefficients
k<-attr(b,"knots")
bk<-attr(b,"Boundary.knots")
knottext<-cbind('knots',sort(c(k,bk)))
write(t(knottext),file='covmodel.coeffs',ncolumns=2,sep=',')
write(t(cbind(names(coef),coef)),file='covmodel.coeffs',ncolumns=2,append=TRUE,sep=',')
EOF

mv covmodel.coeffs ${outputfile}
