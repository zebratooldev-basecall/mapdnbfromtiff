#!/bin/tcsh -efx

set inputs=/Proj/Assembly/Shared/Assembly_tests/WorkFlowInputs

set codever=`echo $CGI_HOME | awk -F/ 'substr($0,1,9)=="/Proj/QA/"{print $(NF-1);exit}{print "1.2.0"}'`
set rel=`echo $codever | awk -F. -v OFS="." '{print $1,$2,$3}'`

if($rel == "1.0.0" || $rel == "1.1.0" || $rel == "1.1.1")then
  set cmpvarthresh=25
  set cmpvsttwo=40
  set cmpccfdelta=50
else
  set cmpvarthresh=20
  set cmpvsttwo=60
  set cmpccfdelta=100
endif

set target=$1
set ADFlist=$2
set rootdir=$3

setenv CGI_HOME $4
setenv PATH ${CGI_HOME}/bin:/usr/local/bin:/bin:/usr/bin
setenv LD_LIBRARY_PATH ${CGI_HOME}/bin


if ( -e ${rootdir}/${target} ) then
  echo "Target assembly area, ${rootdir}/${target}, already exists but should not -- ABORT!"
  exit -1
endif

set badcounter=`echo $ADFlist |sed s/,/_/g | awk -F_ 'BEGIN{n=0}{for(i=1;i<=NF;i++)if($i~/-ADF/){split($i,x,"-");if(x[1]!="01")n++}}END{print n}'`
if( $badcounter > 0 ) then
  echo "ADF list includes ADFs with counter value != 01 (e.g. *02-ADF); script too dumb to help you!"
  exit -1
endif

mkdir -p ${rootdir}/${target}
cd  ${rootdir}/${target}
ln -s ${inputs}/REF .
ln -s ${inputs}/ADF .

set FDFlist=`echo $ADFlist | sed s/ADF/FDF/g`
set firstFDF=`echo $FDFlist | awk -F, '{print $1}'`

if ( 0 ) then 
  foreach adf ( `echo $ADFlist | awk -F, '{for(i=1;i<=NF;i++)print $i}'` )
    human.py --use-custom-build -r ${rootdir}/${target} -t $target -o FDF -i $adf --driver-exec-mode gridwait --skip-mirage &
  end
  wait
else
  foreach fdf (`echo $FDFlist  | awk -F, '{for(i=1;i<=NF;i++)print $i}'`)
    set adf=`echo $fdf | sed s/FDF/ADF/`
    mkdir -p FDF/${fdf}
    echo "ADF $adf" >> FDF/${fdf}/sources
    cp ADF/${adf}/*{scores,calls,wells} FDF/${fdf}
    cp ADF/${adf}/${adf}.xml FDF/${fdf}/${fdf}.xml
  end
endif

human.py --use-custom-build -r ${rootdir}/${target} -t $target -o GAPS -i $firstFDF --driver-exec-mode gridwait

foreach fdf ( `echo $FDFlist | awk -F, '{for(i=1;i<=NF;i++)print $i}'` )
    human.py --use-custom-build -r ${rootdir}/${target} -t $target -o MAP -i $fdf --driver-exec-mode gridwait &
end
wait

set MAPlist=`echo $FDFlist | sed s/FDF/MAP/g | sed s/ADF/MAP/g `
human.py --use-custom-build -r ${rootdir}/${target} -t $target -o ASM -i $MAPlist -p diploid.optimization-max-time=3600 -p haploid.optimization-max-time=3600 -p mito.optimization-max-time=3600 --driver-exec-mode gridwait

cd ASM/*ASM
set ref=`cat sources  | awk '$1=="REF"{print $2}'`
set firstADF=`echo $ADFlist | awk -F, '{print $1}'`
cp ${rootdir}/${target}/ADF/${firstADF}/Mutations.tsv .
set refgbi=${rootdir}/${target}/REF/${ref}/reference.gbi

foreach varfile ( variations/ccf/Variations*tsv )
    set cutoff=`echo $varfile | awk -F/ '{print $NF}' | awk -F- '{split($1,x,"s");print x[2]}'`
    set vsttwo=`echo $varfile | awk -F/ '{print $NF}' | awk -F- '{split($2,x,"_");print x[1]}'`
    set ccfdelta=`echo $varfile | awk -F/ '{print $NF}' | awk -F_ '{split($2,y,".");print substr(y[1],4,length(y[1])-3)}'`
    ${CGI_HOME}/bin/do_calldiff.csh $refgbi $cutoff $ccfdelta $vsttwo
end

${CGI_HOME}/bin/summarize.csh $refgbi ${rootdir}/${target} $cmpvarthresh $cmpccfdelta $cmpvsttwo SummarizeSimOut.txt

set ASMid=`pwd | awk -F/ '{print $NF}'`
human.py --use-custom-build -r ${rootdir}/${target} -t $target -o EXP -i $ASMid --driver-exec-mode gridwait
