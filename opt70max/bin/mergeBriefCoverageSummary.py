#! /usr/bin/python

import sys
from optparse import OptionParser

parser = OptionParser()
parser.disable_interspersed_args()
parser.add_option("--output", "-o", default=None,
                  help="the output file name")
(options, args) = parser.parse_args()
if len(args) == 0:
    parser.print_help()
    raise "bad args"

header = None
keys = [ ]
sum = [ ]
if len(args) > 0:
    file = open(args[0])
    header = file.readline()
    fields = header.split(",")[1:]
    for line in file:
        keys.append(line.split(",")[0])
        sum.append([])
        for field in fields:
            sum[-1].append(0)

for arg in args:
    file = open(arg)
    line = file.readline()
    if line != header:
        raise "header mismatch"
    for ii in xrange(0, len(keys)):
        line = file.readline()
        fields = line.split(",")
        if fields[0] != keys[ii]:
            raise "field mismatch"
        fields = fields[1:]
        for jj in xrange(0,len(fields)):
            sum[ii][jj] += int(fields[jj])
    line = file.readline()
    if line != "" and line is not None:
        raise "eof expected"

out = sys.stdout
if options.output != None:
    out = open(options.output, "w")
out.write(header)
for ii in xrange(0, len(keys)):
    line = keys[ii]
    for val in sum[ii]:
        line += ",%d" % val
    out.write(line+"\n")
