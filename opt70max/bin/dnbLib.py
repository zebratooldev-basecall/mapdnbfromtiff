#!/usr/bin/env python

import os
import re

def parseAttrStr(line):
    '''parse line of key/value pairs from TeraMap output'''
    result = {}

    i = 1
    for field in line.strip().split(' '):
        if len(field) == 0: continue
        if re.search('=',field):
            subFields = field.split('=')
            result[subFields[0]] = subFields[1]
        else:
            result[str(i)] = field
            i += 1

    return result

def strToBoolean(string):
    if string.lower() == 'true':
        return True
    elif string.lower() == 'false':
        return False
    else:
        raise Exception("Invalid boolean string!")

class DnbArchitecture(object):
    def __init__(self, DnbArchitectureStr = "ad2_29_29"):
        self.dnbArchitecture = DnbArchitectureStr
        if self.dnbArchitecture == "ad4_35_35":
            self.leftSubReads = (5,10,10,10)
            self.rightSubReads = (10,10,10,5)
        elif self.dnbArchitecture == "ad2_29_29":
            self.leftSubReads = (10,10,10)
            self.rightSubReads = (10,10,10)
        elif self.dnbArchitecture == "ad2_28_28":
            self.leftSubReads = (10,10,10)
            self.rightSubReads = (10,10,10)
        else:
            raise Exception("Unknown read Structure!")

        # set inialial dnb positions.
        self.leftDnbPositions = list(range(1,sum(self.leftSubReads, 1)))
        self.rightDnbPositions = list(range(min(self.leftDnbPositions)+1, 
                                      sum(self.leftSubReads + self.rightSubReads, 1)))

    def getOffsets(self, side, gaps):
        if side.lower() == 'left':
            subReads = self.leftSubReads
        elif side.lower() == 'right':
            subReads = self.rightSubReads
        else:
            raise Exception("Dnb arm must be labeled left or right only")

        result = list(range(sum(subReads)))
        for i in range(1,len(subReads)):
            for j in range(sum(subReads[:i]),len(result)):
                result[j] = result[j] + gaps[i-1]

        return(result)

    def leftArmLength(self):
        return sum(self.leftSubReads)

    def rightArmLength(self):
        return sum(self.rightSubReads)

class Dnb(object):
    '''object to store dnb instance'''
    def __init__(self, Id, LeftArm=None, RightArm=None, DnbArchitecture = DnbArchitecture()):
        # set default values.
        self.id = Id
        self.leftArm = LeftArm
        self.rightArm = RightArm
        self.dnbArchitecture = DnbArchitecture

    def parseArmMappingSummary(self,line):
        attr = parseAttrStr(line.rstrip('\r\n'))

        # set arm attributes.
        if attr['1'] == 'LeftArm':
            self.leftArm.mappingResult = attr['resultType_']
            self.leftArm.indexOverflow = strToBoolean(attr['indexOverflow_'])
            self.leftArm.matchCount = int(attr['matchCount_'])
            self.leftArm.hasUnreportedPerfectMatches = strToBoolean(attr['hasUnreportedPerfectMatches_'])
            self.leftArm.hasUnreportedMatches = strToBoolean(attr['hasUnreportedMatches_'])
        elif attr['1'] == 'RightArm':
            self.rightArm.mappingResult = attr['resultType_']
            self.rightArm.indexOverflow = strToBoolean(attr['indexOverflow_'])
            self.rightArm.matchCount = int(attr['matchCount_'])
            self.rightArm.hasUnreportedPerfectMatches = strToBoolean(attr['hasUnreportedPerfectMatches_'])
            self.rightArm.hasUnreportedMatches = strToBoolean(attr['hasUnreportedMatches_'])
        else:
            raise Exception('Unexpected arm description')

    def hasUniqueFullMapping(self):
        if self.leftArm == None or self.rightArm == None:
            return False

        if self.leftArm.matchCount == 1 and self.rightArm.matchCount == 1:
            if self.leftArm.mappings[0].hasConsistentMate and self.rightArm.mappings[0].hasConsistentMate:
                return True
            else:
                return False
        else:
            return False

    def id(self):
        return self.id

class DnbArm(object):
    '''object to store insantance of a dnb arm'''
    def __init__(self, Dnb, Side, Sequence, mappingResult=None, indexOverflow=None, matchCount = 0):
        self.dnb = Dnb
        self.side = Side
        self.sequence = Sequence
        self.mappings = []

class DnbArmMapping(object):
    '''object to store instance of dnb arm mapping'''
    def __init__(self, line):
        attr = parseAttrStr(line)

        self.dnbArm = None
        self.chromosome = attr['1']
        self.begin = int(attr['2'])
        if attr['3'][0] == 'L':
            self.arm = 'left'
        else:
            self.arm = 'right'
        self.strand = attr['3'][1]
        self.gaps = map(int, list(attr['gaps'].split(',')))
        self.editCount = int(attr['editCount_'])
        self.hasConsistentMate = strToBoolean(attr['hasConsistentMate_'])
        self.foundByLocalSearch = strToBoolean(attr['foundByLocalSearch_'])

class DnbMappingReader(object):
    '''object used to load dnb mappings from TeraMap output'''
    def __init__(self, TeraMapFn, DnbArchitectureObj):
        self.fn = TeraMapFn
        self.dnbArchitecture = DnbArchitectureObj

        # check that file exists.
        if not os.path.isfile(self.fn):
            raise Exception("input file does not exist!")
            sys.exit(-1)

        # open input file.
        self.ff = open(self.fn, 'r')

    def readDnbs(self, filterNonUniqMappings = False):
        result = []

        while True:
            dnb = self.nextDnb()
            if dnb == None: break
            if filterNonUniqMappings:
                if dnb.hasUniqueFullMapping(): result.append(dnb)
            else:
                result.append(dnb)

        return(result)

    def nextDnb(self):
        # read in first line and create dnb object.
        line = self.ff.readline()
        if not line:
            return None

        # parse first line of TeraMap dnb mapping report.
        fields = line.rstrip('\r\n').split(' ')
        dnb = Dnb(fields[0], DnbArchitecture=self.dnbArchitecture)

        # set dnb arms.
        dnb.leftArm = DnbArm(dnb, 'left', fields[1])
        dnb.rightArm = DnbArm(dnb, 'right', fields[2])

        # load mapping summary attributes.
        dnb.parseArmMappingSummary(self.ff.readline())
        dnb.parseArmMappingSummary(self.ff.readline())

        # read in TeraMap arm mappings.
        while True:
            line = self.ff.readline().rstrip('\r\n')
            if line == '': 
                return dnb
            dnbArmMapping = DnbArmMapping(line)
            if dnbArmMapping.arm == 'left':
                dnbArmMapping.dnbArm = dnb.leftArm
                dnb.leftArm.mappings.append(dnbArmMapping)
            else:
                dnbArmMapping.dnbArm = dnb.rightArm
                dnb.rightArm.mappings.append(dnbArmMapping)

    def close(self):
        self.ff.close()

class MappedDnbCollection(object):
    def __init__(self,fn):
        self.fn = fn

    def loadDnbMappings(self, DnbArchitectureParam, FullUniqueMappingsOnly=True):
        dnbArch = DnbArchitecture(DnbArchitectureStr=DnbArchitectureParam)

        dr = DnbMappingReader(self.fn, DnbArchitectureObj=dnbArch)
        self.dnbs = dr.readDnbs(FullUniqueMappingsOnly)

    def indexByChromosome(self):
        self.dnbsByChrom = {}

        for dnb in self.dnbs:
            if not self.dnbsByChrom.has_key(dnb.leftArm.mappings[0].chromosome):
                self.dnbsByChrom[dnb.leftArm.mappings[0].chromosome] = []
            self.dnbsByChrom[dnb.leftArm.mappings[0].chromosome].append(dnb)

