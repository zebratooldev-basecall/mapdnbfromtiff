#!/usr/bin/env python
import os
import string
import dnbcollection
import driverlib
from driverlib import Error

DEFAULT_ROOT_DIR = '/Proj/Assembly/inazarenko/FakeRoot'

class PipelineObject(object):
    def __init__(self, otype, name, location, sample='', ref='',
                 library='', lane='', adf='', gaps='',
                 mapList=None, asm=''):
        self.otype = otype
        self.name = name
        self.location = location
        self.ref = ref
        self.sample = sample
        self.library = library
        self.lane = lane
        self.adf = adf
        self.gaps = gaps
        self.mapList = mapList
        self.asm = asm

class ObjectManager(object):
    LEGAL_ID_CHARACTERS = frozenset(string.letters + string.digits + '-_')

    LEGAL_TYPES = ['REF', 'ADF', 'FDF', 'GAPS', 'GAP', 'MAP', 'ASM', 'EXP', 'SIM']

    def __init__(self, rootDir, cgiHome):
        self.rootDir = rootDir
        self.cgiHome = cgiHome

        self.cache = {}
        self.usedIds = {}
        self.danglingRef = {}

        for otype in ObjectManager.LEGAL_TYPES:
            path = os.path.join(self.rootDir, otype)
            if os.path.exists(path):
                self.usedIds[otype] = [ dir for dir in os.listdir(path)
                                        if os.path.isdir(os.path.join(path, dir)) and
                                           dir.endswith(otype) ]
            else:
                self.usedIds[otype] = []

    def unlock(self):
        # TODO invent dumb locking mechanism which won't work anyway
        return

    def objectLoc(self, otype, name):
        if not otype in ObjectManager.LEGAL_TYPES:
            raise Error('bad object type: ' + otype)
        return os.path.join(self.rootDir, otype, name)

    def scriptLoc(self, name):
        return os.path.join(self.cgiHome, 'etc', 'workflows', 'human', name)

    def binDir(self):
        return os.path.join(self.cgiHome, 'bin')

    def createId(self, otype, baseName):
        if not otype in ObjectManager.LEGAL_TYPES:
            raise Error('bad object type: ' + otype)

        ids = self.usedIds[otype]
        nextId = 1
        for id in ids:
            if id.startswith(baseName):
                suffix = id[len(baseName)+1:]
                digits = suffix.split('-')[0]
                usedId = int(digits)
                if nextId <= usedId:
                    nextId = usedId + 1

        newId = '%s_%02d-%s' % (baseName, nextId, otype)
        self.usedIds[otype].append(newId)

        os.makedirs(self.objectLoc(otype, newId))

        return newId

    def findDefaultGaps(self, library):
        location = os.path.join(self.rootDir, 'LIB', library, 'default-gaps')
        if not os.path.exists(location):
            raise Error('default gaps not set for library: ' + library)
        f = open(location, 'r')
        line = f.readline().rstrip()
        f.close()

        if not line:
            raise Error('broken default-gaps file for library: ' + library)

        return self.findObject(line)

    def setDefaultGaps(self, library, gapId):
        location = os.path.join(self.rootDir, 'LIB', library, 'default-gaps')
        if os.path.exists(location):
            print "default gaps for library '%s' already set." % library
        else:
            dirPath = os.path.join(self.rootDir, 'LIB', library)
            if not os.path.isdir(dirPath):
                os.makedirs(dirPath)
            f = open(location, 'w')
            print >>f, gapId
            f.close()
            print "default gaps for library '%s' set to '%s'" % (library, gapId)

    def _findLaneForDF(self, location, id):
        c = None
        try:
            c = dnbcollection.Collection(os.path.join(location, id+'.xml'))
        except:
            return dnbcollection.Lane(None, None, None, None)
            
        if len(c.lanes) != 1:
            raise Error('bad collection in %s: more than one lane' % location)
        return c.lanes[0]

    def checkId(self, id):
        for ch in id:
            if ch not in ObjectManager.LEGAL_ID_CHARACTERS:
                raise Error("unexpected character in object id %s: %s" % (repr(id), repr(ch)))

    def findObject(self, id):
        self.checkId(id)

        if id in self.cache:
            return self.cache[id]

        (tail, otype) = id.rsplit('-', 1)
        if not otype:
            raise Error('bad object id: ' + id)
        elif not otype in ObjectManager.LEGAL_TYPES:
            raise Error('bad object type in id: ' + id)

        location = os.path.join(self.rootDir, otype, id)
        if not os.path.isdir(location):
            raise Error('could not find object directory: ' + location)

        if otype == 'ADF':
            lane = self._findLaneForDF(location, id)
            if lane.name is None:
                obj = PipelineObject(otype, id, '')
            else:
                obj = PipelineObject(otype,
                                     id, location,
                                     sample=lane.sample,
                                     library=lane.library.name,
                                     lane=lane.name)
        elif otype == 'REF':
            obj = PipelineObject(otype, id, location)
        else:
            fn = os.path.join(location, 'sources')
            if not os.path.exists(fn):
                sources = []
            else:
                srcf = open(fn, 'r')
                sources = [ x.rstrip().split()[1] for x in srcf.readlines() ]
                srcf.close()

            if not sources:
                raise Error("'sources' file not readable in object directory: " + location)

            if otype == 'ASM':
                sources = [ x for x in sources if x[-3:] != 'REF' ]
                first = None
                for s in sources:
                    try:
                        first = self.findObject(s)
                        break
                    except Error, e:
                        pass
                if first == None:
                    obj = PipelineObject('ASM', id, location, mapList=[])
                else:
                    obj = PipelineObject('ASM', id, location,
                                         sample=first.sample,
                                         mapList=sources, ref=first.ref)
            else:
                sourceMap = {}
                callStore = None
                for src in sources:
                    try:
                        srco = self.findObject(src)
                    except Error, e:
                        #print "Source file for " + id + " references non-existent object " + src
                        srco = PipelineObject(src.rsplit('-', 1)[1], src, '')
                        if src in self.danglingRef:
                            self.danglingRef[src] += [ id ]
                        else:
                            self.danglingRef[src] = [ id ]
                    sourceMap[srco.otype] = srco
                    if srco.otype in frozenset(['ADF', 'FDF']):
                        assert callStore is None
                        callStore = srco


                if otype != 'EXP':
                    assert callStore is not None

                if otype in frozenset(['MAP', 'GAPS', 'GAP', 'EXP']):
                    if 'REF' not in sourceMap:
                        if otype == 'EXP':
                            raise Error('cannot determine reference for %s' % id)

                        # REF source must be specified for MAP and GAPS; however,
                        # Assembly 1.0.0 didn't record it in the sources.
                        if callStore.sample in frozenset(['GS00016-DNA-B01', 'GS00016-DNA-D01']):
                            objRef = 'HUMAN-M_01-REF'
                        elif callStore.sample in frozenset(['GS00016-DNA-A01',
                                                            'GS00011-DNA-C01',
                                                            'GS00013-DNA-C01',
                                                            'GS00016-DNA-C01']):
                            objRef = 'female-no-id'
                        elif callStore.sample == 'GS00016-DNA-E01':
                            objRef = 'twobac-no-id'
                        elif callStore.sample == 'GS00013-DNA-F01':
                            objRef = 'bac167-no-id'
                        else:
                            raise Error('cannot determine reference for %s' % id)
                    else:
                        objRef = sourceMap['REF'].name

                if otype == 'FDF':
                    lane = self._findLaneForDF(location, id)
                    if lane.name == None:
                        obj = PipelineObject(otype, id, '')
                    else:
                        if callStore.lane != '' and \
                           ( lane.sample != callStore.sample or \
                             lane.library.name != callStore.library or \
                             lane.name != callStore.lane ):
                            raise Error('collection in %s is '
                                        'inconsistent with source ADF %s' %
                                                    (location, callStore.name))
                        obj = PipelineObject(otype, id, location,
                                             sample=lane.sample,
                                             library=lane.library.name,
                                             lane=lane.name,
                                             adf=callStore.name)
                elif otype == 'GAPS' or otype == 'GAP':
                    if callStore is None:
                        obj = PipelineObject(otype, id, location)
                    else:
                        obj = PipelineObject(otype, id, location,
                                             sample=callStore.sample,
                                             library=callStore.library,
                                             lane=callStore.lane,
                                             adf=callStore.name,
                                             ref=objRef)
                    try:
                        gfn = os.path.join(location, 'gender')
                        gf = open(gfn, 'r')
                        line = gf.readline().strip()
                        gf.close()
                        if line in ['male', 'female']:
                            obj.gender = line
                        else:
                            obj.gender = 'unknown'
                    except:
                        obj.gender = 'unknown'
                elif otype == 'MAP':
                    if 'GAPS' in sourceMap:
                        go = sourceMap['GAPS']
                    elif 'GAP' in sourceMap:
                        go = sourceMap['GAP']
                    else:
                        go = None
                    obj = PipelineObject(otype, id, location,
                                         sample=callStore.sample,
                                         library=callStore.library,
                                         lane=callStore.lane,
                                         adf=callStore.name,
                                         gaps=go.name,
                                         ref=objRef)
                elif otype == 'EXP':
                    asmObj = sourceMap['ASM']
                    obj = PipelineObject(otype, id, location,
                                         sample=asmObj.sample,
                                         ref=objRef, asm=asmObj.name)
                else:
                    assert False

        self.cache[id] = obj
        return obj

def _flatten(s):
    if isinstance(s, list):
        return ','.join(s)
    else:
        return str(s)

def _none2empty(x):
    if x is None:
        return ''
    else:
        return x

def _main():
    import time
    from optparse import OptionParser
    parser = OptionParser('usage: %prog [options]')
    parser.set_defaults(rootDir=DEFAULT_ROOT_DIR, verbose=True, strict=False)
    parser.add_option('-r', '--root-dir', dest='rootDir',
                      help='use ROOTDIR as the base directory for the pipeline '
                           'object locations')
    parser.add_option('-o', '--output', dest='output',
                      help='create separate csv files in the OUTPUT directory')
    parser.add_option('--html-summary', dest='htmlSummary', action='store_true',
                      help='create HTML file with links to all assembly web reports')
    parser.add_option('-q', '--quiet', dest='verbose', action='store_false',
                      help='do not generate verbose output')
    parser.add_option('-s', '--strict', dest='strict', action='store_false',
                      help='perform strict error checking and exit upon error')
    (options, args) = parser.parse_args()
    if len(args) != 0:
        raise Error('unexpected arguments')
    cgiHome = driverlib.getCgiHome()
    om = ObjectManager(options.rootDir, cgiHome)

    fields = { 'REF': ['otype', 'name'],
               'ADF': ['otype', 'name', 'sample', 'library', 'lane'],
               'FDF': ['otype', 'name', 'sample', 'library', 'lane', 'adf'],
               'GAPS':['otype', 'name', 'sample', 'library', 'adf', 'ref'],
               'GAP':['otype', 'name', 'sample', 'library', 'adf', 'ref'],
               'MAP': ['otype', 'name', 'sample', 'library', 'lane', 'gaps', 'adf', 'ref'],
               'ASM': ['otype', 'name', 'sample', 'mapList', 'ref'],
               'EXP': ['otype', 'name', 'sample', 'asm', 'ref'], }

    if options.htmlSummary:
        explist = om.usedIds['EXP']
        byasm = {}
        for oid in sorted(explist):
            try:
                obj = om.findObject(oid)
                byasm[obj.asm] = obj
            except Error, e:
                pass

        idlist = om.usedIds['ASM']
        bysample = []
        for oid in idlist:
            try:
                obj = om.findObject(oid)
                bysample.append( (obj.sample, obj) )
            except Error, e:
                pass

        print """
        <html><head>
        <style type="text/css">
            table {
            border-collapse: collapse;
            }
            table td {
            border: solid #BBBBBB thin;
            padding-left: 2px;
            padding-right: 2px;
            text-align: right;
            }
        </style>"""

        print '<html><body><table>'
        print '<tr><td>Sample</td><td>ASM</td><td>Comment</td><td>Reports</td>'
        print '<td>Time</td><td>EXP</td><td>ASM/EXP path</td></tr>'
        for smp, obj in sorted(bysample):
            wrdir = os.path.join(obj.location, 'WebReports')
            windir = 'file://///hoard/' + '/'.join(wrdir.split('/')[2:])

            stats = os.stat(obj.location)
            timestr = time.asctime( time.localtime(stats[8]) )

            try:
                commentsfile = os.path.join(obj.location, 'comment')
                cf = open(commentsfile, 'r')
                comment = cf.readlines()[0].split(',')[1].strip()
            except:
                comment = ''

            if obj.name in byasm:
                expstr = byasm[obj.name].name
                exppath = byasm[obj.name].location
            else:
                expstr = "-"
                exppath = "-"
            if os.path.exists(wrdir):
                print '''<tr><td>%s</td><td>%s</td><td>%s</td>
                         <td><a href="%s/%s">summary</a>
                             <a href="%s/%s">mapping</a>
                             <a href="%s/%s">coverage</a>
                             <a href="%s/%s">calls</a></td>
                         <td>%s</td>
                         <td>%s</td>
                         <td>%s, %s</td>
                         </tr>''' % \
                    (smp, obj.name, comment,
                     windir, 'summary.html',
                     windir, 'mapping.html',
                     windir, 'coverage.html',
                     windir, 'callrpt.html',
                     timestr, expstr, obj.location, exppath)
        print '</table></body></html>'
    elif options.output is None:
        for otype in ObjectManager.LEGAL_TYPES:
            flist = fields[otype]
            print otype
            print ','.join(flist)
            idlist = sorted(om.usedIds[otype])
            for oid in idlist:
                try:
                    obj = om.findObject(oid)
                    assert obj.otype == otype
                    print ','.join([ _flatten(obj.__dict__[field]) for field in flist ])
                except Error, e:
                    print ('%s,%s,BROKEN,' % (otype, oid)), e
                    if options.strict:
                        raise e
    else:
        for otype in ['MAP', 'GAPS', 'GAP', 'EXP']:
            flist = fields[otype][1:]
            f = open(os.path.join(options.output, otype + '.csv'), 'wb')

            idlist = sorted(om.usedIds[otype])
            for oid in idlist:
                try:
                    obj = om.findObject(oid)
                    assert obj.otype == otype
                    if options.verbose:
                        print obj.name
                    print >>f, ','.join([ getattr(obj, field) for field in flist ]) + '\r'
                except Error, e:
                    print ('%s,%s,BROKEN,' % (otype, oid)), e
                    if options.strict:
                        raise e
            f.close()

        f = open(os.path.join(options.output, 'XDF.csv'), 'wb')
        flist = fields['FDF'][1:]
        idlist = sorted(om.usedIds['FDF'] + om.usedIds['ADF'])
        for oid in idlist:
            try:
                obj = om.findObject(oid)
                if obj.otype == 'ADF':
                    obj.adf = ''
                if options.verbose:
                    print obj.name
                print >>f, ','.join([ getattr(obj, field) for field in flist ]) + '\r'
            except Error, e:
                print ('%s,BROKEN,' % (oid)), e
                if options.strict:
                    raise e
        f.close()

        # generate pseudo-unique ID every run; just don't run this more
        # than once in 10 seconds or so...
        import time
        count = int(10*(time.time() - 1237390000))

        f = open(os.path.join(options.output, 'ASM.csv'), 'wb')
        flist = ['name', 'sample', 'mapListID', 'ref']
        idlist = sorted(om.usedIds['ASM'])
        maplist = []
        for oid in idlist:
            try:
                obj = om.findObject(oid)
                obj.mapListID = str(count)
                count += 1
                if options.verbose:
                    print obj.name
                print >>f, ','.join([ getattr(obj, field) for field in flist ]) + '\r'
                maplist.append(obj)
            except Error, e:
                print ('%s,BROKEN,' % (oid)), e
                if options.strict:
                    raise e
        f.close()

        f = open(os.path.join(options.output, 'MAPLIST.csv'), 'wb')
        for obj in maplist:
            if options.verbose:
                print obj.name, ' - maplist'
            for m in obj.mapList:
                print >>f, '%s,%s\r' % (obj.mapListID, m)
        f.close()

        f = open(os.path.join(options.output, 'DANGLING-REF.csv'), 'wb')
        for ref,src in om.danglingRef.items():
            for id in src:
                print >>f, '%s,%s\r' % (ref, id)
        f.close()


if __name__ == '__main__':
    _main()
