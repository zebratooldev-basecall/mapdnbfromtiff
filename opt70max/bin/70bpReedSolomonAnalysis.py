#!/usr/bin/python

"""

"""

__author__ = "Anushka Brownley"
__version__ = "$RCSId$"
__date__ = "$Date$"
__copyright__ = "Copyright (c) 2008 Complete Genomics"
__license__ = "Proprietary"

import os.path
import sys
import getopt
import shutil
import re
from rsLookupMetrics import FieldMetrics, LaneMetrics, LookupTable
from datetime import datetime, date, time

# Env vars
CGIHome = os.environ['CGI_HOME']
CGIPath = os.environ['LD_LIBRARY_PATH']

# DNB vars
DNB_Len = 70
RSCodeLen = 10
numFullRSCodes = 6
numRSCodes = 8
firstPartialStartPos = 0
lastPartialStartPos = 65
baseLST = ['A','T','G','C']

if os.environ.get('CGI_HOME') is None:
    print "Please set CGI_HOME."
    sys.exit(-4)

print "CGI Executables found in path " + CGIPath
from CGIPipeline import *

################################
def makeDir(dirPath):
    if not(os.path.isdir(dirPath)):
        os.mkdir(dirPath, 0775)

################################
def usage(script):
    print "\nThis script takes an csv file sequencer run information and a template ADF"
    print "input XML file and outputs a modified ADF input XML file to run through make ADF\n"
    print script
    print "    -h, --help         <arg>   outputs this help menu"
    print "    -a, --adf          <arg>   path to ADF file"
    print "    -s, --slideID      <arg>   slide ID "
    print "    -l, --laneID       <arg>   lane ID"
    print "    -o, --output-dir   <arg>   output directory"
    print "    -i, --arr-id       <arg>   grid array index (0)"
    print "    -n, --arr-size     <arg>   grid array size (1)"
    print "    -r, --rev-comp     <arg>   true to reverse complements RS code (false)"
    print "    -g, --recover      <arg>   use adjacent 10-mers to recover data (false)"
    print "    -p, --print-seq    <arg>   csv of fields to print DNB data"
    print "    -d, --dye-swap     <arg>   list of DNB positions and base swaps (e.g. 10;A:T;T:A,11;G:C,C:G)\n"

################################
def revcomplement(origSeq):
    seq = origSeq[::-1]
    complementDCT = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'N': 'N'}
    complseq = [complementDCT[base.upper()] for base in seq]
    return "".join(complseq)
################################
def convertTo80bp(val):
        if val > 5:
            val +=5
        
        if val > 70:
            val +=5
        
        return val
################################
def convertTo70bp(val):
        if val > 5:
            val -=5
        
        if val > 70:
            val -=5
        
        return val
################################

def getDNBSeq(startPos, endPos, dnb, dyeSwapDCT):
    nMerScoreData = []
    obsSeq = ''
    for newbaseInd in range(startPos,endPos):
        score = dnb.getRawBaseScore(int(newbaseInd))
        nMerScoreData.append(score)
        if score == 201:
            obsSeq += "N"
        else:
            if str(newbaseInd) in dyeSwapDCT.keys():
                if dnb.getBases().get(int(newbaseInd)).upper() in dyeSwapDCT[str(newbaseInd)].keys():
                    obsSeq += dyeSwapDCT[str(newbaseInd)][dnb.getBases().get(int(newbaseInd)).upper()]
                else:
                    obsSeq += dnb.getBases().get(int(newbaseInd)).upper()
            else:
                obsSeq += dnb.getBases().get(int(newbaseInd)).upper()
         
    return (obsSeq,nMerScoreData) 
    
################################
def checkRSCodeConsistency(codeLST):
    consistent = True
    for index in range(1,numFullRSCodes/2):
        i = index*2
        code1 = codeLST[i]
        if code1.find('N') > -1:
            continue
        
        code2 = codeLST[i+1]
        if code2.find('N') > -1:
            continue
        
        if not code1 == code2:
            consistent = False
    
    return consistent

################################
def incrementFiveMerNocall(seq,startPos,fieldOBJ):
    for i in range(0,len(seq)):
        if(seq[i] == 'N'):
            fieldOBJ.incrementFiveMerNoCallCount(1,startPos+i)

################################

def main(argv):
    script = argv[0]
    curPath = os.path.dirname(os.path.abspath(script))
    argv = argv[1:]
    adfFile = None
    slideID = None
    laneID = None
    outputDir = None
    revComp = False
    recoverNmer = False
    ArrID = 1
    ArrSize = 1
    lookupTable = CGIHome + "/etc/config/ReedSolomon/10_bp_lookupTable.csv"
    lookupTable20bp = CGIHome + "/etc/config/ReedSolomon/20_bp_lookupTable.txt"
    basePosLST = [5,6,7,8,9,10,11,12,13,14]
    dyeSwapStr = ''
    fieldLST = []

    try:
        opts, args = getopt.getopt(argv, "h:a:s:l:o:i:n:r:d:g:p:", ["help=", "adf=", "slideID=", "laneID=", "output-dir=", "arr-id=", "arr-size=", "rev-comp=","dye-swap=","recover=","print-seq="])
    except getopt.GetoptError:
        usage(script)
        sys.exit(1)
    
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(script)
            sys.exit(0)
        elif opt in ("-a", "--adf"):
            adfFile = arg
        elif opt in ("-s", "--slideID"):
            slideID = arg
        elif opt in ("-l", "--laneID"):
            laneID = arg
        elif opt in ("-o", "--output-dir"):
            outputDir = arg
        elif opt in ("-i", "--arr-id"):
            ArrID = int(arg) + 1
        elif opt in ("-n", "--arr-size"):
            ArrSize = int(arg)
        elif opt in ("-d", "--dye-swap"):
            if arg.lower() == "false":
                dyeSwapStr = ''
            else:
                dyeSwapStr = arg
        elif opt in ("-p", "--print-seq"):
            if arg.lower() == "none":
                fieldLST = []
            else:
                fieldLST = arg.rstrip().rsplit(",")
        elif opt in ("-r", "--rev-comp"):
            if arg.lower() == "true":
                revComp = True
        elif opt in ("-g", "--recover"):
            if arg.lower() == "true":
                recoverNmer = True
    
    # validate input
    if adfFile is None or slideID is None or laneID is None or outputDir is None:
        print "Please enter all required input parameters, adf file, slideID and laneID."
        usage(script)
        sys.exit(1)
    
    if not(os.path.isfile(adfFile)):
        print "Please enter valid ADF file"
        usage(script)
        sys.exit(1)
    
    if not(len(basePosLST) == RSCodeLen):
        print "ERROR: RS structure must contain 10 bp positions\n"
        sys.exit(1)
    
    for i in range (0,len(fieldLST)):
        if fieldLST[i].rstrip() == "":
            fieldLST.pop(i)
            continue
            
        l = re.compile('L0[0-9]C[0-9][0-9]R[0-9][0-9]')
        if l.match(fieldLST[i]) == None:
            newID = laneID+fieldLST[i]
            if l.match(newID) == None:
                raise Exception("Field name is incorrectly formated: " + fieldLST[i])
            else:
                fieldLST[i] = newID
        elif not fieldLST[i][0:3] == laneID:
            raise Exception("Field does not match lane: " + fieldLST[i] + " not in lane " + laneID)
    
    # format dye swap data
    dyeSwapDCT = {}
    if dyeSwapStr.find(",") > -1:
        dyeSwapLST = dyeSwapStr.split(",")
    else:
        dyeSwapLST = []
    
    for posData in dyeSwapLST:
        swapLST = posData.split(";")
        for baseData in swapLST:
            if baseData.find(":") == -1:
                pos = convertTo70bp(int(baseData))
            else:
                (base,baseSwap) = baseData.split(":")
                if pos in dyeSwapDCT:
                    dyeSwapDCT[pos][base] = baseSwap
                else:
                    dyeSwapDCT[pos] = {base:baseSwap}
    
    # load lookup table and ADF
    lookupTableOBJ = LookupTable(lookupTable, lookupTable20bp,DNB_Len)
    laneMetricsOBJ = LaneMetrics(DNB_Len,numRSCodes)
    collection = Collection(adfFile,'./')
    lane = collection.getLane(slideID + ":" + laneID)
    fields = lane.getFields()
    cycles = lane.getCycles()
    cycleDCT = {}
    for i in range(0, len(cycles)):
        if int(cycles[i][0]) >= 0:
            cycleDCT[cycles[i][0]] = str(cycles[i][4]) # each pos to cycle
    
    keys = cycleDCT.keys()
    keys.sort()
    cycleStr = "_".join(map(cycleDCT.get, keys))
    
    # run data DB load file
    posCycMapDCT = {}
    for i in range(0,DNB_Len):
        pos = convertTo80bp(i)
        posCycMapDCT[pos]=""
        
    if ArrID == 1:
        masterDBFile = outputDir + "/" + slideID + "_" + laneID + ".master_dbload.csv"
        outFH = open(masterDBFile, "w")
        outFH.write("RSAnalysisRun,"+curPath+","+slideID+","+laneID+",1,"+str(int(revComp))+","+str(int(recoverNmer))+","+dyeSwapStr+",\n")
        for i in range(0, len(cycles)):
            # convert 70pos DNB into LIMS compatible 80 pos DNB
            if int(cycles[i][0]) >= 0:
                pos = convertTo80bp(int(cycles[i][0]))
                
                # determine which 10-mer the position is associated with
                tenMer = int((pos-1)/10) + 1
                outFH.write("RSCyclePosMap,"+str(pos)+","+str(cycles[i][1])+","+str(cycles[i][2])+","+str(cycles[i][4])+","+str(tenMer)+"\n")
                posCycMapDCT[pos] = str(cycles[i][4])
                
        outFH.close()
    
    keys = posCycMapDCT.keys()
    keys.sort()
    commentHeader = "Pos"+", Pos".join(["%s" % a for a in keys])
    commentData = ",".join(map(lambda x: posCycMapDCT[x], keys))
    
    # Set up output directories and files
    makeDir(outputDir)
    outputFile = outputDir + "/" + slideID + "_" + laneID + "_" + cycleStr + "." + str(ArrID) + ".csv"
    outFH = open(outputFile, "w")
    laneMetricsOBJ.printHeader(outFH,commentHeader)
    
    laneDBFile = outputDir + "/" + slideID + "_" + laneID + "." + str(ArrID) + ".dbload.csv"
    laneDBFH = open(laneDBFile, "w")
    
    fieldOutFile = outputDir + "/" + slideID + "_" + laneID + "_" + cycleStr + "_byField." + str(ArrID) + ".csv"
    fieldOutFH = open(fieldOutFile, "w")
    laneMetricsOBJ.printHeader(fieldOutFH,commentHeader)
    
    fieldDBFile = outputDir + "/" + slideID + "_" + laneID + "_byField." + str(ArrID) + ".dbload.csv"
    fieldDBFH = open(fieldDBFile, "w")
    
    print "Total number of fields:",len(fields)
        
    fieldCount = 0
    completedFields = 0
    for i in range(0, len(fields)):
        field = fields[i]
        
        # If print-seq is selected then only itterate through fields of interest
        if len(fieldLST) > 0:
            if not(field.getName() in fieldLST):
                continue
            else:
                fieldOutFile = outputDir + "/" + slideID + "_" + laneID + "_" + field.getName() + "_seq.csv"
                fieldFH = open(fieldOutFile, "w")
                scoreStr = ''
                for i in range(0,DNB_Len):
                    scoreStr += "Score Pos "+str(i+1)+","
                    
                tenMerStr = ''
                for i in range(0,numRSCodes):
                    tenMerStr += "Raw Map 10mer "+str(i+1)+",Corrected Map 10mer "+str(i+1)+","
                
                fieldFH.write("Obs Seq,Exp Seq,"+tenMerStr+","+scoreStr+"\n")
                
        queuePos = int(fieldCount) % int(ArrSize)
        fieldCount += 1
        
        # If array position does not match current job ID then skip
        if not(queuePos == int(ArrID) - 1):
            continue
        
        print "Field name:", field.getName(), "Field index:", field.getIndexWithinLane(),"Field size:",field.size()
        if field.getRows() == 0 or field.getColumns() == 0:
            continue
        
        completedFields += 1
        fieldMetricsOBJ = FieldMetrics(DNB_Len,numRSCodes,field.getRows(),field.getColumns())
        
        # itterate each DNB in field
        NoCallCount = 0
        for dnb in field:
            fieldMetricsOBJ.incrementTotalDNBs(1)
            
            # Get full DNB Data
            (fullDNBObsSeq, fullDNBScore) = getDNBSeq(0,DNB_Len,dnb,dyeSwapDCT)
            fullDNB20mers = ['']*(numRSCodes)                                                       # for each possible 10-mer code, stores the expected RS 20-mer
            dnbScore = []                                                                           # full dnb score
            dnbObsSeq = ''                                                                          # full dnb observed sequence
            dnbExpSeq = ''                                                                          # full dnb expected sequence not including partial RS codes
            
            # rev complement the first partial RS code if necessary
            if revComp:
                dnbObsSeq = revcomplement(fullDNBObsSeq[firstPartialStartPos:RSCodeLen/2])
                subScore = fullDNBScore[firstPartialStartPos:RSCodeLen/2]
                subScore.reverse()
                dnbScore.extend(subScore)
            else:
                dnbObsSeq = fullDNBObsSeq[firstPartialStartPos:RSCodeLen/2]
                dnbScore.extend(fullDNBScore[firstPartialStartPos:RSCodeLen/2])
            
            # store observed sequence from first and last partial codes, regardless of no-call count in code
            fullDNB20mers[0] = dnbObsSeq
            incrementFiveMerNocall(dnbObsSeq,0,fieldMetricsOBJ)
            fullDNB20mers[numFullRSCodes+1] = fullDNBObsSeq[lastPartialStartPos:DNB_Len]
            incrementFiveMerNocall(fullDNBObsSeq[lastPartialStartPos:DNB_Len], lastPartialStartPos, fieldMetricsOBJ)
            
            # Full DNB Counts
            rawMappedCount = 0
            correctedMappedCount = 0
            noCallCount = 0
            unmappedCount = 0
            
            # Attempt to map and categorize each complete 10-mer
            for i in range (0,numFullRSCodes):
                allCodeIndex = i+1
                posLST = map (lambda x: int(x)+(RSCodeLen*i) ,basePosLST)
                obsSeq = ''
                nMerScore = []
                
                # get 10-mer sequence and score data and rev comp if necessary
                if int(posLST[0]) < DNB_Len/2 and revComp:
                    obsSeq = revcomplement(fullDNBObsSeq[int(posLST[0]):int(posLST[9])+1])
                    nMerScore = fullDNBScore[int(posLST[0]):int(posLST[9])+1]
                    nMerScore.reverse()
                else:
                    obsSeq = fullDNBObsSeq[int(posLST[0]):int(posLST[9])+1]
                    nMerScore = fullDNBScore[int(posLST[0]):int(posLST[9])+1]
                
                dnbScore.extend(nMerScore)
                
                # check for N's and increment no-call count unless recovery flag is set
                if obsSeq.find("N") > -1:
                    dnbExpSeq += 'N'*RSCodeLen
                    fullDNB20mers[allCodeIndex] = 'N'*(RSCodeLen*2)
                    if not recoverNmer:
                        noCallCount += 1
                        dnbObsSeq += 'N'*RSCodeLen
                        fieldMetricsOBJ.incrementTotalNoCallDNBSegment(1,allCodeIndex)
                    else:
                        dnbObsSeq += obsSeq
                    
                    continue
                
                # add 10mer to full dnb observed seq
                dnbObsSeq += obsSeq
                
                # look up expected sequence for the 10-mer and 20-mer
                (expSeq,exp20Seq) = lookupTableOBJ.seqLookup(obsSeq)
                
                if not exp20Seq == '':
                    # if 10-mer maps, icrement raw or error corrected map count
                    fullDNB20mers[allCodeIndex] = exp20Seq
                    dnbExpSeq += expSeq
                    if expSeq == obsSeq:
                        rawMappedCount += 1
                        fieldMetricsOBJ.incrementTotalRawMappedDNBSegment(1,allCodeIndex)
                    else:
                        correctedMappedCount += 1
                        fieldMetricsOBJ.incrementTotalCorrectedDNBSegment(1,allCodeIndex)
                else:
                    # if 10-mer does not map and recovery won't be attemped, then increment unmapped count
                    fullDNB20mers[allCodeIndex] = 'N'*(RSCodeLen*2)
                    dnbExpSeq += 'N'*RSCodeLen
                    if not recoverNmer:
                        unmappedCount += 1
                        fieldMetricsOBJ.incrementUnmappedDNBSegment(1,allCodeIndex)
            
            # complete full DNB seq and score data with last partial RS code
            dnbObsSeq += fullDNB20mers[numFullRSCodes+1]
            dnbScore.extend(fullDNBScore[DNB_Len-(RSCodeLen/2):DNB_Len])
            dnbExpSeq += fullDNB20mers[numFullRSCodes][15:20]
            if revComp:
                dnbExpSeq = fullDNB20mers[1][15:20] + dnbExpSeq
            else:
                dnbExpSeq = fullDNB20mers[1][firstPartialStartPos:RSCodeLen/2] + dnbExpSeq
                
            # print dnb seq score data if required
            if len(fieldLST) > 0:
                startPos = 0
                codeLen = 5
                mappingStr = ''
                for tenMer in range(0,numRSCodes):
                    if tenMer == numRSCodes-1:
                        codeLen = 5
                    elif tenMer > 0:
                        codeLen = 10
                        
                    rawMap = 0
                    correctedMap = 0
                    if not fullDNBObsSeq[startPos:startPos+codeLen] == 'N'*codeLen and not dnbExpSeq[startPos:startPos+codeLen] == 'N'*codeLen:
                        if fullDNBObsSeq[startPos:startPos+codeLen] == dnbExpSeq[startPos:startPos+codeLen]:
                            rawMap  = 1
                        else:
                            correctedMap = 1
                    
                    mappingStr += str(rawMap)+","+str(correctedMap)+","
                    startPos += codeLen
                    
                fieldFH.write(fullDNBObsSeq + "," + dnbExpSeq + "," + mappingStr + ",".join(["%s" % el for el in dnbScore]) + "\n")
            
            # check first partial RS code to see if segment is mapped or unmapped
            firstPartial = dnbObsSeq[0:RSCodeLen/2]
            if recoverNmer:
                # increment no-call count if all positions in first 5-mer are no-called
                if firstPartial.count('N') == RSCodeLen/2:
                    fieldMetricsOBJ.incrementTotalNoCallDNBSegment(1,0)

                # if adjacent 10-mer is unmapped or no-called, then first 5-mer is unmapped, if it's not no-called
                adj10mer = dnbObsSeq[RSCodeLen/2:RSCodeLen/2+RSCodeLen]
                if fullDNB20mers[1].count('N') == RSCodeLen*2:
                    if firstPartial.count('N') < RSCodeLen/2:
                        fieldMetricsOBJ.incrementUnmappedDNBSegment(1,0)
                    
                    # increment unmapped or no-called counts for adjacent 10-mer
                    if adj10mer.count('N') >= 1:
                        fieldMetricsOBJ.incrementTotalNoCallDNBSegment(1,1)
                        NoCallCount+=1
                        noCallCount += 1
                    else:
                        fieldMetricsOBJ.incrementUnmappedDNBSegment(1,1)
                        unmappedCount += 1
                elif firstPartial.count('N') < RSCodeLen/2:
                    # if first 5-mer is not no-called or unmapped, determined whether it is raw mapped or error corrected mapped
                    if firstPartial == dnbExpSeq[0:RSCodeLen/2]:
                        fieldMetricsOBJ.incrementTotalRawMappedDNBSegment(1,0)
                    else:
                        fieldMetricsOBJ.incrementTotalCorrectedDNBSegment(1,0)
                
            else:
                if firstPartial.find('N') == -1:
                    if firstPartial == dnbExpSeq[0:RSCodeLen/2]:
                        fieldMetricsOBJ.incrementTotalRawMappedDNBSegment(1,0)
                    else:
                        fieldMetricsOBJ.incrementUnmappedDNBSegment(1,0)
                else:
                    fieldMetricsOBJ.incrementTotalNoCallDNBSegment(1,0)
                    
            # check last partial RS code to see if segment is mapped or unmapped
            lastPartial = dnbObsSeq[DNB_Len-(RSCodeLen/2):DNB_Len]
            if recoverNmer:
                # increment no-call count if all positions in last 5-mer are no-called
                if lastPartial.count('N') == RSCodeLen/2:
                    fieldMetricsOBJ.incrementTotalNoCallDNBSegment(1,numRSCodes-1)
                
                # if adjacent 10-mer is unmapped or no-called, then last 5-mer is unmapped, if it's not no-called
                adj10mer = dnbObsSeq[DNB_Len-RSCodeLen-(RSCodeLen/2):DNB_Len-(RSCodeLen/2)]
                if fullDNB20mers[numFullRSCodes].count('N') == RSCodeLen*2:
                    if lastPartial.count('N') < RSCodeLen/2:
                        fieldMetricsOBJ.incrementUnmappedDNBSegment(1,numRSCodes-1)
                        
                    # increment unmapped or no-called counts for adjacent 10-mer
                    if adj10mer.count('N') >= 1:
                        fieldMetricsOBJ.incrementTotalNoCallDNBSegment(1,numRSCodes-2)
                        noCallCount += 1
                    else:
                        fieldMetricsOBJ.incrementUnmappedDNBSegment(1,numRSCodes-2)
                        unmappedCount += 1
                elif lastPartial.count('N') < RSCodeLen/2:
                    # if last 5-mer is not no-called or unmapped, determined whether it is raw mapped or error corrected mapped
                    if lastPartial == dnbExpSeq[DNB_Len-(RSCodeLen/2):DNB_Len]:
                        fieldMetricsOBJ.incrementTotalRawMappedDNBSegment(1,numRSCodes-1)
                    else:
                        fieldMetricsOBJ.incrementTotalCorrectedDNBSegment(1,numRSCodes-1)
            
            else:
                if lastPartial.find('N') == -1:
                    if lastPartial == dnbExpSeq[DNB_Len-(RSCodeLen/2):DNB_Len]:
                        fieldMetricsOBJ.incrementTotalRawMappedDNBSegment(1,numRSCodes-1)
                    else:
                        fieldMetricsOBJ.incrementUnmappedDNBSegment(1,numRSCodes-1)
                else:
                    fieldMetricsOBJ.incrementTotalNoCallDNBSegment(1,numRSCodes-1)
                    
            # if data recovery flag is set, check adjacent 10-mers to recover data
            if recoverNmer:
                for index in range(1,numFullRSCodes/2):
                    i = index*2
                    code1start = (i*2-1) * 5
                    obsCode1 = dnbObsSeq[code1start:code1start+RSCodeLen]
                    code2start = ((i+1)*2-1) * 5
                    obsCode2 = dnbObsSeq[code2start:code2start+RSCodeLen]
                    code1 = fullDNB20mers[i]
                    code2 = fullDNB20mers[i+1]
                    
                    # if adjacent 10-mers are both completely no-called, increment no-call counts for each 10mer
                    if code1.count('N') == RSCodeLen*2 and code2.count('N') == RSCodeLen*2:
                        if obsCode1.count('N') > 0:
                            fieldMetricsOBJ.incrementTotalNoCallDNBSegment(1,i)
                            noCallCount += 1
                        else:
                            fieldMetricsOBJ.incrementUnmappedDNBSegment(1,i)
                            unmappedCount += 1
                                
                        if obsCode2.count('N') > 0:
                           fieldMetricsOBJ.incrementTotalNoCallDNBSegment(1,i+1)
                           noCallCount += 1
                        else:
                            fieldMetricsOBJ.incrementUnmappedDNBSegment(1,i+1)
                            unmappedCount += 1
                    # if first 10-mer is good and second 10-mer is unmapped/no-called, set expected seq for second 
                    # 10-mer to second half of 20-mer mapped by first 10-mer
                    elif code1.count('N') == 0 and code2.count('N') == RSCodeLen*2:
                        dnbExpSeq = dnbExpSeq[0:code2start] + code1[RSCodeLen:RSCodeLen*2] + dnbExpSeq[code2start+RSCodeLen:]
                        correctedMappedCount += 1
                        fieldMetricsOBJ.incrementTotalCorrectedDNBSegment(1,i+1)
                    
                    # if second 10-mer is good and first 10-mer is unmapped/no-called, set expected seq for first 
                    # 10-mer to first half of 20-mer mapped by second 10-mer
                    elif code1.count('N') == RSCodeLen*2 and code2.count('N') == 0:
                        dnbExpSeq = dnbExpSeq[0:code1start]+code2[0:RSCodeLen]+dnbExpSeq[code1start+RSCodeLen:]
                        correctedMappedCount += 1
                        fieldMetricsOBJ.incrementTotalCorrectedDNBSegment(1,i)
            
            if noCallCount == numFullRSCodes:
                fieldMetricsOBJ.incrementTotalNoCallDNBs(1)
                fieldMetricsOBJ.setDuplicate(dnb.getId().getOffsetInLane(), field.getIndexWithinLane(), field.size())
            elif unmappedCount == numFullRSCodes:
                fieldMetricsOBJ.setDuplicate(dnb.getId().getOffsetInLane(), field.getIndexWithinLane(), field.size())
                fieldMetricsOBJ.incrementUnmappedDNBs(1)
            elif rawMappedCount + correctedMappedCount > 0:
                fieldMetricsOBJ.incrementAveScoreCount(dnbScore,dnbObsSeq, dnbExpSeq)
                fieldMetricsOBJ.calculateDuplication(dnb.getId().getOffsetInLane(), dnbExpSeq, field.getIndexWithinLane(), field.size())
                if rawMappedCount == numFullRSCodes:
                    fieldMetricsOBJ.incrementTotalRawMappedDNBs(1)
                elif correctedMappedCount + rawMappedCount == numFullRSCodes:
                    fieldMetricsOBJ.incrementTotalCorrectedDNBs(1)
                elif noCallCount >= 1:
                    fieldMetricsOBJ.incrementTotalPartialCalledDNBs(1)
                else:
                    fieldMetricsOBJ.incrementTotalPartialMappedDNBs(1)
            else:
                if noCallCount >= 1:
                    fieldMetricsOBJ.incrementTotalPartialCalledDNBs(1)
                else:
                    fieldMetricsOBJ.incrementTotalPartialMappedDNBs(1)
        
        # if writing seq data for fields, close field file
        if len(fieldLST) > 0:
            fieldFH.close()
            
        if not checkRSCodeConsistency(fullDNB20mers):
            print "WARNING: inconsistent 10-mers\n"
            print fullDNB20mers
        
        # calculate accuracy for field
        laneMetricsOBJ.incrementLaneData(fieldMetricsOBJ)
        field = field.getName()
        col = field[3:6]
        row = field[6:9]
        fieldMetricsOBJ.printMetrics(fieldOutFH,curPath,slideID,laneID,field,row,col,cycleStr,fieldDBFH,commentData)
        fieldMetricsOBJ.printDBConfMatrixData(fieldDBFH)
    
        # release field from memory
        dnb.dontNeedRelease()
        
    # print lane level metrics
    if completedFields > -1:
        laneMetricsOBJ.printMetrics(outFH,curPath,slideID,laneID,len(fields),1,1,cycleStr,laneDBFH,commentData)
        
        # print lane subset data
        outputFile = outputDir + "/" + slideID + "_" + laneID + "_" + cycleStr + "." + str(ArrID) + ".temp.csv"
        laneDataFH = open(outputFile, "w")
        laneMetricsOBJ.printLaneData(laneDataFH)
        laneDataFH.close()
        
    outFH.close()
    fieldOutFH.close()
    fieldDBFH.close()
    laneDBFH.close()
    
    

################################
if __name__ == "__main__":
    main(sys.argv)
