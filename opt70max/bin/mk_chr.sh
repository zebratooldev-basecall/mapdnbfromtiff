#!/bin/bash

set -x

cnvdir=$1
width=$2
chr="$3"
LIST=`find -L ${cnvdir}/contig_data -name "${chr}_*.${width}.gz" | awk -F_ '{print substr($NF,7),$0}' | sort -n | awk '{print $2}'`

echo found $LIST

if [ "${LIST}" == "" ]; then
    echo "empty LIST"
    exit 10
else
    first=`echo $LIST | awk '{print $1}'`
    zcat $first | head -n 1  > ${cnvdir}/smoothed_${width}/${chr}_header
    zcat $LIST | grep -v '>' | cat ${cnvdir}/smoothed_${width}/${chr}_header - | gzip -c > ${cnvdir}/smoothed_${width}/${chr}_${width}SlidingWindow.tsv.gz
    /bin/rm ${cnvdir}/smoothed_${width}/${chr}_header
fi
