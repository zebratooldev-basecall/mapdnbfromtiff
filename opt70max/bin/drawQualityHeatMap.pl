#!/bin/env perl
use warnings;
use strict;
use SVG;
use SVG::FontSize;
use Getopt::Long;
use Data::Dumper;
#use File::Basename;
sub usage{
    print STDERR <<USAGE;
    ################################################
            Version 1.0 by Vincent-Li   2013.12.19

      Usage: $0 <score_list> <prefix> [option] >prefix.svg

      Option:
       -m <str>: The DnbMap file to specify Dnb dnbStructure. [Default 2AD/2AD-LFR/1AD structure]
       -t <str>: Type of score in input file. [BIC]
       -s <str>: Specify the structure to use. [auto detect by cycle number(only for 2AD and 1AD)]
       -c <\@str>: Use custom score cutoff instead of default. 
                   [BIC:(100,78), FIT:(1,0.5), DUP:(0,12)]
       -d      : Display score value for every position.

    ################################################
USAGE
    exit;
}

############# global variable
my ($dnbMapFile, $scoreType, $dnbStructure, $DisplayScore, %laneInfo, @posMap, @scoreCutoff);
my $defaultMapDir = '/rnd/home/liyuxiang/development/04.qualityPlot/01.DnbMap';


############# Collect parameters
GetOptions(
  "m=s"=>\$dnbMapFile,
  "t=s"=>\$scoreType,
  "s=s"=>\$dnbStructure,
  "d"=>\$DisplayScore,
  "c=i{2}"=>\@scoreCutoff,
);

############# Check parameters
&usage if(@ARGV !=2);
my ($scoreList,  $prefix)=@ARGV;

$scoreType ||= 'BIC';
my %defaultScoreCutoff = ('BIC'=>[100,78], 'FIT'=>[1,0.5], 'DUP'=>[0,12]);

######## Color:  green:#00E25A   yellow:#F1DA36   red:#FF3A3A
my @colorArr = ([0,226,90], [241,218,54], [225,58,58]);
my @gradientColor = map { 'rgb('.(join ',', @{$_}).')' } @colorArr ;
@scoreCutoff = @{$defaultScoreCutoff{$scoreType}} if(@scoreCutoff == 0);

############################  BEGIN MAIN  ############################
&readScoreList($scoreList, \%laneInfo);

if (!defined $dnbMapFile) {
    $dnbStructure = &readLenCheck(\%laneInfo) if(!defined $dnbStructure);
    $dnbMapFile = "$defaultMapDir/${dnbStructure}_DnbPos.txt";
}

if(!-f $dnbMapFile or @scoreCutoff == 0){

}

my ($totalBase, $gapLen) = &readDnbMapFile(\@posMap, \%laneInfo, $dnbMapFile);


&drawSvg();


######## If -m NOT set, check cycle number to determind the file to use
############################   END  MAIN  ############################

###############  sub  ###############
#############################
#
#     readScoreList
#
#  Read score list, store the
# information of Slidelane,
# Position and Score.
#
#############################
sub readScoreList {
    my ($listFile, $lane_p) = @_;

    open IN, "<$listFile" or die("Can't read file:$listFile\n");
    while(my $line=<IN>){
        next if($line=~/^#/);
        my @info = split /\s+/, $line;

        ######## example:  
        ######## Format:   hash->{Slidelane}{DnbPosition}  = Score
        $lane_p->{$info[0]}{$info[1]} = $info[2];
    }
    close IN;
}

#############################
#
#     readDnbMapFile
#
#  read DnbMap file to get
# the releationship of DnbPos
# and lanePos.
#
#############################
sub readDnbMapFile {
    my ($posMap_p, $lane_p, $mapFile) = @_;

    my $pointer = -1;
    my $totalNumber = 0;
    my $gapSize = 0;

    open IN, "<$mapFile" or die("Can't read file:$mapFile\n");
    while(my $line=<IN>){

        #### Format: #DnbPos ReadPos AchPos  IsBarcode
        #########      1       1     A01301      0
        my @info = split /\s+/, $line;
        if($line=~/^#/){
            $gapSize = $info[2] if($line=~/gap/);
            next;
        }
        
        $pointer++ if($info[1] == 1);
        $totalNumber++;

        push @{$posMap_p->[$pointer]}, \@info;
    }
    close IN;
    return ($totalNumber, $gapSize);
}

#############################
#
#     readLenCheck
#
#  Detect the Dnb type by cycle
# number.
#
#############################
sub readLenCheck {
    my ($data_p) = @_;

    my %existType;
    my %lenRecord;

    foreach my $k (keys %{$data_p}) {
        my $cycleNumber = keys %{$data_p->{$k}};
        $lenRecord{$k} = $cycleNumber;

        if ($cycleNumber >= 72) {
            $existType{'2AD'}++;
        }elsif ($cycleNumber >= 50) {
            print STDERR "Unknown type of DNB with lanegth: $cycleNumber in $k\n";
            exit;
        }elsif($cycleNumber >= 41){
            $existType{'1AD'}++;
        }elsif($cycleNumber >= 38){
            $existType{'1AD-N6'}++;
        }else{
            print STDERR "Unknown type of DNB with lanegth: $cycleNumber in $k\n";
            exit;
        }
    }

    if(keys %existType >1){
        print STDERR "Exist multiple types of DNB:\n";
        print STDERR "#Slidelane\tCyclesNumber\n";
        map { print STDERR "$_\t$lenRecord{$_}\n" } sort keys %lenRecord ;
        exit;
    }

    return (keys %existType)[0]
}

#$svg->title('id','document-title','-cdata','This is the title');
#$svg->circle('cx',400,'cy',200,'r',100,'stroke','red','fill','green');
#$svg->ellipse('cx',200,'cy',200,'rx',100,'ry',50,'stroke','red','fill','green');
#$svg->line('x1',0,'y1',0,'x2',100,'y2',100,'stroke','red','stroke-width',3);
#$svg->rect('x',10, 'y',20,'rx',20,'ry',20,'width',40,'height',50,'stroke','red','fill','green');
#$svg->text('x',400,'y',400,'stroke','red','fill','red','-cdata','hello, world','font-size',20);
#$svg->polygon('points',[0,0,100,0,50,50],'fill','red','stroke','green');
#$svg->tag('circle', 'cx',400,'cy',200,'r',100,'stroke','red','fill','green');
#$svg->tag('path', 'd', "M 0 0 L 100 0 L50 100 Z");
#############################
#
#     drawSvg
#
#  Create a svg object
#
#############################
sub drawSvg{

    my $blockWidth = 40;
    my $blockHeight = int($blockWidth*0.618);
    my $gapWidth = $blockWidth*2;
    my $laneGap = 20;
    my $stx = 200;
    my $sty = 120;
    my $svgWidth = $stx + $totalBase*$blockWidth + $gapWidth*2 + 20;
    my $svgHeight = $sty + (keys %laneInfo) * ($blockHeight + $laneGap) + 30;

    my $svg = SVG->new('width',$svgWidth,'height',$svgHeight);
    my $font = FontSize->new();
    my $svgMiddle = int($svgWidth/2);

    ###### define gradient example
    my $exampleWidth = 200;
    my $gradient = $svg->gradient('-type','linear','id',"grad",'x1','0','x2','1','y1','1','y2','1');
    $gradient->stop('offset','0','style',"stop-color:$gradientColor[0]");
    $gradient->stop('offset','0.5','style',"stop-color:$gradientColor[1]");
    $gradient->stop('offset','1','style',"stop-color:$gradientColor[2]");
    $svg->rect('x',$stx, 'y',$sty/2,'width',$exampleWidth,'height',$blockHeight,'stroke','none','fill',"url(#grad)",'opacity', 1);
    &Accurate_text($svg, $scoreCutoff[0], 20, $stx, $sty/2 - 9, 'black', 'middle');
    &Accurate_text($svg, $scoreCutoff[1], 20, $stx + $exampleWidth, $sty/2 - 9, 'black', 'middle');


    # $svg->rect('x',0, 'y',0,'width',3000,'height',3000,'stroke','none','fill','blue','opacity', 0.1);
    &Accurate_text($svg, $scoreType, 30, $svgMiddle, $sty/2, 'black', 'middle');

    
    my $drawPosNumber = 1;
    foreach my $k (sort keys %laneInfo) {
        &Accurate_text($svg, $k, 20, $stx - 10, $sty + $blockHeight/2, 'black', 'end');
        
        my $tx = $stx - $gapWidth;
        foreach my $p (@posMap) {
            my $thisGroup = $svg->group('style',{'stroke','black','stroke-width',1});
            $tx += $gapWidth;

            foreach my $bk (@{$p}) {
                ######### If this is the first lane, draw position number on the head
                if($drawPosNumber){
                    &Accurate_text($svg, $bk->[1], 15, $tx+$blockWidth/2, $sty-9, 'black', 'middle');
                }

                ######### calculate the color of quality and fill the blank
                my $thisCol = 'white';
                $thisCol = &colorCalculater($laneInfo{$k}{$bk->[2]}) if(defined $laneInfo{$k}{$bk->[2]});
                $thisGroup->rect('x',$tx, 'y',$sty, 'width',$blockWidth,'height',$blockHeight,'fill',"$thisCol");

                ######### show score value if set -d
                if($DisplayScore){
                    my $scoreText = defined $laneInfo{$k}{$bk->[2]} ? $laneInfo{$k}{$bk->[2]} : 'N';
                    &Accurate_text($thisGroup, $scoreText, 12, $tx+$blockWidth/2, $sty+$blockHeight/2, 'black', 'middle');
                }
                $tx += $blockWidth;
            }
        }
        ###### only display once
        $drawPosNumber = 0;
        $sty += $blockHeight + $laneGap;
    }



    open OUT,">$prefix.svg" or die("$!\n");
    print OUT $svg->xmlify();
}

#############################
#
#     colorCalculater
#
#  Calculate the color of filling
# block by gradient formula
#
#############################
sub colorCalculater {
    my ($score) = @_;

    ########### Set the value to the border of scorecutoff
    my ($min, $max) = sort {$a<=>$b} @scoreCutoff;
    $score = $max if($score > $max);
    $score = $min if($score < $min);

    ########### calculate the distance to one side, to design the block of gradient
    my $scoreLen = abs($scoreCutoff[0] - $scoreCutoff[1]);
    my $scoreDistance = abs($score - $scoreCutoff[0]);

    ###### The score is more close to scoreCutoff[0]
    my @stCol;
    my @edCol;
    if($scoreDistance <= $scoreLen/2){
        @stCol = @{$colorArr[0]};
        @edCol = @{$colorArr[1]};
    }else{
        @stCol = @{$colorArr[1]};
        @edCol = @{$colorArr[2]};
        $scoreDistance -= $scoreLen/2;
    }
    my @newCol = map { $stCol[$_] + int(($edCol[$_] - $stCol[$_])*($scoreDistance/$scoreLen*2)) } 0..2 ;
    return 'rgb('.join(',', @newCol).')';
}


#############################
#
#     Accurate_text  Ver0.2  2013.05.22
#
#  Draw a text by accurate position.
#
# 
# svg_p:  svg object.
# text:   main text.
# size:   size of font.
# pos_x:  X position of font, controled by margin.
# pos_y:  Y position of font mid.
# color:  color of text. [black]
# margin: margin start, end or middle. [start]
#
# eg. &Accurate_text($svg, "text", 20, $pos_x, $pos_y, 'black', 'm');
#
#############################
sub Accurate_text {
    my ($svg_p, $text, $size, $pos_x, $pos_y, $text_color, $margin) = @_;

    my $font_family = 'Arial';
    $margin ||='start';
    $text_color ||= 'black';
    $pos_y = $pos_y + $size/4 + 1;

    my $attr;

    if($margin eq 'end' or $margin=~/^e/i){
        $attr = 'end';
    }elsif($margin eq 'middle' or $margin=~/^m/i){
        $attr = 'middle';
    }
    if($attr){
        $svg_p->text('x',$pos_x,'y',$pos_y,'stroke','none','fill',"$text_color",'-cdata',"$text",'font-size',$size, 'text-anchor', $attr);
    }else{
        $svg_p->text('x',$pos_x,'y',$pos_y,'stroke','none','fill',"$text_color",'-cdata',"$text",'font-size',$size);
    }
    return 1;
}

################## God's in his heaven, All's right with the world. ##################
