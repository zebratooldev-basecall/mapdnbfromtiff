#!/usr/bin/python
import os,sys
import optparse
import numpy
import matplotlib
matplotlib.use('Agg')
import pylab as P

def reviseSexProfiles(chr,XhapStart,XhapEnd,profiles,multiplier):
    
    n = len(multiplier)

    f = open(profiles)
    for l in f:
        if chr != "chrX":
            print l.rstrip()
            continue
        w = l.rstrip().split(",")
        if int(w[1]) < XhapStart or int(w[1]) >= XhapEnd:
            print l.rstrip()
            continue
        nw = len(w)
        if nw-3 != n:
            print >> sys.stderr, "Unexpected mismatch between entries in profiles and gender-map based multipliers"
            sys.exit(-1)
        lineout = ",".join([w[0],w[1],w[2]])
        for i in range(3,nw):
            lineout += "," + str(float(w[i])*multiplier[i-3])
        print lineout
        
    f.close()
            
    
    
def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(chr='',XhapStart='',XhapEnd='',genderMap='',inProfiles='')
    parser.add_option('--chr', dest='chr',
                      help='chromosome analyzed in this execution')
    parser.add_option('--XhapStart', dest='XhapStart',
                      help='starting point of haploid portion of male chrX')
    parser.add_option('--XhapEnd', dest='XhapEnd',
                      help='ending point of haploid portion of male chrX')
    parser.add_option('--genderMap', dest='genderMap',
                      help='file containing mapping of asm ID to boolean (male=true)')
    parser.add_option('--inProfiles', dest='inProfiles',
                      help='file containing profile data to revise')

    (options, args) = parser.parse_args()
    if len(args) == 0:
        parser.error('must specify at least one profile file to work on')

    if not options.chr:
        parser.error('chr specification required')
    if not options.XhapStart:
        parser.error('XhapStart specification required')
    if not options.XhapEnd:
        parser.error('XhapEnd specification required')
    if not options.genderMap:
        parser.error('genderMap specification required')
    if not options.inProfiles:
        parser.error('inProfiles specification required')


    gM = open(options.genderMap)
    gMap = {}
    for l in gM:
        w = l.rstrip().split()
        gMap[w[0]]=w[1]
    gM.close()

    multipliers = []
    for a in args:
        asm = a.split("/")[-2]
        if gMap[asm] == "1":
            multipliers += [2]
        else:
            multipliers += [1]
    
    reviseSexProfiles(options.chr,int(options.XhapStart),int(options.XhapEnd),options.inProfiles,multipliers)


if __name__ == '__main__':
    main()
    
