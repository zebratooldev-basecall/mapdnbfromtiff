#!/usr/bin/env python

import os
import optparse
from cgiutils import bundle
import wfclib.jsonutil
from refconstants import GlobalRefConstants

parser = optparse.OptionParser("usage: %prog [options]")
parser.add_option("-w", "--work-dir", dest="workDir",
                  default = "./test", help="directory that will be set as the workflow work directory [default: %default]")
parser.add_option("-g", "--gold-dir", dest="goldDir",
                  default = "./gold", help="directory that contains gold data [default: %default]")
parser.add_option("-u", "--update-gold", dest="copyGold",
                  action = "store_true", help="update gold data")
parser.add_option("-n", "--workflow-name", dest="workflowName",
                  default = "test", help="name of the new workflow [default: %default]")
parser.add_option("-x", "--overrides", dest="overrides",
                  default = "smallSIM_100K", help="workflow overrides. Predefined configs: smallSIM_100K,SmokeTest,SmokeTestWithArc,male36,female36,male37,female37. [default: %default]")
parser.add_option("-f", "--workflow-file", dest="workflowFile",
                  default = "pipelineTest.py", help="name of the workflow file [default: %default]")
parser.add_option("-c", "--workflow-class", dest="workflowClass",
                  default = "PipelineTest", help="name of the workflow class [default: %default]")
parser.add_option("-r", "--ref-dir", dest="refRootDir",
                  default = "/prod/pv-01/pipeline/REF/", help="reference root directory [default: %default]")
parser.add_option("-m", "--email", dest="emailResultsTo",
                  default = "", help="send email at the end of execution to the list of users")
parser.add_option("-s", "--scrat", dest="scratCommand",
                  default = "gnoll", help="SCRAT client execution command. For example: 'rat', 'gnoll', 'rat -s lxv-testscrat01:8080'")
(options, args) = parser.parse_args()
if len(args) != 0:
    parser.error("unexpected arguments: "+args)

globalRefConstants = GlobalRefConstants(options.refRootDir)
if options.overrides=='smallSIM_100K':
    sampleType = "chr2_10m_10_1m"
    refConstants = globalRefConstants[sampleType]
    overrides = '{"referenceRootDir":"%s",'\
                '"sim":{"sampleType":"%s", "coverage":50, "simGenomeConfig":"NoMutations", "simGenomeEditInput":"%s", "laneSizeInBases":1000000} }'\
                        %(options.refRootDir,sampleType,os.path.join(options.refRootDir,refConstants.refId,refConstants.simGenomeInVariations))
elif options.overrides=='SmokeTest':
    sampleType = "smoketest1"
    refConstants = globalRefConstants[sampleType]
    overrides = '{"referenceRootDir":"%s",'\
                '"sim":{"sampleType":"%s", "coverage":50, "laneSizeInBases":1000000} }'\
                        %(options.refRootDir, sampleType)
elif options.overrides=='SmokeTestWithArc':
    sampleType = "smoketest1"
    archiveDir = os.path.abspath(os.path.join(options.workDir,'Archive'))
    if not os.path.exists(archiveDir):
        os.makedirs(archiveDir)
    refConstants = globalRefConstants[sampleType]
    overrides = '{"referenceRootDir":"%s",'\
                '"export":{"forceCopyToArchive":"true", "archiveLocation":"%s"},'\
                '"sim":{"sampleType":"%s", "coverage":50, "laneSizeInBases":1000000} }'\
                        %(options.refRootDir, archiveDir, sampleType)
elif options.overrides=='male36':
    sampleType = "male"
    refConstants = globalRefConstants[sampleType]
    print options.refRootDir,refConstants.refId,refConstants.simGenomeInVariations
    overrides = '{"referenceRootDir":"%s",'\
                '"sim":{"sampleType":"%s", "coverage":50, "laneSizeInBases":3000000000} }'\
                        %(options.refRootDir,sampleType)
elif options.overrides=='female36':
    sampleType = "female"
    refConstants = globalRefConstants[sampleType]
    overrides = '{"referenceRootDir":"%s",'\
                '"sim":{"sampleType":"%s", "coverage":50, "laneSizeInBases":3000000000} }'\
                        %(options.refRootDir,sampleType)
elif options.overrides=='male37':
    sampleType = "male37"
    refConstants = globalRefConstants[sampleType]
    overrides = '{"referenceRootDir":"%s",'\
                '"sim":{"sampleType":"%s", "coverage":50, "laneSizeInBases":3000000000} }'\
                        %(options.refRootDir,sampleType)
elif options.overrides=='female37':
    sampleType = "female37"
    refConstants = globalRefConstants[sampleType]
    overrides = '{"referenceRootDir":"%s",'\
                '"sim":{"sampleType":"%s", "coverage":50, "laneSizeInBases":3000000000} }'\
                        %(options.refRootDir,sampleType)
else:
    overrides = options.overrides 

print "load overrides:\n",overrides
inpState = wfclib.jsonutil.loads(overrides)

INIT = bundle(test=bundle())
INIT.test.goldRoot = os.path.abspath(options.goldDir)
INIT.test.copyGold = options.copyGold

if options.emailResultsTo:
    INIT.test.emailResultsTo = options.emailResultsTo

INIT.rupdate(inpState)

mergedOverrides = wfclib.jsonutil.json.dumps(INIT, indent=0).replace("\n","")

commandLine = " ".join([
  options.scratCommand, 
  'create',
  options.workflowName,
  options.workflowFile,
  '-w '+options.workDir,
  "-x '%s'" % mergedOverrides,
  '-c '+options.workflowClass
])

print 'running: ',commandLine
os.system(commandLine)
#'ag create test100kb$1 /Proj/Users/vkarpinchyk/src/main/inst/opt70max/etc/workflows/onestep/pipelineTest.py -w ./test100kb$1 -x '{"referenceRootDir":"'$WORK_DIR/REF'","sim":{"sampleType":"chr2_10m_10_1m", "coverage":50, "simGenomeConfig":"NoMutations", "simGenomeEditInput":"'$WORK_DIR/Variations20-40_ccf100_chr2_100kb.tsv'", "laneSizeInBases":100000000}, "test":{"goldRoot":"/Proj/Users/vkarpinchyk/data/export/ShortQA/out100kb", "copyGold":'$COPY_GOLD'} }' -c PipelineTest'
