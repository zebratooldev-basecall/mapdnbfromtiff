#!/usr/bin/python
import signal, os, sys, optparse, traceback, logging, shutil
import wfclib
from wfclib import bundle, jsonutil, Stage
import cgiutils

SET_ENV_SCRIPT = \
"""
export CGI_HOME=%s
export PATH=$CGI_HOME/bin:/usr/local/bin:/bin:/usr/bin
export LD_LIBRARY_PATH=$CGI_HOME/bin
export PYTHONPATH=$CGI_HOME/bin
"""

BASH_SET_TRACE_ON = 'set -x\n'

BASH_CHECK_STATUS_COMMAND = \
"""RC=$?
set +x
if [ $RC != 0 ]; then
    exit $RC
fi
"""

class StepScript(object):
    def __init__(self, name, commands, arrayParamValues=[],
                 workflowTasksPerGridTask=1):
        self.name = name
        self.commands = isinstance(commands, str) and [ commands ] or commands
        self.array = bool(arrayParamValues)
        self.workflowTasksPerGridTask = workflowTasksPerGridTask
        self._createArrayParamValuesDict(arrayParamValues)
        self._validateParamValues()

    def _createArrayParamValuesDict(self, arrayParamValues):
        if len(arrayParamValues) == 0:
            self.arrayParamValues = arrayParamValues
        elif type(arrayParamValues[0]) == str:
            self.arrayParamValues = [ {"PARAM":val} for val in arrayParamValues ]
        else:
            self.arrayParamValues = arrayParamValues

    def _validateParamValues(self):
        for val in self.arrayParamValues:
            if not isinstance(val, dict):
                raise RuntimeError('step %s: expected type of arrayParamValues is [ {"key":"value"} ]"' % self.name)
            for key in val:
                if not type(key) == str:
                    raise RuntimeError('step %s: arrayParamValues must contain string keys' % self.name)
                if not type(val[key]) == str:
                    raise RuntimeError('step %s: arrayParamValues must contain string values' % self.name)
                if key not in self.arrayParamValues[0]:
                    raise RuntimeError('step %s: arrayParamValues must contain same sets of keys' % self.name)
            for key in self.arrayParamValues[0]:
                if key not in val:
                    raise RuntimeError('step %s: arrayParamValues must contain same sets of keys' % self.name)

    def _shellEscape(self, ss):
        return ss.replace('"', '\\"')

    def _addShellArrayProlog(self, lines):
        lines.append('#$ -t 1-%d' % self.taskCount())
        lines.append('export CGI_ARRAY_PARAM_NAMES=\'%s\'' % ':'.join(self.arrayParamValues[0].keys()))
        for key in self.arrayParamValues[0].keys():
            lines.append( key + '_vals=( "" ' +
                           ' '.join(['"' + self._shellEscape(p[key]) + '"' for p in self.arrayParamValues]) + \
                           ' )' )
            lines.append( 'export %s=${%s_vals[$SGE_TASK_ID]}' % (key,key) )

    def _addShellArrayMultiProlog(self, lines):
        lines.append( 'WORKFLOW_TASK_ID_START=$(( ($SGE_TASK_ID-1)*%d/%d+1 ))' %
                      (len(self.arrayParamValues),self.taskCount()) )
        lines.append( 'WORKFLOW_TASK_ID_END=$(( ($SGE_TASK_ID)*%d/%d ))' %
                      (len(self.arrayParamValues),self.taskCount()) )
        lines.append( 'for WORKFLOW_TASK_ID in $(seq $WORKFLOW_TASK_ID_START $WORKFLOW_TASK_ID_END); do' )
        for key in self.arrayParamValues[0].keys():
            lines.append( 'export %s=${%s_vals[$WORKFLOW_TASK_ID]}' % (key,key) )

    def _addShellArrayMultiPostlog(self, lines):
        lines.append( 'done' )

    def taskCount(self):
        if not self.array:
            return 1
        count = len(self.arrayParamValues) / self.workflowTasksPerGridTask
        if len(self.arrayParamValues) % self.workflowTasksPerGridTask != 0:
            count += 1
        return count

    def make(self, cgiHome):
        """Creates script to run the commands of this task as an SGE job"""
        lines = []
        lines.append('#!/bin/bash')
        lines.append('ulimit -c 0')
        lines.append('umask 027')
        lines.append('set -o pipefail')

        lines.append('echo "task slots: ${CGI_CPU_COUNT}"')
        lines.append('echo cpu info:')
        lines.append('egrep -e "model name|cpu MHz|bogomips|cache size" /proc/cpuinfo | head -n 4')
        lines.append('echo SGE_TASK_ID: $SGE_TASK_ID')

        # set the environment to completely ignore user-specified path
        # to avoid picking up unexpected executables
        lines.append(SET_ENV_SCRIPT % cgiHome)

        if self.array:
            self._addShellArrayProlog(lines)

        if self.array and self.workflowTasksPerGridTask > 1:
            self._addShellArrayMultiProlog(lines)

        lines.append('START_TIME_STAMP=`date --utc +%s`')

        for c in self.commands:
            lines.append(BASH_SET_TRACE_ON)
            lines.append(c)
            lines.append(BASH_CHECK_STATUS_COMMAND)

        lines.append('END_TIME_STAMP=`date --utc +%s`')
        lines.append('echo "step elapsed time in seconds: $((END_TIME_STAMP-START_TIME_STAMP))"')

        if self.array and self.workflowTasksPerGridTask > 1:
            self._addShellArrayMultiPostlog(lines)

        return '\n'.join(lines)

def write_service_script(service, incompleteCommand, options):
    script = open(service.scriptFile, 'w')
    print >>script, '#!/bin/bash'
    print >>script, (SET_ENV_SCRIPT % options.cgiHome)
    print >>script, "ulimit -c 0"
    print >>script, "set -o pipefail"
    print >>script, incompleteCommand + ' \\'
    print >>script, '--data-chunk-list=${CGI_DATA_CHUNK_LIST} \\'
    print >>script, '--service-ready-file=${CGI_OUTPUT_FILE_NAME}'
    script.close()

def write_file(fn, data):
    f = open(fn, 'w')
    try: print >>f, data
    finally: f.close()

def make_scrat_step(step, options):
    fullName = '%s.%s.%s' % (options.workflow, options.stage, step.name)

    scriptMaker = StepScript(step.name, step.commands,
                             step.arrayParamValues,
                             step.get('workflowTasksPerGridTask', 1))

    r = bundle()
    r.name = step.name
    r.scriptFile = os.path.join(options.scriptDir, fullName + '.script')
    r.taskCount = scriptMaker.taskCount()
    r.logDir = scriptMaker.array and os.path.join(options.logDir, fullName) or options.logDir
    r.slotHog = step.hog
    r.memory = int(step.memory * 1024)
    r.resources = step.resources
    r.priority = step.priority
    r.concurrencyLimit = step.concurrencyLimit

    deps = isinstance(step.depends, str) and step.depends.split(',') or step.depends
    deps = [ x.strip() for x in deps ]
    r.prereqs = ','.join(deps)

    write_file(r.scriptFile, scriptMaker.make(options.cgiHome))

    return r

def convert_actions_to_scrat(steps, services, notifications, options):
    result = bundle(services=[], steps=[], notifications=[])

    for ss in services:
        fullName = '%s.%s.%s' % (options.workflow, options.stage, ss.name)
        service = bundle(name=ss.name, concurrencyLimit=ss.concurrencyLimit)
        service.scriptFile = os.path.join(options.scriptDir, fullName + '.script')
        service.logDir = os.path.join(options.logDir, fullName)
        service.data = [ bundle(description=d, memory=m)
                            for d, m in ss.serviceData ]
        service.slotHog = ss.hog
        write_service_script(service, ss.serviceCommand, options)
        result.services.append(service)

    for ss in steps:
        step = make_scrat_step(ss, options)
        result.steps.append(step)

    for nn in notifications:
        result.notifications.append(nn)

    allLogDirs = set([x.logDir for x in result.services] + [x.logDir for x in result.steps])
    for ld in allLogDirs:
        cgiutils.makedirs(ld)

    return result

def run_callback(workflowClass, input, options, debug=False):
    logging.basicConfig(level=logging.INFO,
                        filename=None,
                        format='%(levelname)3s %(asctime)s.%(msecs)03d %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    input.workDir = options.workDir
    input.cgiHome = options.cgiHome
    if hasattr(workflowClass, 'MULTIPLEX') and \
            options.stage not in [ s.name for s in workflowClass.STAGES ]:
        strand = options.stage[-1]
        if strand not in input:
            raise RuntimeError('The last character of the stage "%s" '
                               'does not represent an assembly strand. Is it a non-multiplexed stage?' % options.stage)
        state = input[strand]
        method = 'impl' + options.stage[:-1]
        state.workDir = os.path.join(input.workDir, strand)
        state.originalWorkDir = os.path.join(input.originalWorkDir, strand)
        state.cgiHome = options.cgiHome
        state.shared = input.shared
        state.shared.all = input
        newState, actions = wfclib.runStageCallback(workflowClass.MULTIPLEX,
                                                    method, state, debug)
        del newState['shared']['all']
        del newState['shared']
        input[strand] = newState
        newState = input
    else:
        method = 'impl' + options.stage
        newState, actions = wfclib.runStageCallback(workflowClass, method, input, debug)
    # track which stage used which state file, for rat clone
    if not 'stageInputStates' in newState:
        newState['stageInputStates'] = []
    stageState = bundle(stage=options.stage, state=options.input)
    newState['stageInputStates'].append(stageState)
    return newState, actions

def run_actions(actions, options, state):
    for d in actions.allocDirectories:
        if not d.startswith(state.workDir+'/'):
            raise RuntimeError('allocAndMkdir directory %s not in workDir %s' % (d, state.workDir))
        drel = d[len(state.workDir)+1:]
        while drel.startswith('/'):
            drel = drel[1:]
        if not os.path.isdir(d):
            baseDir = state.workDir
            if hasattr(state, 'workDirList'):
                if not hasattr(state, 'workDirIdx'):
                    state.workDirIdx = -1
                state.workDirIdx = (state.workDirIdx+1) % len(state.workDirList)
                baseDir = state.workDirList[state.workDirIdx]
            if baseDir != state.workDir:
                cgiutils.makedirsandsymlink(os.path.join(baseDir, drel), d)
            else:
                cgiutils.makedirs(d)
    for d in actions.directories:
        cgiutils.makedirs(d)
    for fn, data in actions.files.iteritems():
        write_file(fn, data)

    return convert_actions_to_scrat(actions.steps, actions.services, actions.notifications, options)


def delete_work_directories(workDirList):
    for workDir in workDirList:
        if os.path.lexists(workDir):
            shutil.rmtree(workDir)


def get_strand_names(count):
    return [ chr(ord('A') + i) for i in range(count) ]

def multiplex_dependencies(depStr, strand, strands, syncStages, globalStages):
    deps = depStr and depStr.split(',') or []
    newdeps = []
    for d in deps:
        if '.' in d:
            stage, step = d.split('.')
            if stage in [s.name for s in globalStages]:
                newdeps.append(d)
            elif strand is None or stage in syncStages:
                newdeps += [stage + p + '.' + step for p in strands]
            else:
                newdeps.append(stage + strand + '.' + step)
        else:
            if d in [s.name for s in globalStages]:
                newdeps.append(d)
            elif strand is None or d in syncStages:
                newdeps += [d + p for p in strands]
            else:
                newdeps.append(d + strand)
    return ','.join(newdeps)

def multiplex_stages(stages, syncStages, globalStages, count):
    strands = get_strand_names(count)
    result = []
    for s in stages:
        for strand in strands:
            newdeps = multiplex_dependencies(s.depends, strand, strands,
                                             syncStages, globalStages)
            result.append(Stage(s.name + strand, newdeps))
    for s in globalStages:
        newdeps = multiplex_dependencies(s.depends, None, strands,
                                         syncStages, globalStages)
        result.append(Stage(s.name, newdeps))
    return result

def multiplex_init_state(initState, count):
    strands = get_strand_names(count)
    r = bundle()
    for strand in strands:
        r[strand] = initState.rcopy()
        r[strand].currentSubWorkflow = strand
    return r

def multiplex_global_vars(state, count):
    strandSet = frozenset(get_strand_names(count))
    # globa variables are those not in one of the strand private areas
    gvars = state.copy()
    for strand in strandSet:
        del gvars[strand]
    try: del gvars['shared']
    except KeyError: pass
    # clone them into every private area
    for strand in strandSet:
        state[strand].rupdate(gvars)

def determine_multiplex_count(workflowClass, state):
    if hasattr(workflowClass, 'subWorkflowCount'):
        return workflowClass.subWorkflowCount(state)
    else:
        count = 0
        for i in range(100):
            name = chr(ord('A') + i)
            if name not in state:
                break
            count += 1
        return count

def create_first_state(workflowClass, inputState, overrides, options):
    adjustedState = inputState.rcopy()
    adjustedState.rupdate(overrides)
    adjustedState.workDir = options.workDir
    adjustedState.cgiHome = options.cgiHome
    if hasattr(workflowClass, 'MULTIPLEX'):
        strandClass = workflowClass.MULTIPLEX
        initState = wfclib.combineInitBundles(strandClass)
        count = determine_multiplex_count(workflowClass, adjustedState)
        if count < 1:
            raise RuntimeError('failed to determine the count of sub-workflows')
        state = multiplex_init_state(initState, count)
        state.shared = bundle(subWorkflowCount=count)
        state.rupdate(adjustedState)
        multiplex_global_vars(state, count)
    else:
        count = 0
        state = wfclib.combineInitBundles(workflowClass)
        state.shared = bundle(subWorkflowCount=count)
        state.rupdate(adjustedState)
    state.stageInputStates = []
    return state

def validate_stages(stages):
    stageNames = [ stage.name for stage in stages ]
    for stage in stages:
        if stage.depends is not None:
            for depend in stage.depends.split(','):
                if depend.split('.')[0] not in stageNames:
                    raise RuntimeError('stage %s depends on stage %s which does not exist' %
                                       ( stage.name, depend.split('.')[0] ))

def get_workflow_stages(workflowClass, firstState):
    if hasattr(workflowClass, 'MULTIPLEX'):
        stages = workflowClass.MULTIPLEX.STAGES
        return multiplex_stages(stages,
                                workflowClass.SYNC_STAGES,
                                workflowClass.STAGES,
                                firstState.shared.subWorkflowCount)
    else:
        return workflowClass.STAGES

def main(options):
    haveParams = options.workflow and options.op and options.cgiHome and \
                 options.module and options.workflowClass and options.stage and \
                 options.input and options.output and options.workDir and \
                 options.results
    if not haveParams:
        raise RuntimeError('missing mandatory option')

    options.scriptDir = os.path.join(options.workDir, 'scripts')
    options.logDir = os.path.join(options.workDir, 'logs')
    assert os.path.isdir(options.workDir) # must be created by scratd
    assert os.path.isdir(options.logDir) or options.op == 'delete'
    cgiutils.makedirs(options.scriptDir)

    mod = cgiutils.loadModule(options.module)
    workflowClass = getattr(mod, options.workflowClass)

    if options.overrideFile:
        f = open(options.overrideFile, 'r')
        try: options.overrideText = f.read()
        finally: f.close()
    else:
        options.overrideText = None

    if options.input:
        f = open(options.input, 'r')
        try: data = f.read()
        finally: f.close()
        input = jsonutil.loads(data)
    else:
        input = bundle()

    if options.op == 'init':
        if options.overrideText:
            try:
                overrides = jsonutil.loads(options.overrideText)
            except ValueError, e:
                raise RuntimeError('bad override syntax: %s' % e)
        else:
            overrides = bundle()
        firstState = create_first_state(workflowClass, input, overrides, options)
        reply = bundle(stages=get_workflow_stages(workflowClass, firstState))
        validate_stages(reply.stages)
        jsonutil.dump(firstState, options.output)
    elif options.op == 'call':
        assert input
        state, actions = run_callback(workflowClass, input, options)
        reply = run_actions(actions, options, state)
        if options.output:
            jsonutil.dump(state, options.output)
    elif options.op == 'delete':
        if options.overrideText:
            try:
                overrides = jsonutil.loads(options.overrideText)
            except ValueError, e:
                raise RuntimeError('bad override syntax: %s' % e)
        else:
            overrides = bundle()
        state = input.rcopy()
        state.rupdate(overrides)
        state.workDir = options.workDir
        state.cgiHome = options.cgiHome
        workDirList = []
        if hasattr(state, 'workDirList') and state.workDirList:
            workDirList = state.workDirList
            if options.workDir not in workDirList:
                raise RuntimeError('workDir not in state.workDirList: %s' % options.workDir)
        else:
            workDirList = [ options.workDir ]
        delete_work_directories(workDirList)
        reply = bundle()

    jsonutil.dump(reply, options.results)

def wfc_signal_handler(signum, frame):
    print 'Signal handler called with signal', signum
    raise RuntimeError('Signal handler called with signal '+str(signum))

# INIT action.
#    Create default state, override with state from input file, write
#    resulting state to output file, write list of stages to stdout.
# CALL action.
#    Read state from input file, call the method, write state to output
#    file, write description of necessary actions to stdout.
if __name__ == '__main__':

    # Set the signal handlers so that we get a stack trace.
    signal.signal(signal.SIGBUS, wfc_signal_handler)
    signal.signal(signal.SIGSEGV, wfc_signal_handler)
    signal.signal(signal.SIGILL, wfc_signal_handler)
    signal.signal(signal.SIGFPE, wfc_signal_handler)

    opts = optparse.OptionParser('usage: %prog [options]')
    opts.set_defaults()
    opts.add_option('--workflow', help='name of the workflow')
    opts.add_option('--op', type='choice', choices=['init', 'call', 'delete'], help='action to execute')
    opts.add_option('--cgi-home', dest='cgiHome', help='CGI_HOME value for the jobs')
    opts.add_option('--module', help='path to the python module')
    opts.add_option('--class', dest='workflowClass', help='name of the workflow class')
    opts.add_option('--stage', help='stage to execute the callback for')
    opts.add_option('--input', help='path to the input state file')
    opts.add_option('--output', help='path to the output state file')
    opts.add_option('--work-dir', dest='workDir', help='working directory of the workflow')
    opts.add_option('--results', help='execution result data')
    opts.add_option('--override-file', dest='overrideFile', help='file with overrides')

    (options, args) = opts.parse_args()
    if len(args) != 0:
        opts.error('unexpected arguments')

    try:
        main(options)
    except cgiutils.Abort, ae:
        write_file(options.results, str(ae))
        traceback.print_exc()
        print '%s' % ae # print the message from the Abort exception as is
        sys.exit(1)
    except Exception, e:
        write_file(options.results, '%s: %s' % (e.__class__.__name__, e))
        traceback.print_exc()
        sys.exit(1)
