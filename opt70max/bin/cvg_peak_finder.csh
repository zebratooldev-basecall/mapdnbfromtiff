#!/bin/tcsh

### Identify peaks in the coverage distribution and produce some diagnostic plots.  
### Preliminary data manipulation is done with awk; the bigger computation and generation of plots is done with R.

set workdir=$1
set incvgfile=$2
# if there is a third argument and its value is not 0, this is a smoketest run, which gets special treatment
set isSmallTest=$3

### for small tests, stub behavior -- write out a file with some default peaks
if ( $isSmallTest != 0 && $isSmallTest != "" ) then
  echo | awk '{for(i=0;i<4;i++)print 2+i*22}' > ${workdir}/cvgpeaks-prplot
  exit
endif

### number of lines in coverage file
set n=`cat  ${workdir}/$incvgfile|wc -l`

### determine highest coverage value we want to consider
set topcvg=`awk '{print $4}' ${workdir}/$incvgfile | sort -n | awk -v n=$n 'NR==int(n*.999)+1{print $1+5;done=1;exit}END{if(done!=1)print $1}' `

### compute coverage histogram
awk -v topcvg=$topcvg '$1!="chrM"{if($4>topcvg)$4=topcvg;a[int($4*10)]++}END{for(i=0;i<=topcvg*10;i++)print i,a[i]*1}' ${workdir}/$incvgfile > ${workdir}/cvghisto_inclSex

### convert histogram into cumulative curve
awk '{a+=$2;print $1,a}' ${workdir}/cvghisto_inclSex > ${workdir}/cvgcdf_inclSex

### replace blank count values with 0
awk 'NF==1{$2=0}{print}' ${workdir}/cvghisto_inclSex > ${workdir}/cvgcounts_inclSex


### Using R, identify peaks in two different ways
### -- the first uses the simple histogram and a simple-minded method that finds local peaks
### -- the second uses a quantile-function transformation and a more involved method to find non-trivial peaks
### In both cases, use two smoothing steps (smooth() and LOESS()) to smooth the data before looking for peaks.
### This double-smoothing seems to work reasonably well, but is admittedly a hack.
###
### Only the second version is used later in the pipeline.  Both are computed to allow comparison in cases where
### the results are surprising

R --no-save --no-restore --no-init-file  <<EOF
# read in coverage histogram
d<-as.matrix(read.table("${workdir}/cvgcounts_inclSex"))

# set the width of LOESS smoothing
sp<-0.035
if ( length(d[,1]) < 200 ) {
    sp<-20/length(d[,1])
}
if (sp>.5){
  sp<-.5
}

# smooth coverage data
s<-smooth(d[,2])
l<-loess(s~d[,1],sp=0.05,degree=1)

# identify peaks and produce plots
png(file="${workdir}/loess_histo.png")
ld<-length(d[,1])
plot(1:ld,d[,2],"l",xlim=c(1,ld))
lines(1:ld,l\$fitted+10,"l",col="red")
xhisto<-vector()
for (i in 2:(ld-1)){
  if ( (l\$fitted[i-1] < l\$fitted[i]) && (l\$fitted[i] > l\$fitted[i+1]) ){
    xhisto<-append(xhisto,d[i,1]/10)
    points(d[i,1],s[i],col="cyan")
  }
}
xhisto
dev.off()

# write out peaks from first method
write(xhisto,ncolumns=1,"${workdir}/cvgpeaks-histo")


# read in cumulative data
c<-as.matrix(read.table("${workdir}/cvgcdf_inclSex"))

# convert to cdf
n<-max(c[,2])
Ctmp<-c[,2]/n

# trim ends
f<-(Ctmp>0 & Ctmp<1)
C<-Ctmp[f]

# coverage value 'coordinates' of trimmed vectors
coord<-c[f,1]

# compute normal distribution quantile function
q<-qnorm(C)
qlen<-length(q)

# compute deltas of quantile function
Q<-q[2:qlen]-q[1:qlen-1]

# determine width of LOESS smoothing
sp<-0.03
if ( length(Q) < 200 ) {
    sp<-20/length(Q)
}
if ( length(Q) > 1000 ){
    sp<- 30/length(Q)
}
if (sp>.5){
  sp<-.5
}

s<-smooth(Q)
L<-loess(s~coord[2:qlen],span=sp,degree=1)

# create plot of quantile function data, with LOESS fit
png(file="${workdir}/loess_prplot.png")
plot(coord[2:qlen],Q,"l",xlim=c(1,coord[qlen]))
lines(coord[2:qlen],L\$fitted,col="red")

# write out fitted curves, for manual review
write(l\$fitted,file="${workdir}/fitted_histo")
write(L\$fitted,file="${workdir}/fitted_prplot")

### PEAK-FINDING PROCESS
##### Starting from the highest point, iteratively look for lower and lower peaks, lowering the 'waterline'.
##### As the waterline drops, declare new peaks as the highest point on an island when 
##### that peak is at least delta above the waterline

# index values of peaks
peakIdxs<-vector()

# magic number determining whether a peak is trivial or not
delta<-.001


# convert to dataframe and sort by descending fitted delta-quantile
fit<-L\$fitted
l<-length(fit)
idx<-c(1:l)
df<-data.frame(idx,fit)
dfs<-df[with(df,order(-fit)),]

# to avoid redundant computation, we record positions that we do not need to process again
seen<-rep(FALSE,l)

# process all positions in descending order of fitted value
for(i in 1:l){

   # x is the index in unsorted distribution
   x = dfs[i,1]

   ### CORE LOGIC
   # for each next-highest unprocessed position ...
   ### If we have already seen the position, skip further processing
   ### consider positions leftward one at a time
   ###### if the leftward position is at least delta lower, leftSuccess=TRUE
   ###### if the leftward position has already been seen, stop leftward processing
   ###### if the leftward position is lower than any we have seen, update lowest and remember position
   ###### if the leftward position is at least delta higher than the lowest position we have considered,
   ######    we stop processing leftward
   ###### if we reach the first position, remember first position and leftSuccess=TRUE
   ### consider positions rightward one at a time in symmetrical fashion
   ### if leftSuccess and rightSuccess, add starting position to peaks list
   ### mark between left and right remembered positions as 'seen'

   # if we have already processed this position enough, we can skip it
   if ( seen[x] ){
      next
   }


   # fitted value of current position 
   y = dfs[i,2]

   # do leftward processing
   j=x-1
   leftSuccess=FALSE
   if(x==1){ leftSuccess=TRUE }
   minLeft=y
   minLeftLoc=x
   seenLeft=x
   while(j >= 1){
	if ( df[j,2]<=minLeft){
	   if ( df[j,2]+delta < y ){
              leftSuccess=TRUE
           }
           minLeft=df[j,2]
           minLeftLoc=j
        }
        # if we have seen this position before, stop looking to the left
        if(seen[j]){
           seenLeft=j+1
           break
	}
        # if we have gone back up enough higher than min, stop looking to left
	if(minLeft+delta < df[j,2]){
	   seenLeft=minLeftLoc	   
           break
        }
        # if we reach all the way to the left, consider it a success
        if(j==1){
           leftSuccess=TRUE
           seenLeft=1
           break
        }
        j=j-1
   }

   # do rightward processing
   j=x+1
   rightSuccess=FALSE
   if(x==l){ leftSuccess=TRUE }
   minRight=y
   minRightLoc=x
   seenRight=x
   while(j<=l){
	if ( df[j,2]<=minRight){
	   if ( df[j,2]+delta < y ){
              rightSuccess=TRUE
           }
           minRight=df[j,2]
           minRightLoc=j
        }
        if(seen[j]){
           seenRight=j-1
           break
	}
	if(minRight+delta < df[j,2]){
	   seenRight=minRightLoc	   
           break
        }
        if(j==l){
           rightSuccess=TRUE
           seenRight=l
           break
        }
        j=j+1
   }

   # mark positions between seenLeft and seenRight, inclusive, as seen
   for(k in seenLeft:seenRight){
        seen[k]=TRUE
   }

   # if current position is the highest point on island defined by waterline delta below height of current position, 
   # declare it a peak; store the index position
   if(leftSuccess && rightSuccess){
      peakIdxs=append(peakIdxs,x)
   }

}

# convert index positions to coverage values
x<-vector()
for(i in 1:length(peakIdxs)){
   x<-append(x,coord[peakIdxs[i]]/10)
   points(coord[peakIdxs[i]],fit[peakIdxs[i]],col="cyan")
}
X<-sort(x)
X
dev.off()

write(X,ncolumns=1,"${workdir}/cvgpeaks-prplot")

EOF
