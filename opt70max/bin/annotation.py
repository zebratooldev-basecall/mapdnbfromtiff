#!/usr/bin/python
import re

'''Searchable store of annotated intervals of the genome.
Each interval is a 4-tuple of (chrNo, begin, end, payload).
'''
class Annotation(object):
    class Node(object):
        def __init__(self, interval):
            self.left = None
            self.right = None
            self.interval = interval
            self.maxEnd = (0, 0)
            pass

    def __init__(self, intervals):
        intervals.sort()
        self.root = self.create(intervals, 0, len(intervals))

    def create(self, ints, b, e):
        if b >= e:
            return None
        mid = (b + e) / 2
        n = Annotation.Node(ints[mid])
        n.left = self.create(ints, b, mid)
        n.right = self.create(ints, mid + 1, e)
        n.maxEnd = max( (n.interval[0], n.interval[2]),
                         not n.left and (-1, -1) or n.left.maxEnd,
                         not n.right and (-1, -1) or n.right.maxEnd)
        return n

    def overlaps(self, r1, r2):
        if r1[0] != r2[0]:
            return False
        return r1[1] < r2[2] and r1[2] > r2[1]

    def searchNode(self, range, node, result):
        if not node:
            return

        # print node.interval, node.maxEnd

        # check if the interval begins to the right of the rightmost
        # end of this subtree, in which case there will be no results
        if range[:2] >= node.maxEnd:
            return
        # check nodes to the left
        if node.left:
            self.searchNode(range, node.left, result)
        # check this node itself
        if self.overlaps(range, node.interval):
            result.append(node.interval)
        # if the range ends before this node, then it's not going to overlap
        # anything to the right
        if (range[0], range[2]) < node.interval[:2]:
            return
        if node.right:
            self.searchNode(range, node.right, result)

    def searchPoint(self, chrom, offset):
        r = []
        self.searchNode((chrom, offset, offset+1), self.root, r)
        return r

    def searchRange(self, range):
        r = []
        self.searchNode(range, self.root, r)
        return r

    def printNode(self, node, depth):
        if not node:
            return
        self.printNode(node.left, depth+1)
        print ' '*depth, node.interval, node.maxEnd
        self.printNode(node.right, depth+1)

CHROM_REGEX = re.compile(r'^chr([XYM123456789][\d]?)$')
def chromNo(chrom):
    match = CHROM_REGEX.match(chrom)
    if not match:
        raise ValueError(chrom)
    c = match.groups()[0]
    if c == 'X':
        return 23
    elif c == 'Y':
        return 24
    elif c == 'M':
        return 25
    ic = int(c)
    if ic < 1 or ic > 22:
        raise ValueError(chrom)
    return ic

def loadAnnotation(filename, columns=(0, 1, 2, 3), sep='\t'):
    '''Load annotations from a file. Field numbers for chromosome, begin-end
    offsets and the payload are passed through columns parameter. Chromosome
    field is expected to be in the form chrZZ, and the lines with all other
    chromosome field contents are ignored. Offsets are expected to be one-based,
    inclusive-end'''
    needFieldCount = max(columns) + 1

    intervals = []

    f = open(filename, 'r')
    for line in f:
        data = line.split(sep)
        if len(data) < needFieldCount:
            continue
        try:
            chrom = chromNo(data[columns[0]])
            begin = int(data[columns[1]]) - 1
            end = int(data[columns[2]])
        except ValueError:
            continue
        payload = data[columns[3]]
        if begin >= end:
            continue
        intervals.append((chrom, begin, end, payload))
    f.close()

    tree = Annotation(intervals)
    return tree

if __name__ == '__main__':
    print 'loading data...'
    x = loadAnnotation('/Proj/Assembly/Shared/wormhole-annotations/build36/segdup.txt', columns=(1, 2, 3, 4))
    print 'searching...'
    print x.searchRange((1, 2576599-1, 2576600+1))
    print x.searchPoint(1, 2576599)
    x.printNode(x.root.left.left.left.left, 0)


if False:
    x = Annotation([
                        (1, 10, 20, 'a'),
                        (1, 15, 20, 'b'),
                        (1, 10, 15, 'c'),
                        (1, 10, 16, 'd'),
                        (2, 10, 20, 'e'),
                        (2, 10, 20, 'f'),
                        (2, 10, 20, 'g'),
                        (2, 10, 20, 'h'),
                        (3, 11, 21, 'i')
                    ])
    x.printNode(x.root, 0)
    print x.searchPoint(2, 15)
    print x.searchRange((1, 5, 30))
    print x.searchPoint(3, 10)
    print x.searchPoint(3, 21)
    print x.searchPoint(3, 11)
    print x.searchPoint(3, 20)
