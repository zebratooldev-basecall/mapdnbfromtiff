#!/usr/bin/env python

import sys,os
import optparse
from cnvwindow import WholeGenomeCNVWindowIterator
from gbifile import GbiFile 
from refconstants import GlobalRefConstants
from cStringIO import StringIO
import bz2
import gzip

def consume_header(fin):
    while True:
        l = fin.readline()
        if not l:
            break
        if l[0] == '>':
            w = l.rstrip('\n').split('\t')
            if l.rstrip('\n') != '>offset\trefScore\tuniqueSequenceCoverage\tweightSumSequenceCoverage\tgcCorrectedCoverage\tgrossWeightSumSequenceCoverage':
                raise Exception('unexpected fields header:\n' + l.rstrip())
            return
    raise Exception('did not find fields header')


def processWindows(inCvgDir,gbifilename,output,winshift,winwidth,asmId,chr,refType,referenceRootDir):

    globalRefConstants = GlobalRefConstants(referenceRootDir)
    ignoreRanges = globalRefConstants[refType].cnvIgnoreRegions
    gbi = GbiFile(gbifilename)
    cnvWins = WholeGenomeCNVWindowIterator()
    cnvWins.initialize(gbi,ignoreRanges,winshift,winwidth)

    if cnvWins.eof():
        raise Exception( 'unexpected end of cnv windows')

    fn = inCvgDir + '/coverageRefScore-' + chr + '-' + asmId + '.tsv.bz2'
    fin = bz2.BZ2File(fn,'r')
    consume_header(fin)
        
    fn = output + '/' + chr + '_' + str(winwidth) + 'SlidingWindow.tsv.gz'
    fout = gzip.open(fn,"wb")
    fout.write('>chr\tbegin\tend\tuniqueCoverage\tdnbCorrectedCoverage\tweightSumCoverage\tGC\tgcCorrectedCoverage\n')

    while not cnvWins.eof():
        win = cnvWins.next()
        if chr != win.chr_:
            continue

        print win.chr_ + ' ' + str(win.begin_) + ' ' + str(win.end_)

        uniqCvg=0
        wgtSumCvg=0
        gcCvg=0
        gcDenom = 0.0

        for i in range(win.begin_,win.end_):
            l = fin.readline()
            if not l:
                raise Exception( 'unexpected end of coverage file')
            w = l.rstrip('\n').split('\t')
            if int(w[0]) != i:
                raise Exception( 'unexpected offset ' + w[0] + ' for ' + chr)
            uniqCvg += int(w[2])
            wgtSumCvg += int(w[3])
            if w[4] != 'N':
                gcCvg += int(w[4])
                gcDenom += 1.0

        if gcDenom == 0:
            gcCvgFinal = -1.0
        else:
            gcCvgFinal = float(gcCvg)/gcDenom

        wlen = float(win.end_-win.begin_)
        fout.write('%s\t%d\t%d\t%.1f\t-1\t%.1f\t-1\t%.1f\n' % (chr,
                              win.begin_,
                              win.end_,
                              float(uniqCvg)/wlen,
                              float(wgtSumCvg)/wlen,
                              gcCvgFinal))

    l = fin.readline()
    if l:
        raise Exception( 'did not reach end of coverage data; first unprocessed line is:\n' + l)

    fin.close()
    fout.close()

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(output='',inCvgDir='',gbi='',refType='',
                        winShift='2000',winWidth='2000',asmId='',chr='')
    parser.add_option('-c', '--cvg-input', dest='inCvgDir',
                      help='input EXP REF directory')
    parser.add_option('--reference', dest='gbi',
                      help='gbi file name')
    parser.add_option('--reference-root-dir', dest='referenceRootDir',
                      help='referenceRootDir of the workflow')
    parser.add_option('--ref-type', dest='refType',
                      help='reference type (female, male37,...)')
    parser.add_option('--win-shift', dest='winShift',
                      help='size of window shift')
    parser.add_option('--win-width', dest='winWidth',
                      help='window width')
    parser.add_option('--assembly-id', dest='asmId',
                      help='assembly ID')
    parser.add_option('--chr', dest='chr',
                      help='chromosome to work on (chr1, ...)')
    parser.add_option('-o', '--output', dest='output',
                      help='output directory')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.inCvgDir:
        parser.error('cvg-input specification required')
    if not options.gbi:
        parser.error('gbi specification required')
    if not options.output:
        parser.error('output specification required')
    if not options.asmId:
        parser.error('assembly-id specification required')
    if not options.chr:
        parser.error('chr specification required')
    if not options.refType:
        parser.error('ref-type specification required')
    
    processWindows(options.inCvgDir,options.gbi,options.output,
                   int(options.winShift),int(options.winWidth),options.asmId,options.chr,options.refType, options.referenceRootDir)


if __name__ == '__main__':
    main()
