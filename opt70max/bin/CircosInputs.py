#!/bin/env python
import os,sys
import optparse
#from cnvwindow import WholeGenomeCNVWindowIterator
#from gbifile import GbiFile 
import os, re, datetime
from subprocess import Popen, PIPE
from os.path import join as pjoin
import glob
import bz2
import subprocess
from cgatools.util import DelimitedFile
from cgatools.util.misc import error
import string

def impactSort(a,b):
    # gene name
    if a[3] != b[3]:
        if a[3] < b[3]:
            return -1
        else:
            return 1
    # impact severity
    if a[2] != b[2]:
        return a[2] - b[2]
    # chromosome
    if a[0] != b[0]:
        if a[0] < b[0]:
            return -1
        else:
            return 1
    # begin position
    return int(a[1]) - int(b[1])
    

class CircosInputs:
    # number of bases per sample for histogram/density tracks
    windowWidth = 1000000
    
    # somatic score cutoff
    somaticCutoff = 30

    def __init__(self):
        pass
    
    def chrToLabel(self,c):
        if len(c) > 3 and c[:3] == 'chr':
            return 'hs' + c[3:]
        return c
    
    def createSomaticJunctionInputs(self,jcnDiff,workDir):
        jcnIn = DelimitedFile(jcnDiff)
        jcnIn.addField('Id')
        jcnIn.addField('LeftChr')
        jcnIn.addField('LeftPosition')
        jcnIn.addField('RightChr')
        jcnIn.addField('RightPosition')

        jcnOut = open(pjoin(workDir,'junctions.tsv'),'w')
    
        for (id,leftChr,leftPos,rightChr,rightPos) in jcnIn:
    
            color = "cgi7"
            if leftChr != rightChr:
                color = "cgi6"
            jcnOut.write('\t'.join(['junction'+id,self.chrToLabel(leftChr),leftPos,str(1+int(leftPos)),'color='+color]) + '\n')
            jcnOut.write('\t'.join(['junction'+id,self.chrToLabel(rightChr),rightPos,str(1+int(rightPos)),'color='+color]) + '\n')
            jcnOut.write('\n')
    
        jcnIn.close()
    
        jcnOut.close()


    
    def createInterChrJunctionInputs(self,jcnDiff,workDir):
        jcnIn = DelimitedFile(jcnDiff)
        jcnIn.addField('Id')
        jcnIn.addField('LeftChr')
        jcnIn.addField('LeftPosition')
        jcnIn.addField('RightChr')
        jcnIn.addField('RightPosition')

        jcnOut = open(pjoin(workDir,'interChrJunctions.tsv'),'w')
    
    
        for (id,leftChr,leftPos,rightChr,rightPos) in jcnIn:
            if leftChr != rightChr:
                color = "cgi6"
                jcnOut.write('\t'.join(['junction'+id,self.chrToLabel(leftChr),leftPos,str(1+int(leftPos)),'color='+color]) + '\n')
                jcnOut.write('\t'.join(['junction'+id,self.chrToLabel(rightChr),rightPos,str(1+int(rightPos)),'color='+color]) + '\n')
                jcnOut.write('\n')
    
        jcnIn.close()
    
        jcnOut.close()
    
    
    def createSnpDensityInputs(self,masterVar,workDir):
        mvIn = DelimitedFile(masterVar)
        mvIn.addField('chromosome')
        mvIn.addField('begin')
        mvIn.addField('end')
        mvIn.addField('zygosity')
        mvIn.addField('varType')
        mvIn.addField('allele1VarFilter')
        mvIn.addField('allele2VarFilter')

        hetCountFile = pjoin(workDir,'hetCount.tsv')
        homCountFile = pjoin(workDir,'homCount.tsv')
        hetCountOut = open(hetCountFile,'w')
        homCountOut = open(homCountFile,'w')
    
        rangeChr = ''
        rangeBeg = -1
        hetCount = 0
        homCount = 0

    
        for (mvChr,mvBeg,mvEnd,mvZyg,mvVT,mvA1VQ,mvA2VQ) in mvIn:
    
            if mvVT != 'snp' or \
               ( mvZyg != 'hom' and mvZyg != 'het-ref' ) or \
               mvA1VQ != "PASS" or \
               mvA2VQ != "PASS":
                continue
    
            if rangeChr == '' or rangeChr != mvChr or rangeBeg + self.windowWidth < int(mvBeg):
                if rangeChr != '':
                    chrLbl = self.chrToLabel(rangeChr)
                    hetCountOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeBeg+self.windowWidth),str(hetCount)])+'\n')
                    homCountOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeBeg+self.windowWidth),str(homCount)])+'\n')
                    rangeBeg += self.windowWidth
                    while rangeChr == mvChr and rangeBeg+self.windowWidth < int(mvBeg):
                        hetCountOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeBeg+self.windowWidth),'0'])+'\n')
                        homCountOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeBeg+self.windowWidth),'0'])+'\n')
                        rangeBeg += self.windowWidth
                    homCount = 0
                    hetCount = 0
    
                if rangeChr != mvChr:
                    rangeChr = mvChr
                    rangeBeg = int(mvBeg)
                    
            if mvZyg == 'hom':
                homCount += 1
            else:
                hetCount += 1
    
        mvIn.close()        
    
        if rangeChr != '':
            chrLbl = self.chrToLabel(rangeChr)
            hetCountOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeBeg+self.windowWidth),str(hetCount)])+'\n')
            homCountOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeBeg+self.windowWidth),str(homCount)])+'\n')
    
        hetCountOut.close()
        homCountOut.close()
    
        hetCountIn = open(hetCountFile,'r')
        maxCount = 1
        
        for l in hetCountIn:
            c = int(l.rstrip('\n').split('\t')[3])
            if c > maxCount:
                maxCount = c
        hetCountIn.seek(0)
        hetDensityOut = open(pjoin(workDir,"hetDensity.tsv"),'w')
        for l in hetCountIn:
            w = l.rstrip().split('\t')
            hetDensityOut.write('\t'.join([w[0],w[1],w[2],str(float(w[3])/float(maxCount))])+'\n')
        hetCountIn.close()
        hetDensityOut.close()
    
        homCountIn = open(homCountFile,'r')
        maxCount = 1
        for l in homCountIn:
            c = int(l.rstrip('\n').split('\t')[3])
            if c > maxCount:
                maxCount = c
        homCountIn.seek(0)
        homDensityOut = open(pjoin(workDir,"homDensity.tsv"),'w')
        for l in homCountIn:
            w = l.rstrip().split('\t')
            homDensityOut.write('\t'.join([w[0],w[1],w[2],str(float(w[3])/float(maxCount))])+'\n')
        homCountIn.close()
        homDensityOut.close()
        
    def createLOHInputs(self,lohSource,workDir):
        lohIn = open(lohSource,'r')
        lohDensityFile = pjoin(workDir,'lohDensity.tsv')
        lohDensityOut = open(lohDensityFile,'w')
    
        lohScale = float(self.windowWidth / 1000.)
    
        chrCol = -1
        begCol = -1
        classCol = -1
    
        rangeChr = ''
        rangeBeg = -1
        lohCount = 0
    
        l = lohIn.readline()
        w = l.rstrip().split("\t")
        if w[0] != 'SuperlocusId':
            print 'Unexpected header for calldiff superlocus output:\n\t' + l
            sys.exit(-1)
            
        for i in range(0,len(w)):
            x = w[i]
            if i == 0:
                x = w[i][1:]
            if x == 'Chromosome':
                chrCol = i
            if x == 'Begin':
                begCol = i
            if x == 'Classification':
                classCol = i
    
        for l in lohIn:
            w = l.rstrip().split("\t")
    
            if w[classCol] != 'ref-identical;onlyB' and w[classCol] != 'alt-identical;onlyA':
                continue
    
            if rangeChr == '' or rangeChr != w[chrCol] or rangeBeg + self.windowWidth < int(w[begCol]):
                if rangeChr != '':
                    chrLbl = self.chrToLabel(rangeChr)
                    lohDensityOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeBeg+self.windowWidth),str(float(lohCount)/lohScale)])+'\n')
                    rangeBeg += self.windowWidth
                    while rangeChr == w[chrCol] and rangeBeg+self.windowWidth < int(w[begCol]):
                        lohDensityOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeBeg+self.windowWidth),'0.0'])+'\n')
                        rangeBeg += self.windowWidth
                    lohCount = 0
    
                if rangeChr != w[chrCol]:
                    rangeChr = w[chrCol]
                    rangeBeg = int(w[begCol])
                    
            lohCount += 1
    
        lohIn.close()
    
        if rangeChr != '':
            lohDensityOut.write('\t'.join([self.chrToLabel(rangeChr), str(rangeBeg),str(rangeBeg+self.windowWidth),str(float(lohCount)/lohScale)])+'\n')
    
        lohDensityOut.close()
    
    
    def createLAFInputs(self,cnvDetails,workDir):
        lafIn = DelimitedFile(cnvDetails)
        lafIn.addField('chr')
        lafIn.addField('begin')
        lafIn.addField('end')

        if lafIn.hasField('bestLAFpaired') or lafIn.hasField('bestLAFsingle') or lafIn.hasField('bestLAF'):
            if lafIn.hasField('bestLAFpaired'):
                lafIn.addField('bestLAFpaired')
            else:
                if lafIn.hasField('bestLAFsingle'):
                    lafIn.addField('bestLAFsingle')
                else:
                    lafIn.addField('bestLAF')
        else:
            error('failed to find bestLAF* field in cnvDetails file %s', cnvDetails)

        if lafIn.hasField('lowLAFpaired') or lafIn.hasField('lowLAFsingle') or lafIn.hasField('lowLAF'):
            if lafIn.hasField('lowLAFpaired'):
                lafIn.addField('lowLAFpaired')
            else:
                if lafIn.hasField('lowLAFsingle'):
                    lafIn.addField('lowLAFsingle')
                else:
                    lafIn.addField('lowLAF')
        else:
            error('failed to find lowLAF* field in cnvDetails file %s', cnvDetails)

        if lafIn.hasField('highLAFpaired') or lafIn.hasField('highLAFsingle') or lafIn.hasField('highLAF'):
            if lafIn.hasField('highLAFpaired'):
                lafIn.addField('highLAFpaired')
            else:
                if lafIn.hasField('highLAFsingle'):
                    lafIn.addField('highLAFsingle')
                else:
                    lafIn.addField('highLAF')
        else:
            error('failed to find highLAF* field in cnvDetails file %s', cnvDetails)
    
        lafOut = open(pjoin(workDir,'laf.tsv'),'w')
        lafMinOut = open(pjoin(workDir,'lafMin.tsv'),'w')
        lafMaxOut = open(pjoin(workDir,'lafMax.tsv'),'w')

        rangeChr = ''
        rangeBeg = -1
        rangeEnd = -1
        laf = 0.0
        lafMin = 0.5
        lafMax = 0.0
    
        winCount = 0
        
    
        for (chr,beg,end,bestLAF,lowLAF,highLAF) in lafIn:
            if rangeChr == '' or rangeChr != chr or rangeBeg + self.windowWidth < int(beg):
                if rangeChr != '':
                    chrLbl = self.chrToLabel(rangeChr)
                    lafOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeEnd),str(laf/float(winCount))])+'\n')
                    lafMinOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeEnd),str(lafMin)])+'\n')
                    lafMaxOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeEnd),str(lafMax)])+'\n')
    
                winCount = 0
                laf = 0.0
                lafMin = 0.5
                lafMax = 0.0
                rangeChr = chr
                rangeBeg = int(beg)
    
            laf += float(bestLAF)
            if float(lowLAF) < lafMin:
                lafMin = float(lowLAF)
            if float(highLAF) > lafMax:
                lafMax = float(highLAF)
            
            winCount += 1
            rangeEnd = int(end)
    
        lafIn.close()
    
        if rangeChr != '':
            chrLbl = self.chrToLabel(rangeChr)
            lafOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeEnd),str(laf/float(winCount))])+'\n')
            lafMinOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeEnd),str(lafMin)])+'\n')
            lafMaxOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeEnd),str(lafMax)])+'\n')
    
    
        lafOut.close()
        lafMinOut.close()
        lafMaxOut.close()
    
    
    def createCNVLevelInputs(self,cnvSegments,workDir):
        segsIn = DelimitedFile(cnvSegments)
        segsIn.addField('chr')
        segsIn.addField('begin')
        segsIn.addField('end')
        segsIn.addField('relativeCvg')
        segsIn.addField('calledLevel')

        levelOut = open(pjoin(workDir,'calledLevel.tsv'),'w')
        relCvgOut = open(pjoin(workDir,'segRelCvg.tsv'),'w')
    
        for (chr,beg,end,relativeCvg,calledLevel) in segsIn:
            chrLbl = self.chrToLabel(chr)
            levelOut.write('\t'.join([chrLbl,beg,end,calledLevel])+'\n')
            relCvgOut.write('\t'.join([chrLbl,beg,end,relativeCvg])+'\n')
                            
        segsIn.close()
        levelOut.close()

        
    def createCNVPloidyInputs(self,cnvSegments,workDir):
        segsIn = DelimitedFile(cnvSegments)
        segsIn.addField('chr')
        segsIn.addField('begin')
        segsIn.addField('end')
        segsIn.addField('relativeCvg')
        segsIn.addField('calledPloidy')

        copyOut = open(pjoin(workDir,'calledPloidy.tsv'),'w')
        relCvgOut = open(pjoin(workDir,'segRelCvg.tsv'),'w')
    
        for (chr,beg,end,relativeCvg,calledPloidy) in segsIn:
            chrLbl = self.chrToLabel(chr)
            copyOut.write('\t'.join([chrLbl,beg,end,calledPloidy])+'\n')
            relCvgOut.write('\t'.join([chrLbl,beg,end,relativeCvg])+'\n')
                            
        segsIn.close()
        copyOut.close()

    
    def createSomaticVarTracks(self,somaticVar,workDir,useScoreInsteadOfFlag):
        somaticIn = DelimitedFile(somaticVar)
        somaticIn.addField('chromosome')
        somaticIn.addField('begin')
        somaticIn.addField('end')
        somaticIn.addField('allele1Gene')
        somaticIn.addField('allele2Gene')
        somaticIn.addField('fisherSomatic')
        somaticIn.addField('allele1VarFilter')




        somCountFile = pjoin(workDir,'somaticCount.tsv')
        somCountOut = open(somCountFile,'w')
        somGeneVarOut = open(pjoin(workDir,'somaticImpactedGenes.tsv'),'w')

        rangeChr = ''
        rangeBeg = -1
        somCount = 0
    
    
        impactMap = {'MISSENSE':0, 'DELETE':1, 'INSERT':2, 'DELETE+':3, 'INSERT+':4, 'NONSTOP':5, 'DISRUPT':6, 'FRAMESHIFT':7, 'NONSENSE':8, 'MISSTART':9 }
    
        impacted = []
        for (chr,beg,end,allele1Gene,allele2Gene,somaticScore,somaticValue) in somaticIn:
            if not somaticValue:
                continue
            if not somaticScore:
                continue
            
            if useScoreInsteadOfFlag:
                if (not somaticValue) or (float(somaticValue) >= self.somaticCutoff):
                    continue
            else:
                filters = somaticValue.split(";")
                foundFET = False
                for f in filters:
                    if string.find(f,"FET") != -1:
                        foundFET = True
                        break
                if foundFET:
                    continue
    
            if rangeChr == '' or rangeChr != chr or rangeBeg + self.windowWidth < int(beg):
                if rangeChr != '':
                    chrLbl = self.chrToLabel(rangeChr)
                    somCountOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeBeg+self.windowWidth),str(somCount)])+'\n')
                    rangeBeg += self.windowWidth
                    while rangeChr == chr and rangeBeg+self.windowWidth < int(beg):
                        somCountOut.write('\t'.join([chrLbl, str(rangeBeg),str(rangeBeg+self.windowWidth),'0'])+'\n')
                        rangeBeg += self.windowWidth
                    somCount = 0
    
                if rangeChr != chr:
                    rangeChr = chr
                    rangeBeg = int(beg)
                    
            somCount += 1
    
            if allele1Gene:
                u = allele1Gene.split(';')
                for i in range(0,len(u)):
                    v = u[i].split(':')
                    if v[-1] in impactMap:
                        impacted += [[self.chrToLabel(chr),beg,impactMap[v[-1]],v[2]]]
            if allele2Gene:
                u = allele2Gene.split(';')
                for i in range(0,len(u)):
                    v = u[i].split(':')
                    if v[-1] in impactMap:
                        impact = v[-1].rstrip('+').lower()
                        impacted += [[self.chrToLabel(chr),beg,impactMap[v[-1]],v[2]]]
    
        if rangeChr != '':
            somCountOut.write('\t'.join([self.chrToLabel(rangeChr), str(rangeBeg),str(rangeBeg+self.windowWidth),str(somCount)])+'\n')
    
        somaticIn.close()
        somCountOut.close()
    
        somCountIn = open(somCountFile,'r')
        somDensityOut = open(pjoin(workDir,'somaticDensity.tsv'),'w')
        maxCount = 1
        for l in somCountIn:
            w = l.rstrip('\n').split('\t')
            if int(w[3]) > maxCount:
                maxCount = int(w[3])
        somCountIn.seek(0)
        for l in somCountIn:
            w = l.rstrip('\n').split('\t')
            somDensityOut.write('\t'.join(w[0:3]+[str(float(w[3])/float(maxCount))])+'\n')
        somCountIn.close()
        somDensityOut.close()
    
        invertSeverity = {0:'missense', 1:'delete', 2:'insert', 3:'delete', 4:'insert', 5:'nonstop', 6:'disrupt', 7:'frameshift', 8:'nonsense', 9:'misstart' }
    
        impacted.sort(cmp=impactSort)
        pGene = ''
        pChr = ''
        pBeg = ''
        pIm = -1
        for v in impacted:
            if pGene != v[3]:
                if pGene != '':
                    somGeneVarOut.write('\t'.join([pChr,pBeg,str(int(pBeg)+1),pGene,'color='+invertSeverity[pIm]])+'\n')
                pChr = v[0]
                pBeg = v[1]
                pGene = v[3]
            pIm = v[2]
            
        if pChr != '':
            somGeneVarOut.write('\t'.join([pChr,pBeg,str(int(pBeg)+1),pGene,'color='+invertSeverity[pIm]])+'\n')
            
        somGeneVarOut.close()
    
    
    def createCircosConfs(self,confDir,etcDir,workDir,sampleType,plotType,refDir):
        if '+' in confDir:
            print 'conf-dir contains +; not handled, sorry!'
            sys.exit(-1)
        if '+' in etcDir:
            print 'etc-dir contains +; not handled, sorry!'
            sys.exit(-1)
        if '+' in sampleType:
            print 'sample-type contains +; not handled, sorry!'
            sys.exit(-1)
            
        typeconffiles = glob.glob(confDir +'/'+ plotType +'/*.conf')
        genericconffiles = glob.glob(confDir +'/*.conf')
        for f in typeconffiles + genericconffiles:
            leaf = f.split('/')[-1]
            if os.system('sed -e s:MYCONF:%s: -e s:MYETC:%s: %s > %s/%s' %
                         ( workDir, workDir, f, workDir, leaf) ):
                raise Exception('trouble modifing conf file ' + leaf)
        if os.system('cp -f %s/colors.conf %s' % (etcDir,workDir) ):
            raise Exception('trouble copying colors.conf')
        if os.system('cp -f %s/circos_karyotype.txt %s' % (refDir,workDir) ):
            raise Exception('trouble copying circos karyotype')

    def createCircosHtml(self,confDir,workDir,plotType,plotLabel):
        if '+' in confDir:
            print 'conf-dir contains +; not handled, sorry!'
            sys.exit(-1)
        if '+' in plotLabel:
            print 'plot-label contains +; not handled, sorry!'
            sys.exit(-1)
        htmltemplate = confDir +'/'+ plotType + '/circos.html'
        if os.system('sed -e s:MYLABEL:%s: %s > %s' % (plotLabel, htmltemplate, workDir+'/circos.html') ):
            raise Exception('trouble modifying circos.html')
        if os.system('cp -f %s/legend.png %s' % (confDir+'/'+plotType, workDir) ):
            raise Exception('trouble copying circos legend')
