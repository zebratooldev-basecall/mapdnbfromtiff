#!/usr/bin/env python

'''
Use this program to convert annotated vcf output
of trio workflow to standard loci file format.

Locus file is tab-separated, with header, in format
chr,offset,refAllele,altAllele

Output seqparate loci files for het-ref variants
'''

import sys
from optparse import OptionParser

def write_output(outbase,outputs):
    for vartype,outlines in outputs.iteritems():
        outfile = '%s_%s_loci.tsv' % (outbase,vartype)
        fo = open(outfile,'w')
        try:
            fo.write('>chr\toffset\tref\talt\n')
            for outline in outlines:
                fo.write('\t'.join(map(str,outline)) + '\n')
        finally:
            fo.close()

def preprocess_vcf(infile):
    outputs = {} ## outputs[vartype] = [(output),(output)...]

    fi = open(infile)
    try:
        for line in fi:
            if line[0] == '#':  continue
            if line[0] == '>':  continue
            if line == '\n':  continue

            # only want MIEs (denoted as 'BAD')
            if 'BAD' in line:                
                line = line.rstrip().split('\t')
                chr,pos,id,ref,alt = line[9:14]
                chr = 'chr' + chr
                zygosity, vartype = line[3:5]

                if zygosity == 'het-ref':
                    output = (chr,pos,ref,alt)
                    outputs[vartype] = outputs.setdefault(vartype,[])
                    outputs[vartype].append(output)
    finally:
        fi.close()

    return outputs 

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-i','--input-file', help='annotated vcf file from trio output', dest='infile')
    parser.add_option('-o','--output-base', help='base name for output file', dest='outbase')

    # parse command line arguments.
    options,args = parser.parse_args(arguments[1:])

    return options.infile, options.outbase

def main(args):
    infile, outbase = parse_arguments(args)
    
    outputs = preprocess_vcf(infile)

    write_output(outbase,outputs)

if __name__=='__main__':
    main(sys.argv)
