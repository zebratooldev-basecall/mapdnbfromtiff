#! /usr/bin/python

import sys
import os
from glob import glob
from scipy.stats import norm
import numpy as np

def getInfo(xml):
    try:
        file = open(xml, 'r')
    except:
        print "Error opening %s" %xml
    for line in file:
        line = line.strip()
        if "sample" in line:
            fields = line.split(" ")
            sample = fields[-1].rstrip(">").split("=")[-1].strip("\"")
            project = fields[-2].split("=")[-1].strip("\"")
        if line.startswith("<Library"):
            library = line.rstrip(">").split("=")[-1].strip("\"")
        elif line.startswith("<Slide"):
            slide = line.split("=")[-1].strip(">").strip("\"")
        elif line.startswith("<Lane DNB"):
            xlane = line.split(" ")[4].split("=")[-1].strip("\"")
    lane = slide+"-"+xlane
    return (project, sample, library, lane)

def getTag(cfg):
    tags = {}
    try:
        file = open(cfg, 'r')
    except:
        print "Error opening %s" %cfg
    for line in file:
        if line.startswith("Id"): continue
        fields = line.split(",")
        tags[fields[0]]=(fields[4], fields[5], fields[-1]) # (Tag, OrigTag, Well)
    return tags

#if len(sys.argv)<2:
#    print "Usage: $0 <GS*>"
#    exit(1)

tags = {}
workdir = sys.argv[1]

# tags = getTag(cfg) ## put into a different table
flfr = open('%s/lfr-stats.csv'%(workdir,), 'w')
llfr = open('%s/low-tags.csv'%(workdir,), 'w')

header = ["Project", "Sample", "Library", "Lane"]
for i in range(385):
    header.append("V"+str(i))

for i in range(385):
    header.append("V"+str(i)+"_normed")

flfr.write(",".join(header)+"\n")

for workdir in sys.argv[1:]:
    name = os.path.basename(workdir)
   
    info = ()    

    xml = glob("%s/ADF/*/*.xml"%(workdir,))
    if len(xml)==0: continue
    info = getInfo(xml[0])
    lane=info[-1]
    start = ",".join(info)
    flfr.write(start) 
    llfr.write(start)

    stat = "%s/ADF/%s/tagStatistics.csv" % (workdir,lane)
    fstat = open(stat, 'r')
    parse = 0
    dist = []
    for line in fstat:
	
        if line.startswith("Tag"):
	    parse = 1
	    continue
	if parse:
	    if not line.strip(): parse = 0; continue
	    id, count = line.strip().split(",")
	    flfr.write(","+count)
	    dist.append(float(count))
     
    darray=np.array(dist)
    mean=darray.mean() 
    for i in range(len(darray)):
	darray[i] = (darray[i]-mean)/mean
	
    nlfr=norm.cdf(darray)
    for i in range(len(nlfr)):	
	flfr.write(","+str(nlfr[i]))
	if nlfr[i] <= 0.25: llfr.write(",V"+str(i))
    llfr.write("\n")
    flfr.write("\n")
