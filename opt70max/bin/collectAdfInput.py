#!/usr/bin/python

import os, sys
from optparse import OptionParser
import wfclib

from collectAdfInputLib import *

def runMakeAdf(jsonDataPath, laneName, dnbDefinition):
    print "Running makeADF:"
    if dnbDefinition=="human":
        dnbStructure="A01:3:1-5 (-3,-1) A02:5:10-1 (0,2) A02:3:1-10 (4,7) A03:5:10-1 (300,700)A03:3:1-10 (4,7) A04:5:10-1 (0,2) A04:3:1-10 (-3,-1) A01:5:5-1"
    elif dnbDefinition=="2adaptor":
        raise Exception('2adaptor DNB structure is not supported')
    else:
        dnbStructure=dnbDefinition
        
    workDir = os.path.dirname(jsonDataPath)
    if not workDir:
        workDir = "."  
    threadCount = 8
    command = 'makeADF --dump-env --dump-timings --accession-id=%s --lane-info-file="%s" --output-dir="%s"' \
              ' --run-reports --report-mirage-max-concordance=0.75 --nano --thread-count=%d --use-all-8bit-sinks' \
              ' --read-structure="%s"'
    command = command % (laneName,jsonDataPath,workDir,threadCount,dnbStructure)
    os.system(command) 


parser = OptionParser('usage: %prog [options] ')
parser.disable_interspersed_args()
parser.add_option('-l', '--lane', default=None,
                  help='Lane Id in the format: GS00000-FS3-L00')
parser.add_option('-o', '--lims-output', default='makeADF.json',
                  help='Path to output the LIMS data')
parser.add_option('-m', '--make-adf', default=None,
                  help='Run makeAdf for the lane, values: human, 2adaptor or "full dnb definition"')

(options, args) = parser.parse_args()

if not options.lane:
    parser.print_help()
    sys.exit()

(slide,lane) = options.lane.split('-L')
limsData = getLimsData(slide,lane)
limsJson = extractLimsJson(limsData)
findDataLocations(limsJson)

wfclib.jsonutil.dump(limsJson,'input_for_'+options.lims_output)

lane = limsJson.lanes[0] 
lane.name = "%s-L%02d" % (lane.slideId, int(lane.laneId))
lane.library = limsJson.libraries[0]
 
wfclib.jsonutil.dump(lane,options.lims_output)

if options.make_adf:
    lane = limsJson.lanes[0]
    runMakeAdf(options.lims_output, lane.name, options.make_adf)

#limsFile = open('add_'+options.lims_output,"w")
#limsFile.write(str(limsJson))
#limsFile.close()

