#!/usr/bin/python
import os,sys
import optparse
from driverlib import *


def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(gapsDir='',refDir='',libID='',mappingDataDir='')
    parser.add_option('-g', '--gaps-dir', dest='gapsDir',
                      help='directory containing (partial) gaps object to work on')
    parser.add_option('-r', '--ref-dir', dest='refDir',
                      help='directory containing reference/simulation data')
    parser.add_option('-l', '--lib-id', dest='libID',
                      help='ID of library')
    parser.add_option('-d', '--mapping-data-dir', dest='mappingDataDir',
                      help='directory that contains mapper output and intermediate covmodel files')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.gapsDir:
        parser.error('gaps-dir specification required')
    if not options.refDir:
        parser.error('ref-dir specification required')

    outputFileName = os.path.join(options.gapsDir, options.libID + '.model_coeffs')
    if os.system("computeCovModel.sh " + options.mappingDataDir + " " + outputFileName):
        raise Error('some trouble with R command')


if __name__ == '__main__':
    main()
