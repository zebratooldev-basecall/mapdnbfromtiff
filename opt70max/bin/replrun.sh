#! /bin/bash

run=$(basename $(pwd))
overrides='
{
  "input" : {
              "referenceBuild":"36",
              "includeReadAndMapping":false
            },
  "asm" :   {
              "ph6Alpha":"1e-9"
            },
  "reportTimings" : false
}'

asms="/rnd/home/jbaccash/archive/trio/NA19240/bb-only/ADF/child:NA19240-A"
asms="$asms /rnd/home/jbaccash/archive/replicates/NA19240/ADF/GS00662-CLS_F11:NA19240-B"

for adfsname in $asms; do

    adfs=${adfsname%:*}
    name=${adfsname#*:}

    mkdir -p $name
    ln -s $adfs ${name}/ADF
    rat create ${run}-${name} onestep.py -c OneStepFromAdf -w $PWD/${name} -x "$overrides"

done
