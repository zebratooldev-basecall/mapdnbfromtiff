#!/usr/bin/env python
import os
import sys
import shutil
from optparse import OptionParser

import accessionlib
import driverlib
from driverlib import Error

import numpy
from numpy import *

import Pycluster

debug=False

def getMateDistr(filename,min,max):

    if(min<0 or min>max):
        raise Error('ugh; stupid programmer')

    infile = open(filename, 'r')

    inMateInfo = False

    cdf = numpy.zeros( (max+1) )

    ttl = 0.0
    offsetmax=0
    for line in infile:
        line = line.strip()
        if line:
            words = line.split(',')

            if( words[0] == "mate distance"):
                inMateInfo = True
            else:
                if( inMateInfo ):
                    offset=int(words[0])
                    freq=float(words[2])
                    if(offset<min):
                        raise Error('expected mate gap range exceeded: offset %d < expected min %d' % (offset,min))
                    if(offset>max):
                        raise Error('expected mate gap range exceeded: offset %d > expected max %d' % (offset,max))

                    ttl += freq
                    cdf[offset] = ttl
                    if offsetmax < offset:
                        offsetmax=offset
                        
    for offset in range(min,max+1):
        if offset>offsetmax:
            cdf[offset]=cdf[offset-1]*ttl
        cdf[offset] /= ttl

    return cdf

def run(om, options):

    maxGap = 2000

    asmloc = ''
    if(options.assemblyId == ''):
        maps = options.mapList.split(',')
    else:
        asm = om.findObject(options.assemblyId)
        if( asm.otype != 'ASM'):
            raise Error('assemblyId must specify an ASM')
        asmloc = asm.location
        maps = [ x for x in asm.mapList ]
        for x in asm.mapList:
            print x

    l = len(maps)
    mapObjects = [om.findObject(ii) for ii in maps]
    cdfs = []
    distances = numpy.zeros( (l,l) )

    for i in range(0,l):
        reportfile = mapObjects[i].location + "/reports/FullDnbSummaryMatches.csv"
        cdfs.append(getMateDistr(reportfile,0,maxGap))

        for j in range(0,i):
            maxDeltaCDF = 0.0
            for g in range(0,maxGap):
                delta = abs(cdfs[i][g] - cdfs[j][g])
                if delta > maxDeltaCDF:
                    maxDeltaCDF = delta
            distances[i][j] = maxDeltaCDF
            distances[j][i] = maxDeltaCDF
            if debug:
                print "distance(%s,%s)=%f\n" % (maps[i],maps[j],maxDeltaCDF)

    if asmloc != '':
        matedistrcsv = open(os.path.join(asmloc,'mateDistributions.csv'),'w')
        topline = ''
        nextline = ''
        for i in range(0,l):
            topline = topline + ',' + mapObjects[i].name
        for i in range(0,l):
            nextline = nextline + ',' + mapObjects[i].library
        print >>matedistrcsv, topline
        print >>matedistrcsv, nextline
        for g in range(0,maxGap):
            gapline = str(g)
            for i in range(0,l):
                gapline = gapline + "," + str(cdfs[i][g])
            print >>matedistrcsv, gapline
        matedistrcsv.close()

    tree = Pycluster.treecluster(None,None,None,0,'a','e',distances)

    if debug:
        for j in range(0,maxGap):
            stri = ""
            for i in range(0,l):
                stri = stri + "," + str(cdfs[i][j])
            print stri

    if debug:
        for i in range(0,l):
            min=1.0;
            for j in range(0,l):
                if i != j and distances[i][j] < min:
                    min = distances[i][j]
            print "min from %s is %f\n" % (maps[i],min)


    nodes = tree[:]
    nodestrings = ['']
    heights = [0]
    for node in nodes:
        if node.left >= 0:
            astr = maps[node.left]
            aheight = 0
        else:
            astr = nodestrings[-node.left]
            aheight = heights[-node.left]
        
        if node.right >= 0:
            bstr = maps[node.right]
            bheight = 0
        else:
            bstr = nodestrings[(-node.right)]
            bheight = heights[-node.right]

        nodestrings.append("(%s:%f,%s:%f)" % (astr,node.distance/2-aheight,bstr,node.distance/2-bheight))
        heights.append(node.distance/2)

    if len(nodestrings) != l :
        raise Error('node count not as expected')
    
    print nodestrings[l-1] + ';\n'

#    print tree

    


def main():
    parser = OptionParser('usage: %prog [options]')
    parser.set_defaults(mapList='',assemblyId='')
    parser.add_option('-r', '--root-dir', dest='rootDir',
                      help='use ROOTDIR as the base directory for the pipeline '
                           'object locations')
    parser.add_option('-a', '--assemblyId', dest='assemblyId',
                      help='Id of assembly to analyze')
    parser.add_option('-m', '--map-list', dest='mapList',
                      help='comma-separated list of MAP objects to use, if assemblyId not specified')
    parser.add_option('--use-custom-build', dest = 'useCustomBuild',
                      action='store_true',
                      help='use executables located at $CGI_HOME instead of '
                           'the current blessed build')
    (options, args) = parser.parse_args()
    if len(args) != 0:
        raise Error('unexpected arguments')

    useBlessedBuild = not options.useCustomBuild
    cgiHome = driverlib.getCgiHome(useBlessedBuild)
    print 'home: ' + cgiHome

    om = accessionlib.ObjectManager(options.rootDir, cgiHome)

    os.environ['LD_LIBRARY_PATH'] = om.binDir()
    os.environ['PATH'] = om.binDir() + \
        ':/sge-root/bin/lx24-amd64:/usr/kerberos/bin:/usr/local/bin:/bin:/usr/bin'

    try:
        run(om, options)
    finally:
        om.unlock()

    
if __name__ == '__main__':
    try:
        main()
    except Error, msg:
        print 'error: ', msg
        sys.exit(1)
    sys.exit(0)
