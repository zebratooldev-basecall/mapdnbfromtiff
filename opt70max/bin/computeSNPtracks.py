#!/bin/env python
import os,sys
import optparse
from cnvwindow import WholeGenomeCNVWindowIterator
from gbifile import GbiFile 
from refconstants import GlobalRefConstants

class outputClass(object):
    def __init__(self,outputpfx):
        self.hetCount = open(outputpfx + "-hetCount.tsv","w")
        self.homCount = open(outputpfx + "-homCount.tsv","w")
        self.halfCount = open(outputpfx + "-halfCount.tsv","w")
        self.hetMedMAF = open(outputpfx + "-hetMedMAF.tsv","w")
        self.hetMinMAF = open(outputpfx + "-hetMinMAF.tsv","w")
        self.hetMaxMAF = open(outputpfx + "-hetMaxMAF.tsv","w")
        self.homMeanMAF = open(outputpfx + "-homMeanMAF.tsv","w")
        self.countsForMAF = open(outputpfx + "-countsForMAF.tsv","w")

winLen=1000000


n=-1
chromosomeCol = n
beginCol = n
endCol = n
zygosityCol = n
varTypeCol = n
referenceCol = n
allele1SeqCol = n
allele2SeqCol = n
allele1ReadCountCol = n
allele2ReadCountCol = n
neitherAlleleReadCountCol = n
totalReadCountCol = n

chromosome = ""
begin = -1
end = -1
zygosity = ""
varType = ""
reference = ""
allele1Seq = ""
allele2Seq = ""
allele1ReadCount = -1
allele2ReadCount = -1
neitherAlleleReadCount = -1
totalReadCount = -1

pChr = ""
begin = None
winBegin = -1
winEnd = -1
homCount = 0
halfCount = 0
hetCount = 0
hetMAF = []
homMAF = []

numFields = 0

cnvWinIterator = WholeGenomeCNVWindowIterator()

def readHeader(w):
    n = 0
    for x in w:
        if x == "chromosome":
            global chromosomeCol
            chromosomeCol = n
        if x == "begin":
            global beginCol
            beginCol = n
        if x == "end":
            global endCol
            endCol = n
        if x == "zygosity":
            global zygosityCol
            zygosityCol = n
        if x == "varType":
            global varTypeCol
            varTypeCol = n
        if x == "reference":
            global referenceCol
            referenceCol = n
        if x == "allele1Seq":
            global allele1SeqCol
            allele1SeqCol = n
        if x == "allele2Seq":
            global allele2SeqCol
            allele2SeqCol = n
        if x == "allele1ReadCount":
            global allele1ReadCountCol
            allele1ReadCountCol = n
        if x == "allele2ReadCount":
            global allele2ReadCountCol
            allele2ReadCountCol = n
        if x == "neitherAlleleReadCount":
            global neitherAlleleReadCountCol
            neitherAlleleReadCountCol = n
        if x == "totalReadCount":
            global totalReadCountCol
            totalReadCountCol = n
        n += 1
    global numFields
    numFields = n

def line_is_useful(w):
    if len(w) != numFields:
        return False
    if w[varTypeCol] != "snp":
        return False
    if w[zygosityCol] != "hom" and w[zygosityCol] != "het-ref" and w[zygosityCol] != "half":
        return False
    
    if int(w[totalReadCountCol]) == 0:
        return False

    if w[zygosityCol] == "half":
        if int(w[allele1ReadCountCol]) == 0:
            return False
    else:
        if int(w[allele1ReadCountCol])+int(w[allele2ReadCountCol]) == 0:
            return False

    return True


def readData(w):
    if line_is_useful(w):
        global chromosome
        chromosome = w[chromosomeCol]
        global begin
        begin = w[beginCol]
        global end
        end = w[endCol]
        global zygosity
        zygosity = w[zygosityCol]
        global varType
        varType = w[varTypeCol]
        global reference
        reference = w[referenceCol]
        global allele1Seq
        allele1Seq = w[allele1SeqCol]
        global allele2Seq
        allele2Seq = w[allele2SeqCol]
        global allele1ReadCount
        allele1ReadCount = w[allele1ReadCountCol]
        global allele2ReadCount
        allele2ReadCount = w[allele2ReadCountCol]
        global neitherAlleleReadCount
        neitherAlleleReadCount = w[neitherAlleleReadCountCol]
        global totalReadCount
        totalReadCount = w[totalReadCountCol]

        return True
    else:
        return False


def init_window(outputs):
    global pChr
    global chromosome
    global winBegin
    global winEnd
    global homCount
    global halfCount
    global hetCount
    global hetMAF
    global homMAF

    pChr = chromosome
    homCount = 0
    halfCount = 0
    hetCount = 0
    hetMAF = []
    homMAF = []           

    winEnd = winBegin
    while winEnd < int(begin):
        if cnvWinIterator.eof():
            raise Exception("ERROR: CNV window iterator out of windows before end of masterVar file")
        win = cnvWinIterator.next()
        chromosome = win.chr_
        winBegin = win.begin_
        winEnd = win.end_
        if winEnd < int(begin):
            output_window(outputs)

def output_window(outputs):
    global winBegin
    outChr="hs"+pChr[3:]
    print " ".join(("Output for",outChr,str(winBegin),str(winEnd)))
    outputs.hetCount.write("\t".join((outChr,str(winBegin),str(winEnd),str(hetCount)))+"\n")
    outputs.homCount.write("\t".join((outChr,str(winBegin),str(winEnd),str(homCount)))+"\n")
    outputs.halfCount.write("\t".join((outChr,str(winBegin),str(winEnd),str(halfCount)))+"\n")
    if(hetCount > 10):
        hetMAF.sort()
        outputs.hetMedMAF.write("\t".join((outChr,str(winBegin),str(winEnd),str(hetMAF[int(hetCount/2)])))+"\n")
        outputs.hetMinMAF.write("\t".join((outChr,str(winBegin),str(winEnd),str(hetMAF[0])))+"\n")
        outputs.hetMaxMAF.write("\t".join((outChr,str(winBegin),str(winEnd),str(hetMAF[-1])))+"\n")
    if(homCount > 10):
        mean = 0
        for x in homMAF:
            mean += x
        outputs.homMeanMAF.write("\t".join((outChr,str(winBegin),str(winEnd),str(mean/homCount)))+"\n")
    winBegin = -1

def consumeData(w,outputs):

    if winBegin == -1:
        init_window(outputs)

    if zygosity == "hom":
        global homCount
        homCount += 1
        global homMAF
        x = float(allele1ReadCount) / float(totalReadCount)
        if x > .5:
            x = 1.-x
        homMAF.append( x )
    elif zygosity == "half":
        global halfCount
        halfCount += 1
    else:
        global hetCount
        hetCount += 1
        global hetMAF
        x = float(allele1ReadCount) / (float(allele1ReadCount)+float(allele2ReadCount) )
        if x > .5:
            x = 1.-x
        hetMAF.append( x)
            
    

def computeSNPtracks(masterVar,outputPfx):

    mv = open(masterVar)
    ok = False

    outputs = outputClass(outputPfx)
    
    for l in mv:
        w = l.rstrip().split("\t")
        if ok:
            if readData(w):
                if chromosome != pChr or (begin != None and winEnd < int(begin)):
                    if pChr != "" and winBegin != None:
                        output_window(outputs)
                consumeData(w,outputs)
                                  
        if w[0] == ">locus":
            ok = True
            readHeader(w)
        
    if pChr != "" and winBegin != None:
        output_window(outputs)
                
            
        

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(masterVar='',output='',windowWidth='100000',sampleType='',gbiFile='')
    parser.add_option('-m', '--master-var', dest='masterVar',
                      help='location of master var file')
    parser.add_option('-o', '--output-prefix', dest='outputPfx',
                      help='prefix for output file names')
    parser.add_option('--window-width',dest='windowWidth',
                      help='window width')
    parser.add_option('--sample-type',dest='sampleType',
                      help='sample type')
    parser.add_option('--reference-root-dir',dest='referenceRootDir',
                      help='referenceRootDir of the workflow')
    parser.add_option('--reference-gbi',dest='gbiFile')
    
    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.masterVar:
        parser.error('master-var specification required')
    if not options.outputPfx:
        parser.error('output-prefix specification required')

    globalRefConstants = GlobalRefConstants(options.referenceRootDir)
    refConstants = globalRefConstants[options.sampleType]
    gbi = GbiFile(options.gbiFile)
    global cnvWinIterator
    wwidth = int(options.windowWidth)
    cnvWinIterator.initialize(gbi,refConstants.cnvIgnoreRegions,wwidth,wwidth)

    computeSNPtracks(options.masterVar,options.outputPfx)


if __name__ == '__main__':
    import sys, traceback
    main()
    
