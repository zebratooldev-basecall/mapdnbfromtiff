#!/usr/bin/env python

import sys
import getopt

def Write(outf, exp, chrCol, bgnCol, endCol, chrm, start, end):
    exp[chrCol] = chrm
    exp[bgnCol] = str(start)
    exp[endCol] = str(end)
    outf.write( "\t".join( exp ) + "\n")

def main():

    # parse command line options and update variables
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:", [])
    except getopt.GetoptError:
        print "Usage error!"
        sys.exit()

    for opt, arg in opts:
        if opt == "-h":
            sys.exit('Use me properly.')
        elif opt == '-i':
            INFILE = arg
        elif opt == '-o':
            OUTFILE = arg

           
    inf = open(INFILE, 'r')
    outf = open(OUTFILE, 'w')
    RangeColumn = 0
    prev_chr = ""
    prev_start = 0
    prev_end = 0
    prev_locus = ""
    no_call_example = ""
    for line in inf:
        ## Skip header lines
        if line.startswith("#") or len(line.rstrip()) == 0:
            outf.write(line)
            continue

        words = line.split("\t")

        if line.startswith(">"):
            ## Process header
            RangeColumn = [x for x in range(len(words)) if words[x]=="contig"][0]
            chrCol = [x for x in range(len(words)) if words[x]=="chromosome"][0]
            bgnCol = [x for x in range(len(words)) if words[x]=="begin"][0]
            endCol = [x for x in range(len(words)) if words[x]=="end"][0]
            outf.write( "\t".join( words[:RangeColumn] ) + "\n" )
            continue

        if words[RangeColumn] != "" or "no-ref" in line or words[0]==prev_locus:
            # This overlaps a region, output previous no-call and output this line as is; reset
            if prev_chr != "":
                Write( outf, no_call_example, chrCol, bgnCol, endCol, prev_chr, prev_start, prev_end )
            outf.write( "\t".join( words[:RangeColumn] ) + "\n" )
            prev_locus = words[0]
            prev_chr = ""
            prev_start = 0
            prev_end = 0
        else:
            # This is a no-call
            if no_call_example=="":
                no_call_example = words[:RangeColumn]
                no_call_example[0] = "0"
                
            if prev_chr != words[chrCol] or int(words[bgnCol]) > prev_end:
                if prev_chr != "":
                    Write( outf, no_call_example, chrCol, bgnCol, endCol, prev_chr, prev_start, prev_end )
                prev_chr   = words[chrCol]
                prev_start = int(words[bgnCol])
                prev_end   = int(words[endCol])
            else:
                prev_end = max( prev_end, int(words[endCol]) )


###################################################

if __name__ == "__main__":
    main()
