#!/usr/bin/env python
import os
import StringIO
import pymssql
import ConfigParser
import sets
import itertools
import re
import string
from optparse import OptionParser

"""
Upload IDQC genotypes and report on stats related to the new data

"""

class DBContam:

    def __init__(self,dbLogin):
        self.conn = None
        self.dbLogin=dbLogin
        self.rows = []
        self.accumulate=""
        self.insertion_count=0

    def connect(self,whichDB="contam:dwProd"):
        self.conn = pymssql.connect(host=self.dbLogin.get(whichDB,'host'),
                               user=self.dbLogin.get(whichDB,'user'),
                               password=self.dbLogin.get(whichDB,'password'),
                               database=self.dbLogin.get(whichDB,'database'))
        if not self.conn:
            print "can't connect"
            os.sys.exit()
        
    def execute(self,query):
        cur = self.conn.cursor()
        cur.execute(query)
        self.rows = cur.fetchall()
        cur.close()
        return self.rows[0][0]

    def execute_no_result(self,query,debug):
        cur = self.conn.cursor()	
        if not debug:
            try :
                cur.execute(query)
            except pymssql.OperationalError:
                pass
            cur.close()
        else:
            print query

    def insertContamStats(self,header,data,debug):
        if data==None:
            print self.accumulate
            self.execute_no_result(self.accumulate,debug)
            self.accumulate = ""
            self.conn.commit()
        else:
            if data[4] != 'NA' and data[16] != 'NA':
                self.accumulate += "insert into [CGI\drosenfeld].cs_contam %s values ('%s','%s','%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);\n" % (header, data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],data[8],data[9],data[10],data[11],data[12],data[13],data[14],data[15],data[16])
                self.insertion_count = self.insertion_count+1
                if self.insertion_count > 72:
                    self.execute_no_result(self.accumulate,debug)
                    self.accumulate = ""
                    self.insertion_count=0

    def insertChimeraStats(self,header,data,debug):
        if data==None:
            print self.accumulate
            self.execute_no_result(self.accumulate,debug)
            self.accumulate = ""
            self.conn.commit()
        else:
            if data[2] != 'N' :
                self.accumulate += "insert into [jwang].cs_chimera %s values ('%s','%s',%s);\n" % (header, data[0],data[1],",".join(data[2:]))

                self.insertion_count = self.insertion_count+1
                if self.insertion_count > 72:
                    self.execute_no_result(self.accumulate,debug)
                    self.accumulate = ""
                    self.insertion_count=0


    def insertGCRollupStats(self,header,data,debug,table="[jwang].cs_gc_rollup_v2"):
        if data==None:
            print self.accumulate
            self.execute_no_result(self.accumulate,debug)
            self.accumulate = ""
            self.conn.commit()
        else:
            if data[0] != 'SIM' and data[0] != 'GS199-ASM':
                self.accumulate += "insert into %s %s values ('%s','%s',%s);\n" % (table, header, data[0], data[1], ",".join(data[2:]))
                self.insertion_count = self.insertion_count+1
                if self.insertion_count > 72:
                    self.execute_no_result(self.accumulate,debug)
                    self.accumulate = ""
                    self.insertion_count=0

    def createLfrStats(self,debug):

	hcolumn = "Project VARCHAR(50), Sample VARCHAR(50), Library VARCHAR(50), Lane VARCHAR(50)"
        colnorm = ""
	for i in range(385):
	    hcolumn += ",V"+str(i)+" BIGINT"
            colnorm += ",V"+str(i)+"_normed FLOAT"
	hcolumn += colnorm
        self.accumulate += "CREATE TABLE [jwang].tmp_cs_lfr_stats(%s);\n" % hcolumn
	self.accumulate += "GRANT UPDATE on [jwang].tmp_cs_lfr_stats to gcw_IDQC;\n"
	self.accumulate += "GRANT INSERT on [jwang].tmp_cs_lfr_stats to gcw_IDQC;\n"
        print self.accumulate
#	self.execute_no_result(self.accumulate,debug)
	self.accumulate = ""
#        self.conn.commit()

    def insertLfrStats(self,header,data,debug):
        if data==None:
            print self.accumulate 
            self.execute_no_result(self.accumulate,debug)
            self.accumulate = ""
            self.conn.commit()
	else:
            if data[4] != 'NA':
                self.accumulate += "insert into [jwang].tmp_cs_lfr_stats %s values ('%s','%s','%s','%s',%s);\n" % (header, data[0], data[1], data[2], data[3], ','.join(data[4:]))
                self.insertion_count = self.insertion_count+1
                if self.insertion_count > 72:	
                    self.execute_no_result(self.accumulate,debug)
                    self.accumulate = ""
                    self.insertion_count=0 

    def insertMapStats(self,header,data,debug):
        if data==None:
            print self.accumulate
            self.execute_no_result(self.accumulate,debug)
            self.accumulate = ""
            self.conn.commit()
        else:
            if data[1] != 'NA':
                self.accumulate += "insert into [jwang].cs_MapStats %s values ('%s',%s);\n" % (header, data[0], ','.join(data[1:]))
                self.insertion_count = self.insertion_count+1
                if self.insertion_count > 72:
                    self.execute_no_result(self.accumulate,debug)
                    self.accumulate = ""
                    self.insertion_count=0

    def insertNonDups(self,header,data,debug):
        if data==None:
            print self.accumulate
            self.execute_no_result(self.accumulate,debug)
            self.accumulate = ""
            self.conn.commit()
        else: 
            self.accumulate += "insert into jwang.cs_nonDuplicateRate_2ad %s values ('%s',%s);\n" % (header, data[0], ",".join(data[1:]))
            self.insertion_count = self.insertion_count+1
            if self.insertion_count > 72:
                self.execute_no_result(self.accumulate,debug)
                self.accumulate = ""
                self.insertion_count=0

    def insertCoverageGCcurves(self,header,data,debug):
        if data==None:
            print self.accumulate
            self.execute_no_result(self.accumulate,debug)
            self.accumulate = ""
            self.conn.commit()
        else: 
            self.accumulate += "insert into [bioinfo].cs_coverage_gc_curves %s values ('%s',%s,'%s');\n" % (header, data[0], ",".join(data[1:3]),data[3])
            self.insertion_count = self.insertion_count+1
            if self.insertion_count > 72:
                self.execute_no_result(self.accumulate,debug)
                self.accumulate = ""
                self.insertion_count=0

    def insertCoverageGCstats(self,header,data,debug):
        if data==None:
            print self.accumulate
            self.execute_no_result(self.accumulate,debug)
            self.accumulate = ""
            self.conn.commit()
        else: 
            self.accumulate += "insert into [bioinfo].cs_coverage_gc_bias_stats %s values ('%s',%s);\n" % (header, data[0], ",".join(data[1:]))
            self.insertion_count = self.insertion_count+1
            if self.insertion_count > 72:
                self.execute_no_result(self.accumulate,debug)
                self.accumulate = ""
                self.insertion_count=0


def uploadContamScreen(df1,df2,df3,df3_old,df4,df5,df6,df7,df8,lfr,dbLogin,debug):
    db=DBContam(dbLogin)
    db.connect()

    df=open(df8,'r')
    line = df.readline()
    cols = line.strip().split(",")
    header= '("'+string.replace('","'.join(cols),".","#")+'")'
    line=df.readline()

    while line:
        data = line.strip().split(",")	
        db.insertCoverageGCstats(header,data,debug)
        line=df.readline()
    db.insertCoverageGCstats(header,None,debug)
    df.close()
 
    df=open(df7,'r')
    line = df.readline()
    cols = line.strip().split(",")
    header= '("'+string.replace('","'.join(cols),".","#")+'")'
    line=df.readline()

    while line:
        data = line.strip().split(",")	
        db.insertCoverageGCcurves(header,data,debug)
        line=df.readline()
    db.insertCoverageGCcurves(header,None,debug)
    df.close()
 
    df=open(df6,'r')
    line = df.readline()
    cols = line.strip().split(",")
    header= '("'+string.replace('","'.join(cols),".","#")+'")'
    line=df.readline()

    while line:
        data = line.strip().split(",")	
        db.insertMapStats(header,data,debug)
        line=df.readline()
    db.insertMapStats(header,None,debug)
    df.close()
 
    df=open(df5,'r')
    line = df.readline()
    cols = line.strip().split(",")
    header= '("'+string.replace('","'.join(cols),"_","_")+'")'
    line=df.readline()
    while line:
        data = line.strip().split(",")
        db.insertNonDups(header,data,debug)
        line=df.readline()
    db.insertNonDups(header,None,debug)
    df.close()

    df=open(df3,'r')
    line = df.readline()
    cols = line.strip().split(",") #[0:4]
    for i in range(len(cols)):
	cols[i] = cols[i].replace("/","#")
    header= '("'+string.replace('","'.join(cols),".","#")+'")'
    line=df.readline()
    while line:
        data = string.replace(line.strip(),"%","").split(",")
        db.insertGCRollupStats(header,data,debug)
        line=df.readline()
    db.insertGCRollupStats(header,None,debug)
    df.close() 

    df=open(df3_old,'r')
    tbl="[CGI\drosenfeld].[cs_gc_rollup]"
    line = df.readline()
    cols = line.strip().split(",")[0:4]
    for i in range(len(cols)):
        cols[i] = cols[i].replace("/","#")
    header= '("'+string.replace('","'.join(cols),".","#")+'")'
    line=df.readline()
    while line:
        data = string.replace(line.strip(),"%","").split(",")
        db.insertGCRollupStats(header,data,debug,tbl)
        line=df.readline()
    db.insertGCRollupStats(header,None,debug,tbl)
    df.close()
 
    df=open(df2,'r')
    line = df.readline()
    cols = line.strip().split(",")
    header= '("'+string.replace('","'.join(cols),".","#")+'")'
    line=df.readline()
    while line:
        data = string.replace(line.strip(),"%","").split(",")
        db.insertChimeraStats(header,data,debug)
        line=df.readline()
    db.insertChimeraStats(header,None,debug)
    df.close()

    df=open(df1,'r')
    line = df.readline()
    cols = line.strip().split(",")
    header= '("'+string.replace('","'.join(cols),".","#")+'")'
    line=df.readline()
    while line:
        data = line.strip().split(",")
        db.insertContamStats(header,data,debug)
        line=df.readline()
    db.insertContamStats(header,None,debug)
    df.close()

    print lfr
    if lfr == 'LFR':
        print "Running InsertLfrStats"
        df=open(df4,'r')
        line = df.readline()
        cols = line.strip().split(",")
        header= '("'+string.replace('","'.join(cols),".","#")+'")'
        line=df.readline()
        while line:
            data = line.strip().split(",")
            db.insertLfrStats(header,data,debug)
            line=df.readline()
        db.insertLfrStats(header,None,debug)
        df.close()

    return


def main():

    parser = OptionParser('usage: %prog [options]')
    parser.add_option('--debug',dest='debug',action='store_true',default=False)
    parser.add_option('--dir', dest='dir',
                      help='path to directory containing cs-stats.csv, chimerarpt.csv, gcrpt-rollup.csv, lfr-stats.csv, cs-nonDupRate.csv')
    parser.add_option('--lib',dest='lib',default='STD')
    parser.add_option('--config',dest='config',help='path to uploader config file')
    (options, args) = parser.parse_args()
    
    dbLogin = ConfigParser.ConfigParser()
    dbLogin.read(options.config)
    
    if os.path.exists(options.dir):	
        print options.lib
        uploadContamScreen(os.path.join(options.dir,"cs-stats.csv"),
                           os.path.join(options.dir,"chimerarpt.csv"),
                           os.path.join(options.dir,"gcrpt-rollup.csv"),
			   os.path.join(options.dir,"gcrpt-rollup-old.csv"),
			   os.path.join(options.dir,"lfr-stats.csv"),
			   os.path.join(options.dir,"cs-nonDupRate.csv"),
			   os.path.join(options.dir,"cs-mapStats.csv"),
			   os.path.join(options.dir,"coverage_gc_curves.csv"),
			   os.path.join(options.dir,"coverage_gc_stats.csv"),
			   options.lib,
                           dbLogin,
                           options.debug)
    else:
        print "No Path to "+options.dir
    return

if __name__ == '__main__':
    main()

