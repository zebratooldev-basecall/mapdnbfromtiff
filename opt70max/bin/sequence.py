#!/usr/bin/env python

import sys
import string

#SEQUENCE_DIR="/rnd/home/archive/externalData/sequenceRepository/build36"
#SEQUENCE_DIR="/rnd/home/archive/externalData/sequenceRepository/build37.1"

complement = string.maketrans('ATCGN','TAGCN')

def reverse_complement(sequence):
    return sequence.upper().translate(complement)[::-1]

class ChrSequence(object):
    def __init__(self,fn):
        try:
            ff = open(fn, 'r')
        except:
            sys.stderr.write('Couldn\'t open fasta file %s\n' % fn)
            sys.exit(-1)

        # process fasta header.
        line = ff.readline().rstrip('\r\n')
        if not line.startswith('>'):
            sys.stderr.write('Invalid fasta format!')
            sys.exit(-1)

        self.name = line[1:].lower()
        self.sequence = ''.join(line[:-1].upper() for line in ff.readlines())
        ff.close()

    def get_sequence(self,chrName,begin,end):
        if chrName.lower() != self.name:
            sys.stderr.write('chrName %s does not match %s from the fasta header!' % (chrName,self.name))
            sys.exit(-1)

        if end > len(self.sequence):
            sys.stderr.write('end beyond chr bounds! len(%s)=%d...end=%d' % (self.name,len(self.sequence),end))
            sys.exit(-1)

        return self.sequence[begin:end]


