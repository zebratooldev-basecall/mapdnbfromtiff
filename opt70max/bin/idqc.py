#!/usr/bin/python

import os, sys
from optparse import OptionParser
from string import maketrans, find
from exceptions import Exception


class IdqcMatchRec(object):
    def __init__(self, hFields, fields):
        for (hField,field) in zip(hFields, fields):
            self.__dict__[hField] = field


parser = OptionParser('usage: %prog [options]')
parser.add_option('-i', '--idqc', metavar="FILE", 
    help = 'input file/directory with probed IDQC results')
parser.add_option('-d', '--dbsnp-to-cgi', metavar="FILE", 
    help = 'input dbSNP-to-CGI annotation file')
parser.add_option('-o', '--output', metavar="FILE", 
    help = 'output qualification file')
parser.add_option('-s', '--sample', metavar="STRING", 
    help = 'sample ID')
parser.add_option('-t', '--threshold', metavar="[0..1]", default = 0.9, 
    help = 'pass/fail ratio threshold')
parser.add_option('-m', '--min-tested', metavar="[0..48]", default = 30, 
    help = 'min # of tested genotypes threshold')
parser.add_option('-r', '--reports', metavar="FILE",
    help = 'output file with data tables from WebReprots and AQC')
parser.add_option('-v', '--var-threshold', metavar="FILE",
    help = 'variations file threshold, e.g. 20-40_ccf100')
parser.add_option('-w', '--wlf-run', action="store_true",
    help = 'run in "workflow" mode, always returns 0 exit code')
parser.add_option('-x', '--dry-run', action="store_true",
    help = '[debug] don\'t really grep any IDQC data, try infrastructure')
(options, args) = parser.parse_args()

# Sanity check input parameters.
if not options.idqc:
    parser.error('IDQC file must be specified')
if not options.dbsnp_to_cgi:
    parser.error('dbSNP-to-CGI file must be specified')
if not options.output:
    parser.error('output file must be specified')
if not options.sample:
    parser.error('sample ID must be specified')
if not options.var_threshold and options.reports is not None:
    parser.error('variations threshold must be specified')

print 'sample ID =', options.sample
print 'threshold =', options.threshold
print 'min-tested =', options.min_tested

if os.path.isdir(options.idqc):
    print 'searching for IDQC files in: "'+options.idqc+'"'
    files = os.listdir(options.idqc)
    candidates = []
    for f in files:
        name, ext = os.path.splitext(f)
        if ext == '.csv':
            fpath = os.path.join(options.idqc, f)
            grepCmd = 'egrep "^.*,'+options.sample+',.*$" '+fpath
            #print '@@@', grepCmd
            found = False
            for l in os.popen(grepCmd):
                if not found and len(l) != 0:
                    print 'IDQC candidate found: '+fpath
                    candidates.append(fpath)
                    found = True
    if len(candidates) == 0:
        print 'sample ID '+options.sample+' is not found'
        options.idqc = None
    else:
        print 'found %d IDQC candidate file(s)'%len(candidates)
        candidates.sort()
        bestIdqc = candidates[0]
        bestTime = os.stat(bestIdqc).st_ctime
        for f in candidates[1:]:
            ctime = os.stat(f).st_ctime
            if ctime >= bestTime:
                bestIdqc = f
                bestTime = ctime
        print 'best IDQC candidate file: '+bestIdqc
        options.idqc = bestIdqc

idqc = dict()
if not options.idqc:
    print 'no IDQC data, can\'t read genotypes'
else:
    print 'reading IDQC keys: "'+options.idqc+'"'
    for line in open(options.idqc, 'r'):
        tokens = line.split(',')
        #print '@@@', tokens
        if len(tokens) != 12 or tokens[4] != options.sample:
            continue
        if tokens[1][0:2] == "rs":
            key = tokens[1]
            value = tokens[9]
            if value in ['No Call', 'Invalid']:
                value = ['?','?']
            else:
                value = value.split(':')
            if len(value) != 2:
                print '[ERROR] bad IDQC key %s value format: %s, ignored' % (key, tokens[9])
            elif len([x for x in value if x in ['A','C','T','G','?']]) != 2:
                print '[ERROR] invalid IDQC key %s value: %s, ignored' % (key, tokens[9])
            else:
                value.sort()
                if key in idqc:
                    if value == idqc[key][0]:
                        print '[WARNING] duplicate IDQC key entry %s found' % key
                    else:
                        oldVal = idqc[key][0]
                        print '[WARNING] IDQC key %s value overridden: %s:%s -> %s:%s' \
                            % (key, oldVal[0], oldVal[1], value[0], value[1])
                idqc[key] = [value, tokens]
    print 'IDQC keys set:'
    noTestCnt = 0
    for key, val in idqc.items():
        print '    '+key+' = '+val[0][0]+':'+val[0][1]
        if val[0][0] == '?' and val[0][1] == '?':
            noTestCnt += 1
    if len(idqc) != 48:
        print '[WARNING] incomplete IDQC keys: %d != 48' % len(idqc)
    if noTestCnt > 0:
        print 'IDQC has %d out of %d keys no-called' % (noTestCnt, len(idqc))
    else:
        print 'IDQC has called all %d keys' % len(idqc)

# auto-detect TABs vs CSV
(root, ext) = os.path.splitext(options.dbsnp_to_cgi)
if ext == '.csv':
    fieldSep = ','
elif ext == '.tsv' or ext == '.tab':
    fieldSep = '\t'
else:
    raise Exception('unknown file format: "%s"' % options.dbsnp_to_cgi)

hFields = [ 'zygosity',
            'varTypeA', 'hapA', 'varScoreVAFA',
            'varTypeB', 'hapB', 'varScoreVAFB']
asm = dict()
if len(idqc) == 0:
    print 'no IDQC keys, can\'t read assembly keys'
elif options.dry_run:
    print 'dry run, skipping reading assembly keys'
else:
    # Find header row.
    ff = open(options.dbsnp_to_cgi)
    while True:
        header = ff.readline()
        if '' == header:
            raise Exception('unexpected end of file searching for file header: '+
                            options.dbsnp_to_cgi)
        if header.startswith('>'):
            break
    ff.close()
    hFields = header[1:].rstrip('\r\n').split(fieldSep)

    # uses child process "grep" to extract ASM genotypes
    grepCmd = ''
    for rsid in iter(idqc):
        grepCmd += '|^dbsnp\.[0-9]*:'+rsid+fieldSep+'.*'
    grepCmd = 'egrep "' + grepCmd[1:]+'" ' + options.dbsnp_to_cgi
    grepOut = os.popen(grepCmd)
    print 'reading assembly keys: "'+options.dbsnp_to_cgi+'"'
    nToQ = maketrans('N','?')
    noTestCnt = 0
    # pipe CSV lines out of the "grep" exec
    for line in grepOut:
        if line == "\n":
            continue
        print line.rstrip('\r\n')
        tokens = line.rstrip('\r\n').split(fieldSep)
        rec = IdqcMatchRec(hFields, tokens)

        # Autosomes only.
        if rec.chromosome in [ 'chrX', 'chrY', 'chrM' ]:
            continue

        key = rec.dbSnpId.split(":")[1]
        value = [rec.hapA.translate(nToQ), rec.hapB.translate(nToQ)]
        if find(value[0], '?') >= 0:
            value[0] = '?'
        if find(value[1], '?') >= 0:
            value[1] = '?'
        if value[0] == '?' and value[1] == '?':
            noTestCnt += 1
        # move ? to 1st position
        value.sort()
        # fix for DEL variations
        if value[0] == '':
            if value[1] == '':
                value[0] = '-'
            else:
                value[0] = value[1]
            value[1] = '-'
        asm[key] = [value, rec]
    grepOut.close()
    print 'assembly keys testset:'
    for key, val in asm.items():
        print '    '+key+' = '+val[0][0]+':'+val[0][1]
    if len(asm) != 48:
        print "[WARNING] incomplete ASM keys: %d != 48" % len(asm)
    if noTestCnt > 0:
        print 'assembly has %d out of %d keys no-called' % (noTestCnt, len(asm))
    else:
        print 'assembly has called all %d keys' % len(asm)

matched = 0 # both alleles matched
compatible = 0 # one allele matched, one no-called
noTest = 0 # both alleles no-called
halfMatched = 0 # one allele matched, one wasn't
mismatched = 0 # both alleles mismatched
notFound = 0 # genotype is missing
unknown = 0 # other complex variation
total = 0 # sum of all genotypes

print 'comparing IDQC vs assembly keys'
for key in iter(idqc):
    total += 1
    if key in asm:
        v1 = idqc[key][0]
        v2 = asm[key][0]
        assert(v1[1]!='?' or v1[0]=='?')
        assert(v2[1]!='?' or v2[0]=='?')
        outcome = 'error'
        if v1[1]=='?' or v2[1]=='?':
            outcome = 'no-test'
            noTest += 1
        elif len(v1[0]+v1[1]+v2[0]+v2[1]) > 4:
            outcome = 'unknown'
            unknown += 1
        elif v1 == v2:
            if v1[0]=='?' or v2[0]=='?':
                outcome = 'compatible'
                compatible += 1
            else:
                outcome = 'matched'
                matched += 1
        else:
            if v1[0]=='?':
                if v2[0]=='?':
                    if v1[1] == v2[1]:
                        outcome = 'compatible'
                        compatible += 1
                    else:
                        outcome = 'no-test'
                        noTest += 1
                elif v1[1]==v2[0] or v1[1]==v2[1]:
                    outcome = 'compatible'
                    compatible += 1
                else:
                    outcome = 'mismatched'
                    mismatched += 1
            elif v2[0]=='?':
                if v2[1]==v1[0] or v2[1]==v1[1]:
                    outcome = 'compatible'
                    compatible += 1
                else:
                    outcome = 'mismatched'
                    mismatched += 1
            elif v1[0]==v2[0] or v1[0]==v2[1] or v1[1]==v2[0] or v1[1]==v2[1]:
                outcome = 'half-matched'
                halfMatched += 1
            else:
                outcome = 'mismatched'
                mismatched += 1
        print '    '+key+': '+v1[0]+':'+v1[1]+' '+outcome+' '+v2[0]+':'+v2[1]
    else:
        outcome = 'not-found'
        notFound += 1
        print '    '+key+' '+outcome
    idqc[key] += [outcome]

print 'writing output file: "'+options.output+'"'
out = open(options.output, 'w')
out.write('# Detailed sample IDQC check results\n')
out.write('# Sample ID: '+options.sample+'\n')
out.write('# Genotypes file: '+str(options.idqc)+'\n')
out.write('# dbSNP annotations: '+options.dbsnp_to_cgi+'\n')
out.write('\n')
out.write('ChamberID,rsID,testedAlleles,Confidence,compareResult,'+
          'zygosity,varTypeX,hapX,scoreX,varTypeY,hapY,scoreY,pass\n')
for key in iter(idqc):
    outcome = idqc[key][2]
    t1 = idqc[key][1]
    if key in asm:
        rec = asm[key][1]
    else:
        rec = IdqcMatchRec(hFields, [ '' ] * len(hFields))
    out.write(t1[0]+',')
    out.write(t1[1]+',')
    out.write(t1[9]+',')
    out.write(t1[7]+',')
    out.write(outcome+',')
    out.write(rec.zygosity+',')
    out.write(rec.varTypeA+',')
    out.write(rec.hapA+',')
    out.write(rec.varScoreVAFA+',')
    out.write(rec.varTypeB+',')
    out.write(rec.hapB+',')
    out.write(rec.varScoreVAFB+',')
    passFail = {
        'matched': '1',
        'compatible': '1',
        'no-test': 'NA',
        'half-matched': '0',
        'mismatched': '0',
        'not-found': 'NA',
        'unknown': 'NA'
    }[outcome]
    out.write(passFail+'\n')
out.close()

print 'statistics:'
print '    matched:', matched
print '    compatible:', compatible
print '    half-matched:', halfMatched
print '    mismatched:', mismatched
print '    no-test:', noTest
print '    not-found:', notFound
print '    unknown:', unknown
print '    total:', total

passed = matched + compatible
failed = mismatched + halfMatched
tested  = passed + failed
if tested != 0:
    threshold = float(passed) / tested
else:
    threshold = 0

# IDQC qualification checks:
if tested < options.min_tested:
    retCode = 1
    answer = '*FAILED* min-tested: %d<%d' % (tested, options.min_tested)
elif threshold < options.threshold:
    retCode = 1
    answer = '*FAILED* threshold: %s<%s' % (threshold, options.threshold)
else:
    retCode = 0
    answer = 'PASSED: %s>=%s, %d>=%d' % (threshold, options.threshold,\
                                         tested, options.min_tested)

print 'qualification:'
print '    passed:', passed
print '    failed:', failed
print '    threshold: %s' % threshold
print '    '+answer

# Generate data for HTML reports and AQC check
if options.reports is not None:
    print 'writing reports: "'+options.reports+'"'
    ofs = open(options.reports, 'w')
    print >>ofs, '# Sample IDQC check results'
    print >>ofs, '# Sample ID:', options.sample
    print >>ofs, '# Genotypes file:', str(options.idqc)
    print >>ofs, '# dbSNP annotations:', options.dbsnp_to_cgi
    print >>ofs, ''
    print >>ofs, 'freeform,idqcResults'+options.var_threshold
    print >>ofs, 'metric,value'
    print >>ofs, 'matched,'+str(matched)
    print >>ofs, 'compatible,'+str(compatible)
    print >>ofs, 'half-matched,'+str(halfMatched)
    print >>ofs, 'mismatched,'+str(mismatched)
    print >>ofs, 'no-test,'+str(noTest)
    print >>ofs, 'not-found,'+str(notFound)
    print >>ofs, 'unknown,'+str(unknown)
    print >>ofs, 'total,'+str(total)
    print >>ofs, 'passed,'+str(passed)
    print >>ofs, 'failed,'+str(failed)
    print >>ofs, 'tested,'+str(tested)
    print >>ofs, 'threshold,'+str(threshold)
    print >>ofs, ''
    ofs.close()

if not options.wlf_run:
    sys.exit(retCode)
