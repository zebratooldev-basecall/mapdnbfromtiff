#! /usr/bin/env python

import math,os,sys
from os.path import join as pjoin

def ReadAdmix(fn,ance,out):
    print "Reading",fn
    ff = open(fn)
    oo = open(out,"w")
    target=ff.readline().rstrip('\r\n').split(' ')
    sum={}
    index=0
    count={}
    for line in ff:
        fields = line.rstrip('\r\n').split(' ')
        region=ance[index]['region'] 
        if region not in sum:
            sum[region]=[]
            for i in range(0,len(target)):
                data=float(fields[i])
                sum[region].append(data)
            count[region]=1
        else:
            for i in range(0,len(target)):
                data=float(fields[i])
                sum[region][i]+=data
            count[region]+=1
        index+=1
    oo.write("freeform,Ancestry\n")
    oo.write("Region,Fraction\n")
    if index==0:
        oo.write("NA,0\n"); 
        return 0;
    for region in sum.keys():
        for i in range(0,len(target)):
            sum[region][i]=sum[region][i]/count[region]
    est={}
    for i in range(0,len(target)):
        max=0
        for region in sum.keys():
            if sum[region][i] > max:
                max=sum[region][i]
                est[i]=region
        oo.write(est[i]+","+target[i]+"\n")
    ff.close()

def ReadRegion(fn,ance):
   print 'Loading',fn
   ff = open(fn)
   index=0
   for line in ff:
       fields = line.rstrip('\r\n').split(' ')
       country=fields[2]
       region=fields[3]
       ance[index]={}
       ance[index]['country']=country
       ance[index]['region'] = region 
       index+=1
   ff.close()
 
Ancepath = os.path.abspath(pjoin(sys.argv[1],'etc'))
Outpath = os.path.abspath(sys.argv[2])
Ance = {}
ReadRegion(pjoin(Ancepath,'HGDP_hapmap_IN0.1_prune0.1.region'),Ance)
ReadAdmix(pjoin(Outpath,'TempAdmixture.7.Q'),Ance,pjoin(Outpath,'Ancestry.csv'))
