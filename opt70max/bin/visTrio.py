#! /usr/bin/python

import sys, os
from bisect import bisect
from optparse import OptionParser
from callfile import LocusStream, Locus, Call
from os.path import split as psplit, join as pjoin


def writeLoci(outfile, loci):
    out = open(outfile, "w")
    try:
        for (locus,comment) in loci:
            for line in comment:
                out.write('# %s\n' % line)
            out.write(str(locus)+"\n")
    finally:
        out.close()


def mkHtmlTable(fll):
    result = [ '<div class=\"alntablecontainer\"><table class=\"alntable\">',
               '<thead><tr>',
               ''.join(['<td class=\"pdnbs\" bgcolor=\"lightgrey\">%s</td>' % f for f in fll[0]]),
               '</tr></thead><tbody>' ]
    for fl in fll[1:]:
        result.append('<tr>')
        result.append(''.join(['<td class=\"pdnbs\">%s</td>' % f for f in fl]))
        result.append('</tr>')
    result.append('</tbody></table></div><br/>')
    return result


def gatherLociByStatus(fn, stati):
    ff = open(fn)
    try:
        loci = []
        header = ff.readline().strip()
        headerFields = header.split(",")
        statusId = headerFields.index("status")
        chrId = headerFields.index("chromosome/supercontig name")
        beginId = headerFields.index("begin")
        endId = headerFields.index("end")
        refId = headerFields.index("ref sequence")
        alleleIds = []
        for individual in [ "Ch", "P1", "P2" ]:
            for allele in xrange(0, 4):
                field = "%s allele%d" % (individual, allele)
                if field in headerFields:
                    alleleIds.append(headerFields.index(field))
        for line in ff:
            line = line.strip()
            fields = line.split(",")
            if fields[statusId] in stati:
                locus = Locus(int(fields[0]))
                alleleMap = { fields[refId] : 0 }
                for alleleId in alleleIds:
                    allele = fields[alleleId]
                    if allele not in alleleMap:
                        alleleMap[allele] = 1
                alleles = sorted([ (alleleMap[key], key) for key in alleleMap.keys() ])
                for ii in xrange(len(alleles)):
                    allele = alleles[ii][1]
                    call = Call([str(locus.id), str(len(alleles)), str(ii+1),
                                 fields[chrId], fields[beginId], fields[endId], "sub",
                                 fields[refId], allele, "0", "", ""])
                    locus.addCall(call)
                loci.append( (locus, ["".join(mkHtmlTable([ headerFields, fields]))]) )
        return loci
    finally:
        ff.close()


def system(cmd):
    print '-'*70
    print 'executing:'
    print cmd
    print '-'*70
    sts = os.system(cmd)
    if sts != 0:
        raise Exception('command execution failed with status: %d' % sts)


def main():
    parser = OptionParser('usage: %prog [options] <call-file-1> <call-file-2>')
    parser.disable_interspersed_args()
    parser.add_option('-o', '--output-dir', default='.',
                      help='Path to output directory for visualizations.')
    parser.add_option('-s', '--sample-size', type=int, default=100,
                      help='Max number of vis loci to use.')
    parser.add_option('--trio-output', default=None,
                      help='The path to the simplified trio output.')
    parser.add_option('--child', default=None,
                      help='Path to child call file.')
    parser.add_option('--parent1', default=None,
                      help='Path to parent1 call file.')
    parser.add_option('--parent2', default=None,
                      help='Path to parent2 call file.')

    (options, args) = parser.parse_args()

    for key in [ 'trio-output',
                 'child', 'parent1', 'parent2' ]:
        if options.__dict__[key.replace('-', '_')] is None:
            raise Exception("required arg is missing: %s" % key)

    if len(args) != 0:
        raise Exception("unexpected args")

    print "gathering incompatible superloci"
    loci = gatherLociByStatus(options.trio_output, [ "incompatible" ])
    lf = os.path.abspath(pjoin(options.output_dir, "Loci.csv"))
    writeLoci(lf,loci)

    origs = []
    for id in [ "child", "parent1", "parent2" ]:
        cfn = options.__dict__[id]
        od = pjoin(options.output_dir, id)
        if not os.path.exists(od):
            os.mkdir(od)

        origs.append(cfn)

    cgiHome = os.getenv("CGI_HOME")
    for id in [ "child", "parent1", "parent2" ]:
        od = pjoin(options.output_dir, id)
        
        lf = os.path.abspath(pjoin(options.output_dir, "Loci.csv"))
        cfn0 = options.__dict__[id]
        asmDir = psplit(psplit(psplit(cfn0)[0])[0])[0]
        cfn = ":".join(origs)
        sampleArg = "-p in.sample-size=%d" % options.sample_size
        cmd = ("driver.py -m grid -f %s/etc/workflows/human/VisualizeLoci -w %s "+
               "-p in.assembly-dir=%s "+
               "%s "+
               "-p in.locus-file=%s "+
               "-p in.call-file=%s") % ( cgiHome, os.path.abspath(od), asmDir,
                                              sampleArg,
                                              lf, cfn )
        ff = open(pjoin(od, "vis.sh"),"w")
        ff.write("#! /bin/bash\n\n%s\n" % cmd)
        ff.close()

        system(cmd)


if __name__ == '__main__':
    main()
