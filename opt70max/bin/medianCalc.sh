#!/bin/bash

COVDIR=$1
MEDIANDIR=$2
WIDTH=$3

# number of windows 
n=`zcat ${COVDIR}/smoothed_${WIDTH}/chr[1-9]*tsv.gz | grep -v '>' | wc -l`

# name of first file
first=`echo ${COVDIR}/smoothed_${WIDTH}/chr[1-9]*tsv.gz | awk '{print $1}'`

# column containing gcCorrectedCoverage, obtained by parsing header line of first file
gccol=`zcat $first | awk 'NR==1{gccol=-1;for(i=1;i<=NF;i++)if($i=="gcCorrectedCoverage"){gccol=i;break};print gccol}'`
if [ $gccol == "-1" ]; then
  echo "ERROR: Did not find gcCorrectedCoverage in header"
  exit -1
fi

# find median of gcCorrectedCoverage values: concatenate values, sort them, and print middle value
zcat ${COVDIR}/smoothed_${WIDTH}/chr[1-9]*tsv.gz |\
 grep -v '>' |\
 awk -v gccol=$gccol '{print $gccol}' |\
 sort -n |\
 awk -v n=$n -v width=${WIDTH} 'NR==int(n/2){print $1,n,width}' > ${MEDIANDIR}/median_depth
