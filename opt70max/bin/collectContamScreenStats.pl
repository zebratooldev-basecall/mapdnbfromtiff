#!/usr/bin/perl

$noheader=1;
foreach $x (@ARGV) {
    open(F,$x."/collection.xml") or next;
    $cls = '';
    $sample= '';
    $project= '';
    $slide= '';
    $lane= '';
    while (<F>) {
	if (/^\s+<Library(.*)name="(.*)"/){
	    $cls  = $2;
	}
	if (/^\s+<Lane(.*)name="(.*)"(.*)project="(.*)"(.*)sample="(.*)"/){
	    $lane=$2;
	    $project=$4;
	    $sample=$6;
	}
        if (/^\s+<Slide(.*)index="(.*)"(.*)name="(.*)"/){
	    $slide=$4
        }
    }
    close(F);
    open(F,$x."/ASM/contam-rollup/lane_contamination_estimates.txt") || !$noheader || die("can't open ASM/contam-rollup/lane_contamination_estimates.txt");
    $header = <F>;
    $header =~ s/\t/,/g  ;
    if ($noheader) {
	print "lane,project,sample,lib,${header}";
	$noheader = 0;
    }
    if ($header) {
	$data=<F>;
	$data =~ s/\t/,/g  ;
	printf "%s-%s,%s,%s,%s,%s",$slide, $lane, $project,$sample,$cls,$data
    }
}
