#! /usr/bin/env python

import os, subprocess, sys, wfclib.jsonutil
from os.path import join as pjoin, basename, splitext
from glob import glob
from optparse import OptionParser
from subprocess import Popen, PIPE


# Parents of 19240.
MOTHER='/rnd/home/jbaccash/archive/qual/trio-baseline-36/var-GS00028-DNA_A01-ASM.tsv'
FATHER='/rnd/home/jbaccash/archive/qual/trio-baseline-36/var-GS00028-DNA_B01-ASM.tsv'


def doCall(args):
    print '"' + '" \\\n"'.join(args) + '"'
    sts = subprocess.call(args)
    if sts != 0:
        raise Exception('%s failed with sts %d' % (args[0], sts))


def loadState(workDir, fnglob):
    fns = glob(os.path.join(workDir, 'state', fnglob))
    stateFiles = Popen(["ls", "-t"] + fns, stdout=PIPE).communicate()[0]
    stateFile = os.path.join(workDir, 'state', stateFiles.split()[0])
    return wfclib.jsonutil.load(stateFile)


def sanitizeVariantParams(oo, name):
    if os.path.isdir(oo.__dict__[name]):
        oo.__dict__[name+'_files'] = ':'.join(
            sorted(glob(pjoin(oo.__dict__[name], 'ASM', 'variations', 'annotated', 'Variations*.tsv'))))
    else:
        oo.__dict__[name+'_files'] = oo.__dict__[name]
        oo.__dict__[name+'_files'] = ':'.join( [ oo.__dict__[name] ] * (1 + oo.child_files.count(':')) )
    if oo.child_files.count(':') != oo.__dict__[name+'_files'].count(':'):
        raise Exception('child file counts != %s file counts: %s %s' %
                        (name, oo.child_files, oo.__dict__[name+'_files']))


def sanitizeParams(oo, args):
    if len(args) != 0:
        raise Exception('unexpected args')

    if oo.include_range == 'build36':
        oo.include_range = 'chr1,0,chrX,2709520'
    elif oo.include_range == 'build37':
        oo.include_range = 'chr1,0,chrX,2699520'

    sanitizeVariantParams(oo, 'child')
    sanitizeVariantParams(oo, 'father')
    sanitizeVariantParams(oo, 'mother')

    if oo.reference is None:
        if not os.path.isdir(oo.child):
            raise Exception('reference must be specified for one-child trio compare')
        state = loadState(oo.child, '*.VarCall.*')
        oo.reference = state.reference

    if not os.path.isdir(oo.output):
        os.makedirs(oo.output)


def runTask(oo, args):
    odir = oo.output

    reference = oo.reference
    child = oo.child_files.split(':')[oo.task]
    father = oo.father_files.split(':')[oo.task]
    mother = oo.mother_files.split(':')[oo.task]
    cfcsv = splitext(basename(child))[0] + '.csv'

    additionalParams = []
    if oo.detailed_output:
        additionalParams += [
            '--hypotheses-output=%s' % pjoin(odir, 'trio_hypotheses_%s' % cfcsv),
            '--verbose-report=%s' % pjoin(odir, 'trio_verbose_%s' % cfcsv),
            ]
    if oo.filter_nocalls:
        additionalParams += [ '--filter-nocalls' ]

    doCall(['trio',
            '--dump-env',
            '--dump-timings',
            '--dump-hypotheses=incompatible,compatible,identical',
            '--dbgout',
            '--include-range=%s' % oo.include_range,
            '--combine-loci-distance=%s' % oo.combine_loci_distance,
            '--combine-loci-distinct-3-mer-max=%s' % oo.combine_loci_distinct_3_mer_max,
            '--reference=%s' % reference,
            '--input=%s' % child,
            '--input=%s' % father,
            '--input=%s' % mother,
            '--stats-output=%s' % pjoin(odir, 'trio_stats_%s' % cfcsv),
            '--child-var-stats=%s' % pjoin(odir, 'trio_child_var_stats_%s' % cfcsv),
            '--report=%s' % pjoin(odir, 'trio_report_%s' % cfcsv),
            '--simplified-report=%s' % pjoin(odir, 'trio_simplified_%s' % cfcsv),
            '--skipped-loci=%s' % pjoin(odir, 'trio_skipped_%s' % cfcsv),
            '--skipped-loci-stat=%s' % pjoin(odir, 'trio_skipped_stat_%s' % cfcsv)
            ] +
           additionalParams
           )


def main():
    parser = OptionParser('usage: %prog [--task=TASKID] PARAMS')
    parser.disable_interspersed_args()
    parser.add_option('--output', default=pjoin(os.getcwd(), 'trio'),
                      help='Path to output directory for trio results.')
    parser.add_option('--child', default=os.getcwd(),
                      help='Path to child workDir or variant file.')
    parser.add_option('--father', default=FATHER,
                      help='Path to father workDir or variant file.')
    parser.add_option('--mother', default=MOTHER,
                      help='Path to mother workDir or variant file.')
    parser.add_option('--reference', default=None,
                      help='Path to reference gbi file.')
    parser.add_option('--combine-loci-distance', type=int, default=0,
                      help='Number of bases to extend superlocus.')
    parser.add_option('--combine-loci-distinct-3-mer-max', type=int, default=4,
                      help='Number of distinct 3-mers to extend superlocus.')
    parser.add_option('--include-range', default='build36',
                      help='Range of reference bases to include in the comparison.')
    parser.add_option('--detailed-output', action="store_true", default=False,
                      help='Also dump hypotheses and verbose output.')
    parser.add_option('--filter-nocalls', action="store_true", default=False,
                      help='Pass filter-nocalls param to trio.')
    parser.add_option('--task', type=int, default=None,
                      help='Which task to run, otherwise submits array grid job.')

    (options, args) = parser.parse_args()
    sanitizeParams(options, args)

    if options.task is not None:
        runTask(options, args)
    else:
        doCall(['rat',
                'run',
                '--memory=1',
                '--work-dir=%s' % options.output,
                '--params=%s' % ','.join(str(x) for x in xrange(0, 1+options.child_files.count(':'))),
                "'" + "' '".join(sys.argv) + "' --task=$PARAM"
                ])


if __name__ == '__main__':
    main()
