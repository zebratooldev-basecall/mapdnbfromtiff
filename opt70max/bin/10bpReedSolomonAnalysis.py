#!/usr/bin/python

"""

"""

__author__ = "Anushka Brownley"
__version__ = "$RCSId$"
__date__ = "$Date$"
__copyright__ = "Copyright (c) 2008 Complete Genomics"
__license__ = "Proprietary"

import os.path
import sys
import getopt
from rsLookupMetrics import FieldMetrics, LaneMetrics, LookupTable

CGIHome = os.environ['CGI_HOME']
CGIPath = os.environ['LD_LIBRARY_PATH']
DNB_Len = 10
baseLST = ['A','T','G','C']

if os.environ.get('CGI_HOME') is None:
    print "Please set CGI_HOME."
    sys.exit(-4)

print "CGI Executables found in path " + CGIPath
from CGIPipeline import *

################################
def makeDir(dirPath):
    if not(os.path.isdir(dirPath)):
        os.mkdir(dirPath, 0775)

################################
def usage(script):
    print "\nThis script takes an csv file sequencer run information and a template ADF"
    print "input XML file and outputs a modified ADF input XML file to run through make ADF\n"
    print script
    print "    -h, --help         <arg>   outputs this help menu"
    print "    -a, --adf          <arg>   path to ADF file"
    print "    -s, --slideID      <arg>   slide ID "
    print "    -l, --laneID       <arg>   lane ID"
    print "    -o, --output-dir   <arg>   output directory"
    print "    -r, --rev-comp     <arg>   True/False reverse complements RS code"
    print "    -c, --cycle-list   <arg>   underscore-separated list of cycles in RS position order"
    print "    -p, --pos-list     <arg>   comma-separated list of positions associated with cycles\n"
    
################################
def complement(seq):
    complementDCT = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'N': 'N'}
    complseq = [complementDCT[base.upper()] for base in seq]
    return "".join(complseq)

################################
def main(argv):
    script = argv[0]
    curPath = os.path.dirname(os.path.abspath(script))
    argv = argv[1:]
    adfFile = None
    slideID = None
    laneID = None
    revComp = False
    cycleData = None
    lookupTable = CGIHome + "/etc/config/ReedSolomon/10_bp_lookupTable.csv"
    lookupTable20bp = CGIHome + "/etc/config/ReedSolomon/20_bp_lookupTable.txt"
    posLST = [0,1,2,3,4,5,6,7,8,9]
    
    try:
        opts, args = getopt.getopt(argv, "h:a:s:l:o:r:c:p:", ["help=", "adf=", "slideID=", "laneID=", "output-dir=", "rev-comp=", "cycle-list=", "pos-list="])
    except getopt.GetoptError:
        usage(script)
        sys.exit(1)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(script)
            sys.exit(0)
        elif opt in ("-a", "--adf"):
            adfFile = arg
        elif opt in ("-s", "--slideID"):
            slideID = arg
        elif opt in ("-l", "--laneID"):
            laneID = arg
        elif opt in ("-o", "--output-dir"):
            outputDir = arg
        elif opt in ("-c", "--cycle-list"):
            cycleData = arg
        elif opt in ("-p", "--pos-list"):
            posData = arg
            posLST = posData.split(",")
        elif opt in ("-r", "--rev-comp"):
            if arg == "True":
                revComp = True
            
    # validate input
    if adfFile is None or slideID is None or laneID is None or cycleData is None:
        print "Please enter all required input parameters, adf file, slideID, laneID, and cycle list."
        usage(script)
        sys.exit(1)

    if not(os.path.isfile(adfFile)):
        print "Please enter valid ADF file"
        usage(script)
        sys.exit(1)

    if not(len(posLST) == 10):
        print "ERROR: RS structure must contain 10 bp positions\n"
        sys.exit(1)
    
    # load lookup table and ADF
    lookupTableOBJ = LookupTable(lookupTable, lookupTable20bp,DNB_Len)
    laneMetricsOBJ = LaneMetrics(DNB_Len,1)
    collection = Collection(adfFile,'./')
    lane = collection.getLane(slideID + ":" + laneID)
    fields = lane.getFields()
    cycles = lane.getCycles()
    
    # Set up output directories and files
    makeDir(outputDir)
    outputFile = outputDir + "/" + slideID + "_" + laneID + "_" + cycleData + ".csv"
    outFH = open(outputFile, "w")
    laneMetricsOBJ.printHeader(outFH)
    fieldOutFile = outputDir + "/" + slideID + "_" + laneID + "_" + cycleData + "_byField.csv"
    fieldOutFH = open(fieldOutFile, "w")
    laneMetricsOBJ.printHeader(fieldOutFH)
    print "Total number of fields:",len(fields)
    for i in range(0, len(fields)):
        field = fields[i]
        print "Field name:", field.getName(), "Field index:", field.getIndexWithinLane(),"Field size:",field.size()
        if field.getRows() == 0 or field.getColumns() == 0:
            continue
            
        fieldMetricsOBJ = FieldMetrics(DNB_Len,field.getRows(),field.getColumns(),1)
        # lookup each 10 bp read segment in the lookup table and track segment level metrics
        for dnb in field:
            fieldMetricsOBJ.incrementTotalDNBs(1)
            nonRegisteredCount = 0
            obsSeq = ''
            nMerScore = []
            nMerScore2 = []
            
            # construct 10 bp sequence/score data
            for newbaseInd in posLST:
                score = dnb.getRawBaseScore(int(newbaseInd))
                nMerScore.append(score)
                if score == 201:
                    nonRegisteredCount += 1
                    if revComp:
                        obsSeq = "N" + obsSeq
                    else:
                        obsSeq += "N"
                else:
                    if revComp:
                        obsSeq = dnb.getBases().get(int(newbaseInd)).upper() + obsSeq
                    else:
                        obsSeq += dnb.getBases().get(int(newbaseInd)).upper()
            
            if revComp:
                nMerScore.reverse()
                obsSeq = complement(obsSeq)
            
            # for segments with <=1 no-call find segment in lookup table, else count segment nocalls
            if nonRegisteredCount >= 1:
                fieldMetricsOBJ.incrementTotalNoCallDNBs(1)
                fieldMetricsOBJ.setDuplicate(dnb.getId().getOffsetInLane(), field.getIndexWithinLane(), field.size())
                continue
            else:
                (expSeq,exp20Seq) = lookupTableOBJ.seqLookup(obsSeq)

            # determine if DNB is horizontal or vertical duplicate
            # count raw and error corrected code matches per position
            if not exp20Seq == '':
                if expSeq == obsSeq:
                    fieldMetricsOBJ.incrementTotalRawMappedDNBs(1)
                    fieldMetricsOBJ.incrementAveScoreCount(nMerScore,obsSeq, expSeq)
                else:
                    fieldMetricsOBJ.incrementTotalCorrectedDNBs(1)
                    fieldMetricsOBJ.incrementAveScoreCount(nMerScore,obsSeq, expSeq)
                
                fieldMetricsOBJ.calculateDuplication(dnb.getId().getOffsetInLane(), expSeq, field.getIndexWithinLane(), field.size())
            else:
                if expSeq == '':
                    fieldMetricsOBJ.setDuplicate(dnb.getId().getOffsetInLane(), field.getIndexWithinLane(), field.size())
                    fieldMetricsOBJ.incrementUnmappedDNBs(1)
                else:
                    fieldMetricsOBJ.incrementTotalAltDNBs(1)
                    fieldMetricsOBJ.calculateDuplication(dnb.getId().getOffsetInLane(), obsSeq, field.getIndexWithinLane(), field.size())
        
        # calculate accuracy for field
        laneMetricsOBJ.incrementLaneData(fieldMetricsOBJ)
        field = field.getName()
        col = field[3:6]
        row = field[6:9]
        fieldMetricsOBJ.printMetrics(fieldOutFH,curPath,slideID,laneID,field,row,col)

    # print lane level metrics 
    laneMetricsOBJ.printMetrics(outFH,curPath,slideID,laneID,len(fields),1,1)
    outFH.close()
    fieldOutFH.close()
    
    
################################
if __name__ == "__main__":
    main(sys.argv)
