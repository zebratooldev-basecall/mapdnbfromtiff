#! /usr/bin/env python

import sys,os,re
from os.path import basename, join as pjoin
from glob import glob


def glob1(fn):
    fns = glob(fn)
    if len(fns) != 1:
        raise Exception('glob1: '+fn)
    return fns[0]

class Table(object):
    def __init__(self, type, name):
        self.type = type
        self.name = name
        self.data = []

    def byRcName(self, rowName, colName):
        for xid in xrange(len(self.data)):
            if self.data[xid][0] == rowName:
                break
        else:
            raise Exception('Row not found: %s.%s' % (self.name,rowName))
        for yid in xrange(len(self.data[0])):
            if self.data[0][yid] == colName:
                break
        else:
            raise Exception('Column not found: %s.%s' % (self.name,colName))
        return self.data[xid][yid]


def readTable(ff, table, separator=','):
    while True:
        line = ff.readline()
        if '' == line or line.isspace():
            return
        line = line.rstrip('\r\n')
        table.data.append(map(lambda x:x.strip(),line.split(separator)))


def loadTables(fn, tables, separator=',', nameSuffix=''):
    ff = open(fn)
    while True:
        line = ff.readline()
        if '' == line:
            break
        if line.startswith('#'):
            continue
        if line.isspace():
            continue
        line = line.rstrip('\r\n')
        type = 'freeform'
        if line.find(separator) != -1:
            (type,name) = line.split(',')
        else:
            name = line
        table = Table(type, name + nameSuffix)
        readTable(ff, table, separator)
        tables[table.name] = table
    ff.close()


def loadTrioTables(fn, tables, name):
    table = Table('freeform', name)
    ff = open(fn)
    header = ff.readline().rstrip('\r\n')
    if header != 'score,annotation,count':
        raise Exception('bad header')
    table.data.append(header.split(',')[1:])
    while True:
        line = ff.readline().rstrip('\r\n')
        if '' == line:
            break
        table.data.append(line.split(',')[1:])
    ff.close()
    tables[table.name] = table


def loadSomaticData(fn, somatic):
    ff = open(fn)
    header = ff.readline().rstrip('\r\n')
    hFields = header.split('\t')
    if hFields[8] != 'varType' or hFields[-1] != 'SomaticScore':
        raise Exception('bad header')
    for line in ff:
        line = line.rstrip('\r\n')
        fields = line.split('\t')
        vt = fields[8]
        if vt not in somatic:
            somatic[vt] = []
        somatic[vt].append(float(fields[-1]))
    ff.close()


class GenomeData(object):
    def __init__(self, name, workDir, threshold, xtra=''):
        self.name = name
        self.workDir = workDir
        self.threshold = threshold
        self.xtra = xtra
        self.tables = {}
        loadTables(pjoin(self.workDir, 'ASM', 'callrpt', 'callrpt_Variations%s.csv' % self.threshold), self.tables)
        loadTables(pjoin(self.workDir, 'ASM', 'callrpt', 'dbsnprpt_Variations%s.csv' % self.threshold), self.tables)
        loadTables(pjoin(self.workDir, 'ASM', 'callrpt', 'callannotaterpt_Variations%s.csv' % self.threshold), self.tables)
        fnSnpDiff = pjoin(self.workDir, 'snpdiff', 'snpdiff_hapmap_%sStats.tsv' % self.threshold)
        if os.path.exists(fnSnpDiff):
            loadTables(fnSnpDiff, self.tables, '\t', ' hapmap_'+self.threshold)
        fnSnpDiff = pjoin(self.workDir, 'snpdiff', 'snpdiff_infinium_%sStats.tsv' % self.threshold)
        if os.path.exists(fnSnpDiff):
            loadTables(fnSnpDiff, self.tables, '\t', ' infinium_'+self.threshold)
        fnTrioB = pjoin(self.workDir, 'trio-v-baseline', 'trio_stats_Variations%s.csv' % self.threshold)
        if os.path.exists(fnTrioB):
            loadTrioTables(fnTrioB, self.tables, 'trio-v-baseline %s' % self.threshold)
        self.somatic = {}
        fnSomatic = pjoin(self.workDir, 'calldiff', '%sSomaticOutput.tsv' % self.threshold)
        if os.path.exists(fnSomatic):
            loadSomaticData(fnSomatic, self.somatic)
        fn1kg = pjoin(self.workDir, '1kg', '%s-1kgstats.csv' % self.threshold)
        if os.path.exists(fn1kg):
            loadTables(fn1kg, self.tables)


    # Return SNP transition/transversion ratio.
    # regions: '' -> genome, 'CodingRegionsOnly' -> exome
    def getTiTv(self, region=''):
        table = self.tables['snpTrans' + region + self.threshold]
        return float(table.byRcName('transitions','Value')) / float(table.byRcName('transversions','Value'))

    # Return count of loci.
    # varType: SNP, INS, DEL, SUB
    # zygosity: REF_HET, HOM, OPPOSITE_NOCALL, TOTAL_LINE
    # region: '', CodingRegionsOnly
    def getLocusCount(self, varType, zygosity, region=''):
        varType  = varType .upper().replace('-','_')
        zygosity = zygosity.upper().replace('-','_')
        table = self.tables['variationCount' + region + self.threshold]
        return int(table.byRcName(varType.upper(), zygosity.upper()))

    # Returns novelty rate.
    # varType: SNP, INS, DEL, SUB
    # zygosity: refHet, hom, '' (total)
    # region: '', Coding
    def getNovelty(self, varType, zygosity, region=''):
        varType = varType.upper()
        num = zygosity + 'Novel'
        den = zygosity + 'Total'
        if '' == zygosity:
            num = 'novel'
            den = 'total'
        table = self.tables['verboseNovelVsDbSnpCount' + region + self.threshold]
        return float(table.byRcName(varType, num)) / float(table.byRcName(varType, den))

    # region: '', CodingRegionsOnly
    def getCallRate(self, region=''):
        table = self.tables['calledBaseCount' + region + self.threshold]
        return float(table.byRcName('called', 'total')) / float(table.byRcName('reference', 'total'))

    # impact: synonymousSnp, missenseSnp, nonsenseSnp, nonstopSnp,
    #         misstartSnp, disruptSnp, frameShiftingIns, framePreservingIns,
    #         frameUnknownsIns, frameShiftingDel, framePreservingDel,
    #         frameUnknownsDel, frameShiftingSub, framePreservingSub,
    #         frameUnknownsSub, frameShiftingIndel, framePreservingIndel,
    #         frameUnknownsIndel
    # zygosity: het, hom, other, total
    def getLocusImpactCount(self, impact, zygosity='total'):
        table = self.tables['functionalAnnotation' + self.threshold]
        return int(table.byRcName(impact, zygosity))

    def getNonSynonymousSnpLocusCount(self, zygosity='total'):
        table = self.tables['functionalAnnotation' + self.threshold]
        impacts = [ 'missenseSnp', 'nonsenseSnp', 'nonstopSnp', 'misstartSnp' ]
        return sum([int(table.byRcName(impact, zygosity)) for impact in impacts])

    # baseline: hapmap, infinium
    # metric: discordance, nocall
    # genotype: ref-ref, het-ref-alt, het-alt-alt, hom-alt-alt
    def getSnpDiffLocusFraction(self, baseline='hapmap', metric='discordance', genotype='total'):
        table = self.tables['Locus concordance by fraction %s_%s' % (baseline,self.threshold)]
        return float(table.byRcName(genotype, metric))

    # metric: identical, compatible, incompatible
    def getTrioSuperlocusCount(self, metric):
        table = self.tables['trio-v-baseline %s' % self.threshold]
        return int(table.byRcName(metric, 'count'))

    # varType: snp, ins, del, sub
    # relativeSensitivity: 0...1
    def getSomaticCount(self, varType, relativeSensitivity):
        table = self.somatic[varType]
        minScore = 1.0 - relativeSensitivity
        count = 0
        for score in table:
            if score >= minScore - .00001:
                count += 1
        return count

    def getSensitivityForSomaticCount(self, varType, somaticCount):
        relativeSensitivity = 1.0
        if len(self.somatic[varType]) >= somaticCount:
            table = sorted(self.somatic[varType], reverse=True)
            relativeSensitivity = 1.0 - table[somaticCount]
        locusCount = 0
        for zygosity in [ 'REF-HET', 'HOM', 'OPPOSITE_NOCALL' ]:
            locusCount += self.getLocusCount(varType.upper(), zygosity)
        return int(locusCount * relativeSensitivity)

    # method: testvariants, dbsnptool
    # matchCat: nc, called0, match1, match
    def get1kgIndelCount(self, method, matchCat):
        return int(self.tables['compare1kgindel'].byRcName(method, matchCat))
        

class DataWriter(object):
    def __init__(self, run, threshold, genomes):
        self.run = run
        self.threshold = threshold
        match = re.match('([0-9]*)-([0-9]*)_ccf([0-9]*)', threshold)
        self.homScoreThreshold = match.group(1)
        self.hetScoreThreshold = match.group(2)
        self.ccfThreshold = match.group(3)
        self.genomes = genomes
        self.format = lambda x:x

    def write(self, metric, getMetric):
        for genome in self.genomes:
            run = self.run
            try:
                mm = getMetric(genome)
            except:
                sys.stderr.write('not found: %s,%s,%s,%s,%s\n' %
                                 (run,self.homScoreThreshold,self.hetScoreThreshold,
                                  genome.name,metric))
                continue
            print '%s,%s,%s,%s,%s,%s,%s' % (run,self.homScoreThreshold,self.hetScoreThreshold,
                                            self.ccfThreshold,genome.name,metric,self.format(getMetric(genome)))


print 'run,homScoreThreshold,hetScoreThreshold,ccfThreshold,genome,metric,value'
for run in sys.argv[1:]:
    avars = glob(pjoin(run, 'NA19240-A', 'ASM', 'variations',
                       'annotated', 'Variations*ccf*.tsv'))
    scoreThresholds = [ basename(avar)[10:-4] for avar in avars ]
    for threshold in scoreThresholds:
        rA = GenomeData('NA19240-A', pjoin(run,'NA19240-A'), threshold, '')
        rB = GenomeData('NA19240-B', pjoin(run,'NA19240-B'), threshold, '')

        dw = DataWriter(run, threshold, [rA,rB])
        dw.format = lambda x:'%.3f%%' % (100.0*x)
        dw.write('Base Fraction Fully Called Genome', lambda x:x.getCallRate())
        dw.write('Base Fraction Fully Called Exome', lambda x:x.getCallRate('CodingRegionsOnly'))
        dw.format = lambda x:'%.4f' % x
        dw.write('SNP Transitions/Transversions Genome', lambda x:x.getTiTv())
        dw.write('SNP Transitions/Transversions Exome', lambda x:x.getTiTv('CodingRegionsOnly'))
        for varType in [ 'SNP', 'INS', 'DEL', 'SUB' ]:
            dw.format = lambda x:x
            for zygosity in [ 'REF-HET', 'HOM', 'OPPOSITE_NOCALL' ]:
                dw.write('Locus Count %s %s' % (varType,zygosity), lambda x:x.getLocusCount(varType,zygosity))
            dw.format = lambda x:'%.3f%%' % (100.0*x)
            for zygosity in [ 'refHet', 'hom' ]:
                dw.write('Novelty Genome %s %s' % (varType,zygosity), lambda x:x.getNovelty(varType, zygosity))
        dw.format = lambda x:x
        for varType in [ 'Ins', 'Del', 'Sub' ]:
            dw.write('Frame Shifting Loci %s' % varType, lambda x:x.getLocusImpactCount('frameShifting'+varType))
        dw.write('Non-synonymous SNP Loci', lambda x:x.getNonSynonymousSnpLocusCount())

        dw.format = lambda x:'%.3f%%' % (100.0*x)
        for base in [ 'hapmap', 'infinium']:
            for metric in [ 'discordance', 'nocall' ]:
                dw.write('Snpdiff %s %s' % (base,metric), lambda x:x.getSnpDiffLocusFraction(base, metric))
#        for metric in [ 'identical', 'compatible', 'incompatible' ]:
#            dw.write('Trio versus baseline %s' % metric, lambda x:x.getTrioSuperlocusCount(metric))

        dw.format = lambda x:x
        for method in [ 'dbsnptool-relaxed' ]:
            for matchCat in [ 'nc', 'called0', 'match1', 'match' ]:
                dw.write('1kg %s (by %s)' % (matchCat, method), lambda x:x.get1kgIndelCount(method, matchCat))

        dw.format = lambda x:x
        for varType in [ 'snp', 'ins', 'del', 'sub' ]:
            for somaticCount in [ 500 ]:
                dw.write('Sensitivity (loci) for %d somatic %s' % (somaticCount, varType), lambda x:x.getSensitivityForSomaticCount(varType, somaticCount))
            for rs in [ .90, .95, 1.00 ]:
                dw.write('Somatic count %s relative sensitivity %.2f' % (varType, rs), lambda x:x.getSomaticCount(varType, rs))
