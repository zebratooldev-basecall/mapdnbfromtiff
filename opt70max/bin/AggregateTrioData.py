#!/usr/bin/env python

import os
import sys
import re
import getopt

def main():

    # parse command line options and update variables
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:w:", [])
    except getopt.GetoptError:
        sys.exit(usage)

    for opt, arg in opts:
        if opt == "-h":
            sys.exit('Use me properly.')
        elif opt == '-w':
            WORKDIR = arg
        elif opt == '-i':
            INFILES = arg.split(",")
        elif opt == '-o':
            OUTFILE = arg
            
    ## Write the header (taken from teh first file)
    outf = open(OUTFILE, 'w')
    outf.write( "Group\tTrio")
    infile = open( INFILES[0] )
    for line in infile:
        (num, type, everything) = line.split(None, 2)
        outf.write("\t" + type )
    outf.write("\n")
    infile.close()

    ## Write all the numbers from all the files
    for file in INFILES:
        (group, trio, filename) = re.sub("^/", "", re.sub(WORKDIR, "", file)).split("/")
        outf.write(group + "\t" + trio)
        infile = open(file, 'r')
        for line in infile:
            (num, everything) =  line.split(None, 1)
            outf.write("\t" + num)
        outf.write("\n")
        infile.close()
            
###################################################

if __name__ == "__main__":
    main()
