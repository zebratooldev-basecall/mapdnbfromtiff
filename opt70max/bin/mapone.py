#! /usr/bin/env python

import os,optparse
from os.path import dirname, basename
import tempfile

CMDS = {
         # Default arguments, like what is run during the Map stage.
         'default': '''
TeraMap \
--single-dnb "@DNB@" \
--skip-index-check \
--text-output="1" \
--dump-env \
--dump-timings \
--reference="@REFDIR@/reference.gbi" \
--dnb-collection="@WORKDIR@/collection.xml" \
--gaps-dir="@WORKDIR@/allgaps" \
--index-config="@REFDIR@/55or70_0to2_4to7_index/35+35_0to2_4to7_nomutate.config" \
--index-dir="@REFDIR@/55or70_0to2_4to7_index" \
--output="@OUTDIR@" \
--file-name-pattern="@OUTFN@-00-" \
--array-job-count="1" \
--array-job-index="00" \
--min-excess-bases="3,3" \
--genome-base-count="0" \
--max-cost="5,5" \
--local-search \
--min-ls-excess-bases="7" \
--local-search-cost="5,5" \
--min-ls-call-count="12" \
--score-threshold="0" \
--no-call-limit="3" \
--match-buffer-size="80" \
--max-output="27" \
--threads="1" \
--neglected-small-gap-fraction="0.001" \
--gap-range="1,-4,0" \
--gap-range="2,0,2" \
--gap-range="3,4,7" \
--gap-range="5,4,7" \
--gap-range="6,0,2" \
--gap-range="7,-4,0" \
--mate-gap-range="0,700" \
--enable-local-search-if-consistent="1" \
--enable-multi-read-local-search="0" \
--max-index-matches="40" \
--sampling-fraction="1.0" >/dev/null
''',
         # Big buffers to minimize risk of index overflow etc.
         'bigbuf': '''
TeraMap \
--single-dnb "@DNB@" \
--skip-index-check \
--text-output="1" \
--dump-env \
--dump-timings \
--reference="@REFDIR@/reference.gbi" \
--dnb-collection="@WORKDIR@/collection.xml" \
--gaps-dir="@WORKDIR@/allgaps" \
--index-config="@REFDIR@/55or70_0to2_4to7_index/35+35_0to2_4to7_nomutate.config" \
--index-dir="@REFDIR@/55or70_0to2_4to7_index" \
--output="@OUTDIR@" \
--file-name-pattern="@OUTFN@-00-" \
--array-job-count="1" \
--array-job-index="00" \
--min-excess-bases="3,3" \
--genome-base-count="0" \
--max-cost="5,5" \
--local-search \
--min-ls-excess-bases="7" \
--local-search-cost="5,5" \
--min-ls-call-count="12" \
--score-threshold="0" \
--no-call-limit="3" \
--match-buffer-size="1000000" \
--max-output="1000000" \
--threads="1" \
--neglected-small-gap-fraction="0.001" \
--gap-range="1,-4,0" \
--gap-range="2,0,2" \
--gap-range="3,4,7" \
--gap-range="5,4,7" \
--gap-range="6,0,2" \
--gap-range="7,-4,0" \
--mate-gap-range="0,700" \
--enable-local-search-if-consistent="1" \
--enable-multi-read-local-search="0" \
--max-index-matches="1000000" \
--sampling-fraction="1.0" >/dev/null
''',
         'bigbuf_wide': '''
TeraMap \
--single-dnb "@DNB@" \
--skip-index-check \
--text-output="1" \
--dump-env \
--dump-timings \
--reference="@REFDIR@/reference.gbi" \
--dnb-collection="@WORKDIR@/collection.xml" \
--gaps-dir="@WORKDIR@/allgaps" \
--index-config="@REFDIR@/55or70_0to3_2to8_index/removedIndex_35+35_0to3_2to8_nomutate.config" \
--index-dir="@REFDIR@/55or70_0to3_2to8_index" \
--output="@OUTDIR@" \
--file-name-pattern="@OUTFN@-00-" \
--array-job-count="1" \
--array-job-index="00" \
--min-excess-bases="3,3" \
--genome-base-count="0" \
--max-cost="5,5" \
--local-search \
--min-ls-excess-bases="7" \
--local-search-cost="5,5" \
--min-ls-call-count="12" \
--score-threshold="0" \
--no-call-limit="3" \
--match-buffer-size="1000000" \
--max-output="1000000" \
--threads="1" \
--neglected-small-gap-fraction="0.001" \
--gap-range="1,-4,1" \
--gap-range="2,0,3" \
--gap-range="3,-3,8" \
--gap-range="5,2,8" \
--gap-range="6,0,3" \
--gap-range="7,-4,1" \
--mate-gap-range="0,1000" \
--enable-local-search-if-consistent="1" \
--enable-multi-read-local-search="0" \
--max-index-matches="1000000" \
--sampling-fraction="1.0" >/dev/null
''',
         'bigbuf_wide_nomategap': '''
TeraMap \
--single-dnb "@DNB@" \
--skip-index-check \
--text-output="1" \
--dump-env \
--dump-timings \
--reference="@REFDIR@/reference.gbi" \
--dnb-collection="@WORKDIR@/collection.xml" \
--gaps-dir="@WORKDIR@/allgaps" \
--index-config="@REFDIR@/55or70_0to3_2to8_index/removedIndex_35+35_0to3_2to8_nomutate.config" \
--index-dir="@REFDIR@/55or70_0to3_2to8_index" \
--output="@OUTDIR@" \
--file-name-pattern="@OUTFN@-00-" \
--array-job-count="1" \
--array-job-index="00" \
--min-excess-bases="3,3" \
--genome-base-count="0" \
--max-cost="5,5" \
--local-search \
--min-ls-excess-bases="7" \
--local-search-cost="5,5" \
--min-ls-call-count="12" \
--score-threshold="0" \
--no-call-limit="3" \
--match-buffer-size="1000000" \
--max-output="1000000" \
--threads="1" \
--neglected-small-gap-fraction="0.001" \
--gap-range="1,-4,1" \
--gap-range="2,0,3" \
--gap-range="3,-3,8" \
--gap-range="5,2,8" \
--gap-range="6,0,3" \
--gap-range="7,-4,1" \
--mate-gap-range="0,0" \
--enable-local-search-if-consistent="1" \
--enable-multi-read-local-search="0" \
--max-index-matches="1000000" \
--sampling-fraction="1.0" >/dev/null
''',
         # Big buffers to minimize risk of index overflow etc., 2 adapters
         'bigbuf2adapters': '''
TeraMap \
--single-dnb "@DNB@" \
--skip-index-check \
--text-output="1" \
--dump-env \
--dump-timings \
--reference="@REFDIR@/reference.gbi" \
--dnb-collection="@WORKDIR@/collection.xml" \
--gaps-dir="@WORKDIR@/allgaps" \
--index-config="@REFDIR@/55or70_0to2_4to7_index/25+25_minus1to2.config" \
--index-dir="@REFDIR@/55or70_0to2_4to7_index" \
--output="@OUTDIR@" \
--file-name-pattern="@OUTFN@-00-" \
--array-job-count="1" \
--array-job-index="00" \
--min-excess-bases="3,3" \
--genome-base-count="0" \
--max-cost="5,5" \
--local-search \
--min-ls-excess-bases="7" \
--local-search-cost="5,5" \
--min-ls-call-count="12" \
--score-threshold="0" \
--no-call-limit="3" \
--match-buffer-size="1000000" \
--max-output="1000000" \
--threads="1" \
--neglected-small-gap-fraction="0.001" \
--gap-range 1,-1,2 \
--gap-range 2,0,0 \
--gap-range 4,-1,2 \
--gap-range 5,0,0 \
--mate-gap-range="0,700" \
--enable-local-search-if-consistent="1" \
--enable-multi-read-local-search="0" \
--max-index-matches="1000000" \
--sampling-fraction="1.0" 
''',
         # Big buffers to minimize risk of index overflow etc. Also
         # 0-length mate gap range, to avoid filtering of inconsistent
         # arm mappings. (May also cause localSearch not to run.)
         'bigbuf_nomategap': '''
TeraMap \
--single-dnb "@DNB@" \
--skip-index-check \
--text-output="1" \
--dump-env \
--dump-timings \
--reference="@REFDIR@/reference.gbi" \
--dnb-collection="@WORKDIR@/collection.xml" \
--gaps-dir="@WORKDIR@/allgaps" \
--index-config="@REFDIR@/55or70_0to2_4to7_index/35+35_0to2_4to7_nomutate.config" \
--index-dir="@REFDIR@/55or70_0to2_4to7_index" \
--output="@OUTDIR@" \
--file-name-pattern="@OUTFN@-00-" \
--array-job-count="1" \
--array-job-index="00" \
--min-excess-bases="3,3" \
--genome-base-count="0" \
--max-cost="5,5" \
--local-search \
--min-ls-excess-bases="7" \
--local-search-cost="5,5" \
--min-ls-call-count="12" \
--score-threshold="0" \
--no-call-limit="3" \
--match-buffer-size="1000000" \
--max-output="1000000" \
--threads="1" \
--neglected-small-gap-fraction="0.001" \
--gap-range="1,-4,0" \
--gap-range="2,0,2" \
--gap-range="3,4,7" \
--gap-range="5,4,7" \
--gap-range="6,0,2" \
--gap-range="7,-4,0" \
--mate-gap-range="0,0" \
--enable-local-search-if-consistent="1" \
--enable-multi-read-local-search="0" \
--max-index-matches="1000000" \
--sampling-fraction="1.0" >/dev/null
''',
         }

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.add_option('--reference', default=None,
                      help='Reference gbi file name.')
    parser.add_option('--workdir', default=None,
                      help='Workdir of genome.')
    parser.add_option('--mode', default='default',
                      help='Mapping mode. One of %s.' % (','.join(sorted(CMDS.keys()))))
    parser.add_option('--dnb', default=None,
                      help='The ID of the DNB to map.')
    parser.add_option('--output-prefix', default='./map',
                      help='Output prefix to use.')

    (opts, args) = parser.parse_args()
    if args:
        parser.error('unexpected arguments')

    for required in [ 'reference', 'workdir', 'dnb' ]:
        if required not in opts.__dict__:
            parser.error('required param missing: '+required)

    if opts.mode not in CMDS:
        parser.error('mode not found: '+opts.mode)

    cmd = CMDS[opts.mode]
    for (sfrom,sto) in [ ('@REFDIR@', dirname(opts.reference)),
                         ('@WORKDIR@', opts.workdir),
                         ('@DNB@', opts.dnb),
                         ('@OUTFN@', basename(opts.output_prefix)),
                         ('@OUTDIR@', dirname(opts.output_prefix)),
                         ]:
        cmd = cmd.replace(sfrom,sto)
    return os.system(cmd)

if __name__ == '__main__':
    main()
