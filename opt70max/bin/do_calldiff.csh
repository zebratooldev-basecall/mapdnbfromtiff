#!/bin/tcsh -efx


set reference=$1 

set cutoff=$2

set ccfdelta=$3
set vsttwo=$4

cd variations/ccf
calldiff --dump-env --dump-timings -r $reference --input ../../Mutations.tsv --input Variations${cutoff}-${vsttwo}_ccf${ccfdelta}.tsv --stats calldiff-stats_${cutoff}-${vsttwo}_ccf${ccfdelta}.tsv --verbose calldiff-verbose_${cutoff}-${vsttwo}_ccf${ccfdelta}.tsv --output calldiff-output_${cutoff}-${vsttwo}_ccf${ccfdelta}.tsv
cd ../..

exit 0
