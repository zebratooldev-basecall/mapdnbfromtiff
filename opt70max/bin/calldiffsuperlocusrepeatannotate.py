#!/usr/bin/python

# usage: prog superlocus.tsv repeattable.txt
# suitable repeat table in /Proj/Assembly/Shared/wormhole-annotations/build36/rmsk-ordered.txt

def chrom_no(chrom):
    if not chrom.startswith('chr'):
        raise ValueError()
    if chrom == 'chrX':
        return 23
    elif chrom == 'chrY':
        return 24
    elif chrom == 'chrM':
        return 25
    return int(chrom[3:])


class Repeats(object):
    def __init__(self, fn):
        self.f = open(fn, 'r')
        self.f.readline() # skip header
        self.next()

    def next(self):
        while True:
            line = self.f.readline()
            if not line:
                self.chrom=30
                self.begin = self.end = 4000000000
                self.name = 'end'
                self.divergence = 0
                return
            _, _, divc, delc, insc, chrom, b, e, _, _, _, repc, repf =  line.split('\t')[:13]

            #import pdb; pdb.set_trace()

            try:
                self.chrom = chrom_no(chrom)
            except ValueError:
                continue
            self.begin = int(b)
            self.end = int(e)
            self.name = repf
            self.divergence = float(int(divc) + int(delc) + int(insc)) / 10
            return

    def forward(self, chrom, beg):
        while self.chrom < chrom:
            self.next()
        while self.chrom == chrom and self.end < beg:
            self.next()

def annotate(cdfn, repfn):
    rep = Repeats(repfn)
    f = open(cdfn, 'r')
    header = f.readline()
    print header[:-1] + '\tInRepeat\tDistance\tRepBeg\tRepEnd\tName\tDiv'

    while f:
        line = f.readline()
        if not line:
            break
        _, chrom, beg, end = line.split('\t')[:4]
        chrom = chrom_no(chrom)
        beg = int(beg)
        end = int(end)
        rep.forward(chrom, beg)
        if rep.chrom == chrom and rep.begin < beg and rep.end > end:
            dist = min(beg - rep.begin, rep.end - end)
            print '%s\tY\t%d\t%d\t%d\t%s\t%.1f' % (
                line[:-1], dist,
                rep.begin, rep.end,
                rep.name, rep.divergence)
        else:
            print '%s\tN\t\t\t\t\t' % line[:-1]

import sys
annotate(sys.argv[1], sys.argv[2])