#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os

import re, datetime
from glob import glob
from executeQueryInSQL import MsSQLapi

# import pprint
# import cPickle as pickle

###### Document Decription
''' This program is used to calculate the whole bases and bases after filtering process of specific ADF.

    [Introduction]:
    On CGI platform, the rawbases are always be same, only influenced by Slide type.
    During the make ADF process, two kinds of DNBs will be filtered:
    The first one is NoCallDNBs, means whole DNBs with 'N' bases.
    The second one is MirageDNBs, means DNBs' signal is influenced by the DNBs surrounded. (one kind of sequence duplication)

    [Formula]:
    TotalRawDnbs = UsableDNBs + NoCallDNBs + MirageDNBs
                 (  input DNBs for TeraMap  )

    The statistics is stored in nanocall.tsv.gz of ADF directory,

    [*]: After make ADF process, MirageDNBs are filtered, but NocallDNBs are still preserved in ADF files.
       When TeraMap is called, there is another filtering process:
       All DNBs with same sequence will reserve only one copy. A cutoff will be set to filter low quality DNBs,
       NocallDNBs and LowQualDNBs will be filtered in this process. The avaliable DNBs will be called Searched DNBs.

    To upload the data to SQL server, a config file is needed for connection. section as log name, 'DIDReport:ReportDB' required for project information request, 'AdfWriter' for data upload. If a lane existed in database, all columns will be updated with new values. Otherwise new row will be created to saved new result. All command is writed in a storage procedure in SQL named 'checkAndUpdateLaneYield'.
'''

###### Version and Date
prog_version = '1.3.0'
prog_date = '2014.03.20'

###### Usage
usage = '''

     Version %s  by Vincent-Li  %s

     Given a dir of workflow contain ADF dir,
     calculate the raw data and clean data.

     If config file is given, will upload
     the result to devsql.

     Usage: %s <Analysis_dir> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class dataStat(object):
    """A structure to storage yield and lanes information"""
    
    def __init__(self, yieldList):
        self.data = {}

        ########## Format:
        ###### [0]:lane [1]:basePerDnbs [2]:RawBase [3]:RawDnb [4]:CleanBase [5]:CleanDnb [6]:DupBase [7]:DupDnb [8]:NocallBase
        ###### new Added, [9]: libraryMethod [10]: withBarcode [11]:sid [12]:DID [13]:sequenceCompleteTime
        for x in yieldList:
            self.data[x[0]] = x
            libraryMethod = '2AD'
            withBarcode = 0
            if x[1] == '68':
                libraryMethod = '2AD-LFR'
                withBarcode = 1
            elif x[1] == '41':
                libraryMethod = '1AD'
                withBarcode = 1
            self.data[x[0]].extend([libraryMethod, withBarcode])
        self.laneList = self.data.keys()

    def getInfoFromSQL(self, sqlConnectObj):
        ''' Get SID, DID and sequence complete time in SQL'''
        tmpList = [ x.split('-L0') for x in self.laneList]
        for x in self.laneList:
            queryText = "select sid, DID, seq_complete_time FROM dbo.pub_SlideLane A1, dbo.pub_SlideLaneDNB A2 WHERE A1.pub_Slide_Lane_Id = A2.pub_Slide_Lane_Id AND slide_barcode = '%s' AND lane = '%s'" % tuple(x.split('-L0'))
            returnList = sqlConnectObj.executeQuery(queryText)
            if len(returnList) != 1:
                raise IndexError, 'Expect 1 line return from SQL of lane %s, get %s', (x, len(returnList))
            ###### remove '.\d' in sid, eg: GSxxxxx-DNA_A01.1
            returnList[0] = list(returnList[0])
            returnList[0][0] = returnList[0][0][:-2]

            self.data[x].extend(returnList[0])

    def upload(self, sqlConnectObj, outputSuccess=False):
        ''' Upload information to devsql, storage procedure 'checkAndUpdateLaneYield' was defined in SQL. '''
        for k,v in self.data.iteritems():
            thisTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            v.append(thisTime)
            v[13] = v[13].strftime('%Y-%m-%d %H:%M:%S')
            v = [ '\'' + str(v[i]) + '\'' for i in [0, 11, 12, 9, 10] + range(1,9) + [13, 14] ]
            queryText = "EXEC checkAndUpdateLaneYield %s" % ",".join(v)
            sqlConnectObj.executeQueryRaw(queryText)

        sqlConnectObj.commit()

        if outputSuccess == True:
            print 'Successfully upload data to SQL'


##########################################################################
############################  BEGIN Function  ############################
##########################################################################
def calculateBasePerDnbs(file):
    filelines = os.popen('wc -l %s | cut -f 1 -d " " ' % file).readline().strip()
    filelines = int(filelines) - 2
    return filelines

def openFile(filename):
    try:
        if filename[-3:] == '.gz':
            fileHandle = os.popen('gzip -dc %s' % filename, 'r', 100000000)
        elif filename[-4:] == '.bz2':
            fileHandle = os.popen('bzip2 -dc %s' % filename, 'r', 100000000)
        else:
            fileHandle = open(filename, 'r', 100000000)
    except IOError, error:
        raise error
    return fileHandle

def StatNanocall(analysisDir, statAverage=False, noheadPrint=False, perlaneYield=False):
    laneList = glob('%s/ADF/GS*' % analysisDir)

    ####### exit if not exit ADF file
    if len(laneList) == 0:
        print >>sys.stderr, "ERROR: not exit dir %s/ADF/GS*" % analysisDir
        sys.exit(1)

    laneNumber = len(laneList)

    basePath = os.path.basename(analysisDir)

    totalRawDnb = 0
    totalMirageDnb = 0
    totalNoCallBase = 0
    totalCleanBase = 0
    returnList = []

    if not noheadPrint == True:
        if perlaneYield == True:
            returnList.append(['#Lane', 'BasePerDNBs', 'RawBases', 'RawDNBs', 'CleanBases', 'CleanDNBs', 'DuplicateBases', 'DuplicateDNBs', 'NoCallBases'])
        else:
            returnList.append(['#ADF', 'LaneNumber', 'BasePerDnbs', 'RawBases', 'RawDNBs', 'CleanBases', 'CleanDNBs', 'DuplicateBases', 'DuplicateDNBs', 'NoCallBases'])

    for e in laneList:

        laneId = os.path.basename(e)

        ######## Get bases per Dnbs
        cycleMapCsv = e + '/cyclemap.csv'

        if not os.path.exists(cycleMapCsv):
            print >>sys.stderr, "ERROR: not exit csv file %s" % cycleMapCsv
            sys.exit(1)
        else:
            basePerDnbs = calculateBasePerDnbs(cycleMapCsv)

        ######## init
        thisRawDnb = 0
        thisMirageDnb = 0
        thisNoCallBase = 0

        ######## read nanocall to stat data volume
        nonoCallTsv = e + '/nanocall.tsv.gz'
        nanoFh = openFile(nonoCallTsv)
        nanoFh.readline()

        ####### Sum up the data for each field
        for line in nanoFh:
            info = line.split()

            thisRawDnb += int(info[2])
            thisMirageDnb += int(info[3])
            thisNoCallBase += int(info[4])

        thisRawBase = thisRawDnb * basePerDnbs
        thisCleanBase = thisRawBase - (thisMirageDnb * basePerDnbs) - thisNoCallBase

        ####### print out result if only calculate per lane data
        if perlaneYield == True:
            returnList.append([str(e) for e in (laneId, basePerDnbs, thisRawBase, thisRawDnb, thisCleanBase, int(thisCleanBase / basePerDnbs), thisMirageDnb * basePerDnbs, thisMirageDnb, thisNoCallBase)])

        ####### Add to totatl VAR
        totalRawDnb += thisRawDnb
        totalMirageDnb += thisMirageDnb
        totalNoCallBase += thisNoCallBase
        totalCleanBase += thisCleanBase

        nanoFh.close()

    ######## Total statistics
    totalRawBase = totalRawDnb * basePerDnbs
    totalCleanBase = totalRawBase - (totalMirageDnb * basePerDnbs) - totalNoCallBase
    totalCleanDnb = int(totalCleanBase / basePerDnbs)

    ######## calculate average for lanes
    if statAverage == True:
        (totalRawDnb, totalMirageDnb, totalNoCallBase, totalRawDnb, totalCleanBase, totalCleanDnb) = [ int(e / laneNumber) for e in (totalRawDnb, totalMirageDnb, totalNoCallBase, totalRawDnb, totalCleanBase, totalCleanDnb)]

    ######## End if just stat by lane
    if perlaneYield == False:
        returnList.append([str(e) for e in (basePath, laneNumber, basePerDnbs, totalRawBase, totalRawDnb, totalCleanBase, totalCleanDnb, totalMirageDnb * basePerDnbs, totalMirageDnb, totalNoCallBase)])

    ######## return results
    return returnList
    

######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():

    ########## Phrase parameters
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)
    ArgParser.add_argument("-a", "--average", action="store_true", dest="aver", default=False, help="Display data volume per lane instead of sum. [%(default)s]")
    ArgParser.add_argument("-n", "--nohead", action="store_true", dest="nohead", default=False, help="Do NOT display head of the information. [%(default)s]")
    ArgParser.add_argument("-l", "--perlane", action="store_true", dest="perlane", default=False, help="Stat in lanes instead of ASM. [%(default)s]")
    ArgParser.add_argument("-c", "--config", action="store", dest="configFile", metavar="CONFIG", help="The config file of database to upload the data. [%(default)s]")

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 1:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (ANALYSIS_DIR,) = args

    ############################# Body #############################
    if para.configFile:
        ##### upload instead of print
        dataObj = dataStat(StatNanocall(ANALYSIS_DIR, noheadPrint=True, perlaneYield=True))

        ###### Request project information of lanes
        sqlObj = MsSQLapi()
        sqlObj.readConfig(config=para.configFile, section='DIDReport:ReportDB')
        dataObj.getInfoFromSQL(sqlObj)
        sqlObj.close()

        ###### upload information to SQL
        sqlObj.readConfig(section='AdfWriter')
        dataObj.upload(sqlObj, outputSuccess=True)
        sqlObj.close()
    else:
        for x in StatNanocall(ANALYSIS_DIR, statAverage=para.aver, noheadPrint=para.nohead, perlaneYield=para.perlane):
            print "\t".join(x)

    


    

#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
