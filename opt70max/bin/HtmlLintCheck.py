#!/usr/bin/python


# Lint checker for HTML files


# Import some modules we need.
import HTMLParser
import optparse
import sys



# Derived class that does the work.
class HtmlLintChecker(HTMLParser.HTMLParser):

    # Constructor.
    def __init__(self, fileName, minTags=1, maxTags=0):
    
        # Initialize the base class.
        HTMLParser.HTMLParser.__init__(self)
        
        # Initialize data members.
        self.tagCount = 0           # Number of start tags found so far
        self.htmlTagFound = False   # Flag set true when the html start tag is encountered
        
        # Do the parsing.
        self.feed(open(fileName).read())
        self.close()
        
        # Check that all is ok.
        if not self.htmlTagFound:
            raise NameError('No html tag found')
        if self.tagCount<minTags:
            raise NameError('Not enough tags found')    
        if maxTags>0 and self.tagCount>maxTags:
            raise NameError('Too many tags found')    


        
    # Function called when a start tag is encountered.        
    def handle_starttag(self, tag, attrs):
        self.tagCount = self.tagCount + 1  # Increment number of tags found so far
        if tag == 'html':
            self.htmlTagFound = True       # We did find the htm start tag



# Parse the command line.
commandLineParser = optparse.OptionParser(description='Lint checker for HTML files')
commandLineParser.add_option("-m", "--minTags", type="int", default=5,
    help="Minimum acceptable number of start tags")
commandLineParser.add_option("-M", "--maxTags", type="int", default=1000000,
    help="Maximum acceptable number of start tags (or zero if no maximum)")
(options, arguments) = commandLineParser.parse_args()
if len(arguments) != 1:
    print 'Should be called with one positional argument'   
    commandLineParser.print_help()
    sys.exit(2)

        
# Parse the html file.        
parser = HtmlLintChecker(arguments[0], options.minTags, options.maxTags)
     

    