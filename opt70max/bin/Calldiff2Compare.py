#!/usr/bin/env python
import sys,re,os
import simplejson as js
import bz2
from optparse import OptionParser

################################# Gobal Variant #################################

################################# General Function #################################
class DelimReader(object):
    def __init__(self, ff, delimiter="\t"):
        if re.search('bz2$',ff):
            self.ff = bz2.BZ2File(ff)
        else:
            self.ff = open(ff)
        self.delimiter = delimiter
        self.current_id = '1'
        self.current_line = ''
        self.resultA = []
        self.resultB = []
        while True:
            line = self.ff.readline()
            if '#' == line[0]: continue #skip the annotation
            if '' == line[0]: continue  #skip the empty line
            # because we joined two master file together the header would be duplicated
            line = line.replace('>','')
            self.hFields = line.rstrip('\r\n').split(self.delimiter)
            break

    def close(self):
        self.ff.close()

    def next(self):
        resultA = []
        resultB = []

        while True:
            line = self.ff.readline()
            if '' == line:
                return None,None
            fields = line.rstrip('\r\n').split(self.delimiter)
            if len(fields) != len(self.hFields):
                raise Exception('field count mismatch')
            tempdict = {} 
            for i in range(0,len(self.hFields)):
                name = self.hFields[i]
                val = fields[i]
                tempdict[name]=val
            if self.current_id != tempdict['SuperlocusId']:
                resultA = self.resultA
                resultB = self.resultB
                
                self.resultA = []
                self.resultB = []
                if tempdict['File'] == 'A':
                    self.resultA.append(tempdict)
                elif tempdict['File'] == 'B':
                    self.resultB.append(tempdict)
                self.current_id = tempdict['SuperlocusId']

                return resultA,resultB
            else:
                if tempdict['File'] == 'A':
                    self.resultA.append(tempdict)
                elif tempdict['File'] == 'B':
                    self.resultB.append(tempdict)

def int_(string):
    if string:
        re = None
        try: 
            re=int(string)
        except :
            re = string
        return re
    else:
        return 0
def get_asm_name(joinedFile):
    asm_name=os.path.basename(joinedFile)
    return asm_name

def open_list(key,outputFolder):
    fh=open(os.path.join(outputFolder,key+'_list.tsv'),'w')
    fh.write("SuperlocusId\tA,LocusClassification|B,LocusClassification\tA,LocusDiffClassification|B,LocusDiffClassification\tA,chromosome,begin,end|B,chromosome,begin,end\tA,genotype|B,genotype\tA,Filter|B,Filger\tA,VAF|B,VAF\tA,EAF|B,EAF\tmutation_type\tcompare_type\trecord_type\n")
    return fh
def ini_counter():
    temp = {}
    for zt in ['het','hom','total']:
        temp[zt]={}
        for key in ['FP','TP','FN','candidate-positive']:
            temp[zt][key] = 0
    return temp
def ini_breakdown():
    temp = {}
    for zt in ['het','hom','total']:
        temp[zt]={}
        for key in ['FP','TP','FN','candidate-positive']:
            temp[zt][key] = {}
    return temp

################################# mutaion related #################################
class superLocus:
    def __init__(self,Adict,Bdict,hom_cutoff=20,het_cutoff=40):
        la = len(Adict)
        lb = len(Bdict)
        #print "#",la,lb
        if la<=2 and lb <=2:
            for fields in Adict:
                if fields['varType'] == 'no-call':
                    la -= 1
                else:
                    if 'hom' in fields['LocusClassification']:
                        if int_(fields['varScoreEAF'])<hom_cutoff: la -= 1
                    else:
                        if int_(fields['varScoreEAF'])<het_cutoff: la -= 1
            for fields in Bdict:
                if fields['varFilter'] == '' or fields['varFilter'] == 'VQLOW':
                    lb -= 1

        self.record_type = 'None'
        self.compare_type = 'None'
        self.mutation_type = 'no-call'
        self.loci_count = 1
        if la<2:
            if lb<2:
                self.record_type = 'hazy'   #I don't know what is happening
                self.compare_type = 'hazy'
            elif lb==2:
                self.record_type = 'capture_lost'   #FN
                self.compare_type = 'FN'
                self.mutation_type = Bdict[0]['LocusClassification']
            else:
                self.record_type = 'complicated_multi-record'   #deal with it later
        elif la==2:
            if lb<2:
                self.mutation_type = Adict[0]['LocusClassification']
                if lb == 0:
                    if len(Adict[0]['LocusDiffClassification'].split(';')) !=2 : 
                        self.record_type = 'complicated'  
                        self.compare_type = 'unknow_mismatch'
                    else:
                        self.record_type = 'wgs_lost' #Candidate positive and FP 
                        i,j = Adict[0]['LocusDiffClassification'].split(';')
                        if 'consistent' in i or 'consistent' in j:
                            self.compare_type = 'candidate-positive'
                        else:
                            self.compare_type = 'FP'
                elif lb == 1 :
                    self.record_type = 'wgs_half' #Candidate positive
                    self.compare_type = 'candidate-positive'
            elif lb==2:
                #Major
                if len(Adict[0]['LocusDiffClassification'].split(';')) !=2 : 
                    self.record_type = 'complicated'  
                    self.compare_type = 'unknow_mismatch'
                else:
                    self.record_type = 'normal'  
                    i,j = Adict[0]['LocusDiffClassification'].split(';')
                    if 'identical' in i and 'identical' in j:
                        self.compare_type = 'TP'
                    else:
                        self.compare_type = 'FP'
                    self.mutation_type = Bdict[0]['LocusClassification']
            else:
                self.record_type = 'complicated_multi-record'   #deal with it later
        else:
            self.record_type = 'complicated_multi-record'   #deal with it later
        if la!=0 and lb!=0:
            mut_a = Adict[0]['LocusClassification'].split('-')[-1]
            mut_b = Bdict[0]['LocusClassification'].split('-')[-1]
            if mut_a != mut_b:
                self.record_type = 'complicated_multi-mut_type' #ignore
                self.compare_type = 'mut_type_mismatch'
        if len(Adict)!=0 or len(Bdict)!=0:
            for obj in Adict + Bdict:
                if  obj['varType']!='ref' and (not obj['varType'] in obj['LocusClassification']):
                    self.record_type = 'complicated_multi-mut_type' #ignore
                    self.compare_type = 'mut_type_mismatch'
                    break

        self.Adict = Adict
        self.Bdict = Bdict
        #print la,lb
    def print_dict(self,from_complicated=False):
        #SuperlocusId A,LocusClassification|B,LocusClassification A,LocusDiffClassification|B,LocusDiffClassification A,chromosome,begin,end|B,chromosome,begin,end A,genotype|B,genotype A,Filter|B,Filger A,VAF|B,VAF A,EAF|B,EAF mutation_type compare_type record_type from_complicated
        SuperlocusId, LocusClassification, LocusDiffClassification, position, genotype, Filter, VAF, EAF = [''] * 8
        if len(self.Adict) !=0 :
            SuperlocusId=self.Adict[0]['SuperlocusId']
            LocusClassification=self.Adict[0]['LocusClassification']
            LocusDiffClassification=self.Adict[0]['LocusDiffClassification']
            position= ",".join([self.Adict[0][x]  for x in 'chromosome,begin,end'.split(',')])
            genotype= ','.join([self.Adict[i]['alleleSeq'] for i in range(len(self.Adict))])
            VAF= ','.join([self.Adict[i]['varScoreVAF'] for i in range(len(self.Adict))])
            EAF= ','.join([self.Adict[i]['varScoreEAF'] for i in range(len(self.Adict))])
            Filter= ','.join([self.Adict[i]['varFilter'] for i in range(len(self.Adict))])
        else:
            SuperlocusId, LocusClassification, LocusDiffClassification, position, genotype, Filter, VAF, EAF = ['no-record'] * 8
        if len(self.Bdict) != 0 :
            if SuperlocusId == 'no-record':
                SuperlocusId = self.Bdict[0]['SuperlocusId']
            elif self.Bdict[0]['SuperlocusId'] != SuperlocusId:
                print 'this loci is mis-matched! %s vs %s' %(SuperlocusId, self.Bdict[0]['SuperlocusId'] )
                sys.exit(1)
            LocusClassification+='|' + self.Bdict[0]['LocusClassification']
            LocusDiffClassification+='|' + self.Bdict[0]['LocusDiffClassification']
            position+='|' +  ",".join([self.Bdict[0][x]  for x in 'chromosome,begin,end'.split(',')])
            genotype+='|' +  ','.join([self.Bdict[i]['alleleSeq'] for i in range(len(self.Bdict))])
            VAF+='|' +  ','.join([self.Bdict[i]['varScoreVAF'] for i in range(len(self.Bdict))])
            EAF+='|' +  ','.join([self.Bdict[i]['varScoreEAF'] for i in range(len(self.Bdict))])
            Filter+='|' +  ','.join([self.Bdict[i]['varFilter'] for i in range(len(self.Bdict))])
        else: 
            LocusClassification += '|no-record' 
            LocusDiffClassification += '|no-record' 
            position += '|no-record' 
            genotype += '|no-record' 
            Filter += '|no-record' 
            VAF += '|no-record' 
            EAF += '|no-record' 
        
        genotype = genotype.replace('?','N')
        self.LocusDiffClassification = LocusDiffClassification
        return "\t".join([SuperlocusId, LocusClassification, LocusDiffClassification, position, genotype, Filter, VAF, EAF,
            self.mutation_type, self.compare_type, self.record_type, str(from_complicated)
            ])
    def parser_complicated(self):
        self.purify()
        temp = {}
        for obj in self.Adict:
            begin = obj['begin']
            if not begin in temp.keys(): temp[begin]={'A':[],'B':[]}
            temp[begin]['A'].append(obj)
        for obj in self.Bdict:
            begin = obj['begin']
            if not begin in temp.keys(): temp[begin]={'A':[],'B':[]}
            temp[begin]['B'].append(obj)
        return temp
    def purify(self):
        temp = {}
        for obj in self.Adict:
            locus = obj['locus']
            if not locus in temp.keys(): temp[locus]=0
            temp[locus]+=1
        trouble_locus = [str(x) for x in temp.keys() if temp[x]>=3]
        for i in range(len(self.Adict)):
            locus = self.Adict[i]['locus']
            if locus in trouble_locus:
                self.Adict[i]['LocusClassification'] = 'complicated'
        temp = {}
        for obj in self.Bdict:
            locus = obj['locus']
            if not locus in temp.keys(): temp[locus]=0
            temp[locus]+=1
        trouble_locus = [str(x) for x in temp.keys() if temp[x]>=3]
        for i in range(len(self.Bdict)):
            locus = self.Bdict[i]['locus']
            if locus in trouble_locus:
                self.Bdict[i]['LocusClassification'] = 'complicated'


def read_with_org(asm_name,joinedFile,outputFolder):
    header={}

    count = {}
    count_breakdown = {} 
    for vt in ['snp', 'ins', 'del', 'sub']:
        count[vt]=ini_counter()
        count_breakdown[vt]=ini_breakdown()

    ff = DelimReader(joinedFile)
    while True:
        varA,varB = ff.next()
        if varA is None:
            break
        spl = superLocus(varA,varB)

        if spl.record_type == 'complicated_multi-record':
            for obj in spl.parser_complicated().values():                
                varA = obj['A']
                varB = obj['B']
                spl_ = superLocus(varA,varB)
                if spl_.record_type in ['normal','capture_lost','wgs_lost']:
                    mut=spl_.mutation_type.split('-')[-1]
                    key = "_".join([mut,spl_.compare_type])
                    if not key in header.keys(): header[key]=open_list(key,outputFolder)
                    header[key].write(spl_.print_dict(True)+"\n")
                    if mut in count.keys():
                        zt = spl_.mutation_type.split('-')[0]
                        count[mut][zt][spl_.compare_type] += 1 
                        if not (spl_.LocusDiffClassification in count_breakdown[mut][zt][spl_.compare_type].keys()): count_breakdown[mut][zt][spl_.compare_type][spl_.LocusDiffClassification] = 0 
                        count_breakdown[mut][zt][spl_.compare_type][spl_.LocusDiffClassification]+=1

                        count[mut]['total'][spl_.compare_type] += 1 
                        if not (spl_.LocusDiffClassification in count_breakdown[mut]['total'][spl_.compare_type].keys()): count_breakdown[mut]['total'][spl_.compare_type][spl_.LocusDiffClassification] = 0 
                        count_breakdown[mut]['total'][spl_.compare_type][spl_.LocusDiffClassification]+=1

                else:
                    key=spl_.record_type
                    if not key in header.keys(): header[key]=open_list(key,outputFolder)
                    header[key].write(spl_.print_dict(True)+"\n")
        elif spl.record_type in ['normal','capture_lost','wgs_lost']:
            mut=spl.mutation_type.split('-')[-1]
            key = "_".join([mut,spl.compare_type])
            if not key in header.keys(): header[key]=open_list(key,outputFolder)
            header[key].write(spl.print_dict()+"\n")
            if mut in count.keys():
                zt = spl.mutation_type.split('-')[0]
                count[mut][zt][spl.compare_type] += 1 
                if not (spl.LocusDiffClassification in count_breakdown[mut][zt][spl.compare_type].keys()): count_breakdown[mut][zt][spl.compare_type][spl.LocusDiffClassification] = 0 
                count_breakdown[mut][zt][spl.compare_type][spl.LocusDiffClassification]+=1

                count[mut]['total'][spl.compare_type] += 1 
                if not (spl.LocusDiffClassification in count_breakdown[mut]['total'][spl.compare_type].keys()): count_breakdown[mut]['total'][spl.compare_type][spl.LocusDiffClassification] = 0 
                count_breakdown[mut]['total'][spl.compare_type][spl.LocusDiffClassification]+=1
        else:
            key=spl.record_type
            if not key in header.keys(): header[key]=open_list(key,outputFolder)
            header[key].write(spl.print_dict()+"\n")
    ff.close()

    for k in header.keys():
        header[k].close()

    stat = open(os.path.join(outputFolder,'Sensitivity_FDR.stat.csv'),'w')
    stat.write("varType,zygosity,#TP_mgt,#FP_mgt,#FN_mgt,candidate_positive_mgt,Sensitivity_mgt,FDR_mgt\n")
    for vt in ['snp', 'ins', 'del', 'sub']:
        for zy in ['het','hom','total']:
            TP = count[vt][zy]['TP']
            FP = count[vt][zy]['FP']
            FN = count[vt][zy]['FN']
            candidate_positive = count[vt][zy]['candidate-positive']
            FDR = 100*float(FP)/(FP+TP)
            Sens = 100*float(TP)/(FN+TP)
            stat.write(",".join([str(x) for x in [vt,zy,TP,FP,FN,candidate_positive,Sens,FDR]])+"\n")
    stat.close()    

    breakdown = open(os.path.join(outputFolder,'Sensitivity_FDR.stat_breakdown.csv'),'w')
    breakdown.write("varType,zygosity,type,inputA_LocusClassification\n")
    for vt in ['snp', 'ins', 'del', 'sub']:
        for zy in ['het','hom','total']:
            for ty in ['TP','FP','FN','candidate-positive']:
                for lc in count_breakdown[vt][zy][ty].keys():
                    breakdown.write(",".join([str(x) for x in [vt,zy,ty,lc,count_breakdown[vt][zy][ty][lc]]])+"\n")
    breakdown.close()

def read_with_differentEAF(asm_name,joinedFile,outputFolder,EAFCut):
    count = {}
    for vt in ['snp', 'ins', 'del', 'sub']:
        count[vt]=ini_counter()
    ff = DelimReader(joinedFile)
    while True:
        varA,varB = ff.next()
        if varA is None:
            break
        spl = superLocus(varA,varB,hom_cutoff=EAFCut,het_cutoff=EAFCut)

        if spl.record_type == 'complicated_multi-record':
            for obj in spl.parser_complicated().values():                
                varA = obj['A']
                varB = obj['B']
                spl_ = superLocus(varA,varB,hom_cutoff=EAFCut,het_cutoff=EAFCut)
                if spl_.record_type in ['normal','capture_lost','wgs_lost']:
                    mut=spl_.mutation_type.split('-')[-1]
                    key = "_".join([mut,spl_.compare_type])
                    if mut in count.keys():
                        zt = spl_.mutation_type.split('-')[0]
                        count[mut][zt][spl_.compare_type] += 1 
                        count[mut]['total'][spl_.compare_type] += 1 
                else:
                    key=spl_.record_type
        elif spl.record_type in ['normal','capture_lost','wgs_lost']:
            mut=spl.mutation_type.split('-')[-1]
            key = "_".join([mut,spl.compare_type])
            if mut in count.keys():
                zt = spl.mutation_type.split('-')[0]
                count[mut][zt][spl.compare_type] += 1 
                count[mut]['total'][spl.compare_type] += 1 
        else:
            key=spl.record_type
    ff.close()
    return count

def get_ROC_curve(asm_name,joinedFile,outputFolder):
    stat = open(os.path.join(outputFolder,'Sensitivity_FDR.ROC_curve.csv'),'w')
    stat.write("EAFcut,varType,zygosity,#TP_mgt,#FP_mgt,#FN_mgt,candidate_positive_mgt,Sensitivity_mgt,FDR_mgt\n")
    for EAFcut in range(0,100,5):
        count = read_with_differentEAF(asm_name,joinedFile,outputFolder,EAFcut)
        for vt in ['snp', 'ins', 'del', 'sub']:
            for zy in ['het','hom','total']:
                TP = count[vt][zy]['TP']
                FP = count[vt][zy]['FP']
                FN = count[vt][zy]['FN']
                candidate_positive = count[vt][zy]['candidate-positive'] 
                FDR = 100*float(FP)/(FP+TP)
                Sens = 100*float(TP)/(FN+TP)
                stat.write(",".join([str(x) for x in [EAFcut,vt,zy,TP,FP,FN,candidate_positive,Sens,FDR]])+"\n")
    stat.close()


################################# Main function related #################################
def parse_arguments(arguments):
    parser = OptionParser()
    parser.add_option('-i','--input',default='',help='input file',dest='i')
    parser.add_option('-o','--outputDir',default='./',help='output dir',dest='o')
    parser.add_option('-a','--asm-name',default='',help='asm name',dest='a')
    options,args = parser.parse_args(arguments[1:])
    return(
        options.i,
        options.o,
        options.a
        )

def main(argv):
    joinedFile,outputFolder,asm_name = parse_arguments(argv)

    if asm_name == '':
        asm_name = get_asm_name(joinedFile)

    read_with_org(asm_name,joinedFile,outputFolder)
    get_ROC_curve(asm_name,joinedFile,outputFolder)
main(sys.argv)