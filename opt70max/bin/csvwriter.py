#!/usr/bin/env python

# Globals.
ofn = None
ofs = None
hasTable = None

def openCsvFile(name):
    '''
    Opens a global CSV output file. Must be called prior any other calls.
    There suppose to be one CSV file per MAKO report template.
    '''
    global ofs, ofn, hasTable
    ofn = name
    ofs = open(name, 'w');
    hasTable = False

def closeCsvFile():
    '''
    Closes the global CSV output file. Must be called at the very end of
    MAKO template execution.
    '''
    global ofs, ofn, hasTable
    if ofs is None:
        return
    ofn = None
    ofs.close();
    ofs = None
    hasTable = None
    
def writeHeader(str):
    '''
    Writes a commented out CSV file header.
    This function can be used only before 1st table output.
    '''
    global ofs, hasTable
    assert(hasTable == False)
    ofs.write('# '+str+'\n')

def writeTable(data, name, th = "", extra = None):
    '''
    Writes out table data. All tables are assumed to be in a "freeform" format.
    Fisrt row of the table must contain a header line.
    '''
    global ofs, hasTable
    if ofs is None:
        return
    if not hasTable:
        ofs.write('\n')
        hasTable = True
    ofs.write('freeform,'+name+th)
    if extra != None:
        ofs.write(','+extra)
    ofs.write('\n')
    if len(data) > 0:
        ofs.write('>')
        size = len(data[0])
    for row in data:
        assert(len(row) == size)
        if len(row) > 0:
            ofs.write(str(row[0]).replace(',',''))
            for val in row[1:]:
                ofs.write(','+str(val).replace(',',''))
        ofs.write('\n')
    ofs.write('\n')

def writeNomalizedTable(data, name, th = "", extra = None):
    '''
    Writes out normalized table data. Tabke data is cleaned from HTML tags,
    fisrt row is converted to camelBack notation as well as table name.
    '''
    normTab = cleanHtmlTable(camelBackCol0(data))
    normName = camelBack(name)
    writeTable(normTab, normName, th, extra)

def camelBack(val):
    '''
    Convert any string to a "camel-back" notation.
    Example: camelBack('HELLO WORLD') == 'helloWorld'.
    '''
    if type(val) is str:
        val = val.strip()
        if val.find(' ') > 0:
            val = val.replace(':', ' ')
            val = val.replace('-', '_ ')
            val = val.lower().title()
            val = val.replace(' ', '')
        return val[0:1].lower()+val[1:]
    else:
        # don't convert non-strings!
        return val

def camelBackCol0(data):
    '''
    Converts table header into to a "camel-back" notation.
    Example: ['Avg Ploidy','Sequence Cvg'] -> ['avgPloidy','sequenceCvg']
    '''
    tab = [[]]
    row0 = data[0]
    for col in row0:
        tab[0].append(camelBack(col))
    tab += data[1:]
    return tab

def camelBackRow0(data):
    '''
    Converts table's first row into to a "camel-back" notation.
    See camelBackHeader() for example.
    '''
    tab = []
    for row in data:
        tab.append([camelBack(row[0])]+row[1:])
    return tab

def cleanHtmlTable(data):
    '''
    Removes all HTML tags from the table.
    Example: '<b>100</b>' -> '100'
    '''
    cleanTab = []
    for row in data:
        cleanRow = []
        for val in row:
            if type(val) is str:
                while (True):
                    tagB = val.find('<')
                    tagE = val.find('>')
                    if tagB >= 0 and tagB < tagE:
                        val = val[:tagB]+val[tagE+1:]
                        continue
                    break
                val = val.replace(',','')
            cleanRow.append(val)
        cleanTab.append(cleanRow)
    return cleanTab