#!/usr/bin/env python

import sys
import aqccutoffs

class CqcCutoffs(aqccutoffs.AqcCutoffs):
    pass


class CqcReport(aqccutoffs.AqcReport):

    def _renderQcRow(self, result, name, value, cutoffs, filters):
        fields = [ name, value ]
        tr = cutoffs.testCutoffs(name, value, False)
        for ar in tr:
            # XXXXX no filters used in CQC for now - if self._passesFilter(ar.cutoff.categories, filters):
            fields.append(ar)
            fields.append(ar._renderCuts()[1])
        self._renderRow(result, fields, True)

    def _renderRow(self, result, fields, useBold=False):
        aqccutoffs.AqcReport._renderRow(self, result, fields, True)

    def _renderQcTableBegin(self, result, name, cutoffs, filters):
        self._renderTableBegin(result, name, ['Name', 'Value', 'Outcome', 'Failure criterion'])

    def _renderTableBegin(self, result, name, header):
        result.append('<p>&nbsp;</p><h3>'+name+'</h3>')
        result.append('<table>')
        result.append('<tr>')
        for hd in header:
            result.append(' <th>'+hd+'</th>')
        result.append('</tr>')


def main():
    cqcCutoffs = CqcCutoffs('/dev/null')
    cqcReport = CqcReport()
    print 0

if __name__ == '__main__':
    main()

