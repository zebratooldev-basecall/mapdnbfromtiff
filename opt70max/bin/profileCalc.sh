#!/bin/bash

ASMID=$1
CHR=$2
WWIDTH=$3
PROFILEDIR=$4
MEDIANDIR=$5
SMOOTHDIR=$6

avgdepth=`awk 'NR==1{print $1}' ${MEDIANDIR}/median_depth`

# name of first file
first=`echo ${SMOOTHDIR}/${ASMID}/smoothed_${WWIDTH}/${CHR}_*.tsv.gz | awk '{print $1}'`

# column containing gcCorrectedCoverage, obtained by parsing header line of first file
gccol=`zcat $first | awk 'NR==1{gccol=-1;for(i=1;i<=NF;i++)if($i=="gcCorrectedCoverage"){gccol=i;break};print gccol}'`
if [ $gccol == "-1" ]; then
  echo "ERROR: Did not find gcCorrectedCoverage in header"
  exit -1
fi

zcat ${SMOOTHDIR}/${ASMID}/smoothed_${WWIDTH}/${CHR}_*.tsv.gz |\
   awk -v d=$avgdepth -v OFS=, -v gccol=$gccol '$2=="begin"{next}{print $2,$3, $gccol/d}' > ${PROFILEDIR}/cvgprofile
