#!/usr/bin/env python

import os
import sys
import glob
import pymssql
import logging
from optparse import OptionParser
from ConfigParser import ConfigParser

class DelimReader(object):
    def __init__(self, ff, delimiter=","):
        self.ff = ff
        self.delimiter = delimiter
        self.hFields = self.ff.readline().rstrip('\r\n').split(self.delimiter)

    def close(self):
        self.ff.close()

    def next(self):
        line = self.ff.readline()
        if '' == line:
            return None
        fields = line.rstrip('r\n').split(self.delimiter)
        if len(fields) != len(self.hFields):
            raise Exception('field count mismatch')
        result = {}
        for (name,val) in zip(self.hFields, fields):
            result[name] = val
        return result

def load_list(fn, delimiter=",", ff=None):
    if ff is None:
        ff = open(fn)
    result = []
    reader = DelimReader(ff, delimiter)
    while True:
        val = reader.next()
        if val is None:
            break
        result.append(val)
    reader.close()
    return result

def setup_logger():
    global logger

    # create logger
    logger = logging.getLogger('mini-assembly-uploader_logger')
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

def execute_sql_statements(asm_name, dbLogin, sql_accumulator, whichDB="concordance:dwProd"):
    logger.info('Executing accumulated SQL statements')
    logger.info('  - %d sql inserts to execute' % len(sql_accumulator))
    conn = pymssql.connect(host=dbLogin.get(whichDB,'host'),
                        user=dbLogin.get(whichDB,'user'),
                        password=dbLogin.get(whichDB,'password'),
                        database=dbLogin.get(whichDB,'database'))

    # check connection.
    if not conn:
        logger.error('Unable to connect to %s!' % dbLogin.get(whichDB,'host'))
        os.sys.exit(-1)

    # setup cursor.
    cur = conn.cursor()

    # loop through sql statements and execute 50 at a time. This is due to 
    # string length restrictions in pymssql.
    i, istart, iend = 0, 0, 0
    exit_cond = False
    while True:
        # set slice bounds
        istart = i * 50
        iend = istart + 50
        if iend > len(sql_accumulator):
            iend = len(sql_accumulator)
            exit_cond = True

        logger.info('  - inserting statements %d through %d...' % (istart,iend))

        # create cursor.
        cur = conn.cursor()

        # execute a slice of accumulated sql statements.
        cur.execute('\n'.join(sql_accumulator[istart:iend]))

        # close connection.
        cur.close()

        i += 1

        # exit condition
        if exit_cond: break

    # commit.
    conn.commit()

    logger.info('Loading of mini-assembly %s complete...' % asm_name)

def get_cls(input_dir):
    result = ''

    cls_dir_pattern = '%s/GAPS/GS*' % input_dir
    for dir in glob.glob(cls_dir_pattern):
        result = os.path.split(dir)[1]

    return result

def load_aqc_metrics(input_dir):
    logger.info('Loading metrics from aqc.tsv file...')
    aqc = {}
    aqc_file_path = os.path.join(input_dir, 'WebReports', 'aqc.tsv')
    logger.info('  - input file %s...' % aqc_file_path)

    # check for the file.
    if not os.path.exists(aqc_file_path):
        logger.error('File not found! %s' % aqc_file_path)

    # read in aqc metric values.
    sr = open(aqc_file_path, 'r')
    for line in sr:
        if line.startswith('name'): continue
        fields = line.rstrip('\r\n').split('\t')
        if fields[1] == "":
            aqc[fields[0]] = fields[2]
        elif fields[1] == "VQHIGH":
            aqc[fields[0]] = fields[2]
    sr.close()

    logger.info('  - %d metrics loaded into memory' % len(aqc))

    return aqc

def insert_metrics(asm_name, cls_id, aqc, stacking_metrics, sql_accumulator):
    logger.info('Creating inserts statements for a subset of aqc metrics')
    columnMap = {
        'input_dnbs': 'Total input full DNBs',
        'full_coverage_gb': 'Fully mapped yield (Gb)',
        'non_duplicate_rate': 'Non-duplicated / unique full sequence coverage',
        'fraction_searched_with_full_mappings': 'Fraction of searched DNBs with full DNB mappings',
        'min_pct_with_mates_paired_sampled': 'Min per-library pct with mates paired (sampled)',
        'overall_discordance': 'Overall discordance',
        'pct_genome_under_one_fifth_mean': '% genome with < 0.2 mean coverage',
        'pct_genome_by_gc_under_three_fifths_mean': '% genome by GC with < 0.6 mean coverage',
        'pct_exome_by_gc_under_three_fifths_mean': '% autosomal exome by GC with < 0.6 mean coverage',
        'fully_called_genome_fraction': 'Fully called genome fraction',
        'partially_called_genome_fraction': 'Partially called genome fraction',
        'no_called_genome_fraction': 'No-called genome fraction',
        'fully_called_exome_fraction': 'Fully called coding sequence fraction'
        }

    columns = []
    values = []

    for columnName, aqcName in columnMap.items():
        logger.debug('  - creating insert statement for (%s:%s)' % (aqcName, columnName))
        columns.append(columnName)
        values.append(aqc[aqcName])

    if stacking_metrics[0] == None:
        sqlString = "insert into asm_metrics (asm_name,cls,%s,left_arm_stacked_alt_support,right_arm_stacked_alt_support) values ('%s','%s',%s,NULL,NULL)" % (','.join(columns),asm_name,cls_id,','.join(values))
    else:
        sqlString = "insert into asm_metrics (asm_name,cls,%s,left_arm_stacked_alt_support,right_arm_stacked_alt_support) values ('%s','%s',%s,%f,%f)" % (','.join(columns),asm_name,cls_id,','.join(values),stacking_metrics[0],stacking_metrics[1])
    sql_accumulator.append(sqlString)
    logger.info('  - 1 sql inserts added to accumulator')

def load_stacking_histogram(asm_name, cls_id, input_dir, var_type, sql_accumulator):
    logger.info('Trying to load the dnb stacking histogram')
    n = 0

    stacking_cutoff = 79
    fp = os.path.join(input_dir, 'varSupport', asm_name, 'summary', '%s_dnb_stacking.tsv' % var_type)
    logger.info('  - input file %s...' % fp)

    # if file does not exists we assume varSupport was not ran.
    if not os.path.exists(fp):
        logger.warning('  - dnb stacking histogram file does not exists!')
        return (None, None)

    # load table.
    table = load_list(fp, '\t')

    # create insert statements.
    left_arm_var_count, right_arm_var_count = 0, 0
    left_stacked_count, right_stacked_count = 0, 0
    for data in table:
        sqlString = "insert into asm_dnb_stacking (cls,var_type,support_type,side,stacking_percent,variant_count) values ('%s','%s','%s','%s',%s,%s)" % (cls_id,var_type,data['support_type'],data['side'],data['stacking_percent'],data['variant_count'])
        sql_accumulator.append(sqlString)
        n += 1
        if data['support_type'] == 'alt':
            if data['side'] == 'L':
                left_arm_var_count += int(data['variant_count'])
                if int(data['stacking_percent']) > stacking_cutoff:
                    left_stacked_count += int(data['variant_count'])
            else:
                right_arm_var_count += int(data['variant_count'])
                if int(data['stacking_percent']) > stacking_cutoff:
                    right_stacked_count += int(data['variant_count'])

    left_stacking_fraction = float(left_stacked_count) / float(left_arm_var_count)
    right_stacking_fraction = float(right_stacked_count) / float(right_arm_var_count)

    logger.info('  - %d sql inserts added to accumulator' % n)
    logger.info('  - left_stacking_fraction=%f, right_stacking_fraction=%f' % (left_stacking_fraction, right_stacking_fraction))

    return (left_stacking_fraction, right_stacking_fraction)

def load_coverage_by_gc(cls_id, input_dir, type, sql_accumulator):
    logger.info('Loading coverage by gc curve for %s...' % type)
    n = 0

    # check type to set filename
    if type == "g":
        fp = os.path.join(input_dir, "ASM", "coverage", "allCoverageByContentGC500.csv")
    elif type == "e":
        fp = os.path.join(input_dir, "ASM", "coverage", "autoExomeCoverageByContentGC500.csv")
    else:
        logger.error("Unknown coverage type %s! mini-assembly-uploader.py::load_coverage_by_gc()" % type)
        sys.exit(-1)
    logger.info('  - input file %s' % fp)

    # load table.
    table = load_list(fp, ',')

    # compute average coverage.
    base_count, coverage_count = 0, 0
    for data in table:
        base_count += int(data['Count'])
        coverage_count += int(data['calledcvg:weightratio>.99'])

    avg_coverage = float(coverage_count) / float(base_count)
    logger.debug('  - average_coverage=%f...' % avg_coverage)

    # loop through histogram and calculate cumulative base percentile and normalized coverage.
    cum_count = 0
    for data in table:
        cum_count += int(data['Count'])
        cumulative_percentile = float(cum_count) / float(base_count)
        norm_coverage = float(data['calledcvg:weightratio>.99']) / float(data['Count']) / avg_coverage

        sqlString = "insert into asm_coverage_by_gc (cls,[type],cumulative_basecount_percentile_by_gc,normalized_coverage) values ('%s','%s',%f,%f)" % (cls_id,type,cumulative_percentile,norm_coverage)
        sql_accumulator.append(sqlString)
        n += 1

    logger.info('  -  %d sql inserts added to accumulator' % n)

def parse_arguments(args):
    parser = OptionParser()

    parser.add_option('-a', '--asm-directory', help='Mini-assembly working directory', default=None, dest='input_dir')
    parser.add_option('-c', '--db-config', help='Path to db user config file', default=None, dest='config_file')

    options, args = parser.parse_args(args)

    # make sure input directory is set.
    if options.input_dir == None:
        logger.error('Input directory is required!')
        sys.exit(1)

    # make sure the db config file paramter is set.
    if options.config_file == None:
        logger.error('DB user config file parameter is required!')
        sys.exit(-1)

    logger.info('Running mini-assembly uploader tool for the following assembly %s...' % options.input_dir)

    return options.input_dir, options.config_file

def main(arguments):
    # set up logger
    setup_logger()

    # parse command line arguments.
    input_dir, config_file = parse_arguments(arguments[1:])

    # get assembly name (working directory)
    asm_name = os.path.split(input_dir)[1]

    # get CLS
    cls_id = get_cls(input_dir)

    # sql statements accumulator.
    sql_accumulator = []

    # load dnb stacking table using snps.
    stacking_metrics = load_stacking_histogram(asm_name, cls_id, input_dir, 'snp', sql_accumulator)

    # load coverage by gc tables.
    load_coverage_by_gc(cls_id, input_dir, 'g', sql_accumulator)
    load_coverage_by_gc(cls_id, input_dir, 'e', sql_accumulator)

    # load aqc metrics
    aqc = load_aqc_metrics(input_dir)

    # insert selected aqc metrics
    insert_metrics(asm_name, cls_id, aqc, stacking_metrics, sql_accumulator)

    # setup the db config.
    dbUserConfig = ConfigParser()
    dbUserConfig.read(config_file)

    # execute accumulated sql statements.
    execute_sql_statements(asm_name, dbUserConfig, sql_accumulator)

main(sys.argv)
