#!/usr/bin/env python
import os,sys
import optparse

class Counter(object):
    def __init__(self, name, expression):
        self.counter = 0
        self.name = name
        self.expression = expression
        self.evalobject = compile(expression, 'counter:' + name, 'eval')

    def run(self, data):
        return eval(self.evalobject, data)

def parseLine(options, line, r):
    tokens = line.split(',')
    r['arm'] = tokens[1]
    r['L'] = (r['arm'] == 'L')
    r['R'] = (r['arm'] == 'R')
    r['strand'] = tokens[2]

    prev = -1
    for ii in range(1, options.readCount+1):
        rn = 'r' + str(ii)
        pos = int(tokens[3+ii])
        r[rn] = pos
        if prev >= 0:
            dn = 'd' + str(ii-1)
            r[dn] = pos - prev
        prev = pos

    for ii in range(4+options.readCount, len(tokens)):
        if tokens[ii] == '-':
            r['s' + str(ii - options.readCount - 3)] = False
        else:
            r['s' + str(ii - options.readCount - 3)] = tokens[ii]

def applyCounters(data, counters):
    for f in counters:
        if f.run(data):
            f.counter += 1

def printCounters(counters, outf):
    print >>outf, 'dict,enzyme'
    for f in counters:
        print >>outf, '%s,%d' % (f.name, f.counter)

def process(options, fn, counters, outfn):
    data = {}
    f = open(fn, 'r')
    for line in f:
        parseLine(options, line.strip(), data)
        applyCounters(data, counters)
    f.close()

    outf = open(outfn, 'w')
    printCounters(counters, outf)
    outf.close()

DEFAULT_COUNTERS = \
[
    Counter('left', 'L'),
    Counter('right', 'R'),
]

if __name__ == '__main__':
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(readCount=3)
    parser.add_option('-i', '--input',
                      help="name of the source file produced by LocalScan")
    parser.add_option('-o', '--output',
                      help="name of the output file")
    parser.add_option('--counter', action='append',
                      help="counter name followed by semicolon and counter expression")
    parser.add_option('--read-count', type="int", dest="readCount",
                      help="number of reads per arm in the LocalScan output file")
    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    try:
        if options.counter:
            counters = []
            for c in options.counter:
                name, expr = c.split(';')
                counters.append(Counter(name, expr))
        else:
            counters = DEFAULT_COUNTERS
        process(options, options.input, counters, options.output)
    except:
        import traceback
        traceback.print_exc()
        sys.exit(1)
    sys.exit(0)


