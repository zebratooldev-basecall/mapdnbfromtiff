#!/usr/bin/python
import os,sys
import optparse
import numpy
from numpy import *
from math import *

#####################################
### magic constant for smoothing ratios
#####################################

pseudocounts = 5.0



def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(gapsDir='',refDir='',libID='',mappingDataDir='',
                        leftGapCount=3,rightGapCount=3)
    parser.add_option('-g', '--gaps-dir', dest='gapsDir',
                      help='directory containing (partial) gaps object to work on')
    parser.add_option('-r', '--ref-dir', dest='refDir',
                      help='directory containing reference/simulation data')
    parser.add_option('-l', '--lib-id', dest='libID',
                      help='ID of library')
    parser.add_option('-d', '--mapping-data-dir', dest='mappingDataDir',
                      help='directory that contains mapper output and intermediate covmodel files')
    parser.add_option('--left-gap-count',type=int,dest='leftGapCount',
                      help='number of gaps in left arm')
    parser.add_option('--right-gap-count',type=int,dest='rightGapCount',
                      help='number of gaps in right arm')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.gapsDir:
        parser.error('gaps-dir specification required')
    if not options.refDir:
        parser.error('ref-dir specification required')
    if not options.mappingDataDir:
        parser.error('mapping-data-dir specification required')
    if not options.leftGapCount:
        parser.error('left-gap-count specification required')
    if not options.rightGapCount:
        parser.error('right-gap-count specification required')

    ##############################
    #### get sequence patch values for sim data
    ##############################

    leftendratio = {}
    rightendratio = {}
    leftAcuIratio = {}
    rightAcuIratio = {}
    leftEcoP15ratio = {}
    rightEcoP15ratio = {}

    leftendf = open(os.path.join(options.gapsDir,options.libID + '.leftend'),'r')
    for l in leftendf:
        l.strip()
        w=l.split()
        v=w[0].split(',')
        leftendratio[v[1]]=w[1]
    leftendf.close()

    rightendf = open(os.path.join(options.gapsDir,options.libID + '.rightend'),'r')
    for l in rightendf:
        l.strip()
        w=l.split()
        v=w[0].split(',')
        rightendratio[v[1]]=w[1]
    rightendf.close()

    leftAcuIf = open(os.path.join(options.gapsDir,options.libID + '.leftAcuI'),'r')
    for l in leftAcuIf:
        l.strip()
        w=l.split()
        v=w[0].split(',')
        leftAcuIratio[v[1]]=w[1]
    leftAcuIf.close()

    rightAcuIf = open(os.path.join(options.gapsDir,options.libID + '.rightAcuI'),'r')
    for l in rightAcuIf:
        l.strip()
        w=l.split()
        v=w[0].split(',')
        rightAcuIratio[v[1]]=w[1]
    rightAcuIf.close()

    leftEcoP15f = open(os.path.join(options.gapsDir,options.libID + '.leftEcoP15'),'r')
    for l in leftEcoP15f:
        l.strip()
        w=l.split()
        v=w[0].split(',')
        leftEcoP15ratio[v[1]]=w[1]
    leftEcoP15f.close()

    rightEcoP15f = open(os.path.join(options.gapsDir,options.libID + '.rightEcoP15'),'r')
    for l in rightEcoP15f:
        l.strip()
        w=l.split()
        v=w[0].split(',')
        rightEcoP15ratio[v[1]]=w[1]
    rightEcoP15f.close()


    ##############################
    #### massage mapping stats for real data
    ##############################

    massagedstats = open(os.path.join(options.mappingDataDir,'covmodelinputs.txt'),'w')

    testdata = [
        "395,216,44,37,10,9,6,10,0,9,6,16,4,0,GACGTCCT,CTAGAGTA,GAGTCTCA,TGAGACAT,ACTTGCAG,GGGAAGGA,TCCTAATCAAGAGTCTCACCTGGGTACATGAACTTAGGAGGCAGCCTGGAGGGGGCATGTGAGACACTAG,,,GTCTGG,CCTCCA,,,",
        "358,165,44,38,13,7,11,4,0,9,10,10,6,0,AGGAGAAT,TACATTTC,GAACCCGG,GCAGGACA,TCATGCCA,AGCATTGA,TGACAAAGCATGAATAGCGCGGGGCAAGGACTACAGAATCCCTTGGAACCCGGGAACTCCGGTGAGATCA,GCCGA,,,,,ATTGAC,",
        "420,176,40,38,4,6,13,12,0,8,6,13,8,0,TTCTTTTG,GCTGTAGC,GCAGGCAC,GCACTTAC,GCTTCCGC,TAGTTGTT,GTTAGGTTTGTGTGTACTTCCAGGCCAGGTGGCTGTTTGCATGGGGCAGGCACCATAGAGGTAAGGTGCT,AGTGC,,,,,TTGTTA,",
        "428,246,48,42,8,9,11,7,0,8,10,12,5,0,AACCAGGA,CCCTAAGA,ACCGTGGG,GGTCCATG,CTACAGAC,AGGAGAGG,GAGGAACCAGCATGTTGGGGTCTAGGCCAATCCCTAGGACAGTGAACCGTGGGGGCCTCACCTGAGCTAC,GAGACT,,,,,GAGAGG,",
        "358,157,37,32,6,10,11,8,0,7,5,6,17,0,TATCAGTA,TCCCCCAT,TTGGTCTT,AAGCCCAG,CGTGAGAT,CTGCCTGA,CTGATTCCCTAGGTGGCAATGGCTAGCAGGGTCCCAGTATTTATTTTGGTCTTAGATCTTCACTTACGTG,TCTATA,,,,,GCCTGA,",
        "424,252,51,45,11,8,13,3,0,5,9,15,6,0,CTACCCAC,AGGGGAGC,GGATGGGA,CACCCGGA,GCAGGGCT,TGGACTGG,CCACAATTGGGGATGGGAAGACAGCCCAGAGGCAGCTGGAGGGAGGGGGCTGTTTCCAGCCACCCTGAGG,,,GTAGGG,CAGCCC,G,,",
        "352,213,47,41,9,9,12,5,0,6,13,7,9,0,ATTTGTCA,CTCCCCAT,GAAGGGCT,CCTCAGTG,GACCGTGG,TCTGGTGT,GTCACCTGAGGAAGGGCTAACTGAGCCGAGTGACCTGTGAAGCAGGAACCCCTTTTGCCCTCAGTCTCCC,,,CTGGTA,GGGTT,,,"
        ]

    baseheaders="p1base"
    for i in range(2,71):
        baseheaders=baseheaders+" p"+str(i)+"base"
    gapsheaders=""
    lg = options.leftGapCount
    if lg > 0:
        gapsheaders += "lgap1"
        for i in range(2,lg+1):
            gapsheaders += " lgap"+str(i)
    rg = options.rightGapCount
    if rg > 0:
        if lg > 0:
            gapsheaders += " "
        gapsheaders += " rgap1"
        for i in range(2,rg+1):
            gapsheaders += " rgap"+str(i)

    print >> massagedstats, "isReal dnbGCpcnt armsGC armsAG leftA leftC leftG leftT leftN rightA rightC rightG rightT rightN leftend rightend leftAcuI rightAcuI leftEcoP15 rightEcoP15 outerleftend innerleftend outerrightend innerrightend "+baseheaders+" "+gapsheaders

    realdata = open(os.path.join(options.mappingDataDir,'mappingStatsData.csv'),'r')

    n=0
    for l in realdata:
        l=l.strip()
        w=l.split(',')

        outerleftend = w[14][0:4]
        innerleftend = w[14][4:]

        if not w[14] in leftendratio:
            w[14] = 0.0;
        else:
            w[14] = leftendratio[w[14]]

        outerrightend = w[15][0:4]
        innerrightend = w[15][4:]

        if not w[15] in rightendratio:
            w[15] = 0.0;
        else:
            w[15] = rightendratio[w[15]]

        if not w[16] in leftAcuIratio:
            w[16] = 0.0;
        else:
            w[16] = leftAcuIratio[w[16]]

        if not w[17] in rightAcuIratio:
            w[17] = 0.0;
        else:
            w[17] = rightAcuIratio[w[17]]

        if not w[18] in leftEcoP15ratio:
            w[18] = 0.0;
        else:
            w[18] = leftEcoP15ratio[w[18]]

        if not w[19] in rightEcoP15ratio:
            w[19] = 0.0;
        else:
            w[19] = rightEcoP15ratio[w[19]]

        bases=list(w[20])


        print >> massagedstats, 1,float(w[1])/float(w[0]),w[2],w[3],w[4],w[5],w[6],w[7],w[8],w[9],w[10],w[11],w[12],w[13],w[14],w[15],w[16],w[17],w[18],w[19],outerleftend,innerleftend,outerrightend,innerrightend,
        for b in bases:
            print >> massagedstats,b,
        for i in range(0,lg):
            print >> massagedstats, "x"+w[21+i],
        for i in range(0,rg):
            print >> massagedstats, "x"+w[21+lg+i],
        print >> massagedstats, ""

        n=n+1
        if n > 200000:
            break

    realdata.close()


    ##############################
    #### massage mapping stats for sim data
    ##############################

    simdata = open(os.path.join(options.refDir,'CovModel/mappingStatsData.csv'),'r')

    n=0
    for l in simdata:
        l=l.strip()
        w=l.split(',')

        outerleftend = w[14][0:4]
        innerleftend = w[14][4:]

        if not w[14] in leftendratio:
            w[14] = 0.0;
        else:
            w[14] = leftendratio[w[14]]

        outerrightend = w[15][0:4]
        innerrightend = w[15][4:]

        if not w[15] in rightendratio:
            w[15] = 0.0;
        else:
            w[15] = rightendratio[w[15]]

        if not w[16] in leftAcuIratio:
            w[16] = 0.0;
        else:
            w[16] = leftAcuIratio[w[16]]

        if not w[17] in rightAcuIratio:
            w[17] = 0.0;
        else:
            w[17] = rightAcuIratio[w[17]]

        if not w[18] in leftEcoP15ratio:
            w[18] = 0.0;
        else:
            w[18] = leftEcoP15ratio[w[18]]

        if not w[19] in rightEcoP15ratio:
            w[19] = 0.0;
        else:
            w[19] = rightEcoP15ratio[w[19]]

        bases=list(w[20])

        print >> massagedstats, 0,float(w[1])/float(w[0]),w[2],w[3],w[4],w[5],w[6],w[7],w[8],w[9],w[10],w[11],w[12],w[13],w[14],w[15],w[16],w[17],w[18],w[19],outerleftend,innerleftend,outerrightend,innerrightend,
        for b in bases:
            print >> massagedstats,b,
        for i in range(0,lg):
            print >> massagedstats, "x"+w[21+i],
        for i in range(0,rg):
            print >> massagedstats, "x"+w[21+lg+i],
        print >> massagedstats, ""

        n=n+1
        if n > 200000:
            break

    simdata.close()


    massagedstats.close()



if __name__ == '__main__':
    main()
