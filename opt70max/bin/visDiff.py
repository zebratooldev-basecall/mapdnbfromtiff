#! /usr/bin/python

import subprocess, sys, os
import wfclib.jsonutil
from bisect import bisect
from optparse import OptionParser
from callfile import LocusStream, Locus, Call
from os.path import split as psplit, join as pjoin
from glob import glob
from subprocess import Popen, PIPE


def loadState(workDir, fnglob):
    fns = glob(os.path.join(workDir, 'state', fnglob))
    stateFiles = Popen(["ls", "-t"] + fns, stdout=PIPE).communicate()[0]
    stateFile = os.path.join(workDir, 'state', stateFiles.split()[0])
    return wfclib.jsonutil.load(stateFile)


class AssemblyPaths:
    def __init__(self, callFileName):
        self.callFileName = callFileName
        (dd, cf) = psplit(callFileName)
        self.assemblyDir = psplit(psplit(dd)[0])[0]
        self.workDir = psplit(self.assemblyDir)[0]
        self.state = loadState(self.workDir, 'st.*')
        self.callannotate = pjoin(self.assemblyDir, "callrpt", "annotation", "callannotate_%s.csv" % cf[:-4])
        self.annotation = pjoin(self.assemblyDir, "variations", "annotated", "%s" % cf)
        self.trioAnnotation = pjoin(self.assemblyDir, "trio", "trio_output_%s.csv" % cf[:-4])


def readCodingRegions(filename):
    """Return the coding regions as super-contig ranges."""
    ff = open(filename)
    try:
        regions = []
        for line in ff:
            line = line.split('#')[0].strip()
            if '' == line:
                continue
            if 'contig,begin,end' == line:
                continue
            fields = line.split(",")
            if len(fields) != 3:
                raise Exception("could not parse coding region: %s" % line)
            regions.append( (fields[0], int(fields[1]), int(fields[2])) )
        return regions
    finally:
        ff.close()


def readLocusRanges(callFileName):
    """Return the super-contig ranges of the loci."""
    stream = LocusStream(callFileName)
    try:
        regions = []
        while True:
            locus = stream.nextLocus()
            if locus is None:
                break
            regions.append( (locus.chromosome,locus.begin,locus.end) )
        return regions
    finally:
        stream.close()


def readLociById(asmPaths, ids):
    stream = LocusStream(asmPaths.annotation)
    try:
        loci = [ ]
        while True:
            locus = stream.nextLocus()
            if locus is None:
                break
            if locus.id in ids:
                loci.append(locus)
        return loci
    finally:
        stream.close()


def sdCallIsCompatible(ch1, ch2):
    if ch1 == 'N' or ch2 == 'N' or ch1 == ch2:
        return True
    return False


def diffSnpDiffCalls(output_dir, ap1, ap2, checkRef):
    ff1 = open(pjoin(output_dir, 'called-by-asm1', 'snp-calls.txt'))
    ff2 = open(pjoin(output_dir, 'called-by-asm2', 'snp-calls.txt'))
    result = ([], [])
    id = 1
    for line1 in ff1:
        line2 = ff2.readline()
        f1 = line1.split(',')
        f2 = line2.split(',')
        if f1[0] != f2[0]:
            raise Exception('failed reading snpdiff files')
        c1 = f1[7]
        c2 = f2[7]
        sdc = sdCallIsCompatible
        if not ( (sdc(c1[0], c2[0]) and sdc(c1[1], c2[1])) or
                 (sdc(c1[0], c2[1]) and sdc(c1[1], c2[0])) ):
            ff = [f1,f2]
            compat1 = sdc(c1[0], f1[6]) and sdc(c1[1], f1[6])
            compat2 = sdc(c2[0], f2[6]) and sdc(c2[1], f2[6])
            if checkRef and not (compat1 or compat2):
                continue
            resultIndex = 0
            if compat1:
                resultIndex = 1
            fm = ff[resultIndex]
            calls = {}
            calls[fm[6][0]] = 1
            calls[fm[7][0]] = 1
            calls[fm[7][1]] = 1
            calls = sorted(calls.keys())
            locus = Locus(id)
            for ii in xrange(len(calls)):
                cc = calls[ii]
                fields = [ str(id), str(len(calls)), str(ii+1),
                           f1[2], str(int(f1[3])-1), f1[3], 'sub', f1[6], cc, '20', '', '' ]
                locus.addCall(Call(fields))
            result[resultIndex].append(locus)
            id += 1
    return result


def diffCalls(m1, m2):
    """Return (l1,l2): the set of loci in m1 that are not in m2 and
    the set of loci in m2 that are not in m1. Here, m1 is a map from
    key to locus, for each key of interest."""
    (ids1, ids2) = ( {}, {} )
    for key in m1:
        if key not in m2:
            ids1[m1[key].id] = key
    for key in m2:
        if key not in m1:
            ids2[m2[key].id] = key
    l1 = [ (id,m1[ids1[id]]) for id in ids1 ]
    l2 = [ (id,m2[ids2[id]]) for id in ids2 ]
    l1.sort()
    l2.sort()
    return ( [locus for (key,locus) in l1], [locus for (key,locus) in l2] )


def readFrameShiftLoci(asmPaths):
    if asmPaths is None:
        return {}
    ff = open(asmPaths.callannotate)
    try:
        ids = {}
        idMap = {}
        for line in ff:
            if line.find("FRAMESHIFT") != -1:
                fields = line.strip().split(",")
                vartype = fields[6]
                if vartype not in [ "=", "ref", "ref-consistent", "no-call-rc", "no-call" ]:
                    key = "%s,%s" % (",".join(fields[3:7]), fields[8])
                    id = int(fields[1])
                    idMap[key] = id
                    ids[id] = 1
        loci = readLociById(asmPaths, ids)
        locusMap = {}
        for locus in loci:
            for allele in locus.alleles:
                for call in allele.calls:
                    key = "%s,%d,%d,%s,%s" % (call.chromosome,call.begin,call.end,call.vartype,call.alleleSeq)
                    if key in idMap:
                        locusMap[key] = locus
        if len(locusMap.keys()) != len(idMap.keys()):
            raise Exception("Internal error")
        return locusMap
    finally:
        ff.close()


def readLoci(asmPaths, vartype, novel):
    if asmPaths is None:
        return {}
    stream = LocusStream(asmPaths.annotation)
    try:
        locusMap = {}
        while True:
            locus = stream.nextLocus()
            if locus is None:
                break
            for allele in locus.alleles:
                for call in allele.calls:
                    if call.vartype == vartype:
                        if call.xRef == "":
                            if not novel:
                                continue
                        else:
                            if novel:
                                continue
                            # For now, limit known-snp analysis to chr20.
                            if vartype == "snp" and call.chromosome != "chr20":
                                continue
                        key = "%s,%d,%d,%s,%s" % (call.chromosome,call.begin,call.end,call.vartype,call.alleleSeq)
                        locusMap[key] = locus
        return locusMap
    finally:
        stream.close()


def dumpFakeSnpGenotypes(asmPaths, fn):
    locations = {}
    for ap in asmPaths:
        stream = LocusStream(ap.annotation)
        while True:
            locus = stream.nextLocus()
            if locus is None:
                break
            for allele in locus.alleles:
                for call in allele.calls:
                    if call.vartype in [ "snp" ]:
                        key = (call.chromosome, call.begin+1)
                        locations[key] = call.sequence
        stream.close()
    keys = sorted(locations.keys())
    ff = open(fn, 'w')
    id = 1
    for (chromosome,begin) in keys:
        print >>ff, 'rs%d A/C/G/T %s %s + AA' % (id, chromosome, begin)
        id += 1
    ff.close()


def readTrioIncompatibleLoci(asmPaths):
    if asmPaths is None:
        return {}
    ff = open(asmPaths.trioAnnotation)
    try:
        ids = {}
        idMap = []
        for line in ff:
            fields = line.strip().split(",")
            if fields[4].find("incompatible") == -1:
                continue
            key = (fields[1],int(fields[2]),int(fields[3]))
            locusFields = fields[5].split(";")
            for lf in locusFields:
                hasNonRef = False
                for vartype in [ "snp", "del", "ins", "delins", "sub", "ref-inconsistent", "no-call-ri" ]:
                    if lf.find(vartype) != -1:
                        hasNonRef = True
                        break
                if hasNonRef:
                    id = int(lf.split(":")[0])
                    idMap.append( (key,id) )
                    ids[id] = 1
        loci = readLociById(asmPaths, ids)
        idMap.sort()
        locusMap = {}
        for locus in loci:
            key = ( locus.chromosome,locus.begin,locus.end )
            idx = bisect(idMap, (key,None))
            minIdx = max(idx-1,0)
            maxIdx = min(idx+1,len(idMap))
            for ( (chr,begin,end), id ) in idMap[minIdx:maxIdx]:
                if chr == locus.chromosome and begin<=locus.begin and locus.end<=end:
                    locusMap["%s,%d,%d"%key] = locus
        if len(locusMap.keys()) != len(idMap):
            for (key,id) in idMap:
                if "%s,%d,%d"%key not in locusMap:
                    print "key not in locusMap:",key,id
            raise Exception("Internal error")
        return locusMap
    finally:
        ff.close()


def regionOverlap(regions, rg, dist):
    """Returns True iff rg is less than or equal to dist bases
    from a region in regions. Regions must be sorted."""
    idx = bisect(regions, rg)
    minIdx = max(idx-1,0)
    maxIdx = min(idx+1,len(regions))
    for (chr,begin,end) in regions[minIdx:maxIdx]:
        if rg[0] != chr:
            continue
        if rg[1] > end+dist:
            continue
        if rg[2] < begin-dist:
            continue
        return True
    return False


def compactRanges(regions):
    """Merges adjacent or overlapping regions. Regions is expected to
    be sorted."""
    result = []
    for rg in regions:
        if len(result) == 0 or rg[0] != result[-1][0] or rg[1] > result[-1][2]:
            result.append(rg)
        else:
            result[-1] = (rg[0],min(rg[1],result[-1][1]),max(rg[2],result[-1][2]))
    return result


def writeLoci(outfile, loci):
    out = open(outfile, "w")
    try:
        for locus in loci:
            out.write(str(locus)+"\n")
    finally:
        out.close()


def system(cmd):
    print '-'*70
    print 'executing:'
    print cmd
    print '-'*70
    sts = os.system(cmd)
    if sts != 0:
        raise Exception('command execution failed with status: %d' % sts)


def main():
    parser = OptionParser('usage: %prog [options] <call-file-1> <call-file-2>')
    parser.disable_interspersed_args()
    parser.add_option('-o', '--output-dir', default='.',
                      help='Path to output directory for visualizations.')
    parser.add_option('-s', '--sample-size', type=int, default=100,
                      help='Max number of vis loci to use.')
    parser.add_option('--locus-selector', default='frame-shift',
                      help='Type of locus to diff (frame-shift, novel-indel).')

    (options, args) = parser.parse_args()

    if len(args) != 2 and len(args) != 1:
        raise Exception("one or two call files required")
    ap1 = AssemblyPaths(args[0])
    if len(args) > 1:
        ap2 = AssemblyPaths(args[1])
    else:
        ap2 = None

    print "gathering and diffing %s loci" % options.locus_selector
    if options.locus_selector == 'frame-shift':
        (l1, l2) = diffCalls(readFrameShiftLoci(ap1), readFrameShiftLoci(ap2))
    elif options.locus_selector == 'novel-snp':
        (l1, l2) = diffCalls(readLoci(ap1, "snp", True), readLoci(ap2, "snp", True))
    elif options.locus_selector == 'known-snp':
        (l1, l2) = diffCalls(readLoci(ap1, "snp", False), readLoci(ap2, "snp", False))
    elif options.locus_selector == 'novel-ins':
        (l1, l2) = diffCalls(readLoci(ap1, "ins", True), readLoci(ap2, "ins", True))
    elif options.locus_selector == 'known-ins':
        (l1, l2) = diffCalls(readLoci(ap1, "ins", False), readLoci(ap2, "ins", False))
    elif options.locus_selector == 'novel-del':
        (l1, l2) = diffCalls(readLoci(ap1, "del", True), readLoci(ap2, "del", True))
    elif options.locus_selector == 'known-del':
        (l1, l2) = diffCalls(readLoci(ap1, "del", False), readLoci(ap2, "del", False))
    elif options.locus_selector == 'novel-delins':
        (l1, l2) = diffCalls(readLoci(ap1, "delins", True), readLoci(ap2, "delins", True))
    elif options.locus_selector == 'known-delins':
        (l1, l2) = diffCalls(readLoci(ap1, "delins", False), readLoci(ap2, "delins", False))
    elif options.locus_selector == 'trio-incompatible-plus':
        (l1, l2) = diffCalls(readTrioIncompatibleLoci(ap1), readTrioIncompatibleLoci(ap2))
    elif options.locus_selector in [ 'snpdiff-incompatible', 'snpdiff-incompatible-ref' ]:
        dumpFakeSnpGenotypes([ap1, ap2], pjoin(options.output_dir, 'snp_fake_genotypes.txt'))
        for (ap,id) in [(ap1,'asm1'), (ap2,'asm2')]:
            reference = ap.state.reference
            od = pjoin(options.output_dir, "called-by-"+id)
            if not os.path.exists(od):
                os.mkdir(od)

            retcode = subprocess.call(['snpdiff',
                                       '--reference=%s' % reference,
                                       '--genotype-file=snp_fake_genotypes.txt',
                                       '--call-file=%s' % ap.annotation,
                                       '--output=%s' % pjoin(od, 'snp-calls.txt'),
                                       ])
            if retcode != 0:
                raise Exception('snpdiff failed')

        (l1, l2) = diffSnpDiffCalls(options.output_dir, ap1, ap2, options.locus_selector.endswith('-ref'))
    else:
        raise Exception("unknown locus-selector: "+options.locus_selector)

    lr = [ (locus.chromosome,locus.begin,locus.end) for locus in l1 + l2 ]
    lr.sort()
    lr = compactRanges(lr)

    origs = []
    APLLID = [ (ap1,l1,"asm1"), (ap2,l2,"asm2") ]
    for (ap,ll,id) in APLLID:
        if ap is None:
            continue

        print "writing %s loci..." % id
        od = pjoin(options.output_dir, "called-by-"+id)
        if not os.path.exists(od):
            os.mkdir(od)
        for (subap,subll,subid) in APLLID:
            if not os.path.exists(pjoin(od, subid)):
                os.mkdir(pjoin(od, subid))

        lf = os.path.abspath(pjoin(options.output_dir, "called-by-"+id, "Loci.csv"))
        writeLoci(lf,ll)
        origs.append(ap.callFileName)

    for (ap,ll,id) in APLLID:
        if ap is None:
            continue

        print "running driver.py for %s..." % id
        lf = os.path.abspath(pjoin(options.output_dir, "called-by-"+id, "Loci.csv"))
        origCallFile = ap.callFileName
        origCallFile = ":".join(origs)
        cgiHome = os.getenv("CGI_HOME")
        sampleArg = "-p in.sample-size=%d" % options.sample_size
        for (subap,subll,subid) in APLLID:
            od = pjoin(options.output_dir, "called-by-"+id, subid)
            cmd = ("driver.py -m grid -f %s/etc/workflows/human/VisualizeLoci -w %s "+
                   "-p in.work-dir=%s "+
                   "%s "+
                   "-p in.locus-file=%s "+
                   "-p in.call-file=%s") % ( cgiHome, os.path.abspath(od), subap.workDir, sampleArg,
                                             lf, origCallFile )
            ff = open(pjoin(od, "vis.sh"),"w")
            ff.write("#! /bin/bash\n\n%s\n" % cmd)
            ff.close()

            system(cmd)

if __name__ == '__main__':
    main()
