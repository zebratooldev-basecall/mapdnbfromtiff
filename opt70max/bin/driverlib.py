import os, sys, shutil, subprocess
import re

if os.name == 'nt':
    USING_WINDOWS = True
elif os.name == 'posix':
    USING_WINDOWS = False
else:
    raise Error('platform not supported: ' + os.name)

class Error(Exception): pass

CGI_RELEASE_AREA = '/Proj/QA/Release/Assembly'
#CGI_RELEASE_AREA = '/Proj/Assembly/inazarenko/FakeInst'

def _getBlessedBuild():
    f = open(os.path.join(CGI_RELEASE_AREA, 'current'), 'r')
    try:
        verstr = f.read().rstrip()
    finally:
        f.close()
    m = re.match(r'([\d]+)\.([\d]+)\.([\d]+)\.([\d]+)', verstr)
    if not m or m.lastindex != 4:
        raise Error('bad contents of current: ' + verstr)
    parts = m.groups()
    assert len(parts) == 4
    version = '.'.join(parts[0:3])
    buildId = '.'.join(parts)
    build = os.path.join(CGI_RELEASE_AREA, version, buildId, 'opt70max')
    if not os.path.isdir(build):
        raise Error('no build directory where current points to: ' + build)
    return os.path.abspath(build)

def getCgiHome(blessedBuild=False):
    if blessedBuild:
        cgiHome = _getBlessedBuild()
        os.environ['CGI_HOME'] = cgiHome
    else:
        if not 'CGI_HOME' in os.environ:
            raise Error('CGI_HOME must be set in the environment')
        cgiHome = os.environ['CGI_HOME']

    if not USING_WINDOWS:
        PIPELINE_DLL_NAME = 'gbi'
        # Check for presence of an arbitrary binary file under 'bin'
        binDir = os.path.join(cgiHome, 'bin')
        if not os.path.isfile(os.path.join(binDir, PIPELINE_DLL_NAME)):
            raise Error(PIPELINE_DLL_NAME + " not found in 'bin' subdirectory under " + cgiHome)

        startLoc = os.path.abspath(sys.path[0])
        if binDir != startLoc:
            errorText = "inconsistent configuration: script started from '%s'; " \
                        "expected binaries location is '%s'" % (startLoc, binDir)
            if blessedBuild:
                raise Error(errorText)
            else:
                # TODO: should this be a hard error as well?
                print 'WARNING:', errorText

    return cgiHome

class Include(object):
    scriptPath = None
    globalVariables = None
    includeSet = set()

    def __init__(self, name):
        assert Include.scriptPath
        assert Include.globalVariables

        # disallow any path specification in the includes for now
        if os.path.dirname(name):
            raise Error('include file name must not contain path; got: ' + name)

        fn = os.path.join(Include.scriptPath, name)
        if not os.path.isfile(fn):
            raise Error("include file '%s' not found in '%s'" % (name, Include.scriptPath) )
        execfile(fn, Include.globalVariables)
        Include.includeSet.add(fn)

    @staticmethod
    def copyAll(dest, prefix):
        for fn in Include.includeSet:
            destName = os.path.join(dest, prefix + os.path.basename(fn))
            try:
                shutil.copy(fn, destName)
            except IOError, e:
                if e.errno == 13 and e.filename == destName:
                    # permission denied on target file, perhaps we can remove it
                    # and try again in the spirit of "cp -f"
                    os.remove(destName)
                    shutil.copy(fn, destName)
                else:
                    raise


LOCAL_CHECK_STATUS_COMMAND = \
"""RC=$?
if [ $RC != 0 ]; then
    exit $RC
fi
"""

SGE_CHECK_STATUS_COMMAND = \
"""RC=$?
if [ $RC != 0 ]; then
    END_TIME_STAMP=`date --utc +%s`
    echo "execution failed after $((END_TIME_STAMP-START_TIME_STAMP)) seconds with status: $RC"
    exit 100
fi
"""

class Dir(object):
    """Incapsulates directory management for workflows"""

    dirList = []

    def __init__(self, location, base=None, mustExist=False):
        """Constructor"""
        if base:
            location = os.path.join(str(base), location)

        self.location = os.path.abspath(location)

        if mustExist and not os.path.isdir(self.location):
            raise Error('not an existing directory: ' + self.location)

        Dir.dirList.append(self)

    def file(self, fn, mustExist=False):
        """Prepends directory path to file name"""
        fullName = os.path.join(self.location, fn)
        if mustExist and not os.path.exists(fullName):
             raise Error('not an existing file: ' + fullName)
        return fullName

    def __call__(self):
        """Returns full path associated with dir"""
        return self.location

    def __str__(self):
        """Returns full path associated with dir"""
        return self.location

    def createDir(loc):
        """Creates the specified directory if it doesn't already exist"""
        # Try creating the path if it's not yet a directory.
        # If it fails, check if the path is now a directory (that is,
        # someone else created it for us between the check and the
        # actual mkdir command). If it is indeed a directory, silently
        # continue, otherwise re-raise the exception
        if not os.path.isdir(loc):
            try:
                os.makedirs(loc)
            except OSError:
                if not os.path.isdir(loc):
                    raise

    createDir = staticmethod(createDir)

    def create():
        """Creates all registered directories that don't already exist"""
        for dir in Dir.dirList:
            Dir.createDir(dir.location)

    create=staticmethod(create)

    def dump(f):
        """Prints all directories to the given stream"""
        print >>f, 'Log of directories used by the workflow:'
        for d in Dir.dirList:
            print >>f, d.location
    dump=staticmethod(dump)

class File(object):
    """Allows to create a text file with given contents at specified location"""

    fileList = []

    def __init__(self, name, contents, mustNotExist=False):
        """Constructor"""
        if mustNotExist and os.path.lexists(name):
            raise Error('file already exists: ' + name)
        self.name = name
        self.contents = contents
        File.fileList.append(self)

    def __create(self):
        f = open(self.name, 'w')
        print >>f, self.contents
        f.close()

    def __call__(self):
        """Returns full path to this file"""
        return self.name

    def __str__(self):
        """Returns full path to this file"""
        return self.name

    def create():
        """Creates all registered directories that don't already exist"""
        for file in File.fileList:
            file.__create()
    create=staticmethod(create)

    def dump(f):
        """Prints all file contents to the given stream"""
        if not File.fileList:
            return

        print >>f, 'Log of file contents to be created:'
        for file in File.fileList:
            print >>f, "File:", file.name
            print >>f, '-'*70
            print >>f, file.contents
            print >>f, '-'*70
    dump=staticmethod(dump)

class Params(object):
    """Allows to create parameter sets and refer to them by name"""
    overrides = {}
    paramSets = {}

    def __init__(self, name, params):
        if name in Params.paramSets:
            raise Error('parameter set %s already registered' % name)

        if name in Params.overrides:
            ovr = Params.overrides[name]
            newParams = []
            seenParams = {}

            for (p,v) in params:
                if p in ovr:
                    if p in seenParams:
                        raise Error('parameter %s occures multiple times in set %s; '
                                    'cannot override from command line' % (p, name))
                    else:
                        seenParams[p] = True
                    newParams.append( (p, ovr[p]) )
                else:
                    newParams.append( (p, v) )
            Params.paramSets[name] = newParams
        else:
            Params.paramSets[name] = params

    def get(set, name):
        p = Params.getSet(set)
        val = None
        for (k, v) in p:
            if k == name:
                if val:
                    raise Error("Params.get() failed: name '%s' occures more than once in set '%s'" % (name, set))
                else:
                    val = v
        if val is None:
            raise Error("Params.get() failed: name '%s' not found in set '%s'" % (name, set))
        return val
    get=staticmethod(get)

    def getSet(name):
        if not name in Params.paramSets:
            raise Error("parameter set '%s' not registered" % name)
        return Params.paramSets[name]
    getSet=staticmethod(getSet)

    def dump(f):
        """Prints all configured parameters in format compatible with setOverrides"""
        print >> f, '# dump of all configuration parameters\n'
        for (sn, vlist) in Params.paramSets.iteritems():
            print >> f, '\n# parameter set: %s\n' % sn
            for (k, v) in vlist:
                if v:
                    print >> f, '%s.%s=%s' % (sn, k, v)
                else:
                    print >> f, '%s.%s # no value' % (sn, k)
        print >> f, '# end of parameter dump'
    dump=staticmethod(dump)

    def setOverrides(overrides):
        for po in overrides:
            m = re.match(r'([-\w]+)\.([-\w]+)=(.+)', po)
            if not m or m.lastindex != 3:
                raise Error('invalid parameter spec: ' + po)
            (set, name, value) = m.groups()
            if not set in Params.overrides:
                Params.overrides[set] = {}
            Params.overrides[set][name] = value
    setOverrides=staticmethod(setOverrides)

def createCommand(exe, paramSets):
    if type(paramSets) == str:
        paramSets = [ paramSets ]

    tokens = [ exe ]
    for ps in paramSets:
        for (k, v) in Params.getSet(ps):
            if v:
                if v.find('"') != -1:
                    raise Error("parameter value for '%s' "
                                "contains double quote: %s" % (k, v))
                tokens.append('--%s="%s"' % (k, v))
            else:
                tokens.append('--%s' % k)
    if USING_WINDOWS:
        return ' '.join(tokens)
    else:
        return ' \\\n'.join(tokens)

SET_ENV_SCRIPT = \
"""
export CGI_HOME=%s
export PATH=$CGI_HOME/bin:/usr/local/bin:/bin:/usr/bin:/sge-root/bin/lx24-amd64
export LD_LIBRARY_PATH=$CGI_HOME/bin
"""

SET_ENV_AND_TRAP_SCRIPT = \
"""
trap "echo 'soft run time limit exceeded'; exit 100" 10
""" + SET_ENV_SCRIPT

START_SUBGRID_SCRIPT = \
"""
logf="%(logdir)s/pe_hostfile.log"
echo "hostfile: $PE_HOSTFILE" >> $logf
echo -n "date: " >> $logf
date >> $logf
echo -n "main host: " >> $logf
hostname >> $logf
echo '--------------------------------------------------' >> $logf
cat $PE_HOSTFILE >> $logf
echo '--------------------------------------------------' >> $logf
hostlist=""
hostno=1
for host in `awk '{print $1}' $PE_HOSTFILE`; do
    echo "starting node daemon $hostno on: $host"
    qrsh -V -inherit $host \\
        subgridnode \\
            --debug=1 --subgrid-id="%(subgrid)s" \\
            --log-file-name="%(logdir)s/node.$hostno.log" &
    if [ x$hostlist == x ]; then
        hostlist=$host
    else
        hostlist+=",$host"
    fi
    hostno=$((hostno+1))
done
wait
echo "sleeping a bit"
sleep 10
echo "starting subgrid master"
subgridmaster --debug=1 --dump-env --nodes="$hostlist" \\
    --subgrid-id="%(subgrid)s" --storage-dir="%(logdir)s"
rc=$?
if [ $rc != 0 ]; then
    END_TIME_STAMP=`date --utc +%%s`
    echo "master execution failed after $((END_TIME_STAMP-START_TIME_STAMP)) seconds with status: $rc"
    echo "sleeping 70 seconds so that nodes time out and die"
    sleep 70
    exit 100
fi
echo "subgrid master exited normally"
echo "sleeping 30 seconds so that nodes get a chance to exit"
sleep 30
echo "subgrid script exiting"
"""

class Task(object):
    """Captures data for a single command-line step"""

    def __init__(self, step, commands,
                 hog=False, memory=None,
                 array=False, arrayParamValues=[],
                 workflowTasksPerGridTask=1,
                 subgridSize=0, isSubGridTask=False):
        """Constructor"""
        self.commands = commands
        self.step = step
        self.hog = hog
        self._setMemory(memory)
        self.array = array
        self.arrayParamValues = arrayParamValues
        self.workflowTasksPerGridTask = workflowTasksPerGridTask
        self.subgridSize = subgridSize
        self.isSubGridTask = isSubGridTask

    def _setMemory(self, memory):
        if memory is not None:
            if not isinstance(memory, int):
                raise Error('memory parameter must be an integer, found: '
                            + str(memory))
            memory = int(memory)
            if not (0 < memory <= 62):
                raise Error('memory parameter must be in (0, 62] range, found: '
                            + str(memory))
        self.memory = memory

    def _exec(commands, cgiHome, printOnly):
        """Run given command locally"""
        if USING_WINDOWS:
            for c in commands:
                print "----------- command:"
                print c
                print "----------- output:"

                if not printOnly:
                    sts = os.system(c)
                    if sts != 0:
                        raise Error("Execution failed with status '%s'" % sts)
                else:
                    print "dry run"
        else:
            script = [ SET_ENV_AND_TRAP_SCRIPT % cgiHome ]

            for c in commands:
                script.append(c)
                script.append(LOCAL_CHECK_STATUS_COMMAND)
            script = '\n'.join(script)
            print "----------- script:"
            print script
            print "----------- output:"

            if not printOnly:
                sys.stdout.flush()
                sys.stderr.flush()
                child = subprocess.Popen("/bin/bash",
                                         stdin=subprocess.PIPE)

                # send script to the child
                print >> child.stdin, script
                child.stdin.close()

                # wait for child to really go away
                sts = child.wait()
                if sts != 0:
                    raise Error("Execution failed with status '%s'" % sts)
            else:
                print "dry run"
    _exec=staticmethod(_exec)

    def run(self, cgiHome, printOnly=True):
        """Run the commands of this step locally"""
        if self.array:
            for pv in self.arrayParamValues:
                formattedCommands = []
                for command in self.commands:
                    c = command
                    for key in pv:
                        c = c.replace('$%s' % key, pv[key]).replace('${%s}' % key, pv[key])
                    formattedCommands.append(c)
                Task._exec(formattedCommands, cgiHome, printOnly)
        else:
            Task._exec(self.commands, cgiHome, printOnly)

    def _shellEscape(self, ss):
        return ss.replace('"', '\\"')

    def _addShellArrayProlog(self, lines):
        lines.append('#$ -t 1-%d' % self.sgeTaskCount())
        for key in self.arrayParamValues[0].keys():
            lines.append( ('%s_vals=( "" ' +
                           ' '.join(['"' + self._shellEscape(p[key]) + '"' for p in self.arrayParamValues]) + \
                           ' )') % key )
            lines.append( '%s=${%s_vals[$SGE_TASK_ID]}' % (key,key) )

    def _addShellArrayMultiProlog(self, lines):
        lines.append( 'WORKFLOW_TASK_ID_START=$(( ($SGE_TASK_ID-1)*%d/%d+1 ))' %
                      (len(self.arrayParamValues),self.sgeTaskCount()) )
        lines.append( 'WORKFLOW_TASK_ID_END=$(( ($SGE_TASK_ID)*%d/%d ))' %
                      (len(self.arrayParamValues),self.sgeTaskCount()) )
        lines.append( 'for WORKFLOW_TASK_ID in $(seq $WORKFLOW_TASK_ID_START $WORKFLOW_TASK_ID_END); do' )
        for key in self.arrayParamValues[0].keys():
            lines.append( '%s=${%s_vals[$WORKFLOW_TASK_ID]}' % (key,key) )

    def _addShellArrayMultiPostlog(self, lines):
        lines.append( 'done' )

    def sgeTaskCount(self):
        if not self.array:
            return 1
        count = len(self.arrayParamValues) / self.workflowTasksPerGridTask
        if len(self.arrayParamValues) % self.workflowTasksPerGridTask != 0:
            count += 1
        return count

    def setLogDir(self, baseLogDir, workflowId):
        self.myLogDir = baseLogDir
        if self.array or self.subgridSize != 0:
            self.myLogDir = os.path.join(self.myLogDir,
                                         '%s.%s' % (workflowId, self.step.name))
            Dir(self.myLogDir)
        if self.myLogDir.find(",") != -1:
            raise Error("Log directory contains invalid characters: %s" % myLogDir)

    def createSgeScript(self, cgiHome, workflowId, options):
        """Creates script to run the commands of this task as an SGE job"""
        lines = []
        lines.append('#!/bin/bash')
        # name the job
        lines.append('#$ -N %s.%s' % (workflowId, self.step.name))
        # shell to use
        lines.append( "#$ -S /bin/bash" )

        # stdout/stderr all goes to the same file in the log directory
        lines.append('#$ -o ' + self.myLogDir)
        lines.append('#$ -j y')

        # job is restartable in case the execution host dies while running it
        lines.append('#$ -r y')

        SLOTS_PER_MACHINE = 8
        slots = 1

        if self.hog:
            # reserve all machine slots for this job
            lines.append('#$ -pe smp %d' % SLOTS_PER_MACHINE)
            # and request resource reservation to (hopefully) avoid
            # starvation when there's a contending stream of 1-slot jobs
            lines.append('#$ -R y')
            slots = SLOTS_PER_MACHINE
        elif self.subgridSize != 0:
            lines.append('#$ -pe subgrid %d' % (self.subgridSize * SLOTS_PER_MACHINE))
            lines.append('#$ -R y')
            slots = self.subgridSize * SLOTS_PER_MACHINE

        lines.append('#$ -l job_version=3')
        lines.append('#$ -l cgi_slots=%d' % slots)
        self.slots = slots

        assert (self.memory is not None)
        memoryUsageMb = self.memory * 1024
        if self.hog:
            # SGE assumes that the memory requirement is *per slot*, so for
            # parallel jobs we need to adjust
            memoryUsageMb /= SLOTS_PER_MACHINE
        lines.append('#$ -l virtual_free=%dM' % memoryUsageMb)

        if self.array:
            self._addShellArrayProlog(lines)

        if not 'enableCore' in options:
            lines.append('ulimit -c 0')

        # memory restrictions didn't work for convoluted reasons; removed.

        lines.append('echo "task slots: %s"' % slots)
        lines.append('echo cpu info:')
        lines.append('egrep -e "model name|cpu MHz|bogomips|cache size" /proc/cpuinfo | head -n 4')

        # set the environment to completely ignore user-specified path
        # to avoid picking up unexpected executables
        lines.append(SET_ENV_AND_TRAP_SCRIPT % cgiHome)

        if self.array and self.workflowTasksPerGridTask > 1:
            self._addShellArrayMultiProlog(lines)

        lines.append('START_TIME_STAMP=`date --utc +%s`')

        if self.subgridSize != 0:
            # this is a subgrid creation task
            script = START_SUBGRID_SCRIPT % { 'subgrid':'$JOB_ID',
                                              'logdir':self.myLogDir }
            lines.append(script)
        else:
            # normal task that runs some commands
            for c in self.commands:
                lines.append(c)
                lines.append(SGE_CHECK_STATUS_COMMAND)

        lines.append('END_TIME_STAMP=`date --utc +%s`')
        lines.append('echo "step elapsed time in seconds: $((END_TIME_STAMP-START_TIME_STAMP))"')

        if self.array and self.workflowTasksPerGridTask > 1:
            self._addShellArrayMultiPostlog(lines)

        return '\n'.join(lines)

class SubStep(object):
    def __init__(self, parentName, name, depends=[],
                 commands=[], arrayParamValues=[], concurrencyLimit=0, priority=0):
        self.name = name
        self.step = Step.get(parentName)
        self.step.subStepList.append(self)
        if name in self.step.subSteps:
            raise Error("Step named '%s' already exists" % name)
        self.step.subSteps[name] = self

        if type(depends) == str:
            depends = [ depends ]
        if type(commands) == str:
            commands = [ commands ]

        prereqs = []
        for dep in depends:
            try:
                depStep = self.step.subSteps[dep]
            except KeyError, e:
                raise Error("Sub-step '%s' not defined" % e)
            prereqs.append(depStep)
        self.deps = prereqs

        arrayParamValuesDict = Step._createArrayParamValuesDict(arrayParamValues)
        Step._validateParamValues(parentName + '.' + name, arrayParamValuesDict)
        self.task = Task(self, commands, False, 1,
                         bool(arrayParamValuesDict),
                         arrayParamValuesDict, 1, isSubGridTask=True)
        self.concurrencyLimit = concurrencyLimit
        self.priority = priority


class Step(object):
    """Describes single worflow step"""

    steps = {}
    stepList = []

    def __init__(self, name, depends=[],
                 commands=[], hog=False, memory=None,
                 arrayParamValues=[], workflowTasksPerGridTask=1,
                 subgridSize=0, subgridServiceCommand=None,
                 subgridServiceData=[]):
        """Constructor"""
        self.subStepList = []
        self.subSteps = {}
        self.subgridSize = subgridSize
        self.subgridServiceCommand = subgridServiceCommand
        self.subgridServiceData = subgridServiceData

        # Convenience wrappers for parameters that are frequently
        # single-valued
        if type(depends) == str:
            depends = [ depends ]
        if type(commands) == str:
            commands = [ commands ]

        if name in Step.steps:
            raise Error("Step named '%s' already exists" % name)
        self.name = name

        prereqs = []
        for dep in depends:
            depStep = Step.get(dep)
            prereqs.append(depStep)

        self.deps = prereqs
        self.index = len(Step.stepList)

        if workflowTasksPerGridTask < 1:
            raise Error("workflowTasksPerGridTask must be >= 1")

        if subgridSize == 0:
            if memory is None:
                raise Error('memory is not set for step: ' + name)
            arrayParamValuesDict = Step._createArrayParamValuesDict(arrayParamValues)
            Step._validateParamValues(name, arrayParamValuesDict)
            self.tasks = [ Task(self, commands, hog, memory,
                                bool(arrayParamValuesDict),
                                arrayParamValuesDict, workflowTasksPerGridTask) ]
        else:
            if memory is not None:
                raise Error('memory is set for a subgrid step: ' + name)
            self.tasks = [ Task(self, [], memory=2, subgridSize=subgridSize) ]

        Step.steps[name] = self
        Step.stepList.append(self)

    def isSubgrid(self):
        if self.subgridSize != 0:
            return True
        else:
            return False

    def setLogDir(self, baseLogDir, workflowId):
        for t in self.tasks:
            t.setLogDir(baseLogDir, workflowId)
        for s in self.subStepList:
            s.task.setLogDir(os.path.join(baseLogDir,"%s.%s" % (workflowId,self.name)), workflowId)

    def createSubgridFiles(self, cgiHome, workflowId, options):
        if not self.isSubgrid():
            return

        if not self.subStepList:
            raise Error('no steps for subgrid: ' + self.name)
        storageDir = self.tasks[0].myLogDir
        conf = open(os.path.join(storageDir, 'subgrid.conf'), 'w')
        for s in self.subStepList:
            task = s.task
            line = '%s.%s,%d,%d,%d,%s' % \
                        (workflowId, s.name, task.sgeTaskCount(),
                         s.priority, s.concurrencyLimit, task.myLogDir)
            for dep in s.deps:
                line += ",%s.%s" % (workflowId,dep.name)
            print >>conf, line
            script = open(os.path.join(storageDir, '%s.%s.script' % (workflowId,s.name)), 'w')
            print >>script, task.createSgeScript(cgiHome, workflowId, options)
            script.close()
        conf.close()
        if self.subgridServiceCommand and self.subgridServiceData:
            conf = open(os.path.join(storageDir, 'services.conf'), 'w')
            for dataUnit in self.subgridServiceData:
                print >>conf, '%s,%d' % dataUnit
            conf.close();
            script = open(os.path.join(storageDir, 'services.template.script'), 'w')
            print >>script, '#!/bin/bash'
            print >>script, (SET_ENV_SCRIPT % cgiHome)
            print >>script, "ulimit -c 0"
            print >>script, self.subgridServiceCommand.strip()

        statfn = os.path.join(storageDir, 'subgrid.status')
        if os.path.exists(statfn):
            os.remove(statfn)

    @staticmethod
    def _createArrayParamValuesDict(arrayParamValues):
        if len(arrayParamValues) == 0:
            return arrayParamValues
        if type(arrayParamValues[0]) == str:
            return [ {"PARAM":val} for val in arrayParamValues ]
        return arrayParamValues

    @staticmethod
    def _validateParamValues(stepName, arrayParamValues):
        for val in arrayParamValues:
            if not isinstance(val, dict):
                raise Error('Expected type of arrayParamValues is [ {"key":"value"} ]"')
            for key in val:
                if not type(key) == str:
                    raise Error("step %s: arrayParamValues must contain string keys" % stepName)
                if not type(val[key]) == str:
                    raise Error("step %s: arrayParamValues must contain string values" % stepName)
                if key not in arrayParamValues[0]:
                    raise Error("step %s: arrayParamValues must contain same sets of keys" % stepName)
            for key in arrayParamValues[0]:
                if key not in val:
                    raise Error("step %s: arrayParamValues must contain same sets of keys" % stepName)

    def get(name):
        if not name in Step.steps:
            raise Error("Step '%s' not defined" % name)
        return Step.steps[name]
    get=staticmethod(get)

def _isPrereq(candidate, task):
    """Returns True if candidate is a prereq of task"""
    if task == candidate:
        return True
    else:
        for t in task.deps:
            if _isPrereq(candidate, t):
                return True
    return False

def sortedSteps(begin, end):
    if begin:
        bs = Step.get(begin)
    if end:
        es = Step.get(end)
    list = []
    for s in Step.stepList:
        if ( not begin or _isPrereq(bs, s) ) and \
           ( not end or _isPrereq(s, es) ) :
            list.append(s)

    return list

class GridJobInfo(object):
    def __init__(self, job):
        self.job = job
        self.states = []

def _getNodeText(root, name, mustExist=True):
    import xml.dom

    elems = root.getElementsByTagName(name)
    if len(elems) != 1:
        if mustExist:
            raise str("Expected single '%s' under '%s'" % (name, root.tagName))
        else:
            return None
    e = elems[0]
    for ch in e.childNodes:
        if ch.nodeType == xml.dom.Node.TEXT_NODE:
            return ch.nodeValue.strip()
    if mustExist:
        raise str("Expected text node '%s' under '%s'" % (name, root.tagName))
    else:
        return None

def _parseQStatXml(xmlStr):
    from xml.dom import minidom

    doc = minidom.parseString(xmlStr).documentElement

    jobInfo = {}

    jobElems = doc.getElementsByTagName('job_list')
    for elem in jobElems:
        job = _getNodeText(elem, 'JB_job_number')
        state = _getNodeText(elem, 'state')
        tasks = _getNodeText(elem, 'tasks', False)

        if not job in jobInfo:
            jobInfo[job] = GridJobInfo(job)

        if not tasks:
            tasks = 'all'

        jobInfo[job].states.append( (tasks, state) )

    return jobInfo

def getGridJobInfo():
    """Returns status of user's jobs on the grid.

    The status is returned as a dict mapping job ID to GridJobInfo instance.
    """
    child = subprocess.Popen(['qstat', '-xml', '-u', '*'],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)

    output = child.stdout.read()
    sts = child.wait()
    if sts != 0:
        raise Error("qstat failed with status '%s', msg:\n%s" % (sts, output))

    return _parseQStatXml(output)
