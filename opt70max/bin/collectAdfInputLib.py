#!/usr/bin/python

import httplib
import base64
import wfclib.jsonutil
import urllib
import socket
import xml.dom
from xml.dom import minidom


# get optional arguments to the DPM findAccessions method from specified arguments
def buildArguments(argv):
    rc = ''
    if len(argv) > 2:
        for n in range(2, len(argv)):
            rc = rc + '&' + argv[n]
    return rc

# get the text associated with a dom element
def getText(element):
    for node in element.childNodes:
        if node.nodeType == node.TEXT_NODE:
            return node.data

# check response from DPM method for html; prin response and throw exception if found
def checkForHtml(response):
    count = response.count("<html>")
    if count > 0:
        print response
        raise Exception("Unexpected HTML response received from DPM")

# get the paths associated with accessions by using the DPM getPaths method
def getPathsFromDPM(accessions):
    params = "&accessions=" + "&accessions=".join(accessions)
    rc = {}
    url = "http://vip-app-pgcw3:8080/dpm/rs/dpm/getPaths?clientType=native/unix" + params
    data = urllib.urlopen(url).read();
    checkForHtml(data)
    dom = xml.dom.minidom.parseString(data)
    paths = dom.getElementsByTagName("entry")
    for path in paths:
        key = ''
        for node in path.childNodes:
            if node.localName == 'key':
                key = getText(node)
            elif node.localName == 'value':
                value = getText(node)
                rc[key] = value
    dom.unlink()
    return rc

def updateLocationsByDpm(accessionCycles,requestAccessions):
    rc = getPathsFromDPM(requestAccessions)
    for (k,v) in rc.items():
        accessionCycles[k].location = v

def findDataLocations(limsJson):
    lanes = limsJson.lanes
    
    accessionCycles = {}
    for l in lanes:
        slide = l.slideId
        lane = l.laneId
        cycles = l.cycles
        for c in cycles:
            cycle = int(c.number)
            accession = "%s_C%02d_L%s" % (slide, cycle, lane)
            accessionCycles[accession]=c
    
    requestAccessions = []
    for acc in accessionCycles.keys():
        requestAccessions.append(acc)
        #make request
        if len(requestAccessions)==50:
            updateLocationsByDpm(accessionCycles,requestAccessions)
            requestAccessions=[]
    if len(requestAccessions)!=0:
        updateLocationsByDpm(accessionCycles,requestAccessions)

def extractLimsJson(limsData):
    root = minidom.parseString(limsData).documentElement

    elems = root.getElementsByTagName('data')
    if len(elems) != 1:
        print limsData
        raise str("Expected single <data> block under '%s'" % (root.tagName))
    dataNode = elems[0]
    assert len(dataNode.childNodes)==1, "The <data> should have exactly one child"
    e = dataNode.childNodes[0]
    assert e.nodeType == xml.dom.Node.TEXT_NODE, "Node type: %s" % str(e.nodeType) 
    jsonData = e.nodeValue.strip()
    parsedJson = wfclib.jsonutil.loads(jsonData)
    return parsedJson
    
class LimsConnectionData(object):
    def __init__(self, server, workflow, user="makeADF", password="Gen0mics"):
        self.server = server
        self.workflow = workflow
        self.user = user
        self.password = password
        
    def __str__(self):    
        return self.server+'/'+self.workflow
    
def getLimsData(slide, lane, commData = LimsConnectionData("vip-app-plims:80","SeqLIMS")):
    print "extracting data from LIMS:", commData  
    
    hostname = socket.gethostname()
    
    request = \
    """<linx action="Get CyclePositionMap for SlideLane" clientType="makeADF"
                        clientVersion="collectAdf v1" device="asmPipeline"
                        host="%s" password="%s" port="80" task="Process Library"
                        user="%s" workflow="Sequencing LIMS">
                    <items>                                                               
                                     <FSX>%s</FSX>
                                     <Lane>%s</Lane>
                    </items>
    
    </linx>"""
    
    request = request % (hostname,commData.password,commData.user,slide,lane)
    
    auth = 'Basic ' + base64.encodestring(commData.user + ':' + commData.password).strip()
    headers = {"Content-type": "text/vnd-wildtype-linxml",
               "Accept": "*/*",
               "Authorization": auth}
    conn = httplib.HTTPConnection(commData.server)
    conn.request("POST", "/%s/Task_Authenticate" % commData.workflow, request, headers)
    response = conn.getresponse()
    if response.status!=200:
        print response.status, response.reason
        raise Exception('communication error')
    data = response.read()
    conn.close()
    return data
    
