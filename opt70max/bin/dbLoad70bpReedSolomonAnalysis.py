#!/usr/bin/python

"""

"""

__author__ = "Anushka Brownley"
__version__ = "$RCSId$"
__date__ = "$Date$"
__copyright__ = "Copyright (c) 2008 Complete Genomics"
__license__ = "Proprietary"

import os.path
import sys
import getopt
import shutil
import pymssql
import time
from datetime import datetime, date, time

HOST='VIP-PSQLDB02:1433'
USR='dwproduser'
PSWD='dwpr0d'
DBNAME='dwProd'

################################
def usage(script):
    print "\nThis script loads Reed Solomon Analysis results into the database\n"
    print script
    print "    -h, --help       <arg>   outputs this help menu"
    print "    -i, --input-dir  <arg>   directory with database input files"
    print "    -s, --slideID    <arg>   slide ID "
    print "    -l, --laneID     <arg>   lane ID"
    print "    -a, --arr-size   <arg>   grid array size\n"

################################################################
# If db execute fails retry
def executeTryLoop(cur, statement):
    waitTime = 2
    attempt = 0
    while True:
        try:
            cur.execute(statement)
            break
        except ValueError:
            time.sleep(waitTime)
            attempt += 1
            waitTime *= waitTime
            if attempt == 10:
                raise Exception("The following DB statement failed after 10 attempts: " + statement)

################################################################
# check that file exists
def checkFile(filename):
    if not os.path.isfile(filename):
        print "ERROR, the following required file does not exist: " + filename
        sys.exit(1)
        
################################################################
# gets the ID of the most recently inserted row in the current session
def getInsertID(cur, error):
    cur.execute("select scope_identity();")
    results = cur.fetchone()
    
    if len(results) > 1:
        print "ERROR: multiple "+error+" returned: " + results
        sys.exit(1)
    else:
        return int(results[0])
        
################################################################
def loadRSAnalysisRun(RSAnalysisRun,cycleStr,cur):
    date = str(datetime.now()).split(".")
    dateStr = date[0].replace("-","/")
    
    # set dye swap bit based on dye swap input parameter data 
    if RSAnalysisRun['DyeSwap'] == '':
        dyeSwap = 0
    else:
        dyeSwap = 1
    
    values = "'"+RSAnalysisRun['SWVersion']+"',"+\
             "'"+RSAnalysisRun['SlideID']+"',"+\
             "'"+RSAnalysisRun['LaneID']+"',"+\
             "'"+cycleStr+"',"+\
             RSAnalysisRun['ValidRun']+","+\
             RSAnalysisRun['RevComp']+","+\
             RSAnalysisRun['Recover']+","+\
             str(dyeSwap)+","+\
             "'"+RSAnalysisRun['DyeSwap']+"',"+\
             "'"+dateStr+"',"+\
             "'"+dateStr+"'"
             
    executeTryLoop(cur,"INSERT INTO rs_analysis.RSAnalysisRun (SWVersion,\
                                                        SlideID,\
                                                        LaneID,\
                                                        CycleSet,\
                                                        ValidRun,\
                                                        RevComp,\
                                                        Recover,\
                                                        DyeSwap,\
                                                        DyeSwapData,\
                                                        CreateDate,\
                                                        ModifyDate) VALUES("+values+");")
    return getInsertID(cur, "AnalysisRunIDs")
    
################################################################
def loadRSCyclePosMap(rsAnalysisRunID,RSCyclePosMap,cur):
    cyclePosMapIDDCT = {}
    for pos in RSCyclePosMap.keys():
        values = str(rsAnalysisRunID)+","+\
                 str(pos)+","+\
                 "'"+RSCyclePosMap[pos]['Anchor']+"',"+\
                 RSCyclePosMap[pos]['AnchorOffset']+","+\
                 RSCyclePosMap[pos]['Cycle']+","+\
                 RSCyclePosMap[pos]['TenMerNum']
        
        executeTryLoop(cur,"INSERT INTO rs_analysis.RSCyclePosMap (RSAnalysisRunID,\
                                                            DNBPosition,\
                                                            Anchor,\
                                                            AnchorOffset,\
                                                            Cycle,\
                                                            TenMerNum) VALUES("+values+");")
        cyclePosMapIDDCT[pos] = getInsertID(cur, "RSCyclePosMapIDs pos"+str(pos))
    
    return cyclePosMapIDDCT

################################################################
def loadLaneRSMetrics(lineLST, rsAnalysisRunID, cur):
    #print "loading LaneRSMetrics"
    header = lineLST[0]
    totalDNBs = lineLST[2]
    totalNoCallDNBs = lineLST[3]
    totalUnmappedDNBs = lineLST[4]
    totalPartialCalledDNBs = lineLST[5]
    totalPartialMappedDNBs = lineLST[6]
    totalRawMappedDNBs = lineLST[7]
    totalCorrectedDNBs = lineLST[8]
    totalHorizontalDuplicateDNBs = lineLST[9]
    totalVerticalDuplicateDNBs = lineLST[10]
    
    values = str(rsAnalysisRunID)+","+\
             totalDNBs+","+\
             totalNoCallDNBs+","+\
             totalUnmappedDNBs+","+\
             totalPartialCalledDNBs+","+\
             totalPartialMappedDNBs+","+\
             totalRawMappedDNBs+","+\
             totalCorrectedDNBs+","+\
             totalHorizontalDuplicateDNBs+","+\
             totalVerticalDuplicateDNBs
    
    executeTryLoop(cur,"INSERT INTO rs_analysis.LaneRSMetrics (RSAnalysisRunID,\
                                                         TotalDNBCount,\
                                                         NoCalledDNBCount,\
                                                         UnmappedDNBCount,\
                                                         PartialCalledDNBCount,\
                                                         PartialMappedDNBCount,\
                                                         RawMappedDNBCount,\
                                                         CorrectedMappedDNBCount,\
                                                         HorizDupDNBCount,\
                                                         VertDupDNBCount) VALUES("+values+");")
    
    return getInsertID(cur, "LaneRSMetricsIDS")
    
################################################################
def loadLanePositionMetrics(lineLST, laneRSMetricsID, cyclePosMapIDDCT, cur):
    #print "loading LanePositionMetrics"
    header = lineLST[0]
    completeData = False
    pos = int(lineLST[1])
    
    if not pos in cyclePosMapIDDCT.keys():
        return
    
    cyclePosMapID = cyclePosMapIDDCT[pos]
    
    EE = lineLST[2]
    if not EE == "N/A":
        completeData = True
    
    rawError = lineLST[3]
    if not rawError == "N/A":
        completeData = True
    
    critCutoffSize = int(lineLST[4])
    criticalCutoffLST = []
    curPos = 5
    for i in range(0,critCutoffSize):
        criticalCutoffLST.append(lineLST[curPos])
        curPos+=1
    
    areaDCT = {}
    for i in range(0,critCutoffSize-1):
        areaDCT[str(criticalCutoffLST[i])+"-"+str(criticalCutoffLST[i+1])] = lineLST[curPos]
        if not lineLST[curPos] == "N/A":
            completeData = True
        
        curPos += 1
    
    accuracyLST=[]
    for i in range(0,20+critCutoffSize):
        accuracyLST.append(str(lineLST[curPos]))
        if not lineLST[curPos] == "N/A":
            completeData = True
        curPos+=1
    
    callrateLST=[]
    for i in range(0,20+critCutoffSize):
        callrateLST.append(str(lineLST[curPos]))
        if not lineLST[curPos] == "N/A":
            completeData = True
        curPos+=1
    if completeData:
        insert = "INSERT INTO rs_analysis.LanePositionMetrics (LaneRSMetricsID,\
                                                               RSCyclePosMapID,\
                                                               EffectiveError,\
                                                               RawError"
        values = str(laneRSMetricsID)+","+\
                 str(cyclePosMapID)+","+\
                     EE+","+\
                     rawError
                
        executeTryLoop(cur,insert+") VALUES("+values+");")
        LanePositionMetricsID = getInsertID(cur, "LanePositionMetricsID")
        for key in areaDCT.keys():
            if not areaDCT[key] == "N/A":
                if areaDCT[key] == "MAX":
                    areaDCT[key] = -1
                
                executeTryLoop(cur,"INSERT INTO rs_analysis.LaneAreaMetrics (LanePositionMetricsID,\
                                                                             CutoffInterval,\
                                                                             Area) VALUES("+str(LanePositionMetricsID)+",'"+key+"',"+str(areaDCT[key])+");")
        
        # load critical cutoff data
        for i in range(0,critCutoffSize):
            if not accuracyLST[i] == "N/A":
                executeTryLoop(cur,"INSERT INTO rs_analysis.LaneAccuracyMetrics (LanePositionMetricsID,\
                                                                          Accuracy,\
                                                                          CallRate) VALUES("+str(LanePositionMetricsID)+","+accuracyLST[i]+","+callrateLST[i]+");")
        for i in range(0,20):
            rate = 5 * (i+1)
            if not accuracyLST[i] == "N/A":
                executeTryLoop(cur,"INSERT INTO rs_analysis.LaneAccuracyMetrics (LanePositionMetricsID,\
                                                                          DataPercentage,\
                                                                          Accuracy,\
                                                                          CallRate) VALUES("+str(LanePositionMetricsID)+","+str(rate)+","+accuracyLST[i+critCutoffSize]+","+callrateLST[i+critCutoffSize]+");")
    else:
        executeTryLoop(cur,"INSERT INTO rs_analysis.LanePositionMetrics (LaneRSMetricsID,RSCyclePosMapID) VALUES("+str(laneRSMetricsID)+","+str(cyclePosMapID)+");")

################################################################
def loadLaneTenMerMetrics(lineLST, laneRSMetricsID, TenMerDCT, cur):
    #print "loading LaneTenMerMetrics"
    completeData = False
    header = lineLST[0]
    tenMerNum = lineLST[1]
    if not tenMerNum in TenMerDCT.keys():
        return
        
    totalNoCall = lineLST[2]
    totalUnmapped = lineLST[3]
    totalRawMapped = lineLST[4]
    totalCorrected = lineLST[5]
    rawError = lineLST[6]
    effectiveError = lineLST[7]
    
    keys = TenMerDCT[tenMerNum].keys()
    keys.sort()
    cycleStr = "_".join(map(TenMerDCT[tenMerNum].get, keys))
    
    values = str(laneRSMetricsID)+","+\
             tenMerNum+","+\
             totalNoCall+","+\
             totalUnmapped+","+\
             totalRawMapped+","+\
             totalCorrected+",'"+\
             cycleStr+"'"
    
    if rawError == "N/A" and effectiveError == "N/A":
        executeTryLoop(cur,"INSERT INTO rs_analysis.LaneTenMerMetrics (LaneRSMetricsID,\
                                                                TenMerNum,\
                                                                NoCalledCodeCount,\
                                                                UnmappedCodeCount,\
                                                                RawMappedCodeCount,\
                                                                CorrectedMappedCodeCount,\
                                                                TenMerCycleSet) VALUES("+values+");")
    else:
        values += ","+rawError+","+effectiveError
        executeTryLoop(cur,"INSERT INTO rs_analysis.LaneTenMerMetrics (LaneRSMetricsID,\
                                                                TenMerNum,\
                                                                NoCalledCodeCount,\
                                                                UnmappedCodeCount,\
                                                                RawMappedCodeCount,\
                                                                CorrectedMappedCodeCount,\
                                                                TenMerCycleSet,\
                                                                AveRawError,\
                                                                AveEffectiveError) VALUES("+values+");")

################################################################
def loadFieldRSMetrics(lineLST, rsAnalysisRunID, cur):
    #print "loading FieldRSMetrics"
    header = lineLST[0]
    field = lineLST[1]
    totalDNBs = lineLST[2]
    totalNoCallDNBs = lineLST[3]
    totalUnmappedDNBs = lineLST[4]
    totalPartialCalledDNBs = lineLST[5]
    totalPartialMappedDNBs = lineLST[6]
    totalRawMappedDNBs = lineLST[7]
    totalCorrectedDNBs = lineLST[8]
    totalHorizontalDuplicateDNBs = lineLST[9]
    totalVerticalDuplicateDNBs = lineLST[10]
    
    values = str(rsAnalysisRunID)+","+\
             "'"+field+"',"+\
             totalDNBs+","+\
             totalNoCallDNBs+","+\
             totalUnmappedDNBs+","+\
             totalPartialCalledDNBs+","+\
             totalPartialMappedDNBs+","+\
             totalRawMappedDNBs+","+\
             totalCorrectedDNBs+","+\
             totalHorizontalDuplicateDNBs+","+\
             totalVerticalDuplicateDNBs
    
    executeTryLoop(cur,"INSERT INTO rs_analysis.FieldRSMetrics (RSAnalysisRunID,\
                                                         FieldName,\
                                                         TotalDNBCount,\
                                                         NoCalledDNBCount,\
                                                         UnmappedDNBCount,\
                                                         PartialCalledDNBCount,\
                                                         PartialMappedDNBCount,\
                                                         RawMappedDNBCount,\
                                                         CorrectedMappedDNBCount,\
                                                         HorizDupDNBCount,\
                                                         VertDupDNBCount) VALUES("+values+");")
    
    return getInsertID(cur, "FieldRSMetricsID")
            
################################
def loadFieldPositionMetrics(lineLST, fieldRSMetricsID, cyclePosMapIDDCT, cur):
    #print "loading FieldPositionMetrics"
    header = lineLST[0]
    completeData = False
    pos = int(lineLST[1])
    
    if not pos in cyclePosMapIDDCT.keys():
        return ("N/A", "N/A")
        
    cyclePosMapID = cyclePosMapIDDCT[pos]
    
    EE = lineLST[2]
    if not EE == "N/A":
        completeData = True
        
    rawError = lineLST[3]
    if not rawError == "N/A":
        completeData = True
    
    critCutoffSize = int(lineLST[4])
    criticalCutoffLST = []
    curPos = 5
    for i in range(0,critCutoffSize):
        criticalCutoffLST.append(lineLST[curPos])
        curPos+=1
        
    areaDCT = {}
    for i in range(0,critCutoffSize-1):
        areaDCT[str(criticalCutoffLST[i])+"-"+str(criticalCutoffLST[i+1])] = lineLST[curPos]
        if not lineLST[curPos] == "N/A":
            completeData = True
        
        curPos += 1
        
    accuracyLST=[]
    for i in range(0,20+critCutoffSize):
        accuracyLST.append(str(lineLST[curPos]))
        if not lineLST[curPos] == "N/A":
            completeData = True
        curPos+=1
    
    callrateLST=[]
    for i in range(0,20+critCutoffSize):
        callrateLST.append(str(lineLST[curPos]))
        if not lineLST[curPos] == "N/A":
            completeData = True
        curPos+=1
    
    if completeData:
        insert = "INSERT INTO rs_analysis.FieldPositionMetrics (FieldRSMetricsID,\
                                                                RSCyclePosMapID,\
                                                                EffectiveError,\
                                                                RawError"
        values = str(fieldRSMetricsID)+","+\
                 str(cyclePosMapID)+","+\
                     EE+","+\
                     rawError
        
        executeTryLoop(cur,insert+") VALUES("+values+");")
        
        FieldPositionMetricsID = getInsertID(cur, "FieldPositionMetricsID")
        # load area metrics
        for key in areaDCT.keys():
            if not areaDCT[key] == "N/A":
                if areaDCT[key] == "MAX":
                    areaDCT[key] = -1
                
                executeTryLoop(cur,"INSERT INTO rs_analysis.FieldAreaMetrics (FieldPositionMetricsID,\
                                                                          CutoffInterval,\
                                                                          Area) VALUES("+str(FieldPositionMetricsID)+",'"+key+"',"+str(areaDCT[key])+");")
        
        # load critical cutoff data
        for i in range(0,critCutoffSize):
            if not accuracyLST[i] == "N/A":
                executeTryLoop(cur,"INSERT INTO rs_analysis.FieldAccuracyMetrics (FieldPositionMetricsID,\
                                                                       Accuracy,\
                                                                       CallRate) VALUES("+str(FieldPositionMetricsID)+","+accuracyLST[i]+","+callrateLST[i]+");")
        
        # load data percentage CR vs accuracy data
        for i in range(0,20):
            rate = 5 * (i+1)
            if not accuracyLST[i] == "N/A":
                executeTryLoop(cur,"INSERT INTO rs_analysis.FieldAccuracyMetrics (FieldPositionMetricsID,\
                                                                       DataPercentage,\
                                                                       Accuracy,\
                                                                       CallRate) VALUES("+str(FieldPositionMetricsID)+","+str(rate)+","+accuracyLST[i+critCutoffSize]+","+callrateLST[i+critCutoffSize]+");")
    else:
        executeTryLoop(cur,"INSERT INTO rs_analysis.FieldPositionMetrics (FieldRSMetricsID,RSCyclePosMapID) VALUES("+str(fieldRSMetricsID)+","+str(cyclePosMapID)+");")
        FieldPositionMetricsID = getInsertID(cur, "FieldPositionMetricsID")
    
    return (FieldPositionMetricsID, pos)

################################
def loadFieldTenMerMetrics(lineLST, fieldRSMetricsID, TenMerDCT, cur):
    #print "loading FieldTenMerMetrics"
    completeData = False
    header = lineLST[0]
    tenMerNum = lineLST[1]
    if not tenMerNum in TenMerDCT.keys():
        return
        
    totalNoCall = lineLST[2]
    totalUnmapped = lineLST[3]
    totalRawMapped = lineLST[4]
    totalCorrected = lineLST[5]
    rawError = lineLST[6]
    effectiveError = lineLST[7]
    
    
    keys = TenMerDCT[tenMerNum].keys()
    keys.sort()
    cycleStr = "_".join(map(TenMerDCT[tenMerNum].get, keys))
    
    values = str(fieldRSMetricsID)+","+\
             tenMerNum+","+\
             totalNoCall+","+\
             totalUnmapped+","+\
             totalRawMapped+","+\
             totalCorrected+",'"+\
             cycleStr+"'"
    
    if rawError == "N/A" and effectiveError == "N/A":
        executeTryLoop(cur,"INSERT INTO rs_analysis.FieldTenMerMetrics (FieldRSMetricsID,\
                                                                 TenMerNum,\
                                                                 NoCalledCodeCount,\
                                                                 UnmappedCodeCount,\
                                                                 RawMappedCodeCount,\
                                                                 CorrectedMappedCodeCount,\
                                                                 TenMerCycleSet) VALUES("+values+");")
    else:
        values += ","+rawError+","+effectiveError
        executeTryLoop(cur,"INSERT INTO rs_analysis.FieldTenMerMetrics (FieldRSMetricsID,\
                                                                 TenMerNum,\
                                                                 NoCalledCodeCount,\
                                                                 UnmappedCodeCount,\
                                                                 RawMappedCodeCount,\
                                                                 CorrectedMappedCodeCount,\
                                                                 TenMerCycleSet,\
                                                                 AveRawError,\
                                                                 AveEffectiveError) VALUES("+values+");")

###########################################################################
def loadRSConfMatrix(lineLST, fieldPositionID, cyclePosMapIDDCT, cur):
    #print "loading RSConfMatrix"
    completeData = False
    confMatrix = ''
    header = lineLST[0]
    cutoff = lineLST[1]
    pos = int(lineLST[2])
    
    if not pos in cyclePosMapIDDCT.keys():
        return
        
    if not pos in fieldPositionID.keys():
        print "ERROR: Position does not exist in Field Metrics: " + str(pos)
        sys.exit(1)
        
    fieldPositionMetricsID = fieldPositionID[pos]
    confMatrixStr = ''
    for i in range(3,19):
        if lineLST[i].rstrip() == '':
            continue
            
        if not lineLST[i] == "N/A":
            completeData = True
            (base1,base2,count) = lineLST[i].rstrip().rsplit(":")
            confMatrixStr += ","+count
            confMatrix += ","+base1+"to"+base2
    
    if completeData:
        values = str(fieldPositionMetricsID)+","+\
                     cutoff+\
                     confMatrixStr
        executeTryLoop(cur,"INSERT INTO rs_analysis.RSConfMatrix (FieldPositionMetricsID,\
                                                           Cutoff"+\
                                                           confMatrix+") VALUES("+values+");")
    else:
        executeTryLoop(cur,"INSERT INTO rs_analysis.RSConfMatrix (FieldPositionMetricsID,Cutoff) VALUES("+str(fieldPositionMetricsID)+","+str(cutoff)+");")

###########################################################################

def main(argv):
    script = argv[0]
    curPath = os.path.dirname(os.path.abspath(script))
    argv = argv[1:]
    inputDir = None
    slideID = None
    laneID = None
    arrSize = None

    try:
        opts, args = getopt.getopt(argv, "h:i:s:l:a:", ["help=", "input-dir=", "slideID=", "laneID=", "arr-size="])
    except getopt.GetoptError:
        usage(script)
        sys.exit(1)
    
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(script)
            sys.exit(0)
        elif opt in ("-i", "--input-dir"):
            inputDir = arg
        elif opt in ("-s", "--slideID"):
            slideID = arg
        elif opt in ("-l", "--laneID"):
            laneID = arg
        elif opt in ("-a", "--arr-size"):
            arrSize = int(arg)
            
    # validate input
    if inputDir is None or arrSize is None or slideID is None or laneID is None:
        print "Please enter an input directory, a slideID, a laneID, and an array size."
        usage(script)
        sys.exit(1)
    
    if not(os.path.isdir(inputDir)):
        print "Please enter valid input directory"
        usage(script)
        sys.exit(1)
        
    # Connect to database
    conn = pymssql.connect(host=HOST, user=USR, password=PSWD, database=DBNAME)
    cur = conn.cursor()
    
    # check that all required db data upload files exist
    masterDBFile = inputDir + "/" + slideID + "_" + laneID + ".master_dbload.csv"
    checkFile(masterDBFile)
    laneDBFile = inputDir + "/" + slideID + "_" + laneID + ".dbload.csv"
    checkFile(laneDBFile)    
    for ArrID in range(0,arrSize):
        ArrID += 1
        fieldDBFile = inputDir + "/" + slideID + "_" + laneID + "_byField." + str(ArrID) + ".dbload.csv"
        checkFile(fieldDBFile)
    
    # Read analysis and cycle position tracking data
    inFH = open(masterDBFile, "r")
    RSAnalysisRun = {}
    masterDataLST = inFH.readline().rstrip().rsplit(",")
    RSAnalysisRun['SWVersion'] = masterDataLST[1]
    RSAnalysisRun['SlideID'] = masterDataLST[2]
    RSAnalysisRun['LaneID'] = masterDataLST[3]
    RSAnalysisRun['ValidRun'] = masterDataLST[4]
    RSAnalysisRun['RevComp'] = masterDataLST[5]
    RSAnalysisRun['Recover'] = masterDataLST[6]
    RSAnalysisRun['DyeSwap'] = masterDataLST[7]
    RSCyclePosMap = {}
    TenMerDCT = {}
    for line in inFH:
        lineLST = line.rstrip().rsplit(",")
        index = int(lineLST[1])
        RSCyclePosMap[index] = {}
        RSCyclePosMap[index]['Anchor'] = lineLST[2]
        RSCyclePosMap[index]['AnchorOffset'] = lineLST[3]
        RSCyclePosMap[index]['Cycle'] = lineLST[4]
        RSCyclePosMap[index]['TenMerNum'] = lineLST[5]
        if lineLST[5] in TenMerDCT.keys():
            TenMerDCT[lineLST[5]][index] = lineLST[4]
        else:
            TenMerDCT[lineLST[5]] = {index:lineLST[4]}
    inFH.close()
    keys = RSCyclePosMap.keys()
    keys.sort()
    cycleStr = "_".join(map(lambda x: x['Cycle'], map(RSCyclePosMap.get, keys)))

    # Load RS Analysis Run data
    rsAnalysisRunID = loadRSAnalysisRun(RSAnalysisRun,cycleStr,cur) 
    
    # Load RSCyclePosMap data
    cyclePosMapIDDCT = loadRSCyclePosMap(rsAnalysisRunID,RSCyclePosMap,cur)
    
    # Load lane level data
    laneDBFH = open(laneDBFile, "r")
    print "Loading " + laneDBFile + " . . ."
    for line in laneDBFH:
        lineLST = line.rstrip().rsplit(",")
        if lineLST[0] == "RSMetrics":
            laneRSMetricsID = loadLaneRSMetrics(lineLST, rsAnalysisRunID, cur)
        elif lineLST[0] == "PositionMetrics":
            loadLanePositionMetrics(lineLST, laneRSMetricsID, cyclePosMapIDDCT, cur)
        elif lineLST[0] == "TenMerMetrics":
            loadLaneTenMerMetrics(lineLST, laneRSMetricsID, TenMerDCT, cur)
    
    # Load field level data
    fieldPositionID = {}
    for ArrID in range(0,arrSize):
        ArrID += 1
        
        fieldDBFile = inputDir + "/" + slideID + "_" + laneID + "_byField." + str(ArrID) + ".dbload.csv"
        fieldDBFH = open(fieldDBFile, "r")
        print "Loading " + fieldDBFile + " . . ."
        for line in fieldDBFH:
            lineLST = line.rstrip().rsplit(",")
            if lineLST[0] == "RSMetrics":
                fieldRSMetricsID = loadFieldRSMetrics(lineLST, rsAnalysisRunID, cur)
            elif lineLST[0] == "PositionMetrics":
                (ID, pos) = loadFieldPositionMetrics(lineLST, fieldRSMetricsID, cyclePosMapIDDCT, cur)
                if not ID == "N/A":
                    fieldPositionID[pos] = ID
            elif lineLST[0] == "TenMerMetrics":
                loadFieldTenMerMetrics(lineLST, fieldRSMetricsID, TenMerDCT, cur)
            elif lineLST[0] == "RSConfMatrix":
                loadRSConfMatrix(lineLST, fieldPositionID, cyclePosMapIDDCT, cur)
    
    cur.close()
    conn.commit()
    
################################
if __name__ == "__main__":
    main(sys.argv)
