import os

class Email(object):
    def __init__(self):
        self.workDir = '.'
        self.inputemail = 'zhaofuxiang@genomics.cn'
    
    def implEmail(self):
        FROMEMAIL = 'zhaofuxiang@genomics.cn'
        subject = 'Your job (%s) is done!' % (os.path.basename(self.workDir))
        content = ''' Your job is done!\n All your files cound be found here %s/MAPS/ ''' % self.workDir

        msg = 'From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n' % (FROMEMAIL, self.inputemail, subject)
        msg += content
        print msg

        import smtplib
        s = smtplib.SMTP('localhost')
        s.sendmail(FROMEMAIL, self.inputemail, msg)
        s.quit()
 

email = Email()
email.implEmail()
