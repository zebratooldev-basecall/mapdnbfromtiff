#!/usr/bin/env python

import os
import sys
import re
import getopt

def main():

    # parse command line options and update variables
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:", [])
    except getopt.GetoptError:
        sys.exit(usage)

    for opt, arg in opts:
        if opt == "-h":
            sys.exit('Use me properly.')
        elif opt == '-i':
            INFILE = arg
           
    inf = open(INFILE, 'r')
    outf = open(re.sub("[.]vcf","_Categorized.bed", INFILE), 'w')
    prevpos = ""
    prevoutline = ""
    for line in inf:

        ## Only want MIEs, not concordant calls
        if "GOOD" in line or "good" in line: continue

        if "bad" in line: 
            BadFlag = "bad"
        else:
            BadFlag = "BAD"
            
        ## Only want fully called loci in the parents (the child can be half-called)
        ParentNoCallFlag = "ParentsFullyCalled"
        if "HasNoCalls=True" in line:
            ParentNoCallFlag = "ParentsHaveNoCalls"

        (allele, variantID, variant, zygosity, varType, VAFscore, EAFscore, HasNoCalls, Concordance, chr, pos, ID, ref, alt, qual, filter, info, format, kid, mom, dad) = line.split()

        ## Calculate start and stop position based on varType
        start = 0
        stop = 0
        pos = int(pos)
        if varType in set([ "snp", "mnp", "ref" ]):
            start = pos-1
        elif varType in set([ "del", "ins", "complex" ]):
            start = pos
        if varType in set([ "del", "mnp", "ref", "complex" ]):
            stop = pos + len(ref) - 1
        elif varType in set([ "snp", "ins" ]):
            stop = pos

        # Find alternate varType if this is a ref call
        altVarType = "."
        kid = re.sub("[|/]", "", re.sub(":.*", "", kid))
        mom = re.sub("[|/]", "", re.sub(":.*", "", mom))
        dad = re.sub("[|/]", "", re.sub(":.*", "", dad))

        ## even more strict definition of "MIE"
        RefParentsFlag = "ParentsHomRef"
        if mom != "00" or dad != "00":
            RefParentsFlag = "ParentsNotHomRef"

        if varType  ==  "ref":
            options = ""
            if zygosity == "hom" or zygosity == "half":
                if not "0" in mom: options += mom
                if not "0" in dad: options += dad              
            elif zygosity == "het-ref":
                if kid[0] in mom and kid[0] in dad:  options += kid[0]
                if kid[1] in mom and kid[1] in dad:  options += kid[1]
                mom = re.sub(kid[0],"",re.sub(kid[1],"",mom))
                dad = re.sub(kid[0],"",re.sub(kid[1],"",dad))
                if len(mom)==2: options += mom
                if len(dad)==2: options += dad
                if len(mom)==1 and len(dad)==1: options += mom + dad
            else:
                sys.exit("I'm confused about this:\n" + line)
            varTypes = "ref," + re.sub(".*TYPE=", "", info)
            altVarType = "_".join(sorted(set([ varTypes.split(",")[int(x)] for x in set(options) if x!="." ])))
  
        ## construct output line
        outline = "\t".join([chr, str(start), str(stop), varType, zygosity, altVarType, BadFlag, ParentNoCallFlag, RefParentsFlag]) + "\n"

        ## Check if the previous line was the same, if it is, don't write this line too 
        if pos == prevpos:
            ## potentially duplicate lines -  try to figure out what to do
            if outline == prevoutline:
                ## if they are identical then just don't print now, print on the next round.  No need to change prev* variables since they are all identical
                continue
            else:
                ## construct the new output line that is a hybrid of this line and the previous line
                oldInfo = prevoutline.split("\t")
                start = min( start, int(oldInfo[1]))
                stop  = max( stop,  int(oldInfo[2]))
                varType = "_".join(sorted([ varType, oldInfo[3] ]))
                prevoutline = "\t".join([chr, str(start), str(stop), varType, zygosity, altVarType, BadFlag, ParentNoCallFlag, RefParentsFlag]) + "\n"
                ## no need to update prevpos, it hasn't changed
        else :
            # actually write to output
            outf.write( prevoutline )
            ## Setup for next line
            prevpos = pos
            prevoutline = outline

    outf.write( prevoutline )
            
###################################################

if __name__ == "__main__":
    main()
