#!/usr/bin/env python

from optparse import OptionParser
from os.path import join as pjoin
from ConfigParser import ConfigParser
import os
import sys
import pymssql

"""
Upload results from Discordance Pattern CNT workflow to dwProd.

"""
class DBLoader:

    def __init__(self,dbLogin):
        self.conn = None
        self.dbLogin=dbLogin
        self.rows = []
        self.accumulate=""
        self.insertion_count=0

    def connect(self,whichDB="idqc:dwProd"):
        self.conn = pymssql.connect(host=self.dbLogin.get(whichDB,'host'),
                               user=self.dbLogin.get(whichDB,'user'),
                               password=self.dbLogin.get(whichDB,'password'),
                               database=self.dbLogin.get(whichDB,'database'))
        if not self.conn:
            print "can't connect"
            os.sys.exit()

    def execute(self,query):
        cur = self.conn.cursor()
        cur.execute(query)
        self.rows = cur.fetchall()
        cur.close()
        return self.rows[0][0]

    def execute_no_result(self, query, debug):
        cur = self.conn.cursor()
        if not debug:
            #try :
            cur.execute(query)
            #except pymssql.OperationalError:
            #    pass
            #cur.close()
        else:
            print query
            print "debug run! no insertion!"

    def check_header(self, fn, header, expected):
        test = header.rstrip('\r\n')

        if test != ','.join(expected):
            print test
            print expected
            raise Exception('Header for %s file does not matched expected!' % fn)

    def commit_accumulated_insertions(self, debug):
        if self.insertion_count > 0:
            print self.accumulate
            self.execute_no_result(self.accumulate, debug)
            self.accumulate = ""
            self.conn.commit()
            self.insertion_count=0

    def insert_positional_discordance(self, lane, fn, debug):
        HEADER = ['position','totalBaseCount','discordantBaseCount','discordanceRate']

        # check for file.
        if not os.path.exists(fn):
            raise Esceiption('Positional discordance file does not exist! %s' % fn)
        print(fn)

        # read through file and insert records.
        ff = open(fn, 'r')
        header = ff.readline()

        # check header.
        self.check_header(fn, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            print(len(fields))
            if fields[3] != 'inf':
                self.accumulate += "INSERT INTO [bioinfo].cnt_positional_discordance VALUES ('%s',%s,%s,%s,%s);\n" % \
                    (lane, fields[0], fields[1], fields[2], fields[3])
                self.insertion_count += 1
                if self.insertion_count > 49:
                    self.commit_accumulated_insertions(debug)
            else:
                print('insert_positional_discordance: Division by zero error! lane=%s position=%s' % (lane, fields[0]))
                print(fields)
        ff.close()
        self.commit_accumulated_insertions(debug)


    def insert_correlated_discordance(self, lane, fn, debug):
        HEADER = ['queryPosition,comparisonPosition,totalDiscBaseCount,correlatedDiscBaseCount,correlatedDiscordanceFraction']

        # check for file.
        if not os.path.exists(fn):
            raise Esceiption('Correlated positional discordance file does not exist! %s' % fn)

        # read through file and insert records.
        ff = open(fn, 'r')
        header = ff.readline()

        # check header.
        self.check_header(fn, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            if fields[4] != 'inf':
                self.accumulate += "INSERT INTO [dbo].dp_correlated_positional_discordance VALUES ('%s',%s,%s,%s,%s,%s);\n" % \
                    (lane, fields[0], fields[1], fields[2], fields[3],fields[4])
                self.insertion_count += 1
                if self.insertion_count > 49:
                    self.commit_accumulated_insertions(debug)
            else:
                print('insert_correlated_discordance: Division by zero error! lane=%s queryPosition=%s comparisonPosition=%s' % (lane, fields[0], fields[1]))
                print(fields)
        ff.close()
        self.commit_accumulated_insertions(debug)

    def insert_correlated_base_calls(self, lane, fn, debug):
        HEADER = ['queryPosition','comparisonPosition','totalDiscBaseCount','correlatedBaseCalls','correlatedBaseCallFraction','overAbundantCorrBaseCalls','fracOverAbundantCorrBaseCalls']

        # check for file.
        if not os.path.exists(fn):
            raise Esceiption('Correlated base call file does not exist! %s' % fn)

        # read through file and insert records.
        ff = open(fn, 'r')
        header = ff.readline()

        # check header.
        self.check_header(fn, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            if fields[4] != 'inf' and fields[6] != 'inf':
                self.accumulate += "INSERT INTO [dbo].dp_correlated_base_calls VALUES ('%s',%s,%s,%s,%s,%s,%s,%s);\n" % \
                    (lane, fields[0], fields[1], fields[2], fields[3], fields[4], fields[5], fields[6])
                self.insertion_count += 1
                if self.insertion_count > 49:
                    self.commit_accumulated_insertions(debug)
            else:
                print('insert_correlated_base_calls: Division by zero error! lane=%s queryPosition=%s comparisonPosition=%s' % (lane, fields[0], fields[1]))
                print(fields)
        ff.close()
        self.commit_accumulated_insertions(debug)

    def insert_confusion_matrices(self, lane, fn, debug):
        HEADER = ['position','callBase','refBase','count','fraction']

        # check for file.
        if not os.path.exists(fn):
            raise Exception('Positional confusion matrices file does not exist! %s' % fn)

        # read through file and insert records.
        ff = open(fn, 'r')
        header = ff.readline()

        # check header.
        self.check_header(fn, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            if fields[4] != 'inf':
                self.accumulate += "INSERT INTO [bioinfo].cnt_confusion_matrices VALUES ('%s',%s,'%s','%s',%s,%s);\n" % \
                    (lane, fields[0], fields[1], fields[2], fields[3], fields[4])
                self.insertion_count += 1
                if self.insertion_count > 49:
                    self.commit_accumulated_insertions(debug)
            else:
                print('insert_confusion_matrices: Division by zero error! lane=%s position=%s callBase=%s refBase=%s' % (lane, fields[0], fields[1], fields[2]))
                print(fields)
        ff.close()
        self.commit_accumulated_insertions(debug)
    
    # insert mapping info
    def insert_gap_diff(self, lane, fn, mapType, debug):

        # check for file
        if not os.path.exists(fn):
            raise Exception('%s file does not exist' % fn)
        
        # paired mappings 
        # check header.
        if mapType == 'paired':
            HEADER = ['leftVsRightArmGapDifference','fullUniqMappedDnbCount','fullUniqMappedDnbPercent']
            ff = open(fn, 'r'); header = ff.readline()
            self.check_header(fn, header, HEADER)

            # loop through file, read records, and perform inserts.
            for line in ff:
                fields = line.rstrip('\r\n').split(',')
                self.accumulate += \
                    "INSERT INTO [bioinfo].cnt_arm_gap_difference VALUES ('%s','paired',%s,%s,%s);\n" % \
                    (lane, fields[0], fields[1],fields[2])
                self.insertion_count += 1
                if self.insertion_count > 49:
                    self.commit_accumulated_insertions(debug)
            ff.close()
            self.commit_accumulated_insertions(debug)

        # chimeric mappings
        elif mapType == 'chimera':
            
            # check header.
            HEADER = ['gapDifference','chimericDnbCountTotal','dnbCountWithGapDiff','dnbPctWithGapDiff']
            ff = open(fn, 'r'); header = ff.readline()
            self.check_header(fn, header, HEADER)

            # loop through file, read records, and perform inserts.
            for line in ff:
                fields = line.rstrip('\r\n').split(',')
                self.accumulate += \
                    "INSERT INTO [bioinfo].cnt_arm_gap_difference  VALUES ('%s','chimera',%s,%s,%s);\n" % \
                    (lane, fields[0], fields[2], fields[3])
                self.insertion_count += 1
                if self.insertion_count > 49:
                    self.commit_accumulated_insertions(debug)
            ff.close()
            self.commit_accumulated_insertions(debug)

    def insert_misc_stats(self, lane, fnUniqSeq, fnOverlap, debug):
        # load uniq sequence info
        HEADER = ['metric','count']

        # check for file
        if not os.path.exists(fnUniqSeq):
            raise Exception('%s file does not exist' % fnUniqSeq)

        # check header.
        ff = open(fnUniqSeq, 'r'); header = ff.readline()
        self.check_header(fnUniqSeq, header, HEADER)

        # loop through file, read records, and perform inserts.
        totalDnb = 0; uniqSeq = 0
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            if fields[0] == 'totalFullyUniquelyMappedDNBs':
                totalDnb = fields[1]
            elif fields[0] == 'uniqueSequencesOfFullyUniquelyMappedDNBs':
                uniqSeq = fields[1]

        # load overlap info and write
        HEADER = ['unmappedArms','armswith3Tin5T','pctArmsWith3Tin5T']

        # check for file
        if not os.path.exists(fnOverlap):
            raise Exception('%s file does not exist' % fnOverlap)

        # check header.
        ff = open(fnOverlap, 'r'); header = ff.readline()
        self.check_header(fnOverlap, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            self.accumulate += \
                "INSERT INTO [bioinfo].cnt_mapping_misc  VALUES ('%s',%s,%s,%s,%s,%s);\n" % \
                (lane, totalDnb, uniqSeq, fields[0], fields[1], fields[2])
            self.insertion_count += 1
            if self.insertion_count > 49:
                self.commit_accumulated_insertions(debug)
        ff.close()
        self.commit_accumulated_insertions(debug)
    
    def insert_map_gc_content(self, lane, fn, debug):
        HEADER = ['chromosome','ATbases','GCbases','GCpctOfUniquelyFullyMappedDNBs']

        # check for file
        if not os.path.exists(fn):
            raise Exception('%s file does not exist' % fn)

        # check header.
        ff = open(fn, 'r'); header = ff.readline()
        self.check_header(fn, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            self.accumulate += \
                "INSERT INTO [bioinfo].cnt_gc_content_by_chromosome VALUES ('%s','%s',%s,%s,%s);\n" % \
                (lane, fields[0], fields[1], fields[2], fields[3])
            self.insertion_count += 1
            if self.insertion_count > 49:
                self.commit_accumulated_insertions(debug)
        ff.close()
        self.commit_accumulated_insertions(debug)

    # insert unmapped stats
    def insert_unmap_base_seq(self, lane, fn, fnGC, debug):
        # load gc by base info
        gcs = {} #gcs[position] = gcpct
        HEADER = ['Position','pctGC']

        # check for file
        if not os.path.exists(fnGC):
            raise Exception('%s file does not exist' % fnGC)

        # check header.
        ff = open(fnGC, 'r'); header = ff.readline()
        self.check_header(fnGC, header, HEADER)

        # loop through file, read records
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            position = int(fields[0])
            gcpct = float(fields[1])
            gcs[position] = gcpct
        ff.close()

        # go through other seq info and upload
        if not os.path.exists(fn):
            raise Exception('%s file does not exist' % fn)

        # check header.
        HEADER = ['Position','Base','Fraction']
        ff = open(fn, 'r'); header = ff.readline()
        self.check_header(fn, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            position = int(fields[0])
            gcpct = gcs.get(position,0)
            if gcpct == 'nan':
                gcpct = 0
            if fields[1] == 'nan':
                continue
            if fields[2] == 'nan':
                continue
            self.accumulate += \
                "INSERT INTO [bioinfo].cnt_unmapped_base_seq VALUES ('%s',%s,'%s',%s,%s);\n" % \
                (lane, fields[0], fields[1], fields[2], gcpct)
            self.insertion_count += 1
            if self.insertion_count > 49:
                self.commit_accumulated_insertions(debug)
        ff.close()
        self.commit_accumulated_insertions(debug)

    def insert_unmap_gc_by_seq(self, lane, fn, debug):
        HEADER = ['pctGC','dnbCount','totalDnbs','dnbPct']

        # check for file
        if not os.path.exists(fn):
            raise Exception('%s file does not exist' % fn)

        # check header.
        ff = open(fn, 'r'); header = ff.readline()
        self.check_header(fn, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            self.accumulate += \
                "INSERT INTO [bioinfo].cnt_unmapped_gc_by_sequence  VALUES ('%s',%s,%s);\n" % \
                (lane, fields[0], fields[3])
            self.insertion_count += 1
            if self.insertion_count > 49:
                self.commit_accumulated_insertions(debug)
        ff.close()
        self.commit_accumulated_insertions(debug)

    def insert_unmap_kmers(self, lane, fn, debug):
        HEADER = ['kmer','avg_observed_over_expected']

        # check for file
        if not os.path.exists(fn):
            raise Exception('%s file does not exist' % fn)

        # check header.
        ff = open(fn, 'r'); header = ff.readline()
        self.check_header(fn, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            self.accumulate += \
                "INSERT INTO [bioinfo].cnt_unmapped_kmers  VALUES ('%s','%s',%s);\n" % \
                (lane, fields[0], fields[1])
            self.insertion_count += 1
            if self.insertion_count > 49:
                self.commit_accumulated_insertions(debug)
        ff.close()
        self.commit_accumulated_insertions(debug)

    ###### Mega-section for parsing MappingStats.csv TeraMap output and uploading to multiple tables
    def insert_mapping_stats(self, lane, fn, dnbarch, debug):
        # check for file
        if not os.path.exists(fn):
            raise Exception('%s file does not exist' % fn)

        # check that dnb architecture is supported
        dnbinserts = {'ad2_40_40' : 38,
                       'ad2_29_29' : 29 }
        if dnbarch not in dnbinserts:
            raise Exception('%s dnb architecture is not supported' % dnbarch)
        dnbinsert = dnbinserts[dnbarch]

        # read through file
        fi = open(fn)

        try:
            insertSizes = {} ##insertSizes[insert] = [left dnb count, right dnb count]
            mategaps = {} ##mategaps[mategap size] = count
            leftgaptotal = 0; rightgaptotal = 0; mategaptotal = 0
            for line in fi:
                line = line.rstrip().split(',')
                if line[0] == 'dict' or line[0] == 'array':
                    table = line[1]
                    if table == 'map':
                        map_values = [lane]
                        line = fi.next()
                        while line != '\n':
                            line = line.rstrip().split(',')
                            map_values.append(line[1])
                            line = fi.next() 
                        outstring1 = ",%s" * 48
                        outstring2 = "INSERT INTO [bioinfo].cnt_mapping_stats VALUES ( '%s'" + \
                                outstring1 + ");\n"
                        self.accumulate += outstring2 % (tuple(map_values))
                        self.insertion_count += 1
                        if self.insertion_count > 49:
                            self.commit_accumulated_insertions(debug)
                    elif table == 'gap1':
                        line = fi.next(); line = fi.next()
                        while line != '\n':
                            line = line.rstrip().split(',')
                            insert = int(line[0]) + dnbinsert 
                            count = int(line[1])
                            line = fi.next()
                            insertSizes[insert] = insertSizes.setdefault(insert, [0,0])
                            insertSizes[insert][0] = count
                            leftgaptotal += count
                    elif table == 'gap5':
                        line = fi.next(); line = fi.next()
                        while line != '\n':
                            line = line.rstrip().split(',')
                            insert = int(line[0]) + dnbinsert 
                            count = int(line[1])
                            line = fi.next()
                            insertSizes[insert] = insertSizes.setdefault(insert, [0,0])
                            insertSizes[insert][1] = count
                            rightgaptotal += count
                    elif table == 'mateGap':
                        line = fi.next(); line = fi.next()
                        while line != '\n':
                            line = line.rstrip().split(',')
                            insert = int(line[0]); count = int(line[1])
                            line = fi.next()
                            mategaps[insert] = mategaps.get(insert,0) + count
                            mategaptotal += count

        finally:
            fi.close()
            # calculate fraction for insert sizes, upload
            for insertSize, counts in insertSizes.iteritems():
                pctleft = float(counts[0]) / float(leftgaptotal) * 100
                pctright = float(counts[1]) / float(rightgaptotal) * 100
                self.accumulate += \
                     "INSERT INTO [bioinfo].cnt_arm_insert_sizes VALUES ( '%s',%s,%s,%s,%s,%s );\n" % (lane, insertSize, counts[0],counts[1],pctleft,pctright)
                self.insertion_count += 1
                if self.insertion_count > 49:
                    self.commit_accumulated_insertions(debug)
            
            # calculate pct values for mate gaps and upload
            for insert, count in mategaps.iteritems():
                pctgap = float(count) / float(mategaptotal)
                self.accumulate += \
                    "INSERT INTO [bioinfo].cnt_mate_gap VALUES ( '%s',%s,%s,%s );\n" % (lane, insert, count,pctgap)
                self.insertion_count += 1
                if self.insertion_count > 49:
                    self.commit_accumulated_insertions(debug)
            self.commit_accumulated_insertions(debug)

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-d', '--input-directory-disc', help='path to directory containing discordance pattern output files', dest='input_directory_disc')
    parser.add_option('-u', '--input-directory-unmapped', help='path to directory containing unmapped analysis output files', dest='input_directory_unmapped')
    parser.add_option('-m', '--input-directory-mapped', help='path to mapping directory ', dest='input_directory_mapped')
    parser.add_option('-l', '--lane',            help='lane to run db uploader on',                                    dest='lane')
    parser.add_option('-c', '--db-user-config',  help='configuration file with db users and passwords',                dest='db_config_file')
    parser.add_option('-t', '--test',           help='debug upload tool, prints insert statements to log file',       dest='debug', action='store_true', default=False)
    parser.add_option('-a', '--dnb-architecture',           help='dnb architecture alias.  Default:  ad2_40_40',       dest='dnbarch', default='ad2_40_40')

    # parse command line arguments.
    options, args = parser.parse_args(arguments[1:])

    return options.input_directory_disc, options.input_directory_unmapped, options.input_directory_mapped, options.lane, options.db_config_file, options.debug, options.dnbarch

def main(arguments):
    # parse command line arguments.
    inputDirectory, inputDirectoryUnmapped, inputDirectoryMapped, lane, db_config_file, debug, dnbarch = parse_arguments(arguments)

    # read in db user config file.
    dbUserConfig = ConfigParser()
    dbUserConfig.read(db_config_file)

    # initialize db loader.
    dbl = DBLoader(dbUserConfig)
    dbl.connect()

    # updated tables
    dbl.insert_gap_diff(lane, pjoin(inputDirectory, lane, 'arm_gap_differences.csv'), 'paired', debug) 
    dbl.insert_gap_diff(lane, pjoin(inputDirectoryUnmapped, lane, 'chimericDnbGapDifferences.csv'), 'chimera', debug) 
    dbl.insert_misc_stats(lane, pjoin(inputDirectory, lane, 'dnb_sequence_duplication.csv'), pjoin(inputDirectoryUnmapped, lane, 'complete3T5Toverlap.csv'), debug)
    dbl.insert_unmap_base_seq(lane, pjoin(inputDirectoryUnmapped, lane, 'fastqcOut','base_sequences.csv'), pjoin(inputDirectoryUnmapped, lane, 'fastqcOut', 'gc_by_base.csv'), debug)

    # insert mapping info
    dbl.insert_mapping_stats(lane, pjoin(inputDirectoryMapped, lane, 'reports/MappingStats.csv'), dnbarch, debug)
    dbl.insert_map_gc_content(lane, pjoin(inputDirectory,lane,'gc_content.csv'), debug)

    # insert unmapped stats
    dbl.insert_unmap_gc_by_seq(lane, pjoin(inputDirectoryUnmapped, lane, 'fastqcOut', 'gc_by_sequence.csv'), debug)
    #dbl.insert_unmap_kmers(lane, pjoin(inputDirectoryUnmapped, lane, 'fastqcOut', 'kmers_OE.csv'), debug)
    
    # insert positional discordance.
    dbl.insert_positional_discordance(lane, pjoin(inputDirectory, lane, 'positional_discordance.csv'), debug)
    #dbl.insert_correlated_discordance(lane, pjoin(inputDirectory, lane, 'correlated_discordance.csv'), debug)
    #dbl.insert_correlated_base_calls(lane, pjoin(inputDirectory, lane, 'correlated_base_calls.csv'), debug)
    dbl.insert_confusion_matrices(lane, pjoin(inputDirectory, lane, 'positional_confusion_matrices.csv'), debug)

main(sys.argv)
