#!/bin/tcsh -ef

set ASMPROFDIR=$1
set SIM=$2
set width=$3

cd ${ASMPROFDIR}

echo $width
set list=`echo *forplot.noSim`
foreach f ( $list )
   set chr=`echo $f | awk -F. '{print $1}'`
   tail -n +2 $f | sort -k 2,2 > ${f}.lexsort
   sort -k 1,1 ${SIM}/smoothed_${width}/${chr}.profile > ${chr}.sim
   set missing=`join -1 2 -2 1 -v 1 ${f}.lexsort ${chr}.sim  |wc -l`
   if ( $missing != "0") then
       echo "ABORT ON $f in $ASMPROFDIR"
       exit -1
   endif
   join -1 2 -2 1 ${f}.lexsort ${chr}.sim | awk '{tmp=$2;$2=$1;$1=tmp;print}' | sort -k 1,1 -k 2,2n > ${chr}.forplot
   /bin/rm ${chr}.sim ${f}.lexsort
end

