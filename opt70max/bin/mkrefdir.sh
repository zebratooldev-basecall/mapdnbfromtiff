#! /bin/bash

# This script fills out a reference directory with dummy files and
# such so that you can run workflows against the reference. To run
# this, you must have a reference.gbi file in the current working
# directory, and you must be in a directory like
# <rootdir>/REF/<refid>. You must also have $CGI_HOME/bin in your
# PATH.

if [ "$CGI_HOME" = "" ]; then
    echo error: CGI_HOME not set
    exit 1
fi

if [ "$CGI_HOME/bin/driver.py" != $(which driver.py) ]; then
    echo 'error: $CGI_HOME/bin/driver.py is not first driver.py in PATH.'
    exit 1
fi

if [ $(basename $(dirname $(pwd))) != "REF" ]; then
    echo 'error: grandparent directory must be named REF'
    exit 1
fi

set -e -x

ln -snf ../HUMAN-M_06-REF/CovModel CovModel

echo '"snp_id","var_str","contig_chr","start_pos","end_pos","ref_allele","orientation","loc_type","chromosome_id"' >dbSNP.csv

cat >coding-regions.csv <<EOF
# callannotate $Change: 12517 $ Mar 30 2009 11:23:49
# Date: 2009-Mar-30 11:41:46.709054
contig,begin,end
EOF

echo 'mrnaacc,protacc,geneid,orientation,exoncount,chrom,exonstarts,exonends,genomecdsstart,genomecdsend,mrnacdsstart,mrnacdsend,selenocysteine,nucleotideseq,proteinseq,frameshift' >gene_annotations.csv

cat >summary_validation.csv <<EOF
freeform,summaryValidation
name,minValue,maxValue
Unique sequence coverage (Gb),120,
% genome with coverage >= 20,70,
% genome with coverage >= 30,50,
% genome with coverage >= 40,40,
% genome with coverage >= 50,,
% genome with coverage >= 60,,
Coverage validation failures,,0
SNP Transitions/transversions,1.75,2.25
SNP het/hom ratio,1.5,2.5
INS het/hom ratio,1.5,2.5
DEL het/hom ratio,1.5,2.5
DELINS het/hom ratio,1.5,2.5
SNP total count,2500000,4000000
INS total count,100000,300000
DEL total count,100000,300000
DELINS total count,,100000
SNP novel rate,,15
INS novel rate,,50
DEL novel rate,,50
DELINS novel rate,,
Fully called genome fraction,,
Partially called genome fraction,,
No-called genome fraction,,85
Synonymous SNP loci,7000,11000
Missense SNP loci,7000,11000
Nonsense SNP loci,50,200
Nonstop SNP loci,,50
Frame-shifting INS loci,,200
Frame-shifting DEL loci,,200
Frame-shifting DELINS loci,,200
Frame-preserving INS loci,,
Frame-preserving DEL loci,,
Frame-preserving DELINS loci,,
Frame-shifting/preserving ratio,,2
Nonsyn/syn SNP ratio,0.75,1.25
Insertion/deletions ratio,0.75,1.25
Ins+del/SNP ratio,0.075,0.125
Coding insertion/deletions ratio,0.75,1.25
Coding SNP/all SNP ratio,,0.01
Coding (ins+del)/all (ins+del) ratio,,0.01
Validation failures,,0
EOF

mkdir 55or70_0to2_4to7_index
mkdir 55or70_0to3_3to8_index

if [ $(ls -l reference.gbi | awk '{print $5}') -lt 10000000 ]; then

# Use small-reference index configs.
echo Using small index configs, gbi file size: $(ls -l reference.gbi | awk '{print $5}').

cat >55or70_0to2_4to7_index/35+35_0to2_4to7.config <<EOF
#
#
version 1
index index.1 spec 0,1,2,3,4,5,6,7,8,9;12;0,1,2,3,4,5
index index.2 spec 0,1,2,3,4,5,6,7,8,9;17;0,1,2,3,4,5
index index.3 spec 0,1,2,3,4,5,6,7,8,9;-24;0,1,2,3,4,5

driver arm 0 fixed 1 floating 2 mutable 6 index-id 0
driver arm 0 fixed 2 floating 3 mutable 6 index-id 1
driver arm 0 fixed 3 floating 1 mutable 6 index-id 2
driver arm 1 fixed 0 floating 1 mutable 6 index-id 1
driver arm 1 fixed 1 floating 2 mutable 6 index-id 0
driver arm 1 fixed 2 floating 0 mutable 6 index-id 2

EOF

cat >55or70_0to2_4to7_index/35+35_0to2_4to7_nomutate.config <<EOF
#
#
version 1
index index.1 spec 0,1,2,3,4,5,6,7,8,9;12;0,1,2,3,4,5
index index.2 spec 0,1,2,3,4,5,6,7,8,9;17;0,1,2,3,4,5
index index.3 spec 0,1,2,3,4,5,6,7,8,9;-24;0,1,2,3,4,5

driver arm 0 fixed 1 floating 2 mutable 0 index-id 0
driver arm 0 fixed 2 floating 3 mutable 0 index-id 1
driver arm 0 fixed 3 floating 1 mutable 0 index-id 2
driver arm 1 fixed 0 floating 1 mutable 0 index-id 1
driver arm 1 fixed 1 floating 2 mutable 0 index-id 0
driver arm 1 fixed 2 floating 0 mutable 0 index-id 2

EOF

cat >55or70_0to3_3to8_index/35+35_0to3_3to8.config <<EOF
#
# Dnb archtecture (simplified to exclude gap values we are sacrificing):
# 5 (-2,0) 10 (0,2) 10 (4,7) 10  -mate-gap-  10 (4,7) 10 (0,2) 10 (-2,0) 5
# Reference size: full human (18 bases in the key)
#
version 1
index index.1 spec 0,1,2,3,4,5,6,7,8,9;13;0,1,2,3,4
index index.2 spec 0,1,2,3,4,5,6,7,8,9;18;0,1,2,3,4
index index.3 spec 0,1,2,3,4,5,6,7,8,9;-23;0,1,2,3,4
driver arm 0 fixed 1 floating 2 mutable 5 index-id 0
driver arm 0 fixed 2 floating 3 mutable 5 index-id 1
driver arm 0 fixed 3 floating 1 mutable 5 index-id 2
driver arm 1 fixed 0 floating 1 mutable 5 index-id 1
driver arm 1 fixed 1 floating 2 mutable 5 index-id 0
driver arm 1 fixed 2 floating 0 mutable 5 index-id 2

EOF

cat >55or70_0to3_3to8_index/35+35_0to3_3to8_nomutate.config <<EOF
#
# Dnb archtecture (simplified to exclude gap values we are sacrificing):
# 5 (-2,0) 10 (0,2) 10 (4,7) 10  -mate-gap-  10 (4,7) 10 (0,2) 10 (-2,0) 5
# Reference size: full human (18 bases in the key)
#
version 1
index index.1 spec 0,1,2,3,4,5,6,7,8,9;13;0,1,2,3,4
index index.2 spec 0,1,2,3,4,5,6,7,8,9;18;0,1,2,3,4
index index.3 spec 0,1,2,3,4,5,6,7,8,9;-23;0,1,2,3,4
driver arm 0 fixed 1 floating 2 mutable 0 index-id 0
driver arm 0 fixed 2 floating 3 mutable 0 index-id 1
driver arm 0 fixed 3 floating 1 mutable 0 index-id 2
driver arm 1 fixed 0 floating 1 mutable 0 index-id 1
driver arm 1 fixed 1 floating 2 mutable 0 index-id 0
driver arm 1 fixed 2 floating 0 mutable 0 index-id 2

EOF

else

# Use large-reference index configs.
echo Using large index configs, gbi file size: $(ls -l reference.gbi | awk '{print $5}').

cat >55or70_0to2_4to7_index/35+35_0to2_4to7.config <<EOF
#
# Dnb archtecture (simplified to exclude gap values we are sacrificing):
# 5 (-2,0) 10 (0,2) 10 (4,7) 10  -mate-gap-  10 (4,7) 10 (0,2) 10 (-2,0) 5
# Reference size: full human (18 bases in the key)
#
version 1
index index.1 spec 0,1,2,3,4,5,6,7,8,9;12;0,1,2,3,4,5,6,7
index index.2 spec 0,1,2,3,4,5,6,7,8,9;17;0,1,2,3,4,5,6,7
index index.3 spec 0,1,2,3,4,5,6,7,8,9;-24;0,1,2,3,4,5,6,7
driver arm 0 fixed 1 floating 2 mutable 8 index-id 0
driver arm 0 fixed 2 floating 3 mutable 8 index-id 1
driver arm 0 fixed 3 floating 1 mutable 8 index-id 2
driver arm 1 fixed 0 floating 1 mutable 8 index-id 1
driver arm 1 fixed 1 floating 2 mutable 8 index-id 0
driver arm 1 fixed 2 floating 0 mutable 8 index-id 2

EOF

cat >55or70_0to2_4to7_index/35+35_0to2_4to7_nomutate.config <<EOF
#
# Dnb archtecture (simplified to exclude gap values we are sacrificing):
# 5 (-2,0) 10 (0,2) 10 (4,7) 10  -mate-gap-  10 (4,7) 10 (0,2) 10 (-2,0) 5
# Reference size: full human (18 bases in the key)
#
version 1
index index.1 spec 0,1,2,3,4,5,6,7,8,9;12;0,1,2,3,4,5,6,7
index index.2 spec 0,1,2,3,4,5,6,7,8,9;17;0,1,2,3,4,5,6,7
index index.3 spec 0,1,2,3,4,5,6,7,8,9;-24;0,1,2,3,4,5,6,7
driver arm 0 fixed 1 floating 2 mutable 0 index-id 0
driver arm 0 fixed 2 floating 3 mutable 0 index-id 1
driver arm 0 fixed 3 floating 1 mutable 0 index-id 2
driver arm 1 fixed 0 floating 1 mutable 0 index-id 1
driver arm 1 fixed 1 floating 2 mutable 0 index-id 0
driver arm 1 fixed 2 floating 0 mutable 0 index-id 2

EOF

cat >55or70_0to3_3to8_index/35+35_0to3_3to8.config <<EOF
#
# Dnb archtecture (simplified to exclude gap values we are sacrificing):
# 5 (-4,0) 10 (0,3) 10 (3,8) 10  -mate-gap-  10 (8,8) 10 (0,3) 10 (-4,0) 5
# Reference size: full human (18 bases in the key)
#
version 1
index index.1 spec 0,1,2,3,4,5,6,7,8,9;13;0,1,2,3,4,5,6,7
index index.2 spec 0,1,2,3,4,5,6,7,8,9;18;0,1,2,3,4,5,6,7
index index.3 spec 0,1,2,3,4,5,6,7,8,9;-23;0,1,2,3,4,5,6,7
driver arm 0 fixed 1 floating 2 mutable 8 index-id 0
driver arm 0 fixed 2 floating 3 mutable 8 index-id 1
driver arm 0 fixed 3 floating 1 mutable 8 index-id 2
driver arm 1 fixed 0 floating 1 mutable 8 index-id 1
driver arm 1 fixed 1 floating 2 mutable 8 index-id 0
driver arm 1 fixed 2 floating 0 mutable 8 index-id 2

EOF

cat >55or70_0to3_3to8_index/35+35_0to3_3to8_nomutate.config <<EOF
#
# Dnb archtecture (simplified to exclude gap values we are sacrificing):
# 5 (-4,0) 10 (0,3) 10 (3,8) 10  -mate-gap-  10 (8,8) 10 (0,3) 10 (-4,0) 5
# Reference size: full human (18 bases in the key)
#
version 1
index index.1 spec 0,1,2,3,4,5,6,7,8,9;13;0,1,2,3,4,5,6,7
index index.2 spec 0,1,2,3,4,5,6,7,8,9;18;0,1,2,3,4,5,6,7
index index.3 spec 0,1,2,3,4,5,6,7,8,9;-23;0,1,2,3,4,5,6,7
driver arm 0 fixed 1 floating 2 mutable 0 index-id 0
driver arm 0 fixed 2 floating 3 mutable 0 index-id 1
driver arm 0 fixed 3 floating 1 mutable 0 index-id 2
driver arm 1 fixed 0 floating 1 mutable 0 index-id 1
driver arm 1 fixed 1 floating 2 mutable 0 index-id 0
driver arm 1 fixed 2 floating 0 mutable 0 index-id 2

EOF

fi

driver.py -f $CGI_HOME/etc/workflows/human/createAnnotationIndex -m local -p in.reference-id=$(basename $(pwd)) -p in.object-root-dir=$(dirname $(dirname $(pwd)))

TeraMap --build-index --reference reference.gbi --index-dir 55or70_0to2_4to7_index --index-config 55or70_0to2_4to7_index/35+35_0to2_4to7.config
TeraMap --build-index --reference reference.gbi --index-dir 55or70_0to3_3to8_index --index-config 55or70_0to3_3to8_index/35+35_0to3_3to8.config
