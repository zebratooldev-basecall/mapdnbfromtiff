#!/usr/bin/python
import os,sys
import optparse
from driverlib import Error

def do_CNV_QC(dataFile,statsFile):
    data = open(dataFile,'r')
    statsOut = open(statsFile,'w')
    inSegData=0
    numCNVs=0
    ttlCNVlen=0
    numKnownCNVs=0
    ttlKnownCNVlen=0
    knownCol=-1
    for l in data:
        l.strip()
        w=l.split()
        if(len(w)==0):
            continue
        if inSegData:
            if w[6] != "=" and w[5] != "N":

               numCNVs += 1
               ttlCNVlen += (int(w[2])-int(w[1]))

               ts=l.split("\t")
               if len(ts) <= knownCol:
                   raise Error("Trouble looking for field index %d in line\n%s\n" % ( knownCol,l) )
               
               if len(ts[knownCol]) > 0:
                   numKnownCNVs += 1
                   ttlKnownCNVlen += (int(w[2])-int(w[1]))
                   
        if w[0] == ">chr":

            inSegData=1

            if "knownCNV" not in w:
                raise Error("Segments file %s must include a knownCNV column (annotation results required)" % dataFile)
            else:
                try:
                    knownCol = w.index("knownCNV")
                except ValueError:
                    raise Error("Bug in code for finding knownCNV field")
                                                                      

    print >> statsOut, "freeform,CNV_QC_stats"
    print >> statsOut, "number of called CNVs," + str(numCNVs)
    print >> statsOut, "total length of called CNVs," + str(ttlCNVlen)
    if numCNVs > 0:
        print >> statsOut, "fraction of called CNVs similar to known CNVs,%.4f" % (float(numKnownCNVs)/float(numCNVs))
    else:
        print >> statsOut, "fraction of called CNVs similar to known CNVs,1"
    if ttlCNVlen > 0:
        print >> statsOut, "fraction of called CNV bases that are in calls similar to known CNVs,%.4f" % (float(ttlKnownCNVlen)/float(ttlCNVlen))
    else:
        print >> statsOut, "fraction of called CNV bases that are in calls similar to known CNVs,1"
    data.close()
    statsOut.close()

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(dataFile='', statsFile='')
    parser.add_option('-i', '--input-cnv-calls', dest='dataFile',
                      help='CNV segments file on which to compute CNV QC stats')
    parser.add_option('-o', '--output-stats-file', dest='statsFile',
                      help='name of file to contain computed CNV QC statistics')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.dataFile:
        parser.error('input-cnv-calls specification required')

    if not options.statsFile:
        parser.error('output-stats-file specification required')

    do_CNV_QC(options.dataFile,options.statsFile)

if __name__ == '__main__':
    main()
