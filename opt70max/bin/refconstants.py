#! /usr/bin/env python
import os
from os.path import join as pjoin

class RefConstantsBase(object):
    def setDefault(self, kwargs, key, value):
        if key in kwargs:
            self.__dict__[key] = kwargs[key]
        else:
            self.__dict__[key] = value

    def __str__(self):
        keys = self.__dict__.keys()
        keys.sort()
        lines = []
        for key in keys:
            lines.append("%s=%s" % (key,self.__dict__[key]))
        return "\n".join(lines)

class RefConstants(RefConstantsBase):
    def __init__(self, **kwargs):
        # Set by GlobalRefConstants.__getitem__
        self.referenceRootDir = None
        # The default value for the ref id.
        self.setDefault(kwargs, "refId", None)

        # The default value for the Auto-QC id.
        self.setDefault(kwargs, "aqcId", None)

        # Number of phase 3 jobs per assembly interval.
        self.setDefault(kwargs, "phase3BatchCount", 100)

        # Number of phase 6 jobs per assembly interval.
        self.setDefault(kwargs, "phase6BatchCount", 100)

        # List of (chr,begin,end,caption) representing regions of the
        # genome to gather coverage detail during assembly.
        self.setDefault(kwargs, "cvgDetailRegions", [])

        # List of reference contigs with ploidy=1.
        self.setDefault(kwargs, "haploidContigs", [])

        # List of reference contigs to be treated like mitochondrion
        # (very high coverage).
        self.setDefault(kwargs, "mitoContigs", [])

        # List of assembly intervals (right-open intervals). We split
        # the assembly by intervals for manageability.
        self.setDefault(kwargs, "intervals", [ (0,1) ])

        # List of reference regions to ignore for CNV.
        # Use is to exclude portions of chrX in females that were chopped out in males to allow different ploidy across PARs
        self.setDefault(kwargs, "cnvIgnoreRegions", {})

        # Indicates whether a reference type has standard baseline normalization data
        # i.e. cvgNormalizer*csv files in <ref dir>/CovModel/
        self.setDefault(kwargs, "hasBaseline", 'False')

        # Indicates whether this is a test assembly on which certain shortcuts may be taken.
        # E.g. DNB-level coverage bias modeling should be skipped for this reference
        # (i.e. cm step in impGaps should be done) and whether gc-bias-model is omitted in CNV smoothcov steps.
        self.setDefault(kwargs, "isSmallTest", True)

        # Parameters for de novo assembly.
        self.setDefault(kwargs, "deNovoServerCount", 8)
        self.setDefault(kwargs, "deNovoServerMappingDensity", 10)
        self.setDefault(kwargs, "deNovoClientMappingDensity", 80)

        self.setDefault(kwargs, "dbSnpBuild", "dbSNP build 000")
        self.setDefault(kwargs, "dbSnpBaselineBuild", "dbSNP build 000")
        self.setDefault(kwargs, "geneAnnotationsBuild", "NCBI build 00.0")
        self.setDefault(kwargs, "referenceBuild", "NCBI build 00")

        self.setDefault(kwargs, "simGenomeInVariations", None)

        self.setDefault(kwargs, "parRegions", "ChromosomeNotCalled,BeginNotCalled,EndNotCalled,ChromosomeDiploid,BeginDiploid,EndDiploid")
        self.setDefault(kwargs, "sexdetRules", None)

    def getChromosomeNames(self):
        from gbifile import GbiFile
        gbi = GbiFile(pjoin(self.referenceRootDir, self.refId, 'reference.gbi'))
        return gbi.listChromosomes()

    def getMitoChromosomeNames(self):
        from gbifile import GbiFile
        gbi = GbiFile(pjoin(self.referenceRootDir, self.refId, 'reference.gbi'))
        result = []
        for contig in self.mitoContigs:
            chrom = gbi.contigs[contig].supername
            if chrom not in result:
                result.append(chrom)
        return result


class RefConstantsHumanBased(RefConstants):
    def __init__(self, **kwargs):
        RefConstants.__init__(self, **kwargs)

        self.setDefault(kwargs, "phase3BatchCount", 100)
        self.setDefault(kwargs, "phase6BatchCount", 100)

        self.setDefault(kwargs, "dbSnpBuild", "dbSNP build 130")
        self.setDefault(kwargs, "dbSnpBaselineBuild", "dbSNP build 129")
        self.setDefault(kwargs, "geneAnnotationsBuild", "NCBI build 36.3")
        self.setDefault(kwargs, "referenceBuild", "NCBI build 36")

# Regions to gather coverage detail for human assemblies.
cvgDetailRegionsHuman = [ ("chr12",38991138,39141490,  "new BAC"),
                          ("chr9",130778837,130948194, "old BAC"),
                          ("chr7",62000000,63000000,   "widely variant cov"),
                          ("chr21",25000000,26000000,  "variant cov"),
                          ("chr12",112000000,112001000,"variant cov"),
                          ("chr1",30004000,30005000,   "fairly constant high cov"),
                          ]

class RefConstantsFullRice(RefConstants):
    def __init__(self, **kwargs):
        RefConstants.__init__(self, **kwargs)
        self.setDefault(kwargs, "hasBaseline", 'False')
        self.setDefault(kwargs, "isSmallTest", False)

        self.setDefault(kwargs, "phase3BatchCount", 300)
        self.setDefault(kwargs, "phase6BatchCount", 300)

        self.setDefault(kwargs, "cvgDetailRegions", cvgDetailRegionsHuman)

class RefConstantsFullSoybean(RefConstants):
    def __init__(self, **kwargs):
        RefConstants.__init__(self, **kwargs)
        self.setDefault(kwargs, "hasBaseline", 'False')
        self.setDefault(kwargs, "isSmallTest", False)

        self.setDefault(kwargs, "phase3BatchCount", 300)
        self.setDefault(kwargs, "phase6BatchCount", 300)

        self.setDefault(kwargs, "cvgDetailRegions", cvgDetailRegionsHuman)

class RefConstantsFullBiome(RefConstants):
    def __init__(self, **kwargs):
        RefConstants.__init__(self, **kwargs)
        self.setDefault(kwargs, "hasBaseline", 'False')
        self.setDefault(kwargs, "isSmallTest", False)

        self.setDefault(kwargs, "phase3BatchCount", 300)
        self.setDefault(kwargs, "phase6BatchCount", 300)

        self.setDefault(kwargs, "cvgDetailRegions", cvgDetailRegionsHuman)


class RefConstantsFullHuman(RefConstantsHumanBased):
    def __init__(self, **kwargs):
        RefConstantsHumanBased.__init__(self, **kwargs)
        self.setDefault(kwargs, "hasBaseline", 'True')
        self.setDefault(kwargs, "isSmallTest", False)

        self.setDefault(kwargs, "phase3BatchCount", 300)
        self.setDefault(kwargs, "phase6BatchCount", 300)

        self.setDefault(kwargs, "cvgDetailRegions", cvgDetailRegionsHuman)

        self.setDefault(kwargs, "sexdetRules", "274,0.005,0.05\n"\
                                               "279,0.005,0.05")


class RefConstantsHuman37Based(RefConstants):
    def __init__(self, **kwargs):
        RefConstants.__init__(self, **kwargs)

        self.setDefault(kwargs, "phase3BatchCount", 100)
        self.setDefault(kwargs, "phase6BatchCount", 100)

        self.setDefault(kwargs, "dbSnpBuild", "dbSNP build 137")
        self.setDefault(kwargs, "dbSnpBaselineBuild", "dbSNP build 129")
        self.setDefault(kwargs, "geneAnnotationsBuild", "NCBI Annotation Release 104")
        self.setDefault(kwargs, "referenceBuild", "NCBI build 37")

# Regions to gather coverage detail for human assemblies.
cvgDetailRegionsHuman37 = [("chr12",40704871,40855223,"new BAC"),
                           ("chr9",131739016,131908373,"old BAC"),
                           ("chr7",62362565,63362565,"widely variant cov"),
                           ("chr21",26078129,27078129,"variant cov"),
                           ("chr12",113515617,113516617,"variant cov"),
                           ("chr1",30231413,30232413,"fairly constant high cov"),
                          ]

class RefConstantsFullHuman37(RefConstantsHuman37Based):
    def __init__(self, **kwargs):
        RefConstantsHuman37Based.__init__(self, **kwargs)
        self.setDefault(kwargs, "hasBaseline", 'True')
        self.setDefault(kwargs, "isSmallTest", False)

        self.setDefault(kwargs, "phase3BatchCount", 300)
        self.setDefault(kwargs, "phase6BatchCount", 300)

        self.setDefault(kwargs, "cvgDetailRegions", cvgDetailRegionsHuman37)

        self.setDefault(kwargs, "sexdetRules", "267,0.005,0.05\n"\
                                               "272,0.005,0.05")

def splitsToIntervals(splits):
    """Input is list of contigs in contig order [a, b, c, d, ..., z],
    where a is the first contig for assembly, z is one past the end of
    the last assembly contig, and the other contigs are where to split
    the assembly up into intervals. Output is the list of
    intervals."""
    return [ (prev,split) for (prev,split) in zip(splits[:-1], splits[1:]) ]


_defaultRefConstants = {}

#
# soybean gmax 189
#

_defaultRefConstants["soybean"] = RefConstantsFullRice(
    refId            = "SOYBEAN_01",
    aqcId            = "SOYBEAN_Gmax_189",
    haploidContigs   = [ ],
    mitoContigs      = [0]
    )

# rice IRGSP Build5
#
_defaultRefConstants["rice"] = RefConstantsFullRice(
    refId            = "RICE_01",
    aqcId            = "RICE_IRGSP_Build5",
    haploidContigs   = [ ],
    mitoContigs      = [0]
    )

# Biome, reference obtained from BGI (511.bacteria.genomes.1k.gbi)
#
_defaultRefConstants["biome"] = RefConstantsFullRice(
    refId            = "BIOME_01",
    aqcId            = "BIOME_01",
    haploidContigs   = [ ],
    mitoContigs      = [0]
    )

#
# female37 - Human female build 37.
#

_defaultRefConstants["female37"] = RefConstantsFullHuman37(
    refId            = "HUMAN37-F_11-REF",
    aqcId            = "HUMAN37-F",
    haploidContigs   = [ ],
    mitoContigs      = [ 265 ],
    intervals        = splitsToIntervals([0, 38, 57, 66, 75, 83, 107, 150, 173,
                                          185, 201, 217, 253, 265, 266]),
    cnvIgnoreRegions = { "249":("2699520","2699570"),
                         "264":("154930994","154931044") },
    parRegions       = "ChromosomeNotCalled,BeginNotCalled,EndNotCalled,ChromosomeDiploid,BeginDiploid,EndDiploid\n" \
                       ",,,chrX,60000,2699520\n" \
                       ",,,chrX,154931044,155260560"
    )

#
# female - Human female.
#

_defaultRefConstants["female"] = RefConstantsFullHuman(
    refId            = "HUMAN-F_16-REF",
    aqcId            = "HUMAN-F",
    haploidContigs   = [ ],
    mitoContigs      = [ 272 ],
    intervals        = splitsToIntervals([0, 39, 60, 70, 81, 91, 112, 156, 182,
                                          194, 212, 230, 263, 272, 273]),
    cnvIgnoreRegions = { "261":("2709520","2709570"),
                         "271":("154584187","154584237") },
    parRegions       = "ChromosomeNotCalled,BeginNotCalled,EndNotCalled,ChromosomeDiploid,BeginDiploid,EndDiploid\n" \
                       ",,,chrX,0,2709520\n" \
                       ",,,chrX,154584237,154913754"
    )

#
# male37 - Human male build 37.
#

_defaultRefConstants["male37"] = RefConstantsFullHuman37(
    refId            = "HUMAN37-M_11-REF",
    aqcId            = "HUMAN37-M",
    haploidContigs   = range(250,266) + range(267, 278),
    mitoContigs      = [ 278 ],
    intervals        = splitsToIntervals([0, 38, 57, 66, 75, 83, 107, 150, 173,
                                          185, 201, 217, 250, 266, 267, 278, 279]),
    parRegions       = "ChromosomeNotCalled,BeginNotCalled,EndNotCalled,ChromosomeDiploid,BeginDiploid,EndDiploid\n" \
                       "chrY,10000,2649520,chrX,60000,2699520\n" \
                       "chrY,59034049,59363566,chrX,154931043,155260560"
    )

#
# male - Human male.
#

_defaultRefConstants["male"] = RefConstantsFullHuman(
    refId            = "HUMAN-M_14-REF",
    aqcId            = "HUMAN-M",
    haploidContigs   = range(262, 273) + range(274, 283),
    mitoContigs      = [ 283 ],
    intervals        = splitsToIntervals([0, 39, 60, 70, 81, 91, 112, 156, 182,
                                          194, 212, 230, 262, 273, 274, 283, 284]),
    parRegions       = "ChromosomeNotCalled,BeginNotCalled,EndNotCalled,ChromosomeDiploid,BeginDiploid,EndDiploid\n" \
                       "chrY,0,2709520,chrX,0,2709520\n" \
                       "chrY,57443437,57772954,chrX,154584237,154913754"
    )


#
# chr21 - Chromosome 21
#

_defaultRefConstants["chr21"] = RefConstantsHumanBased(
    refId                       = "TEST-CHR21_02-REF",
    intervals                   = splitsToIntervals([0, 2, 4, 6]),
    deNovoServerCount           = 1,
    deNovoServerMappingDensity  = 80,
    deNovoCleintMappingDensity  = 80
    )


#
# smoketest1 - New small test case for nightly build; combines segdups with circular haploid chrM
#    Intent is to use it with simtool.py
#

_defaultRefConstants["smoketest1"] = RefConstants(
    refId                       = "TEST-SMOKETEST1_06-REF",
    phase3BatchCount            = 32,
    phase6BatchCount            = 32,
    deNovoServerCount           = 1,
    deNovoServerMappingDensity  = 80,
    deNovoCleintMappingDensity  = 80,
    cvgDetailRegions            = [ ("OneMBpTestWithSegDups", 240000, 340000, "transition into seg dup region"),
                                    ("twoContigSuperContig", 38780, 68780, "random region from second contig of split supercontig"),
                                    ("chrM", 0, 5000, "beginning of chrM") 
                                  ],
    haploidContigs              = [ 3 ],
    mitoContigs                 = [ 3 ],
    intervals                   = splitsToIntervals([0, 3, 4]),
    cnvIgnoreRegions            = { "0" : ("100000","100050") },
    hasBaseline                 = 'True'
    )

#
# chr2_10m_15m - 5 Mb test case taken from chr2 starting at offset 10,000,000.
#                Has a few small segmental dups that test ccf, but not too much.
#                No tricky stuff like circular contigs, multiple contigs, etc.
#

_defaultRefConstants["chr2_10m_15m"] = RefConstantsHumanBased(
    refId                       = "TEST-CHR2-10M-15M_04-REF",
    simGenomeInVariations       = "sim_Variations20-40_ccf100_chr2_5meg.tsv",
    deNovoServerCount           = 1,
    deNovoServerMappingDensity  = 80,
    deNovoCleintMappingDensity  = 80
    )

#
# chr2_10m_11m - 1 Mb test case taken from chr2 starting at offset 10,000,000.
#

_defaultRefConstants["chr2_10m_11m"] = RefConstantsHumanBased(
    refId                       = "TEST-CHR2-10M-11M_02-REF",
    simGenomeInVariations       = "sim_Variations20-40_ccf100_chr2_1meg.tsv",
    deNovoServerCount           = 1,
    deNovoServerMappingDensity  = 80,
    deNovoCleintMappingDensity  = 80
    )

#
# chr2_10m_10.1m - 100 Kb test case taken from chr2 starting at offset 10,000,000.
#

_defaultRefConstants["chr2_10m_10_1m"] = RefConstantsHumanBased(
    refId                       = "TEST-CHR2-10M-10_1M_02-REF",
    phase3BatchCount            = 10,
    phase6BatchCount            = 10,
    deNovoServerCount           = 1,
    deNovoServerMappingDensity  = 80,
    deNovoCleintMappingDensity  = 80,
    simGenomeInVariations       = "sim_Variations20-40_ccf100_chr2_100kb.tsv"
    )

_defaultRefConstants["chr2_10m_alu"] = RefConstantsHumanBased(
    refId                       = "TEST-CHR2-10M-ALU_01-REF",
    phase3BatchCount            = 10,
    phase6BatchCount            = 10,
    deNovoServerCount           = 1,
    deNovoServerMappingDensity  = 80,
    deNovoCleintMappingDensity  = 80,
    simGenomeInVariations       = "aluvar.tsv"
    )

class GlobalRefConstants(object):
    '''Dictionary, reference id -> RefConstants object.'''

    def __init__(self, referenceRootDir):
        self.referenceRootDir = referenceRootDir
        self.cache = {}


    def __getitem__(self, sampleType):
        if not self.cache.has_key(sampleType):
            self.cache[sampleType] = self.loadConstants(sampleType)
            self.cache[sampleType].referenceRootDir = self.referenceRootDir
        return self.cache[sampleType]


    def loadConstants(self, sampleType):
        if os.path.exists(self.rcpath(sampleType)):
            return self.loadFromOverrides(sampleType)

        if not _defaultRefConstants.has_key(sampleType):
            raise Exception( 'unknown sample type: %s, referenceRootDir=%s' %
                             (sampleType, self.referenceRootDir) )

        refId = _defaultRefConstants[sampleType].refId
        if os.path.exists(self.rcpath(refId)):
            return self.loadFromOverrides(refId)

        return _defaultRefConstants[sampleType]


    def rcpath(self, refId):
        return pjoin(self.referenceRootDir, refId, 'rcoverrides.py')


    def loadFromOverrides(self, refId):
        fn = self.rcpath(refId)
        globals = {}
        execfile(fn, globals)
        refConstants = globals['refConstants']

        actualRefId = refConstants.refId
        if refId != actualRefId:
            raise Exception( 'refId mismatch in %s: expected=%s, found=%s' %
                             (fn, refId, actualRefId) )
        return refConstants
