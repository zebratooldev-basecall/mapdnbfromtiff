#! /usr/bin/env python

import sys, os
from bisect import bisect
from optparse import OptionParser
from os.path import split as psplit, join as pjoin
from glob import glob
import gzip


CVG_NOCALL  = 0
CVG_PARTIAL = 1
CVG_CALLED  = 2
CVG_CAT_COUNT = 3

cvgTypeMsg = { CVG_NOCALL:  "nocall",
               CVG_PARTIAL: "partial",
               CVG_CALLED:  "called",
               }


class CvgTypeMap:
    def __init__(self, callRangeDir, callFile):
        self.callRangeDir = callRangeDir
        self.callFile = callFile
        self.contig = -1
        self.regions = []

    def getCvgType(self, contig, pos):
        if self.contig != contig:
            self.loadContig(contig)
        idx = bisect( self.regions, (pos,pos,0) )
        if idx > 0 and pos < self.regions[idx-1][1]:
            return self.regions[idx-1][2]
        if idx < len(self.regions) and self.regions[idx][0] <= pos:
            return self.regions[idx][2]
        return CVG_NOCALL

    def loadContig(self, contig):
        regions = []
        fn = pjoin(self.callRangeDir, str(contig), self.callFile)
        ff = open(fn)
        try:
            ff.readline() # header
            for line in ff:
                (contig,begin,end,calledAlleles,ploidy) = map(int, line.split(","))
                cvgType = self.getCvgTypeGivenCallCount(calledAlleles,ploidy)
                regions.append( (begin,end,cvgType) )
            self.contig = contig
            self.regions = regions
        finally:
            ff.close()

    def getCvgTypeGivenCallCount(self, calledAlleles, ploidy):
        if 0 == calledAlleles:
            return CVG_NOCALL
        if calledAlleles == ploidy:
            return CVG_CALLED
        return CVG_PARTIAL


def incCounter(counter, cvg):
    if cvg not in counter:
        counter[cvg] = 0
    counter[cvg] += 1


def main():
    parser = OptionParser('usage: %prog [options]')
    parser.disable_interspersed_args()
    parser.add_option('-o', '--output', default=None,
                      help='Path to output summary file.')
    parser.add_option('--callrange-dir', default=None,
                      help='Path to directory containing callrange info (produced by callrpt).')
    parser.add_option('--input-detail', default=None,
                      help='Path to input CoverageDetail report (produced by covrpt).')
    parser.add_option('-f', '--fields', default=None,
                      help='Which coverage counters to use (default is all counters).')
    parser.add_option('-c', '--call-files',
                      default=('Variations20-40_ccf100.csv,'+
                               'Variations20-100_ccf100.csv'),
                      help='Which call files to use ("all" for call files in callrange-dir).')

    (options, args) = parser.parse_args()

    if options.output is None:
        raise Exception("output param is required")
    if options.callrange_dir is None:
        raise Exception("callrange-dir param is required")
    if options.input_detail is None:
        raise Exception("input-detail param is required")

    if options.input_detail.endswith(".gz"):
        detail = os.popen("gunzip -c '%s'" % options.input_detail)
    else:
        detail = open(options.input_detail)
    header = detail.readline()
    if not header.startswith("Contig,Position,Base,"):
        raise Exception("unrecognized detail header: " + header)
    headerFields = header.strip().split(",")
    inputCounters = headerFields[3:]

    counterHeaders = []
    if options.fields is None:
        counterHeaders = inputCounters
    else:
        counterHeaders = options.fields.split(",")

    counterMap = [ -1 ] * len(counterHeaders)
    for ii in xrange(len(counterMap)):
        if not counterHeaders[ii] in inputCounters:
            raise Exception("input-detail file does not have field: "+counterHeaders[ii])
        counterMap[ii] = inputCounters.index(counterHeaders[ii])

    callFiles = []
    if options.call_files == "all":
        callFiles = [ psplit(callFile)[1] for callFile in
                      glob(pjoin(options.callrange_dir,"0","Variations*.csv")) ]
    else:
        callFiles = options.call_files.split(",")
    cvgTypeMap = [ CvgTypeMap(options.callrange_dir, callFile) for callFile in callFiles ]

    counters = []
    for counterHeader in counterHeaders:
        counters.append([])
        for callFile in callFiles:
            counters[-1].append([])
            for ii in xrange(CVG_CAT_COUNT):
                counters[-1][-1].append({})

    for line in detail:
        fields = line.strip().split(",")
        contig = int(fields[0])
        pos = int(fields[1])
        cvga = map(int, fields[3:])
        for ii in xrange(len(callFiles)):
            cvgType = cvgTypeMap[ii].getCvgType(contig,pos)
            for jj in xrange(len(counterHeaders)):
                cc = counters[jj]
                cvg = cvga[counterMap[jj]]
                incCounter(cc[ii][cvgType], cvg)

    maxCvg = -1
    for cc in counters:
        for dd in cc:
            for ee in dd:
                for cvg in ee:
                    maxCvg = max(maxCvg, cvg)

    outHeaderFields = [ "Coverage" ]
    for counterHeader in counterHeaders:
        for callFile in callFiles:
            for cvgType in xrange(CVG_CAT_COUNT):
                outHeaderFields.append( "%s %s %s" % (callFile,cvgTypeMsg[cvgType],counterHeader) )
    out = open(options.output, "w")
    out.write( ",".join(outHeaderFields) + "\n" )
    for cvg in xrange(maxCvg+1):
        counts = []
        for cc in counters:
            for dd in cc:
                for ee in dd:
                    count = 0
                    if cvg in ee:
                        count = ee[cvg]
                    counts.append(count)
        if sum(counts) > 0:
            out.write( "%d,%s\n" % ( cvg, ",".join( map(str,counts) ) ) )



if __name__ == '__main__':
    main()
