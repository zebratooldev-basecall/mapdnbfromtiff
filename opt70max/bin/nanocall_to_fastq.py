#!/usr/bin/env python

import os
import sys
import logging
from score_converter import *
from optparse import OptionParser

def setup_logger():
    global logger

    # create logger
    logger = logging.getLogger('nanocall-to-fastq_logger')
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

def expand_integer_list(range_str):
    result = []

    # split by comma first.
    for tok in range_str.split(','):
    
        # check if tok contains '-' this indicates a range.
        if tok.find('-') != -1:
    
            # found a '-' to use it to split token.
            sub_tokens = tok.split('-')

            # we should find two integers.
            if len(sub_tokens) != 2:
                logger.error('Hyphen can only concatenate two items!')
                logger.error('%d items found in %s from the original argument %s' % (len(sub_tokens), tok, range_str))
                sys.exit(-1)
            for item in sub_tokens:
                if not item.isdigit():
                    logger.error('ERROR: Non integer character %s found in columns sring argument %s' % (item, range_str))
                    sys.exit(-1)

            # add all values in range to collection.
            for i in range(int(sub_tokens[0]),int(sub_tokens[1])+1):
                result.append(i)
        else:
            if not tok.isdigit():
                logger.error('ERROR: Non integer character %s found in columns sring argument %s' % (tok, range_str))
                sys.exit(-1)

            # add integer.
            result.append(int(tok))

    # return results.
    return result

def load_calls_and_scores(fn):
    '''load base calls and scores from an individual nanocall csv file'''

    # log filename
    logger.info('Loading base calls and scores from %s' % fn)

    # load basecalls and scores.
    results = []

    # open file
    sr = open(fn, 'r')
    tok = None

    # skip header portion of the nanocall summary file.
    while tok != 'Index':
        fields = sr.readline().rstrip('\r\n').split(',')
        if len(fields) > 1:
            tok = fields[0]
        else:
            continue

    # loop through remaining lines.
    for line in sr:
        fields = line.rstrip('\r\n').split(',')
        call, score = fields[1], fields[2]
        results.append((call, float(score)))
    sr.close()

    # Return results.
    return results    

def load_nanocall_files(input_dir, slide_barcode, lane, field, cycle_list):
    '''load a set of nanocall files and index by cycle'''

    logger.info('Loading base calls and scores for a collection of cycles')
    results = {}

    # format variables.
    slide_mask = slide_barcode[:-7] + 'xxx-FS3'
    lane_name  = 'L%02d' % lane

    # loop through cycles.
    i = 0; calls_length = 0
    for cycle in cycle_list:
        # create file path to nanocall file.
        cycle_name = 'C%02d' % cycle
        input_fn = os.path.join(input_dir, slide_mask, slide_barcode, cycle_name, lane_name, '%s.%s.%s%s_NanoCall.csv' % (slide_barcode, cycle_name, lane_name, field))

        # load.
        calls = load_calls_and_scores(input_fn)

        # confirm calls list are all of the same length.
        if i == 0:
            calls_length = len(calls)
        else:
            if calls_length != len(calls):
                logger.error('ERROR! Found nanocall output with different lengths (%d, %d)' % (calls_length, len(calls)))
                sys.exit(-1)
        logger.info('%d calls and scores written in from %s' % (len(calls), input_fn))
        i += 1

        # add results to collection.
        results[cycle] = calls

    # return basecalls, scores, and the field length.
    return results, calls_length

def print_fastq_file(output_fn, slide_barcode, lane, field_name, cycle_list, basecalls, calls_length):
    '''print out reads and scores to a file in fastq format'''
    logger.info('writing reads and scores out to a file in fastq format')

    # open stream.
    sw = open(output_fn, 'w')

    # loop through dnbs.
    for i in xrange(calls_length):
        # header
        sw.write('@%s-L%02d.%s.%07d\n' % (slide_barcode, lane, field_name, i+1))

        # read.
        sw.write(''.join([ basecalls[cycle][i][0] for cycle in cycle_list ]) + '\n')

        # description
        sw.write('+\n')

        # scores
        scores = []
        # loop through scores.
        for cycle in cycle_list:
            logger.debug('continuous base call score %f for dnb %d' % (basecalls[cycle][i][1], i+1))
            # convert from continuous score to discretized value.
            d_score = discretize_score(basecalls[cycle][i][1])
            logger.debug('continuous score %f converted to discretized score %f' % (basecalls[cycle][i][1], d_score[1]))

            # get phred scaled base 33 score.
            p_score = phred33score(d_score[0])
            logger.debug('adf score %d converted phred 33 socre %d' % (d_score[0], p_score))

            # add to collection.
            scores.append(p_score)

        # write ascii charcter for scores to file.
        sw.write(''.join([ chr(score) for score in scores ]) + '\n')

        # update progress.
        if i % 100000 == 0:
            logger.info('written %d reads and scores out to fastq' % i)

    # close stream
    sw.close()


def parse_arguments(args):
    '''parse command line arguments'''
    logger.info('Parsing command line arguments')

    # setup parser object.
    parser = OptionParser()

    parser.add_option('-i', '--source-directory', help='The source directory for the nanocall csv files', type='string', default=None, dest='input_dir')
    parser.add_option('-d', '--output-filename', help='The parent directory that contains slides', type='string', default=None, dest='output_fn')
    parser.add_option('-s', '--slide-barcode', help='The slide barcode', type='string', default=None, dest='slide_barcode')
    parser.add_option('-l', '--lane', help='The lane number', type='int', default=None, dest='lane')
    parser.add_option('-f', '--field-name', help='The name of the fields to process', type='string', default=None, dest='field_name')
    parser.add_option('-c', '--cycle-list', help='Comma delimited list and/or range that specifies cycles and their order', type='string', default=None, dest='cycle_list')

    options, args = parser.parse_args(args)

    if options.input_dir == None or options.output_fn == None or options.slide_barcode == None or options.lane == None or options.field_name == None or options.cycle_list == None:
        logger.error('ERROR! Missing required command line arguments')
        parser.print_help()
        sys.exit(-1)

    # check to see if lane is an integer.
    #if not options.lane.isdigit():
    #    print('ERROR! Lane value (%s) must be an integer')
    #    sys.exit(-1)

    return options.input_dir, options.output_fn, options.slide_barcode, options.lane, options.field_name, options.cycle_list

def main(arguments):
    # setup logger.
    setup_logger()

    # parse command line arguments.
    input_dir, output_fn, slide_barcode, lane, field_name, cycle_list_str = parse_arguments(arguments)

    # expand cycle list.
    cycle_list = expand_integer_list(cycle_list_str)

    # load nanocall csvs.
    basecalls, calls_length = load_nanocall_files(input_dir, slide_barcode, lane, field_name, cycle_list)

    # print fastq file.
    print_fastq_file(output_fn, slide_barcode, lane, field_name, cycle_list, basecalls, calls_length)

if __name__ == '__main__':
    main(sys.argv)
