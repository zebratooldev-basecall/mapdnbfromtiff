#!/bin/tcsh -ef

set refgbi=$1 
set basedir=$2
set cutoff=$3
set ccfdelta=$4
set vsttwo=$5
set output=$6

set callbases=`grep -v no-call variations/ccf/Variations${cutoff}-${vsttwo}_ccf${ccfdelta}.tsv  | awk 'NF>7&&$7!="varType"{a+=$6-$5}END{print a}'`

set refbases=`gbi -l $refgbi | tail -n +3 | awk '{a+=$2}END{print a}'`

set callstats=`awk '$2=="Categorized"{exit}NF>0&&$2!="Summary"{print;print "#"}' variations/ccf/calldiff-stats_${cutoff}-${vsttwo}_ccf${ccfdelta}.tsv`


set mappedbasecount=0
foreach rptfile ( ${basedir}/MAP/*/reports/MappingStats.csv )
  @ mappedbasecount += `grep mappedBaseCount $rptfile | awk -F, '{print $2}'`
end

set exactCov='NA'

set noMapLeft=`awk -F, '$1=="leftSearchedHalfDnbCount"{n=$NF}$1=="leftNotMappedHalfDnbCount"{print int(100000*$NF/n)/1000}' ${basedir}/MAP/*/reports/MappingStats.csv`
set noMapRight=`awk -F, '$1=="rightSearchedHalfDnbCount"{n=$NF}$1=="rightNotMappedHalfDnbCount"{print int(100000*$NF/n)/1000}' ${basedir}/MAP/*/reports/MappingStats.csv`
set excessNoCall=`grep 'NotSearchedHalf' ${basedir}/MAP/*/reports/MappingStats.csv | awk -F, '{a+=$NF}END{print a}'`

echo "bases in ref,$refbases" > $output
echo "called bases,$callbases" >> $output
echo "$callstats" | awk -F\# '{print "calldiff stats" $1; for(i=2;i<NF;i++)print $i}' >> $output
echo "mapped base count,$mappedbasecount" >> $output
echo "exact match coverage,$exactCov" >> $output
echo "% no-mapped left arms,$noMapLeft" >> $output
echo "% no-mapped right arms,$noMapRight" >> $output
echo "excess no-call arms,$excessNoCall" >> $output

