#!/usr/bin/env python

import sys,glob
from optparse import OptionParser
from lib_tags import TagSeqIndexer

# global attributes.
HEADER = ['called_seq','matched_tag_set','matched_seq_id','matched_tag_seq_id','matched_tag_seq','mismatches','score']
ALLOWED_MISMATCHES = 0

def compare_seqs(seq1,seq2):
    if not len(seq1) == len(seq2):
        raise Exception('For comparison len(seq1): %s must equal len(seq2): %s...'%(len(seq1),len(seq2)))

    match_score = 0
    for x in range(len(seq1)):
        if seq1[x] == seq2[x]:
            match_score +=1

    return match_score

def calc_match_score(tag_seq,calls,scores):
    if not len(tag_seq) == len(calls):
        raise Exception('For comparison len(tag_seq): %s must equal len(calls): %s...'%(len(tag_seq.sequence),len(calls)))

    match_score = 0
    for x in range(len(tag_seq)):
        # only sum base scores at matching positions.
        if tag_seq[x] == calls[x] and scores[x] != 'NaN':
            match_score += int(scores[x])

    return match_score

def sum_base_call_scores(scores):
    score = 0
    for x in range(len(scores)):
        if scores[x] == 'NaN': continue
        score += int(scores[x])

    return score

def count_n(sequence):
    n_count = 0
    for i in range(len(sequence)):
        if sequence[i] == 'N': n_count += 1

    return n_count

def match_tag(tsi,calls,scores):
    called_seq = ''.join(calls)
    match_set,match_set_seq_id,match_seq = 'NA','0',''
    mismatches,match_score = 0,0

    if tsi.has_tag(called_seq):
        match_tag_seq = tsi.get_tag(called_seq)
        match_set = match_tag_seq.tag_set.name
        match_set_seq_id = match_tag_seq.seq_id
        match_seq = tsi.seq_index_by_id[match_set][match_set_seq_id].sequence
        mismatches = match_tag_seq.mismatch
        match_score = calc_match_score(match_tag_seq.sequence,calls,scores)
    elif count_n(called_seq) > ALLOWED_MISMATCHES:
        match_set = 'N'

    # calc base call scores sum and over write match score
    #match_score = sum_base_call_scores(scores)

    global_tag_id = match_set + '.' + str(match_set_seq_id)

    return (called_seq,match_set,match_set_seq_id,global_tag_id,match_seq,mismatches,match_score)

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-i','--input-file-path',help='input file path',dest='input_file_path')
    parser.add_option('-m','--mismatches-allowed',default=0,type='int',help='maximum number of mismatches allowed in tag sequence index',dest='mismatches_allowed')
    parser.add_option('-o','--output-file',help='output file path',dest='output_file_path')
    parser.add_option('-t','--tag-set-file',help='file path to tag set file',dest='tag_set_file')
    parser.add_option('-s','--dnb-subsequence-positions',default='54,55,56,57,58,59,60,61,62,63',help='subset of dnb sequence positions to create tag sequence (ex. \"54,55,56,57,58,59,60,61,62,63\")',dest='dnb_subsequence')

    # parse command line arguments.
    options,args = parser.parse_args(arguments[1:])

    # check existence of required parameters.
    if options.input_file_path == None or options.tag_set_file == None:
        parser.print_help()
        raise Exception('Missing required arguments!')

    # create output file path if missing.
    output_file_path = options.output_file_path
    if output_file_path == None:
        output_file_path = options.input_file_path + '/tag_matches.txt'

    return (options.input_file_path,
            options.mismatches_allowed,
            options.tag_set_file,
            output_file_path,
            options.dnb_subsequence)

def find_input_files(inputDirectory):
  print('  Find input files')
  print('    input directory: %s/*_calls.csv' % inputDirectory)
  # find call files.
  call_files = glob.glob(inputDirectory+'/'+'*_calls.csv')

  # for each call file, create score file.
  score_files = []
  file_pairs = 0
  for cf in call_files:
    sf = cf[:-10]+'_scores.csv'
    score_files.append(sf)
    file_pairs +=1
    print('      - %s' % cf)
    print('      - %s' % sf)
    print('    %d file pairs found...' % file_pairs)

  return call_files,score_files

def parse_calls(line):
    # remove white spaces and split by '-'.
    toks = line.rstrip('\r\n').replace(' ','').split('-')

    # return list of called characters, check for tag seq by '-'
    if len(toks) > 1:
        return list(toks[0]) + list(toks[1])
    else:
        return list(toks[0])

def parse_scores(line):
    # remove white spaces and split by '-'.
    toks = line.rstrip('\r\n').replace(' ','').split('-')

    # if scores for tags exist return as a single list.
    if len(toks) > 1:
        return toks[0].split(',') + toks[1].split(',')
    else:
        return toks[0].split(',')

def main(arguments):
    # parse command line arguments.
    input_file_path, \
    mismatches_allowed, \
    tag_set_file, \
    output_file_path, \
    dnb_subsequence = parse_arguments(arguments)

    # set global variables
    ALLOWED_MISMATCHES = mismatches_allowed

    # parse dnb sub-sequence positions.
    positions = [int(x) for x in dnb_subsequence.split(',')]

    # read in oligo tables and index.
    tsi = TagSeqIndexer(tag_set_file)
    tsi.build_indices(mismatches_allowed)

    # open output file and write header.
    sw = open(output_file_path,'w')
    sw.write('\t'.join(HEADER) + '\n')

    # find calls/scores file pairs
    calls_files,scores_files = find_input_files(input_file_path)

    # loop through calls/scores file pairs.
    for ifile in range(len(calls_files)):
        calls_file = calls_files[ifile]
        scores_file = scores_files[ifile]

        # open input files, look up tags, calculate scores and metrics.
        calls_sr = open(calls_file,'r')
        scores_sr = open(scores_file,'r')

        # read through calls/scores file pair and match tags.
        while True:
            calls_line = calls_sr.readline()
            scores_line = scores_sr.readline()

            # exit if EOF
            if not calls_line and not scores_line: break

            # create lists of base calls and scores.
            all_calls = parse_calls(calls_line)
            all_scores = parse_scores(scores_line)

            # check line length
            if len(all_calls) != len(all_scores):
                raise Exception('len(base calls) != len(base call scores)!\n\t%s\n\t\t%s\n\t%s\n\t\t%s\n'%(calls_file,''.join(all_calls),scores_file,','.join(all_scores)))

            # parse out sequence and scores.
            calls = [all_calls[i-1] for i in positions]
            scores = [all_scores[i-1] for i in positions]

            # match results
            results = match_tag(tsi,calls,scores)
            str_list = [str(x) for x in results]
            sw.write('\t'.join(str_list) + '\n')

        # close input file pair
        calls_sr.close()
        scores_sr.close()

    # close output file.
    sw.close()

main(sys.argv)
