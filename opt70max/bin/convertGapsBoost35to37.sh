#!/bin/bash
if [ -n "$1" ]; then 
 mv $1 $1.boost35
 if [ -f "$1.boost35" ]; then
   cat $1.boost35 | dos2unix | sed '/^\(.*count>\)$/{N;/\(\<item>\)$/s/\(.*nt>\)\n\(.*\)\(<item>\)/\1\n\2\<item_version\>0\<\/item_version\>\n\2\3/;}' | unix2dos >$1
 fi
else
 echo Please provide the name of a gap file.
 echo Example:
 echo '  convertGapsBoost35to37.sh GapFile.xml'
fi