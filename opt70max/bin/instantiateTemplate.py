#!/usr/bin/python -u

import os
import sys
import replcode
from optparse import OptionParser
from Ft.Xml import InputSource, Sax

def loadXmlTable(fname):
    class TableToList:
	def startDocument(self):
	    self.rows = []
	    self.currentRow = []
	    self.currentContent = ''
	    self.inRows = 0
	    self.inR = 0
            self.inC = 0

	def startElementNS(self, name, qname, attribs):
	    if (qname == 'c'):
		self.inC = 1
	    elif (qname == 'r'):
                self.inR = 1
	    elif (qname == 'rows'):
		self.inRows = 1

	def endElementNS(self, name, qname):
	    if (qname == 'c'):
		self.inC = 0
		self.currentRow += [self.currentContent.strip()]
		self.currentContent = ''
	    elif (qname == 'r'):
		self.inR = 0
		if (self.inRows):
		    self.rows += [self.currentRow]
		self.currentRow = []
	    elif (qname == 'rows'):
		self.inRows = 0

	def characters(self, content):
	    self.currentContent += content

    factory = InputSource.DefaultFactory
    isrc = factory.fromUri("file:" + fname)
    parser = Sax.CreateParser()
    handler = TableToList()
    parser.setContentHandler(handler)
    parser.parse(isrc)
    return handler.rows

def main(argv):
    parser = OptionParser(usage="usage: %prog [options] templateFileName")
    parser.add_option("-o", "--outfile", dest="outFileName",
		      help="Output file name (default: stdout)")
    (options, args) = parser.parse_args()
    if (len(args) != 1):
	parser.error("Must specify template file name in command line args")
    if (options.outFileName):
	outputFile = open(options.outFileName, 'w')
    else:
	outputFile = sys.stdout

    input_text = open(args[0]).read()
    global_dict = {
        'loadXmlTable':loadXmlTable,
        'CGI_HOME':os.environ['CGI_HOME']
        }
    output_text = replcode.runPythonCode(input_text,global_dict)
    print >> outputFile, output_text

if __name__ == "__main__":
    main(sys.argv[1:])
