#! /usr/bin/env python

import os, sys, re, subprocess
from optparse import OptionParser
from os.path import split as psplit, join as pjoin
from subprocess import Popen, PIPE, call
from gbifile import *



def zopen(fn):
    po = Popen(['bzcat',  fn], stdout=PIPE)
    return (po, po.stdout)



class Mappings(object):
    def __init__(self,mappingsPath):
        (self.proc, self.fileReader) = zopen(mappingsPath)
        # get the column names and start the empty dictionary 
        row = self.fileReader.readline().strip()
        while row.startswith('#') or row == '':
            row = self.fileReader.readline().strip()
        self.colNames = ["flags","chromosome","offsetInChr","gap1","gap2","gap3","weight","mateRec"]
        if self.colNames != row[1:].split('\t'):
            print "Unexpected column names in mappings file."
            raise Exception("Bad mappings.")
        self.mappingList = []
        self.colNumber = len(self.colNames)
        self.numMappings = 0
    
    def nextMappingList(self):
        cols = self.fileReader.readline().strip().split('\t')
        if cols[0] == '':
            # end of file
            return False
        if len(cols) != self.colNumber:
            print "Incorrect number of values in mappings file."
            raise Exception("Bad mappings.")
        rec = 0
        f = int(cols[0])
        armMappings = [{"rec":rec,"flags":f,"chromosome":cols[1],"offsetInChr":int(cols[2]),
                        "gap1":int(cols[3]),"gap2":int(cols[4]),"gap3":int(cols[5]),
                        "weight":ord(cols[6])-33,"mateRec":int(cols[7])}]
        if f < 4:
            armMappings[rec]["strand"] = 'F'
        else:
            armMappings[rec]["strand"] = 'R'
        # read mappings until you encounter odd flag
        while f % 2 == 0:
            cols = self.fileReader.readline().strip().split('\t')
            rec += 1
            f = int(cols[0])
            armMappings.append({"rec":rec,"flags":f,"chromosome":cols[1],"offsetInChr":int(cols[2]),
                                "gap1":int(cols[3]),"gap2":int(cols[4]),"gap3":int(cols[5]),
                                "weight":ord(cols[6])-33,"mateRec":int(cols[7])})
            if f < 4:
                armMappings[rec]["strand"] = 'F'
            else:
                armMappings[rec]["strand"] = 'R'
        #print armMappings
        self.mappingList = []
        self.numMappings = 0
        numArmMappings = len(armMappings)
        mappingUsed  = [False]*numArmMappings
        for i in range(numArmMappings):
            if mappingUsed[i]:
                continue
            self.mappingList.append({})
            self.numMappings += 1
            if armMappings[i]["flags"] in [0,1,4,5]:
                self.mappingList[self.numMappings-1]['left'] = armMappings[i]
                if armMappings[i]["mateRec"] != i: # not just single arm mapping
                    self.mappingList[self.numMappings-1]['right'] = armMappings[armMappings[i]["mateRec"]]
                else:
                    self.mappingList[self.numMappings-1]['right'] = None
            else:
                self.mappingList[self.numMappings-1]['right'] = armMappings[i]
                if armMappings[i]["mateRec"] != i: # not just single arm mapping
                    self.mappingList[self.numMappings-1]['left'] = armMappings[armMappings[i]["mateRec"]]
                else:
                    self.mappingList[self.numMappings-1]['left'] = None
            mappingUsed[armMappings[i]["mateRec"]] = True
        return True
    
    def printCurrentMappings(self):
        for i in range(self.numMappings):
            print "Left  Mapping: ", i, 
            if self.mappingList[i]['left'] != None :
                print "; rec: ",self.mappingList[i]['left']["rec"],
                print "; flags: ",self.mappingList[i]['left']["flags"],
                print "; chromosome: ",self.mappingList[i]['left']["chromosome"],
                print "; offsetInChr: ",self.mappingList[i]['left']["offsetInChr"],
                print "; gap1: ",self.mappingList[i]['left']["gap1"],
                print "; gap2: ",self.mappingList[i]['left']["gap2"],
                print "; gap3: ",self.mappingList[i]['left']["gap3"],
                print "; weight: ",self.mappingList[i]['left']["weight"],
                print "; mateRec: ",self.mappingList[i]['left']["mateRec"],
                print "; strand: ",self.mappingList[i]['left']["strand"]
            else :
                print "; None"
            print "Right Mapping: ", i, 
            if self.mappingList[i]['right'] != None :
                print "; rec: ",self.mappingList[i]['right']["rec"],
                print "; flags: ",self.mappingList[i]['right']["flags"],
                print "; chromosome: ",self.mappingList[i]['right']["chromosome"],
                print "; offsetInChr: ",self.mappingList[i]['right']["offsetInChr"],
                print "; gap1: ",self.mappingList[i]['right']["gap1"],
                print "; gap2: ",self.mappingList[i]['right']["gap2"],
                print "; gap3: ",self.mappingList[i]['right']["gap3"],
                print "; weight: ",self.mappingList[i]['right']["weight"],
                print "; mateRec: ",self.mappingList[i]['right']["mateRec"],
                print "; strand: ",self.mappingList[i]['right']["strand"]
            else :
                print "; None"
    
    def close(self):
        try :
            if self.proc.poll() == None:
                pass
                #self.proc.terminate() # only in newer versions, so just wait for the whole program to finish...
        except :
            pass



class Reads(object):
    def __init__(self,readsPath):
        (self.proc, self.fileReader) = zopen(readsPath)
        # get the column names and start the empty dictionary
        row = self.fileReader.readline().strip()
        while row.startswith('#') or row == '':
            row = self.fileReader.readline().strip()
        self.colNames = ["flags","reads","scores"]
        if self.colNames != row[1:].split('\t'):
            print "Unexpected column names in reads file."
            raise Exception("Bad reads.")
        self.read = {"flags":None,"reads":None,"scores":None}
        self.colNumber = len(self.colNames)
    
    def nextRead(self):
        cols = self.fileReader.readline().strip().split('\t')
        if cols[0] == '':
            # end of file
            return False
        if len(cols) != self.colNumber:
            print "Incorrect number of values in reads file."
            raise Exception("Bad reads.")
        self.read["flags"]  = int(cols[0])
        self.read["reads"]  = cols[1]
        self.read["scores"] = cols[2]
        return True
    
    def printCurrentRead(self):
        print "Read flag: ", self.read["flags"], "; reads: ", self.read["reads"], "; scores: ", self.read["scores"]
    
    def close(self):
        try :
            if self.proc.poll() == None:
                pass
                #self.proc.terminate() # only in newer versions, so just wait for the whole program to finish...
        except :
            pass



class EditLog(object):
    def __init__(self,editLogPath):
        self.fileReader = open(editLogPath)
        # get the column names and start the empty dictionary
        row = self.fileReader.readline().strip()
        while row.startswith('#') or row == '':
            row = self.fileReader.readline().strip()
        self.colNames = ["dnb-no","wellId","wellTag","strand","ctg","ofs","bases","gap","strand","ctg","ofs","bases","gap","strand","ctg","ofs","bases","gap","strand","ctg","ofs","bases","gap","strand","ctg","ofs","bases","gap","strand","ctg","ofs","bases","gap","strand","ctg","ofs","bases","gap","strand","ctg","ofs","bases","gap"]
        if self.colNames != row.split(','):
            print "Unexpected column names in editLog."
            raise Exception("Bad editLog.")
        self.dnb = {"dnb-no":None,"wellId":None,"wellTag":None,"strand":['']*8,"ctg":[-1]*8,"ofs":[-1]*8,"bases":['']*8,"gap":[-1]*7}
        self.colNumber = len(self.colNames)-1 # the last "gap" is missing in the values
    
    def nextDnb(self):
        cols = self.fileReader.readline().strip().split(',')
        if cols[0] == '':
            # end of file
            return False
        if len(cols) != self.colNumber:
            print "Incorrect number of values in editLog."
            raise Exception("Bad editLog.")
        self.dnb["dnb-no"]  = int(cols[0])
        self.dnb["wellId"]  = cols[1]
        self.dnb["wellTag"] = cols[2]
        for i in range(8):
            self.dnb["strand"][i] = cols[3+i*5]
        for i in range(8):
            self.dnb["ctg"][i]    = int(cols[4+i*5])
        for i in range(8):
            self.dnb["ofs"][i]    = int(cols[5+i*5])
        for i in range(8):
            self.dnb["bases"][i] = cols[6+i*5]
        for i in range(7):
            if cols[7+i*5] == 'chimera':
                self.dnb["gap"][i]    = None
            else:
                self.dnb["gap"][i]    = int(cols[7+i*5])
        return True
    
    def printCurrentDnb(self):
        print "Dnb number: ", self.dnb["dnb-no"], "; wellId: ", self.dnb["wellId"], "; wellTag: ", self.dnb["wellTag"], 
        print "; strands: ", ",".join(self.dnb["strand"]), 
        print "; ctg: ", ",".join([str(s) for s in self.dnb["ctg"]]), 
        print "; ofs: ", ",".join([str(s) for s in self.dnb["ofs"]]), 
        print "; bases: ", ",".join(self.dnb["bases"]), 
        print "; gap: ", ",".join([str(s) for s in self.dnb["gap"]])
    
    def close(self):
        try :
            self.fileReader.close()
        except :
            pass




def doPrint(args):
    (options, args) = parseOptions(args)
    gbi = GbiFile(options.gbi)
    numDnbs = 0
    for fileNumber in range(len(options.editLogs)):
        editLog = EditLog(options.editLogs[fileNumber])
        reads = Reads(options.reads[fileNumber])
        mappings = Mappings(options.mappings[fileNumber])
        try :
            #while editLog.nextDnb() :
            while numDnbs < 100:
                numDnbs += 1
                editLog.nextDnb() # comment out if using the first while loop
                editLog.printCurrentDnb()
                oneDnb = editLog.dnb
                reads.nextRead()
                oneRead = reads.read
                reads.printCurrentRead()
                if not (oneRead["flags"] in [5,6,9,10]): # these have mapping(s) for at least one of the two arms
                    mappings.nextMappingList()
                    mapList = mappings.mappingList
                    mappings.printCurrentMappings()
                print "-----"
                # print the chromosome locations of beginings of left and right arms from editLog
                if None in oneDnb["gap"]:
                    print "Chimera" 
                else :
                    strnd = oneDnb["strand"]
                    if strnd[0] == strnd[1] == strnd[2] == strnd[3] == strnd[4] == strnd[5] == strnd[6] == strnd[7] == 'F' :
                        cont = gbi.contigs[oneDnb["ctg"][0]]
                        print "Left  arm original location: ", cont.supername[:-4], " ", cont.offset + oneDnb["ofs"][0]
                        cont = gbi.contigs[oneDnb["ctg"][4]]
                        print "Right arm original location: ", cont.supername[:-4], " ", cont.offset + oneDnb["ofs"][4]
                    elif strnd[0] == strnd[1] == strnd[2] == strnd[3] == strnd[4] == strnd[5] == strnd[6] == strnd[7] == 'R' :
                        cont = gbi.contigs[oneDnb["ctg"][3]]
                        print "Left  arm original location: ", cont.supername[:-4], " ", cont.offset + oneDnb["ofs"][3]-9
                        cont = gbi.contigs[oneDnb["ctg"][7]]
                        print "Right arm original location: ", cont.supername[:-4], " ", cont.offset + oneDnb["ofs"][7]-4
                    else :
                        print "Inconsistent strands!"
                print "*****************************************************************************************************"
        finally :
            print "Processed ", numDnbs, " dnbs."
            editLog.close()
            reads.close()
            mappings.close()



def doAccumulate(args):
    (options, args) = parseOptions(args)
    # exclude simulated chimeras (they have one or more of gaps None in editLog)
    # accumulate single arm mappings separately
    gbi = GbiFile(options.gbi)
    numDnbs = 0
    correctMappings = [0]*maxAsciiWeight
    incorrectMappings = [0]*maxAsciiWeight
    correctMappingsOneArm = [0]*maxAsciiWeight
    incorrectMappingsOneArm = [0]*maxAsciiWeight
    correctBestMappings = [0]*maxAsciiWeight
    okMappings = 0
    incorrectBestMappings = [0]*maxAsciiWeight
    offMappings = 0
    wrongChrMappings = 0
    correctBestMappingsOneArm = [0]*maxAsciiWeight
    okMappingsOneArm = 0
    incorrectBestMappingsOneArm = [0]*maxAsciiWeight
    offMappingsOneArm = 0
    wrongChrMappingsOneArm = 0
    bestMappingOffsetsLeft = {}
    bestMappingOffsetsRight = {}
    for i in range(-100,101):
        bestMappingOffsetsLeft[i] = 0
        bestMappingOffsetsRight[i] = 0
    # initialize histograms to 0
    for fileNumber in range(len(options.editLogs)):
        editLog = EditLog(options.editLogs[fileNumber])
        reads = Reads(options.reads[fileNumber])
        mappings = Mappings(options.mappings[fileNumber])
        try :
            while editLog.nextDnb() :
            #while numDnbs < 100:
                numDnbs += 1
                #editLog.nextDnb() # comment out if using the first while loop
                #editLog.printCurrentDnb()
                oneDnb = editLog.dnb
                reads.nextRead()
                oneRead = reads.read
                #reads.printCurrentRead()
                if (oneRead["flags"] in [5,6,9,10]):
                    continue
                else : # these have mapping(s) for at least one of the two arms
                    mappings.nextMappingList()
                    mapList = mappings.mappingList
                    #mappings.printCurrentMappings()
                if None in oneDnb["gap"]:
                    # Chimera => ignore
                    continue
                strnd = oneDnb["strand"]
                leftArmOrigLocation = -1
                leftArmOrigChr = ''
                rightArmOrigLocation = -1
                rightArmOrigChr = ''
                if strnd[0] == strnd[1] == strnd[2] == strnd[3] == strnd[4] == strnd[5] == strnd[6] == strnd[7] == 'F' :
                    cont = gbi.contigs[oneDnb["ctg"][0]]
                    leftArmOrigChr = cont.supername[:-4]
                    leftArmOrigLocation = cont.offset + oneDnb["ofs"][0]
                    if oneDnb["ctg"][0] in [262,273] : leftArmOrigLocation -= 30 # these contigs were shifted in the Mutated.gbi compared to Reference.gbi
                    cont = gbi.contigs[oneDnb["ctg"][4]]
                    rightArmOrigChr = cont.supername[:-4]
                    rightArmOrigLocation = cont.offset + oneDnb["ofs"][4]
                    if oneDnb["ctg"][4] in [262,273] : rightArmOrigLocation -= 30 # these contigs were shifted in the Mutated.gbi compared to Reference.gbi
                elif strnd[0] == strnd[1] == strnd[2] == strnd[3] == strnd[4] == strnd[5] == strnd[6] == strnd[7] == 'R' :
                    cont = gbi.contigs[oneDnb["ctg"][3]]
                    leftArmOrigChr = cont.supername[:-4]
                    leftArmOrigLocation = cont.offset + oneDnb["ofs"][3]-9
                    if oneDnb["ctg"][3] in [262,273] : leftArmOrigLocation -= 30 # these contigs were shifted in the Mutated.gbi compared to Reference.gbi
                    cont = gbi.contigs[oneDnb["ctg"][7]]
                    rightArmOrigChr = cont.supername[:-4]
                    rightArmOrigLocation = cont.offset + oneDnb["ofs"][7]-4
                    if oneDnb["ctg"][7] in [262,273] : rightArmOrigLocation -= 30 # these contigs were shifted in the Mutated.gbi compared to Reference.gbi
                else :
                    print "Inconsistent strands!"
                    continue
                #print "*****************************************************************************************************"
                # get best mapping (single or both-arm)
                bestMapping = None
                bestMappingWeight = -1
                for mp in mapList :
                    if mp['left'] == None:
                        # single arm mapping
                        if bestMapping == None or ((bestMapping['left'] == None or bestMapping['right'] == None ) and mp['right']['weight'] > bestMappingWeight) :
                            bestMapping = mp
                            bestMappingWeight = mp['right']['weight']
                    elif mp['right'] == None:
                        # single arm mapping
                        if bestMapping == None or ((bestMapping['left'] == None or bestMapping['right'] == None ) and mp['left']['weight'] > bestMappingWeight) :
                            bestMapping = mp
                            bestMappingWeight = mp['left']['weight']
                    else :
                        # both arms mapped
                        if bestMapping == None or (bestMapping['left'] != None and bestMapping['right'] != None and \
                           min(mp['left']['weight'],mp['right']['weight']) > bestMappingWeight) :
                            bestMapping = mp
                            bestMappingWeight = min(mp['left']['weight'],mp['right']['weight'])
                        elif (bestMapping['left'] == None or bestMapping['right'] == None) :
                            # replace best single-arm mapping if you find mapping with both arms
                            bestMapping = mp
                            bestMappingWeight = min(mp['left']['weight'],mp['right']['weight'])
                # best mapping integrity check - weights should be the same for both arms:
                if bestMapping['left'] != None and bestMapping['right'] != None and bestMapping['left']['weight'] != bestMapping['right']['weight'] :
                    print "Inconsistent weights for both arms best mapping:"
                    mappings.printCurrentMappings()
                    raise Exception("Mappings error.")
                # increment histograms for best mappings
                if bestMapping['left'] == None :
                    if bestMapping['right']['chromosome'] != rightArmOrigChr :
                        incorrectBestMappingsOneArm[bestMappingWeight] += 1
                        wrongChrMappingsOneArm += 1
                    elif bestMapping['right']['offsetInChr'] < rightArmOrigLocation -5 or bestMapping['right']['offsetInChr'] > rightArmOrigLocation +5 :
                        incorrectBestMappingsOneArm[bestMappingWeight] += 1
                        offMappingsOneArm += 1
                    else :
                        # good one-arm mapping
                        correctBestMappingsOneArm[bestMappingWeight] += 1
                        okMappingsOneArm += 1
                elif bestMapping['right'] == None :
                    if bestMapping['left']['chromosome'] != leftArmOrigChr :
                        incorrectBestMappingsOneArm[bestMappingWeight] += 1
                        wrongChrMappingsOneArm += 1
                    elif bestMapping['left']['offsetInChr'] < leftArmOrigLocation -5 or bestMapping['left']['offsetInChr'] > leftArmOrigLocation +5 :
                        incorrectBestMappingsOneArm[bestMappingWeight] += 1
                        offMappingsOneArm += 1
                    else :
                        # good one-arm mapping
                        correctBestMappingsOneArm[bestMappingWeight] += 1
                        okMappingsOneArm += 1
                else :
                    # best mapping has both arms
                    if bestMapping['left']['chromosome'] != leftArmOrigChr or bestMapping['right']['chromosome'] != rightArmOrigChr :
                        incorrectBestMappings[bestMappingWeight] += 1
                        wrongChrMappings += 1
                    elif bestMapping['left']['strand'] != oneDnb["strand"][0] :
                        incorrectBestMappings[bestMappingWeight] += 1
                        wrongChrMappings += 1
                    elif bestMapping['right']['offsetInChr'] < rightArmOrigLocation -5 or bestMapping['right']['offsetInChr'] > rightArmOrigLocation +5 or \
                         bestMapping['left']['offsetInChr'] < leftArmOrigLocation -5 or bestMapping['left']['offsetInChr'] > leftArmOrigLocation +5:
                        incorrectBestMappings[bestMappingWeight] += 1
                        offMappings += 1
                        bestMappingOffsetsLeft[min(max(bestMapping['left']['offsetInChr'] - leftArmOrigLocation, -100),100)] += 1
                        bestMappingOffsetsRight[min(max(bestMapping['right']['offsetInChr'] - rightArmOrigLocation, -100),100)] += 1
                        #if min(max(bestMapping['left']['offsetInChr'] - leftArmOrigLocation, -100),100) == -30 :
                        #if bestMapping['left']['weight'] > 30 :
                        #    print "-------------------------------------------------------------------------"
                        #    print "Best mapping with weight > 30:"
                        #    editLog.printCurrentDnb()
                        #    strnd = oneDnb["strand"]
                        #    if strnd[0] == strnd[1] == strnd[2] == strnd[3] == strnd[4] == strnd[5] == strnd[6] == strnd[7] == 'F' :
                        #        cont = gbi.contigs[oneDnb["ctg"][0]]
                        #        print "Left  arm original location: ", cont.supername[:-4], " ", cont.offset + oneDnb["ofs"][0]
                        #        cont = gbi.contigs[oneDnb["ctg"][4]]
                        #        print "Right arm original location: ", cont.supername[:-4], " ", cont.offset + oneDnb["ofs"][4]
                        #    elif strnd[0] == strnd[1] == strnd[2] == strnd[3] == strnd[4] == strnd[5] == strnd[6] == strnd[7] == 'R' :
                        #        cont = gbi.contigs[oneDnb["ctg"][3]]
                        #        print "Left  arm original location: ", cont.supername[:-4], " ", cont.offset + oneDnb["ofs"][3]-9
                        #        cont = gbi.contigs[oneDnb["ctg"][7]]
                        #        print "Right arm original location: ", cont.supername[:-4], " ", cont.offset + oneDnb["ofs"][7]-4
                        #    else :
                        #        print "Inconsistent strands!"
                        #    reads.printCurrentRead()
                        #    mappings.printCurrentMappings()
                        #    print "-------------------------------------------------------------------------"
                    else :
                        # good both-arm mapping
                        correctBestMappings[bestMappingWeight] += 1
                        okMappings += 1
                        bestMappingOffsetsLeft[min(max(bestMapping['left']['offsetInChr'] - leftArmOrigLocation, -100),100)] += 1
                        bestMappingOffsetsRight[min(max(bestMapping['right']['offsetInChr'] - rightArmOrigLocation, -100),100)] += 1
                # increment histograms for all listed mappings
                for mp in mapList :
                    if mp['left'] == None:
                        # single arm mapping
                        if mp['right']['chromosome'] != rightArmOrigChr :
                            incorrectMappingsOneArm[mp['right']['weight']] += 1
                        elif mp['right']['offsetInChr'] < rightArmOrigLocation -5 or mp['right']['offsetInChr'] > rightArmOrigLocation +5 :
                            incorrectMappingsOneArm[mp['right']['weight']] += 1
                        else :
                            # good one-arm mapping
                            correctMappingsOneArm[mp['right']['weight']] += 1
                    elif mp['right'] == None:
                        # single arm mapping
                        if mp['left']['chromosome'] != leftArmOrigChr :
                            incorrectMappingsOneArm[mp['left']['weight']] += 1
                        elif mp['left']['offsetInChr'] < leftArmOrigLocation -5 or mp['left']['offsetInChr'] > leftArmOrigLocation +5 :
                            incorrectMappingsOneArm[mp['left']['weight']] += 1
                        else :
                            # good one-arm mapping
                            correctMappingsOneArm[mp['left']['weight']] += 1
                    else :
                        # both arm mapping
                        # check arm consistency
                        if mp['left']['strand'] != mp['right']['strand'] :
                            print "Inconsistent strands for mapping arms:"
                            mappings.printCurrentMappings()
                            raise Exception("Mappings error.")
                        if mp['left']['chromosome'] != mp['right']['chromosome'] :
                            print "Inconsistent chromosomes for mapping arms:"
                            mappings.printCurrentMappings()
                            raise Exception("Mappings error.")
                        # increment histograms
                        if mp['left']['chromosome'] != leftArmOrigChr or mp['right']['chromosome'] != rightArmOrigChr :
                            incorrectMappings[min(mp['left']['weight'],mp['right']['weight'])] += 1
                        elif mp['left']['strand'] != oneDnb["strand"][0] :
                            incorrectMappings[min(mp['left']['weight'],mp['right']['weight'])] += 1
                        elif mp['left']['offsetInChr'] < leftArmOrigLocation -5 or mp['left']['offsetInChr'] > leftArmOrigLocation +5 or \
                             mp['right']['offsetInChr'] < rightArmOrigLocation -5 or mp['right']['offsetInChr'] > rightArmOrigLocation +5 :
                            incorrectMappings[min(mp['left']['weight'],mp['right']['weight'])] += 1
                        else :
                            # good one-arm mapping
                            correctMappings[min(mp['left']['weight'],mp['right']['weight'])] += 1
                # print progress
                if numDnbs % 100000 == 0 :
                    print "Processed ", numDnbs, "dnbs."
                    print "Both arm mappings: Good: ", okMappings, ", Wrong chromosome: ", wrongChrMappings, ", Off: ", offMappings
                    print "One  arm mappings: Good: ", okMappingsOneArm, ", Wrong chromosome: ", wrongChrMappingsOneArm, ", Off: ", offMappingsOneArm
        finally :
            print "Processed ", numDnbs, " dnbs."
            editLog.close()
            reads.close()
            mappings.close()
            # in case it takes too long, write out files even after interrupting
            writeHistogram('BestMappings.csv',correctBestMappings,incorrectBestMappings)
            writeHistogram('BestMappingsOneArm.csv',correctBestMappingsOneArm,incorrectBestMappingsOneArm)
            writeHistogram('AllMappings.csv',correctMappings,incorrectMappings)
            writeHistogram('AllMappingsOneArm.csv',correctMappingsOneArm,incorrectMappingsOneArm)
            writeDictionaryHistogram('BestMappingOffsets-leftArm.csv',bestMappingOffsetsLeft)
            writeDictionaryHistogram('BestMappingOffsets-rightArm.csv',bestMappingOffsetsRight)
    
    # write out histograms
    writeHistogram('BestMappings.csv',correctBestMappings,incorrectBestMappings)
    writeHistogram('BestMappingsOneArm.csv',correctBestMappingsOneArm,incorrectBestMappingsOneArm)
    writeHistogram('AllMappings.csv',correctMappings,incorrectMappings)
    writeHistogram('AllMappingsOneArm.csv',correctMappingsOneArm,incorrectMappingsOneArm)
    writeDictionaryHistogram('BestMappingOffsets-leftArm.csv',bestMappingOffsetsLeft)
    writeDictionaryHistogram('BestMappingOffsets-rightArm.csv',bestMappingOffsetsRight)
    
    



def parseOptions(args):
    parser = OptionParser('usage: ParseSimulationMappings.py <print|accumulate> [options]')
    parser.disable_interspersed_args()
    parser.add_option('-e', '--editLog', default=None, dest="editLogs", action='append',
                      help='Path to editLog csv file (in ADF directory).')
    parser.add_option('-r', '--reads', default=None, dest="reads", action='append',
                      help='Path to EXP reads_*.tsv.bz2 file.')
    parser.add_option('-m', '--mapping', default=None, dest="mappings", action='append',
                      help='Path to EXP mapping_*.tsv.bz2 file.')
    parser.add_option('-g', '--gbi', default=None, dest="gbi", action='store',
                      help='Path to reference.gbi file.')
    (options, args) = parser.parse_args(args)
    print options
    if len(args) != 0:
        print "Unexpected options:"
        print args
        raise Exception('unexpected args')
    if options.editLogs == None or options.reads == None or options.mappings == None :
        print "editLogs or reads or mappings file(s) were not provided!"
        raise Exception("missing args")
    if(len(options.editLogs) != len(options.reads) or len(options.editLogs) != len(options.mappings)):
        print "Number of files in editLogs, reads and mappings do not agree!"
        raise Exception("Inconsistent number of input files")
    return (options, args)


def writeHistogram(fileName, correctList, incorrectList):
    ff = open(fileName, 'w')
    try :
        ff.write('weight,correct,incorrect\n')
        for i in range(maxAsciiWeight):
            ff.write(str(i))
            ff.write(',')
            ff.write(str(correctList[i]))
            ff.write(',')
            ff.write(str(incorrectList[i]))
            ff.write('\n')
    finally :
        ff.close()


def writeDictionaryHistogram(fileName, dictionary):
    ff = open(fileName, 'w')
    try :
        ff.write('value,count\n')
        for i in sorted(dictionary.keys()):
            ff.write(str(i))
            ff.write(',')
            ff.write(str(dictionary[i]))
            ff.write('\n')
    finally :
        ff.close()


maxAsciiWeight = 94

COMMANDS = { 'print' : doPrint,
             'accumulate' : doAccumulate }

def main():
    parser = OptionParser('usage: %prog <cmd> [options]')
    parser.disable_interspersed_args()

    (options, args) = parser.parse_args(sys.argv[1:])

    if len(args) == 0 or args[0] not in COMMANDS:
        print 'usage: ParseMappings.py <cmd> <options>'
        print 'Commands available:'
        for cmd in COMMANDS:
            print '  %s' % cmd
        raise Exception('bad args')
    COMMANDS[args[0]](args[1:])


if __name__ == '__main__':
    main()
