#!/usr/bin/env python
import sys
import sequence
import datetime
import numpy as np
import subprocess
from wfclib import jsonutil
import glob

now = datetime.datetime.now()

from optparse import OptionParser
from os.path import join as pjoin
from os.path import basename
from dnbLib import *
from sequence import ChrSequence

CHROM_DIR = '/rnd/home/sgiles/data/REF'
BASES = 'ACGT'

class Slide(object):
    slide=None
    cycles=[]
    lanes=[]

class DnbNanoCall(object):
    #DnbNanoCall use index as key of 
    def __init__(self, bases=None, refBases=None, Discordances = None, leftArm_SeqLen=None, rightArm_SeqLen = None, score=None, leftArmInsertRefSeq = None, rightArmInsertRefSeq=None):
        # set default values.
        self.readBases = bases
        self.refBases = refBases
        self.Discordances = Discordances
        self.leftArmInsertRefSeq = leftArmInsertRefSeq
        self.leftArm_SeqLen = leftArm_SeqLen
        self.rightArmInsertRefSeq = rightArmInsertRefSeq
        self.rightArm_SeqLen = rightArm_SeqLen
        self.score = score
        
    def add_nanocall(self, score):
        if not self.score:
            self.score = score
        else:
            self.score=self.score + "," + score
            
def ReadNanocallCSV(fname):
    fname = str(fname)
    fn=str(fname).strip('\'').strip()
    print "NanocallCSV:", fn
    try:
        reader = open(fn, 'r')
    except:
        sys.stderr.write('Couldn\'t open nanocall csv file %s\n' % fn)
        sys.exit(-1)

    # process fasta header.
    start_parsing = 0
    headers = []
    col = {}
    count = 0
    
    for line in reader:
        if line.lower().startswith('index'):
            headers = line.rstrip().split(",")
            for h in range(len(headers)):
                col[headers[h]] =  h
                
            if col["Index"] == -1 | col["Call"] == -1 | col["Score"] == -1 | col["TxR"] == -1 | col["Fit"] == -1 | col["Cy5"] == -1 | col["Cy3"] == -1 | col["C"] == -1 | col["T"] == -1 | col["A"] == -1 | col["G"] == -1:
                sys.stderr.write('Not all columns are present!')
                sys.exit(-1)
            else:
                start_parsing = 1
                continue
            
        elif start_parsing == 1:
            arr = line.rstrip().split(",")
            try:
                indx = int(str(int(arr[0])-1))
                if indx in dnbNC:
                    dnb = dnbNC[indx]
                    score=','.join(str(x) for x in arr[1:])
                    DnbNanoCall.add_nanocall(dnb, score)
                    count = count + 1
            except:
                pass
    
    reader.close()
    
    print "Found %d records in TeraMap" %count
   
    return 
                    

def parse_arguments(arguments):
    parser = OptionParser()
    
    parser.add_option('-i','--input-dir`',        help='text output file from TeraMap',               dest='input_dir')
    parser.add_option('-d','--dnb-architecture',  help='string representation of dnb architecture',   dest='dnb_architecture', default='ad2_29_29')
    parser.add_option('-p','--dnb-positions',  help='number representation of dnb position', dest='dnb_position')
    parser.add_option('-r','--reference-build',   help='UCSC name of the human reference build used', dest='reference_build', default='hg18')
    parser.add_option('-n','--nanocall-files',        help='list of nanocall csv file',  dest='nanocall_file')
    parser.add_option('-o','--output-filename',   help='output report file name',  dest='outDFilename')

    # parse command line arguments.
    options,args = parser.parse_args(arguments[1:])

    # check reference build compatibility.
    supportedBuilds = set(['hg18','hg19'])
    if not options.reference_build in supportedBuilds:
        raise Exception('Reference build %s not supported!' % options.reference_build)

    return options.input_dir, options.dnb_architecture, options.reference_build, options.dnb_position, options.nanocall_file, options.outDFilename
    

def log(string):
    now = datetime.datetime.now()

    print('%s: %s' % (now.strftime('%Y-%m-%d %H:%M:%S'),string))

def find_discordant_bases(seq, mapping, ref, side):
    posDisc = ['F'] * len(seq)
    expRefBases = [''] * len(seq)

    offsets = mapping.dnbArm.dnb.dnbArchitecture.getOffsets(side, mapping.gaps)

    for i in range(len(seq)):
        expRefBases[i] = ref[offsets[i]]
        if seq[i] != 'N' and seq[i] != ref[offsets[i]]:
            posDisc[i] = 'T'
        
    return posDisc, expRefBases

def find_discordances(leftArm, rightArm, chrSeq):
    # this function will full denb sequence, corresponding reference, and discordant positions.
    bases = list(leftArm.sequence) + list(rightArm.sequence)

    # get discordances and expanded reference bases.
    #   left Arm.
    mapping = leftArm.mappings[0]

    leftArmRef = chrSeq.get_sequence(mapping.chromosome, mapping.begin, 
                                    mapping.begin + len(leftArm.sequence) + sum(mapping.gaps))
    leftArm_SeqLen= len(leftArm.sequence) + sum(mapping.gaps)
    if mapping.strand == '-':
        leftArmRef = sequence.reverse_complement(leftArmRef)
    leftArmDiscPos, leftArmExpRefBases = find_discordant_bases(leftArm.sequence,
                                                             mapping, leftArmRef, leftArm.side)

    #   right Arm.
    mapping = rightArm.mappings[0]

    rightArmRef = chrSeq.get_sequence(mapping.chromosome, mapping.begin,
                                    mapping.begin + len(rightArm.sequence) + sum(mapping.gaps))
    rightArm_SeqLen= len(rightArm.sequence) + sum(mapping.gaps)
    if mapping.strand == '-':
        rightArmRef = sequence.reverse_complement(rightArmRef)
    rightArmDiscPos, rightArmExpRefBases = find_discordant_bases(rightArm.sequence, 
                                                               mapping, rightArmRef, rightArm.side)

    discordances = leftArmDiscPos + rightArmDiscPos
    expRefBases = leftArmExpRefBases + rightArmExpRefBases
    
    disc = ''
    
    for i in range(len(discordances)):
        if(discordances[i] == "T"):
            disc = disc + "_" + str(i+1)
            
    
    bases2 = ''.join(bases)
    expRefBases2 = ''.join(expRefBases)
   
    return bases2, expRefBases2, leftArmRef, rightArmRef, disc, leftArm_SeqLen, rightArm_SeqLen


def write_report(fn):
    global dnb_poss
    out = open(fn, 'w')

    nKeys = len(dnbNC.keys())
    print "write_report, dnb_poss", dnb_poss
    headers= ["Call", "Score", "TxR", "Fit", "Cy5", "Cy3", "C", "T", "A", "G"]
    my_headers=""          
    for c in cycles:
        strC="_"+c+','
        my_headers = my_headers + strC.join(headers)+strC
    
    #my_headers.rstrip(',') don't know why it didn't work
    my_headers=my_headers[:-1]
    print "my_headers:", my_headers
    
    xcalls = []
    xrefs = []
    xmatches = []
    xdnb_headers = []
    
    out.write('Index,%s,readBases,refBases,discordances,leftArmRefBases,leftArmSeqLen,rightArmRefBases,rightArmSeqLen' %my_headers)
    for i in range(len(dnb_poss)):
        if int(dnb_poss[i]) != -1:
            out.write(",read_%s,ref_%s,mismatch_%s" %(str(dnb_poss[i]), str(dnb_poss[i]), str(dnb_poss[i])))
    out.write('\n')
              
    for key in dnbNC.keys():
        dnb = dnbNC[key]
        index = int(key)+1
        out.write('%d,%s,%s,%s,%s,%s,%s,%s,%s' % (index, dnb.score, dnb.readBases, dnb.refBases, dnb.Discordances,dnb.leftArmInsertRefSeq,dnb.leftArm_SeqLen,dnb.rightArmInsertRefSeq,dnb.rightArm_SeqLen) )
        for i in xrange(len(dnb_poss)):
            if int(dnb_poss[i]) != -1:
                reads = dnb.readBases
                refs = dnb.refBases
                xcall = reads[int(dnb_poss[i])-1]
                xref= refs[int(dnb_poss[i])-1]
                mismatch = 1
                if xcall == xref: mismatch  = 0
                out.write(',%s,%s,%d' % (xcall, xref,mismatch))
                
        out.write('\n')
        
    out.close()

def main(arguments):
    global dnbNC
    global dnb_poss
    global headers
    global cycles

    dnbNC = {}
    headers=""
    cycles=[]
    inputDir, dnbArchitecture, referenceBuild, dnb_poss, ncfiles, outputFileName = parse_arguments(arguments)
    print "ncfiles:", ncfiles
    ncfiles = (str(ncfiles))[1:-1]
    print "main, dnb_poss:", dnb_poss, type(dnb_poss)
    dnb_poss = dnb_poss.split(',')
    
    
    mapFilePattern = '%s/map-*.txt' % (inputDir)
        
    # create dnb architecture object to get dnb length.
    da = DnbArchitecture(DnbArchitectureStr = dnbArchitecture)
    
    for inputFileName in glob.glob(mapFilePattern):
        print inputFileName
    # load TeraMap output file.
    #   Currently the DnbLibCollection by default filters all non-unique & non-fully mapped dnbs.
        log('loading TeraMap file: %(inputFileName)s' % vars())
        dc = MappedDnbCollection(inputFileName)
        dc.loadDnbMappings(DnbArchitectureParam=dnbArchitecture, FullUniqueMappingsOnly=True)
        log('  - loaded dnb count: %d' % (len(dc.dnbs)))
    
        # index dnbs by chromosome.
        log('  - indexing dnbs by chromosome...')
        dc.indexByChromosome()
        
        
        # loop through chromosomes with dnb mappings.
        for chrName in dc.dnbsByChrom.keys():
            # skip chrM.
            if chrName == 'chrM': continue
            log('    - loading chromosome: %s' % chrName)
            chrFn = pjoin(CHROM_DIR, referenceBuild, chrName + '.fa')
            chrSeq = ChrSequence(chrFn)
    
            # loop through fully mapped unique dnbs.
            for dnb in dc.dnbsByChrom[chrName]:
                # process arms to get full dnb sequence, discordanct positions, and reference bases.
                bases, refBases, leftInsertRefSeq, rightInsertRefSeq, Disc,leftArm_SeqLen, rightArm_SeqLen = find_discordances(dnb.leftArm, dnb.rightArm, chrSeq)
                indx = (dnb.id).split(':')[1]
                dnbNC[int(indx)] = DnbNanoCall(bases, refBases, Disc, leftArm_SeqLen, rightArm_SeqLen, leftArmInsertRefSeq=leftInsertRefSeq, rightArmInsertRefSeq=rightInsertRefSeq)
    
    for file in ncfiles.split(','):
        file=file.replace("'", "").strip()
        print "ncfile=", file
        bname = basename(file)
        #outputFileName = "%s/%s" %(outDir, bname.replace("_NanoCall.csv", "_disc.csv"))
        
        cycles.append(bname.split('.')[1]) #get cycles to pass to write_out
        ReadNanocallCSV(file)

    write_report(outputFileName)
     
if __name__ == '__main__':
    main(sys.argv)
