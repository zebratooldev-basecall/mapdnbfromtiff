#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
# import argparse
import optparse
import ConfigParser
import pymssql

# import pprint
# import cPickle as pickle

###### Document Decription
'''  '''

###### Version and Date
prog_version = '1.0.0'
prog_date = '2014.03.07'

###### Usage
usage = '''

     Version %s  by Vincent-Li  %s

     Usage: %s parameter >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class MsSQLapi(object):
    """An shared cleass for MS SQL connection and query execute
        usage:
            newObj = MsSQLapi(config=xxx, section=xxx, [host=xxx, user=xxx, password=xxx, database=xxx])
        From Config File:
            newObj.readConfig([config=xxx, section=xxx])
            newObj.connectSQL()
        Or directly from given parameter:
            newObj.connectSQL([host=xxx, user=xxx, password=xxx, database=xxx])
    """

    dict={}

    def __init__(self, **arg):

        self.__dict__.update(arg)
        self.dbLoginOpt = ConfigParser.ConfigParser()

    def readConfig(self, config='', section='', autoConnect=True, autoCommit=False):
        self.config = config or self.config
        self.section = section or self.section
        try:
            self.dbLoginOpt.read(self.config)
            tmpDict = {}
            for x in ['host', 'user', 'password', 'database']:
                tmpDict[x] = self.dbLoginOpt.get(self.section, x)
                self.__dict__.update(tmpDict)
                self.dict[x] = self.dbLoginOpt.get(self.section, x)

            if autoConnect == True:
                self.connectSQL(autoCommit=autoCommit)
                return self.connect
            else:
                return tmpDict
        except Exception, e:
            raise e
        


    def connectSQL(self, host='', user='', password='', database='', autoCommit=False):
        self.host = host or self.dict['host']
        self.user = user or self.dict['user']
        self.password = password or self.dict['password']
        self.database = database or self.dict['database']
        try:
            self.connect = pymssql.connect(host=self.host, user=self.user, password=self.password, database=self.database)
            if autoCommit == True:
                self.connect.autocommit(True)
            self.cur = self.connect.cursor()
            return self.connect
        except Exception, e:
            raise e

    def close(self):
        self.cur.close()
        self.connect.close()

    def executeQueryRaw(self, query):
        try:
            self.cur.execute(query)
            return self.cur
        except Exception, e:
            raise e
            self.close()

    def executeQuery(self, query, autoClose=False):
        if isinstance(query, str):
            returnList = self.executeQueryRaw(query).fetchall()
        elif isinstance(query, list):
            returnList = []
            for x in query:
                returnList.append(self.executeQueryRaw(query).fetchall())

        if autoClose == True:
            self.close()
        return returnList

    def commit(self):
        self.connect.commit()





##########################################################################
############################  BEGIN Function  ############################
##########################################################################


######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():

    ########## Phrase parameters
    parser = optparse.OptionParser(usage=usage)
    parser.add_option('-c', '--config', dest='config', help='Config file of SQL.')
    parser.add_option('-s', '--setion', dest='section', help='Section in config for SQL login.')
    parser.add_option('-o', '--host', dest='host', help='Host name for SQL login.')
    parser.add_option('-u', '--user', dest='user', help='User name for SQL login.')
    parser.add_option('-p', '--password', dest='password', help='Password for SQL login.')
    parser.add_option('-d', '--database', dest='database', help='Database to execute query.')
    parser.add_option('-q', '--query', dest='query', help='query to execute.')

    (para, args) = parser.parse_args()

    if len(args) != 0:
        parser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        pass    

    ############################# Body #############################
    if para.config and para.section:
        sqlObj = MsSQLapi(config=para.config, section=para.section)
        sqlObj.readConfig()
    elif para.host and para.user and para.password and para.database:
        sqlObj = MsSQLapi(host=para.host, user=para.user, password=para.password, database=para.database)
        sqlObj.connectSQL()
    else:
        parser.print_help()
        raise Exception, "Parameters number error, please set either [config, section] or [host, user, password, database]."

    if para.query:
        for lines in sqlObj.executeQuery(para.query):
            print "\t".join([str(x) for x in lines])
    else:
        raise Exception, "Not exist parameter query."


#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
