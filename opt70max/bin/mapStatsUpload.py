#!/usr/bin/env python
import os
import StringIO
import pymssql
import cgi
import ConfigParser
import sets
import itertools
import re
import genericUpload

"""
Upload Mapping Stats

"""

class MapStatsUpload (genericUpload.genericUpload):

    def __init__(self,table,dbLogin,lane):
        genericUpload.genericUpload.__init__(self,table,dbLogin)
        self.tables=["lane_discordance","lane_discordancePerPosition","lane_dnbCountPerFragmentGCPercent",
                     "lane_gap","lane_inputDnbCountByNoCallCount","lane_map_stats","lane_miscallMatrix","lane_noCallCountPerInputDnbPosition"]
        self.connect()
        self.lane = lane
        self.current_time= self.getCurrentTime()


    def replaceDataForLane(self):
        for x in self.tables:
            self.execute_no_result("update dwprod.[cgi\\drosenfeld].%s set replaced='%s' where lane='%s'" %(x, self.current_time,self.lane))
        
    def processFile(self,datafile):
        if datafile:
            line = datafile.readline()        
            while line:
                if re.match("dict,map", line):
                    gap=None
                    self.table="dwprod.[cgi\\drosenfeld].lane_map_stats"
                    self.setHeader(["lane","added","replaced","metric","value"],
                                   ["S", "D","D", "S", "F"])
                    line = datafile.readline()
                    continue
                if re.match("array,gap",line) or re.match("array,mateGap",line):
                    gap=line.split(",")[1]
                    header=datafile.readline()
                    h=["lane","added","replaced", "gap"]
                    h.extend(header.split(","))
                    self.table="dwprod.[cgi\\drosenfeld].lane_gap"
                    self.setHeader(h,
                                   ["S","D","D","S","I","F"])
                    line = datafile.readline()
                    continue
                if re.match("array,",line):
                    gap=None
                    table=line.split(",")[1]
                    header=datafile.readline()
                    h=["lane","added","replaced"]
                    h.extend(header.split(","))
                    self.table="dwprod.[cgi\\drosenfeld].lane_%s" % table
                    self.setHeader(h,
                                   ["S","D","D","I","F","F","F","F"])
                    line = datafile.readline()
                    continue
                if re.match("^$",line.strip()):
                    gap=None
                    line = datafile.readline()
                    continue
                if gap:
                    data=[self.lane,self.current_time, None, gap]
                else:
                    data=[self.lane,self.current_time, None]
                data.extend(line.strip().split(","))
                self.uploadRow(data)
                print data
                line=datafile.readline()
            self.uploadRow(None)
            self.postProcessUpload()

def main():
    from optparse import OptionParser
    parser = OptionParser('usage: %prog file')
    parser.add_option('--lane', dest='lane',help='lane id')
    parser.add_option('--file', dest='file',help='file with MappingStats for lane')
    parser.add_option('--config', dest='config',help='full path to dbLogin.cfg config file')
    (options, args) = parser.parse_args()
    dbLogin = ConfigParser.ConfigParser()
    dbLogin.read(options.config)
    uploader=MapStatsUpload("",dbLogin,options.lane)
    if options.file:
        datafile=open(options.file)
    else:
        datafile=open("MAP/%s/reports/MappingStats.csv" %options.lane)
    uploader.replaceDataForLane()
    uploader.processFile(datafile)
    return


if __name__ == '__main__':
    main()

