#! /usr/bin/env python

import sys
from optparse import OptionParser


MAX_SSCORE = 100

def loadGenotypes(fn):
    ff = open(fn)
    hFields = ff.readline().rstrip('\r\n').split('\t')
    chromosomeIdx = hFields.index('chromosome')
    beginIdx = hFields.index('begin')
    endIdx = hFields.index('end')
    alleleSeqIdx = hFields.index('alleleSeq')
    genotypes = {}
    for line in ff:
        fields = line.rstrip('\r\n').split('\t')
        key = ','.join([ fields[idx] for idx in [chromosomeIdx, beginIdx, endIdx, alleleSeqIdx] ])
        genotypes[key] = 1
    ff.close()
    return genotypes


def getSensBySomaticScore(fn, cat, genotypes):
    cumTbl = {}
    for ii in xrange(-MAX_SSCORE, MAX_SSCORE+1):
        cumTbl[ii] = 0

    ff = open(fn)
    hFields = ff.readline().rstrip('\r\n').split('\t')
    scIdx = hFields.index('SomaticCategory')
    ssIdx = hFields.index('SomaticScore')
    chromosomeIdx = hFields.index('chromosome')
    beginIdx = hFields.index('begin')
    endIdx = hFields.index('end')
    alleleSeqIdx = hFields.index('alleleSeq')
    for line in ff:
        fields = line.rstrip('\r\n').split('\t')
        if fields[scIdx] != cat:
            continue
        key = ','.join([ fields[idx] for idx in [chromosomeIdx, beginIdx, endIdx, alleleSeqIdx] ])
        if key not in genotypes:
            continue
        ss = int(fields[ssIdx])
        ss = min(max(ss, -MAX_SSCORE), MAX_SSCORE)
        cumTbl[ss] += 1
    ff.close()

    for ii in xrange(MAX_SSCORE-1, -MAX_SSCORE-1, -1):
        cumTbl[ii] += cumTbl[ii+1]

    return cumTbl


def main():
    parser = OptionParser('usage: %prog [--task=TASKID] PARAMS')
    parser.disable_interspersed_args()
    parser.add_option('--somatic-category', default=None,
                      help='Somatic category for selection (snp, ins, del, or sub).')
    parser.add_option('--output', default=None,
                      help='Path to output file.')
    parser.add_option('--somatic', default=None,
                      help='Path to input SomaticOutput.tsv file.')
    parser.add_option('--genotypes', default=None,
                      help='Path to input genotypes file.')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')
    for name in [ 'somatic_category', 'output', 'somatic', 'genotypes' ]:
        if name not in options.__dict__:
            parser.error('required parameter not specified: '+name.replace('_', '-'))

    genotypes = loadGenotypes(options.genotypes)
    cumTbl = getSensBySomaticScore(options.somatic, options.somatic_category, genotypes)

    ff = open(options.output, 'w')
    print >>ff, 'SomaticScore,CumulativeCount'
    for ii in xrange(-MAX_SSCORE, MAX_SSCORE+1):
        print >>ff, '%s,%s' % (ii, cumTbl[ii])


if __name__ == '__main__':
    main()
