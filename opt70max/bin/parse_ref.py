#!/usr/bin/env python

import os
import sys
from optparse import OptionParser
from bz2 import BZ2File

# script level variables.
counter = {}
sw = None

class TsvReader(object):
    def __init__(self, ff):
        self.ff = ff
        while True:
            line = self.ff.readline()
            if line.startswith('>'): break
            
        self.hFields = line[1:].rstrip('\r\n').split('\t')
        
    def close(self):
        self.ff.close()

    def next(self):
        line = self.ff.readline()
        if '' == line:
            return None
        fields = line.rstrip('r\n').split('\t')
        if len(fields) != len(self.hFields):
            raise Exception('field count mismatch')
        result = {}
        for (name,val) in zip(self.hFields, fields):
            result[name] = val
        return result

############################################################################################

def parse_arguments(arguments):
    parser = OptionParser()
    
    parser.add_option('-i','--ref-file-name',help='merged coverageRefScore file and \"chr\" column of regions file',dest='ref_fn')
    parser.add_option('-o','--output-file-base-name',default='ExomeCoverageStats',help='base name to be used when constructing output file names',dest='output_file_basename')
    
    options,args = parser.parse_args(arguments)
    
    # check number of params.
    if len(arguments) < 2:
        parser.print_help()
        raise Exception('Error! Incorrect number of parameters')
    
    # check for required input files.
    if options.ref_fn == None:
        parser.print_help()
        raise Exception('Error! merged coverageRefScore + regions file required for this utility.')
    
    return(options.ref_fn,
           options.output_file_basename)

                                                                        
############################################################################################



############################################################################################
def main(arguments):

    '''main entry point into program'''
    # parse command line arguments.
    ref_fn, output_fn = parse_arguments(arguments)

    ref = open(ref_fn, 'r')
    out = open(output_fn, 'w')
    inf = TsvReader(ref)
    info = {}

    metrics = [ k for k in inf.hFields if( "Coverage" in k and not "gc" in k )]
    ## have to remove the gcCorrectedCoverage calculations since it sometimes has an N in it (what does that mean??)
    
    while True:
        base = inf.next()
        if base == None: break

        in_region = 1
        if base['chr'] == "":
            in_region = 0

        for metric in metrics:
            value = base[metric]
            info[ (in_region, metric, value) ] =  info.get( (in_region, metric, value), 0 ) + 1

    out.write( "Location\tMetric\tValue\tCount\n" )
    region_string = ["out", "in"]
    for in_region in [0,1]:
        for metric in metrics:
            for value in sorted([ int(k[2]) for k in sorted(info.keys()) if in_region in k and metric in k ]):
                out.write( "\t".join( [region_string[in_region], metric, str(value), str(info[(in_region,metric,str(value))]) ] ) + "\n" )
            

############################################################################################

if __name__ == '__main__':
    main(sys.argv[1:])
        

                                                                                                                    
