#!/usr/bin/env python

import sys
import copy
import lib_itertools

class TagSeq(object):

    def __init__(self,seq_id,sequence):
        self.seq_id = seq_id
        self.sequence = sequence
        self.mismatch = 0

    def set_tagset(self,tag_set):
        self.tag_set = tag_set

class TagSet(object):

    def __init__(self,line):
        #####################################################
        # read in tag name and tag sequences from input line.
        #
        # set_1   GCCAGCATTA  TGTCATAAAT  TTAATTAAGG  ... 
        # set_2   GTCCAAACAG  ATTGGCAACT  CCTTGCATGG  ...
        #####################################################
        fields = line.rstrip('\r\n').split('\t')
        self.name = fields[0]

        # parse tag sequences, create object, and add to list.
        self.tags = []
        for x in range(len(fields)):
            if x == 0: continue
            tag_seq = TagSeq(x,fields[x])
            self.tags.append(tag_seq)
            tag_seq.set_tagset(self)

class TagSeqIndexer(object):

    def __init__(self,input_file):
        # initialize containers.
        self.seq_index = {}
        self.tag_sets = {}
        self.seq_index_by_id = {}

        # open file, read lines, create tag sets, add to index.
        sr = open(input_file,'r')
        for line in sr:
            tag_set = TagSet(line)
            self.tag_sets[tag_set.name] = tag_set

            # index tag sequences by tag set name and seq id.
            for tag_seq in tag_set.tags:
                if not self.seq_index_by_id.has_key(tag_set.name):
                    self.seq_index_by_id[tag_set.name] = {}
                self.seq_index_by_id[tag_set.name][tag_seq.seq_id] = tag_seq

        # close input file.
        sr.close()

    def index_tag_seq(self,tag_seq):
        if self.seq_index.has_key(tag_seq.sequence):
            raise Exception('Error: overlapping tag sequences in index!\n\t[%s, %s, %s]\n\t[%s, %s, %s]' % (self.seq_index[tag_seq.sequence].seq_id,self.seq_index[tag_seq.sequence].mismatch,self.seq_index[tag_seq.sequence].sequence,tag_seq.seq_id,tag_seq.mismatch,tag_seq.sequence))

        self.seq_index[tag_seq.sequence] = tag_seq

    def build_indices(self,max_mismatches):
        for tag_set in self.tag_sets.values():
            self.build_index(tag_set,max_mismatches)

    def build_index(self,tag_set,max_mismatches):
        bases = 'ACGT'

        # loop through tag sequences.
        for tag_seq in tag_set.tags:
            # loop through the range of mis-matches allowed.
            for mm in range(max_mismatches+1):
                # catch zero mm case and add sequence to index.
                if mm == 0:
                    # index tag seq and continue
                    self.index_tag_seq(tag_seq)
                    continue
                # generate and iterate through combinations of positions to mutate.
                for positions_to_mutate in lib_itertools.combinations(range(len(tag_seq.sequence)),mm):
                    # loop through positions and create compliment base sets for mutation sites.
                    complement_base_sets = []
                    for position in positions_to_mutate:
                        complement_bases = []
                        for base in bases:
                            if base == tag_seq.sequence[position]: continue
                            complement_bases.append(base)
                        complement_base_sets.append(complement_bases)
                    # generate and iterate through product of complement base sets, mutate tag sequence, and index.
                    for complement_bases in lib_itertools.product(*complement_base_sets):
                        # make copy and alter sequence
                        seq_copy = list(tag_seq.sequence)
                        for i in range(len(positions_to_mutate)):
                            seq_copy[positions_to_mutate[i]] = complement_bases[i]

                        # make copy of tag_seq object
                        tag_seq_copy = copy.deepcopy(tag_seq)
                        tag_seq_copy.mismatch = mm
                        tag_seq_copy.sequence = ''.join(seq_copy)

                        # index.
                        self.index_tag_seq(tag_seq_copy)

    def has_tag(self,sequence_str):
        '''Does sequence exist in tag seq index'''
        return self.seq_index.has_key(sequence_str)

    def get_tag(self,sequence_str):
        '''Return TagSeq obejct from tag seq index'''
        if not self.seq_index.has_key(sequence_str):
            raise Exception('Error: %s not found in index!'%sequence_str)

        return self.seq_index[sequence_str]

