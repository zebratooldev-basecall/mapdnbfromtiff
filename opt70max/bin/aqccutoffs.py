#!/usr/bin/env python

import sys
import asmrptutil


SEVERITIES = [ 'NORMAL', 'SEVERE' ]
AQC_THRESHOLD = 'VQHIGH'

# This class is shared by CQC and AQC 
class CalcCalledRatio:
    def run(self, data, name, th):
        called = asmrptutil.getTableValue(data, name, 3)
        total = asmrptutil.getTableValue(data, "reference", 3)
        return asmrptutil.floatDiv(called, total)


# This class is shared by CQC and AQC 
class CalcFractionOppositeNoCall:
    def run(self, data, name, th):
        oppositeNoCall = asmrptutil.getTableInt(data, name, 5)
        total = asmrptutil.getTableInt(data, name, 8)
        return asmrptutil.floatDiv(oppositeNoCall, total)


class AqcCutoff(object):
    def __init__(self, name, categories, cuts = None):
        self.name = name
        self.categories = categories
        self.cuts = cuts


class AqcResult(object):
    def __init__(self, cutoff, value, details = True):
        try:
            self.severity = None
            self.cutoff = cutoff
            self.value = value
            self.textcolor = '#000'
            self.color = '#FFF'
            self.outcome = 'NA'
            if cutoff.cuts is None:
                self.outcome = 'NA'
                self.color = '#FFFFFF'
                self.textcolor = '#000'
                self.fail = False
            else:
                self.outcome = 'NT'
                self.color = '#FFFFFF'
                self.fail = False
                for ii in xrange(len(SEVERITIES)-1, -1, -1):
                    (minV,maxV) = cutoff.cuts[ii]
                    severity = SEVERITIES[ii]
                    if minV.strip() == '' and maxV.strip() == '':
                        pass
                    elif minV.strip() != '' and (value == 'NA' or float(value) < float(minV)):
                        self.outcome = 'FAIL'
                        if details:
                            self.outcome = '< ' + minV
                        self.color = self._getFailColor(severity)
                        self.textcolor = self._getFailTextColor(severity)
                        self.fail = True
                        self.severity = severity
                        break
                    elif maxV.strip() != '' and (value == 'NA' or float(value) > float(maxV)):
                        self.outcome = 'FAIL'
                        if details:
                            self.outcome = '> ' + maxV
                        self.color = self._getFailColor(severity)
                        self.textcolor = self._getFailTextColor(severity)
                        self.fail = True
                        self.severity = severity
                        break
                    else:
                        self.outcome = 'OK'
                        self.color = '#CCFFCC'
                        self.fail = False
        except Exception, ee:
            raise Exception( 'cutoff application failed for cutoff "%s", value "%s": %s' %
                             (cutoff.name, value, str(ee)) )

    def _getFailColor(self, severity):
        if 'NORMAL' == severity:
            return '#FFFF00'
        return '#FF4400'

    def _getFailTextColor(self, severity):
        if 'NORMAL' == severity:
            return '#000'
        return '#fff'

    def _renderCuts(self):
        severe = self.cutoff.cuts[1]
        min = severe[0]
        max = severe[1]
        connect = max != '' and min != ''
        rpass = ''
        rfail = ''
	if min != '':
            rpass += ('>' + min)
            rfail += ('<' + min)
        if connect:
            rpass += ' and '
            rfail += ' or '
        if max != '':
            rpass += ('<' + max)
            rfail += ('>' + max)
        return (rpass,rfail)


class AqcCutoffs(object):
    def __init__(self, fn):
        self.fn = fn
        ff = open(fn)
        self._readHeader(ff)
        self._readData(ff)
        ff.close()


    def testCutoffs(self, name, value, details = True):
        result = [ AqcResult(AqcCutoff(name, combo), value, details) for combo in self.categoryCombinations ]
        if name in self.cutoffs:
            result = [ AqcResult(self.cutoffs[name][idx], value, details) for idx in xrange(len(result)) ]
        return result


    def _readData(self, ff):
        fn = self.fn
        self.cutoffs = {}
        prevName = None
        for line in ff:
            line = line.rstrip()
            fields = [ f.strip() for f in line.split(',') ]
            if len(fields) < 3 or len(fields) > 6:
                raise Exception('unexpected field count: '+line)
            name = fields[0]
            categories = []
            if '' != fields[1]:
                categories = fields[1].split(';')
            cuts = []
            for ii in xrange(len(SEVERITIES)):
                offset = 2 + 2*ii
                (minValue, maxValue) = ('', '')
                if offset < len(fields) and fields[offset] != '':
                    minValue = fields[offset]
                    float(fields[offset])
                if offset+1 < len(fields) and fields[offset+1] != '':
                    maxValue = fields[offset+1]
                    float(fields[offset+1])
                cuts.append( (minValue, maxValue) )
            for category in categories:
                if category not in self.cat2dim:
                    raise Exception(fn+': unrecognized category: '+category)
            if name in self.cutoffs and prevName != name:
                raise Exception(fn+': cutoffs not defined in consecutive lines for name: '+name)
            self._addAqcCutoffs(name, categories, cuts)
            prevName = name


    def _addAqcCutoffs(self, name, categories, cuts):
        table = self.cutoffs
        if name not in table:
            table[name] = [ AqcCutoff(name, cat) for cat in self.categoryCombinations ]
        count = 0
        for cutoff in table[name]:
            if ( cutoff.cuts is None and
                 self._categoryMatches(cutoff.categories, categories) ):
                cutoff.cuts = cuts
                count += 1
        if 0 == count:
            raise Exception(self.fn+': cutoff has no effect: %s,%s,%s' %
                            (name,':'.join(categories),
                             ':'.join('%s:%s' % (minV,maxV) for (minV,maxV) in cuts)))


    def _categoryMatches(self, combo, categories):
        for cat in categories:
            if cat not in combo:
                return False
        return True


    def _readHeader(self, ff):
        fn = self.fn
        dim2cat = {}
        cat2dim = {}
        catlist = []
        dimlist = []
        while True:
            line = ff.readline()
            if '' == line:
                break
            if line.startswith('>'):
                line = line.rstrip()
                if line != '>name,category,NORMAL:min,NORMAL:max,SEVERE:min,SEVERE:max':
                    raise Exception(fn+': unexpected header line: '+line)
                break
            elif line.startswith('#'):
                line = line.rstrip()
                if line.startswith('#category,'):
                    fields = line.split(',')
                    (key,values) = (fields[1].strip(), fields[2].strip())
                    if key in dim2cat:
                        raise Exception(fn+': category re-defined: '+key)
                    dim2cat[key] = []
                    dimlist.append(key)
                    values = values.split(';')
                    if len(values) < 2:
                        raise Exception(fn+': category needs at least two values: '+key)
                    for value in values:
                        if value in cat2dim:
                            raise Exception(fn+': category re-defined: '+value)
                        dim2cat[key].append(value)
                        cat2dim[value] = key
                        catlist.append(value)
                else:
                    raise Exception(fn+': unexpected header line: '+line.rstrip())
            elif line.strip(' ,\r\n') != '':
                raise Exception(fn+': unexpected header line: '+line.strip())

        self.dim2cat = dim2cat
        self.cat2dim = cat2dim
        self.catlist = catlist
        self.dimlist = dimlist
        self.categoryCombinations = []
        self._addCategoryCombinations(self.categoryCombinations, [None] * len(self.dimlist), 0)


    def _addCategoryCombinations(self, combos, combo, idx):
        if idx == len(self.dimlist):
            combos.append(combo[:])
            return
        for category in self.dim2cat[self.dimlist[idx]]:
            combo[idx] = category
            self._addCategoryCombinations(combos, combo, idx+1)


class AqcReport(object):
    def __init__(self):
        self.recs = []


    def addQcTable(self, name, threshold=''):
        self.recs.append( ('qctable', (name, threshold)) )


    def addQcRow(self, name, value, threshold=''):
        self.recs.append( ('qcrow', (name, value, threshold)) )


    def addGenderRow(self, detectedGender, manifestGender):
        self.recs.append( ('genderrow', (detectedGender, manifestGender)) )


    def addTsvRow(self, name, value):
        self.recs.append( ('tsvrow', (name,value)) )


    def addQcFailCountRows(self):
        self.recs.append( ('qcfailcountrows', ()) )


    def addTable(self, name, header):
        self.recs.append( ('table', (name, header)) )


    def addRow(self, fields):
        self.recs.append( ('row', (fields)) )

    def addGenericHtml(self, string):
        self.recs.append( ('generic', string) )

    def addFilterRows(self):
        self.recs.append( ('filterrows', ()) )

    def validateCutoffs(self, cutoffs, TargetRegionsFile="None"):
        ### Add "None" as default value of TargetRegionsFile to fix
        ### a bug of tumor pair analysis. by Vincent Li, 2015.10.19
        qcMetrics = {}
        for (rectype,recval) in self.recs:
            if rectype != 'qcrow':
                continue
            (name, value, threshold) = recval
            if threshold != '' and threshold != AQC_THRESHOLD:
                continue
            qcMetrics[name] = 1
        for name in cutoffs.cutoffs:
            if name not in qcMetrics:
                if not ("arget" in name and TargetRegionsFile=="None"):
                    raise Exception('Cutoff defined for non-existent AQC metric: '+name)


    def renderHtml(self, cutoffs, filters):
        result = []
        firstTable = True
        for (rectype,recval) in self.recs:
            if rectype == 'qctable':
                (name, threshold) = recval
                if threshold != '' and threshold != AQC_THRESHOLD:
                    continue
                if not firstTable:
                    self._renderTableEnd(result)
                self._renderQcTableBegin(result, name, cutoffs, filters)
                firstTable = False
            if rectype == 'qcrow':
                (name, value, threshold) = recval
                if threshold != '' and threshold != AQC_THRESHOLD:
                    continue
                self._renderQcRow(result, name, value, cutoffs, filters)
            if rectype == 'qcfailcountrows':
                self._renderQcFailCountRows(result, cutoffs, filters)
            if rectype == 'table':
                (name, header) = recval
                if not firstTable:
                    self._renderTableEnd(result)
                self._renderTableBegin(result, name, header)
                firstTable = False
            if rectype == 'row':
                self._renderRow(result, recval)
            if rectype == 'filterrows':
                self._renderFilterRows(result, filters)
            if rectype == 'generic':
                if not firstTable:
                    self._renderTableEnd(result)
                self._renderGeneric(result, recval)
                firstTable = True

        if not firstTable:
            self._renderTableEnd(result)

        return '\n'.join(result)


    def renderTsv(self, cutoffs, filters):
        header = 'name\tthreshold\tvalue'
        for combo in cutoffs.categoryCombinations:
            if self._passesFilter(combo, filters):
                header += '\t' + self._headerDim(combo, cutoffs, filters)
        result = [ header ]
        for (rectype,recdata) in self.recs:
            if rectype == 'genderrow':
                (detected,manifest) = recdata
                outcome = 'OK'
                if manifest.upper() == 'UNKNOWN':
                    outcome = 'NA'
                elif detected.upper() != manifest.upper():
                    outcome = 'SEVERE:!= '+manifest.upper()
                row = 'Detected gender\t\t%s' % detected.upper()
                tr = cutoffs.testCutoffs('Detected gender', '0')
                for ar in tr:
                    if self._passesFilter(ar.cutoff.categories, filters):
                        row += '\t' + outcome
                result.append(row)
            if rectype == 'tsvrow':
                (name,value) = recdata
                outcome = 'NA'
                row = '%s\t\t%s' % (name,value)
                tr = cutoffs.testCutoffs(name, '0')
                for ar in tr:
                    if self._passesFilter(ar.cutoff.categories, filters):
                        row += '\t' + outcome
                result.append(row)
            if rectype != 'qcrow':
                continue
            (name,value,threshold) = recdata
            row = '%s\t%s\t%s' % (name,threshold,value)
            tr = cutoffs.testCutoffs(name, value)
            for ar in tr:
                if self._passesFilter(ar.cutoff.categories, filters):
                    if ar.outcome not in [ 'OK', 'NA', 'NT' ]:
                        row += '\t' + ar.severity + ':' + ar.outcome
                    else:
                        row += '\t' + ar.outcome
            result.append(row)
        return '\n'.join(result)

    def _renderGeneric(self, result, string):
        result.append(string)

    def _renderTableEnd(self, result):
        result.append('</table>')

    def _renderTableBegin(self, result, name, header):
        result.append('<hr/><p><b>'+name+'</b></p>')
        result.append('<table>')
        result.append('<tr>')
        for hd in header:
            result.append(' <th>'+hd+'</th>')
        result.append('</tr>')


    def _headerDim(self, combo, cutoffs, filters):
        vals = []
        # We have to give the var category and tn category because
        # spotfire reports depend on that being there. If we change
        # this we have to let the spotfire coders (Matt M) know. Examples:
        # low-var, normal
        # high-var, tumor
        HEADER_DIM_LIST = [ 'var', 'tn' ]
        dimCount = 0
        for dim in HEADER_DIM_LIST:
            if dim in cutoffs.dim2cat:
                dimCount += 1
        if dimCount == len(HEADER_DIM_LIST):
            for dim in HEADER_DIM_LIST:
                for val in combo:
                    if cutoffs.cat2dim[val] == dim:
                        vals.append(val)
        else:
            # smoketest for example may not have tn or var categories
            for val in combo:
                if cutoffs.cat2dim[val] not in filters:
                    vals.append(val)
        return 'valid(' + ', '.join(vals) + ')'


    def _renderQcTableBegin(self, result, name, cutoffs, filters):
        header = [ 'Name', 'Value' ]
        for combo in cutoffs.categoryCombinations:
            if self._passesFilter(combo, filters):
                header.append(self._headerDim(combo, cutoffs, filters))
        self._renderTableBegin(result, name, header)


    def _renderQcRow(self, result, name, value, cutoffs, filters):
        fields = [ name, value ]
        tr = cutoffs.testCutoffs(name, value)
        for ar in tr:
            if self._passesFilter(ar.cutoff.categories, filters):
                fields.append(ar)
        self._renderRow(result, fields)


    def _renderRow(self, result, fields, useBold=False):
        result.append('<tr>')
        first = True
        for f in fields:
            field = f
            if None == field:
                field = 'NA'
            if isinstance(field, int):
                field = str(field) 
            if isinstance(field, str):
                tField = field.replace("<", "&lt;")
                tField = tField.replace(">", "&gt;")
                weight = "font-weight:normal;"
                if useBold and first:
                    weight = "font-weight:bold;"
                    first = False
                result.append('<td style="%s">' % weight)
                result.append(str(tField)+'</td>')
                continue
            try:
                # It will throw if field is not a AqcCutoff.
                result.append('<td style="color:%s;background-color:%s;border:solid 1px %s">%s</td>' \
                              % (field.textcolor, field.color, field.color, field.outcome))
            except:
                result.append('<td>ERROR IN HTML OUTPUT</tr>')
        result.append('</tr>')


    def _renderQcFailCountRows(self, result, cutoffs, filters):
        counts = {}
        for severity in reversed(SEVERITIES):
            counts[severity] = []
            for combo in cutoffs.categoryCombinations:
                if self._passesFilter(combo, filters):
                    counts[severity].append(0)
            colCount = len(counts[severity])
        for (rectype, recval) in self.recs:
            if rectype == 'qcrow':
                (name, value, threshold) = recval
                if threshold != '' and threshold != AQC_THRESHOLD:
                    continue
                idx = 0
                tr = cutoffs.testCutoffs(name, value)
                for ar in tr:
                    if self._passesFilter(ar.cutoff.categories, filters):
                        if ar.fail:
                            counts[ar.severity][idx] += 1
                        idx += 1
        for severity in reversed(SEVERITIES):
            fields = [ severity+' validation failures', '' ]
            ct = []
            for ii in xrange(len(SEVERITIES)):
                if severity == SEVERITIES[ii]:
                    ct.append( ('0','0') )
                else:
                    ct.append( ('','') )
            ct = AqcCutoff(severity, [], ct)
            for idx in xrange(colCount):
                ar = AqcResult(ct, counts[severity][idx])
                ar.outcome = str(counts[severity][idx])
                fields.append(ar)
            self._renderRow(result, fields)


    def _renderFilterRows(self, result, filters):
        for filter in filters:
            fields = [ filter+' filter', filters[filter] ]
            self._renderRow(result, fields)

    def _passesFilter(self, combo, filters):
        for filter in filters:
            if filters[filter] not in combo:
                return False
        return True


class PctColoredCutoffs(object):
    def __init__(self, fn):
        self.fn = fn
        self.cutoffs = []
        ff = open(fn)
        header = ff.readline().rstrip()
        if header != 'position,minA,maxA,minC,maxC,minG,maxG,minT,maxT':
            raise Exception('bad header: '+fn)
        for line in ff:
            line = line.rstrip()
            fields = line.split(',')
            position = int(fields[0])
            if position != len(self.cutoffs):
                raise Exception('bad position: '+line)
            self.cutoffs.append( map(float, fields[1:]) )
        ff.close()

    def annotateBaseFraction(self, bf):
        BASES = 'ACGT'
        result = []
        for rec in bf[1:]:
            lane = rec[0]
            position = int(rec[1])
            pB = [ float(val) / float(rec[-1]) for val in rec[2:6] ]
            fails = []
            flag = 'OK'
            for base in xrange(0, 4):
                if pB[base] < self.cutoffs[position][2*base]:
                    flag = 'BAD'
                    fails.append('%s = %.4f < %.3f' % (BASES[base], pB[base], self.cutoffs[position][2*base]))
                    continue
                if pB[base] > self.cutoffs[position][2*base+1]:
                    flag = 'BAD'
                    fails.append('%s = %.4f > %.3f' % (BASES[base], pB[base], self.cutoffs[position][2*base+1]))
                    continue
                fails.append('')
            result.append( [ lane, str(position+1) ] + [ '%.3f' % val for val in pB ] +
                           [ flag ] + fails )
        return result

    def maxPositionFailsByLane(self, bf):
        failsByLane = {}
        for rec in bf[1:]:
            lane = rec[0]
            if lane not in failsByLane:
                failsByLane[lane] = 0
            position = int(rec[1])
            pB = [ float(val) / float(rec[-1]) for val in rec[2:6] ]
            for base in xrange(0, 4):
                if pB[base] < self.cutoffs[position][2*base]:
                    failsByLane[lane] += 1
                    break
                if pB[base] > self.cutoffs[position][2*base+1]:
                    failsByLane[lane] += 1
                    break
        return max( [ 0 ] + failsByLane.values() )


def main():
    cutoffs = AqcCutoffs(sys.argv[1])
    filters = { 'build':'36', 'gender':'male' }
    report = AqcReport()
    report.addQcTable('General')
    report.addQcFailCountRows()
    report.addQcRow('Number of IDQC genotypes tested', '31')
    report.addQcRow('Full sequence cov (Gbp)', '90')
    report.addQcRow('Gross mapping yield (Gb)', '110')
    print '''<html>
<head>
<title>Auto-QC</title>
<style type="text/css">
    table {
    border-collapse: collapse;
    }
    table td {
    border: solid #BBBBBB thin;
    padding-left: 2px;
    padding-right: 2px;
    text-align: right;
    }
</style>
</head>
<body>


<h1><p>Auto-QC Report</p></h1>
'''
    print report.renderHtml(cutoffs, filters)
    print '''</body>
</html>
'''


if __name__ == '__main__':
    main()
