#! /usr/bin/env python

import os, subprocess, sys, wfclib.jsonutil
from os.path import join as pjoin, basename, splitext
from glob import glob
from optparse import OptionParser
from subprocess import Popen, PIPE


def doCall(args):
    print '"' + '" \\\n"'.join(args) + '"'
    sts = subprocess.call(args)
    if sts != 0:
        raise Exception('%s failed with sts %d' % (args[0], sts))


def loadState(workDir, fnglob):
    fns = glob(os.path.join(workDir, 'state', fnglob))
    stateFiles = Popen(["ls", "-t"] + fns, stdout=PIPE).communicate()[0]
    stateFile = os.path.join(workDir, 'state', stateFiles.split()[0])
    return wfclib.jsonutil.load(stateFile)


def sanitizeVariantParams(oo, name):
    if os.path.isdir(oo.__dict__[name]):
        oo.__dict__[name+'_files'] = ':'.join(
            sorted(glob(pjoin(oo.__dict__[name], 'ASM', 'variations', 'annotated', 'Variations*.tsv'))))
    else:
        oo.__dict__[name+'_files'] = oo.__dict__[name]


GENOTYPE_FILES_MAP = {
    'NA19240-B36':'/rnd/home/jbaccash/archive/qual/snpdiff-36/rel24/hapmap.tsv:/rnd/home/jbaccash/archive/qual/snpdiff-36/rel24/infinium.tsv'
    'NA19240-B37':'/rnd/home/jbaccash/archive/qual/snpdiff-37/from-rel24/hapmap.tsv:/rnd/home/jbaccash/archive/qual/snpdiff-37/from-rel24/infinium.tsv'
    }

def sanitizeParams(oo, args):
    if len(args) != 0:
        raise Exception('unexpected args')

    if oo.genotype_files in GENOTYPE_FILES_MAP:
        oo.genotype_files = GENOTYPE_FILES_MAP[oo.genotype_files]

    if oo.genotype_files.count(':') != oo.genotype_ids.count(':'):
        raise Exception('genotype-files and genotype-ids counts differ')

    sanitizeVariantParams(oo, 'asm')

    if oo.reference is None:
        if not os.path.isdir(oo.asm):
            raise Exception('reference must be specified for single-variant-file snpdiff')
        state = loadState(oo.asm, '*.VarCall.*')
        oo.reference = pjoin(state.refDir, 'reference.crr')

    if not os.path.isdir(oo.output):
        os.makedirs(oo.output)


def runTask(oo, args):
    odir = oo.output

    reference = oo.reference
    genotypeIds = oo.genotype_ids.split(':')
    gkey = genotypeIds[oo.task % len(genotypeIds)]
    genotypes = oo.genotype_files.split(':')[oo.task % len(genotypeIds)]
    calls = oo.asm_files.split(':')[oo.task / len(genotypeIds)]
    cf = splitext(basename(calls))[0]

    doCall(['/Proj/QA/Release/cgatools/1.3.0/1.3.0.9/binaries/bin/cgatools',
            'snpdiff',
            '--reference=%s' % reference,
            '--genotypes=%s' % genotypes,
            '--variants=%s' % calls,
            '--output-prefix=%s' % pjoin(odir, 'snpdiff_%s_%s' % (gkey, cf)),
            ]
           )


def main():
    parser = OptionParser('usage: %prog [--task=TASKID] PARAMS')
    parser.disable_interspersed_args()
    parser.add_option('--output', default=pjoin(os.getcwd(), 'snpdiff'),
                      help='Path to output directory for snpdiff results.')
    parser.add_option('--asm', default=os.getcwd(),
                      help='Work dir for assembly to run snpdiff against.')
    parser.add_option('--reference', default=None,
                      help='Path to reference crr file.')
    parser.add_option('--genotype-ids', default='hapmap:infinium',
                      help='Colon-separated list of identifiers to correspond to genotype-files param.')
    parser.add_option('--genotype-files', default='NA19240-B36',
                      help='Colon-separated list of genotypes files for comparison (or NA12940-B36 or NA19240-B37).')
    parser.add_option('--task', type=int, default=None,
                      help='Which task to run, otherwise submits array grid job.')

    (options, args) = parser.parse_args()
    sanitizeParams(options, args)

    if options.task is not None:
        runTask(options, args)
    else:
        taskCount = (1+options.asm_files.count(':')) * (1+options.genotype_files.count(':'))
        doCall(['rat',
                'run',
                '--memory=1',
                '--work-dir=%s' % options.output,
                '--params=%s' % ','.join(str(x) for x in xrange(0, taskCount)),
                "'" + "' '".join(sys.argv) + "' --task=$PARAM"
                ])


if __name__ == '__main__':
    main()
