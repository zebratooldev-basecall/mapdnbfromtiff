#! /usr/bin/env python

"""
Use this program to parse .txt TeraMap output,
characterizing unmapped dnbs.

"""

import sys
import datetime
from optparse import OptionParser
from os.path import join as pjoin
from dnbLib import *

now = datetime.datetime.now()

def check_chimera(dnb):
    chimera = True
    for mapping in dnb.leftArm.mappings:
        if mapping.hasConsistentMate:
            chimera = False

    return chimera

def initialize_holders():
    global noMapped
    global leftMapped
    global rightMapped
    global gapDiffs
    global overlap3Ts; global totalUnmappedArms
    
    noMapped = {} ##nompapped[arm] = [(3T,5T,id),(3T,5T)...]
    noMapped['L'] = []; noMapped['R'] = []
    leftMapped = {}
    leftMapped['L'] = []; leftMapped['R'] = []
    rightMapped = {}
    rightMapped['L'] = []; rightMapped['R'] = []
    gapDiffs = {}
    overlap3Ts = 0; totalUnmappedArms = 0

def check_3T_overlap(sequence3T, sequence5T):
    ''' This function checks if 3T sequences are fully contained in 5T sequence 
    (useful to check for small insert) '''
    overlap = False
    if sequence3T in sequence5T:
        overlap = True
    return overlap

def get_arms_mapped(dnb):
    armsMapped = ''
    if dnb.leftArm.mappingResult == "VALID_MAPPING" and dnb.rightArm.mappingResult == "VALID_MAPPING":
        armsMapped = 'LR'
    elif dnb.leftArm.mappingResult == "VALID_MAPPING":
        armsMapped = 'L'
    elif dnb.rightArm.mappingResult == "VALID_MAPPING":
        armsMapped = 'R'

    return armsMapped

def write_fastq_full_paired_end(outfileFull, outfileL, outfileR, seqDict):
    for n in range(0,len(seqDict['L'])):
        seq3TL, seq5TL, ID = seqDict['L'][n] 
        seq3TR, seq5TR = seqDict['R'][n][:2]
        outseqFull = seq3TL + seq5TL + seq3TR + seq5TR
        outseqL = seq3TL + seq5TL
        outseqR = seq3TR + seq5TR
        outqualFull = 'I' * len(outseqFull)
        outqualL = 'I' * len(outseqL)
        outqualR = 'I' * len(outseqR)
        outfileFull.write('@%s\n%s\n+\n%s\n' % (ID, outseqFull, outqualFull))
        outfileL.write('@%s/1\n%s\n+\n%s\n' % (ID, outseqL, outqualL))
        outfileR.write('@%s/2\n%s\n+\n%s\n' % (ID, outseqR, outqualR))
    seqDict['L'] = []; seqDict['R'] = []

def write_fastq_full(outfile,seqDict):
    for n in range(0,len(seqDict['L'])):
        seq3TL, seq5TL, ID = seqDict['L'][n] 
        seq3TR, seq5TR = seqDict['R'][n][:2]
        outseq = seq3TL + seq5TL + seq3TR + seq5TR
        outqual = 'I' * len(outseq)
        outfile.write('@%s\n%s\n+\n%s\n' % (ID, outseq, outqual))
    seqDict['L'] = []; seqDict['R'] = []
    
def remove_added_Ns(armSeq,T3bases,T5bases):
    seq3T = ""; seq5T = ""
    for i in T3bases:
        seq3T += armSeq[i]
    for i in T5bases:
        seq5T += armSeq[i]

    return seq3T, seq5T
    
def get_dnb_range(dnbArch):
    """ This function outputs slicing indexes for 3' and 5' positions, taking
    into account artificially inserted Ns """
    T3s = {"ad2_40_40":range(0,10),
           "ad1_12_19":range(0,12)} 
    T5s = {"ad2_40_40":range(10,18) + range(20,40),
           "ad1_12_19":range(12,21) + range(22,31)}

    return T3s[dnbArch], T5s[dnbArch]

def find_gapDiffs(dnb):
    #This function will find the difference in gap size between arms within a dnb
    mapping = dnb.leftArm.mappings[0]
    gaps_left = mapping.gaps
    
    mapping = dnb.rightArm.mappings[0]
    gaps_right = mapping.gaps
    
    gapDiff = sum(gaps_left) - sum(gaps_right)
    gapDiffs[gapDiff] = gapDiffs.get(gapDiff,0) + 1

def open_fastq_out(outfileBase):
    bothUnmappedFull = open(outfileBase + '_bothArmsUnmapped.fastq', 'w')
    bothUnmappedL = open(outfileBase + '_bothArmsUnmapped_1.fastq', 'w')
    bothUnmappedR = open(outfileBase + '_bothArmsUnmapped_2.fastq', 'w')
    leftUnmapped = open(outfileBase + '_leftArmUnmapped.fastq', 'w')
    rightUnmapped = open(outfileBase + '_rightArmUnmapped.fastq', 'w')
    
    return bothUnmappedFull, bothUnmappedL, bothUnmappedR, leftUnmapped, rightUnmapped

def write_stat_summary(outputBase):
    # write out csv file with summary statistics
    outfile = outputBase + '_unmapped_summary.csv'
    fo = open(outfile,'w')

    try:
        # write out 3T overlap stat
        fo.write('array,complete3T5Toverlap\n')
        pctOverlap = float(overlap3Ts) / float(totalUnmappedArms)
        fo.write('unmappedArms, armsWith3Tin5T, pctArmsWith3Tin5T\n')
        fo.write('%d,%d,%.6f\n' % (totalUnmappedArms, overlap3Ts, pctOverlap))

        # write out gap differences
        try:
            fo.write('\narray,gap_differences_in_chimeric_dnbs\n')
            fo.write('gapDifference,chimericDnbCountTotal,dnbCountWithGapDiff,dnbPctWithGapDiff\n')
            totalChimeras = sum(gapDiffs.values())
            overlaps = gapDiffs.keys()
            for n in range(min(overlaps), max(overlaps) + 1):
                overlapCount = gapDiffs.get(n,0)
                overlapPct = float(overlapCount) / float(totalChimeras)
                fo.write('%d,%d,%d,%.5f\n' % (n, totalChimeras,overlapCount, overlapPct))
        except ValueError:
            print >> sys.stderr, 'no data for gap differences or overlaps'
        
    finally:
        fo.close()

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-i','--input-file',        help='text output file from TeraMap',               dest='input_file')
    parser.add_option('-o','--output-filebase',   help='base name for fastq and stat output files',                     dest='output_fn')
    parser.add_option('-d','--dnb-architecture',  help='string representation of dnb architecture',   dest='dnb_architecture', default='ad2_40_40')

    # parse command line arguments.
    options,args = parser.parse_args(arguments[1:])

    # check dnb architecture compatibility.
    supportedArchs = set(['ad2_40_40','ad1_12_19'])
    if options.dnb_architecture not in supportedArchs:
        raise Exception('DNB architecture %s not supported!' % options.dnb_architecture)

    return options.input_file, options.output_fn, options.dnb_architecture

def main(arguments):
    global noMapped
    global leftMapped
    global rightMapped
    global gapDiffs
    global overlap3Ts; global totalUnmappedArms
    # parse arguments
    inputFileName, outputFileBase, dnbArchitecture = parse_arguments(arguments)
    print >> sys.stderr, 'inputfile:  %s'%inputFileName
    #pull dnb architecture
    T3bases, T5bases = get_dnb_range(dnbArchitecture)

    #load TeraMap output
    dc = MappedDnbCollection(inputFileName)
    dc.loadDnbMappings(DnbArchitectureParam=dnbArchitecture, FullUniqueMappingsOnly=False)
    mydnbs = dc.dnbs ## list of dnb objects

    # initialize holders for sequences and gap output
    initialize_holders()

    # open fastq output files
    buof, buol, buor, luo, ruo = open_fastq_out(outputFileBase)

    try:
        # loop through dnbs and figure out what I want
        for dnb in mydnbs:
            # check how many arms mapped
            armsMapped = get_arms_mapped(dnb)
        
            # do something specific depending on number of arms mapped
            if armsMapped == 'LR':
                # Check if mappings are consistent
                chimera = check_chimera(dnb)
                if chimera:
                    # get gap differences for unique inconsistent mappings
                    if len(dnb.leftArm.mappings) == 1 and len(dnb.rightArm.mappings) == 1:
                        find_gapDiffs(dnb)
            else:
                # get 3' and 5' sequences with added Ns removed
                if dnbArchitecture == 'ad1_12_19':
                    seq3Tright = ''
                    seq5Tright = dnb.rightArm.sequence[:9] + dnb.rightArm.sequence[10:]
                    seq3Tleft = dnb.leftArm.sequence; seq5Tleft = ''
                else:
                    seq3Tright, seq5Tright = remove_added_Ns(dnb.rightArm.sequence,T3bases,T5bases)
                    seq3Tleft, seq5Tleft = remove_added_Ns(dnb.leftArm.sequence,T3bases,T5bases)
                
                # check 3T overlap and write out fastq based on which arms mapped
                if armsMapped == 'L':
                    totalUnmappedArms += 1
                    if check_3T_overlap(seq3Tright,seq5Tright):
                        overlap3Ts += 1    
                    leftMapped['L'].append((seq3Tleft,seq5Tleft,str(dnb.id) ))
                    leftMapped['R'].append((seq3Tright,seq5Tright,str(dnb.id)))
                    if len(leftMapped['L']) > 49:
                        write_fastq_full(ruo, leftMapped)
        
                elif armsMapped == 'R':
                    totalUnmappedArms += 1
                    if check_3T_overlap(seq3Tleft,seq5Tleft):
                        overlap3Ts += 1    
                    rightMapped['L'].append((seq3Tleft,seq5Tleft,str(dnb.id)))
                    rightMapped['R'].append((seq3Tright,seq5Tright,str(dnb.id)))
                    if len(rightMapped['L']) > 49:
                        write_fastq_full(luo, rightMapped)

                elif not armsMapped:
                    for seqPair in [(seq3Tleft, seq5Tleft),(seq3Tright,seq5Tright)]:
                        totalUnmappedArms += 1
                        if check_3T_overlap(seqPair[0],seqPair[1]):
                            overlap3Ts += 1
                    noMapped['L'].append((seq3Tleft,seq5Tleft,str(dnb.id) ))
                    noMapped['R'].append((seq3Tright,seq5Tright,str(dnb.id) ))
                    if len(noMapped['L']) > 49:
                        write_fastq_full_paired_end(buof, buol, buor, noMapped)

    finally:
        write_fastq_full(ruo, leftMapped)
        write_fastq_full(luo, rightMapped)
        write_fastq_full_paired_end(buof, buol, buor, noMapped)
        for outfile in [buof, buol, buor, luo, ruo]:
            outfile.close()

    # write summary stat output file
    write_stat_summary(outputFileBase)

if __name__ == '__main__':
    main(sys.argv)
