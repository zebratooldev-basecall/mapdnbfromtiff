#!/usr/bin/env python

import sys

class CgiReport(object):
    def __init__(self, fn):
        self.filename = fn
        self.dicts = []
        self.arrays = []
        self.dictIdx = {}
        self.arrayIdx = {}
        self.dictFields = {}

        try:
            ff = open(fn, 'r')
            self.__loadReport(ff)
            ff.close()
        except:
            print "Unexpected error: ", sys.exc_info()[0]
            raise

    def __loadReport(self, ff):
        while True:
            line = ff.readline()
            if not line: break
            line = line.rstrip('\r\n')
            if line == '': continue

            fields = line.split(',')
            if fields[0] == 'dict':
                self.__loadDict(ff, fields[1])
            elif fields[0] == 'array':
                self.__loadArray(ff, fields[1])
            else:
                raise Exception('Unknown report block: %s' % fields[0])

    def __loadDict(self, ff, name):
        result = {}
        self.dictFields[name] = []

        while True: 
            line = ff.readline().rstrip('\r\n')
            if line == '': break

            fields = line.split(',')
            result[fields[0]] = fields[1]
            self.dictFields[name].append(fields[0])

        self.dicts.append(result)
        self.dictIdx[name] = result

    def __loadArray(self, ff, name):
        result = []
        hFields = ff.readline().rstrip('\r\n').split(',')

        while True:
            line = ff.readline().rstrip('\r\n')
            if line == '': break

            fields = line.split(',')
            record = {}
            for (key,val) in zip(hFields, fields):
                record[key] = val
            result.append(record)

        self.arrays.append(result)
        self.arrayIdx[name] = result

