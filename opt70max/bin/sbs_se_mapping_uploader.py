#!/usr/bin/python
import sys, os, re, pymssql, time, operator, glob
from optparse import OptionParser
from ConfigParser import ConfigParser

########################### Global variant ###########################
table_template = '[dwProd].[bioinfo].[%s]'
table_match_table = {
    'map_by_field': 'se_mapping_by_field_table',
    'map_': 'se_mapping_summary_table',
    'discordancePerPosition': 'se_mapping_pos_dis',
    'discordancePattern': 'se_mapping_discordance_pattern',
    'baseComposition': 'se_mapping_base_composition'
}

dbConfig_path = os.path.join(os.environ["CGI_HOME"], 'etc/config/DB/dbLogin.cfg')
dbConfig = ConfigParser()
dbConfig.read(dbConfig_path)
########################### Functions ###########################
def caplital_to_seperated_string(string,returnArray=False):
    list_ = [a.lower() for a in re.split(r'([A-Z][a-z,\_,\d]*)', string) if a]
    if returnArray: 
        return list_
    else:
        return '_'.join(list_)

def open_connection(dbConfig):
    whichDB = 'idqc:dwProd'

    print 'Opening database connection...'
    print '  - retrieving params from config for %s database...' % whichDB
    conn = pymssql.connect(host=dbConfig.get(whichDB,'host'),
                           user=dbConfig.get(whichDB,'user'),
                           password=dbConfig.get(whichDB,'password'),
                           database=dbConfig.get(whichDB,'database'))

    # check connection.
    if not conn:
        print 'Unable to connect to %s!' % dbLogin.get(whichDB,'host')
        os.sys.exit(-1)

    return conn

def get_request_id (requestor,email,dbConfig,mapping_fraction,demux_id):
    conn = open_connection(dbConfig)
    cur = conn.cursor()

    sql = '''INSERT INTO [dwProd].[bioinfo].[se_mapping_request_table] (requestor,email,mapping_fraction,demux_id) OUTPUT inserted.[request_id] VALUES (\'%s\',\'%s\',%f,%s) ''' % (requestor,email,float(mapping_fraction),demux_id)

    cur.execute(sql)
    rows = cur.fetchone()
    conn.commit()
    
    request_id = int(rows[0])
    conn.close()

    print 'the request_id is %d\n' % request_id
    if request_id == 0 :
        sys.exit(1)

    return request_id

def execute_sql_statements(dbConfig,sql_accumulator):
    print 'Executing accumulated SQL statements'
    print '  - %d sql inserts to execute' % len(sql_accumulator)

    # get connection.
    conn = open_connection(dbConfig)

    # setup cursor.
    cur = conn.cursor()

    # loop through sql statements and execute 50 at a time. This is due to
    # string length restrictions in pymssql.
    i, istart, iend = 0, 0, 0
    exit_cond = False
    while True:
        # set slice bounds
        istart = i * 50
        iend = istart + 50
        if iend > len(sql_accumulator):
            iend = len(sql_accumulator)
            exit_cond = True

        print '  - inserting statements %d through %d...' % (istart,iend)

        # create cursor.
        cur = conn.cursor()

        # execute a slice of accumulated sql statements.
        cur.execute('\n'.join(sql_accumulator[istart:iend]))

        # close connection.
        cur.close()

        i += 1

        # exit condition
        if exit_cond: break

    # commit.
    conn.commit()

    # close
    conn.close()

    print '  - insert completed...'

def read_an_array_block(string_block,addiction_info={},record_order=False):
    array = []
    header = [caplital_to_seperated_string(x) for x in string_block.pop(0).split(',')]
    rank = 1
    for line in string_block:
        temp = addiction_info.copy()
        line = line.split(',')
        for i in range(len(line)):
            temp[header[i]] = line[i]
        if record_order: 
            temp['rank'] = rank
            rank += 1
        array.append(temp)
    return array

def read_a_dict_block(string_block):
    temp = {}
    for line in string_block:
            nm, value = line.split(',')
            nm = caplital_to_seperated_string(nm)
            temp[nm] = value
    return temp

def return_insert_sql_from_dict(dict_,table_name):
    key = []
    value = []
    for k in sorted(dict_.keys()):
        key.append(k)
        x = dict_[k]
        if re.match(r'[a-z,A-z]', str(x)):
            x = '\'' + str(x) + '\''
        else:
            x = str(x)
        value.append(x)
    value = ','.join(value)
    sql = '''INSERT INTO %s (%s) VALUES(%s) ''' % (table_name, ",".join(key), value)
    return sql

########################### Object ###########################
class array_base_class:
    def __init__(self, addiction_info, string_block, orignal_table_name,record_order=False):
        self.orignal_table_name = orignal_table_name
        self.array = read_an_array_block(string_block,addiction_info,record_order)
    def return_sql_query(self):
        sql_list = []
        table_name = table_template % table_match_table[self.orignal_table_name]
        for dict_ in self.array:
            sql_list.append(return_insert_sql_from_dict(dict_, table_name))
        return sql_list
    def get_value(self,key):
        temp = []
        for x in self.array:
            temp.append(x[key])
        return temp
    def merge(self,string_block,key_list=[],treat_as_str_keys=[]):
        if len(key_list) == 0: return
        key_list = [caplital_to_seperated_string(k) for k in key_list]

        existing_keys = {}
        for i in range(len(self.array)):
            item = self.array[i]
            key = ','.join([ item[k] for k in key_list])
            if key in existing_keys.keys():
                print 'the key is not unique!! %s: %s' % (self.orignal_table_name, ','.join(key_list))
                sys.exit(1)
            existing_keys[key] = i

        temp_array = read_an_array_block(string_block)
        for item in temp_array:
            key = ','.join([ item[k] for k in key_list]) 
            if key in existing_keys.keys():
                i = existing_keys[key]
                for key_ in item.keys():
                    if key_ not in key_list: 
                        if key_ in self.array[i].keys():
                            if not key_ in treat_as_str_keys: 
                                try :
                                    self.array[i][key_] = str( int(self.array[i][key_]) + int(item[key_]) )
                                except:
                                    if not (item[key_] in self.array[i][key_]):
                                        self.array[i][key_] = self.array[i][key_] + ',' + item[key_]
                            else:
                                if not (item[key_] in self.array[i][key_]):
                                    self.array[i][key_] = self.array[i][key_] + ',' + item[key_]
                        else:
                            print 'new key! not supposed to be! %s' % (key_)
                            sys.exit(1)
            else:
                self.array.append(item)
    def re_rank(self,rank_by=""):
        if rank_by == "": return

        self.array = sorted (self.array, key = lambda x: int(x[rank_by]), reverse=True)
        rank = 1
        for i in range(len(self.array)):
            self.array[i]['rank'] = rank
            rank += 1
    def append(self,request_id,string_block):
        addiction_info = {'request_id' : request_id}
        temp_array = read_an_array_block(string_block,addiction_info)
        self.array.extend(temp_array)

class map_: 
    def __init__(self,request_id,slidelane,requestor,field,email,tag_set,string_block):
        self.dict = read_a_dict_block(string_block)
        self.dict['slidelane'] = slidelane
        self.dict['request_id'] = request_id
        self.dict['tag_set'] = tag_set

        self.sub_fields = []
        self.sub_fields.append(map_by_field(request_id,slidelane,requestor,field,email,tag_set,string_block) )

    def return_sql_query(self):
        self.update_percentile()

        table_name = table_template % table_match_table[self.__class__.__name__]
        return [return_insert_sql_from_dict(self.dict, table_name) ] + [x.return_sql_query() for x in self.sub_fields ]
    def merge(self,request_id,slidelane,requestor,field,email,tag_set,string_block):
        self.sub_fields.append(map_by_field(request_id,slidelane,requestor,field,email,tag_set,string_block) )

        temp_dict = read_a_dict_block(string_block)
        for key in temp_dict.keys():
            if key in self.dict.keys():
                try :
                    self.dict[key] = str(int(self.dict[key]) + int(temp_dict[key]))
                except:
                    self.dict[key] = self.dict[key]  + ',' + temp_dict[key]
            else:
                self.dict[key] = temp_dict[key]
    def update_percentile(self):
        pct_cut = [10,25,50,75,90]
        import numpy as np 
        a = np.array([x.mapping_rate for x in self.sub_fields ])
        for p in pct_cut:
            key = 'p%d_map_rate' % p
            value = np.percentile(a, p)
            self.dict[key] = value
        a = np.array([x.dis_rate for x in self.sub_fields ])
        for p in pct_cut:
            key = 'p%d_dis_rate' % p
            value = np.percentile(a, p)
            self.dict[key] = value

class map_by_field:
    def __init__(self,request_id,slidelane,requestor,field,email,tag_set,string_block):
        self.dict = read_a_dict_block(string_block)
        self.dict['slidelane'] = slidelane
        self.dict['request_id'] = request_id
        self.dict['field'] = field
        self.dict['tag_set'] = tag_set
        if float(self.dict['mapped_bases']): 
            self.dis_rate = float(self.dict['discordant_bases']) / float(self.dict['mapped_bases'])
        else:
            self.dis_rate = 0
        if float(self.dict['input_dnb_count']):
            self.mapping_rate = float(self.dict['mapped_dnb_count']) / float(self.dict['input_dnb_count'])
        else:
            self.mapping_rate = 0
        
    def return_sql_query(self):
        table_name = table_template % table_match_table[self.__class__.__name__]
        return return_insert_sql_from_dict(self.dict, table_name)

class wrapper:
    def __init__(self,stat_file,request_id,slidelane,requestor,field,email,tag_set):
        temp_dictionary = {}
        for b in open(stat_file).read().split('\n\n'):
            lines = b.rstrip('\n').split('\n')
            type_, nm = lines.pop(0).split(',')
            temp_dictionary[nm] = lines
        self.map = map_(request_id,slidelane,requestor,field,email,tag_set,temp_dictionary['map'])

        addiction_info= {
            'request_id': request_id,
            'slidelane': slidelane,
            'tag_set': tag_set
        }

        self.discordancePerPosition = array_base_class(addiction_info, temp_dictionary['discordancePerPosition'], 'discordancePerPosition') 
        self.discordancePattern = array_base_class(addiction_info, temp_dictionary['discordancePattern'], 'discordancePattern')  
        self.baseComposition = array_base_class(addiction_info, temp_dictionary['baseComposition'], 'baseComposition')   

    def merge(self,stat_file,request_id,slidelane,requestor,field,email,tag_set):
        temp_dictionary = {}
        print 'merging  %s ' % stat_file
        for b in open(stat_file).read().split('\n\n'):
            lines = b.rstrip('\n').split('\n')
            type_, nm = lines.pop(0).split(',')
            temp_dictionary[nm] = lines
        self.map.merge(request_id,slidelane,requestor,field,email,tag_set,temp_dictionary['map'])
        self.discordancePerPosition.merge(temp_dictionary['discordancePerPosition'],key_list=['position'])
        self.discordancePattern.merge(temp_dictionary['discordancePattern'],key_list=['query_position','comparison_position'])
        self.baseComposition.merge(temp_dictionary['baseComposition'],key_list=['position'])

    def return_sql_query(self):
        sql_array = self.map.return_sql_query() + \
            self.discordancePerPosition.return_sql_query() + \
            self.discordancePattern.return_sql_query() + \
            self.baseComposition.return_sql_query()
        return sql_array

def parse_arguments(args):
    parser = OptionParser()
    parser.add_option('--workDir', help='working directory', default='./', dest='workDir')
    parser.add_option('--requestor', help='requestor', default=None, dest='requestor')
    parser.add_option('--email', help='email', default=None, dest='email')
    parser.add_option('--mapping-fraction', help='mapping fraction', default=1, dest='mapping_fraction')
    parser.add_option('--input-json-file', help='the input json file for specify the sam_summary, advised when you map by field', default=None, dest='json')
    parser.add_option('--demux-id', help='the id in the demux table', default=None, dest='demux_id')

    options, args = parser.parse_args(args)
    workDir = options.workDir
    requestor = options.requestor
    email = options.email
    demux_id = options.demux_id

    mapping_fraction = float(options.mapping_fraction)
    input_json = options.json

    if requestor is None:
        import getpass
        requestor=getpass.getuser()
    if email is None: email = 'NA'
    if not demux_id: 
        demux_id = 'NULL'

    csv_information = {}
    if input_json:
        try:
            import simplejson as js
        except:
            import json as js
        csv_information = js.load(input_json)
        if type(csv_information) != type({}): 
            print 'something is wrong with the jsonfile, it is not a dictionary %s' % input_json
        for key,sub_dict in csv_information.items():
            keys = sub_dict.keys()
            if not (('slidelane' in keys) and ('field' in keys) and ('csv_path' in keys) and ('tag_set' in keys)):
                print 'slidelane or field or csv_path or tag_set is missing in %s ' % key 
                csv_information.pop(key, None)
    else:
        print 'use the walking to find the summaries'
        path = os.path.join(workDir, 'MAP')
        print path
        for root, dirs, files in os.walk(path):
            for file in files:
                if 'summary' in file:
                    file_path = os.path.join(root,file)
                    m1 = re.search('GS\d+\-FS3\-L0\d',file_path) 
                    m2 = re.search('C\d+R\d+', file_path)
                    m3 = re.search('GS.*_(.+?)\.', file_path)
                    slidelane = None
                    field = 'All_fields'
                    tag_set = 'non-pooling'
                    if m1 is None:
                        # This is uncommon, throw an info skip
                        print 'This summary file is unusual: %s' % file_path
                        continue
                    else:
                        slidelane = m1.group(0)
                    if m2 is None:
                        # This is supposed to be a whole lane result#
                        print 'This file is not run by field: %s' % file_path
                    else:
                        field = m2.group(0)
                    if m3 is None or m3.group(1) is None:
                        # This is not a demux lane
                        print 'This file is not a demux lane: %s' % file_path
                    else:
                        tag_set = m3.group(1)

                    #if not 'unmapped' in file_path : continue
                    csv_information[file] = {
                        'slidelane': '\'' + slidelane + '\'',
                        'field': '\'' + field +'\'',
                        'csv_path': file_path,
                        'tag_set': '\'' + tag_set +'\''
                        }

    return csv_information,requestor,email,mapping_fraction,demux_id

def main(argv):
    csv_information,requestor,email,mapping_fraction,demux_id = parse_arguments(argv)

    sql_list = []
    request_id = str(get_request_id (requestor,email,dbConfig,mapping_fraction,demux_id))
    big_table = {}

    for name,item in csv_information.items(): 

        slidelane = item['slidelane']
        field = item['field']
        csv_path = item['csv_path']
        tag_set = item['tag_set']

        key = ','.join([slidelane,tag_set])
        if key in big_table.keys():
            big_table[key].merge(csv_path,request_id,slidelane,requestor,field,email,tag_set)
        else:
            big_table[key] = wrapper(csv_path,request_id,slidelane,requestor,field,email,tag_set)

    for key in big_table.keys():
        sql_list.extend(big_table[key].return_sql_query())

    sql = 'UPDATE %s SET upload_date=convert(datetime,\'%s\',120) WHERE request_id=%s' % ('[dwProd].[bioinfo].[se_mapping_request_table]', time.strftime('%Y-%m-%d %H:%M:%S'), request_id) 
    sql_list.append(sql)
    #print '\n'.join(sql_list)
    print len(sql_list)
    execute_sql_statements(dbConfig,sql_list)

main(sys.argv)