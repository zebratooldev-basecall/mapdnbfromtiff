#!/usr/bin/python
import os,sys
import optparse
import numpy
from numpy import *
from math import *

#####################################
### magic constant for smoothing ratios
#####################################

pseudocounts = 5.0



def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(gapsDir='',refDir='',libID='',mappingDataDir='')
    parser.add_option('-g', '--gaps-dir', dest='gapsDir',
                      help='directory containing (partial) gaps object to work on')
    parser.add_option('-r', '--ref-dir', dest='refDir',
                      help='directory containing reference/simulation data')
    parser.add_option('-l', '--lib-id', dest='libID',
                      help='ID of library')
    parser.add_option('-d', '--mapping-data-dir', dest='mappingDataDir',
                      help='directory that contains mapper output and intermediate covmodel files')


    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.gapsDir:
        parser.error('gaps-dir specification required')
    if not options.refDir:
        parser.error('ref-dir specification required')
    if not options.libID:
        parser.error('lib-id specification required')


    ##############################
    #### get sequence patch values for sim data
    ##############################

    simleftend = {}
    simrightend = {}
    simleftAcuI = {}
    simrightAcuI = {}
    simleftEcoP15 = {}
    simrightEcoP15 = {}

    simleftendttl = 0.0
    simrightendttl = 0.0
    simleftAcuIttl = 0.0
    simrightAcuIttl = 0.0
    simleftEcoP15ttl = 0.0
    simrightEcoP15ttl = 0.0

    simsummarystats = open(os.path.join(options.refDir,'CovModel/covStatsSummary.csv'),'r')
    for l in simsummarystats:
        l.strip()
        w = l.split()
        stat = w[0].split(',')
        if stat[0] == 'leftend':
            simleftend[stat[1]] = float(w[1])
            simleftendttl += float(w[1])
        elif stat[0] == 'rightend':
            simrightend[stat[1]] = float(w[1])
            simrightendttl += float(w[1])
        elif stat[0] == 'leftAcuI':
            simleftAcuI[stat[1]] = float(w[1])
            simleftAcuIttl += float(w[1])
        elif stat[0] == 'rightAcuI':
            simrightAcuI[stat[1]] = float(w[1])
            simrightAcuIttl += float(w[1])
        elif stat[0] == 'leftEcoP15':
            simleftEcoP15[stat[1]] = float(w[1])
            simleftEcoP15ttl += float(w[1])
        elif stat[0] == 'rightEcoP15':
            simrightEcoP15[stat[1]] = float(w[1])
            simrightEcoP15ttl += float(w[1])
    simsummarystats.close()


    ##############################
    #### get sequence patch values for real data
    ##############################

    realleftend = {}
    realrightend = {}
    realleftAcuI = {}
    realrightAcuI = {}
    realleftEcoP15 = {}
    realrightEcoP15 = {}

    realleftendttl = 0.0
    realrightendttl = 0.0
    realleftAcuIttl = 0.0
    realrightAcuIttl = 0.0
    realleftEcoP15ttl = 0.0
    realrightEcoP15ttl = 0.0

    realsummarystats = open(os.path.join(options.mappingDataDir,'covStatsSummary.csv'),'r')
    for l in realsummarystats:
        l.strip()
        w = l.split()
        stat = w[0].split(',')
        if stat[0] == 'leftend':
            realleftend[stat[1]] = float(w[1])
            realleftendttl += float(w[1])
            if not stat[1] in simleftend:
                simleftend[stat[1]] = 0.0
        elif stat[0] == 'rightend':
            realrightend[stat[1]] = float(w[1])
            realrightendttl += float(w[1])
            if not stat[1] in simrightend:
                simrightend[stat[1]] = 0.0
        elif stat[0] == 'leftAcuI':
            realleftAcuI[stat[1]] = float(w[1])
            realleftAcuIttl += float(w[1])
            if not stat[1] in simleftAcuI:
                simleftAcuI[stat[1]] = 0.0
        elif stat[0] == 'rightAcuI':
            realrightAcuI[stat[1]] = float(w[1])
            realrightAcuIttl += float(w[1])
            if not stat[1] in simrightAcuI:
                simrightAcuI[stat[1]] = 0.0
        elif stat[0] == 'leftEcoP15':
            realleftEcoP15[stat[1]] = float(w[1])
            realleftEcoP15ttl += float(w[1])
            if not stat[1] in simleftEcoP15:
                simleftEcoP15[stat[1]] = 0.0
        elif stat[0] == 'rightEcoP15':
            realrightEcoP15[stat[1]] = float(w[1])
            realrightEcoP15ttl += float(w[1])
            if not stat[1] in simrightEcoP15:
                simrightEcoP15[stat[1]] = 0.0

    realsummarystats.close()

    leftendadjust = -log((pseudocounts*len(simleftend.keys())+realleftendttl)/(pseudocounts*len(simleftend.keys())+simleftendttl))
    rightendadjust = -log((pseudocounts*len(simrightend.keys())+realrightendttl)/(pseudocounts*len(simrightend.keys())+simrightendttl))
    leftAcuIadjust = -log((pseudocounts*len(simleftAcuI.keys())+realleftAcuIttl)/(pseudocounts*len(simleftAcuI.keys())+simleftAcuIttl))
    rightAcuIadjust = -log((pseudocounts*len(simrightAcuI.keys())+realrightAcuIttl)/(pseudocounts*len(simrightAcuI.keys())+simrightAcuIttl))
    leftEcoP15adjust = -log((pseudocounts*len(simleftEcoP15.keys())+realleftEcoP15ttl)/(pseudocounts*len(simleftEcoP15.keys())+simleftEcoP15ttl))
    rightEcoP15adjust = -log((pseudocounts*len(simrightEcoP15.keys())+realrightEcoP15ttl)/(pseudocounts*len(simrightEcoP15.keys())+simrightEcoP15ttl))

    leftendratios = open(os.path.join(options.gapsDir,options.libID + '.leftend'),'w')
    for k in simleftend:
        if not k in realleftend:
            num = pseudocounts
        else:
            num = pseudocounts + realleftend[k]
        denom = pseudocounts + simleftend[k]
        print >> leftendratios, "leftend,%s %f" % (k,log(num/denom)+leftendadjust)
    leftendratios.close()

    rightendratios = open(os.path.join(options.gapsDir,options.libID + '.rightend'),'w')
    for k in simrightend:
        if not k in realrightend:
            num = pseudocounts
        else:
            num = pseudocounts + realrightend[k]
        denom = pseudocounts + simrightend[k]
        print >> rightendratios, "rightend,%s %f" % (k,log(num/denom)+rightendadjust)
    rightendratios.close()

    leftAcuIratios = open(os.path.join(options.gapsDir,options.libID + '.leftAcuI'),'w')
    for k in simleftAcuI:
        if not k in realleftAcuI:
            num = pseudocounts
        else:
            num = pseudocounts + realleftAcuI[k]
        denom = pseudocounts + simleftAcuI[k]
        print >> leftAcuIratios, "leftAcuI,%s %f" % (k,log(num/denom)+leftAcuIadjust)
    leftAcuIratios.close()

    rightAcuIratios = open(os.path.join(options.gapsDir,options.libID + '.rightAcuI'),'w')
    for k in simrightAcuI:
        if not k in realrightAcuI:
            num = pseudocounts
        else:
            num = pseudocounts + realrightAcuI[k]
        denom = pseudocounts + simrightAcuI[k]
        print >> rightAcuIratios, "rightAcuI,%s %f" % (k,log(num/denom)+rightAcuIadjust)
    rightAcuIratios.close()

    leftEcoP15ratios = open(os.path.join(options.gapsDir,options.libID + '.leftEcoP15'),'w')
    for k in simleftEcoP15:
        if not k in realleftEcoP15:
            num = pseudocounts
        else:
            num = pseudocounts + realleftEcoP15[k]
        denom = pseudocounts + simleftEcoP15[k]
        print >> leftEcoP15ratios, "leftEcoP15,%s %f" % (k,log(num/denom)+leftEcoP15adjust)
    leftEcoP15ratios.close()

    rightEcoP15ratios = open(os.path.join(options.gapsDir,options.libID + '.rightEcoP15'),'w')
    for k in simrightEcoP15:
        if not k in realrightEcoP15:
            num = pseudocounts
        else:
            num = pseudocounts + realrightEcoP15[k]
        denom = pseudocounts + simrightEcoP15[k]
        print >> rightEcoP15ratios, "rightEcoP15,%s %f" % (k,log(num/denom)+rightEcoP15adjust)
    rightEcoP15ratios.close()


if __name__ == '__main__':
    main()
