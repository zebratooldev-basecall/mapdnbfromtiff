#! /bin/bash

fn="$1"
prefix=knownXnew

for fn in "$@"; do
    thresh=${fn##*/Variations}
    thresh=${thresh%.tsv}
    echo $thresh
    awk -F"\t" '$7=="snp"||$7=="ins"||$7=="del"{if($12==""){known=0}else{known=1};print $7,$10,known}' \
        "$fn" | sort -k 1,1 -k 2,2n -k 3,3 | \
        uniq -c > ${prefix}.${thresh}
    for x in snp ins del; do
        echo ${prefix}.${thresh}.${x}
        awk '$2=="'$x'"{print}' <${prefix}.${thresh} | sort -k 3,3nr -k 4,4 | \
            awk 'BEGIN{x=0;y=0;score=0}$3!=score{print x,y;score=$3}$4==0{y+=$1}$4==1{x+=$1}END{print x,y}' >${prefix}.${thresh}.${x}
    done
done
