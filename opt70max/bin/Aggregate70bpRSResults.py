#!/usr/bin/python

"""

"""

__author__ = "Anushka Brownley"
__version__ = "$RCSId$"
__date__ = "$Date$"
__copyright__ = "Copyright (c) 2008 Complete Genomics"
__license__ = "Proprietary"

import os.path
import sys
import getopt
import shutil
from rsLookupMetrics import FieldMetrics, LaneMetrics, LookupTable
from datetime import datetime, date, time

# Env vars
CGIHome = os.environ['CGI_HOME']
CGIPath = os.environ['LD_LIBRARY_PATH']

# DNB vars
DNB_Len = 70
numRSCodes = 8

if os.environ.get('CGI_HOME') is None:
    print "Please set CGI_HOME."
    sys.exit(-4)

print "CGI Executables found in path " + CGIPath
from CGIPipeline import *

################################ 
def makeDir(dirPath):
    if not(os.path.isdir(dirPath)):
        os.mkdir(dirPath, 0775)

################################
def removeData(removeLST):
    for rmfile in removeLST:
        if os.path.isfile(rmfile):
            os.remove(rmfile)
        elif os.path.isdir(rmfile):
            os.rmdir(rmfile)

################################
def convertTo80bp(val):
        if val > 5:
            val +=5
        
        if val > 70:
            val +=5
        
        return val
        
################################ 
def usage(script):
    print "\nThis script takes an csv file sequencer run information and a template ADF"
    print "input XML file and outputs a modified ADF input XML file to run through make ADF\n"
    print script
    print "    -h, --help         <arg>   outputs this help menu"
    print "    -i, --input-dir    <arg>   input dir containing "
    print "    -s, --slideID      <arg>   slide ID "
    print "    -l, --laneID       <arg>   lane ID"
    print "    -a, --arr-size     <arg>   grid array size"
    print "    -o, --output-dir   <arg>   output directory\n"
    
################################

def main(argv):
    script = argv[0]
    curPath = os.path.dirname(os.path.abspath(script))
    argv = argv[1:]
    inputDir = None
    slideID = None
    laneID = None
    outputDir = None
    arrSize = None
    
    try:
        opts, args = getopt.getopt(argv, "h:i:s:l:a:o:", ["help=", "input-dir=", "slideID=", "laneID=", "arr-size=", "output-dir="])
    except getopt.GetoptError:
        usage(script)
        sys.exit(1)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(script)
            sys.exit(0)
        elif opt in ("-i", "--input-dir"):
            inputDir = arg
        elif opt in ("-s", "--slideID"):
            slideID = arg
        elif opt in ("-l", "--laneID"):
            laneID = arg
        elif opt in ("-a", "--arr-size"):
            arrSize = int(arg)
        elif opt in ("-o", "--output-dir"):
            outputDir = arg
            
    # validate input
    if inputDir is None or slideID is None or laneID is None or outputDir is None or arrSize is None:
        print "Please enter all required input parameters, input directory, slideID and laneID, grid array size, and output directory."
        usage(script)
        sys.exit(1)
    
    if not(os.path.isdir(inputDir) and os.path.isdir(outputDir)):
        print "Please enter valid input and output directories"
        usage(script)
        sys.exit(1)
    
    # load lookup table and ADF
    laneMetricsOBJ = LaneMetrics(DNB_Len, numRSCodes)
    
    cycleStr = ''
    masterDBFile = inputDir+"/"+slideID+"_"+laneID+".master_dbload.csv"
    if not(os.path.isfile(masterDBFile)):
        print "ERROR: master DB file doesn't exist " + masterDBFile
        sys.exit(1)
    
    dbFH = open(masterDBFile, "r")
    posCycMapDCT = {}        
    for line in dbFH:
        if line.find("RSAnalysisRun") > -1:
            continue
    
        (type,pos,adaptor,adPos,cycle,tenmer) = line.strip().split(",")
        posCycMapDCT[int(pos)] = cycle
    
    keys = posCycMapDCT.keys()
    keys.sort()
    cycleStr = "_".join(map(lambda x: posCycMapDCT[x], keys))
    for i in range(0,DNB_Len):
        pos = convertTo80bp(i)
        if pos not in posCycMapDCT.keys():
            posCycMapDCT[pos]=""
            
    keys = posCycMapDCT.keys()
    keys.sort()
    commentHeader = "Pos"+", Pos".join(["%s" % a for a in keys])
    commentData = ",".join(map(lambda x: posCycMapDCT[x], keys))
    if cycleStr == '':
        print "ERROR: Cycle Data not in master DB file: "+ masterDBFile
        sys.exit(1)
    
    # Set up output directories and files
    makeDir(outputDir)
    outputFile = outputDir + "/" + slideID + "_" + laneID + "_" + cycleStr + ".csv"
    outFH = open(outputFile, "w")
    laneMetricsOBJ.printHeader(outFH,commentHeader)
    fieldOutFile = outputDir + "/" + slideID + "_" + laneID + "_" + cycleStr + "_byField.csv"
    fieldOutFH = open(fieldOutFile, "w")
    laneMetricsOBJ.printHeader(fieldOutFH,commentHeader)
    fieldCount = 0
    removeLST = []
    for i in range(1, arrSize+1):
        print "Processing part " + str(i)
        
        # extra array-specific lane files
        laneDBFile = outputDir + "/" + slideID + "_" + laneID + "." + str(i) + ".dbload.csv"
        removeLST.append(laneDBFile)
        
        # copy field data into global field output file
        fieldFile = inputDir + "/" + slideID + "_" + laneID + "_" + cycleStr + "_byField." + str(i) + ".csv"
        if not os.path.isfile(fieldFile):
            print "ERROR, field subset data file not created: " + fieldFile
            sys.exit(1)
            
        inFieldFH = open(fieldFile, "r")
        header = inFieldFH.readline()
        for line in inFieldFH:
            fieldCount += 1
            fieldOutFH.write(line)
        
        inFieldFH.close()
        removeLST.append(fieldFile)
        aggrFile = inputDir + "/" + slideID + "_" + laneID + "_" + cycleStr + "." + str(i) + ".csv"
        removeLST.append(aggrFile)
        
        # read raw data in temp file to aggregate for lane level stats
        tempFile = inputDir + "/" + slideID + "_" + laneID + "_" + cycleStr + "." + str(i) + ".temp.csv"
        if not os.path.isfile(tempFile):
            print "ERROR, subset raw data file not created: " + tempFile
            sys.exit(1)
            
        tempFH = open(tempFile, "r")
        laneMetricsOBJ.readLaneData(tempFH)
        tempFH.close()
        removeLST.append(tempFile)
        
        
    # print lane level metrics 
    laneDBFile = outputDir + "/" + slideID + "_" + laneID + ".dbload.csv"
    laneDBFH = open(laneDBFile, "w")
    laneMetricsOBJ.printMetrics(outFH,curPath,slideID,laneID,fieldCount,1,1,cycleStr,laneDBFH,commentData)
    laneDBFH.close()
    outFH.close()
    fieldOutFH.close()
    removeData(removeLST)
    
################################
if __name__ == "__main__":
    main(sys.argv)
