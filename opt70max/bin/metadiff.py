#! /usr/bin/env python

import os, re, sys
from glob import glob
from os.path import join as pjoin
from subprocess import Popen, PIPE


def findFilesVisitDir(fns, dirname, fnames):
    for fname in fnames:
        fn = pjoin(dirname, fname)
        if not os.path.isdir(fn):
            fns.append(fn)


def findFiles(path):
    fns = []
    os.path.walk(path, findFilesVisitDir, fns)
    return fns


def diffSortedFnRel(expA, expB):
    result = []
    ii = 0
    jj = 0
    fnA = expA.fn
    fnB = expB.fn
    while ii < len(fnA) or jj < len(fnB):
        if ii == len(fnA):
            result.append( (None, fnB[jj]) )
            jj += 1
            continue
        if jj == len(fnB):
            result.append( (fnA[ii], None) )
            ii += 1
            continue
        if fnA[ii][0] < fnB[jj][0]:
            result.append( (fnA[ii], None) )
            ii += 1
            continue
        if fnB[jj][0] < fnA[ii][0]:
            result.append( (None, fnB[jj]) )
            jj += 1
            continue
        result.append( (fnA[ii], fnB[jj]) )
        ii += 1
        jj += 1
    return result


def printFileDiffs(fileDiffs, expA, expB):
    print 'Files in %s only:' % expA.path
    for (fnA, fnB) in fileDiffs:
        if fnB is None:
            print fnA[0]
    print

    print 'Files in %s only:' % expB.path
    for (fnA, fnB) in fileDiffs:
        if fnA is None:
            print fnB[0]
    print


def catCmd(fn):
    if fn.endswith('.gz'):
        return 'zcat'
    if fn.endswith('.bz2'):
        return 'bzcat'
    return 'cat'


def bzopen(fn):
    return Popen([catCmd(fn), fn], stdout=PIPE).stdout


def countMetadataLines(fn):
    ff = bzopen(fn)
    count = 1
    for line in ff:
        if line.startswith('>'):
            break
        count += 1
        if count >= 100:
            return count
    return count


def printFileMetadataDiffs(fileDiffs, expA, expB):
    ignoreKeys = [ 'ASSEMBLY_ID', 'FORMAT_VERSION', 'GENERATED_AT', 'SOFTWARE_VERSION' ]
    ignoreRes = [ re.compile('#%s\t.*\\|\t#%s\t.*' % (key,key)) for key in ignoreKeys ]
    print 'File metadata differences:'
    for (fnA, fnB) in fileDiffs:
        if fnA is None or fnB is None:
            continue
        cmd = ( ('diff --side-by-side --suppress-common-lines <(%s %s | head -n %d) '+
                 '<(%s %s | head -n %d)') %
                (catCmd(fnA[2]), fnA[2], countMetadataLines(fnA[2]),
                 catCmd(fnB[2]), fnB[2], countMetadataLines(fnB[2])) )
        pipe = Popen("bash -c '%s'" % cmd, shell=True, bufsize=8192, stdout=PIPE).stdout
        printCmd = True
        for line in pipe:
            ignore = False
            for ignoreRe in ignoreRes:
                if ignoreRe.match(line):
                    ignore = True
            if ignore:
                continue
            if printCmd:
                print cmd
                printCmd = False
            print line,
        if not printCmd:
            print
        pipe.close()


class ExpFiles:
    def __init__(self, path):
        self.path = path
        sums = glob(pjoin(path, 'ASM', 'summary-*.tsv'))
        if len(sums) != 1:
            raise Exception('failed to determine asm id')
        m = re.match('.*summary-(.*)\\.tsv', sums[0])
        self.asmId = m.group(1)
        self.fnFull = sorted(findFiles(path))
        pathLen = len(pjoin(path, ''))
        self.fnrel = [ fn[pathLen:] for fn in self.fnFull ]
        self.fnrelSimple = [ fn.replace(self.asmId, 'ASMID') for fn in self.fnrel ]
        self.fn = sorted(zip(self.fnrelSimple, self.fnrel, self.fnFull))


expA = ExpFiles(sys.argv[1])
expB = ExpFiles(sys.argv[2])
fileDiffs = diffSortedFnRel(expA, expB)
printFileDiffs(fileDiffs, expA, expB)
printFileMetadataDiffs(fileDiffs, expA, expB)
