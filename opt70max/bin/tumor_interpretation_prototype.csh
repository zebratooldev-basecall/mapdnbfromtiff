#!/bin/tcsh -fx

### sample ID
set tumorasm=$1
set normalasm=$2
set workDir=$3
set projRootDir=$4
set refDir=$5

### set to 1 if first time computes; 0 if redoing a broken run from the point of breakage
set firstTime=1

set smp=`echo $tumorasm | awk -F_ '{print $1 "_" $2}'`

if ( ! -e ${workDir} ) mkdir $workDir

### masterVar file
set mv=`echo ${projRootDir}/${normalasm}/ASM/masterVar*`

### somatic non-diploid CNV details 
set cnvdet=`echo ${projRootDir}/${tumorasm}/ASM/CNV/somaticCnvDetailsNondiploid*bz2`

### sample suffix
set smpsfx=`echo $cnvdet | awk -F- '{print $(NF-1)}'`

### determine columns in masterVar containing attributes of interest
bzip2 -dc $mv | head -30 > ${workDir}/mv.head
set normalTtlReadCount=`awk -F'\t' '$1~/>/{for(i=1;i<=NF;i++)if($i=="totalReadCount"){print i;exit}}' ${workDir}/mv.head`
set tumorAllele1ReadCount=`awk -v colname="allele1ReadCount-$smpsfx" -F'\t' '$1~/>/{for(i=1;i<=NF;i++)if($i==colname){print i;exit}}' ${workDir}/mv.head`
set variantQuality=`awk -v colname="allele1VarQuality" -F'\t' '$1~/>/{for(i=1;i<=NF;i++)if($i==colname){print i;exit}}' ${workDir}/mv.head`

if ( $firstTime ) then 

### extract read counts for 100k windows
bzip2 -dc $mv |\
    awk -F'\t' -v trc=$normalTtlReadCount -v a1rc=$tumorAllele1ReadCount -v vq=$variantQuality \
	'$1~/>/{ok=1;next}\
	ok==1&&$6=="het-ref"&&$7=="snp"&&$trc>=10&&$trc<=300&&$vq=="VQHIGH"&&$(vq+1)=="VQHIGH"{print $3,$4,$5,$a1rc,$(a1rc+1)}' |\
    awk -v OFS="\t" \
	'$1!=prevChr||int($2/100000)!=prevWin{if(NR>1)print prevChr "_" prevWin*100000,counts;prevChr=$1;prevWin=int($2/100000);counts=""}\
	{if(counts != "")counts = counts ";";counts = counts $4 "/" $5}END{print prevChr "_" prevWin*100000,counts}' |\
    sort -k 1,1 \
  > ${workDir}/${smp}_alleleCounts.tsv

### reformat CNV details and join with read counts
echo ">chr\tbegin\tend\tavgNormalizedCvg\tgcCorrectedCvg\tfractionUnique\trelativeCvg\tcalledPloidy\tcalledCNVType\tploidyScore\tCNVTypeScore\tbestLAF\tlowLAF\thighLAF\talleleCounts" > ${workDir}/header
bzip2 -dc $cnvdet | awk -v OFS="\t" '{$1 = $1 "_" $2;$2="";print}' | sort -k 1,1 | join -1 1 -2 1 - ${workDir}/${smp}_alleleCounts.tsv |\
  tr '[ _]' '\t' |\
  sort -k 1,1 -k 2,2n |\
  cat ${workDir}/header - \
 > ${workDir}/${smp}.inputs

endif

### run segmentation
optSegPoisson --allele-freq-granularity 50 -u -c 10 -m .05 -s 100 -o ${workDir}/${smp}.optseg10_p0025 -i ${workDir}/${smp}.inputs --beta-binom-vstar 0.0025


### relcvg in whole-genome coordinates for whole-genome plot
cat ${workDir}/${smp}.inputs |\
  awk -F'\t' 'NR>1{print $1,$2,$7}' |\
  cat ${refDir}/CovModel/chroffsets - |\
  awk 'NF==2{o[$1]=$2;next}{print $2+o[$1],$3}' \
 > ${workDir}/${smp}.relcvg




### 2D histogram from optseg results, in gnuplot "pm3d" format
set numwins=`cat ${workDir}/${smp}.inputs | wc -l`
set median=`awk -F'\t' '{print $4}' ${workDir}/${smp}.inputs | sort -n | awk -v nw=$numwins 'NR>nw/2{print $1;exit}'`
cat ${workDir}/${smp}.optseg10_p0025 |\
  awk -F'\t' -v med=$median '{print $3/(10*med),$5}' |\
  awk '{x=int($1*100+.1);y=$2;c[x*50+y]++}END{for(i=1;i<=500;i++)for(j=1;j<=50;j++)print i/100,j/100,c[i*50+j]*1}'  |\
  awk '{x=$3;if(x>50)x=50;print $1,$2,x;if($2==0.5){print $1,.51,x;print ""}}' \
 > ${workDir}/${smp}.pm3d


gnuplot <<EOF
min(x,y) = (x < y) ? x : y
set terminal postscript color
set output "${workDir}/${smp}_genomeplot.ps"
#set multiplot
set size 1,.5
set origin 0,0.5
set xlabel "genome position"
set ylabel "relative cvg"
set yrange [0:3]
plot "${workDir}/${smp}.relcvg" title ""
set output "${workDir}/${smp}_cvgXlaf.ps"
set origin 0,0.5
set size 1,.5
set origin 0,0.5
set xlabel "relative coverage"
set ylabel "LAF"
set xrange [:3]
set yrange [0:.52]
set pm3d map
#splot "${workDir}/${smp}.pm3d" using 1:2:(log(\$3+1)) title "$smp          "
splot "${workDir}/${smp}.pm3d" title "$smp          "
EOF

echo quit | gs -q -sDEVICE=png256 -r300x300 -sOutputFile=${workDir}/${smp}_genomeplot.png ${workDir}/${smp}_genomeplot.ps
echo quit | gs -q -sDEVICE=png256 -r300x300 -sOutputFile=${workDir}/${smp}_cvgXlaf.png ${workDir}/${smp}_cvgXlaf.ps



cat ${workDir}/${smp}.optseg10_p0025 | awk -v med=$median '{n++}NF>5{if(n>20)print $4/(med*10),$NF/100*$4/(med*10),n;n=0}'  > ${workDir}/${smp}.Rinputs

set RdensityCutoff=2000

R --no-save --slave --quiet <<EOF 
  library(spatstat)
  library(stats)
  source("${CGI_HOME}/bin/tumorInterpretationPrototype.R")
  modelTumorFromOptSegSegs("$workDir","$smp",$RdensityCutoff)
EOF


