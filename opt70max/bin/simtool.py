#! /usr/bin/env python

import os, sys, re, subprocess
from optparse import OptionParser
from os.path import split as psplit, join as pjoin
from bisect import bisect
from glob import glob


class SimCase:
    def __init__(self,fields):
        self.name      = fields[0]
        self.root      = pjoin(os.getcwd(), self.name)
        self.overrides = fields[1]

    def initDirectories(self, runId, description):
        if not os.path.isdir(self.name):
            os.mkdir(self.name)

        os.mkdir(pjoin(self.name, runId))
        ff = open(pjoin(self.name, runId, 'description.txt'), 'w')
        ff.write('%s\n' % description)
        ff.close()

    def run(self, runId, options):
        xtraArgs = []
        if options.server is not None:
            xtraArgs += [ '-s', options.server ]
        retcode = subprocess.call(['rat',
                                   'create',
                                   '%s-%s' % (runId,self.name),
                                   'simulate.py',
                                   '--class=OneStep',
                                   '-x',
                                   self.overrides,
                                   '-w',
                                   pjoin(self.root, runId)] +
                                  xtraArgs)
        if retcode != 0:
            raise Exception( 'rat create for %s %s returned error: %d' % (self.name, runId, retcode) )

    def runClone(self, runId, options, beginStep, origRunId):
        xtraArgs = []
        if options.server is not None:
            xtraArgs += [ '-s', options.server ]
        pp = subprocess.Popen(['rat',
                               'create',
                               '%s-%s' % (runId,self.name),
                               'simulate.py',
                               '--class=OneStep',
                               '--clone=%s' % pjoin(self.root, origRunId),
                               '-b',
                               beginStep,
                               '-w',
                               pjoin(self.root, runId)] +
                              xtraArgs,
                              stdout=subprocess.PIPE)
        cloneText = pp.communicate()[0]
        print cloneText,
        retcode = pp.wait()

        if retcode != 0:
            raise Exception( 'rat clone for %s %s returned error: %d' % (self.name, runId, retcode) )


class SimConfig:
    def __init__(self,fn):
        self.fn = fn
        self.simcases = []
        ff = open(fn)
        try:
            for line in ff:
                line = line.strip()
                if line.startswith('#'):
                    continue
                if line == '':
                    continue
                fields = line.split(None, 1)
                self.simcases.append(SimCase(fields))
                self.validate()
        finally:
            ff.close()

    def validate(self):
        names = {}
        for simcase in self.simcases:
            if simcase.name in names:
                raise Exception('simcase specified twice: %s' % simcase.name)
            names[simcase.name] = 1


def getLastRunId(simcase):
    idre = re.compile(r't(\d+)')
    id = -1
    if os.path.isdir(simcase.root):
        for fn in os.listdir(simcase.root):
            match = idre.match(fn)
            if match:
                id = max(id, int(match.group(1)))
    return id


def newRunId(simconfig):
    id = -1
    for simcase in simconfig.simcases:
        id = max(id, getLastRunId(simcase))
    return 't%02d' % (id+1)


def doRun(args):
    parser = OptionParser('usage: simtool.py run [options]')
    parser.disable_interspersed_args()
    parser.add_option('-c', '--config', default='sim.conf',
                      help='Path to config file.')
    parser.add_option('-d', '--description', default='',
                      help='Short description of this run.')
    parser.add_option('-b', '--begin', default=None,
                      help='Where to start from (a Step in the workflow).')
    parser.add_option('-r', '--run', default=None,
                      help='The run to use as a staring point (when begin param is used).')
    parser.add_option('-s', '--server', default=None,
                      help='host:port of the scheduler node')

    (options, args) = parser.parse_args(args)

    if len(args) != 0:
        raise Exception('unexpected args')

    simconfig = SimConfig(options.config)
    runId = newRunId(simconfig)

    print 'Running with run id: %s' % runId
    for simcase in simconfig.simcases:
        simcase.initDirectories(runId, options.description)
    if options.begin is None:
        for simcase in simconfig.simcases:
            simcase.run(runId, options)
    else:
        origRunId = options.run
        if origRunId is None:
            origRunId = 't%02d' % (int(runId[1:])-1)
        for simcase in simconfig.simcases:
            simcase.runClone(runId, options, options.begin, origRunId)


def getStatsHeader(simcases, runs):
    header = None
    for run in runs:
        for simcase in simcases:
            fn = pjoin(simcase.root, run, 'calldiff', 'asmstats.csv')
            if os.path.exists(fn):
                ff = open(fn)
                line = ff.readline().strip()
                ff.close()
                if header is None:
                    header = line
                elif header != line:
                    raise 'mismatched header lines in asmstats.csv'
    return [ 'description', 'run', 'case' ] + header.split(',')


def getStatsRecords(header, simcases, runs):
    recs = []
    for run in runs:
        for simcase in simcases:
            description = ''
            dfn = pjoin(simcase.root, run, 'description.txt')
            if os.path.exists(dfn):
                dff = open(dfn)
                description = dff.readline().strip()
                dff.close()
            fn = pjoin(simcase.root, run, 'calldiff', 'asmstats.csv')
            rec = [ '' ] * len(header)
            if os.path.exists(fn):
                ff = open(fn)
                ignore = ff.readline().strip()
                rec = ff.readline().strip().split(',')
                ff.close()
            recs.append( [ description, run, simcase.name ] + rec )
    return recs


def doAsmStats(args):
    parser = OptionParser('usage: simtool.py stats [options]')
    parser.disable_interspersed_args()
    parser.add_option('-c', '--config', default='sim.conf',
                      help='Path to config file.')
    parser.add_option('-r', '--run', default=None,
                      help='Comma-separated list of run ids to report stats for.')

    (options, args) = parser.parse_args(args)

    if len(args) != 0:
        raise Exception('unexpected args')

    simconfig = SimConfig(options.config)
    if options.run is None:
        runs = [ 't%02d' % max([getLastRunId(simcase) for simcase in simconfig.simcases]) ]
    elif options.run == 'all':
        runIdInts = range( 0, 1+max([getLastRunId(simcase) for simcase in simconfig.simcases]) )
        runs = [ 't%02d' % id for id in runIdInts ]
    else:
        runs = options.run.split(',')

    header = getStatsHeader(simconfig.simcases, runs)
    recs   = getStatsRecords(header, simconfig.simcases, runs)
    print ','.join(header)
    for rec in recs:
        print ','.join(rec)


def doNbStats(args):
    parser = OptionParser('usage: simtool.py stats [options]')
    parser.disable_interspersed_args()
    parser.add_option('-c', '--config', default='sim.conf',
                      help='Path to config file.')
    parser.add_option('-r', '--run', default=None,
                      help='Comma-separated list of run ids to report stats for.')

    (options, args) = parser.parse_args(args)

    if len(args) != 0:
        raise Exception('unexpected args')

    simconfig = SimConfig(options.config)
    if options.run is None:
        runs = [ 't%02d' % max([getLastRunId(simcase) for simcase in simconfig.simcases]) ]
    elif options.run == 'all':
        runIdInts = range( 0, 1+max([getLastRunId(simcase) for simcase in simconfig.simcases]) )
        runs = [ 't%02d' % id for id in runIdInts ]
    else:
        runs = options.run.split(',')

    header = getStatsHeader(simconfig.simcases, runs)
    recs   = getStatsRecords(header, simconfig.simcases, runs)
    print ','.join(header[0:11])
    for rec in recs:
        print ','.join(rec[0:11])


COMMANDS = { 'run' : doRun,
             'asmstats' : doAsmStats,
             'nbstats' : doNbStats }

def main():
    parser = OptionParser('usage: %prog [options]')
    parser.disable_interspersed_args()

    (options, args) = parser.parse_args(sys.argv[1:])

    if len(args) == 0 or args[0] not in COMMANDS:
        print 'usage: simtool.py <cmd> <options>'
        print 'Commands available:'
        for cmd in COMMANDS:
            print '  %s' % cmd
        raise Exception('bad args')
    COMMANDS[args[0]](args[1:])


if __name__ == '__main__':
    main()
