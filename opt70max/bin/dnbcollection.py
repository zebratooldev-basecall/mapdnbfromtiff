import os
import re
from xml.parsers import expat

from driverlib import Error

class Element(object):
    def __init__(self, name, attributes):
        self.name = name
        self.attributes = attributes
        self.cdata = ''
        self.children = []
    def addChild(self, element):
        self.children.append(element)
    def getAttribute(self,key):
        return self.attributes.get(key)
    def getData(self):
        return self.cdata
    def getElementsByTagName(self, name):
        lst = []
        self._findChildrenByName(name, lst)
        return lst
    def _findChildrenByName(self, name, lst):
        lst.extend([c for c in self.children if c.name == name])
        for c in self.children:
            c._findChildrenByName(name, lst)

class Xml2Obj(object):
    def __init__(self):
        self.root = None
        self.nodeStack = []
    def StartElement(self, name, attributes):
        element = Element(name.encode(), attributes)
        if self.nodeStack:
            parent = self.nodeStack[-1]
            parent.addChild(element)
        else:
            self.root = element
        self.nodeStack.append(element)
    def EndElement(self, name):
        self.nodeStack.pop()
    def CharacterData(self,data):
        if data.strip():
            data = data.encode()
            element = self.nodeStack[-1]
            element.cdata += data
    def Parse(self, filename):
        Parser = expat.ParserCreate()
        Parser.StartElementHandler = self.StartElement
        Parser.EndElementHandler = self.EndElement
        Parser.CharacterDataHandler = self.CharacterData
        ParserStatus = Parser.Parse(open(filename).read(),1)
        return self.root

class Lane(object):
    def __init__(self, collection, name, sample, cycleMap):
        self.collection = collection
        self.name = name
        self.sample = sample
        self.cycleMap = cycleMap
        self.library = None # TBD when library is constructed


    def __cmp__(self, other):
        if self.library != other.library:
            return cmp(self.library, other.library)
        else:
            return cmp(self.name, other.name)

class Library(object):
    def __init__(self, collection, name, dnbArchitecture, lanes):
        assert lanes

        self.collection = collection
        self.name = name
        self.dnbArchitecture = dnbArchitecture
        self.lanes = lanes
        self.sample = lanes[0].sample
        for lane in lanes:
            lane.library = self
            if lane.sample != self.sample:
                raise Error('lane %s is not from sample %s' % (lane.name, self.sample))

    def __cmp__(self, other):
        return cmp(self.name, other.name)

def _getLatestCollection(file):
    (path, name) = os.path.split(file)
    if not name:
        raise Error('bad collection file name: ' + file)
    if not path:
        path = '.'
    files = os.listdir(path)
    if name in files:
        return file
    base = name.split('.')[0]
    if not base:
        raise Error('bad collection file name: ' + file)

    lastIndex = -1
    lastFile = None
    for f in files:
        if f.startswith(base):
            m = re.match(r'.*\,([\d]+)\.xml', f)
            if m and m.lastindex == 1:
                index = int(m.groups()[0])
                if index > lastIndex:
                    lastIndex = index
                    lastFile = f
            elif re.match(r'.*\.xml', f) and not lastFile:
                lastFile = f

    if not lastFile:
        raise Error('bad collection file name: ' + file)

    return os.path.join(path, lastFile)

class Collection(object):
    def __init__(self, file):
        fn = _getLatestCollection(file)

        parser = Xml2Obj()
        doc = parser.Parse(fn)

        libById = {}
        archByLibName = {}
        for elib in doc.getElementsByTagName('Library'):
            id = str(elib.getAttribute('id'))
            name = str(elib.getAttribute('name'))
            reads = []
            for eread in elib.getElementsByTagName('Read'):
                reads.append(int(eread.getAttribute('length')))
            archByLibName[name] = self.getDnbArchitecture(reads)
            libById[id] = name

        libmap = {}
        for eslide in doc.getElementsByTagName('Slide'):
            slide = str(eslide.getAttribute('name'))
            for elane in eslide.getElementsByTagName('Lane'):
                libId = str(elane.getAttribute('library'))
                lib = libById[libId]
                name = str(elane.getAttribute('name'))
                sample = str(elane.getAttribute('sample'))

                cycleMap = {}
                cycleNo = 1
                for ecycle in elane.getElementsByTagName('cycle'):
                    position = str(ecycle.getAttribute('position'))
                    if position:
                        cycleMap[int(position)] = cycleNo
                    cycleNo += 1

                name = slide + '-' + name

                if not lib in libmap:
                    libmap[lib] = []

                libmap[lib].append(Lane(self, name, sample, cycleMap))

        self.lanes=[]
        self.libraries=[]

        for (name, lanes) in libmap.iteritems():
            if len(lanes) != 0:
                self.libraries.append(Library(self, name, archByLibName[name], lanes))
                self.lanes.extend(lanes)

        self.lanes.sort()
        self.libraries.sort()

    def getDnbArchitecture(self, reads):
        if reads == [ 5, 10, 10, 10, 10, 10, 10, 5 ]:
            return 'ad4_35_35'
        elif reads == [ 12, 19, 12, 19 ]:
            return 'ad2_31_31'
        elif reads == [ 10, 10, 10, 10, 10, 10 ]:
            return 'ad2_29_29'
        elif reads == [ 10, 5, 10, 10, 5, 10 ]:
            return 'ad2_25_25'
        elif reads == [ 10, 10, 10, 10 ]:
            return 'ad1_10_28'
        elif reads == [ 12, 10, 10 ]:
            return 'ad1_12_19'
        else:
            # commented out the throwing of an error. BSG
            #raise Error('unable to determing DNB architecture from read lengths: '+str(reads))
            return 'unknown'
