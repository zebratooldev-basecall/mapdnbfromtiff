#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
### Py2.7 encode fix
reload(sys)
sys.setdefaultencoding('utf8')

import copy
from string import maketrans
import re
import time
import string
import json

###### Document Decription
'''  '''

###### Version and Date
prog_version = '1.0.2'
prog_date = '2015/02/27'

###### Usage
usage = '''

     Version %s  by Vincent-Li  %s
g
     Usage: %s <samFile> <prefix> <outfile> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class samStat(object):
    """docstring for samStat"""
    
    def __init__(self, file, prefix):
        # super(samStat, self).__init__()
        self.file = file
        self.prefix = prefix
        
        self.init()

        self.idx = [0, 0]
        self.figureDataBuffer = {}

        self.mapFigure = {}

    def init(self):
        self.misDist = []
        self.gapDist = []
        self.misNum = {}
        self.gapNum = {}
        self.varType = []
       
        self.readLen = 0

    def drawVarRateDist(self):
        xlabels = map(str, xrange(1, self.readLen+1))
        data = {'Mismatch':[], 'Gap':[]}
        if self.MappedReads != 0:
            data['Mismatch'] += [round(float(x)/self.MappedReads*100,4) for x in self.misDist]
            data['Gap'] += [round(float(x)/self.MappedReads*100,4) for x in self.gapDist]
        else:
            data['Mismatch'] += [0.0 for x in self.misDist]
            data['Gap'] += [0.0 for x in self.gapDist]

        self.mapFigure['varRateDist'] = data

    def drawVarRateDistPE(self, title):
        xlabels = map(str, xrange(1, self.readLen+1))
        data = {'Mismatch':[], 'Gap':[]}
        if self.MappedReads != 0:
            data['Mismatch'] += [round(float(x)/self.MappedReads*100,4) for x in self.misDist]
            data['Gap'] += [round(float(x)/self.MappedReads*100,4) for x in self.gapDist]
        else:
            data['Mismatch'] += [0.0 for x in self.misDist]
            data['Gap'] += [0.0 for x in self.gapDist]

        return data
        
    def drawMisTypeDist(self):
        xlabels = map(str, xrange(1, self.readLen+1))
        data = self.varType

        for i in xrange(len(data)):
            for j in xrange(1, len(data[i])):
                try:
                    data[i][j] = round(float(data[i][j] )/self.MappedReads*100, 4)
                except ZeroDivisionError:
                    data[i][j] = 0.0
        misTypeDict = {}
        for t in data:
            misTypeDict[t[0]] = t[1:]
        self.mapFigure['misTypeDist'] = misTypeDict

    def drawMisTypeDistPE(self, title):
        xlabels = map(str, xrange(1, self.readLen+1))
        data = self.varType

        for i in xrange(len(data)):
            for j in xrange(1, len(data[i])):
                try:
                    data[i][j] = round(float(data[i][j] )/self.MappedReads*100, 4)
                except ZeroDivisionError:
                    data[i][j] = 0.0
        misTypeDict = {}
        for t in data:
            misTypeDict[t[0]] = t[1:]

        return misTypeDict

    def drawNumCountBar(self, infoDict, name):
        keys = sorted(infoDict.keys())
        xlabels = map(str, keys)
        data = []
        try:
            data = [ round(float(infoDict[x])/self.MappedReads*100, 2) for x in keys ]
        except ZeroDivisionError:
            data = [ [ 0.0 for x in keys ]]

        if 'numCount' not in self.mapFigure:
            self.mapFigure['numCount'] = {}
        
        self.mapFigure['numCount']['%s' % name] = data
        
    def drawNumCountBarPE(self, infoDict):
        keys = sorted(infoDict.keys())
        xlabels = map(str, keys)
        data = []
        try:
            data = [ round(float(infoDict[x])/self.MappedReads*100, 2) for x in keys ]
        except ZeroDivisionError:
            data = [ [ 0.0 for x in keys ]]

        return data

    def drawInsertSize(self):

        # 1. Normalize the insert size
        if self.insertDict:
            d = self.insertDict
            total = sum(map(int, [d[k] for k in d]))
            for k in d:
                d[k] = round(float(d[k])*100/total, 2)
            
            # 2. Remove zero values
            newDict = {k: d[k] for k in d if d[k] > 0}
            data = {}
            tmpList = []
            
            # Add judgement for inertSize
            insertSizeThre = 1000
            if not newDict or (max(newDict.keys()) - min(newDict.keys())) > insertSizeThre:
                return None

            for k in xrange(1, max(newDict.keys())):
                if k in newDict:
                    tmpList.append(newDict[k])
                else:
                    tmpList.append(0)
            data['insertSize'] = tmpList
            return data
        else:
            return None

    def createReportPara(self):
        self.htmlPara = {}

        self.htmlPara['mapBasic'] = []
        fhIn = open(self.file, 'r')
        
        ### Get the basic mapping values.
        fhIn.readline()
        for i in xrange(9):
            line = fhIn.readline()
            tmp = line.strip().split()
            if i == 0:
                self.readLen = int(tmp[1])
                continue
            if i == 1:
                tmp = [tmp[0], tmp[1], '-', '-']

            self.htmlPara['mapBasic'].append(tmp)

        self.MappedReads = int(self.htmlPara['mapBasic'][1][1])

        #line = fhIn.readline()
        #tmp = line.strip().split()
        #dupRow = [tmp[0], '-', '-', tmp[1]]
        #self.htmlPara['dupRate'] = tmp[1]

        ### Get the bar plot's values.
        line = fhIn.readline()
        tmp = line.strip().split()
        for j in xrange(1, len(tmp)):
            self.misNum[j-1] = int(tmp[j])

        line = fhIn.readline()
        tmp = line.strip().split()
        for j in xrange(1, len(tmp)):
            self.gapNum[j-1] = int(tmp[j])

        line = fhIn.readline()
        tmp = line.strip().split()
        row = [tmp[0], '-', '-', tmp[1]]
        self.htmlPara['mapBasic'].append(row)


        #if self.htmlPara['dupRate'] != '0.00':
        #    self.htmlPara['mapBasic'].append(dupRow)

        ### Get the misDist and gapDist in var plot.
        line = fhIn.readline()
        tmp = line.strip().split()
        for i in xrange(1, len(tmp)):  
            self.misDist.append(int(tmp[i]))

        line = fhIn.readline()
        tmp = line.strip().split()
        for i in xrange(1, len(tmp)):  
            self.gapDist.append(int(tmp[i]))
           
        ### Get the var Type and num.
        line = fhIn.readline()
        while line:
            tmp = line.strip().split()
            t = tmp[0][-4:]
            if not t.endswith('N'):
                d = []
                d.append(t)
                for i in xrange(1, len(tmp)):
                    d.append(int(tmp[i]))
                self.varType.append(d)

            line = fhIn.readline()

        ### Process the mapBasic have up to two digits after the decimal point
        for i in xrange(len(self.htmlPara['mapBasic'])):
            if self.htmlPara['mapBasic'][i][0] == 'DuplicationRate':
                continue
            for j in xrange(len(self.htmlPara['mapBasic'][i])):
                if re.search(r'\.', self.htmlPara['mapBasic'][i][j]):
                    self.htmlPara['mapBasic'][i][j] = round(float(self.htmlPara['mapBasic'][i][j])*100, 2)


        # x_label
        x_labels = range(0, int(self.readLen)+1)
        self.x_labels = range(1, int(self.readLen)+1)
        for i in x_labels:
            if i not in x_labels[::5]:
                self.x_labels[i-1] = ''
        self.x_labels[0] = 1
        self.x_labels = map(str, self.x_labels)

        self.mapTable = self.htmlPara['mapBasic']
        ### plot
        self.idx[1] += 1
        self.htmlPara['basicInfoTableTitle'] = "Tab%d. Basic statistics of ReadsMapping" % self.idx[1]
        self.htmlPara['varDistSvg'] = self.drawVarRateDist()
        self.htmlPara['mismatchTypeSvg'] = self.drawMisTypeDist()
        self.htmlPara['misCountBarSvg'] = self.drawNumCountBar(self.misNum, 'Mismatch')
        self.htmlPara['gapCountBarSvg'] = self.drawNumCountBar(self.gapNum, 'Gap')
        self.mapFigure['numCount']['xTag'] = ['%d' % d for d in xrange(len(self.mapFigure['numCount']['Mismatch']))]

        # Make sure that Mismatch and Gap had same lenght
        self.mapFigure['numCount']['Mismatch'] = self.setListLength(self.mapFigure['numCount']['Mismatch'], len(self.mapFigure['numCount']['xTag']))
        self.mapFigure['numCount']['Gap'] = self.setListLength(self.mapFigure['numCount']['Gap'], len(self.mapFigure['numCount']['xTag']))

        # Filter that mismatch number > 3
        if len(self.mapFigure['numCount']['xTag']) > 4:
            tmpDict = {}
            for k in ['Mismatch', 'Gap']:
                tmpDict[k] = self.mapFigure['numCount'][k][:3]
                tmpDict[k].append(str(sum(map(float, self.mapFigure['numCount'][k][3:]))))
            tmpDict['xTag'] = self.mapFigure['numCount']['xTag'][:3]
            tmpDict['xTag'].append('>3')
            self.mapFigure['numCount'] = tmpDict

        self.htmlPara['readLen'] = self.readLen
        return self.htmlPara

    def createReportParaFromStr(self, mapInfoStr):
        self.htmlPara = {}
        self.htmlPara['mapBasic'] = []
        
        fhIn = iter(mapInfoStr.strip().split('\n'))
        ### Get the basic mapping values.
        fhIn.next()
        for i in xrange(9):
            line = fhIn.next()
            tmp = line.strip().split()
            if i == 0:
                self.readLen = int(tmp[1])
                continue
            if i == 1:
                tmp = [tmp[0], tmp[1], '-', '-']

            self.htmlPara['mapBasic'].append(tmp)

        self.MappedReads = int(self.htmlPara['mapBasic'][1][1])

        #line = fhIn.readline()
        #tmp = line.strip().split()
        #dupRow = [tmp[0], '-', '-', tmp[1]]
        #self.htmlPara['dupRate'] = tmp[1]

        ### Get the bar plot's values.
        line = fhIn.next()
        tmp = line.strip().split()
        for j in xrange(1, len(tmp)):
            self.misNum[j-1] = int(tmp[j])

        line = fhIn.next()
        tmp = line.strip().split()
        for j in xrange(1, len(tmp)):
            self.gapNum[j-1] = int(tmp[j])

        line = fhIn.next()
        tmp = line.strip().split()
        row = [tmp[0], '-', '-', tmp[1]]
        self.htmlPara['mapBasic'].append(row)


        #if self.htmlPara['dupRate'] != '0.00':
        #    self.htmlPara['mapBasic'].append(dupRow)

        ### Get the misDist and gapDist in var plot.
        line = fhIn.next()
        tmp = line.strip().split()
        for i in xrange(1, len(tmp)):  
            self.misDist.append(int(tmp[i]))

        line = fhIn.next()
        tmp = line.strip().split()
        for i in xrange(1, len(tmp)):  
            self.gapDist.append(int(tmp[i]))
           
        ### Get the var Type and num.
        try:
            line = fhIn.next()
        except StopIteration:
            pass
        else:           
            tmp = line.strip().split()
            # print tmp
            t = tmp[0][-4:]
            if not t.endswith('N'):
                d = []
                d.append(t)
                for i in xrange(1, len(tmp)):
                    d.append(int(tmp[i]))
                self.varType.append(d)

            for line in fhIn:
                tmp = line.strip().split()
                # print tmp
                t = tmp[0][-4:]
                if not t.endswith('N'):
                    d = []
                    d.append(t)
                    for i in xrange(1, len(tmp)):
                        d.append(int(tmp[i]))
                    self.varType.append(d)

        ### Process the mapBasic have up to two digits after the decimal point
        for i in xrange(len(self.htmlPara['mapBasic'])):
            if self.htmlPara['mapBasic'][i][0] == 'DuplicationRate':
                continue
            for j in xrange(len(self.htmlPara['mapBasic'][i])):
                if re.search(r'\.', self.htmlPara['mapBasic'][i][j]):
                    self.htmlPara['mapBasic'][i][j] = round(float(self.htmlPara['mapBasic'][i][j])*100, 2)


        # x_label
        x_labels = range(0, int(self.readLen)+1)
        self.x_labels = range(1, int(self.readLen)+1)
        for i in x_labels:
            if i not in x_labels[::5]:
                self.x_labels[i-1] = ''
        self.x_labels[0] = 1
        self.x_labels = map(str, self.x_labels)

        self.mapTable = self.htmlPara['mapBasic']
        ### plot
        self.idx[1] += 1
        self.htmlPara['basicInfoTableTitle'] = "Tab%d. Basic statistics of ReadsMapping" % self.idx[1]
        self.htmlPara['varDistSvg'] = self.drawVarRateDist()
        self.htmlPara['mismatchTypeSvg'] = self.drawMisTypeDist()
        self.htmlPara['misCountBarSvg'] = self.drawNumCountBar(self.misNum, 'Mismatch')
        self.htmlPara['gapCountBarSvg'] = self.drawNumCountBar(self.gapNum, 'Gap')
        self.mapFigure['numCount']['xTag'] = ['%d' % d for d in xrange(len(self.mapFigure['numCount']['Mismatch']))]

        # Make sure that Mismatch and Gap had same lenght
        self.mapFigure['numCount']['Mismatch'] = self.setListLength(self.mapFigure['numCount']['Mismatch'], len(self.mapFigure['numCount']['xTag']))
        self.mapFigure['numCount']['Gap'] = self.setListLength(self.mapFigure['numCount']['Gap'], len(self.mapFigure['numCount']['xTag']))

        # Filter that mismatch number > 3
        if len(self.mapFigure['numCount']['xTag']) > 4:
            tmpDict = {}
            for k in ['Mismatch', 'Gap']:
                tmpDict[k] = self.mapFigure['numCount'][k][:3]
                tmpDict[k].append(str(sum(map(float, self.mapFigure['numCount'][k][3:]))))
            tmpDict['xTag'] = self.mapFigure['numCount']['xTag'][:3]
            tmpDict['xTag'].append('>3')
            self.mapFigure['numCount'] = tmpDict

        self.htmlPara['readLen'] = self.readLen
        return self.htmlPara

    def prepareData(self, dataList):
        htmlPara = {}
        
        # misDist = []
        # gapDist = []
        # misNum = {}
        # gapNum = {}
        # varType = []
        htmlPara['mapBasic'] = []
        
        
        ### Get the basic mapping values.
        fhIn = iter(dataList)
        fhIn.next()
        for i in xrange(9):
            line = fhIn.next()
            tmp = line.strip().split()
            if i == 0:
                self.readLen = int(tmp[1])
                continue
            if i == 1:
                tmp = [tmp[0], tmp[1], '-', '-']

            htmlPara['mapBasic'].append(tmp)

        self.MappedReads = int(htmlPara['mapBasic'][1][1])

        #line = fhIn.next()
        #tmp = line.strip().split()
        #dupRow = [tmp[0], '-', '-', tmp[1]]
        #htmlPara['dupRate'] = tmp[1]

        ### Get the bar plot's values.
        line = fhIn.next()
        tmp = line.strip().split()
        for j in xrange(1, len(tmp)):
            self.misNum[j-1] = int(tmp[j])

        line = fhIn.next()
        tmp = line.strip().split()
        for j in xrange(1, len(tmp)):
            self.gapNum[j-1] = int(tmp[j])

        line = fhIn.next()
        tmp = line.strip().split()
        row = [tmp[0], '-', '-', tmp[1]]
        htmlPara['mapBasic'].append(row)


        # self.htmlPara['mapBasic'].append(dupRow)

        ### Get the misDist and gapDist in var plot.
        line = fhIn.next()
        tmp = line.strip().split()
        for i in xrange(1, len(tmp)):  
            self.misDist.append(int(tmp[i]))

        line = fhIn.next()
        tmp = line.strip().split()
        for i in xrange(1, len(tmp)):  
            self.gapDist.append(int(tmp[i]))
           
        ### Get the var Type and num.
        try:
            line = fhIn.next()
        except StopIteration:
            pass
        else:           
            tmp = line.strip().split()
            # print tmp
            t = tmp[0][-4:]
            if not t.endswith('N'):
                d = []
                d.append(t)
                for i in xrange(1, len(tmp)):
                    d.append(int(tmp[i]))
                self.varType.append(d)

            for line in fhIn:
                tmp = line.strip().split()
                # print tmp
                t = tmp[0][-4:]
                if not t.endswith('N'):
                    d = []
                    d.append(t)
                    for i in xrange(1, len(tmp)):
                        d.append(int(tmp[i]))
                    self.varType.append(d)

        ### Process the mapBasic have up to two digits after the decimal point
        for i in xrange(len(htmlPara['mapBasic'])):
            if htmlPara['mapBasic'][i][0] == 'DuplicationRate':
                continue
            for j in xrange(len(htmlPara['mapBasic'][i])):
                if re.search(r'\.', htmlPara['mapBasic'][i][j]):
                    htmlPara['mapBasic'][i][j] = round(float(htmlPara['mapBasic'][i][j])*100, 2)

        return htmlPara

    def createReportParaPE(self):
        if not os.path.exists(self.file):
            return
        fhIn = open(self.file, 'r')
        dataList = []
        tmp = []
        lines = fhIn.readlines()
        fhIn.close()

        for line in lines:
            if re.search('^-', line):
                if tmp:
                    dataList.append(tmp)
                    tmp = []
                continue
            elif line.startswith('#insertSize'):
                keyLine = lines[-2]
                keyLine = map(int, keyLine.strip().split())
                valueLine = lines[-1]
                valueLine = map(int, valueLine.strip().split())
                self.insertDict = dict(zip(keyLine, valueLine))
                
                insertSize = self.drawInsertSize()
                if insertSize:
                    self.mapFigure['insertSize'] = insertSize
                break
            else:
                tmp.append(line)
        dataList.append(tmp)

        self.htmlPara = {}

        startIdx = self.idx[0]
        self.idx[1] += 1
        self.htmlPara['basicInfoTableTitle'] = "Tab%d. Basic statistics of ReadsMapping" % self.idx[1]
        
        self.mapTable = []
        for pos in xrange(len(dataList)):
            titleFlag = 'read %s' % (str(pos+1) if pos < 2 else 'total')
            # print titleFlag
            self.init()
            self.htmlPara['tableData%d' % (pos,)] = self.prepareData(dataList[pos])
            self.mapTable.append([titleFlag, '', '', ''])
            self.mapTable.extend(self.htmlPara['tableData%d' % (pos,)]['mapBasic'])
            ### Judge the x_labels of figure
            x_labels = range(0, int(self.readLen)+1)
            self.x_labels = range(1, int(self.readLen)+1)
            for i in x_labels:
                if i not in x_labels[::5]:
                    self.x_labels[i-1] = ''
            self.x_labels[0] = 1
            self.x_labels = map(str, self.x_labels)

            ### plot
            self.idx[0] = startIdx
            self.idx[0] += (pos)
            
            self.mapFigure['varRateDist%d' % (pos,)] = self.drawVarRateDistPE('Discordance distribution on reads position in %s' % (titleFlag,))
            self.idx[0] += 2
            self.mapFigure['misTypeDist%d' % (pos,)] = self.drawMisTypeDistPE('Mismatch type distribution on reads position in %s' % (titleFlag,))
            self.idx[0] += 2
            self.mapFigure['numCount%d' % (pos,)] = {}
            self.mapFigure['numCount%d' % (pos,)]['Mismatch'] = self.drawNumCountBarPE(self.misNum)
            self.idx[0] += 2
            self.mapFigure['numCount%d' % (pos,)]['Gap'] = self.drawNumCountBarPE(self.gapNum)
            self.mapFigure['numCount%d' % (pos,)]['xTag'] = ['%d' % d for d in xrange(len(self.mapFigure['numCount%d' % (pos,)]['Mismatch']))]

        self.mapFigure['varRateDist'] = self.mergeDict(self.mapFigure['varRateDist0'], self.mapFigure['varRateDist1'])
        self.mapFigure['misTypeDist'] = self.mergeDict(self.mapFigure['misTypeDist0'], self.mapFigure['misTypeDist1'])
        self.mapFigure['numCountPE'] = self.mergeNumCount(self.mapFigure['numCount0'], self.mapFigure['numCount1'])

        self.htmlPara['readLen'] = self.readLen
        return self.htmlPara

    def mergeDict(self, d1, d2):
        d = copy.deepcopy(d1)
        for k, v in d2.iteritems():
            if k in d:
                d[k].extend(d2[k])
            else:
                d[k] = d2[k]

        return d

    def createReportParaPEFromStr(self, mapInfoStr):
        dataList = []
        tmp = []
        lines = mapInfoStr.strip().split('\n')

        for line in lines:
            if re.search('^-', line):
                if tmp:
                    dataList.append(tmp)
                    tmp = []
                continue
            elif line.startswith('#insertSize'):
                keyLine = lines[-2]
                try:
                    keyLine = map(int, keyLine.strip().split())
                except ValueError as e:
                    break
                valueLine = lines[-1]
                valueLine = map(int, valueLine.strip().split())
                self.insertDict = dict(zip(keyLine, valueLine))
                
                self.mapFigure['insertSize'] = self.drawInsertSize()
                break
            else:
                tmp.append(line)
        dataList.append(tmp)

        self.htmlPara = {}

        startIdx = self.idx[0]
        self.idx[1] += 1
        self.htmlPara['basicInfoTableTitle'] = "Tab%d. Basic statistics of ReadsMapping" % self.idx[1]
        
        self.mapTable = []
        for pos in xrange(len(dataList)):
            titleFlag = 'read %s' % (str(pos+1) if pos < 2 else 'total')
            # print titleFlag
            self.init()
            self.htmlPara['tableData%d' % (pos,)] = self.prepareData(dataList[pos])
            self.mapTable.append([titleFlag, '', '', ''])
            self.mapTable.extend(self.htmlPara['tableData%d' % (pos,)]['mapBasic'])
            ### Judge the x_labels of figure
            x_labels = range(0, int(self.readLen)+1)
            self.x_labels = range(1, int(self.readLen)+1)
            for i in x_labels:
                if i not in x_labels[::5]:
                    self.x_labels[i-1] = ''
            self.x_labels[0] = 1
            self.x_labels = map(str, self.x_labels)

            ### plot
            self.idx[0] = startIdx
            self.idx[0] += (pos)
            
            self.mapFigure['varRateDist%d' % (pos,)] = self.drawVarRateDistPE('Discordance distribution on reads position in %s' % (titleFlag,))
            self.idx[0] += 2
            self.mapFigure['misTypeDist%d' % (pos,)] = self.drawMisTypeDistPE('Mismatch type distribution on reads position in %s' % (titleFlag,))
            self.idx[0] += 2
            self.mapFigure['numCount%d' % (pos,)] = {}
            self.mapFigure['numCount%d' % (pos,)]['Mismatch'] = self.drawNumCountBarPE(self.misNum)
            self.idx[0] += 2
            self.mapFigure['numCount%d' % (pos,)]['Gap'] = self.drawNumCountBarPE(self.gapNum)
            self.mapFigure['numCount%d' % (pos,)]['xTag'] = ['%d' % d for d in xrange(len(self.mapFigure['numCount%d' % (pos,)]['Mismatch']))]

        self.mapFigure['varRateDist'] = self.mergeDict(self.mapFigure['varRateDist0'], self.mapFigure['varRateDist1'])
        self.mapFigure['misTypeDist'] = self.mergeDict(self.mapFigure['misTypeDist0'], self.mapFigure['misTypeDist1'])
        self.mapFigure['numCountPE'] = self.mergeNumCount(self.mapFigure['numCount0'], self.mapFigure['numCount1'])

        self.htmlPara['readLen'] = self.readLen
        
        return self.htmlPara

    def mergeNumCount(self, d1, d2):
        d = {}
        xTag = d1['xTag'] if len(d1['xTag']) > len(d2['xTag']) else d2['xTag']
        d['xTag'] = xTag
        d['MismatchR1'] = self.setListLength(d1['Mismatch'], len(xTag))
        d['MismatchR2'] = self.setListLength(d2['Mismatch'], len(xTag))
        d['GapR1'] = self.setListLength(d1['Gap'], len(xTag))
        d['GapR2'] = self.setListLength(d2['Gap'], len(xTag))
        
        # Filter data that mismatch num > 3
        nd = {}
        if len(xTag) > 4:
            for k in ['MismatchR1', 'MismatchR2', 'GapR1', 'GapR2']:
                nd[k] = d[k][:4]
                nd[k].append(str(sum(map(float, d[k][4:]))))
                
            nd['xTag'] = xTag[:4]
            nd['xTag'].append('>3')
        else:
            nd = d

        return nd

    def setListLength(self, lst, length, v=0):
        if len(lst) >= length:
            return lst

        newList = copy.deepcopy(lst)
        newList.extend([v] * (length - len(lst)))

        return newList

    def setDupRate(self, dupRate, pe=False):
        self.mapTable.append(['DuplicationRate', '-', '-', dupRate])
        #print self.mapTable

    
    def geneSamJsonData(self):
        jsonData = {}

        jsonData['mapTable'] = self.mapTable
        jsonData['mapTable'].insert(0, ['Category', 'ReadsNumber', 'Pct in TotalReads(%)', 'Pct in MappedReads(%)'])
        for v in ['varRateDist', 'misTypeDist', 'numCount', \
                    'varRateDist0', 'misTypeDist0', 'numCount0', \
                    'varRateDist1', 'misTypeDist1', 'numCount1', \
                    'varRateDist2', 'misTypeDist2', 'numCount2', \
                    'numCountPE', 'insertSize']:
            if v in self.mapFigure and self.mapFigure[v]:
                jsonData[v] = json.dumps(self.mapFigure[v], sort_keys=True)

        return jsonData

##########################################################################
############################  BEGIN Function  ############################
##########################################################################


######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)
    ArgParser.add_argument("-r", "--report", action="store", dest="reportFile", metavar="FILE", help="Generate mapping to html file. [%(default)s]")

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 3:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (file, prefix, outfile) = args

    ############################# Main Body #############################
    # SE
    # sam = samStat(file, prefix)
    # sam.createReportPara()
    # sam.convertHTML(outfile)
   
    # PE
    sam = samStat(file, prefix)
    sam.createReportPara()
    sam.convertHTML(outfile)
#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
