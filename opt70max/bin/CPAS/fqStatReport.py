#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
### Py2.7 encode fix
reload(sys)
sys.setdefaultencoding('utf8')

import re
import json

###### Document Decription
''' Read file or string of XXX.fqStat.txt, then generate json data for render html '''

###### Change log
'''
    1.1.0   2015.11.17  Merge fq statics figures of read1 and read2 for PE sequencing, from read total to read1 + read2

'''

###### Version and Date
prog_version = '1.1.0'
prog_date = '2015.11.17'

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <statFile> <prefix> <outfile> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class readsStat(object):
    """docstring for readsStat"""

    def readTsv(self, tsvFile):
        if os.path.exists(tsvFile):
            with open(tsvFile, 'r') as fhIn:
                data = fhIn.read().strip().split('\n')
            fhIn.close  
        else:
            data = tsvFile.strip().split('\n')
        
        for line in data:
            info = line.split()

            if info[0].startswith('#'):
                if len(info) > 2:
                    self.info[info[0][1:]] = info[1:]
                else:
                    self.info[info[0][1:]] = info[1]
            elif re.match(r'\d+$', info[0]): 
                self.readLen += 1
                self.dist.append(info)

    def readTsvPE(self, tsvFile):
        if os.path.exists(tsvFile):
            with open(tsvFile, 'r') as fhIn:
                data = fhIn.read().strip().split('\n')
            fhIn.close  
        else:
            data = tsvFile.strip().split('\n')

        self.info = [{}, {}, {}]
        idx = 0
        currentInfo = self.info[idx]
        for line in data:
            info = line.split()
            if info[0].startswith('-'):
                currentInfo = self.info[idx]
                idx += 1
                continue

            if info[0].startswith('#'):
                if len(info) > 2:
                    currentInfo[info[0][1:]] = info[1:]
                else:
                    currentInfo[info[0][1:]] = info[1]
            elif idx != 3 and re.match(r'\d+$', info[0]):
                self.readLen += 1
                self.dist.append(info)

    def generateParaReadSata(self):
        self.idx[1] += 1
        #self.htmlPara['fqName'] = self.info['Name']
        self.htmlPara['fqStatTitle'] = "Tab%s. Basic statistics of fastq" % self.idx[1]
        self.htmlPara['infoDict'] = self.info
        self.drawBaseTypeDist()
        self.drawGcDist()
        self.drawEstimateErr()
        self.drawAvgQual()
        self.drawQualPortion()
        
    def drawBaseTypeDist(self):
        xlabels = map(str, xrange(1, self.readLen+1))
        data = {'A':[], 'C':[], 'G':[], 'T':[], 'N':[]}
        con = {0:'A', 1:'C', 2:'G', 3:'T', 4:'N'}
        for i in xrange(self.readLen):
            p = self.dist[i]
            for j in range(5):
                pct = round(float(p[j+1])*100/sum(map(int, p[1:6])), 2)
                data[con[j]].append(pct)
        self.idx[0] += 1
        
        self.fqFigure['baseTypeDist'] = data

    def drawGcDist(self):
        xlabels = map(str, xrange(1, self.readLen+1))
        data = {'GC':[]}
        for i in xrange(self.readLen):
            try:
                val = round(100.0*sum(map(int, self.dist[i][2:4]))/sum(map(int, self.dist[i][1:5])), 2)
            except ZeroDivisionError:
                data['GC'].append(0.0)
            else:
                data['GC'].append(val)
        self.idx[0] += 1
       
        self.fqFigure['gcDist'] = data

    def drawEstimateErr(self):
        xlabels = map(str, xrange(1, self.readLen+1))
        data = {'EstErr':[]}
        for i in xrange(self.readLen):
            data['EstErr'].append(round(float(self.dist[i][len(self.dist[i])-1]), 3))

        self.idx[0] += 1

        self.fqFigure['estError'] = data

    def drawAvgQual(self):
        xlabels = map(str, xrange(1, self.readLen+1))
        data = {'Qual':[] }
        for i in xrange(self.readLen):
            totalNum = 0
            sumQ = 0
            for q in range(6,len(self.dist[i])-1):
                qVal = q - 6
                sumQ += int(self.dist[i][q])*qVal
                totalNum += int(self.dist[i][q])
            if totalNum == 0:
                avgQ = None
            else:
                avgQ = round(float(sumQ)/totalNum, 1)
            data['Qual'].append(avgQ)
        self.idx[0] += 1

        self.fqFigure['Qual'] = data

    def drawQualPortion(self):
        xlabels = map(str, xrange(1, self.readLen+1))
        data = {'[0-10)':[], '[10-20)':[], '[20-30)':[],'[30-40]':[]}
        for i in xrange(self.readLen):
            l10 = 0
            l20 = 0
            l30 = 0
            l40 = 0
            for q in range(6,len(self.dist[i])-1):
                qVal = q - 6
                if qVal < 10:
                    l10 += int(self.dist[i][q])
                elif qVal < 20:
                    l20 += int(self.dist[i][q])
                elif qVal < 30:
                    l30 += int(self.dist[i][q])
                else:
                    l40 += int(self.dist[i][q])
            total = l10 + l20 + l30 + l40
            if total == 0:
                for t in data:
                    data[t].append(None)
            else:
                data['[0-10)'].append(round(100.0*l10/total, 1))
                data['[10-20)'].append(round(100.0*l20/total, 1))
                data['[20-30)'].append(round(100.0*l30/total, 1))
                data['[30-40]'].append(round(100.0*l40/total, 1))

        self.idx[0] += 1
        self.fqFigure['qualPortion'] = data
       
    def __str__(self):
        tmpList = []
        for k in self.key:
            if isinstance(self.info[k], list) :
                tmpList.append("#%s\t%s" % (k, "\t".join(self.info[k]) ))
            else:
                tmpList.append("#%s\t%s" % (k, self.info[k]))
        
        for l in xrange(self.readLen):
            tmpList.append("\t".join(self.dist[l]))

        return "\n".join(tmpList)
    
    def __init__(self, statTsv, prefix):
        # super(readsStat, self).__init__()
        self.statTsv = statTsv
        self.htmlPara = {}
        self.key = ['Name', 'PhredQual', 'ReadNum', 'BaseNum', 'N_Count', 'GC%', '>Q10%', '>Q20%', '>Q30%', '>Q40%', 'EstErr%', 'Pos']
        self.info = {}
        self.dist = []
        self.readLen = 0
        self.idx = [0, 0] ## Fig And Table index
        self.prefix = prefix
        
        self.figureDataBuffer = {}

        self.fqTable = []
        self.fqFigure = {}

    def prepareData(self):
        self.readTsv(self.statTsv)

        if re.search(r'\\', self.info['Name']):
            self.info['Name'] = self.info['Name'].split('\\')[-1]
        else:
            self.info['Name'] = self.info['Name'].split('/')[-1]

        self.info['N_Count%'] = round(float(self.info['N_Count'][-1]), 2)
        self.info['EstErr%'] = round(float(self.info['EstErr%']), 2)

        option = ['Name', 'PhredQual', 'ReadNum', 'BaseNum', 'N_Count%', 'GC%', '>Q10%',\
                    '>Q20%', '>Q30%', '>Q40%', 'EstErr%']
        self.fqTable.append(option)
        tmpList = []
        for k in option:
            if k in self.info:
                tmpList.append(self.info[k])
        self.fqTable.append(tmpList)

       
        # x_label
        x_labels = range(0, int(self.readLen)+1)
        self.x_labels = range(1, int(self.readLen)+1)
        for i in x_labels:
            if i not in x_labels[::5]:
                self.x_labels[i-1] = ''
         
        self.x_labels[0] = 1
        self.x_labels = map(str, self.x_labels)

    def prepareDataPE(self):
        self.readTsvPE(self.statTsv)

        self.info[0]['Name'] = 'read 1'
        self.info[1]['Name'] = 'read 2'
        self.info[2]['Name'] = os.path.basename('read total')

        self.info[0]['N_Count%'] = round(float(self.info[0]['N_Count'][-1]), 2)
        self.info[0]['EstErr%'] = round(float(self.info[0]['EstErr%']), 2)
        self.info[1]['N_Count%'] = round(float(self.info[1]['N_Count'][-1]), 2)
        self.info[1]['EstErr%'] = round(float(self.info[1]['EstErr%']), 2)
        self.info[2]['N_Count%'] = round(float(self.info[2]['N_Count'][-1]), 2)
        self.info[2]['EstErr%'] = round(float(self.info[2]['EstErr%']), 2)

        option = ['Name', 'PhredQual', 'ReadNum', 'BaseNum', 'N_Count%', 'GC%', '>Q10%',\
                    '>Q20%', '>Q30%', '>Q40%', 'EstErr%']
        self.fqTable.append(option)
        for i in xrange(3):
            tmpList = []
            for k in option:
                if k in self.info[i]:
                    if k == 'ReadNum' and i == 2: tmpList.append(int(self.info[i][k])/2)
                    else: tmpList.append(self.info[i][k])
            self.fqTable.append(tmpList)
        # x_label
        x_labels = range(0, int(self.readLen)+1)
        self.x_labels = range(1, int(self.readLen)+1)
        for i in x_labels:
            if i not in x_labels[::5]:
                self.x_labels[i-1] = ''
         
        self.x_labels[0] = 1
        self.x_labels = map(str, self.x_labels)

    def geneFqJsonData(self):
        jsonData = {}
        jsonData['fqTable'] = self.fqTable
        for v in ['baseTypeDist', 'gcDist', 'estError', 'Qual', 'qualPortion']:
            if v in self.fqFigure:
                jsonData[v] = json.dumps(self.fqFigure[v])

        return jsonData

##########################################################################
############################  BEGIN Function  ############################
##########################################################################


######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 3:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (statFile, prefix, outfile) = args

    ############################# Main Body #############################
    # SE
    # stat = readsStat(statFile, prefix)
    # stat.prepareData()
    # stat.generateParaReadSata()
    # stat.convertHtml(outfile)
    # PE
    stat = readsStat(statFile, prefix)
    stat.prepareDataPE()
    stat.generateParaReadSata()
    stat.convertHtml(outfile)
   

#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
