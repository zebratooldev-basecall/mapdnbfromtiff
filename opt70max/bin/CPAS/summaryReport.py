#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
### Py2.7 encode fix
reload(sys)
sys.setdefaultencoding('utf8')

import glob
import time
import string
import re

from qcReport import analysis
from fqStatReport import readsStat
from samStatReport import samStat
from cat import CatFqStat, CatMapStat, CatQcStat
from utility import getBarcodeSplitRate

###### Document Decription
''' generate report through read qc, fqStat, samStat files '''

###### Version and Date
prog_version = '1.0.0'
prog_date = '2015-05-21'

##### Chang Log
'''
    1.0.1   2015.11.5    Add MaxOffsetX and MaxOffsetY in summary Table
                    Add Average Duplication Rate for summaryReport's Table
                    Add split rate for summary table if there is split

    1.0.2   2015.11.6    Add histogram of split rate, if rate less than 1, classify them to group named other(<1)

    1.1.0   2015.11.12  Convert table rows and columns of Basecall Info and SE-Fq Statics
    1.1.1   2015.12.7   Fix a bug that dupFile in error status

    1.2.0   2015.12.10  Add phaseTable in summaryReport for option
    1.2.1   2015.12.11  1.Fix a bug that can't generate report when there is no BasecallQC.txt
                        2.When the split rate is all less than 1.0, do not display the splitRate figure

    1.3.0   2016.01.28  1. Fix a bug when there is not exits fqStat file in PE mode it will crash
                        2. Add FIT value in summaryTable
                        3. Change the order of the category in summaryTable
    1.3.1   2016.02.02  1. Add InitialOffsetX/Y in summaryTable and Change FIT% to InitialFIT%
                        2. Fix a bug that program crash when there exists error in fq stat file in PE mode

    1.4.0   2016.02.15  Fix bug that cat returns None

'''

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <prefix> <outHtmlFile>
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))
  
###### Global Variable

###### Class Defined
class SummaryReport(object):
    def __init__(self, prefix, outHtmlFile, templateHtmlFile=None, qcFile=None, fqStatFile1=None, fqStatFile2=None, mapStatFile=None, dupRateFile=None, bioFile=None, reference='Ecoli.fa', pe=False, searchDir=None):
        ### Initlize the var
        self.prefix = prefix
        self.outHtmlFile = outHtmlFile

        self.templateHtmlFile = templateHtmlFile

        self.qcFile = qcFile
        self.fqStatFile1 = fqStatFile1
        self.fqStatFile2 = fqStatFile2
        self.mapStatFile = mapStatFile

        self.dupRateFile = dupRateFile
        self.bioFile = bioFile
        self.reference = reference
        self.pe = pe
        
        self.searchDir = searchDir

        ### Initlize the json data
        self.jsonData = {}
        reportTitle = 'Analysis Report of %s' % (self.prefix)
        self.jsonData['reportTitle'] = '"%s"' % reportTitle
        reportTime = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        self.jsonData['reportTime'] = '"%s"' % reportTime

        ### Initilize the fov's max col and row
        self.maxFov = (0, 0)
        self.calledFovs = 0

        ### Add split rate of summary table
        self.splitRate = None

        # Add barcode split rate histogram
        self.barcodeSplitFile = None

    def prepareData(self):
        infoDict = self.getInfoFromFile()

        if infoDict:
            # If there is not exists qc file name in BasecallQc.txt, then pass it
            try:
                qcFileList = infoDict['QC']
            except:
                pass
            else:
                calledFovs = len(qcFileList)
                self.setCalledFovs(calledFovs)
            # Add chip cmax, rmax for summaryTable and heatmap report
            maxFov = tuple(map(int, infoDict['Cmax_Rmax'][0].strip().split()[1:]))
            self.setMaxFov(maxFov)

            # Add split rate
            if 'BarcodeStat' in infoDict:
                splitFile = os.path.join(self.searchDir, 'BarcodeStat.txt')
                if os.path.exists(splitFile):
                    self.setBarcodeSplitFile(splitFile)

    def generateReport(self):
        ### If gived a search dir, it means I need generate a summary report for single lane,
        ### Else, will generate report for single barcode/fastq
        if self.searchDir != None and os.path.exists(self.searchDir):
            # QC
            totalQcFile = os.path.join(self.searchDir, 'BasecallQC.txt')
            if os.path.exists(totalQcFile):
                qcString = self.splitTotalQcFile(totalQcFile)
                if qcString:
                    qcStat = analysis(None)
                    qcStat.readInfoFromStr(qcString)
                    qcJsonData = qcStat.geneQcJsonData()
                    # wipe off background figue
                    if 'BACKGROUND' in qcJsonData:
                        del(qcJsonData['BACKGROUND'])

                    self.totalGRR = qcStat.getTotalGRR()

                    self.jsonData.update(qcJsonData)
            # FQ
            totalFqList = glob.glob(os.path.join(self.searchDir, '*.fq.gz'))
            totalFqList.sort()
            totalFqStatList = [fqName[:-2]+'fqStat.txt' for fqName in totalFqList]
            if self.pe:
                totalFqPeList = []
                for i in xrange(len(totalFqStatList)/2):
                    if not os.path.exists(totalFqStatList[i*2]) or not os.path.exists(totalFqStatList[i*2+1]):
                        continue
                    singleFqPeData = self.getSingleFqFilePE(totalFqStatList[i*2], totalFqStatList[i*2+1])
                    if singleFqPeData:
                        totalFqPeList.append(singleFqPeData)
                if len(totalFqPeList) > 1:
                    cat = CatFqStat(is_pe=True)
                    fqStatString = cat.cat(totalFqPeList)
                    if fqStatString:
                        self.jsonData.update(self.getFqStatData(fqStatString))
                elif len(totalFqPeList) == 1:
                    fqStatString = totalFqPeList[0]
                    if fqStatString:
                        self.jsonData.update(self.getFqStatData(fqStatString))
            else:
                totalFqStatList = filter(os.path.isfile, totalFqStatList)
                if totalFqStatList:
                    cat = CatFqStat()
                    fqStatString = cat.cat(totalFqStatList)
                    if fqStatString:
                        self.jsonData.update(self.getFqStatData(fqStatString))
        else:
            # QC
            if self.qcFile != None and os.path.exists(self.qcFile):
                qcStat = analysis(self.qcFile)
                qcJsonData = qcStat.geneQcJsonData()
                # wipe off background figue
                if 'BACKGROUND' in qcJsonData:
                    del(qcJsonData['BACKGROUND'])

                self.jsonData.update(qcJsonData)
            # FQ
            if self.pe:
                if self.fqStatFile1 != None and os.path.exists(self.fqStatFile1) and self.fqStatFile2 != None and os.path.exists(self.fqStatFile2):
                    #newFqStatFile = os.path.join(os.path.dirname(fqStatFile1), '%s.fqStat.txt' % (prefix,))
                    totalFqString = self.getSingleFqFilePE(self.fqStatFile1, self.fqStatFile2)
                    self.jsonData.update(self.getFqStatData(totalFqString))
            else:
                if self.fqStatFile1 != None and os.path.exists(self.fqStatFile1):
                    self.jsonData.update(self.getFqStatData(self.fqStatFile1))

        # MAP
        mapJsonData = self.getMapStatData()
        if mapJsonData != None:
            self.jsonData.update(mapJsonData)

        # Add calculate of chip productivity and image area
        if self.searchDir is None:
            self.imageArea = 0
            self.chipProd = 0
        else:
            self.imageArea = self.maxFov[0] * self.maxFov[1]
            try:
                self.chipProd = round(float(self.calledFovs) / float(self.imageArea) * self.totalGRR , 2)
            except ZeroDivisionError as e:
                self.chipProd = 0
            except Exception as e:
                self.chipProd = 0
        #print self.calledFovs, self.imageArea, self.totalGRR

        # Add barcode split rate histogram
        if self.barcodeSplitFile is not None:
            barcodeDict, totalSplitRate = getBarcodeSplitRate(self.barcodeSplitFile, isJson=True)
            self.splitRate = totalSplitRate
            if barcodeDict:
                self.jsonData['barcodeSplitRate'] = barcodeDict

        # Generate report
        newReport = self.outHtmlFile
        isSummary = False if self.searchDir is None else True
        extractSummaryBio(self.jsonData, self.bioFile, self.reference, barcodeSplitRate=self.splitRate, pe=self.pe, imageArea=self.imageArea, chipProd=self.chipProd, isSummary=isSummary)

        transformReport(self.templateHtmlFile, newReport, self.jsonData)

    def getFqStatData(self, fqStatString):
        fqStat = readsStat(fqStatString, self.prefix)
        if self.pe:
            fqStat.prepareDataPE()
        else:
            fqStat.prepareData()
        fqStat.generateParaReadSata()
        fqJsonData = fqStat.geneFqJsonData()

        return fqJsonData

    def getMapStatData(self):
        mapJsonData = None
        if self.mapStatFile != None and os.path.exists(self.mapStatFile):
            if os.path.isfile(self.mapStatFile):
                mapStat = samStat(self.mapStatFile, self.prefix)
                if self.pe:
                    mapStat.createReportParaPE()
                else:
                    mapStat.createReportPara()

            elif os.path.isdir(self.mapStatFile):
                mapStatList = glob.glob(os.path.join(self.mapStatFile, '*.mapInfo.tsv'))
                mapStatList = [mapStat for mapStat in mapStatList if not re.search('split.mapInfo.tsv$', mapStat) ]

                if self.pe:
                    cat = CatMapStat(is_pe=True)
                    mapStatString = cat.cat(mapStatList)
                    if mapStatString:
                        mapStat = samStat(None, self.prefix)
                        mapStat.createReportParaPEFromStr(mapStatString)
                else:
                    cat = CatMapStat()
                    mapStatString = cat.cat(mapStatList)
                    if mapStatString:
                        mapStat = samStat(None, self.prefix)
                        mapStat.createReportParaFromStr(mapStatString)

            dupRate = self.getDupRate()
            if dupRate != None:
                mapStat.setDupRate(dupRate)

            if 'mapStat' in dir():
                mapJsonData = mapStat.geneSamJsonData()

        return mapJsonData

    def splitTotalQcFile(self, totalQcFile):
        with open(totalQcFile, 'r') as fhIn:
            totalFov = fhIn.read()
            if re.search(r'\r\n', totalFov):
                totalFov = re.sub(r'\r\n', '\n', totalFov)
            totalFov = totalFov.strip().split('#FOV\n')
            qcStringList = []
            for fovData in totalFov:
                if not fovData:
                    continue
                fovData = fovData.strip().split('\n')
                qcStringList.append('\n'.join(fovData[1:]))
            cat = CatQcStat()
            outQcString = cat.cat(qcStringList)

        return outQcString

    def getDupRate(self):
        dupRate = None
        if self.dupRateFile != None and os.path.exists(self.dupRateFile):
            dupRate = self._getDupOneFile(self.dupRateFile)
        elif self.mapStatFile is not None and os.path.exists(self.mapStatFile) and os.path.isdir(self.mapStatFile):
            mapDir = self.mapStatFile
            dupFileList = glob.glob(os.path.join(mapDir, "*.rmdup.txt"))
            dupRateList = []
            if len(dupFileList) > 0:
                for dupRateFile in dupFileList:
                    dupRateList.append(self._getDupOneFile(dupRateFile))
            dupRateList = filter(None, dupRateList)
            try:
                dupRate = round(sum(dupRateList) / len(dupRateList), 2)
            except ZeroDivisionError as e:
                pass

        return dupRate

    def _getDupOneFile(self, dupRateFile):
        dupRate = None
        with open(dupRateFile, 'r') as fhIn:
            line = fhIn.readline()
            if line:
                try:
                    dupRate = round(float(line.strip().split()[-1])*100, 2)
                except Exception, e:
                    pass

        return dupRate

    def getSingleFqFilePE(self, fqStatFile1, fqStatFile2):
        fqStatFileList = [fqStatFile1, fqStatFile2]
        cat = CatFqStat()
        totalFqString = cat.create_file_pe(fqStatFileList)

        return totalFqString

    def setMaxFov(self, maxFov):
        self.maxFov = maxFov

    def setCalledFovs(self, calledFovs):
        self.calledFovs = calledFovs

    def setBarcodeSplitFile(self, barcodeSplitFile):
        self.barcodeSplitFile = barcodeSplitFile

    def getInfoFromFile(self):
        if self.searchDir is None:
            return None
        filename = os.path.join(self.searchDir, 'FileName.txt')

        if not os.path.exists(filename):
            return None

        infoDict = {}
        with open(filename, 'r') as fhIn:
            for line in fhIn:
                if line[0] == '#':
                    flag = line.lstrip('#').rstrip()
                    continue
                infoDict.setdefault(flag, []).append(line.strip())

        return infoDict

    def getReportData(self):
        return self.jsonData
    
###### Global Functions
def transformReport(templateReport, newReport, jsonData):
    allVar = ['qcTable', 'phaseTable', 'SIGNAL', 'trendSignal', 'BACKGROUND', 'SNR', 'RHO', 'Matrix', 'GRR', 'accGRR', 'lag', 'runon',\
                        'lag1', 'runon1', 'lag2', 'runon2', 'movement', 'barcodeSplitRate', \
                        'fqTable', 'baseTypeDist', 'gcDist', 'estError',\
                        'Qual', 'qualPortion',\
                        'mapTable', 'varRateDist', 'misTypeDist', 'numCount',\
                        'numCountPE', 'insertSize',\
                        'bioTable', 'summaryTable', 'reportTitle', 'reportTime']


    if templateReport == None:
        templateReport = os.path.join(os.path.dirname(sys.argv[0]), 'templates', 'summaryReportTmpl.html')

    fhIn = open(templateReport, 'r')
    template = fhIn.read()
    fhIn.close()
    
    # Construct a string template and replace json data
    newStr = string.Template(template)
    newStr = newStr.safe_substitute(jsonData)

    notFoundDict = dict.fromkeys([k for k in allVar if k not in jsonData], 'null')
    newStr = string.Template(newStr)
    newStr = newStr.safe_substitute(notFoundDict)

    fhOut = open(newReport, 'w')
    fhOut.write(newStr)
    fhOut.close()

def extractSummaryBio(jsonData, bioFile, reference, barcodeSplitRate=None, pe=False, imageArea=0, chipProd=0, isSummary=False):
    ### 1. Extract Summary Table
    summaryTable = [['Category', 'Value']]
    versionFile = os.path.join(os.path.dirname(sys.argv[0]), 'version.txt')
    if os.path.exists(versionFile):
        with open(versionFile, 'r') as fhIn:
            version = fhIn.read().strip()
            summaryTable.append(['Version', version])

    summaryTable.append(['Reference', reference])

    if 'qcTable' in jsonData:
        data = dict(zip(jsonData['qcTable'][0], jsonData['qcTable'][1]))
        if 'Cycle' in data:
            summaryTable.append(['CycleNumber', data['Cycle']])

    # Add chipProd and imageArea
    if chipProd != 0:
        summaryTable.append(['ChipProductivity%', chipProd])
    if imageArea != 0:
        summaryTable.append(['ImageArea', imageArea])
                    
    # transform order and unit
    divUnit = 10**6
    if 'mapTable' in jsonData:
        data = dict([(k[0], k[1:]) for k in jsonData['mapTable']])
        if 'TotalReads' in data:
            summaryTable.append(['TotalReads(M)', round(float(data['TotalReads'][0])/divUnit, 2)])
        if 'MappedReads' in data:
            summaryTable.append(['MappedReads(M)', round(float(data['MappedReads'][0])/divUnit, 2)])

    if 'mapTable' in jsonData:
        data = dict([(k[0], k[1:]) for k in jsonData['mapTable']])
        if 'DuplicationRate' in data:
            if isSummary:
                summaryTable.append(['AvgDuplicationRate%', data['DuplicationRate'][2]])
            else:
                summaryTable.append(['DuplicationRate%', data['DuplicationRate'][2]])

    ### PE is different from SE
    if pe:
        if 'fqTable' in jsonData:
            data = {}
            for i in xrange(len(jsonData['fqTable'][0])):
                data[jsonData['fqTable'][0][i]] = jsonData['fqTable'][3][i]
            for k in ['>Q30%']:
                if k in data:
                    summaryTable.append([k, data[k]])
    else:
        if 'fqTable' in jsonData:
            data = dict(zip(jsonData['fqTable'][0], jsonData['fqTable'][1]))
            for k in ['>Q30%']:
                if k in data:
                    summaryTable.append([k, data[k]])

    # Add barcode split rate if there is split rate
    if barcodeSplitRate != None:
        summaryTable.append(['SplitRate%', barcodeSplitRate])

    if 'qcTable' in jsonData:
        data = dict(zip(jsonData['qcTable'][0], jsonData['qcTable'][1]))
        for k in ['Lag%', 'Lag1%', 'Lag2%', 'Runon%', 'Runon1%', 'Runon2%', 'GRR%']:
            if k in data:
                summaryTable.append([k, data[k]])
    
    if 'mapTable' in jsonData:
        data = dict([(k[0], k[1:]) for k in jsonData['mapTable']])
        if 'MappedReads' in data:
            summaryTable.append(['MappingRate%', data['MappedReads'][1]])
        if 'MismatchRate' in data:
            summaryTable.append(['AvgErrorRate%', data['MismatchRate'][2]])

    # Add MaxOffsetX and MaxOffsetY in summaryTable
    # Add Fit value in summaryTable
    if 'qcTable' in jsonData:
        data = dict(zip(jsonData['qcTable'][0], jsonData['qcTable'][1]))
        options = ['MaxOffsetX', 'MaxOffsetY', 'InitialOffsetX', 'InitialOffsetY', 'InitialSignal', 'ForSignal(5_35)%', 'RevSignal(5_35)%',
                    'InitialFIT%', 'ForFIT(5_35)%', 'RevFIT(5_35)%']
        for k in options:
            if k in data:
                summaryTable.append([k, data[k]])

        # After extract MaxOffsetX/Y, delete raw MaxOffsetX/Y for display well
        tmp = jsonData['qcTable']
        for k in options:
            if k in data:
                pos = tmp[0].index(k)
                del(tmp[0][pos])
                del(tmp[1][pos])
                tmp = jsonData['qcTable']

    jsonData['summaryTable'] = summaryTable
    ### 2. Extract Bio Table
    bioTable = []
    ##### Read bio info
    if bioFile != None and os.path.exists(bioFile):
        bioTable.append(['Category', 'Value'])

        import codecs
        fhIn = codecs.open(bioFile, 'r', 'gb2312')
        # fhIn = open(bioFile, 'r')
        
        for line in fhIn:
            info = line.strip().split(',')
            # key = info[0].decode('gbk')
            # value = info[1].decode('gbk')        
            key = info[0]
            value = info[1]          
            bioTable.append([unicode2utf8(key), unicode2utf8(value)])    
           
        fhIn.close()
    
    if bioTable:
        jsonData['bioTable'] = bioTable

def unicode2utf8(u):
    s = repr(u)[2:-1]
    return s


#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)
    ArgParser.add_argument("-p", "--PE", action="store_true", dest="pe", default=False, help="Process PE data")
    ArgParser.add_argument("-r", "--ref", action="store", dest="reference", default="Ecoli.fa", help="Use specific reference. [%(default)s]")
    ArgParser.add_argument("-q", "--qcFile", action="store", dest="qcFile", default=None, help="Input qcFile.")
    ArgParser.add_argument("-f1", "--fqStatFile1", action="store", dest="fqStatFile1", default=None, help="Input fq stat File of read1.")
    ArgParser.add_argument("-f2", "--fqStatFile2", action="store", dest="fqStatFile2", default=None, help="Input fq stat File of read2.")
    ArgParser.add_argument("-m", "--mapStatFile", action="store", dest="mapStatFile", default=None, help="Input map stat File.")
    ArgParser.add_argument("-b", "--bioFile", action="store", dest="bioFile", default=None, help="Input bio File.")
    ArgParser.add_argument("-d", "--dupRateFile", action="store", dest="dupRateFile", default=None, help="Input duplication rate File.")
    ArgParser.add_argument("-t", "--templateHtmlFile", action="store", dest="templateHtmlFile", default=None, help="Input template html File.")
    ArgParser.add_argument("-s", "--searchDir", action="store", dest="searchDir", default=None, help="Input search dir to generate report of single lane.")

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 2:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    
    (prefix, htmlFile) = args
    ############################# Main Body #############################
    #startTime = datetime.datetime.now()
    #print 'Start Time: %s\n' % startTime.strftime("%Y-%m-%d %H:%M:%S")

    summrayReport = SummaryReport(prefix, htmlFile, para.templateHtmlFile, para.qcFile, para.fqStatFile1, para.fqStatFile2, para.mapStatFile, para.dupRateFile, para.bioFile, para.reference, para.pe, para.searchDir)
    summrayReport.prepareData()
    summrayReport.generateReport()

    #endTime = datetime.datetime.now()
    #print 'End Time: %s\n' % endTime.strftime("%Y-%m-%d %H:%M:%S")
    #print 'Generate report spend time %s\n' % (endTime - startTime)

  
#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()
