#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
### Py2.7 encode fix
reload(sys)
sys.setdefaultencoding('utf8')

import glob
from splitSamFile import splitSam

###### Document Decription
''' read sam or bam file '''

###### Version and Date
prog_version = '1.0.0'
prog_date = '2015-05-21'

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <samFile> <totalSamStatFile> [--splitFov file] [--PE] >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))
  

def readSam(samFile, totalSamStatFile, splitFov=False, pe=False):
    if not os.path.exists(samFile):
        print 'No %s' % samFile
        return

    sam = splitSam(samFile)
    try:
        sam.readSamFile()
    except IndexError:
        print 'No mapped reads.'
        return

    if not pe:
        sam.statAllSE(totalSamStatFile, splitFov)
    else:
        sam.statAllPE(totalSamStatFile, splitFov)


#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)
    ArgParser.add_argument("-s", "--splitFov", action="store", dest="splitFov", default=False, help="Get one result file or multi-fovs")
    ArgParser.add_argument("-p", "--PE", action="store_true", dest="pe", default=False, help="Process PE data")

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 2:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    
    (samFile, totalSamStatFile) = args
    ############################# Main Body #############################
    #startTime = datetime.datetime.now()
    #print 'Start Time: %s\n' % startTime.strftime("%Y-%m-%d %H:%M:%S")

    readSam(samFile, totalSamStatFile, para.splitFov, para.pe)

    #endTime = datetime.datetime.now()
    #print 'End Time: %s\n' % endTime.strftime("%Y-%m-%d %H:%M:%S")
    #print 'Generate report spend time %s\n' % (endTime - startTime)

  
#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()
