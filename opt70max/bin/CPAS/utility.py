import os, sys
import re
import json

def getBarcodeSplitRate(barcodeSplitFile, isJson=False):
    ''' read BarcodeStat.txt, extract split rate '''
    if not os.path.exists(barcodeSplitFile):
        return

    ### 1. Extract all barcode's split rate into a dict
    barcodeSplitRateDict = {}
    totalSplitRate = None

    with open(barcodeSplitFile, 'r') as fhIn:
        for line in fhIn:
            if re.match("#SpeciesNO", line): 
                continue
            if re.match("Total", line):
                line = line.strip().split()
                totalSplitRate = round(float(line[-1])*100, 2)
                continue
                
            line = line.strip().split()
            if line[0].startswith('barcode'):
                barcodeSplitRateDict[line[0].strip('barcode')] = round(float(line[-1])*100, 2)
            else:
                barcodeSplitRateDict[line[0]] = round(float(line[-1])*100, 2)

    ### 2. The barcodes which split rate less than 1 will be classified to a group named other
    filteredDict = {}
    filteredDict = dict(filter(lambda item: item[1] > 1, barcodeSplitRateDict.iteritems()))
    #if len(filteredDict) < len(barcodeSplitRateDict):
    #    filteredDict['other(<1)'] = getListAveValue([value for value in barcodeSplitRateDict.itervalues() if value < 1])

    if isJson and filteredDict:
        jsonDict = {}
        try:
            jsonDict['xTag'] = sorted(map(int, filteredDict.keys()))
            jsonDict['splitRate'] = [filteredDict[str(k)] for k in jsonDict['xTag']]
        except Exception, e:
            jsonDict['xTag'] = sorted(filteredDict.keys())
            jsonDict['splitRate'] = [filteredDict[k] for k in jsonDict['xTag']]

        filteredDict = json.dumps(jsonDict, sort_keys=True)

    return filteredDict, totalSplitRate

def getListAveValue(lst, precision=4):
    aveValue = 0.0

    try:
        aveValue = round(sum(lst) / len(lst), precision)
    except ZeroDivisionError:
        pass

    return aveValue

def splitList(lst, value=0.0):
    ''' split list by value '''
    pos = []
    for idx, s in enumerate(lst):
        if s == value:
            pos.append(idx)
    return pos

def checkSplitList(lst, pos):
    ''' check is correct for splited list '''
    possiblePos = set([0, len(lst)/2 -1, len(lst)/2, len(lst)-1])
    pos = set(pos)
    iscorrect = False

    if pos.issubset(possiblePos):
        iscorrect = True
    return iscorrect



if __name__ == "__main__":
    # Test getBarcodeSplitRate
    d = getBarcodeSplitRate(sys.argv[1], isJson=True)
    print d


