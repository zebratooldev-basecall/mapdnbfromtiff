#!/usr/bin/env python
#coding: utf8
###### Document Decription
''' New cat with Object Orient for online version.
    It is useful for cat files of all fovs to one file,
    usually applied to qc, fqStat, mapInfo
'''

###### Import Modules
import sys, os
from copy import deepcopy
import re

###### Version and Date
PROG_VERSION = '2.0.0'
PROG_DATE = '2015-12-23'

###### Chang Log
'''
    2.0.0   2015-12-23  Re-construct cat
'''

###### Usage
USAGE = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <dataAddr>
''' % (PROG_VERSION, PROG_DATE, os.path.basename(sys.argv[0]))

######## Global Variable
FQ_STAT_TEMPLATE = \
'''#Name\t%(newFqName)s
#PhredQual\t%(phred)d
#ReadNum\t%(readNum)d
#BaseNum\t%(baseNum)d
#N_Count\t%(N_count)d\t%(N_count_per).2f
#GC%%\t%(GC_per).2f
#>Q10%%\t%(Q10_per).2f
#>Q20%%\t%(Q20_per).2f
#>Q30%%\t%(Q30_per).2f
#>Q40%%\t%(Q40_per).2f
#EstErr%%\t%(Est_err_per).2f
#Pos\tA\tC\tG\tT\tN\t'''

FLAGS = ['1', '2', 'Total']
PHRED = 33
#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class Cat(object):
    ''' The base class for cat qc, fqStat, mapStat '''
    def __init__(self, is_pe=False, outfile=None):
        self.is_pe = is_pe
        self.outfile = outfile

    def cat(self, lst):
        ''' main function that called by subclass'''
        if len(lst) == 0:
            return None
        else:
            single_str = ''
            if os.path.isfile(lst[0]):
                if self.is_pe:
                    single_str = self.cat_file_pe(lst)
                else:
                    single_str = self.cat_file(lst)
            else:
                if self.is_pe:
                    single_str = self.cat_str_pe(lst)
                else:
                    single_str = self.cat_str(lst)

            if self.outfile:
                with open(self.outfile, 'w') as fh_out:
                    fh_out.write(single_str)
            return single_str

    def cat_str(self, str_list):
        ''' cat from str '''
        single_str = ''
        if len(str_list) == 1:
            single_str = str_list[0]
        return single_str

    def cat_file(self, file_list):
        ''' cat from file '''
        str_list = []
        for f in file_list:
            s = read_file(f)
            if s: str_list.append(s)
        return self.cat_str(str_list)

    def cat_str_pe(self, str_list):
        ''' cat from str, in pe mode '''
        split_list = []

        for pe_str in str_list:
            if not pe_str:
                continue
            pe_str = pe_str.strip().split('\n')
            one_fov = []
            tmp = []
            skip_line = False
            for line in pe_str:
                if line.startswith('-'):
                    skip_line = True
                    if tmp:
                        one_fov.append('\n'.join(tmp))
                        tmp = []
                    else:
                        tmp = []
                else:
                    if skip_line:
                        tmp.append(line)
            one_fov.append('\n'.join(tmp))
            split_list.append(one_fov)

        # Judge the split str's size is same or not
        size = set([len(v) for v in split_list])
        if len(size) > 1:
            print "The size of split str is not same."
            sys.exit(-1)

        single_str = ''
        for pos in xrange(3):
            single_str += '----------read%s\n' % FLAGS[pos]
            str_list = [split_list[i][pos] for i in xrange(len(split_list))]
            new_str = self.cat_str(str_list)
            if new_str: single_str += new_str

        return single_str

    def cat_file_pe(self, file_list):
        ''' cat from file, in pe mode '''
        str_list = []
        for f in file_list:
            s = read_file(f)
            if s: str_list.append(s)
        return self.cat_str_pe(str_list)

    def create_str_pe(self, lst):
        ''' creat pe str from multi se str '''
        if len(lst) != 2:
            return None

        lst.append(self.cat_str(lst))
        single_str = ''
        for pos in xrange(3):
            single_str += '----------read%s\n' % FLAGS[pos]
            if lst[pos]: single_str += lst[pos]
            else: return None
        return single_str

    def create_file_pe(self, lst):
        ''' creat pe str from multi se file '''
        str_list = []
        for f in lst:
            s = read_file(f)
            if s: str_list.append(s)
        return self.create_str_pe(str_list)

class CatFqStat(Cat):
    ''' Inherited from class Cat, use for cat fastq statistical info '''
    def __init__(self, is_pe=False, outfile=None):
        Cat.__init__(self, is_pe, outfile)

    def cat_str(self, str_list):
        ''' rewrite this function for fq stat '''
        array = []
        phred = PHRED
        readNumList = []
        ### 1. Travels all string in list
        for fqStat in str_list:
            # Extract info from single string
            singleFqStat = []
            readNum = 0
            fqStat = fqStat.strip().split('\n')
            for line in fqStat:
                if line.startswith('#'):
                    line = line.strip('#').split()
                    if line[0] == 'ReadNum' and line[1] == '0':
                        break
                    elif line[0] == 'ReadNum':
                        readNum = int(line[1])
                else:
                    try:
                        singleFqStat.append(map(int, line.strip().split()[1:-1]))
                    except Exception as e:
                        singleFqStat = []
                        break
            if not singleFqStat:
                continue
            readNumList.append(readNum)
            # Merge to the total array
            if not array:
                array = singleFqStat
            else:
                longerArray = array if len(array) > len(singleFqStat) else singleFqStat
                tmp = [[i+j for i, j in zip(m, n)] for m, n in zip(array, singleFqStat)]
                tmp.extend(longerArray[len(tmp):])
                array = tmp
        if not readNumList:
            return None
        readNum = sum(readNumList)

        ### 2. Calculate the read num and other values.
        cycle = len(array)
        for i in xrange(cycle): array[i].insert(0, i+1)

        baseNum = 0
        N_count = 0
        GC_count = 0
        Q10_count = 0
        Q20_count = 0
        Q30_count = 0
        Q40_count = 0
        for i in array:
            baseNum += int(sum(i[1:6]))
            N_count += int(i[5])
            GC_count += int(sum(i[2:4]))
            Q10_count += int(sum(i[16:]))
            Q20_count += int(sum(i[26:]))
            Q30_count += int(sum(i[36:]))
            Q40_count += int(sum(i[46:]))

        N_count_per = round((float(N_count)/baseNum)*100, 2)
        GC_per = round(float(GC_count)*100 / (baseNum - N_count), 2)
        Q10_per = round(float(Q10_count)*100 / baseNum, 2)
        Q20_per = round(float(Q20_count)*100 / baseNum, 2)
        Q30_per = round(float(Q30_count)*100 / baseNum, 2)
        Q40_per = round(float(Q40_count)*100 / baseNum, 2)

        errList = []
        for c in xrange(cycle):
            qual_len = len(array[c]) - 6
            lst = [1.0/pow(10, i*(0.1)) * (array[c][i+6]/float(readNum)) for i in xrange(qual_len)]
            errList.append(round(sum(lst) * 100, 4))
        Est_err_per = sum(errList) / len(errList)

        ### 3. Output the info of total fqStat.
        newFqName = 'mergedFq'
        totalFqStatString = FQ_STAT_TEMPLATE % vars()
        totalFqStatString += '\t'.join(map(str, xrange(qual_len))) + '\tErr%\n'
        for i in xrange(cycle):
            totalFqStatString += '\t'.join(map(str, array[i]))
            totalFqStatString += '\t%.4f\n' % errList[i]

        return totalFqStatString

class CatMapStat(Cat):
    ''' Inherited from class Cat, use for cat mapping statistical info '''
    def __init__(self, is_pe=False, outfile=None):
        Cat.__init__(self, is_pe, outfile)

    def cat_str(self, str_list):
        ''' rewrite this function for map stat '''
        mapDict = {}
        maxReadLength = 0
        ### 1. Travels all map string to get all data.
        for mapInfoStr in str_list:
            fovDict = {}
            mapInfoStr = mapInfoStr.strip().split('\n')
            for line in mapInfoStr:
                if line.startswith('#'):
                    continue
                line = line.strip().split()
                if line[0] == 'MaxReadsLength':
                    maxReadLength = int(line[1]) if int(line[1]) > maxReadLength else maxReadLength
                    continue
                fovDict[line[0]] = map(str2digit, line[1:])

            if len(mapDict) == 0:
                mapDict = deepcopy(fovDict)
            else:
                for k in fovDict.keys():
                    if k not in mapDict:
                        mapDict[k] = fovDict[k]
                        continue
                    old = mapDict[k]
                    new = fovDict[k]
                    longerArray = old if len(old) > len(new) else new
                    tmp = [i+j for i, j in zip(old, new)]
                    tmp.extend(longerArray[len(tmp):])
                    mapDict[k] = tmp

        if maxReadLength == 0:
            return None

        ### 2. Process the data.
        mapDict['MaxReadsLength'] = [maxReadLength]
        totalReads = float(mapDict['TotalReads'][0])
        mappedReads = float(mapDict['MappedReads'][0])
        options = ['MappedReads', 'UniqMappedReads', 'EffectiveReads', 'ConcordantReads',\
                    'ReadsWithMismatch', 'ReadsWithGap', 'ReadMappedReverse']
        for k in options:
            if k not in mapDict:
                continue
            mapDict[k][0] = int(mapDict[k][0])
            try:
                mapDict[k][1] = round(mapDict[k][0]/totalReads, 4)
            except ZeroDivisionError as e:
                mapDict[k][1] = round(0.0, 4)
            try:
                mapDict[k][2] = round(mapDict[k][0]/mappedReads, 4)
            except ZeroDivisionError as e:
                mapDict[k][2] = round(0.0, 4)
        try:
            mapDict['MismatchRate'][0] = \
                    round(sum(map(float, mapDict['MismatchDistribution'])) / (maxReadLength * mappedReads), 4)
        except ZeroDivisionError as e:
            mapDict['MismatchRate'][0] = round(0.0, 4)

        ### 3. Output the mapInfo.
        allInfo = ['MaxReadsLength', 'TotalReads'] + options + ['MismatchNumberPerReads',\
                    'GapNumberPerReads', 'MismatchRate', 'MismatchDistribution', 'GapDistribution']
        outString = '#%s\n' % 'mergedSam'
        for k in allInfo:
            if k in mapDict:
                outString += '%s\t' % (k,) + '\t'.join(map(str, mapDict[k])) + '\n'
        misTypeKeys = filter(lambda k: k.startswith('MisType'), mapDict.keys())
        misTypeKeys.sort()
        for k in misTypeKeys:
            outString += '%s\t' % (k,) + '\t'.join(map(str, mapDict[k])) + '\n'

        return outString

    def cat_str_pe(self, str_list):
        ''' rewrite this function, because there is insertSize in map stat'''
        mainStr = Cat.cat_str_pe(self, str_list)

        insetSizeStr = '#insertSize\n'
        totalDict = {}
        for idx, pe_str in enumerate(str_list):
            if not pe_str:
                continue
            pe_str = pe_str.strip().split('\n')
            if pe_str[-3] != '#insertSize':
                continue
            pe_str = map(lambda l: l.strip().split(), pe_str[-2:])
            singleDict = dict(zip(map(int, pe_str[0]), pe_str[1]))
            for k, v in singleDict.iteritems():
                if k not in totalDict: totalDict[k] = int(v)
                else: totalDict[k] += int(v)
        insetSizeStr += '\t'.join(map(str, sorted(totalDict.keys()))) + '\n'
        insetSizeStr += '\t'.join(map(str, [totalDict[k] for k in sorted(totalDict.keys())])) + '\n'

        return mainStr + insetSizeStr


class CatQcStat(Cat):
    ''' Inherited from class Cat, use for cat qc statistical info '''
    def __init__(self, outfile=None):
        Cat.__init__(self, False, outfile)
        self.offsetThreshold = '-99999'

    def cat_str(self, str_list):
        ''' rewrite this function for qc stat '''
        ### 1. Travels all fovs to cal all infos
        array = {}

        count = 0
        read1Lenght = 0
        for qcStr in str_list:
            data = {}
            qcStr = qcStr.strip().split('\n')
            for pos in xrange(len(qcStr)/2):
                infoLine = qcStr[pos*2]
                valueLine = qcStr[pos*2 + 1]
                value = valueLine.strip().split()
                if not infoLine:
                    break

                info = infoLine.strip('#').split()
                try:
                    if info[0] == 'movement':
                        value = map(lambda x: x if x != self.offsetThreshold else '0', value)
                    if len(info) == 3 and info[0] == 'lag':
                        read1Lenght = int(info[2]) / 4
                    data[info[0]] = map(str2digit, value)
                except Exception as e:
                    data = {}
                    raise e
                    break
            if not data:
                continue
            if len(array) == 0:
                array = data
            elif len(array) != 0 and len(data) == len(array):
                for i in data.keys():
                    for j in xrange(len(data[i])):
                        array[i][j] += data[i][j]
            else:
                continue
            count += 1

        ### 2. Cal the mean value.
        if count == 0:
            return None
        else:
            for i in array.keys():
                if i == 'NUMDNBS':
                    continue
                for j in xrange(len(array[i])):
                    array[i][j] /= count

        ### 3. Output basecallInfo.
        outQcString = ''
        options = ['NUMDNBS', 'TOTALGRR', 'SIGNAL', 'BACKGROUND', 'SNR', 'RHO',\
                    'BIC', 'FIT', 'GRR', 'accGRR', 'lag', 'runon', 'movement']
        for k in options:
            if k not in array:
                continue
            if k in ['lag', 'runon']:
                if read1Lenght == 0:
                    outQcString += '#%s\t%d\n' % (k, len(array[k]))
                else:
                    outQcString += '#%s\t%d\t%d\n' % (k, len(array[k]), read1Lenght*4)

            else:
                outQcString += '#%s\t%d\n' % (k, len(array[k]))
            outQcString += '\t'.join(map(str, array[k])) + '\n'

        return outQcString

##########################################################################
############################  BEGIN Function  ############################
##########################################################################
def str2digit(v):
    ''' transform string to digit '''
    try:
        if re.search(r'\.', v) or re.search(r'e', v): v = float(v)
        else: v = int(v)
    except ValueError as e:
        raise e
    else:
        return v

def read_file(f):
    ''' read file and store string else return None '''
    if not os.path.exists(f): return None

    with open(f, 'r') as fh_in:
        s = fh_in.read()
    return s

######################################################################
############################  BEGIN Main  ############################
######################################################################


#################################
##
##   Main function of program.
##
#################################
def Main():

    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage=USAGE, version=PROG_VERSION)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 0:
        ArgParser.print_help()
        print >>sys.stderr, "ERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        pass

    ############################# Main Body #############################
    addr = r'E:\processEXE\ZFX\Get4Result\output\PEBarcode'
    fqList = [os.path.join(addr, 'B1234567_L02_1_1.fq.fqStat.txt'),\
                os.path.join(addr, 'B1234567_L02_1_2.fq.fqStat.txt')]
    catfq = CatFqStat()
    catfq.cat(fqList)


#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    Main()


