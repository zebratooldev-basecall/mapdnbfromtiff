#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
reload(sys)
sys.setdefaultencoding('utf8')
import re
import glob
import json
import string
import time
import math
# import pprint
# import cPickle as pickle
from qcReport import analysis
from summaryReport import transformReport, extractSummaryBio
from samStatReport import samStat

###### Document Decription
'''
        1.1.4   11.11   Fix a bug that display error when only have mapping info, no qc info
        
        1.2.0   2015.11.30  Fix a bug that heatmap can't generated when some fov in error stat
        1.2.1   2015.12.7   Change offset's display from last cycle's A channel to max values in all
                            cycles and all channels
        1.3.0   2016.01.28  When there is no input file, print error info and exit the program than
                            throw a exception.

        1.4.0   2016.02.16  Change the heatmap's output format so it suit new heatmap with new data format

'''

###### Version and Date
prog_version = '1.4.0'
prog_date = '2015-04-03'

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <outHeatmapReport> <outBestFovReport>
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class callCollection(object):
    """docstring for callCollection"""
    
    def __init__(self, totalQcFile=False, totalMapFile=False):
        self.reportPara = {}
        self.statData = {}
        self.totalQcFile = totalQcFile
        self.totalMapFile = totalMapFile

        if not self.totalMapFile or not os.path.exists(totalMapFile):
            self.nomap = True
        else:
            self.nomap = False

        if totalMapFile and totalQcFile:
            self.option = [
                           'GRR',
                           'accGRR',
                           'offset_x',
                           'offset_y',
                           'MappingRate',
                           'UniqMappingRate',
                           'ConcordantMappingRate',
                           'MismatchRate'
                           ]

            self.limit = {  
                            "GRR": {"min": "valmin", "max": "valmax", "type": "maxBest"},
                            "accGRR": {"min": "valmin", "max": "valmax", "type": "maxBest"},
                            'offset_x': {"min": 0, "max": 40, "type": "minBest"},
                            'offset_y': {"min": 0, "max": 40, "type": "minBest"},
                            "MappingRate": {"min": 40, "max": 90, "type": "maxBest"},
                            "UniqMappingRate": {"min": "valmin", "max": "valmax", "type": "maxBest"},
                            "ConcordantMappingRate": {"min": "valmin", "max": "valmax", "type": "maxBest"},
                            "MismatchRate": {"min": "valmin", "max": 5, "type": "minBest"},
                          }
        elif totalQcFile and not totalMapFile:
            self.option = [
                           'GRR',
                            'accGRR',
                           'offset_x',
                           'offset_y',
                           ]

            self.limit = {  
                            "GRR": {"min": "valmin", "max": "valmax", "type": "maxBest"},
                           "accGRR": {"min": "valmin", "max": "valmax", "type": "maxBest"},
                            'offset_x': {"min": 0, "max": 40, "type": "minBest"},
                            'offset_y': {"min": 0, "max": 40, "type": "minBest"},
                          }
        elif not totalQcFile and totalMapFile:
            self.option = [
                           'MappingRate',
                           'UniqMappingRate',
                           'ConcordantMappingRate',
                           'MismatchRate'
                           ]

            self.limit = {  
                            "MappingRate": {"min": 40, "max": 90, "type": "maxBest"},
                            "UniqMappingRate": {"min": "valmin", "max": "valmax", "type": "maxBest"},
                            "ConcordantMappingRate": {"min": "valmin", "max": "valmax", "type": "maxBest"},
                            "MismatchRate": {"min": "valmin", "max": 5, "type": "minBest"},
                          }
        else:
            print "No files input. So can't generate heatmap report!"
            raise Exception("No files input")


            
        self.errorDict = { "ERROR1": "Missing image",
                           "ERROR2": "Can not detect track",
                           "ERROR3": "Too large offest"
                         }
        # super(callCollection, self).__init__()

    def __str__(self):
        return json.dumps(self.statData, indent=4, sort_keys=True)
        

    def getFovStat(self):
        #### basecall info
        if self.totalQcFile and os.path.exists(self.totalQcFile):
            with open(self.totalQcFile, 'r') as fhIn:
                totalFov = fhIn.read()
                if re.search(r'\r\n', totalFov):
                    totalFov = re.sub(r'\r\n', '\n', totalFov)
                totalFov = totalFov.split('#FOV\n')
                for fovData in totalFov:
                    if not fovData:
                        continue
                    fovData = fovData.strip().split('\n')
                    fov = fovData[0]
                    if fov not in self.statData:
                        self.statData[fov] = {"fov": fov}
                    self.statData[fov] = {"fov": fov}
                    self.getBaseCallInfo(fovData[1:], self.statData[fov])

        #### map info
        if self.totalMapFile and os.path.exists(self.totalMapFile):
            with open(self.totalMapFile, 'r') as fhIn:
                totalFov = fhIn.read()
                totalFov = totalFov.split('#fov\n')
                for fovData in totalFov:
                    if not fovData:
                        continue
                    fovData = fovData.strip().split('\n')
                    fov = fovData[0]
                    if fov not in self.statData:
                        self.statData[fov] = {"fov": fov}
                    self.getMapStat(fovData[1:], self.statData[fov])

        return self.statData

    def getBaseCallInfo(self, f, d):
        option = { 
                   "TOTALGRR": "GRR",
                   "accGRR": "accGRR",
                   "movement": "offset"
                  }
        ########### Open and read input file
        for i in xrange(len(f)/2):
            idLine = f[i*2]
            valueLine = f[i*2 + 1]

            info = idLine.strip('#').split()[0]
            
            if info in option:
                valueLine = valueLine.strip().split()
                if len(valueLine) <= 0:
                    break
               
                value = valueLine[-1]
                if info == 'movement':
                    #value = round(((float(valueLine[-7])**2 + float(valueLine[-8])**2)**0.5), 2)
                    #d['offset_x'] = round(math.fabs(float(valueLine[-8])), 2)
                    #d['offset_y'] = round(math.fabs(float(valueLine[-7])), 2)
                    # change the display
                    d['offset_x'] = round(max(map(math.fabs, map(float, valueLine[::2]))), 2)
                    d['offset_y'] = round(max(map(math.fabs, map(float, valueLine[1::2]))), 2)
                    continue
                elif re.search(r'\.', value):
                    value = round(float(value)*100, 2)
                else:
                    value = int(value)
                d[option[info]] = value
        
        return d
   
    def getMapStat(self, f, d):
        option = { "MappedReads": "MappingRate",
                   "UniqMappedReads": "UniqMappingRate",
                   "ConcordantReads": "ConcordantMappingRate",
                   "MismatchRate": "MismatchRate" }

        ########### Open and read input file
        for line in f:
            info = line.split()
            if info[0] == "MismatchRate":
                d[option[info[0]]] = round(float(info[1])*100, 2)
            elif info[0] in option:
                d[option[info[0]]] = round(float(info[2])*100, 2)

            ### Add to cal rank.
            if info[0] == 'ConcordantReads':
                d[info[0]] = int(info[1])
            if info[0] == 'MappedReads':
                d[info[0]] = int(info[1])

        return d

    def getErrStat(self, f, d):
        ########### Open and read input file
        try:
            fhIn = open(f, 'r')
        except Exception as e:
            raise e
        
        for line in fhIn:
            if line.startswith('[ERROR'):
                m = re.match(r'\[(ERROR\d+)\]\[(C\d+R\d+)\]\[(\d+)\]\[([ACGT])\](.*)', line)
                if m:
                    if m.group(2) not in d:
                        d[m.group(2)] = {"fov": m.group(2)}
                    d[m.group(2)]['Error'] = "%s: %s, cycle %s, channel %s, %s" % (m.group(1), m.group(2), m.group(3), m.group(4), self.errorDict[m.group(1)])
        fhIn.close
        return d

    def createPara(self):
        fovData = []
        for k in sorted(self.statData.keys()):
            tmpList = [k]+[self.statData[k][v] for v in self.option if v in self.statData[k]]
            fovData.append(tmpList)
        self.reportPara['fovData'] = json.dumps(fovData, sort_keys=True)
        #self.reportPara['fovData'] = json.dumps(self.statData, sort_keys=True)
        self.reportPara['option'] = str(self.option)
        self.reportPara['limit'] = json.dumps(self.limit, sort_keys=True)
        
        ### rank
        if not self.nomap:
            self.reportPara['rankInfoByMappedReads'] = self.addRankByMappedReads()
        else:
            self.reportPara['rankInfoByAccGrr'] = self.addRankByAccGrr()
        
    def addRankByMappedReads(self):
        if len(self.option) == 8:
            option = ['fov', 'GRR', 'accGRR', 'MappingRate', 'MappedReads', 'ConcordantReads']
            th = ['Fov', 'GRR%', 'accGRR%', 'MappingRate%', 'MappedReads', 'ConcordantReads','Rank']
        else:
            option = ['fov', 'MappingRate', 'MappedReads', 'ConcordantReads']
            th = ['Fov', 'MappingRate%', 'MappedReads', 'ConcordantReads', 'Rank']

        ### Construct dict to rank
        rank = {}

        for i in self.statData.keys():
            if 'ConcordantReads' in self.statData[i]:
                rank[i] = self.statData[i]['ConcordantReads']
           
        rank = sorted(rank.iteritems(), key=lambda a:a[1], reverse=True)

        result = []
        result.append(th)
        num = 1
        for i in rank:
            if num <= 100:
                #result[num]['Rank'] = num
                data = self.statData[i[0]]
                tmp = []
                for k in option:
                    if k in data:
                        tmp.append(data[k])
                tmp.append(num)
                result.append(tmp)
            num += 1

        return result

    def addRankByAccGrr(self):
        option = ['fov', 'GRR', 'accGRR']
        th = ['Fov', 'GRR%', 'accGRR%', 'Rank']

        ### Construct dict to rank
        rank = {}

        for i in self.statData.keys():
            if 'accGRR' in self.statData[i]:
                rank[i] = self.statData[i]['accGRR']
           
        rank = sorted(rank.iteritems(), key=lambda a:a[1], reverse=True)

        result = []
        result.append(th)
        num = 1
        for i in rank:
            if num <= 100:
                data = self.statData[i[0]]
                tmp = []
                for k in option:
                    if k in data:
                        tmp.append(data[k])
                tmp.append(num)
                result.append(tmp)
            num += 1

        return result
        
    def getBestFov(self):
        if not self.nomap:
            return self.reportPara['rankInfoByMappedReads'][1][0]
        else:
            return self.reportPara['rankInfoByAccGrr'][1][0]

    def getTotalFov(self, searchDir):
        maxFov = (0, 0)

        dataFile = os.path.join(searchDir, 'FileName.txt')
        if not os.path.exists(dataFile):
            return maxFov

        with open(dataFile, 'r') as fhIn:
            line = fhIn.readline()
            line = fhIn.readline()
            line = line.strip().split()
            if len(line) == 3:
                maxFov = tuple(map(int, line[1:3]))

        self.setRCMax(maxFov)
        #print maxFov

        return maxFov

    def createReport(self, htmlFile, templateReport):
        if templateReport == None:
            templateReport = os.path.join(os.path.dirname(sys.argv[0]), 'templates', 'heatmapReportTmpl.html')
        self.createPara()
        #self.setRCMax(10, 5)
        self.transformReport(templateReport, htmlFile, self.reportPara)
        
    def transformReport(self, templateReport, newReport, jsonData):
        fhIn = open(templateReport, 'r')
        template = fhIn.read()
        fhIn.close()
        
        allVar = ['fovData', 'option', 'limit', 'rankInfoByMappedReads', 'rankInfoByAccGrr', 'maxRow', 'maxCol']
        # Construct a string template and replace json data
        newStr = string.Template(template)
        newStr = newStr.safe_substitute(jsonData)

        notFoundDict = dict.fromkeys([k for k in allVar if k not in jsonData], 'null')
        newStr = string.Template(newStr)
        newStr = newStr.safe_substitute(notFoundDict)

        fhOut = open(newReport, 'w')
        fhOut.write(newStr)
        fhOut.close()

    def setRCMax(self, maxFov):
        self.reportPara['maxCol'] = maxFov[0]
        self.reportPara['maxRow'] = maxFov[1]

class BestFovReport(object):
    def __init__(self, fov, totalMapFile, totalQcFile, pe):
        self.fov = fov
        self.totalMapFile = totalMapFile
        self.totalQcFile = totalQcFile
        self.pe = pe

    def splitTotalFile(self):
        jsonData = {}

        if self.totalQcFile and os.path.exists(self.totalQcFile):
            with open(self.totalQcFile, 'r') as fhIn:
                totalFov = fhIn.read()
                if re.search(r'\r\n', totalFov):
                    totalFov = re.sub(r'\r\n', '\n', totalFov)
                totalFov = totalFov.split('#FOV\n')
                for fovData in totalFov:
                    if not fovData:
                        continue
                    fovData = fovData.strip().split('\n')
                    fov = fovData[0]
                    if fov == self.fov:
                        self.qcInfoStr = '\n'.join(fovData[1:])
                        jsonData.update(self.geneQcData())

        #### map info
        if self.totalMapFile and os.path.exists(self.totalMapFile):
            with open(self.totalMapFile, 'r') as fhIn:
                totalFov = fhIn.read()
                totalFov = totalFov.split('#fov\n')
                for fovData in totalFov:
                    if not fovData:
                        continue
                    fovData = fovData.strip().split('\n')
                    fov = fovData[0]
                    if fov == self.fov:
                        self.mapInfoStr = '\n'.join(fovData[1:])
                        jsonData.update(self.geneMapData())

        return jsonData

    def geneQcData(self):
        qcStat = analysis(None)
        qcStat.readInfoFromStr(self.qcInfoStr)
        return qcStat.geneQcJsonData()

    def geneMapData(self):
        sam = samStat(None, self.fov)
        if self.pe:
            sam.createReportParaPEFromStr(self.mapInfoStr)
        else:
            sam.createReportParaFromStr(self.mapInfoStr)
        return sam.geneSamJsonData()


    def generateReport(self, outReportHtml, reference='Ecoli.fa', templateReport=None, bioFile=None):
        newReport = outReportHtml

        jsonData = {}
        reportTitle = 'Analysis Report of %s' % (self.fov)
        jsonData['reportTitle'] = '"%s"' % reportTitle
        reportTime = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        jsonData['reportTime'] = '"%s"' % reportTime

        jsonData.update(self.splitTotalFile())

        extractSummaryBio(jsonData, bioFile, reference)
        if templateReport == None:
            templateReport = os.path.join(os.path.dirname(sys.argv[0]), 'templates', 'summaryReportTmpl.html')

        # wipe off background figue
        if 'BACKGROUND' in jsonData:
            del(jsonData['BACKGROUND'])

        transformReport(templateReport, newReport, jsonData)
        
##########################################################################
############################  BEGIN Function  ############################
##########################################################################


######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)
    ArgParser.add_argument("-q", "--totalQcFile", action="store", dest="totalQcFile", default=False, help="Input total qc file")
    ArgParser.add_argument("-m", "--totalMapFile", action="store", dest="totalMapFile", default=False, help="Input total map file")
    ArgParser.add_argument("-t1", "--templateHeatmapHtmlFile", action="store", dest="templateHeatmapHtmlFile", default=None, help="Input template html File of heatmap's report.")
    ArgParser.add_argument("-t2", "--templateBestFovHtmlFile", action="store", dest="templateBestFovHtmlFile", default=None, help="Input template html File of best fov's report.")

    ArgParser.add_argument("-p", "--PE", action="store_true", dest="pe", default=False, help="Process PE data")
    ArgParser.add_argument("-r", "--ref", action="store", dest="reference", default="Ecoli.fa", help="Use specific reference. [%(default)s]")
    ArgParser.add_argument("-b", "--bioFile", action="store", dest="bioFile", default=None, help="Input bio File.")

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 2:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (outHeatmapReport, outBestFovReport) = args

    ############################# Main Body #############################
    try:
        cc = callCollection(totalMapFile=para.totalMapFile, totalQcFile=para.totalQcFile)
    except Exception:
        return -1
    if not cc.getFovStat():
        return -1

    ### From bioFile get driectory of FileName.txt, for heatmap's max col/row
    if para.bioFile is not None:
        searchDir = os.path.dirname(para.bioFile)
        cc.getTotalFov(searchDir)
            
    cc.createReport(outHeatmapReport, para.templateHeatmapHtmlFile)
    bestFov = cc.getBestFov()

    bfd = BestFovReport(bestFov, totalMapFile=para.totalMapFile, totalQcFile=para.totalQcFile, pe=para.pe)
    bfd.generateReport(outBestFovReport, para.reference, para.templateBestFovHtmlFile, para.bioFile)
#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
