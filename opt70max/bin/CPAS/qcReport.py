#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
### Py2.7 encode fix
reload(sys)
sys.setdefaultencoding('utf8')

import re
import copy
import time
import math

import json
import string

from utility import getListAveValue, splitList, checkSplitList
###### Document Decription
'''  '''

###### Version and Date
prog_version = '1.0.0'
prog_date = '2015.05.26'

##### Chang Log
'''
    1.0.1   11.5 Add MaxOffsetX and MaxOffsetY in summary Table

    1.0.2   11.10   Merge the figure of lag, runon's read1 and read2

    1.1.0   2015.12.9   When cal the InitialSignal ForSig RevSig in summaryTable and
                        trendSingal figure, change the soure data from singal to rho

    1.2.0   2015.12.10  Add phaseTable in summaryReport for option

    1.3.0   2016.01.28  1. Cal the maxOffsetX/Y, add a step to minus first cycle's offset
                        2. Add Fit value in summaryTable
                        3. Grr and accGrr multi 100 in figure
    1.3.1   2016.02.02  Add InitialOffsetX/Y in summaryTable and Change FIT% to InitialFIT%
'''

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <basecallFile> <outfile>
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable
PHASE_TABLE = True


#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class analysis(object):

    def __init__(self, basecallFile, doPhase=True):
        self.basecallFile = basecallFile
        self.doPhase = doPhase
        self.rawData = {}
        self.table = {}
        self.figure = {}
        self.readInfo()

        self.addIndividualLagTable = True
        self.seq_type = 'SE'
        self.read1Lenght = 0
        self.read2Lenght = 0

    def readInfo(self):
        if self.basecallFile == None:
            return
        ##### Read basecall info
        try:
            fhIn = open(self.basecallFile, 'r')
        except Exception as e:
            raise e

       
        infoLine = 1
        while infoLine:
            infoLine = fhIn.readline()
            valueLine = fhIn.readline()
            if not infoLine:
                break

            info = infoLine.strip('#').split()
            # Add judge the seq type for SE or PE
            if len(info) == 3 and info[0] == 'lag':
                self.read1Lenght = int(info[2]) / 4
                if info[2] != info[1]:
                    self.seq_type = 'PE'
                    self.read2Lenght = (int(info[1]) - int(info[2])) / 4

            if info[1] == '1':
                self.table[info[0]] = float(valueLine)
            else:
                self.rawData[info[0]] = {info[0]:map(float, valueLine.strip().split())}

        fhIn.close()
        self.table['NUMDNBS'] = int(self.table['NUMDNBS'])
        self.table['TOTALGRR'] = round(self.table['TOTALGRR'] * 100, 2)
        self.table['CYCLE'] = len(self.rawData['SIGNAL']['SIGNAL']) / 4

        for k in self.rawData:
            # Other data can plot directily except lag runon mov
            if k not in ['lag', 'runon', 'movement'] and self.rawData[k][k]:
                self.figure[k] = self.rawData[k]
        if self.doPhase and self.rawData['lag']['lag']:
            try:
                self.processPhase2()
            except:
                self.doPhase = False
   
    def readInfoFromStr(self, qcInfoStr):
        ##### Read basecall info
        qcInfoStr = qcInfoStr.strip().split('\n')
        for pos in xrange(len(qcInfoStr)/2):
            infoLine = qcInfoStr[pos*2]
            valueLine = qcInfoStr[pos*2 + 1]
            if not infoLine:
                break

            info = infoLine.strip('#').split()

            if len(info) == 3 and info[0] == 'lag':
                self.read1Lenght = int(info[2]) / 4
                if info[2] != info[1]:
                    self.seq_type = 'PE'
                    self.read2Lenght = (int(info[1]) - int(info[2])) / 4

            if info[1] == '1':
                self.table[info[0]] = float(valueLine)
            else:
                self.rawData[info[0]] = {info[0]:map(float, valueLine.strip().split())}

        self.table['NUMDNBS'] = int(self.table['NUMDNBS'])
        self.table['TOTALGRR'] = round(self.table['TOTALGRR'] * 100, 2)
        self.table['CYCLE'] = len(self.rawData['SIGNAL']['SIGNAL']) / 4

        for k in self.rawData:
            # Other data can plot directily except lag runon mov
            if k not in ['lag', 'runon', 'movement'] and self.rawData[k][k]:
                self.figure[k] = self.rawData[k]

        if self.doPhase and self.rawData['lag']['lag']:
            try:
                self.processPhase2()
            except:
                self.doPhase = False
        else:
            self.doPhase = False

    def processPhase2(self):
        # 0. Determine the size of pe
        if self.read1Lenght == 0:
            rawLagData = self.rawData['lag']['lag']
            tmp = (list(sum(rawLagData[i:i+4]) / 4 for i in range(0, len(rawLagData), 4)))
            lagStr = '\t'.join(map(str, tmp))
            lagStr = lagStr.strip().split('0.0\t')
            #print len(lagStr)
            self.read1Lenght = len(lagStr[1].strip().split('\t')) + 1
            if len(lagStr) == 3:
                self.read2Lenght = len(lagStr[2].strip().split('\t')) + 1

        #print self.read1Lenght
        #print self.read2Lenght
        # 1. Prepare data for following process
        rawLagData = self.rawData['lag']['lag']
        rawRunonData = self.rawData['runon']['runon']
        option = ['runon', 'lag']
        bases = 'ACGT'
        baseType = ['A', 'C', 'G', 'T', 'AVG']
        value = '0.0\t'
        data = {}
        for idx, rawData in enumerate([rawRunonData, rawLagData]):
            data[option[idx]] = {}
            group = [[rawData[i] for i in xrange(j, len(rawData), len(bases))] for j in xrange(len(bases))]
            # cal average of four channel
            group.append(map(getListAveValue, [rawData[i*4:(i+1)*4] for i in xrange(len(rawData)/len(bases))]))
            for b, lst in enumerate(group):
                data[option[idx]][baseType[b]] = lst

        # 2. Calculate the slope and R^2 using lineress
        lagTable = {}
        for k, v in data.iteritems():
            lagTable[k] = {}
            for b, d in v.iteritems():
                lagTable[k][b] = {}
                #lagStr = '\t'.join(map(str, d)) + '\t'
                #lagStr = filter(None, lagStr.split('%s' % value))
                #lagData = [i.strip().split('\t') for i in lagStr]
                #print k, b, d
                if k == 'lag':
                    lagData = [d[1:self.read1Lenght]]
                    if self.read1Lenght < len(d): lagData.append(d[self.read1Lenght+1:])
                else:
                    lagData = [d[1:self.read1Lenght-1]]
                    if self.read1Lenght < len(d): lagData.append(d[self.read1Lenght:-1])
                #print k, b, lagData
                for idx, readData in enumerate(lagData):
                    xCorData = xrange(1, len(readData)+1)
                    slope, intercept, r2 = self.linregress(xCorData, map(float,readData))
                    lagTable[k][b]['r%d' % (idx+1,)] = (round(slope*100, 2), round(intercept, 4), round(r2, 2))

        # 3. Add Runon Lag figure
        for lagKey in data.iterkeys():
            for channelKey in data[lagKey].iterkeys():
                data[lagKey][channelKey] = map(lambda x: round(x, 4), data[lagKey][channelKey])
        self.figure.update(data)

        # 4. Dump AVG's slope and R^2
        for lag in option:
            if lag in lagTable and 'AVG' in lagTable[lag]:
                tmp = lagTable[lag]['AVG']
                if len(tmp) == 1:
                    self.table['%s%%' % lag] = tmp['r1'][0]
                    self.table['%sR^2' % lag] = tmp['r1'][2]
                elif len(tmp) == 2:
                    for read in tmp:
                        self.table['%s%s%%' % (lag, read[1:])] = tmp[read][0]
                        self.table['%s%sR^2' % (lag, read[1:])] = tmp[read][2]

        # 5. Dump all channel's slope, intercept, and R^2
        if not PHASE_TABLE:
            return

        newDict = {}
        for lag in option:
            for base in baseType:
                if not lag in lagTable or not base in lagTable[lag]:
                    continue
                d = lagTable[lag][base]
                for read in d:
                    #print lag, base, read
                    if read not in newDict:
                        newDict[read] = {}
                        for b in baseType:
                            newDict[read][b] = []
                    newDict[read][base].extend(d[read])

        self.phaseTable = []
        for r in newDict:
            self.phaseTable.append(['Read%s' % r[1:],'Runon%','RunonIntercept','RunonR^2','Lag%','LagIntercept','LagR^2' ])
            for base in baseType:
                if not base in newDict[r]:
                    continue
                d = newDict[r][base]
                d.insert(0, base)
                self.phaseTable.append(d)

    def linregress(self, x, y):
        """Linear regression."""

        ### cal gradient and intercept
        sum_x = 0.
        sum_y = 0.
        sum_xx = 0
        sum_yx = 0.
        for i in range(len(x)):
            sum_x  += x[i]
            sum_xx += x[i] * x[i]
            sum_y  += y[i]
            sum_yx += y[i] * x[i]
        denom = ((len(x) * sum_xx) - (sum_x * sum_x));
        if (math.fabs(denom) < 1.e-12):
            return [-1., -1., 0]
        gradient = ((len(x) * sum_yx - sum_x * sum_y) / denom);
        intercept = ((sum_y - gradient * sum_x) / len(x));
        
        ### cal r^2
        mean_x = sum_x / len(x)
        mean_y = sum_y / len(y)
        up = 0
        down1 = 0
        down2 = 0
        
        for i, j in zip(x, y):
            up += (i-mean_x) * (j-mean_y)
            down1 += (i-mean_x)**2
            down2 += (j-mean_y)**2
        down = math.sqrt(down1) * math.sqrt(down2)
        if (math.fabs(down) < 1.e-12):
            r2 = 0
        else:
            r2 = (up / down) ** 2

        return [gradient, intercept, r2]


    def prepareData(self):
        # Transform format to json, Transform float to int
        option = ['SIGNAL', 'BACKGROUND', 'RHO']
        for k in self.figure.keys():

            if k in option:
                
                data = self.figure[k][k]
                tmp = {}
                con = {0:'A', 1:'C', 2:'G', 3:'T'}
                for pos in xrange(len(data)):
                    tmp.setdefault(con[pos % 4], []).append(int(data[pos]))
                self.figure[k] = tmp

        # Merger Bic Fit to Matrix, and multiply 100 for FIT
        if 'BIC' in self.figure and 'FIT' in self.figure:
            self.figure['Matrix'] = {}
            self.figure['Matrix']['BIC'] = [round(v, 2) for v in self.figure['BIC']['BIC']]
            self.figure['Matrix']['FIT'] = [round(v*100, 2) for v in self.figure['FIT']['FIT']]

        # Transform grr's float
        option = ['GRR', 'accGRR']
        for k in option:
            if k in self.figure:
                self.figure[k][k] = [round(v*100, 4) for v in map(float, self.figure[k][k])]

        # Add trend of signal intensity
        trendOption = 'RHO'
        if trendOption in self.figure:
            data = self.figure[trendOption]
            newData = {}
            for k in data:
                try:
                    newData[k] = [round(float(value)*100/data[k][0], 2) for value in data[k] ]
                except ZeroDivisionError as e:
                    newData[k] = [round(0.0, 2) for value in data[k] ]
                    

            self.figure['trendSignal'] = newData

        # Add movement figure, every cycle has 8 values
        if 'movement' in self.rawData:
            data = self.rawData['movement']['movement']
            mov_x = [round(data[i*8], 2) for i in xrange(len(data)/8)]
            mov_y = [round(data[i*8 + 1], 2) for i in xrange(len(data)/8)]
           
            self.figure['movement'] = {'offset_x': mov_x, 'offset_y': mov_y}

        # Add SNR figure
        option = ['SNR']
        for k in self.figure.keys():
            if k in option:
                data = self.figure[k][k]
                tmp = {}
                con = {0:'A', 1:'C', 2:'G', 3:'T'}
                for pos in xrange(len(data)):
                    tmp.setdefault(con[pos % 4], []).append(round(float(data[pos]), 4))
                self.figure[k] = tmp

        # Add items of maxOffsetXY in QCTable
        self.addMaxOffsetXY()

        # Add initial signal and Forsignal and Revsignal in summaryTable
        self.addSignalTrend()

        # Add Fit value in summaryTable
        self.addFit()

    def geneQcJsonData(self):
        ### The order of qc table's data
        qcTableOrder = ['NUMDNBS', 'CYCLE', 'TOTALGRR', 'runon%', 'runon1%', 'runon2%',\
                        'lag%', 'lag1%', 'lag2%', 'runonR^2', 'runon1R^2', \
                        'runon2R^2', 'lagR^2', 'lag1R^2', 'lag2R^2', \
                        'MaxOffsetX', 'MaxOffsetY', 'InitialOffsetX', 'InitialOffsetY', \
                        'InitialSignal', 'ForSignal%', 'RevSignal%', \
                        'InitialFIT%', 'ForFIT(5_35)%', 'RevFIT(5_35)%']
        option = {
                'NUMDNBS': 'DNBNumber',
                'CYCLE': 'Cycle',
                'TOTALGRR': 'GRR%',
                'runon%': 'Runon%',
                'runon1%': 'Runon1%',
                'runon2%': 'Runon2%',
                'lag%': 'Lag%',
                'lag1%': 'Lag1%',
                'lag2%': 'Lag2%',
                'runonR^2': 'RunonR^2',
                'runon1R^2': 'Runon1R^2',
                'runon2R^2': 'Runon2R^2',
                'lagR^2': 'LagR^2',
                'lag1R^2': 'Lag1R^2',
                'lag2R^2': 'Lag2R^2',
                'MaxOffsetX': 'MaxOffsetX',
                'MaxOffsetY': 'MaxOffsetY',
                'InitialOffsetX': 'InitialOffsetX',
                'InitialOffsetY': 'InitialOffsetY',
                'InitialSignal': 'InitialSignal',
                'ForSignal%': 'ForSignal(5_35)%',
                'RevSignal%': 'RevSignal(5_35)%',
                'InitialFIT%': 'InitialFIT%',
                'ForFIT(5_35)%': 'ForFIT(5_35)%',
                'RevFIT(5_35)%': 'RevFIT(5_35)%'
                }
    
        self.prepareData()
    
        qcTable = []
        qcTable.append([option[k] for k in qcTableOrder if k in self.table])
        tmpList = []
        for k in qcTableOrder:
            if k in self.table:
                tmpList.append(self.table[k])
        qcTable.append(tmpList)

        ### Define all elements of qc report
        jsonData = {}

        jsonData['qcTable'] = qcTable
        for v in ['SIGNAL', 'trendSignal', 'BACKGROUND', 'SNR', 'RHO', 'Matrix', 'GRR', 'accGRR', 'lag', 'runon',\
                    'lag1', 'runon1', 'lag2', 'runon2', 'movement']:
            if v in self.figure:
                jsonData[v] = json.dumps(self.figure[v])

        # Add phaseTable
        if PHASE_TABLE and self.doPhase:
            jsonData['phaseTable'] = self.phaseTable

        #print jsonData['phaseTable']
        #print jsonData['qcTable']
        return jsonData

    def getTotalGRR(self):
        try:
            totalGRR = self.table['TOTALGRR']
        except Exception as e:
            raise e
            totalGRR = None

        return totalGRR

    def addMaxOffsetXY(self):
        if 'movement' in self.figure:
            try:
                x = self.figure['movement']['offset_x']
                y = self.figure['movement']['offset_y']
                MaxOffsetX = '%s' % round(max(map(abs, map(lambda i: i-x[0], x))), 2)
                MaxOffsetY = '%s' % round(max(map(abs, map(lambda i: i-y[0], y))), 2)
                InitialOffsetX = round(x[0], 2)
                InitialOffsetY = round(y[0], 2)
            except:
                pass
            else:
                self.table['MaxOffsetX'] = MaxOffsetX
                self.table['MaxOffsetY'] = MaxOffsetY
                self.table['InitialOffsetX'] = InitialOffsetX
                self.table['InitialOffsetY'] = InitialOffsetY

    def addFit(self):
        option = 'FIT'
        if option in self.figure:
            data = map(lambda x: x*100, self.figure[option][option])
            if not data or self.read1Lenght+self.read2Lenght == 0: return
            #tmp_array = data[:self.read1Lenght+self.read2Lenght]
            #self.table['FIT%'] = round(sum(tmp_array) / len(tmp_array), 2)
            self.table['InitialFIT%'] = round(data[0], 2)
            try:
                read1StartPos = 5 - 1
                read1EndPos = 35 - 1
                if read1EndPos < self.read1Lenght:
                    forFit = round(data[read1EndPos]*100 / data[read1StartPos], 2)
                    self.table['ForFIT(5_35)%'] = forFit
            except Exception, e:
                pass
            if self.seq_type == 'SE':
                return
            try:
                read2StartPos = self.read1Lenght + 5 - 1
                read2EndPos = self.read1Lenght + 35 - 1
                revFit = round(data[read2EndPos]*100 / data[read2StartPos], 2)
                self.table['RevFIT(5_35)%'] = revFit
            except Exception, e:
                pass

    def addSignalTrend(self):
        # Change SIGNAL to RHO for caling the trend
        option = 'RHO'
        if option in self.figure:
            bases = 'ACGT'
            signalData = self.figure[option]

            initialSignal = ','.join(map(str, [signalData[b][0] for b in bases]))
            self.table['InitialSignal'] = initialSignal

            try:
                read1StartPos = 5 - 1
                read1EndPos = 35 - 1
                if read1EndPos < self.read1Lenght:
                    forSignal = ','.join(map(str, [int(signalData[b][read1EndPos]*100 / float(signalData[b][read1StartPos])) for b in bases]))
                    self.table['ForSignal%'] = forSignal
            except Exception, e:
                pass
            if self.seq_type == 'SE':
                return
            try:
                read2StartPos = self.read1Lenght + 5 - 1
                read2EndPos = self.read1Lenght + 35 - 1
                revSignal = ','.join(map(str, [int(signalData[b][read2EndPos]*100 / float(signalData[b][read2StartPos])) for b in bases]))
                self.table['RevSignal%'] = revSignal
            except Exception, e:
                pass
                
##########################################################################
############################  BEGIN Function  ############################
##########################################################################


######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 2:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (basecallFile,  outfile) = args

    ############################# Main Body #############################
    stat = analysis(basecallFile)
    stat.convertHtml(outfile)
    # stat.generateMaterial()
    # stat.createHtml(outfile)
    # print stat

#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
