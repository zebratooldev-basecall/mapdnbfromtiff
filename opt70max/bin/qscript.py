#! /usr/bin/python

import sys
import os
from optparse import OptionParser

SLOTS_PER_MACHINE = 8

def shellescape(arg):
    if arg.find("'") == -1:
        return arg
    result = []
    for ch in arg:
        if ch != "'":
            result.append(ch)
        else:
            result.append("'\\''")
    return "".join(result)


def mkscript(options, args):
    lines = []
    lines.append( "#! /bin/bash" )
    lines.append( "" )

    # name the job
    lines.append( "#$ -N %s" % args[0].split("/")[-1] )

    # environment to pass along to executing script
    lines.append( "#$ -v CGI_HOME,PATH,LD_LIBRARY_PATH,PYTHONPATH" )
    lines.append( "#$ -cwd" )

    # stdout/stderr all goes to $JOB_NAME.o$JOB_ID*
    lines.append( "#$ -o ." )
    lines.append( "#$ -j y" )

    # warn me if something seems amiss...
    lines.append( "#$ -w w" )

    # shell to use... is this needed?
    lines.append( "#$ -S /bin/bash" )

    memoryMb = options.memory * 1024
    slotCount = 1
    if options.hog:
        # hog an entire machine (8 cpu slots)
        lines.append( "#$ -pe smp %d" % SLOTS_PER_MACHINE )
        memoryMb /= SLOTS_PER_MACHINE
        slotCount = SLOTS_PER_MACHINE

    memoryMb = max(128, int(memoryMb))
    lines.append( "#$ -l job_version=3,s_rt=259200,virtual_free=%dM,cgi_slots=%d" % (memoryMb,slotCount) )

    if options.notify:
        lines.append( "#$ -m eas" )

    if options.recipient != '':
        lines.append( "#$ -M %s" % options.recipient )

    # don't dump core
    lines.append( "" )
    lines.append( "ulimit -c 0" )

    # finally... the command to run
    lines.append( "" )
    if options.no_shell_escape:
        lines.append( ' '.join(args) )
    else:
        lines.append( '"%s"' % ('" "'.join(map(shellescape,args))) )
    lines.append( "sts=$?" )

    # make sure the command succeeded
    lines.append( "" )
    teststr = " ] && [ ".join(map(lambda ret: "$sts != "+ret, options.expected_return.split(",")))
    lines.append( "if [ %s ]; then" % teststr )
    lines.append( "    echo 'ERROR: unexpected exit code: '$sts" )
    if not options.no_fail:
        lines.append( "    exit 100" )
    lines.append( "fi" )

    return '\n'.join(lines)


parser = OptionParser()
parser.disable_interspersed_args()
parser.add_option("--hog", action="store_true", default=False,
                  help="this command is a hog -- be sure to use up a full machine")
parser.add_option("--memory", "-m", type=float, default=2.0,
                  help="expected memory usage in gigabytes")
parser.add_option("--expected-return", type=str, default="0",
                  help="comma-separated list of allowable return values")
parser.add_option("--no-shell-escape", action="store_true", default=False,
                  help="do not shell-escape the command line passed in to qscript.py")
parser.add_option("--no-fail", action="store_true", default=False,
                  help="don't notify sge if this command exits with an unexpected return value")
parser.add_option("--notify", action="store_true", default=False,
                  help="send email on job status changes")
parser.add_option("--recipient", type=str, default="",
                  help="email address of job status notifications")
(options, args) = parser.parse_args()
print mkscript(options,args)
