#!/usr/bin/env python

import os
import numpy
from csvparser import Data, loadCsvTable
import csvwriter as CW
import sys
from optparse import OptionParser

def initialize_containers():
    global gc_curves_out
    gc_curves_out = []

#-------from aqc.html.mako--------# 
def calc_genome_coverage(covdir,lane):
    # genome coverage by GC content
    data = loadCsvTable(os.path.join(covdir, "allCoverageByContentGC500.csv"))
    content = numpy.array(map(int,data.getColumnByHeader("Content")))
    baseCount = numpy.array(map(float,data.getColumnByHeader("Count")))
    totalCoverageByContent = numpy.array(map(float,data.getColumnByHeader("calledcvg:weightratio>.99")))
    avgCoverageByContent = totalCoverageByContent / baseCount
    bases = numpy.sum(baseCount)
    totalCoverage = numpy.sum(totalCoverageByContent)
    avgCoverage = float(totalCoverage) / float(bases)
    thresh = 0.6 * avgCoverage
    basesBelowThreshold = numpy.sum(numpy.where(
        avgCoverageByContent < thresh, baseCount,
        numpy.zeros((len(baseCount),), dtype=numpy.int)))
    PCT_GENOME_BY_GC_LT_60PCT_MEAN_CVG = "%.2f" % (100.0 * basesBelowThreshold / bases)

    # calculate normalized coverage and cumulative base percentile for GC curves
    normalizedCoverageByContent = avgCoverageByContent / avgCoverage
    cumulativeBaseCount = numpy.cumsum(baseCount)
    cumulativeBaseCountPercentile = cumulativeBaseCount / float(bases) * 100

    # save curve output to write at once
    # this could probably be optimized
    for i in xrange(len(cumulativeBaseCountPercentile)):
        normCvg = "%.2f" % normalizedCoverageByContent[i]
        bcPercentile = "%.2f" % cumulativeBaseCountPercentile[i]
        outline = [lane,bcPercentile,normCvg,'g']
        gc_curves_out.append(outline)

    return PCT_GENOME_BY_GC_LT_60PCT_MEAN_CVG 

def calc_exome_coverage(covdir,lane):
    # exome coverage by GC content
    try:
        data = loadCsvTable(os.path.join(covdir,
                                         "autoExomeCoverageByContentGC500.csv"))
    except:
        data = None

    bases = 0
    if data is not None:
        baseCount = numpy.array(map(float,data.getColumnByHeader("Count")))
        bases = numpy.sum(baseCount)
    if 0 != bases:
        content = numpy.array(map(int,data.getColumnByHeader("Content")))
        totalCoverageByContent = numpy.array(map(float,data.getColumnByHeader("calledcvg:weightratio>.99")))
        avgCoverageByContent = totalCoverageByContent / baseCount
        totalCoverage = numpy.sum(totalCoverageByContent)
        avgCoverage = float(totalCoverage) / float(bases)
        thresh = 0.6 * avgCoverage
        basesBelowThreshold = numpy.sum(numpy.where(
            avgCoverageByContent < thresh, baseCount,
            numpy.zeros((len(baseCount),), dtype=numpy.int)))
        PCT_EXOME_BY_GC_LT_60PCT_MEAN_CVG = "%.2f" % (100.0 * basesBelowThreshold / bases)
    else:
        PCT_EXOME_BY_GC_LT_60PCT_MEAN_CVG = "NA"

    # calculate normalized coverage and cumulative base percentile for GC curves
    normalizedCoverageByContent = avgCoverageByContent / avgCoverage
    cumulativeBaseCount = numpy.cumsum(baseCount)
    cumulativeBaseCountPercentile = cumulativeBaseCount / float(bases) * 100

    # save curve output to write at once
    # this could probably be optimized
    for i in xrange(len(cumulativeBaseCountPercentile)):
        normCvg = "%.2f" % normalizedCoverageByContent[i]
        bcPercentile = "%.2f" % cumulativeBaseCountPercentile[i]
        outline = [lane,bcPercentile,normCvg,'e']
        gc_curves_out.append(outline)

    return PCT_EXOME_BY_GC_LT_60PCT_MEAN_CVG 

def write_gc_curve(outdir):
    gccurveout = os.path.join(outdir,'coverage_gc_curves.csv')
    fo = open(gccurveout,'w')
    try:
        header = ['slidelane','cumulative_basecount_percentile_by_gc','normalized_coverage','coverage_type']
        fo.write(','.join(map(str,header)) + '\n')
        for outline in gc_curves_out:
            fo.write(','.join(map(str,outline)) + '\n')
    finally:
        fo.close()

def write_gc_stats(outdir,outline):
    gcstatout = os.path.join(outdir,'coverage_gc_stats.csv')
    fo = open(gcstatout,'w')
    try:
        header = ['slidelane','genome_gc_bias_by_coverage','exome_gc_bias_by_coverage']
        fo.write(','.join(map(str,header)) + '\n')
        fo.write(','.join(map(str,outline)) + '\n')
    finally:
        fo.close()

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-d','--coverage-dir', help='directory containing coverage files allCoverageByContentGC500.csv', dest='covdir')
    parser.add_option('-o','--output-dir', help='directory for output file', dest='outdir')
    parser.add_option('-l','--slidelane', help='slidelane on which contam screen was run', dest='lane')

    # parse command line arguments.
    options,args = parser.parse_args(arguments[1:])

    return options.covdir, options.outdir,options.lane

def main(args):
    covdir, outdir, lane = parse_arguments(args)

    initialize_containers()
    
    genome_GC_bias = calc_genome_coverage(covdir,lane)
    exome_GC_bias = calc_exome_coverage(covdir,lane)

    write_gc_curve(outdir)
    outline = [lane, genome_GC_bias, exome_GC_bias]
    write_gc_stats(outdir,outline)

if __name__=='__main__':
    main(sys.argv)
