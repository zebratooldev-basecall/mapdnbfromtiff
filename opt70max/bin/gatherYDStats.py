#! /usr/bin/env python

import sys
from optparse import OptionParser
from os.path import join as pjoin, split as psplit
from glob import glob
import re

class TrioStats:
    def __init__(self, trioFile):
        self.identical = 'N'
        self.compatible = 'N'
        self.incompatible = 'N'
        ff = open(trioFile)
        try:
            for line in ff:
                line = line.strip()
                if line == '':
                    return
                fields = line.split(',')
                if fields[1] == 'identical':
                    self.identical = int(fields[2])
                if fields[1] == 'compatible':
                    self.compatible = int(fields[2])
                if fields[1] == 'incompatible':
                    self.incompatible = int(fields[2])
        finally:
            ff.close()

class SnpDiffStats:
    def __init__(self, snpDiffFile):
        ff = open(snpDiffFile)
        try:
            thisBlock = False
            for line in ff:
                if line.startswith('Locus concordance by fraction'):
                    thisBlock = True
                if not thisBlock:
                    continue
                if line.startswith('total,'):
                    fields = line.strip().split(',')
                    self.discordance = float(fields[1])
                    self.nocall = float(fields[2])
                    return
        finally:
            ff.close()

class KnownXNovelStats:
    def __init__(self,knownXNovelFile):
        ff = open(knownXNovelFile)
        try:
            for line in ff:
                (self.known,self.novel) = map(int,line.split())
        finally:
            ff.close()

def getThresholds(workDir):
    result = []
    for fn in glob(pjoin(workDir, 'ASM', 'variations', 'annotated', 'Variations*.tsv')):
        match = re.match(r'.*Variations((\d+)-(\d+)_ccf(\d+)([^/]*)).tsv', fn)
        if not match:
            raise Exception('failed to determine vt, vt2, ccf for: %s' % fn)
        result.append( (int(match.group(2)),int(match.group(3)),int(match.group(4)),match.group(1)) )
    return sorted(result)

def main():
    parser = OptionParser('usage: %prog [options] work-dirs')
    parser.disable_interspersed_args()

    (options, args) = parser.parse_args()

    print ( 'path,threshold,vt,vt2,ccf,'+
            'trio-ident,trio-compat,trio-incompat,'+
            'snpdiff-discord,snpdiff-nocall,' +
            'snp-known,snp-novel,ins-known,ins-novel,del-known,del-novel' )
    for workDir in args:
        for (vt,vt2,ccf,thresh) in getThresholds(workDir):
            trioStats = TrioStats(pjoin(workDir, 'trio', 'trio_stats_Variations%s.csv' % thresh))
            hsdStats = SnpDiffStats(pjoin(workDir, 'snpdiff', 'snpdiff_hapmap_stats_Variations%s.csv' % thresh))
            isdStats = SnpDiffStats(pjoin(workDir, 'snpdiff', 'snpdiff_infinium_stats_Variations%s.csv' % thresh))
            snpKn = KnownXNovelStats(pjoin(workDir, 'ASM', 'knownXnovel', 'knownXnew.%s.snp' % thresh))
            insKn = KnownXNovelStats(pjoin(workDir, 'ASM', 'knownXnovel', 'knownXnew.%s.ins' % thresh))
            delKn = KnownXNovelStats(pjoin(workDir, 'ASM', 'knownXnovel', 'knownXnew.%s.del' % thresh))

            print ( '%s,%s,%s,%s,%s,%s,%s,%s,%.3f%%,%.3f%%,%d,%d,%d,%d,%d,%d' %
                    ( workDir, thresh, vt, vt2, ccf,
                      trioStats.identical, trioStats.compatible, trioStats.incompatible,
                      100.0*isdStats.discordance, 100.0*hsdStats.nocall,
                      snpKn.known, snpKn.novel,
                      insKn.known, insKn.novel,
                      delKn.known, delKn.novel,
                      ) )

if __name__ == '__main__':
    main()
