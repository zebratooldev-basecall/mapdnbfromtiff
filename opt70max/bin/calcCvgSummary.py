#!/usr/bin/env python

import sys
import numpy as np
from optparse import OptionParser
import os

def initialize_containers():
    global summary_metrics
    summary_metrics = [] ## summary_metrics[(metric_heading, slope_type, value),...]

def write_metrics(outbase):
    outfile = outbase + 'cvg_summary_metrics.tsv'
    fo = open(outfile,'w')
    try:
        fo.write('metric_heading\tmetric_detail\tvalue\n')
        for output in summary_metrics:
            fo.write('\t'.join(map(str,output)) + '\n')
    finally:
        fo.close()

def load_cvg_by_gc(covdir,covfile):
    filebase = covfile.split('.')[0]
    infile = os.path.join(covdir,covfile)

    GCregions = [] 
    GCcvgs = {} ## GCcvgs[metricname] = [0,1,...]

    fi= open(infile)
    try:
        header = fi.readline()
        header = header.rstrip().split(',')
        # set up emtpy list for keeping track of cumulative sums
        for metric_heading in header[2:]:
            metric_heading_full = '%s_%s' % (filebase, metric_heading)
            GCcvgs[metric_heading_full] = []
        for line in fi:
            line = line.rstrip().split(',')
            line = map(int,line)
            
            # add coverage to list
            GCregions.append(line[1])
            
            # add coverages to lists
            for n in xrange(2,len(line)):
                metric_heading = header[n]
                metric_heading_full = '%s_%s' % (filebase, metric_heading)
                metric_value = line[n]
                GCcvgs[metric_heading_full].append(metric_value)
    
    finally:
        fi.close()

    return GCregions, GCcvgs

def load_cvg_summary(covdir,covfile):
    filebase = covfile.split('.')[0]
    infile = os.path.join(covdir,covfile)

    cvg_wts = {}
    cvg = []
    fi = open(infile)
    try:
        header = fi.readline()
        header = header.rstrip().split(',')
        # set up emtpy list for keeping track of cumulative sums
        for metric_heading in header[1:]:
            metric_heading_full = '%s_%s' % (filebase, metric_heading)
            cvg_wts[metric_heading_full] = []
        for line in fi:
            line = line.rstrip().split(',')
            line = map(int,line)
            
            # add coverage to list
            cvg.append(int(line[0]))

            # add coverage weights to lists
            for n in xrange(1,len(line)):
                metric_heading = header[n]
                metric_heading_full = '%s_%s' % (filebase, metric_heading)
                metric_value = line[n]
                cvg_wts[metric_heading_full].append(metric_value)
    finally:
        fi.close()

    return cvg, cvg_wts

def add_summary_metrics_byGC(regionCounts, cvgs):
    for metric, cvg in cvgs.iteritems():
        #calculate mean coverage and cutoff
        cvgmean = np.mean(cvg)
        cutoff = 0.6 * cvgmean
        
        # tally up regions < cvg cutoff
        total_regions = 0
        regions_lt_cutoff = 0
        for n in xrange(0,len(cvg)):
            cvg_value = cvg[n]
            region_count = regionCounts[n]
            total_regions += region_count
            if cvg_value < cutoff:
                regions_lt_cutoff += region_count
        
        frac_lt_cutoff = float(regions_lt_cutoff) / float(total_regions)
        summary_metrics.append((metric, 'frac_lt_pt6_mean', frac_lt_cutoff))

def add_summary_metrics(cvg, cvg_wts):
    for metric, wts in cvg_wts.iteritems():
        
        # way too memory-intensive to expand fully
        wt_total = sum(wts)
        wt_mini = [ float(n)/float(wt_total)* 1000 for n in wts]

        # expand coverage values based on weights
        cvg_expanded = np.repeat(cvg,wt_mini)

        # get x percentile coverage values
        pctl5 = np.percentile(cvg_expanded, 5)
        pctl10 = np.percentile(cvg_expanded, 10)
        pctl50 = np.percentile(cvg_expanded, 50)
        pctl90 = np.percentile(cvg_expanded, 90)
        pctl95 = np.percentile(cvg_expanded, 95)
        cvgmean = np.mean(cvg_expanded)

        # calculate fraction < 0.2 * mean
        cutoff = 0.2 * cvgmean
        total_counts = 0
        lt_cutoff = 0
        for n in xrange(0,len(wts)):
            cvgn = cvg[n]
            wt = wts[n]
            total_counts += wt
            if cvgn < cutoff: 
                lt_cutoff += wt

        frac_lt_cutoff = float(lt_cutoff) / float(total_counts)

        # calculate slopes
        slope_10_90 = (0.9 - 0.1) / (pctl90 - pctl10)
        slope_5_95 = (0.95 - 0.05) / (pctl95 - pctl5)
        slope_50_10 = (0.5 - 0.1) / (pctl50 - pctl10)

        # add to summary_metrics ## summary_metrics[(metric_heading, slope_type, value),...]
        summary_metrics.append((metric, 'frac_lt_pt2_mean', frac_lt_cutoff))
        summary_metrics.append((metric, 'slope_10_90', slope_10_90))
        summary_metrics.append((metric, 'slope_5_95', slope_5_95))
        summary_metrics.append((metric, 'slope_50_10', slope_50_10))

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-d','--coverage-directory', help='directory containing summary .csv files from covRpt', dest='covdir')
    parser.add_option('-o','--output-base', help='base name for output files', dest='outbase')

    # parse command line arguments.
    options,args = parser.parse_args(arguments[1:])

    return options.covdir, options.outbase

def main(args):
    covdir, outbase = parse_arguments(args)

    initialize_containers()

    # summary genome coverage metrics
    cvg_genome, cvg_wts_genome = load_cvg_summary(covdir,'allCoverageSummary.csv')
    add_summary_metrics(cvg_genome,cvg_wts_genome)
    
    # summary exome coverage metrics
    cvg_exome, cvg_wts_exome = load_cvg_summary(covdir, 'allExomeCoverageSummary.csv')
    add_summary_metrics(cvg_exome, cvg_wts_exome)

    # coverage by GC genome metrics
    GCregionCounts50, GCcvgs50 = load_cvg_by_gc(covdir,'allCoverageByContentGC50.csv')
    GCregionCounts500, GCcvgs500 = load_cvg_by_gc(covdir,'allCoverageByContentGC500.csv')
    GCregionCounts1000, GCcvgs1000 = load_cvg_by_gc(covdir,'allCoverageByContentGC1000.csv')
    add_summary_metrics_byGC(GCregionCounts50, GCcvgs50)
    add_summary_metrics_byGC(GCregionCounts500, GCcvgs500)
    add_summary_metrics_byGC(GCregionCounts1000, GCcvgs1000)

    # coverage by GC exome metrics
    GCregionCounts50, GCcvgs50 = load_cvg_by_gc(covdir,'allExomeCoverageByContentGC50.csv')
    GCregionCounts500, GCcvgs500 = load_cvg_by_gc(covdir,'allExomeCoverageByContentGC500.csv')
    GCregionCounts1000, GCcvgs1000 = load_cvg_by_gc(covdir,'allExomeCoverageByContentGC1000.csv')
    add_summary_metrics_byGC(GCregionCounts50, GCcvgs50)
    add_summary_metrics_byGC(GCregionCounts500, GCcvgs500)
    add_summary_metrics_byGC(GCregionCounts1000, GCcvgs1000)

    # write out summary metrics
    write_metrics(outbase)

if __name__=='__main__':
    main(sys.argv)
