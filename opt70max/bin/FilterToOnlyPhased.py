#!/usr/bin/env python

import os
import sys
import re
import getopt
import bz2

class TsvReader(object):
    def __init__(self, ff):
        self.ff = ff
        self.header=""
        while True:
            line = self.ff.readline()
            self.header += line
            if line.startswith('>'): break

        self.hFields = line[1:].rstrip('\r\n').split('\t')


    def close(self):
        self.ff.close()

    def next(self):
        line = self.ff.readline()
        if '' == line:
            return None
        fields = line.rstrip('r\n').split('\t')
        if len(fields) != len(self.hFields):
            print str(len(fields)) + "!=" + str(len(self.hFields))
            print line
            raise Exception('field count mismatch')
        result = {}
        for (name,val) in zip(self.hFields, fields):
            result[name] = val
        return result

#################################################################

def main():

    # parse command line options and update variables
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:", [])
    except getopt.GetoptError:
        sys.exit(usage)

    for opt, arg in opts:
        if opt == "-h":
            sys.exit('Use me properly.')
        elif opt == '-i':
            INFILE = arg
        elif opt == '-o':
            OUTFILE = arg

    if "bz2" in INFILE:
        inf = TsvReader(bz2.BZ2File(INFILE))
    else:
        inf = TsvReader(open(INFILE, 'r'))

    outf = open(OUTFILE, 'w')
    outf.write( inf.header )

    while True:
        line = inf.next()
        if line == None: break

#        if line['allele1HapLink']=="" or line['allele2HapLink']=="":
        if not "Phased" in line['allele1HapLink'] or not "Phased" in line['allele2HapLink']:
            if re.search("[ACGT]", line['allele1Seq']) != None:
                line['allele1Seq'] = "?"
                line['zygosity'] = "no-call"
            if re.search("[ACGT]", line['allele2Seq']) != None:
                line['allele2Seq'] = "?"
                line['zygosity'] = "no-call"

        outf.write( "\t".join( [line[x] for x in inf.hFields] ) + "\n" )


###################################################

if __name__ == "__main__":
    main()

