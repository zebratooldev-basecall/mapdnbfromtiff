#!/usr/bin/python
import sys, os
from gbifile import GbiFile
from cgiutils import bundle

def abort(self, msg, *args):
    raise Exception(msg % args)

ref = GbiFile('/Proj/Assembly/ahalpern/Test/RepeatInserts/reference_repeats.gbi')

fn = sys.argv[1]
suffix = 'CoverageDetail.csv'
if not fn.endswith(suffix):
    abort('bad file name')

region = os.path.basename(fn)[:-len(suffix)]
ctg, ofs, ectg, eofs = [ int(x) for x in region.split(',') ]
if ctg != ectg:
    abort('bad contig data')

contig = ref.contigs[ctg]
print '%s:%s:%d-%d' % (fn,contig.supername, contig.offset + ofs + 1, contig.offset + eofs + 1)
fileoffset = contig.offset + ofs

f = open(fn)
f.readline() # skip header

length = eofs - ofs
lc = [0] * length
rc = [0] * length

ii = 0
for line in f:
    data = [int(x) for x in line.split(',')[8:]]
    lc[ii] = data[0] + data[1]
    rc[ii] = data[2] + data[3]
    ii += 1

def findClumps(cov, direction):
    result = []
    start = -1
    end = -1
    sumcov = 0
    for ii, val in enumerate(cov):
        if val == 0:
            if start > 0 and ii - end > 80:
                if end - start > 200 and sumcov/35 > 5:
                    clump = bundle(direction=direction,
                                   ctg=contig.supername,
                                   beg=(fileoffset + start),
                                   end=(fileoffset + end),
                                   length=(end-start),
                                   count=(sumcov/35))
                    result.append(clump)
                start = end = -1
                sumcov = 0
        else:
            if start > 0:
                end = ii
                sumcov += val
            else:
                start = end = ii
                sumcov += val
    return result

fc = findClumps(lc, 'f')
bc = findClumps(rc, 'b')

of = open('seeds.txt', 'a')
try:
    for f in fc:
        for b in bc:
            if (b.beg-400 < f.end < b.beg+40): # and (f.length < 800) and (b.length < 800):
                #import pdb; pdb.set_trace()
                end = f.end - 40
                beg = end - 700
                if beg > contig.offset:
                    print >>of, '%s,%d,%d,fixed 0 50' % (f.ctg, beg, end)
                beg = b.beg + 40
                end = beg + 700
                if end < contig.offset + contig.length:
                    print >>of, '%s,%d,%d,fixed 650 700' % (b.ctg, beg, end)
                #for ii in range(f.beg - fileoffset, b.end - fileoffset):
                #    print lc[ii], rc[ii]
                #sys.exit(0)


finally:
    of.close()
