#!/usr/bin/python
import os,sys
import optparse
import numpy
import matplotlib
matplotlib.use('Agg')
import pylab as P

# from glob import glob
# import mako.template, mako.lookup, mako.exceptions

def make_novel_CNV_plot(chr,cnvbeg,cnvend,width,root,refdir):

    wid = int(width)
    k = int( wid / 1000 )

    cnvlen=cnvend-cnvbeg
    plotbeg=cnvbeg - 10*cnvlen
    plotend=cnvend + 10*cnvlen
    
    loc = []
    detrelcvg = []
    smpcvg = open(root + "/Profile" + width + "/" + chr + ".profile")
    for l in smpcvg:
        w = l.split(" ")
        pos = int(w[0])
        if pos >= plotbeg and pos <= plotend:
            loc.append(pos)
            detrelcvg.append(float(w[1]))
    smpcvg.close()

    basecvg = open(refdir + "/CovModel/Profile" + width + "/" + chr + ".forplot")
    l = basecvg.readline()
    w = l.rstrip().split(" ")
    nbase = len(w)-4
    baserelcvg = [ [] for i in range(nbase) ]
    simrelcvg = []
    baseloc = []
    pos = int(w[1])
    if pos >= plotbeg and pos <= plotend:
        i = 0
        baseloc.append(pos)
        for c in w[3:-1]:
            baserelcvg[i].append(float(c))
            i += 1
        simrelcvg.append(float(w[-1]))
    for l in basecvg:
        w = l.rstrip().split(" ")
        if len(w) != nbase+4:
            print "ERROR: different baseline sample counts within a single chromosome"
            sys.exit(-1)
        pos = int(w[1])
        if pos >= plotbeg and pos <= plotend:
            i = 0
            baseloc.append(pos)
            for c in w[3:-1]:
                baserelcvg[i].append(float(c))
                i += 1
            simrelcvg.append(float(w[-1]))
    basecvg.close()

    fig = P.figure(figsize=(10, 5))

    xlabel = 'position on %s' % chr
    ylabel = 'relative coverage'
    
    ax = fig.add_subplot(1,1,1)
    ax.grid(alpha=0.2,linewidth=1)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    for i in range( 0, nbase):
        line, = ax.plot(baserelcvg[i], xdata = baseloc)
    simline, = ax.plot(simrelcvg, xdata = baseloc, color='black', linewidth=2)
    smpline, = ax.plot(detrelcvg, xdata = loc, color='red', linewidth=3)
    barline, = ax.plot([-.2, -.2], xdata = [ cnvbeg, cnvend ], color = 'orange', linewidth=4)

    ax.set_xlim(plotbeg,plotend)
    ax.set_ylim(ymin=-.5)

    leg = ax.legend( [simline,smpline,barline], ["Sim","sample","called CNV"], loc='upper right', labelspacing = 0.2)
    leg.get_frame().set_fill(False)
    for t in leg.get_texts():
        t.set_fontsize('small')    # the legend text fontsize    
    for l in leg.get_lines():
        l.set_linewidth(1.5)  # the legend line width
        
    fig.savefig(root + "/NovelCNVPlots" + width + "/%s_%d_%d.png" % (chr,cnvbeg,cnvend))
    P.close(fig)
    

def cnv_novel_plot(width,root,callfile,refdir):
    calls = open(callfile,'r')
    novelcnvfile = open(root + "/novelCNVs"+width+".tsv","w")
    header=0
    typeField=-1
    knownField=-1
    numnovel=0 # number of novel CNVs processed so far
    numplots=0 # number of novel CNVs plotted so far -- we may subsample if we get too many
    skip=1     # we output a plot for every 'skip' additional novel CNVs -- skip may be increased during the run
    prevplot=0   # last novel CNV that was actually plotted
    rescaleIncrement=50 # how many plots before we increase skip
    rescaleTimes=0  # number of times we have increased skip
    for l in calls:
        if header==0:
            if l[0] == ">":
                header=1
                words = l.split("\t")
                nw=0
                for w in words:
                    if w.rstrip() == "calledCNVType":
                        typeField=nw
                    if w.rstrip() == "knownCNV":
                        knownField=nw
                    nw += 1
        else:
            words = l.split("\t")
            if (words[typeField].rstrip() == "-" or words[typeField].rstrip() == "+") and words[knownField].rstrip() == "":
                print >> novelcnvfile, "%s\t%s\t%s" % (words[0],words[1],words[2])
                numnovel += 1
                if(numnovel-prevplot==skip):
                    make_novel_CNV_plot(words[0],int(words[1]),int(words[2]),width,root,refdir)
                    numplots += 1
                    prevplot = numnovel
                if int(numplots/rescaleIncrement) > rescaleTimes:
                    skip *= 3
                    rescaleTimes += 1

    calls.close()
    novelcnvfile.close()
    
def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(width='',root='',calls='',refdir='')
    parser.add_option('--root', dest='root',
                      help='directory where results should go')
    parser.add_option('--calls', dest='calls',
                      help='annotated CNV calls file')
    parser.add_option('--window-width', dest='width',
                      help='width of sliding window averaging')
    parser.add_option('--refdir', dest='refdir',
                      help='reference (gbi) directory')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.width:
        parser.error('window-width specification required')
    if not options.refdir:
        parser.error('refdir specification required')
    if not options.calls:
        parser.error('calls specification required')
    if not options.root:
        parser.error('root specification required')

    cnv_novel_plot(options.width,options.root,options.calls,options.refdir)



if __name__ == '__main__':
    main()
