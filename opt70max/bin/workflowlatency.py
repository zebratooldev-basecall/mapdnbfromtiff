#! /usr/bin/env python

import os, shutil, sys, optparse
from os.path import basename, dirname
from glob import glob


class Steps(object):
    def __init__(self, wid, byStage):
        self.wid = wid
        self.byStage = byStage
        self.tm = []
        self.stageOrder = []
        self.stepOrder = {}
        self.stepWidth = {}

    def addFile(self, fn):
        fnfields = fn.split('/')
        tm = fnfields[-4] + '@' + fnfields[-3] + ':' + fnfields[-2]
        ff = open(fn)
        for line in ff:
            if not line.startswith('STEP,') and not line.startswith('SERV,'):
                continue
            fields = line.split(',')
            if fields[8] != self.wid:
                continue
            rt = int(fields[6])
            if 0 == rt:
                continue
            step = fields[1][fields[1].find('.')+1:]
            stage = step[:step.find('.')]
            if self.byStage:
                step = step[:step.find('.')]
            if stage not in self.stageOrder:
                self.stageOrder.append(stage)
                self.stepOrder[stage] = []
            if step not in self.stepOrder[stage]:
                self.stepOrder[stage].append(step)
                self.stepWidth[step] = {}
            if tm not in self.stepWidth[step]:
                self.stepWidth[step][tm] = 0
            self.stepWidth[step][tm] += rt
        ff.close()
        if len(self.stepOrder) > 0:
            self.tm.append(tm)

    def getStepOrder(self):
        result = []
        for stage in self.stageOrder:
            for step in self.stepOrder[stage]:
                result.append(step)
        return result


def isSeparator(tm):
    return tm.endswith(':00') and int(tm.split(':')[0].split('@')[1]) % 4 == 0

def main():
    parser = optparse.OptionParser('usage: %prog [options] WORKFLOW_ID')
    parser.add_option('--stages', action='store_true', default=False,
                      help='Build latency table of stages instead of steps.')
    parser.add_option('--gnoll', action='store_true', default=False,
                      help='Look for workflow residing on gnoll cluster instead of bops.')

    (options, args) = parser.parse_args()

    if len(args) != 1:
        parser.error('expected one argument for WORKFLOW_NAME')

    steps = Steps(args[0], options.stages)
    clusterName = 'bop'
    if options.gnoll:
        clusterName = 'gnoll'
    for fn in sorted(glob('/Proj/Assembly/Shared/PersistentData/ScratMonitor/data2/201*/*/*/%s.qstat' % clusterName)):
        steps.addFile(fn)

    hFields = [ 'Step' ]
    if steps.byStage:
        hFields = [ 'Stage' ]
    for tm in steps.tm:
        if isSeparator(tm):
            hFields.append(tm)
        else:
            hFields.append('')
    print ','.join(hFields)

    for step in steps.getStepOrder():
        fields = [ step ]
        for tm in steps.tm:
            if tm in steps.stepWidth[step]:
                fields.append(str(steps.stepWidth[step][tm]))
            elif isSeparator(tm):
                fields.append('X')
            else:
                fields.append('')
        print ','.join(fields)


if __name__ == '__main__':
    main()
