#!/bin/bash

var=Variations20-60_ccf100.csv
RUNMODE=grid 
DISTANCE=10
mkdir trio

#male
driver.py -w $PWD/trio -f $CGI_HOME/etc/workflows/human/TrioCompare -m $RUNMODE \
 -p in.combine-loci-distance=$DISTANCE \
 -p in.filter-nocalls=yes \
 -p in.child=$PWD/Variations20-60_ccf100.csv \
 -p in.mother=/Proj/Pipeline/Production_Data/ASM/GS00017-DNA-E01_03-ASM/Variations25-40_ccf50.csv \
 -p in.father=/Proj/Pipeline/Production_Data/ASM/GS00017-DNA-C01_03-ASM/Variations25-40_ccf50.csv
