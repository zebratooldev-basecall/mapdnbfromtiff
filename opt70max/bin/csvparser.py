#!/usr/bin/env python
import numpy

class Data(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

class ParseError(RuntimeError):
    def __init__(self, file, line, message):
        str = '%s(%d): %s' % (file.name, line, message)
        RuntimeError.__init__(self, str)

def _goodName(s):
    import re
    if not s or not re.match(r'\w+', s):
        return False
    else:
        return True

def _loadFreeFormBlock(f, ln):
    r = []

    while f:
        line = f.readline().strip()
        ln += 1

        if not line or line[0] == '#':
            break

        r.append(line.split(','))

    return r, ln

def _loadDictBlock(f, ln):
    r = Data()
    r.tupleList = []

    while f:
        line = f.readline().strip()
        ln += 1

        if not line or line[0] == '#':
            break

        try:
            k, v = line.split(',')
            k = k.strip()
        except:
            raise ParseError(f, ln, 'expected key,value; failed to parse "%s"' % line)
        if not _goodName(k):
            raise ParseError(f, ln, 'bad identifier "%s"' % k)
        try:
            v = numpy.float64(v)
        except:
            pass
        else:
            r.__dict__[k] = v
            r.tupleList.append( (k, v) )

    return r, ln

def _loadArrayBlock(f, ln):
    tuples = []
    header = None
    while f:
        line = f.readline().strip()
        ln += 1

        if not line or line[0] == '#':
            break

        items = [ x.strip() for x in line.split(',') ]

        if not header:
            if not items:
                raise ParseError(f, ln, 'expected header, found "%s"' % line)
            for x in items:
                if not _goodName(x):
                    raise ParseError(f, ln, 'bad identifier "%s"' % x)
            header = items
        else:
            try:
                items = [numpy.float64(x) for x in items]
            except:
                raise ParseError(f, ln, 'bad data "%s"' % line)
            if len(items) != len(header):
                raise ParseError( f, ln,
                                  'wrong number of items: found %d, header has %d' %
                                        (len(items), len(header)) )
            tuples.append(items)

    if not header:
        raise ParseError(f, ln, 'unexpected end of data block')

    r = Data()
    for x in header:
        r.__dict__[x] = numpy.empty(len(tuples), dtype=float)

    for ii, tup in zip(range(len(tuples)), tuples):
        for name, value in zip(header, tup):
            r.__dict__[name][ii] = value

    return r, ln

def loadFile(f, destination):
    """Parses CSV file and attaches data to the destination object."""

    if isinstance(f, basestring):
        f = open(f, "r")

    ln = 0
    while f:
        line = f.readline()
        if not line:
            break

        ln += 1
        line = line.strip()
        if not line or line[0] == '#':
            continue

        blockHeader = [ x.strip() for x in line.split(',') ]
        if len(blockHeader) != 2:
            raise ParseError(f, ln, 'bad block header "%s"' % line)
        format, fieldName = blockHeader
        if not _goodName(fieldName):
            raise ParseError(f, ln, 'bad identifier "%s"' % fieldName)

        if format == 'dict':
            r, ln = _loadDictBlock(f, ln)
        elif format == 'array':
            r, ln = _loadArrayBlock(f, ln)
        elif format == 'freeform':
            r, ln = _loadFreeFormBlock(f, ln)
        else:
            raise ParseError(f, ln, 'bad format "%s"' % format)

        destination.__dict__[fieldName] = r


class Table(object):
    def __init__(self):
        self.data = []
        self.colIndexes = {}
        self.rowIndexes = {}

    def addRow(self,cells):
        if 0 == len(self.data):
            for cell in cells:
                self.colIndexes[cell] = []
            for ii in xrange(len(cells)):
                self.colIndexes[cells[ii]].append(ii)
        if cells[0] in self.rowIndexes:
            self.rowIndexes[cells[0]].append( len(self.data) )
        else:
            self.rowIndexes[cells[0]] = [ len(self.data) ]
        self.data.append(cells)
        if len(cells) != len(self.data[0]):
            raise ParseError( 'row length mismatch:\n%s\n%s' % (','.join(self.data[0]), ','.join(cells)) )

    def getCell(self,row,column):
        return self.data[self.getRowIndex(row)][self.getColumnIndex(column)]

    def getColumnIndex(self,name):
        [ index ] = self.colIndexes[name]
        return index

    def getRowIndex(self,name):
        [ index ] = self.rowIndexes[name]
        return index

    def getColumnByHeader(self,name):
        index = self.getColumnIndex(name)
        result = []
        for ii in xrange(1,len(self.data)):
            result.append(self.data[ii][index])
        return result

    def getRowByHeader(self,name):
        index = self.getRowIndex(name)
        return self.data[index][1:]


def loadCsvTableFromStream(f):
    """Parses CSV table."""

    result = Table()
    while f:
        line = f.readline()
        if not line:
            break

        cells = [ x.strip() for x in line.split(',') ]
        result.addRow(cells)

    return result

def loadCsvTable(f):
    """Parses CSV file, assuming a single table."""

    if isinstance(f, basestring):
        f = open(f, "r")

    return loadCsvTableFromStream(f)


if __name__ == '__main__':
    import StringIO

    TEST_DICT = \
    """
    # Some comment
    # more comments, commas, etc...

    # comment after empty line
    dict  ,   foo
    bar, 100
    baz, 100.1
    ban, 1e5
    boo, 1.3e-5

    freeform,zoo
    a, b, c, d, e
    x, 1, 2, 3, 5
    z, 3, 5, 6, 7
    y, 0, 1, 1, 1
    """

    f = StringIO.StringIO(TEST_DICT)
    x = Data()
    loadFile(f, x)

    print "dict format test results:"
    print "foo.__dict__:", x.foo.__dict__
    print "direct var access:", x.foo.bar, x.foo.baz, x.foo.ban, x.foo.boo
    print "freeform:", x.zoo

    TEST_ARRAY = \
    """
    # comment
    array, var1
    h1, h2, h3
    1, 2, 3
    100, 200, 300

    array, var2
    foo, bar, baz
    5, 6, 7
    50, 60, 70
    # comment separator behaves as empty line

    # multiple empty lines are treated as one
    array,var3
    x
    100
    200
    300
    400
    500
    """
    f = StringIO.StringIO(TEST_ARRAY)
    x = Data()
    loadFile(f, x)
    print "array format test results:"
    print x.var1.h1, x.var2.baz, x.var3.x
