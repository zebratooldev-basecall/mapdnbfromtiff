#!/usr/bin/env python

"""
Similar to Shane's aggregateDiscordancePatterns.py
"""
from os.path import join as pjoin
from optparse import OptionParser
import glob
import sys

def initialize_containers():
    global completeOverlaps
    global unmappedArms
    global gapDiffs ## gapDiffs[gapDiff] = {'total': total chimeric dnbs, 'dnbCount' : count}
    completeOverlaps = 0
    unmappedArms = 0
    gapDiffs = {}

def writeout_stats(outdir):
    fo = open('%s/complete3T5Toverlap.csv'%(outdir,),'w')
    try:
        fo.write('unmappedArms,armswith3Tin5T,pctArmsWith3Tin5T\n')
        pctOverlap = float(completeOverlaps) / float(unmappedArms) * 100
        fo.write('%s,%s,%s\n'%(unmappedArms,completeOverlaps,pctOverlap))
    finally:
        fo.close()

    fo = open('%s/chimericDnbGapDifferences.csv'%(outdir,),'w')
    try:
        fo.write('gapDifference,chimericDnbCountTotal,dnbCountWithGapDiff,dnbPctWithGapDiff\n')
        gapdiffs = gapDiffs.keys()
        # different map files might have different gap differences, so the total
        # for gap diff -132 might not be real total
        # find max 'total'
        total = 0
        for gapdiff, values in gapDiffs.iteritems():
            localtotal = values.get('total',0)
            if int(localtotal) > total:
                total = int(localtotal)
        # write out data
        for gapdiff in range(min(gapdiffs),max(gapdiffs) + 1):
            values = gapDiffs.setdefault(gapdiff,{})
            count = values.get('dnbCount',0)
            try:
                pctGapDiff = float(count) / float(total) * 100
            except:
                pctGapDiff = 'inf'
            fo.write('%s,%s,%s,%s\n'%(gapdiff,total,count,pctGapDiff))
    finally:
        fo.close()

def aggCompleteOverlaps(line,fi):
    global unmappedArms
    global completeOverlaps
    line = fi.next(); line = fi.next()
    line = line.rstrip().split(',')
    unmappedArms += int(line[0])
    completeOverlaps += int(line[1])
    line = fi.next()

    return line, fi

def aggGapDiffs(line,fi):
    line = fi.next(); line = fi.next()
    while line:
        line = line.rstrip().split(',')
        gapDiff = int(line[0])
        chimericDNBs = int(line[1])
        dnbWithGapDiff = int(line[2])
        gapDiffs[gapDiff] = gapDiffs.setdefault(gapDiff,{})
        gapDiffs[gapDiff]['total'] = gapDiffs[gapDiff].get('total',0) + chimericDNBs
        gapDiffs[gapDiff]['dnbCount'] = gapDiffs[gapDiff].get('dnbCount',0) + dnbWithGapDiff
        try:
            line = fi.next()
        except StopIteration:
            break

    return line, fi

def parse_statfile(infile):
    global completeOverlaps
    global gapDiffs
    fi = open(infile)
    try:
        for line in fi:
            line = line.rstrip().split(',')
            if line[0] == 'array':
                table = line[1]
                if table == 'complete3T5Toverlap':
                    line, fi = aggCompleteOverlaps(line,fi)
                elif table == 'gap_differences_in_chimeric_dnbs':
                    line, fi = aggGapDiffs(line,fi)
    finally:
        fi.close()

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-d', '--input-directory', help='top level directory in CNT unmappedToFastq workflow', dest='input_directory')
    parser.add_option('-l', '--lane',            help='lane to run aggregation on',                               dest='lane')

    # parse command line arguments.
    options, args = parser.parse_args(arguments[1:])

    return options.input_directory, options.lane

def main(arguments):
    # parse command line arguments.
    inputDirectory, lane = parse_arguments(arguments)

    # create directory for lane.
    laneDir = pjoin(inputDirectory, lane)

    initialize_containers()
    
    for infile in glob.glob('%s/*unmapped_summary.csv' % (laneDir,)):
        parse_statfile(infile)

    writeout_stats(laneDir)

if __name__ == '__main__':
    main(sys.argv)
