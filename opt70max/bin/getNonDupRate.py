#!/usr/bin/env python

## Minor changes from Shane's script

import os,sys
from os.path import join as pjoin
from glob import glob

header = ['slidelane',
          'full_coverage',
          'non_duplicate_full_coverage',
          'non_duplicate_rate']

class CsvReader(object):
    def __init__(self, ff):
        self.ff = ff
        self.hFields = self.ff.readline().rstrip('\r\n').split(',')

    def close(self):
        self.ff.close()

    def next(self):
        line = self.ff.readline()
        if '' == line:
            return None
        fields = line.rstrip('r\n').split(',')
        if len(fields) != len(self.hFields):
            raise Exception('field count mismatch')
        result = {}
        for (name,val) in zip(self.hFields, fields):
            result[name] = val
        return result

def getNonDupRateData(infile):
    ff = open(infile)
    reader = CsvReader(ff)
    while True:
        data = reader.next()
        if data['Field'] == 'Total Coverage':
            fullCoverage = int(data['calledcvg:full:weightratio>.99'])
            fullCoverageOnePerGroup = int(data['calledcvg:full:weightratio>.99:onepergroup'])
            break
    ff.close()
    return (fullCoverage, fullCoverageOnePerGroup, float(fullCoverageOnePerGroup)/float(fullCoverage))


for workdir in sys.argv[1:]:
    fsw = "%s/cs-nonDupRate.csv"%(workdir,)
    sw = open(fsw,'w')
    sw.write(','.join(header) + '\n')
    
    name = os.path.basename(workdir)

    ## added to adapt new wf
    fxml = glob('%s/collection.xml'%(workdir,))
    if len(fxml)==0: continue
    xml = open(fxml[0], 'r')
    for ln in xml:
        ln = ln.strip()
        if ln.startswith("<Slide"):
            slide = ln.split("=")[-1].strip(">").strip("\"")
        if ln.startswith("<Lane DNB"):
            xlane = ln.split(" ")[4].split("=")[-1].strip("\"")
    lane = slide+"-"+xlane

    # retrieve non-duprate from brief coverage report.
    filepath = pjoin(workdir,'ASM', 'coverage', 'allBriefCoverageSummary.csv')
    #filepath = lane
    if os.path.isfile(filepath):
        result = getNonDupRateData(filepath)
        sw.write('%s,%d,%d,%f\n' % (lane,result[0],result[1],result[2]))
    sw.close()
