#!/bin/env python
import os,sys
import optparse
import bz2



def addReadCounts(baseMVar,compareMVar,outMVar,columnSuffix):

    bmv = bz2.BZ2File(baseMVar,'r')
    omv = bz2.BZ2File(outMVar,'w')

    n = len(compareMVar.split(','))
    if n != len(columnSuffix.split(',')):
        print 'number of column-suffix entries not equal to number of compare-mastervar entries'
        sys.exit(-1)

    cmv = []
    suffix = []
    a1col = []
    a2col = []
    racol = []
    trcol = []
    for j in range(0,n):
        cmv += [ bz2.BZ2File(compareMVar.split(',')[j],'r') ]
        suffix += [columnSuffix.split(',')[j]]
        a1col += [-1]
        a2col += [-1]
        racol += [-1]
        trcol += [-1]

        seenHeader = False
        lc = cmv[j].readline()
        while lc:
            if not seenHeader:
                if lc[0] == '>':
                    seenHeader = True
                    w = lc.rstrip('\n').split('\t')
                    for i in range(len(w)):
                        if w[i] == 'allele1ReadCount':
                            a1col[j] = i
                        if w[i] == 'allele2ReadCount':
                            a2col[j] = i
                        if w[i] == 'referenceAlleleReadCount':
                            racol[j] = i
                        if w[i] == 'totalReadCount':
                            trcol[j] = i
                    break
            lc = cmv[j].readline()

        if a1col[j] == -1 or a2col[j] == -1 or racol[j] == -1 or trcol[j] == -1:
            print 'Failed to find expected read counts in compare-mastervar ' + compareMVar.split(',')[j]
            sys.exit(-1)

    seenHeader = False
    for lb in bmv:
        if not seenHeader:
            if lb[0] == '>':
                seenHeader = True
                omv.write(lb.rstrip('\n'))
                for j in range(0,n):
                    omv.write('\t'.join(['',
                                         'allele1ReadCount-'+suffix[j],
                                         'allele2ReadCount-'+suffix[j],
                                         'referenceAlleleReadCount-'+suffix[j],
                                         'totalReadCount-'+suffix[j]]))
                omv.write('\n')
            else:
                omv.write(lb)
        else:
            omv.write(lb.rstrip('\n'))
            for j in range(0,n):
                lc = cmv[j].readline()
                if lc == '':
                    print 'ran out of compare-mastervar too soon for file ' + compareMVar.split(',')[j]
                    sys.exit(-1)
                w = lc.rstrip('\n').split('\t')
                omv.write('\t' + w[a1col[j]] +
                          '\t' + w[a2col[j]] +
                          '\t' + w[racol[j]] +
                          '\t' + w[trcol[j]])
            omv.write('\n')

    for j in range(0,n):
        lc = cmv[j].readline()
        if lc != '':
            print 'did not use up compare-mastervar for file ' + compareMVar.split(',')[j] + ' ; next line:'
            print lc

        cmv[j].close()

    omv.close()
    bmv.close()
    return


def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(columnSuffix='',baseMVar='',compareMVar='',outMVar='')
    parser.add_option('--base-mastervar', dest='baseMVar',
                      help='masterVar file to be augmented')
    parser.add_option('--output-mastervar', dest='outMVar',
                      help='output file name')
    parser.add_option('--compare-mastervar', dest='compareMVar',
                      help='comparative masterVar(s) from which to take read counts; comma-separate multiple files')
    parser.add_option('--column-suffix', dest='columnSuffix',
                      help='suffix(es) (e.g. T1) to append to added column names; comma-separate multiple suffixes')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.baseMVar:
        parser.error('base-mastervar specification required')
    else:
        if options.baseMVar[-3:] != 'bz2':
            print "base-mastervar specification must have bz2 suffix (and be bz2 compressed)"
            sys.exit(-3)
    if not options.compareMVar:
        parser.error('compare-mastervar specification required')
    else:
        if options.compareMVar[-3:] != 'bz2':
            print "compare-mastervar specification must have bz2 suffix (and be bz2 compressed)"
            sys.exit(-3)
    if not options.outMVar:
        parser.error('output-mastervar specification required')
    else:
        if options.outMVar[-3:] != 'bz2':
            print "output-mastervar specification must have bz2 suffix (and will be bz2 compressed)"
            sys.exit(-3)
    if not options.columnSuffix:
        parser.error('column-suffix specification required')
            
    addReadCounts(options.baseMVar,options.compareMVar,options.outMVar,options.columnSuffix)


if __name__ == '__main__':
    import sys, traceback
    try:
        main()
        sys.exit(0)
    except SystemExit:
        pass
    except:
        last_type, last_value, last_traceback = sys.exc_info()
        traceback.print_exception(last_type, last_value, last_traceback)
        sys.exit(1)
    
