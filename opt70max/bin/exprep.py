#!/usr/bin/python
import os
import csvparser
import driverlib
import accessionlib

def parseComment(mobj):
    fn = os.path.join(mobj.location, 'comment')
    f = open(fn, 'r')
    comment = f.readline().strip().split(',')[1]
    mobj.comment = comment

def parseReport(mobj):
    fn = os.path.join(mobj.location, 'reports', 'MappingStats.csv')
    csvparser.loadFile(fn, mobj)

def createObjects():
    cgiHome = driverlib.getCgiHome()
    print "loading assembly objects..."
    om = accessionlib.ObjectManager('/Proj/Assembly/inazarenko/MapRoot', cgiHome)
    maps = sorted(om.usedIds['MAP'])

    r = []
    for m in maps:
        mobj = om.findObject(m)
        parseComment(mobj)
        parseReport(mobj)
        r.append(mobj)

    return r

if __name__ == '__main__':

    """
searchedFullDnbCount,55121679
uniquelyPairedFullDnbCount,26013769
multiplyPairedFullDnbCount,3526378
mappedBaseCount,2061833753
bothArmsMappedUnpairedFullDnbCount,448761
notSearchedFullDnbCount,153
overflowedFullDnbCount,4074834
notMappedFullDnbCount,21057937
leftNotSearchedHalfDnbCount,151
leftSearchedHalfDnbCount,55121681
leftNotMappedHalfDnbCount,17161238
leftOverflowedHalfDnbCount,2300215
leftPairedHalfDnbCount,29640667
leftMappedCost0HalfDnbCount,3688772
leftMappedCost1HalfDnbCount,1023033
leftMappedCost2HalfDnbCount,709473
leftMappedCost3HalfDnbCount,598283
leftMappedCostOver3HalfDnbCount,0
    """

    print "reading input data..."
    ml = createObjects()
    print 'comment,unique-full,multi-full,incon-full,over-full,not-mapped-full,' \
          'L-nm,L-over,L-map,R-nm,R-over,R-map'
    for m in ml:
        print '%s,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f' % \
           (m.comment,
            m.map.uniquelyPairedFullDnbCount,
            m.map.multiplyPairedFullDnbCount,
            m.map.bothArmsMappedUnpairedFullDnbCount,
            m.map.overflowedFullDnbCount,
            m.map.notMappedFullDnbCount,
            m.map.leftNotMappedHalfDnbCount,
            m.map.leftOverflowedHalfDnbCount,
            (m.map.leftPairedHalfDnbCount+m.map.leftMappedCost0HalfDnbCount+
             m.map.leftMappedCost1HalfDnbCount+m.map.leftMappedCost2HalfDnbCount+
             m.map.leftMappedCost3HalfDnbCount+m.map.leftMappedCostOver3HalfDnbCount),
            m.map.rightNotMappedHalfDnbCount,
            m.map.rightOverflowedHalfDnbCount,
            (m.map.rightPairedHalfDnbCount+m.map.rightMappedCost0HalfDnbCount+
             m.map.rightMappedCost1HalfDnbCount+m.map.rightMappedCost2HalfDnbCount+
             m.map.rightMappedCost3HalfDnbCount+m.map.rightMappedCostOver3HalfDnbCount),
            )
