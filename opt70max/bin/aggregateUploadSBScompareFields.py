#!/usr/bin/env python

import sys
import os
import itertools
from os.path import join as pjoin
from optparse import OptionParser
import glob
import pymssql
from ConfigParser import ConfigParser

def initialize_holders():
    global adjBaseSumms
    global fieldScores

    adjBaseSumms = {} ## adjBaseSumms[field][pctdata][pattern] = (catfrac, totalfrac)
    fieldScores = [] ## [ (score,field), (score,field)..]

# stolen from Shane's uploadPositionalDiscordance.py
class DBLoader:

    def __init__(self,dbLogin):
        self.conn = None
        self.dbLogin=dbLogin
        self.rows = []
        self.accumulate=""
        self.insertion_count=0

    def connect(self,whichDB="idqc:dwProd"):
        self.conn = pymssql.connect(host=self.dbLogin.get(whichDB,'host'),
                               user=self.dbLogin.get(whichDB,'user'),
                               password=self.dbLogin.get(whichDB,'password'),
                               database=self.dbLogin.get(whichDB,'database'))
        if not self.conn:
            print "can't connect"
            os.sys.exit()

    def execute(self,query):
        cur = self.conn.cursor()
        cur.execute(query)
        self.rows = cur.fetchall()
        cur.close()
        return self.rows[0][0]

    def execute_no_result(self, query, debug):
        cur = self.conn.cursor()
        if not debug:
            #try :
            cur.execute(query)
            #except pymssql.OperationalError:
            #    pass
            #cur.close()
        else:
            print "debug run! no insertion!"

    def check_header(self, fn, header, expected):
        test = header.rstrip('\r\n')

        if test != ','.join(expected):
            print test
            print expected
            raise Exception('Header for %s file does not matched expected!' % fn)

    def commit_accumulated_insertions(self, debug):
        if self.insertion_count > 0:
            print self.accumulate
            self.execute_no_result(self.accumulate, debug)
            self.accumulate = ""
            self.conn.commit()
            self.insertion_count=0

    def insert_adj_base(self, slidelane, debug, testcycle ):
        # sort field scores to find median field
        fieldScores.sort()
        fieldfracs = [0.1, 0.2, 0.5, 0.8, 0.9]
        for fieldfrac in fieldfracs:
            index = len(fieldScores) * fieldfrac
            medfield = fieldScores[int(index)][1]

            # upload adjacent base summaries for median field
            for pct, patterndata in adjBaseSumms[medfield].iteritems():
                for pattern, (catfrac, totalfrac) in patterndata.iteritems():
                    self.accumulate += "INSERT INTO [bioinfo].sbs_adjacent_base VALUES ('%s',%s,%s,%s,%s,%s,%s,%s,%s);\n" % \
                        (slidelane, testcycle, fieldfrac, pct, pattern[0], pattern[1], pattern[2], catfrac, totalfrac)
                    self.insertion_count += 1
                    if self.insertion_count > 49:
                        self.commit_accumulated_insertions(debug)
            self.commit_accumulated_insertions(debug)

    def insert_heatmap(self, fn, slidelane, debug, testcycle):
        HEADER = ['pctdata','slide','lane','column','row','concordant_dnbs','discordant_dnbs','concordant_fraction','discordant_fraction']

        # check for file.
        if not os.path.exists(fn):
            raise Exception('heatmap file does not exist! %s' % fn)

        # read through file and insert records.
        ff = open(fn, 'r')
        try:
            # check header.
            header = ff.readline()
            self.check_header(fn, header, HEADER)
            
            for line in ff:
                line = line.rstrip().split(',')
                if line[7] != 'inf' and line[8] != 'inf':
                    self.accumulate += "INSERT INTO [bioinfo].sbs_concordance_heatmap VALUES ('%s',%s,%s,'%s','%s',%s,%s,%s,%s,%s,%s);\n" % \
                    (slidelane, testcycle, line[0],line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8] )
                    self.insertion_count += 1
                    if self.insertion_count > 49:
                        self.commit_accumulated_insertions(debug)
                else:
                    print('insert_heatmap:  Division by zero error!  lane=%s, field=%sC%sR%s' % (slidelane, line[1], line[2], line[3] ))
        finally:
            ff.close()
            self.commit_accumulated_insertions(debug)
    
    def load_adj_base_summaries(self, infile, field, debug, testcycle):
        # check for file.
        if not os.path.exists(infile):
            raise Exception('adjacent base summary file does not exist! %s' % infile)

        HEADER = ['slidelane','pctdata','concordance_pattern','cat_fraction','total_fraction','slide','lane','column','row']
        fi = open(infile)
        try:
            # get discordance score for field from first line
            scoreline=fi.readline()
            discscore = scoreline.rstrip().split(',')[-1]
            discscore = float(discscore)
            fieldScores.append((discscore,field))
            
            # check header
            header = fi.readline()
            self.check_header(infile, header, HEADER)

            adjBaseSumms[field] = adjBaseSumms.setdefault(field,{})

            for line in fi:
                line = line.rstrip().split(',')
                # parse out data for summary table
                pattern = (line[2][1],line[3][1],line[4][1])
                pctdata = line[1]
                catfrac, totalfrac = line[5:7]
                adjBaseSumms[field][pctdata] = adjBaseSumms[field].setdefault(pctdata,{})
                adjBaseSumms[field][pctdata][pattern] = (catfrac, totalfrac)
                
                # Upload all field data for heatmap ----UPDATE ME!!---------
                if line[5] != 'inf' and line[6] != 'inf':
                    self.accumulate += "INSERT INTO [bioinfo].sbs_adjacent_base_heatmap VALUES ('%s',%s,%s,%s,%s,%s,%s,%s,'%s','%s',%s,%s);\n" % \
                    (line[0], testcycle, line[1], line[2][1], line[3][1], line[4][1], line[5], line[6], line[7], line[8], line[9], line[10] )
                    self.insertion_count += 1
                    if self.insertion_count > 49:
                        self.commit_accumulated_insertions(debug)
                else:
                    print('insert_heatmap:  Division by zero error!  lane=%s, field=%sC%sR%s' % (slidelane, line[1], line[2], line[3] ))
        finally:
            fi.close()
            self.commit_accumulated_insertions(debug)

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-d', '--input-directory', help='directory containing SBS concordance output', dest='input_directory')
    parser.add_option('-l', '--slidelane', help='slidelane', dest='slidelane')
    parser.add_option('-c', '--db-user-config',  help='configuration file with db users and passwords',                dest='db_config_file')
    parser.add_option('-t', '--test', help='debug upload tool, prints insert statements to log file', dest='debug', action='store_true', default=False)
    parser.add_option('-u', '--unique-test-cycle', help='cycle whose concordance was tested against other positions', dest='testcycle', default=1)

    # parse command line arguments.
    options, args = parser.parse_args(arguments[1:])

    return options.input_directory, options.slidelane, options.db_config_file, options.debug, options.testcycle

def main(arguments):
    # parse command line arguments
    print >> sys.stderr, 'Parsing command line'
    inputDirectory, slidelane, db_config_file, debug, testcycle = parse_arguments(arguments)

    # initialize data containers
    print >> sys.stderr, 'Initializing containers'
    initialize_holders()

    # read in db user config file.
    dbUserConfig = ConfigParser()
    dbUserConfig.read(db_config_file)

    # initialize db loader.
    dbl = DBLoader(dbUserConfig)
    dbl.connect()

    # upload everything for concordant-discordant heat map
    print >> sys.stderr, 'looking for files in current working directory %s' % inputDirectory
    for heatmapfile in glob.glob('%s/*conc_disc_heatmap.csv' % (inputDirectory,)):
        print >> sys.stderr, 'Reading heatmapfile %s' % heatmapfile
        dbl.insert_heatmap(heatmapfile,slidelane,debug, testcycle)

    # read in summary files for adjacent base comparison
    for adjbasefile in glob.glob('%s/*adjacent_summary.csv' % (inputDirectory,)):
        print >> sys.stderr, 'Reading adjacent base file %s' % adjbasefile
        field = adjbasefile.split('_')[-3]
        dbl.load_adj_base_summaries(adjbasefile, field, debug, testcycle)

    # upload adjacent base comparison data for median, 10th, 80th, 90th pctl scoring field
    dbl.insert_adj_base(slidelane, debug, testcycle)

if __name__ == '__main__':
    main(sys.argv)
