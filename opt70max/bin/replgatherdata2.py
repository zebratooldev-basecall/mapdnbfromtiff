#! /usr/bin/env python

import sys,os,re
from os.path import basename, join as pjoin
from glob import glob

MAX_SSCORE = 100


def glob1(fn):
    fns = glob(fn)
    if len(fns) != 1:
        raise Exception('glob1: '+fn)
    return fns[0]

class Table(object):
    def __init__(self, type, name):
        self.type = type
        self.name = name
        self.data = []

    def byRcName(self, rowName, colName):
        for xid in xrange(len(self.data)):
            if self.data[xid][0] == rowName:
                break
        else:
            print self.data
            raise Exception('Row not found: %s.%s' % (self.name,rowName))
        for yid in xrange(len(self.data[0])):
            if self.data[0][yid] == colName:
                break
        else:
            raise Exception('Column not found: %s.%s' % (self.name,colName))
        return self.data[xid][yid]


def readTable(ff, table, separator=','):
    while True:
        line = ff.readline()
        if '' == line or line.isspace():
            return
        line = line.rstrip('\r\n')
        table.data.append(map(lambda x:x.strip(),line.split(separator)))


def loadTable(fn, tables, name, separator=','):
    ff = open(fn)
    table = Table('freeform', name)
    readTable(ff, table, separator)
    tables[table.name] = table
    ff.close()


def loadTables(fn, tables, separator=',', nameSuffix=''):
    ff = open(fn)
    while True:
        line = ff.readline()
        if '' == line:
            break
        if line.startswith('#'):
            continue
        if line.isspace():
            continue
        line = line.rstrip('\r\n')
        type = 'freeform'
        if line.find(separator) != -1:
            (type,name) = line.split(',')
        else:
            name = line
        print 'Loading',fn,name
        table = Table(type, name + nameSuffix)
        readTable(ff, table, separator)
        tables[table.name] = table
    ff.close()


def loadTrioTables(fn, tables, name):
    table = Table('freeform', name)
    ff = open(fn)
    header = ff.readline().rstrip('\r\n')
    if header != 'score,annotation,count':
        raise Exception('bad header')
    table.data.append(header.split(',')[1:])
    while True:
        line = ff.readline().rstrip('\r\n')
        if '' == line:
            break
        table.data.append(line.split(',')[1:])
    ff.close()
    tables[table.name] = table


class ReplRun(object):
    def __init__(self, path):
        self.path = path
        self.name = basename(path)
        self.tables = {}
        loadTables(pjoin(self.path, 'stdcvg_A', 'ASM', 'callrpt', 'callrpt_VariationsVQHIGH.csv'), self.tables)
        loadTables(pjoin(self.path, 'stdcvg_A', 'ASM', 'callrpt', 'dbsnprpt_VariationsVQHIGH.csv'), self.tables)
        loadTables(pjoin(self.path, 'stdcvg_A', 'ASM', 'callrpt', 'callannotaterpt_VariationsVQHIGH.csv'), self.tables)
        for baseline in [ 'hapmap', 'infinium' ]:
            try:
                fnSnpDiff = pjoin(self.path, 'replrpt', 'snpdiff', baseline, 'stdcvg_A', 'Stats.tsv')
                loadTables(fnSnpDiff, self.tables, '\t', ' '+baseline)
            except:
                pass
        self.somtables = {}
        for id in [ 'stdcvg_A-stdcvg_B' ]:
            self.somtables[id] = {}
            try:
                loadTables(pjoin(self.path, 'replrpt', 'calldiff', id, 'DebugSomaticOutput.tsv'),
                           self.somtables[id], separator='\t')
            except:
                pass
        self.somtables['60A40B_full-stdcvg_B'] = {}
        for vt in [ 'snp', 'ins', 'del', 'sub' ]:
            key = 'af20-sens-'+vt
            try:
                loadTable(pjoin(self.path, 'replrpt', 'calldiff', '60A40B_full-stdcvg_B', key+'.csv'),
                          self.somtables['60A40B_full-stdcvg_B'], key)
            except:
                pass


    # Return SNP transition/transversion ratio.
    # regions: '' -> genome, 'CodingRegionsOnly' -> exome
    def getTiTv(self, region=''):
        table = self.tables['snpTrans' + region + 'VQHIGH']
        return float(table.byRcName('transitions','Value')) / float(table.byRcName('transversions','Value'))

    # Return count of loci.
    # varType: SNP, INS, DEL, SUB
    # zygosity: REF_HET, HOM, OPPOSITE_NOCALL, TOTAL_LINE
    # region: '', CodingRegionsOnly
    def getLocusCount(self, varType, zygosity, region=''):
        varType  = varType .upper().replace('-','_')
        zygosity = zygosity.upper().replace('-','_')
        table = self.tables['variationCount' + region + 'VQHIGH']
        return int(table.byRcName(varType.upper(), zygosity.upper()))

    # Returns novelty rate.
    # varType: SNP, INS, DEL, SUB
    # zygosity: refHet, hom, '' (total)
    # region: '', Coding
    def getNovelty(self, varType, zygosity, region=''):
        varType = varType.upper()
        num = zygosity + 'Novel'
        den = zygosity + 'Total'
        if '' == zygosity:
            num = 'novel'
            den = 'total'
        table = self.tables['verboseNovelVsDbSnpCount' + region + 'VQHIGH']
        return float(table.byRcName(varType, num)) / float(table.byRcName(varType, den))

    # region: '', CodingRegionsOnly
    def getCallRate(self, region=''):
        table = self.tables['calledBaseCount' + region + 'VQHIGH']
        return float(table.byRcName('called', 'total')) / float(table.byRcName('reference', 'total'))

    # impact: synonymousSnp, missenseSnp, nonsenseSnp, nonstopSnp,
    #         misstartSnp, disruptSnp, frameShiftingIns, framePreservingIns,
    #         frameUnknownsIns, frameShiftingDel, framePreservingDel,
    #         frameUnknownsDel, frameShiftingSub, framePreservingSub,
    #         frameUnknownsSub, frameShiftingIndel, framePreservingIndel,
    #         frameUnknownsIndel
    # zygosity: het, hom, other, total
    def getLocusImpactCount(self, impact, zygosity='total'):
        table = self.tables['functionalAnnotation' + 'VQHIGH']
        return int(table.byRcName(impact, zygosity))

    def getNonSynonymousSnpLocusCount(self, zygosity='total'):
        table = self.tables['functionalAnnotation' + 'VQHIGH']
        impacts = [ 'missenseSnp', 'nonsenseSnp', 'nonstopSnp', 'misstartSnp' ]
        return sum([int(table.byRcName(impact, zygosity)) for impact in impacts])

    # baseline: hapmap, infinium
    # metric: discordance, nocall
    # genotype: ref-ref, het-ref-alt, het-alt-alt, hom-alt-alt
    def getSnpDiffLocusFraction(self, baseline='hapmap', metric='discordance', genotype='total'):
        table = self.tables['Locus concordance by fraction %s' % baseline]
        return float(table.byRcName(genotype, metric))

    # varType: snp, ins, del, sub
    # relativeSensitivity: 0...1
    def getSomaticCount(self, varType, relativeSensitivity):
        table = self.somatic[varType]
        minScore = 1.0 - relativeSensitivity
        count = 0
        for score in table:
            if score >= minScore - .00001:
                count += 1
        return count

    def getSensitivityForSomaticCount(self, varType, somaticCount):
        relativeSensitivity = 1.0
        if len(self.somatic[varType]) >= somaticCount:
            table = sorted(self.somatic[varType], reverse=True)
            relativeSensitivity = 1.0 - table[somaticCount]
        locusCount = 0
        for zygosity in [ 'REF-HET', 'HOM', 'OPPOSITE_NOCALL' ]:
            locusCount += self.getLocusCount(varType.upper(), zygosity)
        return int(locusCount * relativeSensitivity)

    def getReplDisc50(self, vt, somScore):
        somScore = max(min(somScore,MAX_SSCORE), -MAX_SSCORE)
        rn = str(somScore)
        table = self.somtables['stdcvg_A-stdcvg_B']['%s-ReplicateRoc'%vt]
        return int(table.byRcName(rn, 'DiscordantCount'))

    def getReplSens50(self, vt, somScore):
        somScore = max(min(somScore,MAX_SSCORE), -MAX_SSCORE)
        rn = str(somScore)
        table = self.somtables['stdcvg_A-stdcvg_B']['%s-ReplicateRoc'%vt]
        return int(table.byRcName(rn, 'ScaledSensitivity'))

    def getMixtureSens20(self, vt, somScore):
        somScore = max(min(somScore,MAX_SSCORE), -MAX_SSCORE)
        rn = str(somScore)
        table = self.somtables['60A40B_full-stdcvg_B']['af20-sens-'+vt]
        return int(table.byRcName(rn, 'CumulativeCount'))


def dorn(run, ff):
    try:
        return ff(run)
    except:
        return 'N'


# Gather data.
runs = []
for path in sys.argv[1:]:
    runs.append(ReplRun(path))

try:
    os.mkdir('repldata')
except:
    pass

# Print broad-based metrics.
ff = open('repldata/summary.csv', 'w')
header = [ 'run' ]
for vt in [ 'snp', 'ins', 'del', 'sub' ]:
    pfx = vt+':'
    pfx = '' # Use excel additional header to denote vt.
    header.append(pfx+'hom')
    header.append(pfx+'partial')
    header.append(pfx+'ref-het')
    header.append(pfx+'het-novel')
    if 'snp' == vt:
        header.append(pfx+'nonsyn')
    else:
        header.append(pfx+'frmshift')
    header.append(pfx+'T@SQHIGH')
    header.append(pfx+'F@SQHIGH')
pfx = 'call-rate-'
pfx = '' # Use excel additional header.
header.append(pfx+'genome')
header.append(pfx+'exome')
pfx = 'Ti/Tv-'
pfx = '' # Use excel additional header.
header.append(pfx+'genome')
header.append(pfx+'exome')
pfx = 'snpdiff-'
pfx = '' # Use excel additional header.
header.append(pfx+'discord')
header.append(pfx+'no-call')
header.append(pfx+'discord-inf')
header.append(pfx+'no-call-inf')
print >>ff,','.join(header)
for run in runs:
    fields = [ run.name ]
    for vt in [ 'snp', 'ins', 'del', 'sub' ]:
        fields.append(dorn(run, lambda x:x.getLocusCount(vt,'HOM')))
        fields.append(dorn(run, lambda x:x.getLocusCount(vt,'OPPOSITE_NOCALL')))
        fields.append(dorn(run, lambda x:x.getLocusCount(vt,'REF-HET')))
        fields.append(dorn(run, lambda x:x.getNovelty(vt, 'refHet')))
        if 'snp' == vt:
            fields.append(dorn(run, lambda x:x.getNonSynonymousSnpLocusCount()))
        else:
            fields.append(dorn(run, lambda x:x.getLocusImpactCount('frameShifting'+vt[0].upper()+vt[1:])))
        fields.append(dorn(run, lambda x:x.getReplSens50(vt, -10)))
        fields.append(dorn(run, lambda x:x.getReplDisc50(vt, -10)))
    fields.append(dorn(run, lambda x:x.getCallRate()))
    fields.append(dorn(run, lambda x:x.getCallRate('CodingRegionsOnly')))
    fields.append(dorn(run, lambda x:x.getTiTv()))
    fields.append(dorn(run, lambda x:x.getTiTv('CodingRegionsOnly')))
    for base in [ 'hapmap', 'infinium']:
        for metric in [ 'discordance', 'nocall' ]:
            fields.append(dorn(run, lambda x:x.getSnpDiffLocusFraction(base, metric)))
    print >>ff,','.join(map(str,fields))
ff.close()

# Print 50%AF ROCs.
for vt in [ 'snp', 'ins', 'del', 'sub' ]:
    ff = open('repldata/%s-roc50.csv' % vt, 'w')
    header = [ 'Score' ]
    for run in runs:
        header += [ '%s disc' % run.name, run.name ]
    print >>ff, ','.join(header)

    for score in xrange(-100, 101):
        fields = [ str(score) ]
        for run in runs:
            fields += [ str(dorn(run, lambda x:x.getReplDisc50(vt, score))), str(dorn(run, lambda x:x.getReplSens50(vt, score))) ]
        print >>ff, ','.join(fields)
    ff.close()

# Print 20%AF ROCs.
for vt in [ 'snp', 'ins', 'del', 'sub' ]:
    ff = open('repldata/%s-roc20.csv' % vt, 'w')
    header = [ 'Score' ]
    for run in runs:
        header += [ '%s disc' % run.name, run.name ]
    print >>ff, ','.join(header)

    for score in xrange(-100, 101):
        fields = [ str(score) ]
        for run in runs:
            fields += [ str(dorn(run, lambda x:x.getReplDisc50(vt, score))), str(dorn(run, lambda x:x.getMixtureSens20(vt, score))) ]
        print >>ff, ','.join(fields)
    ff.close()
