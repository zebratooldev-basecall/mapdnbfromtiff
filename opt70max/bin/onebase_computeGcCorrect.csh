#!/bin/tcsh -efx

set covdir=$1
set simfile=$2
set winwidth=$3
set isFullSimPath=$4

if ( $isFullSimPath != 1 ) then
  set simfile=${simfile}/combined.1.gc-trunc
endif

set testww=`echo $winwidth | awk '{if(int($1/1000)*1000!=$1){print "bad"}else{print "good"}}'`
if ( $testww != "good" ) then
    echo onebase_computeGcCorrect.csh winwidth param must be a multiple of 1000 -- ABORTING
    exit -1
endif

cd $covdir

paste ${simfile} ${covdir}/contig_data/combined.1.gc-trunc  > sim_v_asm

### compute ratio of asm to sim average coverage in GC buckets (buckets = 0, 0.001, 0.002, ...., 1.000)
awk -v ww=$winwidth '{gc=$1;if($3>0){sim[gc]=$2/$3}else{sim[gc]=0};ttlsim+=$2;ttlsimdenom+=$3;if($6>0){asm[gc]=$5/$6}else{asm[gc]=0};ttlasm+=$5;ttlasmdenom+=$6}END{for(i=0;i<=ww;i++)if(sim[i]>0)print i,asm[i]/sim[i]*(ttlsim/ttlsimdenom)/(ttlasm/ttlasmdenom)}' sim_v_asm > ratioToSim

### Smooth asm to sim ratio; first use LOESS ... which sometimes throws warnings or errors, in which case use LOWESS
### Input data and smoothed curve are shown in gc_curve.png
### Smoothing parameters span and degree and f for LOESS and LOWESS were determined empirically, based on visual 
### inspection of data from a limited number of samples, rather than any principled analysis.
R --no-save --no-restore --no-init-file  <<EOF
mydata<-as.matrix(read.table("ratioToSim"))
myX<-mydata[,1]
myY<-mydata[,2]
eval<-(max(myX)-min(myX))+1
tryCatch(myFit<-loess.smooth(myX,myY,evaluation=eval,span=0.06,degree=2),finally=(myFit<-lowess(myX,myY,f=0.06)))
myCurve<-cbind(myFit\$x/$winwidth,1/myFit\$y)
write(t(myCurve),file="loessGC",ncolumns=2,sep="\t")
png(file="gc_curve.png")
plot(myX,myY,ylim=c(0,2))
lines(myFit,col="red")
dev.off()
EOF

### Massage the fit: various GC contents need special treatment; these include:
### - extremely high or low values of GC, for which no positions in the genome provide data
### - those for which the correction estimate was finite (i.e. "expected" [smoothed] coverage was zero)
### - those for which the correction estimate was zero (i.e. expected coverage was negative)
### So, rewrite the file but fix these issues
awk -v ww=$winwidth 'BEGIN{incr=1/ww;f=-incr;prev=-100}\
		     NR==1{for(i=f+incr;i+incr/10<$1;i+=incr)print i,prev}\
                     $2=="Inf"{print $1,-100;prev=-100;f=$1;next}\
                     $2<0{print $1,-100;prev=-100;f=$1;next}\
                     {print $1,$2;prev=$2;f=$1}\
		     END{for(i=f+incr;i<=1.000+incr/10;i+=incr)print i,-100}' loessGC > sim_v_asm.gcCorrectionFactors
