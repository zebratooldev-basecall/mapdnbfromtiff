#!/usr/bin/env python
import os
import StringIO
import pymssql
import ConfigParser
import sets
import itertools
import re

"""
Base class for generic uploader

"""


class genericUpload:

    def __init__(self,table,dbLogin):
         self.table = table
         self.dbLogin=dbLogin
         self.batchSize=55
         self.accumulate=""
         self.insertion_count=0
         self.rowcount=0
        
    def setHeader(self,cols,coltypes):
        self.columns = cols;
        self.qryCols = ",".join(cols)
        self.coltypes=coltypes


    def formatData(self,data):
        def wrapStrings(z):
            if z[0]:
                if z[1] == 'S' or z[1]=='D':
                    return "'"+z[0]+"'"
                else:
                    return z[0]
            else:
                if z[1] == 'I':
                    return '0'
                else:
                    return "NULL"
        return ",".join([wrapStrings(x) for x in zip(data,self.coltypes)])
     
    def uploadTable(self,data,whichDB="idqc:dwProd"):
        conn = pymssql.connect(host=self.dbLogin.get(whichDB,'host'),
                               user=self.dbLogin.get(whichDB,'user'),
                               password=self.dbLogin.get(whichDB,'password'),
                               database=self.dbLogin.get(whichDB,'database'))
        cur = conn.cursor()
        for r in data:
            self.uploadRow(r)
        self.postProcessUpload()
        cur.close()
        conn.close()

    def uploadRow(self,data):
        if data==None:
            self.execute_no_result(self.accumulate)
            self.accumulate = ""
            self.conn.commit()
        else:
            self.accumulate += "insert into %s (%s) values (%s);\n" % (self.table, self.qryCols , self.formatData(data))
            self.insertion_count = self.insertion_count+1
            if self.insertion_count > self.batchSize:
		print self.insertion_count, self.accumulate
                self.execute_no_result(self.accumulate)
                self.accumulate = ""
                self.insertion_count=0
                
    def connect(self,whichDB="idqc:dwProd"):
        self.conn = pymssql.connect(host=self.dbLogin.get(whichDB,'host'),
                               user=self.dbLogin.get(whichDB,'user'),
                               password=self.dbLogin.get(whichDB,'password'),
                               database=self.dbLogin.get(whichDB,'database'))
        if not self.conn:
            print "can't connect"
            os.sys.exit()
        
    def execute(self,query):
        cur = self.conn.cursor()
        cur.execute(query)
        self.rows = cur.fetchall()
        self.rowcount=cur.rowcount
        cur.close()
        if self.rowcount > 0:
            return self.rows[0][0]
        

    def execute_no_result(self,query):
	print ">>database prep init"
        cur = self.conn.cursor()
        print ">>database finished init"
	try:
            cur.execute(query)
        except pymssql.OperationalError:
	    print "cannot execute query"
            pass
	
        self.rowcount=cur.rowcount
        cur.close()

    def getCurrentTime(self):
        dt = self.execute("select convert(varchar(20),getdate(),100)")
        return dt

    def postProcessUpload(self):
        pass
        
# def main():
#     dbLogin = ConfigParser.ConfigParser()
#     dbLogin.read("dbLogin.cfg")
#     print "Content-type: text/html"
#     print



#     form = cgi.FieldStorage()
#     if "no-upload" in form:
#         uploadIDQC(None,True,dbLogin)
#     elif "datafile" in form:
#         uploadIDQC(form["datafile"],False,dbLogin)
#     return


# if __name__ == '__main__':
#     main()

