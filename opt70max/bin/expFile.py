import os

#You can find the examples in the Test section at the exnd of the file

class ExpRecord(object):
    def __init__(self, inDict):
        self.__dict__ = inDict
        
    def __str__(self):
        return self.__dict__.__str__() 
        
#iterates through a CGI tsv file. 
#ExpFile reads the header first and sores it as self.header, then calls next()
#Each time next() is called the state of the object is either self.fieldValues have a new record or self.eof==True
#the column(field) names are stored in the self.fieldNames
#the current data line is stored in self.fieldValues
#Use self.fieldNameToIndex map to get an index of the field you need

class ExpFile(object):
    
    def __init__(self, inFile):
        self.initFields(inFile)
        self.next()
        
    def initFields(self, inFile):
        self.SEPARATOR = '\t'
        if type(inFile) is str:
            self.filename = inFile
            extension = os.path.splitext(inFile)[1]
            if extension.lower()==".gz":
                #use popen3 to suppress the "broken pipe" error if the file is closed before we read through the end 
                childIn, self.file, err = os.popen3('zcat "%s"' % inFile, 'r')
                childIn.close()
                err.close()
            elif extension.lower()==".bz2":
                #use popen3 to suppress the "broken pipe" error if the file is closed before we read through the end 
                childIn, self.file, err = os.popen3('bzcat "%s"' % inFile, 'r')
                childIn.close()
                err.close()
            else:
                self.file = open(inFile)
        else:  
            self.filename = None
            self.file = inFile
                
        self.eof = False

        self.line = "\n"
        self.fieldNames = []
        self.fieldNameToIndex = {}
        self.fieldValues = []
        
        #read header
        self.header = self.readHeader()

    def next(self):
        while self.line:
            dataLine = self.line.strip()
            if not dataLine:
                pass
            elif dataLine[0]=='#':
                pass
            elif dataLine[0]=='>':
                self.fieldNames = dataLine[1:].split(self.SEPARATOR)
                for n in xrange(len(self.fieldNames)):
                    self.fieldNameToIndex[self.fieldNames[n]]=n
            else:
                self.fieldValues = dataLine.split(self.SEPARATOR)
                self.line = self.file.readline()
                return True
            self.line = self.file.readline()
        self.eof = True
        return False
        
    def readHeader(self):
        header = {}
        while True:
            self.line = self.file.readline()
            if not self.line:
                self.eof = True
                return header
            headerLine = self.line.strip()
            if not headerLine:
                continue
            if headerLine[0]!="#":
                return header
            headerRecord = headerLine[1:].split(self.SEPARATOR)
            header[headerRecord[0]] = headerRecord[1]
        return header
    
    def getRecord(self):
        result = ExpRecord(dict(zip(self.fieldNames,self.fieldValues)))
        return result
    
    def close(self):
        self.file.close()

class ExpFileIterator(ExpFile):
    def __init__(self, filename):
        self.initFields(filename)

    def next(self):
        if not super(ExpFileIterator,self).next():
            raise StopIteration()
        return self.getRecord()

    def __iter__(self):
        return self
         
#---------------------- Test

if __name__ == '__main__':
    import StringIO
    inString = """
    #HEADER_VALUE1\t100
    #HEADER_VALUE1\t55
    #HEADER_VALUE1\tNOTHING
    
    >Col1\tCol2\tCol3
    11\t22\t33
    1\t2\t3
    """
     
    testFile1 = StringIO.StringIO(inString)
    expFile = ExpFile(testFile1)
    while not expFile.eof:
        print expFile.fieldValues 
        expFile.next() 
    
    testFile2 = StringIO.StringIO(inString)
    expFileIter = ExpFileIterator(testFile2)
    for rec in expFileIter:
        print rec 
        print rec.Col1, rec.Col2, rec.Col3
