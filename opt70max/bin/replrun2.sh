#! /bin/bash

run=$(basename $(pwd))

echo $run

overrides_full='
{
  "input" : {
              "referenceBuild":"37",
              "includeReadAndMapping":false
            },
  "map" :   {
              "samplingFraction":"1.0"
            },
  "adf" :   {
              "checkSampleId":false
            }
}'

overrides_50='
{
  "input" : {
              "referenceBuild":"37",
              "includeReadAndMapping":false
            },
  "map" :   {
              "samplingFraction":"0.5"
            },
  "adf" :   {
              "checkSampleId":false
            }
}'


asms="/home/ctian/titration/ADF_files/replicate/NA19240_highcvg_A/ADF:stdcvg_A"
asms="$asms /home/ctian/titration/ADF_files/replicate/NA19240_highcvg_B/ADF:stdcvg_B"
asms="$asms /home/ctian/titration/ADF_files/titration/60A40B_full/ADF:60A40B_full"

for adfsname in $asms; do

    adfs=${adfsname%:*}
    name=${adfsname#*:}

    mkdir -p $name
    ln -s $adfs ${name}/ADF

done

rat create ${run}-stdcvg_A onestep.py -c OneStepFromAdf -w $PWD/stdcvg_A -x "$overrides_50"
rat create ${run}-stdcvg_B onestep.py -c OneStepFromAdf -w $PWD/stdcvg_B -x "$overrides_50"
rat create ${run}-60A40B_full onestep.py -c OneStepFromAdf -w $PWD/60A40B_full -x "$overrides_full"
