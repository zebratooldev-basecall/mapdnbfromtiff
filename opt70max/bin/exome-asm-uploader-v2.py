#!/usr/bin/python
import os, re, sys, glob, pymssql, logging, time
from optparse import OptionParser
from ConfigParser import ConfigParser

####################### Gobal settings #######################
metrics_array=[]
bank={}

####################### Basic Function #######################
class DelimReader(object):
    def __init__(self, ff, delimiter=","):
        self.ff = ff
        self.delimiter = delimiter
        self.hFields = self.ff.readline().rstrip('\r\n').split(self.delimiter)

    def close(self):
        self.ff.close()

    def next(self):
        line = self.ff.readline()
        if '' == line:
            return None
        fields = line.rstrip('r\n').split(self.delimiter)
        if len(fields) != len(self.hFields):
            raise Exception('field count mismatch')
        result = {}
        for (name,val) in zip(self.hFields, fields):
            result[name] = val
        return result

def load_list(fn, delimiter=",", ff=None):
    if ff is None:
        ff = open(fn)
    result = []
    reader = DelimReader(ff, delimiter)
    while True:
        val = reader.next()
        if val is None:
            break
        result.append(val)
    reader.close()
    return result

def setup_logger():
    global logger

    # create logger
    logger = logging.getLogger('exome-assembly-uploader_logger')
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

####################### Metrics related: read/calculate/store #######################
class metrics:
    def __init__(self,oldname,from_,to,newname,threshold=''):
        self.oldname=oldname
        self.from_=from_
        self.to=to
        self.newname=newname
        self.threshold=threshold
        self.value='NULL'
    def set_value(self,value):
        self.value=str(value)
    def read_from(self):
        return(self.oldname,self.from_)
    def cprint(self):
        temp = []
        for string in [self.from_,self.oldname,self.threshold,self.value,self.to,self.newname]:
            if string == '': 
                temp.append('Nan')
            else: 
                temp.append(string)
        return(','.join(temp))
    def return_all(self):
        return self.from_,self.oldname,self.threshold,self.value,self.to,self.newname

def setup_metrics():
    global metrics_array

    temp=metrics('asm_name','','exome_asm_metrics_V2','asm_name','')
    metrics_array.append(temp)

    temp=metrics('cls','','exome_asm_metrics_V2','cls','')
    metrics_array.append(temp)

    temp=metrics('dnb','','exome_asm_metrics_V2','dnb','')
    metrics_array.append(temp)

    temp=metrics('Total input full DNBs','WebReports/aqc.tsv','exome_asm_metrics_V2','input_dnbs','')
    metrics_array.append(temp)

    temp=metrics('Gross mapping yield (Gb)','WebReports/aqc.tsv','exome_asm_metrics_V2','gross_coverage_gb','')
    metrics_array.append(temp)

    temp=metrics('Fully mapped yield (Gb)','WebReports/aqc.tsv','exome_asm_metrics_V2','full_coverage_gb','')
    metrics_array.append(temp)

    temp=metrics('Unique full sequence coverage in target regions (Gb)','WebReports/aqc.tsv','exome_asm_metrics_V2','full_target_coverage_gb','')
    metrics_array.append(temp)

    temp=metrics('Non-duplicated / unique full sequence coverage','WebReports/aqc.tsv','exome_asm_metrics_V2','non_duplicate_rate','')
    metrics_array.append(temp)

    temp=metrics('Fraction of searched DNBs with full DNB mappings','WebReports/aqc.tsv','exome_asm_metrics_V2','fraction_searched_with_full_mappings','')
    metrics_array.append(temp)

    temp=metrics('Min per-library pct with mates paired (sampled)','WebReports/aqc.tsv','exome_asm_metrics_V2','min_pct_with_mates_paired_sampled','')
    metrics_array.append(temp)

    temp=metrics('Overall discordance','WebReports/aqc.tsv','exome_asm_metrics_V2','overall_discordance','')
    metrics_array.append(temp)

    temp=metrics('TargetRegions On-target capture rate, unique mappings','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','on_target_capture_rate_unique','')
    metrics_array.append(temp)

    temp=metrics('Padded_200 On-target capture rate, unique mappings','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','on_target_capture_rate_200_unique','')
    metrics_array.append(temp)

    temp=metrics('% target with < 0.2 mean coverage','','exome_asm_metrics_V2','pct_target_under_one_fifth_mean','')
    metrics_array.append(temp)

    temp=metrics('% autosomal exome by GC with < 0.6 mean coverage','WebReports/aqc.tsv','exome_asm_metrics_V2','pct_exome_by_gc_under_three_fifths_mean','')
    metrics_array.append(temp)

    temp=metrics('% target with < 0.6 mean coverage','','exome_asm_metrics_V2','pct_target_by_gc_under_three_fifths_mean','')
    metrics_array.append(temp)

    temp=metrics('% autosomal exome with coverage >= 5','WebReports/aqc.tsv','exome_asm_metrics_V2','pct_auto_coding_gte5x_unqiue_coverage','')
    metrics_array.append(temp)

    temp=metrics('% autosomal exome with coverage >= 10','WebReports/aqc.tsv','exome_asm_metrics_V2','pct_auto_coding_gte10x_unqiue_coverage','')
    metrics_array.append(temp)

    temp=metrics('% autosomal exome with coverage >= 15','WebReports/aqc.tsv','exome_asm_metrics_V2','pct_auto_coding_gte15x_unqiue_coverage','')
    metrics_array.append(temp)

    temp=metrics('% autosomal exome with coverage >= 30','WebReports/aqc.tsv','exome_asm_metrics_V2','pct_auto_coding_gte30x_unqiue_coverage','')
    metrics_array.append(temp)

    temp=metrics('% autosomal exome with coverage >= 40','WebReports/aqc.tsv','exome_asm_metrics_V2','pct_auto_coding_gte40x_unqiue_coverage','')
    metrics_array.append(temp)

    temp=metrics('TargetRegions Percent of the target region covered at >=1x   from unique mappings','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','pct_auto_target_gte1x_unqiue_coverage','')
    metrics_array.append(temp)

    temp=metrics('TargetRegions Percent of the target region covered at >=10x  from unique mappings','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','pct_auto_target_gte10x_unqiue_coverage','')
    metrics_array.append(temp)

    temp=metrics('TargetRegions Percent of the target region covered at >=15x  from unique mappings','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','pct_auto_target_gte15x_unqiue_coverage','')
    metrics_array.append(temp)

    temp=metrics('TargetRegions Percent of the target region covered at >=30x  from unique mappings','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','pct_auto_target_gte30x_unqiue_coverage','')
    metrics_array.append(temp)

    temp=metrics('TargetRegions Percent of the target region covered at >=40x  from unique mappings','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','pct_auto_target_gte40x_unqiue_coverage','')
    metrics_array.append(temp)

    temp=metrics('TargetRegions Percent of the target region covered at >=100x from unique mappings','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','pct_auto_target_gte100x_unqiue_coverage','')
    metrics_array.append(temp)

    temp=metrics('TargetRegions Mean coverage from unique mappings','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','target_regions_mean_coverage_unique','')
    metrics_array.append(temp)

    temp=metrics('TargetRegions Median coverage from unique mappings','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','target_regions_median_coverage_unqiue','')
    metrics_array.append(temp)

    temp=metrics('Fully called target sequence fraction','WebReports/aqc.tsv','exome_asm_metrics_V2','fully_called_target_fraction','VQHIGH')
    metrics_array.append(temp)

    temp=metrics('Fully called coding sequence fraction','WebReports/aqc.tsv','exome_asm_metrics_V2','fully_called_exome_fraction','VQHIGH')
    metrics_array.append(temp)

    temp=metrics('left stacking metrics','','exome_asm_metrics_V2','left_arm_stacked_alt_support','')
    metrics_array.append(temp)

    temp=metrics('right stacking metrics','','exome_asm_metrics_V2','right_arm_stacked_alt_support','')
    metrics_array.append(temp)

    temp=metrics('SNP_total_count','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','snp_count_eaf','EAF_20_40')
    metrics_array.append(temp)

    temp=metrics('INS_total_count','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','ins_count_eaf','EAF_20_40')
    metrics_array.append(temp)

    temp=metrics('DEL_total_count','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','del_count_eaf','EAF_20_40')
    metrics_array.append(temp)

    temp=metrics('SUB_total_count','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','sub_count_eaf','EAF_20_40')
    metrics_array.append(temp)

    temp=metrics('SNP_novel_rate','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','snp_novel_rate_eaf','EAF_20_40')
    metrics_array.append(temp)

    temp=metrics('INS_novel_rate','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','ins_novel_rate_eaf','EAF_20_40')
    metrics_array.append(temp)

    temp=metrics('DEL_novel_rate','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','del_novel_rate_eaf','EAF_20_40')
    metrics_array.append(temp)

    temp=metrics('SUB_novel_rate','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','sub_novel_rate_eaf','EAF_20_40')
    metrics_array.append(temp)

    temp=metrics('SNP_Transitions_transversions','WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','snp_titv_ratio_eaf','EAF_20_40')
    metrics_array.append(temp)

    temp=metrics('Gross sequence coverage in target regions (Gb)', 'WebReports/aqc.tsv','exome_asm_metrics_V2','gross_target_coverage_gb','')
    metrics_array.append(temp)

    temp=metrics('TargetRegions Mean coverage from gross mappings', 'WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','target_regions_mean_coverage_gross','')
    metrics_array.append(temp)

    temp=metrics('TargetRegions Median coverage from gross mappings', 'WebReports/Exome/Summary.tsv','exome_asm_metrics_V2','target_regions_median_coverage_gross','')
    metrics_array.append(temp)

    temp=metrics('target_regions_mean_coverage_full', '','exome_asm_metrics_V2','target_regions_mean_coverage_full','')
    metrics_array.append(temp)

    temp=metrics('target_regions_median_coverage_full', '','exome_asm_metrics_V2','target_regions_median_coverage_full','')
    metrics_array.append(temp)

    temp=metrics('Mate distribution mean', 'WebReports/aqc.tsv', 'exome_asm_metrics_V2','mate_distribution_mean','')
    metrics_array.append(temp)

    global bank
    for i in range(len(metrics_array)):
        oldname,from_ = metrics_array[i].read_from()
        if not from_ in bank.keys():
            bank[from_]={}
        bank[from_][oldname] = i

def read_metrics(input_dir):
    input_tables = bank.keys()

    for tableName in input_tables:
        if tableName == '': continue
        metrics_name = bank[tableName].keys()

        delimiter = "\t"
        if re.search('csv$',tableName):
            delimiter = ','

        ff = open(os.path.join(input_dir,tableName),'r')
        for line in ff.readlines():
            fields = line.rstrip('\r\n').split(delimiter)
            if fields[0] in metrics_name:
                oldname = fields[0]
                i = bank[tableName][oldname]
                if len(fields) == 2:
                    metrics_array[i].set_value(fields[1])
                elif fields[1] == metrics_array[i].threshold:
                    metrics_array[i].set_value(fields[2])    

def caculate_metrics(input_dir):
    #TargetRegions Percent of the target region covered
    cal_TR_percent_of_coverage(input_dir)

    #stacking Metrics: calculated in load_stacking_histogram

def cal_TR_percent_of_coverage(input_dir):
    fp = os.path.join(input_dir, "ASM", "coverage", "allTargetCoverageSummary.csv")
    table = load_list(fp, ',')
    total_base_count, total_coverage_count = 0,0
    for data in table:
        coverage = int(data['Coverage'])
        base_count = int(data['calledcvg:weightratio>.99'])
        total_base_count += base_count
        total_coverage_count += (base_count*coverage)

    avg_coverage = float(total_coverage_count)/total_base_count
    logger.debug('  - average_coverage=%f...' % avg_coverage)
    base_count_lt_1fith_mean, base_count_lt_3fith_mean = 0,0
    for data in table:
        coverage = int(data['Coverage'])
        base_count = int(data['calledcvg:weightratio>.99'])
        if coverage < avg_coverage*0.2:
            base_count_lt_1fith_mean += base_count
        if coverage < avg_coverage*0.6:
            base_count_lt_3fith_mean += base_count
    i = bank['']['% target with < 0.2 mean coverage']
    metrics_array[i].set_value(float(base_count_lt_1fith_mean)/total_base_count*100)
    i = bank['']['% target with < 0.6 mean coverage']
    metrics_array[i].set_value(float(base_count_lt_3fith_mean)/total_base_count*100)

    total_base_count, total_coverage_count = 0,0
    for data in table:
        coverage = int(data['Coverage'])
        base_count = int(data['calledcvg:full:apply=weightratio:weightratio>.01'])
        total_base_count += base_count
        total_coverage_count += (base_count*coverage)
    mean = float(total_coverage_count)/total_base_count

    median = 0 
    total_base_count2 = 0
    for data in table:
        coverage = int(data['Coverage'])
        base_count = int(data['calledcvg:full:apply=weightratio:weightratio>.01'])
        total_base_count2 += base_count
        if float(total_base_count2)/total_base_count > 0.5:
            median = coverage
            break
    i = bank['']['target_regions_mean_coverage_full']
    metrics_array[i].set_value(mean)
    i = bank['']['target_regions_median_coverage_full']
    metrics_array[i].set_value(median)

def print_metrics():
    logger.info("Printing...Metrics Collecting result")
    print ">Metrics Collecting result\nFrom,OldName,Threshold,Value,To,Newname"
    for i in range(len(metrics_array)):
        print metrics_array[i].cprint()
    print ">"

def get_dnb_from_ReportDB(input_dir):
    adfs = [ os.path.basename(x) for x in glob.glob(os.path.join(input_dir,'MAP','GS*'))]

    conn = pymssql.connect(host='vip-psqldb04:1433', user='GCW_readonly', password='G3n0m1cs', database='ReportDB')
    cur = conn.cursor()
    sql = ''' SELECT DISTINCT sl.slide_barcode + '-L0' + cast(sl.lane AS varchar) AS slidelane, 
        pDNB.dnb_barcode + '_' + pDNB.dnb_well as DNB
        FROM [ReportDB].[dbo].[pub_SlideLane] AS sl
        inner join [ReportDB].[dbo].[pub_SlideLaneDNB] AS slDNB on sl.pub_Slide_Lane_Id = slDNB.pub_slide_lane_Id
        inner join [ReportDB].[dbo].[pub_DNB] AS pDNB on pDNB.pub_dnb_Id = slDNB.pub_dnb_Id
        where sl.slide_barcode + '-L0' + cast(sl.lane AS varchar) in ('%s')  ''' %  ('\',\''.join(adfs))
    cur.execute(sql)

    dnbs = list(set([row[1] for row in cur]))

    dnb = dnbs[0]
    if len(dnbs) != 1:
        logger.info("the numbers of dnb id is not one, join them as a string in the database %s \n" % ",".join(dnbs))
        dnb = ",".join(dnbs)
    return dnb

def get_member_from_ReportDB(cls):
    conn = pymssql.connect(host='vip-psqldb04:1433', user='GCW_readonly', password='G3n0m1cs', database='ReportDB')
    cur = conn.cursor()
    sql = ''' SELECT dp.CLS,dpm.CLS 
        FROM dwProd.jsheehan.dev_dnb_pool as dp 
        inner join dwProd.jsheehan.dev_dnb_pool_mbr as dpm on dp.dev_dnb_pool_id=dpm.dev_dnb_pool_id 
        WHERE dpm.replaced IS NULL and dp.CLS = \'%s\' ''' % (cls)
    cur.execute(sql)
    return [row[1] for row in cur]

def get_tag_from_ReportDB(cls):
    temp = {}
    conn = pymssql.connect(host='vip-psqldb04:1433', user='GCW_readonly', password='G3n0m1cs', database='ReportDB')
    cur = conn.cursor()
    sql = ''' SELECT dp.nm,dp.CLS,tdp.batch,tdp.well,dpm.CLS,tdpm.batch,tdpm.well,ts.nm,ts.dscrpt,t.seq
        FROM dwProd.jsheehan.dev_dnb_pool AS dp
        INNER JOIN dwProd.jsheehan.dev_dnb_pool_mbr AS dpm ON dp.dev_dnb_pool_id=dpm.dev_dnb_pool_id
        INNER JOIN dwProd.jsheehan.dev_try AS tdp ON dp.dev_try_id=tdp.dev_try_id
        INNER JOIN dwProd.jsheehan.dev_try AS tdpm ON dpm.dev_try_id=tdpm.dev_try_id
        INNER JOIN dwProd.jsheehan.dev_try_tag_set AS tts ON tdpm.dev_try_id=tts.dev_try_id
        INNER JOIN dwProd.jsheehan.dev_tag_set AS ts ON tts.dev_tag_set_id=ts.dev_tag_set_id
        INNER JOIN dwProd.jsheehan.dev_tag_set_member AS tsm ON ts.dev_tag_set_id=tsm.dev_tag_set_id
        INNER JOIN dwProd.jsheehan.dev_tag AS t ON tsm.dev_tag_id=t.dev_tag_id
        WHERE tts.replaced IS NULL and dpm.replaced IS NULL and tdp.replaced IS NULL and tdpm.replaced IS NULL and dp.CLS=\'%s\'
        ORDER BY dp.nm,tdp.well,tdpm.well ''' % cls
    cur.execute(sql)
    for row in cur: 
        member_cls = row[4]
        tag = row[7]
        temp[member_cls] = tag
    return temp

def solve_the_pooling_relationship(ind_cls, wes_cls, fs_cls, cls_from_collection,tagset_id):
    if tagset_id: # this data is from pooling lanes [demuxed adf]
        logger.info('Will use tagset id %s from overrides' % (tagset_id,))
    ## FIX THIS!!----------------
    else: # this non-demux adf
        logger.info('This assembly used non-demuxed adf, or treat it as non-demuxed')

    asm_cls = None
    asm_cls_type = None

    member_in_cls_from_collection = get_member_from_ReportDB(cls_from_collection)
    
    # simplest case - CLS is individual CLS - not pooled
    if len(member_in_cls_from_collection) == 0:
        #cls_from_collection is an individual cls
        logger.info('The cls in the collection is an individual cls')
        if tagset_id and ind_cls is None:
            logger.error('This lane is not pooling data, why there is a tag?')
            sys.exit(-1)
        elif ind_cls is None: 
            # set values for unpooled data
            ind_cls = cls_from_collection
            asm_cls = ind_cls
            asm_cls_type = 'ind'

    # more than one CLS found
    else:
        member_in_member = {}
        for member_cls in member_in_cls_from_collection:
            member_in_member[member_cls] = get_tag_from_ReportDB(member_cls)
        # if all of the CLSs found have no sub-CLSs...
        if sum([len(member_in_member[member_cls].keys()) for member_cls in member_in_cls_from_collection]) == 0:
            #cls_from_collection is an wes cls           
            if tagset_id:
                logger.info('The cls in the collection is an WES cls, and this is an individual assembly')
                WES_tag_table = get_tag_from_ReportDB(cls_from_collection)
                for cls,tag in WES_tag_table.iteritems():
                    if tag == tagset_id:
                        if asm_cls_type:
                            logger.error('There are two same tagsets pooled together: %s in %s and %s' % (tagset_id, ind_cls, cls))
                            sys.exit(-1)
                        if ind_cls is None: 
                            ind_cls = cls
                        if wes_cls is None: 
                            wes_cls=cls_from_collection
                        fs_cls = None
                        asm_cls = ind_cls
                        asm_cls_type = 'ind'
                        break
                # no matches found in xTrackr for the tagset ID you provided
                if asm_cls_type is None:
                    logger.info('No records match your tagset_id, please check.  Individual CLS cannot be automatically determined.')
                    if ind_cls is None:
                        logger.error('Tagset ID does not match records in database.  Individual CLS must be provided.')
                        sys.exit(-1)
                    if wes_cls is None: 
                        wes_cls=cls_from_collection
                    fs_cls = None
                    asm_cls = ind_cls
                    asm_cls_type = 'ind'
            else:
                logger.info('The cls in the collection is an WES cls, and this is a mixed assembly')
                ind_cls = None
                if wes_cls is None: wes_cls=cls_from_collection
                fs_cls = None
                asm_cls = wes_cls
                asm_cls_type = 'wes'
        else:
            #cls_from_collection is an fs cls
            if tagset_id:
                for member_cls in member_in_cls_from_collection:
                    WES_tag_table = member_in_cls_from_collection[member_cls]
                    for cls,tag in WES_tag_table.items():
                        if tag == tagset_id:
                            if not (asm_cls_type is None):
                                logger.error('There is two same tagset pooled together: %s in %s and %s' % (tagset_id, ind_cls, cls))
                                sys.exit(-1)
                            if ind_cls is None: ind_cls = cls
                            if ind_cls is None: ind_cls = cls
                            if wes_cls is None: wes_cls= member_cls
                            if fs_cls is None: fs_cls = cls_from_collection
                            asm_cls = ind_cls
                            asm_cls_type = 'ind'
                            break
                if asm_cls_type is None:
                    logger.error('No record match your tagset_id, please check')
                    sys.exit(-1)
            else:
                logger.info('The cls in the collection is an FS cls, and this is a mixed assembly')
                ind_cls = None
                wes_cls = None
                if fs_cls is None: fs_cls=cls_from_collection
                asm_cls = fs_cls
                asm_cls_type = 'fs'
    return asm_cls, asm_cls_type, ind_cls, wes_cls, fs_cls

def get_cls_from_collection(input_dir):
    import xml.dom.minidom
    collection=xml.dom.minidom.parse(os.path.join(input_dir,'collection.xml'))
    clses = [lib.getAttribute('name') for lib in collection.getElementsByTagName('Library')]
    clses = list(set(clses))
    if len(clses) == 0 :
        logger.error('No cls is found in the collection file, please tell me how you assemble it')
        sys.exit(-1)
    elif len(clses) > 1:
        logger.error('More than one CLS in the collection file, which is not suppose to happen in exome assembly')
        sys.exit(-1)
    else:
        return str(clses.pop(0))

def test_asm_name_unique(asm_id):
    conn = pymssql.connect(host='vip-psqldb04:1433', user='gcw_IDQC', password='CG!updat6', database='dwProd')
    cur = conn.cursor()
    sql = 'SELECT COUNT(*) FROM [dwProd].[bioinfo].[exome_asm_id_V2] where [asm_name] = \'%s\'' % asm_id
    cur.execute(sql)

    if int(cur.fetchall()[0][0]) != 0 :
        return False
    else:
        return True  

def get_asm_name(asm_cls,asm_cls_type,wes_cls,fs_cls,dnb,sequence_platform,baits,tagset_id,coverage_level):
    conn = pymssql.connect(host='vip-psqldb04:1433', user='gcw_IDQC', password='CG!updat6', database='dwProd')
    cur = conn.cursor()

    #get the max 
    sql = 'SELECT MAX(CAST(RIGHT(LEFT([asm_name],12),10) as INT)) FROM [dwProd].[bioinfo].[exome_asm_id_V2] WHERE [asm_name] like \'EX%%ASM\' '
    cur.execute(sql)
    count = int(cur.fetchall()[0][0])+1

    asm_name = "EX%010d-ASM" % (count)

    #insert the information back
    insert_asm_name(asm_name,asm_cls,asm_cls_type,wes_cls,fs_cls,dnb,sequence_platform,baits,tagset_id,coverage_level)
    return asm_name

def insert_asm_name(asm_name,asm_cls,asm_cls_type,wes_cls,fs_cls,dnb,sequence_platform,baits,tagset_id,coverage_level):
    #insert the information back
    conn = pymssql.connect(host='vip-psqldb04:1433', user='gcw_IDQC', password='CG!updat6', database='dwProd')
    cur = conn.cursor()
    sql = ''' INSERT INTO [dwProd].[bioinfo].[exome_asm_id_V2] (asm_name,cls,cls_type,wes_cls,fs_cls,dnb,sequence_platform,baits,tagSet,coverage_level,isCompleted) VALUES (\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',0) ''' % (asm_name,asm_cls,asm_cls_type,wes_cls,fs_cls,dnb,sequence_platform,baits,tagset_id,coverage_level)    
    cur.execute(sql)
    cur.close()
    conn.commit()
    conn.close()
    return 

def override_asm_name(asm_name,asm_cls,asm_cls_type,wes_cls,fs_cls,dnb,sequence_platform,baits,tagset_id,coverage_level):
    #update the information back
    conn = pymssql.connect(host='vip-psqldb04:1433', user='gcw_IDQC', password='CG!updat6', database='dwProd')
    cur = conn.cursor()
    sql = ''' UPDATE [dwProd].[bioinfo].[exome_asm_id_V2] SET cls=\'%s\', cls_type=\'%s\', wes_cls=\'%s\', fs_cls=\'%s\', dnb=\'%s\', sequence_platform=\'%s\', baits=\'%s\', tagSet=\'%s\', coverage_level=\'%s\', isCompleted=0 WHERE asm_name=\'%s\' ''' % (asm_cls,asm_cls_type,wes_cls,fs_cls,dnb,sequence_platform,baits,tagset_id,coverage_level,asm_name)    
    
    cur.execute(sql)
    cur.close()
    conn.commit()
    conn.close()
    return 

def update_isComplete(asm_name):
    conn = pymssql.connect(host='vip-psqldb04:1433', user='gcw_IDQC', password='CG!updat6', database='dwProd')
    cur = conn.cursor()
    sql = ''' UPDATE [dwProd].[bioinfo].[exome_asm_id_V2] SET isCompleted=1 WHERE asm_name=\'%s\' ''' % asm_name
    cur.execute(sql)
    cur.close()
    conn.commit()
    conn.close()

####################### Curve related: read/calculate/store #######################
def load_coverage_by_gc(asm_name, input_dir, type):
    sql_accumulator = []
    logger.info('Loading coverage by gc curve for %s...' % type)
    n = 0

    # check type to set filename
    if type == "g":
        fp = os.path.join(input_dir, "ASM", "coverage", "allCoverageByContentGC500.csv")
    elif type == "e":
        fp = os.path.join(input_dir, "ASM", "coverage", "autoExomeCoverageByContentGC500.csv")
    elif type == "t":
        fp = os.path.join(input_dir, "ASM", "coverage", "allTargetCoverageByContentGC500.csv")
    else:
        logger.error("Unknown coverage type %s! exome-assembly-uploader.py::load_coverage_by_gc()" % type)
        sys.exit(-1)
    logger.info('  - input file %s' % fp)

    # load table.
    table = load_list(fp, ',')

    # compute average coverage.
    base_count, coverage_count = 0, 0
    for data in table:
        base_count += int(data['Count'])
        coverage_count += int(data['calledcvg:weightratio>.99'])

    avg_coverage = float(coverage_count) / float(base_count)
    logger.debug('  - average_coverage=%f...' % avg_coverage)

    # loop through histogram and calculate cumulative base percentile and normalized coverage.
    cum_count = 0
    logger.info("Printing...GC curve")
    print ">GC Curve\nasm_name,type,bin,cumulative_basecount_percentile_by_gc,normalized_coverage"
    for data in table:
        cum_count += int(data['Count'])
        cumulative_percentile = float(cum_count) / float(base_count)
        norm_coverage = float(data['calledcvg:weightratio>.99']) / float(data['Count']) / avg_coverage

        sqlString = "insert into [bioinfo].[exome_asm_coverage_by_gc_V2] (asm_name,[type],[bin],cumulative_basecount_percentile_by_gc,normalized_coverage) values ('%s','%s',%d,%f,%f)" % (asm_name,type,n,cumulative_percentile,norm_coverage)
        print "%s,%s,%d,%f,%f" % (asm_name,type,n,cumulative_percentile,norm_coverage)
        sql_accumulator.append(sqlString)
        n += 1
    print ">"
    return sql_accumulator

def load_stacking_histogram(asm_name, input_dir, var_type):
    sql_accumulator = []
    logger.info('Trying to load the dnb stacking histogram')
    n = 0

    stacking_cutoff = 79
    path = os.path.join(input_dir, 'varSupport', 'GS*', 'summary', '%s_dnb_stacking.tsv' % var_type)
    if len(glob.glob(path)) == 0:
        return []
    fp = glob.glob(path).pop(0)

    logger.info('  - input file %s...' % fp)

    # if file does not exists we assume varSupport was not ran.
    if not os.path.exists(fp):
        logger.warning('  - dnb stacking histogram file does not exists!')
        return (None, None)

    # load table.
    table = load_list(fp, '\t')

    # create insert statements.
    left_arm_var_count, right_arm_var_count = 0, 0
    left_stacked_count, right_stacked_count = 0, 0
    logger.info('Printing...Stacking histogram')
    print ">Stacking histogram\nasm_name,var_type,support_type,side,stacking_percent,variant_count"
    for data in table:
        sqlString = "insert into [bioinfo].[exome_asm_dnb_stacking_V2] (asm_name,var_type,support_type,side,stacking_percent,variant_count) values ('%s','%s','%s','%s',%s,%s)" % (asm_name,var_type,data['support_type'],data['side'],data['stacking_percent'],data['variant_count'])
        sql_accumulator.append(sqlString)
        print ",".join([asm_name,var_type,data['support_type'],data['side'],data['stacking_percent'],data['variant_count']])
        n += 1
        if data['support_type'] == 'alt':
            if data['side'] == 'L':
                left_arm_var_count += int(data['variant_count'])
                if int(data['stacking_percent']) > stacking_cutoff:
                    left_stacked_count += int(data['variant_count'])
            else:
                right_arm_var_count += int(data['variant_count'])
                if int(data['stacking_percent']) > stacking_cutoff:
                    right_stacked_count += int(data['variant_count'])
    print ">"

    left_stacking_fraction = float(left_stacked_count) / float(left_arm_var_count)
    right_stacking_fraction = float(right_stacked_count) / float(right_arm_var_count)

    logger.info('  - %d sql inserts added to accumulator' % n)
    logger.info('  - left_stacking_fraction=%f, right_stacking_fraction=%f' % (left_stacking_fraction, right_stacking_fraction))

    i = bank['']['left stacking metrics']
    metrics_array[i].set_value(left_stacking_fraction)
    i = bank['']['right stacking metrics']
    metrics_array[i].set_value(right_stacking_fraction)
    
    return sql_accumulator

def load_positional_discordance(asm_name,input_dir):
    sql_accumulator = []
    slidelanes = [os.path.basename(x) for x in glob.glob(os.path.join(input_dir,'MAP','GS*'))]
    logger.info('Trying to load the positional discordance')

    for sl in slidelanes:
        logger.info("Printing...Positional discordance: %s" % sl)
        print ">Positional discordance\nasm_name,slidelane,position,discordance_pct"
        fp = os.path.join(input_dir,'MAP',sl,'reports/MappingStats.csv')
        logger.info('  - input file %s...' % fp)

        blocks = open(fp,'r').read().split("\n\n")
        lines = blocks[1].split("\n")
        lines.pop(0)
        lines.pop(0)
        for line in lines:
            position,totalBaseCount,discordantBaseCount = [int(x) for x in line.split(',')]
            if totalBaseCount == 0: continue
            dis_pct = float(discordantBaseCount)/totalBaseCount * 100
            print ','.join(str(x) for x in [asm_name,sl,position,dis_pct])

            sqlString = "insert into [bioinfo].[exome_asm_positional_discordance_V2] (asm_name,slidelane,position,discordance_rate) values ('%s','%s',%s,%f)" % (asm_name,sl,position,dis_pct)
            sql_accumulator.append(sqlString)

    return sql_accumulator

####################### Insert to sql server #######################
def execute_sql_statements(asm_name, dbLogin, sql_accumulator, whichDB="idqc:dwProd"):
    logger.info('Executing accumulated SQL statements')
    logger.info('  - %d sql inserts to execute' % len(sql_accumulator))
    conn = pymssql.connect(host=dbLogin.get(whichDB,'host'),
                        user=dbLogin.get(whichDB,'user'),
                        password=dbLogin.get(whichDB,'password'),
                        database=dbLogin.get(whichDB,'database'))

    # check connection.
    if not conn:
        logger.error('Unable to connect to %s!' % dbLogin.get(whichDB,'host'))
        os.sys.exit(-1)

    # setup cursor.
    cur = conn.cursor()

    # loop through sql statements and execute 50 at a time. This is due to 
    # string length restrictions in pymssql.
    i, istart, iend = 0, 0, 0
    exit_cond = False
    while True:
        # set slice bounds
        istart = i * 50
        iend = istart + 50
        if iend > len(sql_accumulator):
            iend = len(sql_accumulator)
            exit_cond = True

        logger.info('  - inserting statements %d through %d...' % (istart,iend))

        # create cursor.
        cur = conn.cursor()

        # execute a slice of accumulated sql statements.
        cur.execute('\n'.join(sql_accumulator[istart:iend]))

        # close connection.
        cur.close()

        i += 1

        # exit condition
        if exit_cond: break

    # commit.
    conn.commit()

    logger.info('Loading of exome-asm %s complete...' % asm_name)

def insert_metrics(asm_name,cls_id,input_dir,dbUserConfig):
    # read metrics
    read_metrics(input_dir)

    # calculate metrics
    caculate_metrics(input_dir)

    # print out the metrics
    print_metrics()

    # generate the sql_accumulator
    sql_accumulator =[]
    tempbank = {}
    for i in range(len(metrics_array)):
        to = metrics_array[i].to
        if to == "" :
            logger.error("we have a metrics does not know where to upload %s" % metrics_array[i].oldname)
        if not to in tempbank.keys():  tempbank[to] = []
        tempbank[to].append(i)

    for to in tempbank.keys():
        target_array = []
        value_array = []

        for i in tempbank[to]:
            from_,oldname,threshold,value,to,newname = metrics_array[i].return_all()
            target_array.append('[%s]' % newname)
            value_array.append(str(value))
        sqlString = "insert into [bioinfo].[%s] (%s) values (%s)" % (to, ','.join(target_array), ','.join(value_array))
        sql_accumulator.append(sqlString)

    #operate the insertion
    execute_sql_statements(asm_name, dbUserConfig, sql_accumulator)

def insert_curves(asm_name,input_dir,dbUserConfig):
    # GC curves
    exome_gc_curve = load_coverage_by_gc(asm_name, input_dir, type='e')
    target_gc_curve = load_coverage_by_gc(asm_name, input_dir, type='t')
    
    # discordance curves
    positional_dis_curve = load_positional_discordance(asm_name,input_dir)

    # stacking hist
    stacking_curve = load_stacking_histogram(asm_name, input_dir, 'snp')

    # upload all the data in once
    sql_accumulator = exome_gc_curve + target_gc_curve + positional_dis_curve + stacking_curve

    #operate the insertion
    execute_sql_statements(asm_name, dbUserConfig, sql_accumulator)

####################### Main Function #######################
def parse_arguments(args):
    parser = OptionParser()

    parser.add_option('-a', '--asm-directory', help='exome-assembly working directory', default=None, dest='input_dir')
    parser.add_option('-c', '--db-config', help='Path to db user config file', default=None, dest='config_file')

    parser.add_option('--tag-set-id', help='tagset id you want to upload. Attention: if provided, will be used in place of xTrackr results', default=None,dest='tagset_id')
    parser.add_option('--ind-cls', help='individual CLS id you want to upload', default=None,dest='ind_cls')
    parser.add_option('--wes-cls', help='WES CLS id you want to upload.  Usually a pool of individual CLSs', default=None,dest='wes_cls')
    parser.add_option('--fs-cls', help='flow-cell CLS id you want to upload.  Usually a pool of WES CLSs', default=None,dest='fs_cls')

    parser.add_option('--asm-id', help='asm name/id you want to upload', default=None,dest='asm_name')

    parser.add_option('-d', '--dnb-id', help='dnb id you want to upload', default=None,dest='dnb')

    parser.add_option('--sequence-platform', help='sequence platform information', default='BB',dest='sequence_platform')
    parser.add_option('--baits', help='capture baits', default=None,dest='baits')
    parser.add_option('--coverage-level', help='coverage level', default='raw',dest='coverage_level')

    parser.add_option('--override-mode', help='if true, then override the information in the asmID table',action="store_true", default=False,dest='over')

    options, args = parser.parse_args(args)
    input_dir = options.input_dir
    config_file = options.config_file
    tagset_id = options.tagset_id
    ind_cls = options.ind_cls
    wes_cls = options.wes_cls
    fs_cls = options.fs_cls
    dnb = options.dnb
    sequence_platform = options.sequence_platform
    baits = options.baits
    coverage_level = options.coverage_level
    asm_name = options.asm_name
    isOverride = options.over

    logger.info('Information check before runing the upload')

    # make sure input directory is set.
    if input_dir == None:
        logger.error('Input directory is required!')
        sys.exit(1)

    # make sure the db config file paramter is set.
    if config_file == None:
        logger.error('DB user config file parameter is required!')
        sys.exit(-1)

    # make sure the baits information
    if baits is None:
        logger.error('baits information is required!')
        sys.exit(-1)

    cls_from_collection = get_cls_from_collection(input_dir)
    asm_cls, asm_cls_type, ind_cls, wes_cls, fs_cls = solve_the_pooling_relationship(ind_cls, wes_cls, fs_cls, cls_from_collection,tagset_id)

    if dnb is None:
        dnb = get_dnb_from_ReportDB(input_dir)

    # get asm_name, and upload the upstream infomation to the [dwProd].[bioinfo].[exome_asm_id_V2]
    if asm_name is None: 
        asm_name = get_asm_name(asm_cls,asm_cls_type,wes_cls,fs_cls,dnb,sequence_platform,baits,tagset_id,coverage_level)
    else:
        if isOverride:
            override_asm_name(asm_name,asm_cls,asm_cls_type,wes_cls,fs_cls,dnb,sequence_platform,baits,tagset_id,coverage_level)
        else:
            insert_asm_name(asm_name,asm_cls,asm_cls_type,wes_cls,fs_cls,dnb,sequence_platform,baits,tagset_id,coverage_level)

    # record the asm_name, cls and dnb in the metrix array
    i = bank['']['asm_name']
    metrics_array[i].set_value('\'%s\'' % asm_name)
    i = bank['']['cls']
    metrics_array[i].set_value('\'%s\'' % asm_cls)
    i = bank['']['dnb']
    metrics_array[i].set_value('\'%s\'' % dnb)

    logger.info('Running exome-asm uploader tool for the following assembly %s...' % input_dir)

    return input_dir, config_file, asm_name, asm_cls, dnb

def main(arguments):
    # set up logger
    setup_logger()

    # set the metrics container
    setup_metrics()

    # parse command line arguments.
    input_dir, config_file, asm_name, cls_id, dnb = parse_arguments(arguments[1:])

    # setup the db config.
    dbUserConfig = ConfigParser()
    dbUserConfig.read(config_file)

    ### insert the curves
    insert_curves(asm_name,input_dir,dbUserConfig)

    ### insert the metrics
    insert_metrics(asm_name,cls_id,input_dir,dbUserConfig)

    ### complete the flag
    update_isComplete(asm_name)

main(sys.argv)
