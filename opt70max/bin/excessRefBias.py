#!/bin/env python
import os,sys
import optparse
from cgatools.util import DelimitedFile
import math

# Output X, Y, and Z as defined below
#
# A,B,C,D are counts of novel het SNP calls with good total read count
#
# A = count of ref-biased in repeat
# B = count of alt-biased in repeat
# C = count of ref-biased in non-repeat
# D = count of alt-biased in non-repeat
#
# X = A-B = excess ref-biased in repeat
# Y = C-D = excess ref-biased in non-repeat
# Z = distance of (X,Y) to pass/fail boundary, positive if in fail, negative if in pass region
#
#
# Processing looks like this:
#
# |pseudocode|   while reading masterVar
# |pseudocode|      skip unless a variant:
# |pseudocode|          - is het-ref,
# |pseudocode|          - is snp
# |pseudocode|          - is VQHIGH on both alleles
# |pseudocode|          - has null XRef on both alleles
# |pseudocode|          - has alt+ref counts > 20
# |pseudocode|          - has alt fraction outside [.25,.75]
# |pseudocode|  
# |pseudocode|      if alt/(alt+ref)<.25, ref-biased
# |pseudocode|      if alt/(alt+ref)>.75, alt-biased
# |pseudocode|      
# |pseudocode|      if in repeat and ref-biased, A+=1
# |pseudocode|      if in repeat and alt-biased, B+=1
# |pseudocode|      if not in repeat and ref-biased, C+=1
# |pseudocode|      if not in repeat and alt-biased, D+=1
# |pseudocode|  
# |pseudocode|   X = A-B
# |pseudocode|   Y = C-D
# |pseudocode|   Z = point_to_boundary_distance(X,Y)
# |pseudocode|   write out X,Y,Z


def analyzeRefBiasStats(mVarFn,outFn):
    out = open(outFn,"w")
    mVar = DelimitedFile(mVarFn)

    mVar.addField("zygosity")
    mVar.addField("varType")
    mVar.addField("allele1VarFilter")
    mVar.addField("allele2VarFilter")
    mVar.addField("allele1XRef")
    mVar.addField("allele2XRef")
    mVar.addField("allele1ReadCount")
    mVar.addField("allele2ReadCount")
    mVar.addField("repeatMasker")

    rptRefBias = 0
    rptAltBias = 0
    nonrptRefBias = 0
    nonrptAltBias = 0

    for zyg,type,vq1,vq2,xr1,xr2,rc1,rc2,rm in mVar:
        if zyg != "het-ref" or type != "snp" or vq1 != "PASS" or vq2 != "PASS" or xr1 != "" or xr2 != "":
            continue
        readcount1 = int(rc1)
        readcount2 = int(rc2)
        if (readcount1 + readcount2 <= 20):
            continue
        altfrac = readcount1 / float(readcount1+readcount2)
        if altfrac >= .25 and altfrac <= .75:
            continue
        if  rm != "":
            if altfrac < .5:
                rptRefBias += 1
            else:
                rptAltBias += 1
        else:
            if altfrac < .5:
                nonrptRefBias += 1
            else:
                nonrptAltBias += 1


    X = rptRefBias-rptAltBias
    Y = nonrptRefBias - nonrptAltBias

    Z = distance_to_boundary(X,Y)

    print >> out, "freeform,novelSnpBiasStats"
    print >> out, "repeatExcessRefBias,%d" % X
    print >> out, "nonrepeatExcessRefBias,%d" % Y
    print >> out, "excessRefBiasBoundaryDistance,%.0f" % Z



def point_distance_to_line(X,Y,a,b):
    '''for line y=ax+b and point (X,Y), return the shortest distance from point to line'''

    return abs(a*X-Y+b)/math.sqrt(a*a+1)



def distance_to_boundary(X,Y):

    # "fail" boundary is to the right of X = 8000 and below Y = X/4
    maxFreeX = 8000
    minFreeSlope = .25


    ### if X < maxFreeX, it is a "pass"
    if X < maxFreeX:
        # if it is below the corner of the boundary, distance is to vertical portion of boundary
        if Y < maxFreeX * minFreeSlope:
            return X-maxFreeX
        else:
            # if it is below the line perpendicular to Y = X*minFreeSlope and intersecting the corner of the boundary,
            # then it is the distance to the corner
            if Y < maxFreeX*minFreeSlope + (maxFreeX-X)/minFreeSlope:
                return -math.sqrt((maxFreeX-X)*(maxFreeX-X) + (Y-maxFreeX*minFreeSlope)*(Y-maxFreeX*minFreeSlope))
            else:
                # if we get here, then we need the distance to the line y = minFreeSlope * x
                distanceToLine = point_distance_to_line(X,Y,minFreeSlope,0)
                # but we want to report it as negative, since we are above the boundary
                return -distanceToLine

    ### if Y > X*minFreeSlope, then it is a "pass" and we want the distance to the Y=X*minFreeSlope line
    if Y >= X*minFreeSlope:
        distanceToLine = point_distance_to_line(X,Y,minFreeSlope,0)
        return -distanceToLine

    #### if we are here, then we are on or inside the boundary, i.e. "fail", should return a positive value
    #### we want the lesser of the distance to X=maxFreeX and the distance to Y=maxFreeX*minFreeSlope
    distanceToVertical = X-maxFreeX
    distanceToSloped = point_distance_to_line(X,Y,minFreeSlope,0)
    return min(distanceToVertical,distanceToSloped)


def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(masterVar='',outFile='',test='',gridtest='')
    parser.add_option('--masterVar',dest='masterVar',
                      help='masterVar file to be analyzed')
    parser.add_option('--outfile',dest='outFile',
                      help='output file name')
    parser.add_option('--test',dest='test',
                      help='specify X,Y to be tested')
    parser.add_option('--grid-test',dest='gridtest',
                      help='specify X,Y to be tested')
    
    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if options.gridtest:
        o = open(options.gridtest,"w")
        x = 6000
        while x < 12000:
            y = 0
            while y < 4500:
                print >> o, str(x) + " " + str(y) + " " + str(distance_to_boundary(x,y))
                y += 25
            x += 25
        return

    if options.test:
        X = float(options.test.split(",")[0])
        Y = float(options.test.split(",")[1])
        print options.test + " gives " + str(distance_to_boundary(X,Y))
        return

    if not options.masterVar:
        parser.error('masterVar specification required')
    if not options.outFile:
        parser.error('outfile specification required')
    
    analyzeRefBiasStats(options.masterVar,options.outFile)


if __name__ == '__main__':
    main()

