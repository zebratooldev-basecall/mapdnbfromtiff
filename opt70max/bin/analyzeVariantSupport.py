#!/usr/bin/env python

'''
This program takes takes as input a join of a file
containing small variant loci of interest and CG Evidence
DNB files.

Output is a summary which positions in the DNBs overlapped
the small variant calls, calculated by number of DNBs and number
of variant alleles and split by whether the DNB supported the 
ref or alt allele.  Other output files summarize the
base composition of DNBs overlapping the variants.

This code should handle any read structure but has only 
been tested on ad2_28_28 and ad2_29_29.
'''

import sys
from optparse import OptionParser
import re
import operator
import logging

def initialize_logging(outbase,loglevel):
    if loglevel == 'DEBUG':
        logging.basicConfig(filename='%s_%s.log'%(outbase,loglevel),level=logging.DEBUG)
    elif loglevel == 'INFO':
        logging.basicConfig(filename='%s_%s.log'%(outbase,loglevel),level=logging.INFO)
    else:
        logging.basicConfig(filename='%s_%s.log'%(outbase,loglevel),level=logging.WARNING)

def initialize_containers():
    logging.info('initializaing containers')
    global support_posns_dnb ## support_posns_dnb[support_type][posn] = count of dnbs
    global support_posns_vars ## support_posns_vars[support_type][dnb posn] = count of alleles
    global base_comp_vars  ## base_comp_vars[support_type][posn][base] = count of variant supporting positions
    global base_comp_dnb  ## base_comp_dnb[support_type][dnb_posn][base] = count of dnbs
    global miscall_matrix ## miscall_matrix[position][refbase][calledbase] = count
    global correlated_basecalls # correlated_basecalls[support_type][varposn][compare_posn] = [yes-count,no-count]
    global stack_stats # stack_stats[support_type][side][pct stacked dnbs] = count of variants

    support_posns_dnb = {'alt': {}, 'ref': {}, 'ambig': {} }
    support_posns_vars = { 'alt': {}, 'ref': {} }
    base_comp_vars = { 'alt': {}, 'ref': {}, 'ambig': {} }
    base_comp_dnb = { 'alt': {}, 'ref': {}, 'ambig': {} }
    miscall_matrix = {}
    correlated_basecalls = { 'alt': {}, 'ref': {}, 'ambig': {} }
    stack_stats  = { 'alt': {'L': {},
                             'R': {} }, 
                     'ref': {'L': {},
                             'R': {} } }

def comp(seq):
    compdict = {'A':'T', 'T':'A','C':'G','G':'C','N':'N'}
    compseq = [compdict[i] for i in seq]
    compseq = ''.join(compseq)

    return compseq

def get_armsequence(dnbseq,side,alleleAlignment,strand):
    # parse cigar string to get arm length
    armlength = 0
    foundmods = re.findall('[0-9]+[MNBIPD]',alleleAlignment) ## should return a list of strings that match pattern
    for foundmod in foundmods:
        bases = int(foundmod[:-1])
        mod = foundmod[-1]
        if mod != 'B':
            armlength += bases

    # use side to pick which end of dnb seq to take
    if side == 'L':
        armseq = dnbseq[:armlength]
    elif side == 'R':
        armseq = dnbseq[-armlength:]
    
    logging.debug('starting untrimmed arm sequence: %s' %armseq)

    # go through cigar string one more time to remove gaps
    # keep track of which raw posns get converted to which trimmed posns
    foundmods = re.findall('[0-9]+[MNBIPD]',alleleAlignment) 
    if strand == '-':
        foundmods.reverse()
    oldstart = 0
    armseq_trimmed = ''
    skipme = 0
    rawposn = -1; trimposn = -1
    rawposns = []; trimmedposns = []
    for foundmod in foundmods:
        bases = int(foundmod[:-1])
        mod = foundmod[-1]
        if mod == 'M' or mod == 'I':
            trimstart = oldstart 
            newend = oldstart + bases - skipme
            armseq_trimmed += armseq[trimstart:newend]
            oldstart = newend
            
            # add positions
            trimposn -= skipme
            for i in xrange(0,bases):
                rawposn += 1
                trimposn += 1
                rawposns.append(rawposn)
                trimmedposns.append(trimposn)
            
            skipme = 0

        elif mod == 'B': ## time to resolve an overlap
            priorbases = armseq_trimmed[-bases:]
            afterbases = armseq[oldstart:oldstart+bases]
            usebases = ''
            for i in xrange(0,len(priorbases)):
                if priorbases[i] == 'N':
                    usebases += afterbases[i]
                elif priorbases[i] == afterbases[i]:
                    usebases += afterbases[i]
                elif afterbases[i] == 'N':
                    usebases += priorbases[i]
                else:
                    usebases += 'N'
            armseq_trimmed = armseq_trimmed[:-bases] + usebases
            skipme = bases
            
            oldstart += bases
    
    # convert two lists to posnTranslate dictionary 
    logging.debug('list of trimmed positions in arm (after gaps resolved): %s' %trimmedposns)
    logging.debug('list of raw arm positions: %s'% rawposns)
    posnTranslate = {} ## posnTranslate[trimmed_posn] = [untrimmed_posn]
    # check that trimmedposns and rawposns are same length
    if len(trimmedposns) == len(rawposns):
        for i in xrange(0,len(rawposns)):
            trimposn = trimmedposns[i]
            rawposn = rawposns[i]
            posnTranslate[trimposn] = posnTranslate.setdefault(trimposn,[])
            posnTranslate[trimposn].append(rawposn)
    else:
        sys.exit('position lists are not the same length!')

    # reverse but do not comp arm sequences if on - strand
    if strand == '-':
        armseq_trimmed = armseq_trimmed[::-1]
    
    return armseq_trimmed, armseq, posnTranslate

def check_dnballele(strand,dnballeleref,dnballelealt,refallele,altallele,refalignment):
    # vcf files can contain > 1 alt allele.  In that case, call it ambiguous
    logging.debug('alt allele:  %s, ref allele:  %s, alt dnb:  %s, ref dnb:  %s' % \
    (altallele,refallele,dnballelealt,dnballeleref))
    logging.debug('strand:  %s, refalignment:  %s' % (strand, refalignment))
    altallele = altallele.split(',')
    if len(altallele) == 1:
        altallele = altallele[0]
        
        ref = False
        alt = False

        # comp bases if on minus strand to compare to called alleles
        if strand == '-':
            logging.debug('strand == -.  comping')
            logging.debug('dnballeleref before comp:  %s'%(dnballeleref,))
            dnballeleref = comp(dnballeleref)
            dnballelealt = comp(dnballelealt)
            logging.debug('dnballeleref after comp:  %s'%(dnballeleref,))
        
        if refallele == dnballeleref:
            logging.debug("ref = True")
            ref = True
        if altallele == dnballelealt:
            logging.debug('alt = True')
            alt = True

        # in case of length-altering vars, short dnb allele could
        # match ref.  check cigar string to see how entire dnb aligned
        if 'I' in refalignment or 'D' in refalignment:
            logging.debug('I or D in refalignment.  setting ref to False')
            ref = False

        # assign support type
        if ref and not alt:
            logging.debug('ref and not alt found')
            supporttype = 'ref'
        elif alt and not ref:
            logging.debug('alt and not ref found')
            supporttype = 'alt'
        else:
            supporttype = 'ambig'
    else:
        supporttype = 'ambig'

    return supporttype

def calc_miscall_matrix(varposns, dnballeleref, refallele, strand):
    ## miscall_matrix[position][refbase][calledbase] = count
    if len(dnballeleref) != len(refallele):
        logging.warning('dnb allele is of different length from ref allele!  cannot calculate miscall matrix')
    
    else:
        # if on - strand, comp ref allele
        if strand == '-':
            refallele = comp(refallele)
        # in case of overlap, will have more variant positions than allele bases
        dup_count = len(varposns) / len(refallele) + 1
        dnballeleref = dnballeleref * dup_count
        refallele = refallele * dup_count
        varposns.sort()

        logging.debug('varposns:  %s, dnballele:  %s, refallele:  %s' % (varposns,dnballeleref, refallele))
    
        # go through positions in sorted order
        for i in xrange(len(varposns)):
            posn = varposns[i]
            calledbase = dnballeleref[i]
            refbase = refallele[i]
            if calledbase != 'N' and refbase != 'N':
                miscall_matrix[posn] = miscall_matrix.setdefault(posn,{})
                miscall_matrix[posn][refbase] = miscall_matrix[posn].setdefault(refbase,{})
                miscall_matrix[posn][refbase][calledbase] = miscall_matrix[posn][refbase].get(calledbase,0) + 1

                logging.debug('varposn:  %s, refbase:  %s, calledbase:  %s' % (posn,refbase,calledbase))


def calc_correlated_basecalls(varposns,dnbseq,support_type):
    # correlated_basecalls[support_type][varposn][compare_posn] = [yes-count,no-count]
    usedict = correlated_basecalls[support_type]
    for varposn in varposns:
        querybase = dnbseq[varposn]
        usedict[varposn] = usedict.setdefault(varposn,{})
        for compposn in xrange(len(dnbseq)):
            compbase = dnbseq[compposn]
            usedict[varposn][compposn] = usedict[varposn].setdefault(compposn,[0,0])
            if querybase != 'N' and compbase != 'N':
                if querybase == compbase:
                    usedict[varposn][compposn][0] += 1
                else:
                    usedict[varposn][compposn][1] += 1

def parse_infile(infile):
    allele_counts = {} ## allele_counts[dnboffset][support_type][posn] = count
    varlengths = {} ## varlengths[varoffset] = len(altallele)
    reflengths = {} ## reflenghts[varoffset] = len(refallele)
    stacking = {} ## stacking[varoffset][support_type][(side,strand)][armoffset] = count

    fi = open(infile)
    try:
        for line in fi:
            # skip headers and emtpy lines
            if line[0] == '#':  continue
            if line== '\n':  continue
            if line[0] == '>':  continue
            
            #  get the variables we want
            line = line.rstrip().split('\t')
            varoffset, refallele, altallele = int(line[1]),line[2],line[3]
            side, strand, dnboffset, dnbseq = line[11], line[12], int(line[15]), line[24]
            refAlignment, alleleAlignment = line[16], line[14]
            
            logging.debug('dnbseq:  %s, arm:  %s' % (dnbseq,side))
            logging.debug('varoffset:  %s, dnboffset:  %s, refAlignment: %s' % (varoffset, dnboffset, refAlignment))
            logging.debug('refallele:  %s, altallele:  %s' % (refallele,altallele))

            # store length of this variant for later calculations
            varlengths[varoffset] = len(altallele)
            reflengths[varoffset] = len(refallele)

            # pick proper arm sequence from full DNB.  
            # this part is not strand specific
            # resolve gaps
            armseq, armseq_untrimmed, posnTranslate = get_armsequence(dnbseq,side,alleleAlignment,strand)
            logging.debug('armseq:  %s, armseq_untrimmed:  %s' % (armseq, armseq_untrimmed))
            
            
            # Get DNB bases supporting variant
            # Get ref and alt bases separately in case they are
            # length changing variants
            if varoffset > dnboffset:
                varstart = varoffset - dnboffset -1
            else:
                varstart = varoffset - dnboffset
            varendref = varstart + len(refallele)
            varendalt = varstart + len(altallele)
            dnballeleref = armseq[varstart:varendref]
            dnballelealt = armseq[varstart:varendalt]
            # long insertions can give crazy alleles that run off dnb.
            if varstart < 0:
                dnballeleref = ''
                dnballelealt = ''
            logging.debug('varstart:  %s, varendref:  %s, varendalt:  %s' % (varstart,varendref, varendalt))
            logging.debug('dnballele ref:  %s, dnballele alt:  %s' % (dnballeleref, dnballelealt))

            # determine if DNB supports REF or ALT allele
            support_type = check_dnballele(strand,dnballeleref,dnballelealt,refallele,altallele,refAlignment)

            # pick varend based on support type
            if support_type == 'ref' or support_type == 'ambig':
                varend = varendref
            elif support_type == 'alt':
                varend = varendalt

            # first get positions of gap-resolved sequence
            # get DNB positions supporting allele in standard dnb order
            # trim variant positions if extend beyond bounds of DNB
            logging.debug('varstart before:  %s, varend before:  %s' % (varstart, varend))
            if varstart < 0:  varstart = 0
            if varend < 0:  varend = 0
            if varend > len(armseq) :  varend = len(armseq) 
            
            # if strand == '-', need to flip position
            if strand == '-':
                oldstart = varstart; oldend = varend
                varstart = len(armseq) - oldend 
                varend = len(armseq) - oldstart 

            logging.debug('varstart flipped:  %s, varend flipped:  %s' % (varstart, varend))
            
            # convert gap-resolved dnb positions to original dnb positions
            varposns_trimmed = xrange(varstart,varend)
            varposns = [posnTranslate[i] for i in varposns_trimmed]
            varposns = [ i for sublist in varposns for i in sublist]
            logging.debug('varposns before arm considered:  %s' % (varposns,))
            
            if side == 'R':
                varposns = [ operator.add(i,len(armseq_untrimmed)) for i in varposns]
            logging.debug('varposns after arm considered:  %s' % (varposns,))
            
            # save stacking information for each file
            ## stacking[varoffset][support_type][(side,strand)][armoffset] = count
            if support_type != 'ambig':
                armoffset = dnbseq.find(armseq_untrimmed)
                if armoffset >=0:
                    stacking[varoffset] = stacking.setdefault(varoffset,{})
                    stacking[varoffset][support_type] = stacking[varoffset].setdefault(support_type,{})
                    stacking[varoffset][support_type][(side,strand)] = stacking[varoffset][support_type].setdefault((side,strand),{})
                    stacking[varoffset][support_type][(side,strand)][armoffset] \
                    = stacking[varoffset][support_type][(side,strand)].get(armoffset,0) + 1
                
            # calculate miscall matrix for each position
            # only calculate for length-conserving variants and dnbs with alt support
            if len(refallele) == len(altallele) and support_type == 'alt':
                calc_miscall_matrix(varposns,dnballeleref, refallele, strand)
            
            # correlated basecalls by allele support
            calc_correlated_basecalls(varposns,dnbseq,support_type)
            
            # base composition of dnbs broken down by allele support
            for i in xrange(0,len(dnbseq)):
                dnbbase = dnbseq[i]
                if dnbbase != 'N':
                    dnbpos = i
                    base_comp_dnb[support_type][dnbpos] = base_comp_dnb[support_type].setdefault(dnbpos,{})
                    base_comp_dnb[support_type][dnbpos][dnbbase] = base_comp_dnb[support_type][dnbpos].get(dnbbase,0) + 1
            
            # variant position support and base comp
            for posn in varposns:
                # skip positions where variant runs off end of dnb
                if posn < 0 or posn > (len(dnbseq) - 1):  continue

                # base comp at variant positions
                base=dnbseq[posn]
                if base != 'N':
                    base_comp_vars[support_type][posn] = base_comp_vars[support_type].setdefault(posn,{})
                    base_comp_vars[support_type][posn][base] = base_comp_vars[support_type][posn].get(base,0) + 1

                # variant position support
                support_posns_dnb[support_type][posn] = support_posns_dnb[support_type].get(posn,0) + 1
                
                # keep track of dnb support for each variant
                if support_type != 'ambig':
                    allele_counts[varoffset] = allele_counts.setdefault(varoffset,{})
                    allele_counts[varoffset][support_type] = allele_counts[varoffset].setdefault(support_type,{})
                    allele_counts[varoffset][support_type][posn] = allele_counts[varoffset][support_type].get(posn,0) + 1
    finally:
        fi.close()
    
    # calculate per-allele support
    calc_per_allele(allele_counts,varlengths,reflengths)

    # calculate stacking metric
    calc_stacking(stacking)

def calc_stacking(stacking):
    # stack_stats[support_type][side][pct stacked dnbs] = count of variants
    ## stacking[varoffset][support_type][(side,strand)][armoffset] = count
    for varoffset, supports in stacking.iteritems():
        for support_type, arms in supports.iteritems():
            total_dnbs = 0
            max_count = 0
            max_dnb_info = ''
            for dnb_info, arm_info in arms.iteritems():
                for armoffset, count in arm_info.iteritems():
                    total_dnbs += count
                    if count > max_count:
                        max_count = count
                        max_dnb_info = dnb_info
            stack_pct = float(max_count) / float(total_dnbs) * 100
            stack_pct = int(stack_pct)
            stack_dir = stack_stats[support_type][max_dnb_info[0]]
            stack_dir[stack_pct] = stack_dir.get(stack_pct,0) + 1

def calc_per_allele(allele_counts,varlengths,reflengths):
    ## support_posns_vars[support_type][dnb posn] = count of alleles
    totalvars = len(allele_counts)
    noaltvars = 0
    for varoffset, supports in allele_counts.iteritems():
        if 'alt' not in supports:
            noaltvars += 1
        for support_type, posns in supports.iteritems():
            if support_type == 'alt':
                varlength = varlengths[varoffset]
            elif support_type == 'ref':
                varlength = reflengths[varoffset]
            total_dnbs = sum(posns.values()) / float(varlength)
            for posn, dnb_count in posns.iteritems():
                allele_count = float(dnb_count) / float(total_dnbs)
                support_posns_vars[support_type][posn] = support_posns_vars[support_type].get(posn,0) + allele_count
    
    # sanity check of how many variant alleles have no perfect support (should be low)
    logging.info('%s total variants.  %s had no DNBs perfectly matching alt allele' % (totalvars,noaltvars))
    
def write_stacking(outbase):
    outfile = outbase + '_dnb_stacking.tsv'

    support_types = ['alt','ref']
    sides = ['L','R']

    fo = open(outfile,'w')
    try:
        fo.write('support_type\tside\tstacking_percent\tvariant_count\n')
        for support_type in support_types:
            side_info = stack_stats.get(support_type,{})
            for side in sides:
                stack_info = side_info.get(side,{})
                for pct in xrange(0,101):
                    varcount = stack_info.get(pct,0)
                    output = [support_type,side,pct,varcount]
                    fo.write('\t'.join(map(str,output)) + '\n')
    finally:
        fo.close()

def write_miscall_matrix(outbase):
    ## miscall_matrix[position][refbase][calledbase] = count
    outfile = outbase + '_miscall-matrix.tsv'
    bases = ['A','T','G','C']
    fo = open(outfile,'w')
    try:
        fo.write('position\treference_base\tcalled_base\tdnb_count\n')
        for position, baseinfo in miscall_matrix.iteritems():
            for refbase in bases:
                for calledbase in bases:
                    baseinfo[refbase] = baseinfo.setdefault(refbase,{})
                    count = baseinfo[refbase].get(calledbase,0)
                    output = [position,refbase,calledbase,count]
                    fo.write('\t'.join(map(str,output)) + '\n')
    finally:
        fo.close()

def write_correlated_basecalls(outbase):
    # correlated_basecalls[support_type][varposn][compare_posn] = [yes-count,no-count]
    outfile = outbase + '_correlated-basecalls.tsv'
    fo = open(outfile,'w')
    try:
        fo.write('support_type\tquery_variant_position\tcompare_dnb_position\tcorrelated_base_call_rate\n')
        for support_type, varposns in correlated_basecalls.iteritems():
            for varposn, compposns in varposns.iteritems():
                for compposn, counts in compposns.iteritems():
                    total_counts = sum(counts)
                    if total_counts != 0:
                        correlated_rate = float(counts[0]) / total_counts
                        output = [support_type, varposn, compposn, correlated_rate]
                        fo.write('\t'.join(map(str,output)) + '\n')
    finally:
        fo.close()

def write_base_comp(outbase, base_dict, count_type):
    outfile = '%s_%s_base-composition.tsv' % (outbase, count_type)

    fo = open(outfile,'w')
    try:
        fo.write('support_type\tdnb_position\tbase\tbase_count\tbase_fraction\n')
        for support_type, posns in base_dict.iteritems():
            if len(posns) == 0:
                print >> sys.stderr, 'No base composition information for %s' % (support_type,)
            else:
                for posn in xrange(min(posns),max(posns)+1):
                    bases = posns.get(posn,{})
                    totaldnbs = sum(bases.values())
                    for base, count in bases.iteritems():
                        dnbfrac = float(count) / float(totaldnbs)
                        output = [support_type,posn,base,count,dnbfrac]
                        fo.write('\t'.join(map(str,output)) + '\n')
    finally:
        fo.close()
            
def write_support_summary(outbase, support_dict, count_type):
    outfile = '%s_%s-support.tsv'% (outbase,count_type)

    fo = open(outfile,'w')
    try:
        fo.write('support_type\tposition\tcount\n')
        for support_type,posns in support_dict.iteritems():
            if len(posns) == 0:
                print >> sys.stderr, 'No %s support information for %s' % (count_type,support_type)
            else:
                for n in xrange(min(posns.keys()),max(posns.keys())+1):
                    count = posns.get(n,0)
                    fo.write('%s\t%s\t%s\n' % (support_type,n,count))
    finally:
        fo.close()

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-i','--input-files', help='comma-separated list of VCF-evidence joined tsv files', dest='infile')
    parser.add_option('-o','--output-base', help='base name for output file', dest='outbase')
    parser.add_option('-l','--logging-level', help='info level for logging output', dest='loglevel',default='INFO')

    # parse command line arguments.
    options,args = parser.parse_args(arguments[1:])
    loglevel=options.loglevel.upper()

    return options.infile, options.outbase, loglevel

def main(args):
    infiles, outbase, loglevel = parse_arguments(args)
    infiles = infiles.split(',')

    initialize_logging(outbase,loglevel)
    initialize_containers()
    
    for infile in infiles:
        parse_infile(infile)

    write_support_summary(outbase, support_posns_dnb, 'dnb')
    write_support_summary(outbase, support_posns_vars, 'allele')
    write_base_comp(outbase,base_comp_vars,'var-supporting')
    write_base_comp(outbase,base_comp_dnb,'full-dnb')
    write_miscall_matrix(outbase)
    write_correlated_basecalls(outbase)
    write_stacking(outbase)

if __name__=='__main__':
    main(sys.argv)
