#! /usr/bin/env Rscript
options(warn=1);

args <- commandArgs(TRUE);
cmd_args <- commandArgs();

input_dir <- cmd_args[6]
output_file <- cmd_args[7]
pdf_file <- cmd_args[8]
AggFile <-  cmd_args[9]

StatsAndPlots <- function( on_target, output_file, type ){
  mean_cov <- sum(as.numeric( on_target$ValCount )) / sum(as.numeric( on_target$Count ))
  on_target$CumSum <- cumsum(as.numeric(on_target$Count))
  median_cov <- min( on_target$Value[ on_target$CumSum> max(on_target$CumSum)/2 ] )
  cat(paste("Mean coverage from ",   type, "\t", mean_cov,   "\n", sep=""), file=output_file, append=T)
  cat(paste("Median coverage from ", type, "\t", median_cov, "\n", sep=""), file=output_file, append=T)

  on_target$RevCumSum[nrow(on_target):1] <- cumsum(as.numeric(rev(on_target$Count)))
  plot( on_target$Value, on_target$Count/sum(on_target$Count)*100, xlab="Coverage", ylab="Percent of exome covered", main=type )
  AlmostMax <- quantile(on_target$Count,  0.99)
  plot(on_target$Value, on_target$RevCumSum/max(on_target$RevCumSum)*100, xlab="Minumum sequencing depth considered", ylab="Percent of exome covered", main=type)
  abline( v=1,  col="gray")
  abline( v=10,  col="gray")
  abline( v=40,  col="gray")
  abline( v=100,  col="gray")
  abline( v=max(on_target$Value[ on_target$RevCumSum/max(on_target$RevCumSum) >= 0.5 ]),  col="gray")
  abline( h=50, col="gray" )
  abline( h=on_target$RevCumSum[on_target$Value==1]/max(on_target$RevCumSum)*100, col="gray" )
  abline( h=on_target$RevCumSum[on_target$Value==10]/max(on_target$RevCumSum)*100, col="gray" )
  abline( h=on_target$RevCumSum[on_target$Value==40]/max(on_target$RevCumSum)*100, col="gray" )
  abline( h=on_target$RevCumSum[on_target$Value==100]/max(on_target$RevCumSum)*100, col="gray" )
  cat(paste("Percent of the target region covered at >=1x   from ", type, "\t", on_target$RevCumSum[on_target$Value==1  ]/max(on_target$RevCumSum)*100, "\n", sep=""), file=output_file, append=T)
  cat(paste("Percent of the target region covered at >=10x  from ", type, "\t", on_target$RevCumSum[on_target$Value==10 ]/max(on_target$RevCumSum)*100, "\n", sep=""), file=output_file, append=T)
  cat(paste("Percent of the target region covered at >=15x  from ", type, "\t", on_target$RevCumSum[on_target$Value==15 ]/max(on_target$RevCumSum)*100, "\n", sep=""), file=output_file, append=T)
  cat(paste("Percent of the target region covered at >=30x  from ", type, "\t", on_target$RevCumSum[on_target$Value==30 ]/max(on_target$RevCumSum)*100, "\n", sep=""), file=output_file, append=T)
  cat(paste("Percent of the target region covered at >=40x  from ", type, "\t", on_target$RevCumSum[on_target$Value==40 ]/max(on_target$RevCumSum)*100, "\n", sep=""), file=output_file, append=T)
  cat(paste("Percent of the target region covered at >=100x from ", type, "\t", on_target$RevCumSum[on_target$Value==100]/max(on_target$RevCumSum)*100, "\n", sep=""), file=output_file, append=T)
}

LookAtCoverage <- function( info, output_file ){
  ### Capture rate
  info$ValCount <- as.numeric(info$Value) * as.numeric(info$Count)

  sum_uniq_in <- sum(as.numeric(info$ValCount[ info$Metric=="uniqueSequenceCoverage" & info$Location=="in" ] ))
  sum_uniq_out <- sum(as.numeric(info$ValCount[ info$Metric=="uniqueSequenceCoverage" & info$Location=="out" ] ))
  cap_rate_uniq <- sum_uniq_in / ( sum_uniq_in + sum_uniq_out )
  cat(paste("On-target capture rate, unique mappings\t", cap_rate_uniq, "\n", sep=""), file=output_file, append=T)

  sum_wt_in <- sum(as.numeric(info$ValCount[ info$Metric=="weightSumSequenceCoverage" & info$Location=="in" ] ))
  sum_wt_out <- sum(as.numeric(info$ValCount[ info$Metric=="weightSumSequenceCoverage" & info$Location=="out" ] ))
  cap_rate_wt <- sum_wt_in / ( sum_wt_in + sum_wt_out )
  cat(paste("On-target capture rate, unique+multiple mappings\t", cap_rate_wt, "\n", sep=""), file=output_file, append=T)

  ### Coverage plots
  uin <- info[info$Metric=="uniqueSequenceCoverage" & info$Location=="in", ]
  StatsAndPlots(uin, output_file, "unique mappings")

  nuin <- info[info$Metric=="weightSumSequenceCoverage" & info$Location=="in", ]
  StatsAndPlots(nuin, output_file, "unique + multiple mappings")
}

Truncate <- function( info, thresh ){
  for( loc in unique(info$Location) ){
    for( met in unique(info$Metric) ){
	count <- sum( info$Count[ info$Location==loc & info$Metric==met & info$Value>=thresh ])
        if( count > 0  ){
  	  info <- info[ -which( info$Location==loc & info$Metric==met & info$Value>thresh)  , ]
	}
	info$Count[ info$Location==loc & info$Metric==met & info$Value==thresh ] <- count
    }
  }
  return( info )
}


### Load and merge data
files <- paste(input_dir, list.files(input_dir, "stats"), sep="/" )
b <- read.table(files[1], stringsAsFactors=F, header=T)
for( i in 2:length(files) ){
  a <- read.table(files[i], stringsAsFactors=F, header=T)
  b <- rbind(b, a)
}
info <- aggregate(b$Count, by=list(Location=b$Location, Metric=b$Metric, Value=b$Value), FUN = function(x) sum(as.numeric(x)) ) 
colnames(info)[4] <- "Count"
cat("Input files\t",files, "\n", file=output_file)
write.table(info, file=AggFile, sep="\t", quote=F, row.names=F, col.names=T)

pdf(pdf_file)
info <- Truncate(info, 1000)
LookAtCoverage(info, output_file)
dev.off()
