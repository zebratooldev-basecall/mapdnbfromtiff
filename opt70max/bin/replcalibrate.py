#! /usr/bin/env python

import datetime,math,os,sys
from os.path import join as pjoin
from subprocess import Popen, PIPE, call
from bisect import bisect

SOFTWARE_VERSION = '2.0.0'
VT = [ 'snp', 'ins', 'del', 'sub' ]
now = datetime.datetime.now()

class CvgBinner(object):
    def __init__(self, minCvgA):
        self.minCvgA = minCvgA

    def cvg2bin(self, cvg):
        for ii in xrange(len(self.minCvgA)):
            if self.minCvgA[ii] > cvg:
                return ii-1
        return len(self.minCvgA)-1

    def bin2mincvg(self, bin):
        return self.minCvgA[bin]

    def bincount(self):
        return len(self.minCvgA)

rsmooth = '''
id1 <- %d
id2 <- %d
d <- read.csv("%sfoo.csv")
ll <- loess(d$phred ~ d$score, sp=20.0/length(d$score), degree=1)
lo <- ceiling(d$score[1])
hi <- floor(d$score[length(d$score)])
sc <- id1:id2
# Use loess prediction from the fitted range, extending the end points.
ph <- rep(ll$fitted[length(ll$fitted)], length(sc))
ph[1:(lo-id1)] <- ll$fitted[1]
ph[(lo-id1+1):(hi-id1+1)] <- predict(ll, lo:hi)
out <- data.frame(scores=sc, smoothed=ph)
write.csv(out, "%sfoo-smoothed.csv", quote=FALSE, row.names=FALSE)
'''

def smoothOneBin(binned, minScore, maxScore):
    (sc,ph) = binned[0]
    smoothed = []
    for sc in xrange(minScore, maxScore+1):
        smoothed.append(ph)
    return smoothed


def smoothBins(binned, minScore, maxScore):
    if 0 == len(binned):
        raise Exception('no bins to smooth')
    if 1 == len(binned):
        return smoothOneBin(binned, minScore, maxScore)

    global tmppath
    ff = open(pjoin(tmppath,'foo.R'), 'w')
    ff.write(rsmooth % (minScore,maxScore,pjoin(tmppath,''),pjoin(tmppath,'')))
    ff.close()

    ff = open(pjoin(tmppath,'foo.csv'), 'w')
    print >>ff, 'score,phred'
    for (sc,ph) in binned:
        print >>ff, '%.2f,%.2f' % (sc,ph)
    ff.close()

    sts = call(['R', 'CMD', 'BATCH', pjoin(tmppath,'foo.R')])
    if sts != 0:
        raise Exception('R failed')

    ff = open(pjoin(tmppath,'foo-smoothed.csv'))
    header = ff.readline()
    if header.rstrip() != 'scores,smoothed':
        raise Exception('bad header in foo-smoothed.csv')
    miny = min([b[1] for b in binned])
    maxy = max([b[1] for b in binned])
    smoothed = []
    for line in ff:
        fields = line.rstrip().split(',')
        if fields[1] != 'NA':
            smoothed.append(float(fields[1]))
            smoothed[-1] = min(smoothed[-1], maxy)
            smoothed[-1] = max(smoothed[-1], miny)
        else:
            # If R returns NA, something went wrong. Typically, this
            # happens if len(binned) is 1. This case is already fixed
            # now at the beginning of this function. As a sanity
            # check, replace NA with our best effort. Should we
            # eventually throw an exception for NA?
            if len(smoothed) > 0:
                smoothed.append(smoothed[-1])
            else:
                smoothed.append(0)
    ff.close()

    return smoothed


# Class to describe a single observation of heterozygous or homozygous
# call. Actually, an object of this class may describe several
# observations (denoted by the "count" field).
class Observation(object):
    def __init__(self, scoreHetA, scoreHomB, cvgHetA, cvgHomB, dbsnp,
                 count = 1, locusA=None):
        self.scoreHetA = scoreHetA
        self.scoreHomB = scoreHomB
        self.cvgHetA = cvgHetA
        self.cvgHomB = cvgHomB
        self.dbsnp = dbsnp
        self.count = count
        self.PHet = None
        self.locusA = locusA

    def __str__(self):
        return '%s,%s,%s,%s,%s,%s,%s,%s' % ( self.scoreHetA,
                                             self.scoreHomB,
                                             self.cvgHetA,
                                             self.cvgHomB,
                                             self.dbsnp,
                                             self.count,
                                             self.PHet,
                                             self.locusA
                                             )

class Observations(object):
    def __init__(self):
        self.refAltVaf = []
        self.homAltVaf = []
        self.refAltEaf = []
        self.homAltEaf = []


class CalibratedScores(object):
    def __init__(self, binned, smoothed, minScore, maxScore):
        self.binned = binned
        self.smoothed = smoothed
        self.minScore = minScore
        self.maxScore = maxScore
        if maxScore-minScore+1 != len(smoothed):
            raise Exception('bad input')

    def getSmoothed(self, score):
        if score < self.minScore:
            return self.smoothed[0]
        if score > self.maxScore:
            return self.smoothed[-1]
        return self.smoothed[score-self.minScore]

    def getErrorRate(self, score):
        smoothed = self.getSmoothed(score)
        fOverT = math.pow(10.0, -0.1*smoothed)
        return fOverT / (1.0+fOverT)

    def applyCorrection(self, correction):
        smoothed = []
        for ii in xrange(len(self.smoothed)):
            score = self.minScore + ii
            smoothed.append(self.smoothed[ii]+correction.getSmoothed(score))
        result = CalibratedScores([], smoothed, self.minScore, self.maxScore)
        return result


class CvgCalibratedScores(object):
    def __init__(self, cvgBinner, csa):
        self.cvgBinner = cvgBinner
        self.csa = csa

    def getSmoothed(self, cvg, score):
        bin = self.cvgBinner.cvg2bin(cvg)
        return self.csa[bin].getSmoothed(score)

    def getErrorRate(self, cvg, score):
        bin = self.cvgBinner.cvg2bin(cvg)
        return self.csa[bin].getErrorRate(score)

    def applyCorrection(self, correction):
        if self.cvgBinner.bincount() != correction.cvgBinner.bincount():
            raise Exception('bincount mismatch')
        csa = []
        for bin in xrange(self.cvgBinner.bincount()):
            csa.append(self.csa[bin].applyCorrection(correction.csa[bin]))
        return CvgCalibratedScores(self.cvgBinner, csa)


def calibrateFfTt(bins):
    MIN_BIN = 5
    rgbin = []
    ii = 0
    while ii < len(bins):
        sumf = bins[ii][1]
        sumt = bins[ii][2]
        jj = ii+1
        while jj < len(bins) and min(sumf,sumt) < MIN_BIN:
            sumf += bins[jj][1]
            sumt += bins[jj][2]
            jj += 1
        if min(sumf,sumt) < MIN_BIN and 0 != len(rgbin):
            rgbin[-1] = (rgbin[-1][0], jj)
        else:
            rgbin.append( (ii,jj) )
        ii = jj

    binned = []
    for (ii,jj) in rgbin:
        sumf = sum( [bin[1] for bin in bins[ii:jj]] )
        sumt = sum( [bin[2] for bin in bins[ii:jj]] )
        sc   = sum( [bin[0]*(bin[1]+bin[2]) for bin in bins[ii:jj]] ) / (sumf+sumt)
        if sumt > 0 and sumf > 0:
            rate = sumf / float(sumt)
            cscore = -10.0 * math.log10(rate)
        elif sumf > 0:
            cscore = -100.0
        else:
            cscore = 100.0
        binned.append( (sc, cscore) )

    smoothed = smoothBins(binned, bins[0][0], bins[-1][0])
    return CalibratedScores(binned,smoothed, bins[0][0], bins[-1][0])


def collapseBins(bins):
    if 0 == len(bins):
        return []
    result = [ bins[0] ]
    for bin in bins[1:]:
        if bin[0] == result[-1][0]:
            result[-1] = (bin[0], result[-1][1]+bin[1], result[-1][2]+bin[2])
        else:
            result.append(bin)
    return result


def makeAfCorrection(binner, pureDistro, mixedDistro):
    bins  = []
    for ii in xrange(binner.bincount()):
        bins.append([])
        bb = bins[ii]
        for score in pureDistro[ii]:
            bb.append( (score, float(pureDistro [ii][score]), 0.0) )
        for score in mixedDistro[ii]:
            bb.append( (score, 0.0, float(mixedDistro[ii][score])) )
        bb.sort()

    # Collapse bins.
    hetbins = [ collapseBins(bb) for bb in bins ]

    correction = CvgCalibratedScores(binner, [ calibrateFfTt(bb) for bb in bins ])

    return correction


def calibrateGivenPHet(obs, hetCvgBinner, homCvgBinner):
    hetbins = []
    hombins = []
    for ii in xrange(hetCvgBinner.bincount()):
        hetbins.append([])
    for ii in xrange(homCvgBinner.bincount()):
        hombins.append([])
    for ob in obs:
        if ob.scoreHetA != None and ob.PHet != None:
            bb = hetbins[hetCvgBinner.cvg2bin(ob.cvgHetA)]
            bb.append( (ob.scoreHetA, ob.count*(1.0-ob.PHet), ob.count*ob.PHet) )
        if ob.scoreHomB != None and ob.PHet != None:
            bb = hombins[homCvgBinner.cvg2bin(ob.cvgHomB)]
            bb.append( (ob.scoreHomB, ob.count*ob.PHet, ob.count*(1.0-ob.PHet)) )

    for bb in hetbins:
        bb.sort()
    for bb in hombins:
        bb.sort()

    # Collapse bins.
    hetbins = [ collapseBins(bb) for bb in hetbins ]
    hombins = [ collapseBins(bb) for bb in hombins ]

    het = CvgCalibratedScores(hetCvgBinner, [ calibrateFfTt(bb) for bb in hetbins ])
    hom = CvgCalibratedScores(homCvgBinner, [ calibrateFfTt(bb) for bb in hombins ])

    return ( het, hom )


def calibrateHetHom(rabins, hetCvgBinner, homCvgBinner):
    for bin in rabins:
        if bin.scoreHetA is None:
            bin.PHet = 0.0
        elif bin.scoreHomB is None:
            bin.PHet = 1.0
        else:
            bin.PHet = 0.5

    (het, hom) = calibrateGivenPHet(rabins, hetCvgBinner, homCvgBinner)

    for ii in xrange(3):
        (hetc,homc) = getTrueCounts(rabins, het, hom)

        for bin in rabins:
            if bin.scoreHetA is None:
                bin.PHet = 0.0
                continue
            if bin.scoreHomB is None:
                bin.PHet = 1.0
                continue
            PHetByHomScore = hom.getErrorRate(bin.cvgHomB, bin.scoreHomB)
            PHomByHetScore = het.getErrorRate(bin.cvgHetA, bin.scoreHetA)
            PHetToHom = PHetByHomScore / PHomByHetScore * homc / hetc
            bin.PHet = PHetToHom / (1+PHetToHom)
        ( het, hom ) = calibrateGivenPHet(rabins, hetCvgBinner, homCvgBinner)

    return (het,hom)


def dumpSmoothedAndBins(fn, names, cscores, minScore, maxScore):
    ff = open(fn, 'w')
    fields = [ 'score' ]
    for name in names:
        fields.append('smoothed(%s)' % name)
    fields.append('')
    for name in names:
        fields.append('score(%s)' % name)
        fields.append('bins(%s)' % name)
    print >>ff, ','.join(fields)
    for sc in xrange(minScore, maxScore+1):
        fields = [ '%d' % sc ]
        for cscore in cscores:
            fields.append('%.2f' % cscore.getSmoothed(sc))
        fields.append('')
        for cscore in cscores:
            ii = sc - minScore
            if ii < len(cscore.binned):
                fields.append('%.2f' % cscore.binned[ii][0])
                fields.append('%.2f' % cscore.binned[ii][1])
            else:
                fields.append('')
                fields.append('')
        print >>ff, ','.join(fields)
    ff.close()


def dumpMetricsHeader(ff, swVer):
    global now
    print >>ff, '#SOFTWARE_VERSION\t%s' % swVer
    print >>ff, '#CALIB_FORMAT_VERSION\t1'
    print >>ff, '#CALIB_VERSION\t1'
    print >>ff, '#GENERATED_AT\t%s' % str(now)
    print >>ff, '#TYPE\tSCORE_CALIBRATION_METRICS'
    print >>ff, ''


def dumpHeader(ff, swVer, key):
    parts = key.split('-')
    vt = parts[0]
    lm = parts[1]
    em = parts[2]
    af = .5
    if len(parts) > 3 and parts[3].startswith('af'):
        af = float(parts[3][2:4]) / 100.0
    global now
    print >>ff, '#SOFTWARE_VERSION\t%s' % swVer
    print >>ff, '#CALIB_FORMAT_VERSION\t1'
    print >>ff, '#CALIB_VERSION\t1'
    print >>ff, '#GENERATED_AT\t%s' % str(now)
    print >>ff, '#VARTYPE\t%s' % vt
    print >>ff, '#LIKELIHOOD_MODEL\t%s' % lm
    print >>ff, '#ERROR_MODE\t%s' % em
    print >>ff, '#ALLELE_FRACTION\t%.2f' % af
    print >>ff, '#TYPE\tSCORE_CALIBRATION'
    print >>ff, ''


def dumpSmoothedByCvg(cscores, outpath, key, minScore, maxScore, detail=True):
    ff = open(pjoin(outpath, key+'.tsv'), 'w')
    dumpHeader(ff, SOFTWARE_VERSION, key)
    fields = [ '>score' ]
    for bin in xrange(cscores.cvgBinner.bincount()):
        mincvg = cscores.cvgBinner.bin2mincvg(bin)
        fields.append('cvg%d' % mincvg)
    print >>ff, '\t'.join(fields)

    for sc in xrange(minScore, maxScore+1):
        fields = [ '%d' % sc ]
        for bin in xrange(cscores.cvgBinner.bincount()):
            cs = cscores.csa[bin]
            fields.append('%.1f' % cs.getSmoothed(sc))
        print >>ff, '\t'.join(fields)
    ff.close()

    if detail:
        for bin in xrange(cscores.cvgBinner.bincount()):
            cs = cscores.csa[bin]
            mincvg = cscores.cvgBinner.bin2mincvg(bin)
            outdir = pjoin(outpath, 'cvg%d' % mincvg)
            if not os.path.exists(outdir):
                os.mkdir(outdir)
            colname = key
            fncsv = ('cvg%d-' % mincvg) + key + '.csv'
            dumpSmoothedAndBins(pjoin(outdir, fncsv), [colname], [cs], minScore, maxScore)


def getTrueCounts(rabins, het, hom):
    homcountp = 0
    hetcountp = 0
    for bin in rabins:
        if bin.scoreHetA is not None and bin.scoreHomB is None:
            hetcountp += bin.count
        if bin.scoreHomB is not None and bin.scoreHetA is None:
            homcountp += bin.count

    hetc = 0.0
    homc = 0.0

    for bin in rabins:
        if bin.scoreHetA is None:
            homc += 1.0 * bin.count
        elif bin.scoreHomB is None:
            hetc += 1.0 * bin.count
        else:
            PHetToHom = ( ( hom.getErrorRate(bin.cvgHomB, bin.scoreHomB) * homcountp ) /
                          ( het.getErrorRate(bin.cvgHetA, bin.scoreHetA) * hetcountp ) )
            PHet = PHetToHom / (1+PHetToHom)
            hetc += PHet * bin.count
            homc += (1.0-PHet) * bin.count

    return (hetc,homc)


def dumpMetrics(fn, metrics):
    ff = open(fn, 'w')
    dumpMetricsHeader(ff, SOFTWARE_VERSION)
    print >>ff, '>metric\tvalue'
    for metric in sorted(metrics.keys()):
        value = metrics[metric]
        print >>ff, '%s\t%s' % (metric,str(value))
    ff.close()


def dumpBins(fn, rabins):
    ff = open(fn, 'w')
    print >>ff, 'scoreHetA,scoreHomB,cvgHetA,cvgHomB,dbsnp,count,PHet,locusA'
    for bin in rabins:
        print >>ff, str(bin)
    ff.close()


def loadRefDistro(fnAll, fnByVt, data):
    print 'Loading',fnByVt

    ff = open(fnAll)
    hFields = ff.readline().rstrip('\r\n').split(',')
    countIdx = hFields.index('count')
    sumcountall = 0
    for line in ff:
        fields = line.rstrip('r\n').split(',')
        sumcountall += float(fields[countIdx])
    ff.close()

    ff = open(fnByVt)
    hFields = ff.readline().rstrip('\r\n').split(',')
    cvgIdx = hFields.index('cvgFullUnique')
    scIdx = hFields.index('score')
    countIdx = hFields.index('count')
    counts = {}
    sumcountvt = 0
    for line in ff:
        fields = line.rstrip('r\n').split(',')
        cvg = int(fields[cvgIdx]) / 10 * 10
        if cvg > 150:
            cvg = 150
        sc = int(fields[scIdx])
        count = float(fields[countIdx])
        sumcountvt += count
        if count > 0:
            if (cvg,sc) not in counts:
                counts[(cvg,sc)] = count
            else:
                counts[(cvg,sc)] += count
    for (cvg,sc) in counts:
        count = counts[(cvg,sc)]

        # Scale distribution to entire genome. Note that this is
        # dangerous, if it results in score bins that have enough true
        # events by scaling up but the unscaled bin doesn't have
        # enough. If this causes problems, it will end up leading to
        # bins with fluctuating scores.
        count = int(count * sumcountall / sumcountvt)

        data.refAltVaf.append(Observation(None, sc, None, cvg, False, count))
        data.refAltEaf.append(Observation(None, sc, None, cvg, False, count))
    ff.close()

def loadMvAnn(fnA, fnB, fnSom, data):
    print 'Loading',fnSom
    ff = open(fnSom)
    somaticVafByLocus = {}
    somaticEafByLocus = {}
    for vt in VT:
        somaticVafByLocus[vt] = {}
        somaticEafByLocus[vt] = {}
    hFields = ff.readline().rstrip('\r\n').split('\t')
    vtIdx = hFields.index('varType')
    vafIdx = hFields.index('varScoreVAF')
    eafIdx = hFields.index('varScoreEAF')
    refIdx = hFields.index('RefScoreB')
    cvgIdx = hFields.index('RefCvgB')
    if 'xRef' in hFields:
        xRefIdx = hFields.index('xRef')
    else:
        xRefIdx = hFields.index('allele1XRef')
    locusIdx = hFields.index('locus')
    for line in ff:
        fields = line.rstrip('\r\n').split('\t')
        vt = fields[vtIdx]
        vaf = int(fields[vafIdx])
        eaf = int(fields[eafIdx])
        rsc = int(fields[refIdx])
        locus = int(fields[locusIdx])
        cvg = int(fields[cvgIdx]) / 10 * 10
        if cvg > 150:
            cvg = 150
        dbsnp = fields[xRefIdx].find('dbsnp') != -1
        if vt not in VT:
            continue
        # Careful -- I'm not filling in the cvgHetA right now because
        # I don't have it. Will need to fill this in later when
        # reading masterVar file of A genome.
        somaticVafByLocus[vt][locus] = Observation(vaf, rsc, None, cvg, dbsnp, 1, locusA=locus)
        somaticEafByLocus[vt][locus] = Observation(eaf, rsc, None, cvg, dbsnp, 1, locusA=locus)
    ff.close()

    print 'Loading',fnA
    ff = open(fnA)
    while True:
        header = ff.readline()
        if header.startswith('>'):
            break
    hFields = header.rstrip('r\n').split('\t')
    clzIdx = hFields.index('>LocusDiffClassification')
    zygIdx = hFields.index('zygosity')
    vtIdx = hFields.index('varType')
    cpIdx = hFields.index('calledPloidy')
    locusIdx = hFields.index('locus')
    cvgIdx = hFields.index('totalReadCount')
    if 'allele1Score' in hFields and not 'allele1VarScoreVAF' in hFields:
        vaf1Idx = hFields.index('allele1Score')
        eaf1Idx = hFields.index('allele1Score')
        vaf2Idx = hFields.index('allele2Score')
        eaf2Idx = hFields.index('allele2Score')
    else:
        vaf1Idx = hFields.index('allele1VarScoreVAF')
        eaf1Idx = hFields.index('allele1VarScoreEAF')
        vaf2Idx = hFields.index('allele2VarScoreVAF')
        eaf2Idx = hFields.index('allele2VarScoreEAF')
    ploidyIdx = hFields.index('ploidy')
    if 'xRef' in hFields:
        xRefIdx = hFields.index('xRef')
    else:
        xRefIdx = hFields.index('allele1XRef')
    vafc = {}
    eafc = {}
    vafc2 = {}
    eafc2 = {}
    hetvhom = {}
    loaded = 0
    for vt in VT:
        vafc[vt] = {}
        eafc[vt] = {}
        vafc2[vt] = {}
        eafc2[vt] = {}
    for line in ff:
        fields = line.rstrip('r\n').split('\t')
        vt = fields[vtIdx]
        if vt not in VT:
            continue
        if fields[ploidyIdx] != '2':
            continue
        if fields[cpIdx] != '2':
            continue

#         if loaded > 30000:
#             break

        zygosity = fields[zygIdx]
        clz = fields[clzIdx]
        vaf1 = int(fields[vaf1Idx])
        eaf1 = int(fields[eaf1Idx])
        vaf2 = int(fields[vaf2Idx])
        eaf2 = int(fields[eaf2Idx])
        dbsnp = fields[xRefIdx].find('dbsnp') != -1
        locus = int(fields[locusIdx])
        cvg = int(fields[cvgIdx]) / 10 * 10
        if cvg > 150:
            cvg = 150

        if locus in somaticVafByLocus[vt]:
            data[vt].refAltVaf.append(somaticVafByLocus[vt][locus])
            data[vt].refAltVaf[-1].cvgHetA = cvg
            data[vt].refAltEaf.append(somaticEafByLocus[vt][locus])
            data[vt].refAltEaf[-1].cvgHetA = cvg

        if clz == 'ref-identical;alt-identical' and zygosity == 'het-ref':
            if (vaf1,cvg,dbsnp) not in vafc[vt]:
                vafc[vt][(vaf1,cvg,dbsnp)] = 1
            else:
                vafc[vt][(vaf1,cvg,dbsnp)] += 1
            if (eaf1,cvg,dbsnp) not in eafc[vt]:
                eafc[vt][(eaf1,cvg,dbsnp)] = 1
            else:
                eafc[vt][(eaf1,cvg,dbsnp)] += 1
            if (vaf2,cvg,dbsnp) not in vafc2[vt]:
                vafc2[vt][(vaf2,cvg,dbsnp)] = 1
            else:
                vafc2[vt][(vaf2,cvg,dbsnp)] += 1
            if (eaf2,cvg,dbsnp) not in eafc2[vt]:
                eafc2[vt][(eaf2,cvg,dbsnp)] = 1
            else:
                eafc2[vt][(eaf2,cvg,dbsnp)] += 1
            loaded +=1

        if clz == 'alt-identical;onlyB' and zygosity == 'het-ref':
            hetvhom[','.join(fields[3:5])] = (vt, vaf2, eaf2, cvg, dbsnp)

    for vt in VT:
        for (vaf1,cvg,dbsnp) in vafc[vt]:
            data[vt].refAltVaf.append(Observation(vaf1,None,cvg,None,dbsnp,vafc[vt][(vaf1,cvg,dbsnp)]))
        for (eaf1,cvg,dbsnp) in eafc[vt]:
            data[vt].refAltEaf.append(Observation(eaf1,None,cvg,None,dbsnp,eafc[vt][(eaf1,cvg,dbsnp)]))
        for (vaf2,cvg,dbsnp) in vafc2[vt]:
            data[vt].homAltVaf.append(Observation(vaf2,None,cvg,None,dbsnp,vafc2[vt][(vaf2,cvg,dbsnp)]))
        for (eaf2,cvg,dbsnp) in eafc2[vt]:
            data[vt].homAltEaf.append(Observation(eaf2,None,cvg,None,dbsnp,eafc2[vt][(eaf2,cvg,dbsnp)]))
    ff.close()

    print 'Loading',fnB
    ff = open(fnB)
    while True:
        header = ff.readline()
        if header.startswith('>'):
            break
    hFields = header.rstrip('r\n').split('\t')
    clzIdx = hFields.index('>LocusDiffClassification')
    zygIdx = hFields.index('zygosity')
    vtIdx = hFields.index('varType')
    cpIdx = hFields.index('calledPloidy')
    cvgIdx = hFields.index('totalReadCount')
    if 'allele1Score' in hFields and not 'allele1VarScoreVAF' in hFields:
        vaf1Idx = hFields.index('allele1Score')
        eaf1Idx = hFields.index('allele1Score')
        vaf2Idx = hFields.index('allele2Score')
        eaf2Idx = hFields.index('allele2Score')
    else:
        vaf1Idx = hFields.index('allele1VarScoreVAF')
        eaf1Idx = hFields.index('allele1VarScoreEAF')
        vaf2Idx = hFields.index('allele2VarScoreVAF')
        eaf2Idx = hFields.index('allele2VarScoreEAF')
    ploidyIdx = hFields.index('ploidy')
    if 'xRef' in hFields:
        xRefIdx = hFields.index('xRef')
    else:
        xRefIdx = hFields.index('allele1XRef')
    loaded = 0
    vafc = {}
    eafc = {}
    for vt in VT:
        vafc[vt] = {}
        eafc[vt] = {}
    for line in ff:
        fields = line.rstrip('r\n').split('\t')
        vt = fields[vtIdx]
        if vt not in VT:
            continue
        if fields[ploidyIdx] != '2':
            continue
        if fields[cpIdx] != '2':
            continue

#         if loaded > 30000:
#             break

        zygosity = fields[zygIdx]
        clz = fields[clzIdx]
        vaf1 = int(fields[vaf1Idx])
        eaf1 = int(fields[eaf1Idx])
        vaf2 = int(fields[vaf2Idx])
        eaf2 = int(fields[eaf2Idx])
        dbsnp = fields[xRefIdx].find('dbsnp') != -1
        cvg = int(fields[cvgIdx]) / 10 * 10
        if cvg > 150:
            cvg = 150
        if clz == 'alt-identical;alt-identical' and zygosity == 'hom':
            vaf = min(vaf1,vaf2)
            eaf = min(eaf1,eaf2)
            if (vaf,cvg,dbsnp) not in vafc[vt]:
                vafc[vt][(vaf,cvg,dbsnp)] = 1
            else:
                vafc[vt][(vaf,cvg,dbsnp)] += 1
            if (eaf,cvg,dbsnp) not in eafc[vt]:
                eafc[vt][(eaf,cvg,dbsnp)] = 1
            else:
                eafc[vt][(eaf,cvg,dbsnp)] += 1
            loaded +=1

        if clz == 'alt-identical;onlyA' and zygosity == 'hom':
            key = ','.join(fields[3:5])
            vafhom = min(vaf1,vaf2)
            eafhom = min(eaf1,eaf2)
            if key in hetvhom:
                (ovt,ovaf2,oeaf2,ocvg,odbsnp) = hetvhom[key]
                data[vt].homAltVaf.append(Observation(ovaf2,vafhom,ocvg,cvg,dbsnp))
                data[vt].homAltEaf.append(Observation(oeaf2,eafhom,ocvg,cvg,dbsnp))

    for vt in VT:
        for (vaf,cvg,dbsnp) in vafc[vt]:
            data[vt].homAltVaf.append(Observation(None,vaf,None,cvg,dbsnp,vafc[vt][(vaf,cvg,dbsnp)]))
        for (eaf,cvg,dbsnp) in eafc[vt]:
            data[vt].homAltEaf.append(Observation(None,eaf,None,cvg,dbsnp,eafc[vt][(eaf,cvg,dbsnp)]))
    ff.close()


def loadAfDistros(binner, fnPure, fnMixed, vafDistroPure, vafDistroMixed):
    print 'Loading',fnPure
    ff = open(fnPure)
    while True:
        header = ff.readline()
        if header.startswith('>'):
            break
    hFields = header.rstrip('r\n').split('\t')
    clzIdx = hFields.index('>LocusDiffClassification')
    zygIdx = hFields.index('zygosity')
    vtIdx = hFields.index('varType')
    cpIdx = hFields.index('calledPloidy')
    cvgIdx = hFields.index('totalReadCount')
    chromosomeIdx = hFields.index('chromosome')
    beginIdx = hFields.index('begin')
    endIdx = hFields.index('end')
    allele1SeqIdx = hFields.index('allele1Seq')
    if 'allele1Score' in hFields and not 'allele1VarScoreVAF' in hFields:
        vaf1Idx = hFields.index('allele1Score')
    else:
        vaf1Idx = hFields.index('allele1VarScoreVAF')
    ploidyIdx = hFields.index('ploidy')
    diffs = {}
    loaded = 0
    for line in ff:
        fields = line.rstrip('r\n').split('\t')
        vt = fields[vtIdx]
        if vt not in VT:
            continue
        if fields[ploidyIdx] != '2':
            continue
        if fields[cpIdx] != '2':
            continue

#         if loaded > 30000:
#             break

        chromosome = fields[chromosomeIdx]
        begin = int(fields[beginIdx])
        end = int(fields[endIdx])
        allele1Seq = fields[allele1SeqIdx]
        zygosity = fields[zygIdx]
        clz = fields[clzIdx]
        vaf1 = int(fields[vaf1Idx])
        cvg = int(fields[cvgIdx])
        bin = binner.cvg2bin(cvg)

        if clz == 'ref-identical;onlyA' and zygosity == 'het-ref':
            diffs['%s,%d,%d,%s' % (chromosome,begin,end,allele1Seq)] = 1
            if vaf1 not in vafDistroPure[vt][bin]:
                vafDistroPure[vt][bin][vaf1] = 0
            vafDistroPure[vt][bin][vaf1] += 1
            loaded +=1
    ff.close()

    print 'Loading',fnMixed
    ff = open(fnMixed)
    while True:
        header = ff.readline()
        if header.startswith('>'):
            break
    hFields = header.rstrip('r\n').split('\t')
    clzIdx = hFields.index('>LocusDiffClassification')
    zygIdx = hFields.index('zygosity')
    vtIdx = hFields.index('varType')
    cpIdx = hFields.index('calledPloidy')
    cvgIdx = hFields.index('totalReadCount')
    chromosomeIdx = hFields.index('chromosome')
    beginIdx = hFields.index('begin')
    endIdx = hFields.index('end')
    allele1SeqIdx = hFields.index('allele1Seq')
    if 'allele1Score' in hFields and not 'allele1VarScoreVAF' in hFields:
        vaf1Idx = hFields.index('allele1Score')
    else:
        vaf1Idx = hFields.index('allele1VarScoreVAF')
    ploidyIdx = hFields.index('ploidy')
    loaded = 0
    for line in ff:
        fields = line.rstrip('r\n').split('\t')
        vt = fields[vtIdx]
        if vt not in VT:
            continue
        if fields[ploidyIdx] != '2':
            continue

#         if loaded > 30000:
#             break

        chromosome = fields[chromosomeIdx]
        begin = int(fields[beginIdx])
        end = int(fields[endIdx])
        allele1Seq = fields[allele1SeqIdx]
        zygosity = fields[zygIdx]
        clz = fields[clzIdx]
        vaf1 = int(fields[vaf1Idx])
        cvg = int(fields[cvgIdx])
        bin = binner.cvg2bin(cvg)

        if clz == 'ref-identical;onlyA' and zygosity == 'het-ref':
            key = '%s,%d,%d,%s' % (chromosome,begin,end,allele1Seq)
            if key in diffs:
                if vaf1 not in vafDistroMixed[vt][bin]:
                    vafDistroMixed[vt][bin][vaf1] = 0
                vafDistroMixed[vt][bin][vaf1] += 1
                loaded +=1
    ff.close()


outpath = os.path.abspath(sys.argv[1])
tmppath = pjoin(outpath, 'tmp')
if not os.path.exists(tmppath):
    os.mkdir(tmppath)
inp = []
afinp = []
for arg in sys.argv[2:]:
    if arg.startswith('af:'):
        (af,pure,mixed) = arg.split(':')
        pure  = os.path.abspath(pure)
        mixed = os.path.abspath(mixed)
        afinp.append( (pure,mixed) )
    else:
        (path,baseline) = arg.split(':')
        path = os.path.abspath(path)
        baseline = os.path.abspath(baseline)
        inp.append( (path,baseline) )

# Change dir to tmppath, so R output is created in the tmppath.
os.chdir(tmppath)

# Het calibration.
binner  = CvgBinner( [ 0, 20, 30, 40, 50, 60, 70, 80, 90, 110 ] )
# Not enough UC/OC data for such fine-grained bins. Use bigger bins.
ucBinner  = CvgBinner( [ 0, 30, 60 ] )
ucSubBinner = CvgBinner( [ 0 ] )
obinner = CvgBinner( [ 0, 20, 40 ] )

vafDistroPure = {}
vafDistroMixed = {}
for vt in VT:
    vafDistroPure [vt] = []
    vafDistroMixed[vt] = []
    for bin in xrange(binner.bincount()):
        vafDistroPure [vt].append({})
        vafDistroMixed[vt].append({})
for (pure,mixed) in afinp:
    loadAfDistros(binner,
                  pjoin(pure,  'mv-ann.tsv'),
                  pjoin(mixed, 'mv-ann.tsv'),
                  vafDistroPure, vafDistroMixed)

afCorrection = {}
for vt in VT:
    print 'Making %s-vaf-fp-af20correction...' % vt
    afCorrection[vt] = makeAfCorrection(binner, vafDistroPure[vt], vafDistroMixed[vt])
    dumpSmoothedByCvg(afCorrection[vt], outpath, '%s-vaf-fp-af20correction' % vt, 0, 1000)

data = {}
metrics = {}
metrics['calib-version'] = '1'
metrics['calib-format-version'] = '1'
for vt in VT:
    data[vt] = Observations()
for (path,baseline) in inp:
    for vt in VT:
        loadRefDistro(pjoin(baseline, 'refdistro', 'RefScoreDistribution.csv'),
                      pjoin(baseline, 'refdistro-byfreq-'+vt, 'RefScoreDistribution.csv'),
                      data[vt])
    loadMvAnn(pjoin(path,     'mv-ann.tsv'),
              pjoin(baseline, 'mv-ann.tsv'),
              pjoin(path,     'calldiff', 'SomaticOutput.tsv'),
              data)
for vt in VT:
    print 'Calibrating %s-vaf-fp...' % vt
    ( vaffp, ovaffn ) = calibrateHetHom(data[vt].refAltVaf, binner, obinner)
    dumpSmoothedByCvg(vaffp, outpath, '%s-vaf-fp' % vt, 0, 1000)
    dumpBins(pjoin(outpath, '%s-vaf-fp-bins.csv' % vt), data[vt].refAltVaf)
    vaffpaf = vaffp.applyCorrection(afCorrection[vt])
    dumpSmoothedByCvg(vaffpaf, outpath, '%s-vaf-fp-af20' % vt, 0, 1000, False)
    print 'Calibrating %s-eaf-fp...' % vt
    ( eaffp, oeaffn ) = calibrateHetHom(data[vt].refAltEaf, binner, obinner)
    dumpSmoothedByCvg(eaffp, outpath, '%s-eaf-fp' % vt, 0, 1000)
    myUcBinner = ucBinner
    if vt == 'sub':
        myUcBinner = ucSubBinner
    print 'Calibrating %s-vaf-uc...' % vt
    ( vafuc, ovafoc ) = calibrateHetHom(data[vt].homAltVaf, myUcBinner, obinner)
    dumpSmoothedByCvg(vafuc, outpath, '%s-vaf-uc' % vt, 0, 1000)
    print 'Calibrating %s-eaf-uc...' % vt
    ( eafuc, oeafoc ) = calibrateHetHom(data[vt].homAltEaf, myUcBinner, obinner)
    dumpSmoothedByCvg(eafuc, outpath, '%s-eaf-uc' % vt, 0, 1000)
    (hetc, homc) = getTrueCounts(data[vt].refAltVaf, vaffp, ovaffn)
    metrics['count-het-'+vt] = str(int(hetc))

# Hom calibration.
binner  = CvgBinner( [ 0, 20, 40, 60, 100 ] )
# Not enough UC/OC data for such fine-grained bins. Use bigger bins.
ocBinner    = CvgBinner( [ 0, 30, 60 ] )
ocSubBinner = CvgBinner( [ 0 ] )
obinner = CvgBinner( [ 0, 20, 40 ] )

data = {}
for vt in VT:
    data[vt] = Observations()
for (path,baseline) in inp:
    for vt in VT:
        loadRefDistro(pjoin(path, 'refdistro', 'RefScoreDistribution.csv'),
                      pjoin(path, 'refdistro-byfreq-'+vt, 'RefScoreDistribution.csv'),
                      data[vt])
    loadMvAnn(pjoin(baseline, 'mv-ann.tsv'),
              pjoin(path,     'mv-ann.tsv'),
              pjoin(baseline, 'calldiff', 'SomaticOutput.tsv'),
              data)
for vt in VT:
    print 'Calibrating %s-vaf-fn...' % vt
    ( ovaffp, vaffn ) = calibrateHetHom(data[vt].refAltVaf, obinner, binner)
    dumpSmoothedByCvg(vaffn, outpath, '%s-vaf-fn' % vt, -3000, 1000)
    print 'Calibrating %s-eaf-fn...' % vt
    ( oeaffp, eaffn ) = calibrateHetHom(data[vt].refAltEaf, obinner, binner)
    dumpSmoothedByCvg(eaffn, outpath, '%s-eaf-fn' % vt, -3000, 1000)
    myOcBinner = ocBinner
    if vt == 'sub':
        myOcBinner = ocSubBinner
    print 'Calibrating %s-vaf-oc...' % vt
    ( ovafuc, vafoc ) = calibrateHetHom(data[vt].homAltVaf, obinner, myOcBinner)
    dumpSmoothedByCvg(vafoc, outpath, '%s-vaf-oc' % vt, 0, 1000)
    print 'Calibrating %s-eaf-oc...' % vt
    ( oeafuc, eafoc ) = calibrateHetHom(data[vt].homAltEaf, obinner, myOcBinner)
    dumpSmoothedByCvg(eafoc, outpath, '%s-eaf-oc' % vt, 0, 1000)
    (hetc, homc) = getTrueCounts(data[vt].refAltVaf, ovaffp, vaffn)
    if 'snp' == vt:
        metrics['ref-bases'] = str(int(homc))

dumpMetrics(pjoin(outpath, 'metrics.tsv'), metrics)
