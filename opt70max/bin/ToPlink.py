#! /usr/bin/env python

import math,os,sys
from os.path import join as pjoin
from glob import glob
import bz2

def glob1(fn):
    result = glob(fn)
    if len(result) != 1:
        raise Exception('glob1: '+fn)
    return result[0]

def PrintGeno(outpath,geno):
    print 'Writing', outpath
    ff = open(outpath, 'w')
    ff.write("SNPrs\thgdpCalls\tcgiCalls\n")
    index=1
    for rs in geno.keys():
        ff.write(rs)
        ff.write("\t")
        ff.write(geno[rs]['hgdp'])
        ff.write("\t")
        ff.write(geno[rs]['cgi'])
        ff.write("\n")
    ff.close()


def LoadGeno(fn, geno):
    print 'Loading',fn
    ff = open(fn)
    header = ff.readline().rstrip('\r\n').split('\t')
    rsIdx=header.index('SNPID')
    callIdx=header.index('SNP')
    for line in ff:
        fields = line.rstrip('\r\n').split('\t')
        rs=fields[rsIdx]
        call=fields[callIdx]
        geno[rs] = {}
        geno[rs]['hgdp']=call
        geno[rs]['cgi']='NN'
    ff.close()

def GetGeno(varbz2,geno):
    print "Loading",varbz2
    #var_file=bz2.BZ2File(varbz2,"r")
    var_file=open(varbz2)
    for line in var_file:
        if line.find('>dbSnpId') != -1:
            fields = line.rstrip('\r\n').split('\t')
            rsIdx=fields.index('>dbSnpId')
            callAIdx=fields.index('alleleAGenotype')
            callBIdx=fields.index('alleleBGenotype')
            break;
    count=0
    for line in var_file:
        fields = line.rstrip('\r\n').split('\t')
        rs=fields[rsIdx].split(':')[1]
        callA=fields[callAIdx]
        callB=fields[callBIdx]   
        if callA == 'NO-CALL' or callB == 'NO-CALL':
            continue;
        if rs in geno:
             if geno[rs]['hgdp'].find(callA) !=-1 and geno[rs]['hgdp'].find(callB) != -1:
                 geno[rs]['cgi']=callA+' '+callB
                 count+=1
    var_file.close()
    return count

def MakeFile(inpath,geno,outpath,goodmk):
    MAP=open(pjoin(outpath,'TempAdmixture.map'),"w")
    PED=open(pjoin(outpath,'TempAdmixture.ped'),"w")
    if goodmk<100:
        PED.write("unknown unknown 0 0 0 -9 2 2\n")
        MAP.write("1\trs000\t0\t0\n") 
    else:
        mapfile=open(pjoin(inpath,'HGDP_hapmap_IN0.1_prune0.1.map'))
        pedfile=open(pjoin(inpath,'HGDP_hapmap_IN0.1_prune0.1.ped'))
        PED.write("unknown unknown 0 0 0 -9 ")
        keep=[]
        snprs=[]
        index=0
        for line in mapfile:
            fields=line.rstrip('\r\n').split('\t')
            rs=fields[1]
            if rs in geno:
                if geno[rs]['cgi'] != 'NN':
                    keep.append(index) 
                    snprs.append(rs)
                    MAP.write(line)
                    tempgeno=geno[rs]['cgi'].replace(geno[rs]['hgdp'][0],'1')
                    geno[rs]['cgi']=tempgeno.replace(geno[rs]['hgdp'][1],'2')
                    PED.write(geno[rs]['cgi']+" ") 
            index=index+1
        PED.write("\n")
        for line in pedfile:
            fields=line.rstrip('\r\n').split(' ')
            info=" ".join(fields[0:6])
            for i in range(0,len(keep)):
                rs=snprs[i]
                tempgeno=fields[keep[i]*2+6]+" "+fields[keep[i]*2+7]
                newgeno=tempgeno.replace(geno[rs]['hgdp'][0],'1')
                tempgeno=newgeno.replace(geno[rs]['hgdp'][1],'2')
                info += " "
                info += tempgeno 
            PED.write(info)
            PED.write("\n")
        mapfile.close()
        pedfile.close() 
    MAP.close()
    PED.close()

def ReadGeno(fn,geno):
   print 'Loading',fn
   ff = open(fn)
   header = ff.readline().rstrip('\r\n').split('\t')
   rsIdx=header.index('SNPrs')
   hcallIdx=header.index('hgdpCalls')
   ccallIdx=header.index('cgiCalls')
   for line in ff:
       fields = line.rstrip('\r\n').split('\t')
       rs=fields[rsIdx]
       hcall=fields[hcallIdx]
       ccall=fields[ccallIdx]
       geno[rs] = {}
       geno[rs]['hgdp']=hcall
       geno[rs]['cgi']=ccall
   ff.close()
 
Ancepath = os.path.abspath(pjoin(sys.argv[1],'etc'))
dbSNPfile = os.path.abspath(sys.argv[2])
Outpath = os.path.abspath(sys.argv[3])
Geno = {}
LoadGeno(pjoin(Ancepath,'HGDP_hapmap_IN0.1_prune0.1_markerinfo'),Geno)
GoodMarker=GetGeno(dbSNPfile,Geno)
print 'GoodMarker count is ',GoodMarker
MakeFile(Ancepath,Geno,Outpath,GoodMarker)
