#! /usr/bin/env python

import os,sys
import optparse

TUMOR_NO_CALL_MAPPINGS = { "calledLevel": "N",
                           "levelScore": "0"
                           }
NORMAL_NO_CALL_MAPPINGS = { "calledPloidy": "N",
                            "ploidyScore": "0",
                            "calledCNVType": "N",
                            "CNVTypeScore": "0"
                            }
                     


def nocallFile(infile,outfile):

    inf = open(infile)
    outf = open(outfile, "w")


    while True:
        line = inf.readline()
        outf.write(line)
        if line[0] == ">":
            break

    ncmappings = NORMAL_NO_CALL_MAPPINGS
    
    header = line[1:].rstrip("\r\n")
    hFields = header.split("\t")
    for name in hFields:
        if name == "calledLevel":
            ncmappings = TUMOR_NO_CALL_MAPPINGS
    idxToValue = []
    for ii in xrange(len(hFields)):
        name = hFields[ii]
        if name in ncmappings:
            idxToValue.append( (ii,ncmappings[name]) )
    for line in inf:
        fields = line.rstrip("\r\n").split("\t")
        for (idx,val) in idxToValue:
            fields[idx] = val
        outf.write("\t".join(fields) + "\n")

    inf.close()
    outf.close()


def nocallCNVs(inputPfx,outputPfx):

    inSegs = inputPfx + "AnnotatedSegments.tsv"
    outSegs = outputPfx + "AnnotatedSegments.tsv"
    nocallFile(inSegs,outSegs)

    inSegs = inputPfx + "Segments.tsv"
    outSegs = outputPfx + "Segments.tsv"
    nocallFile(inSegs,outSegs)

    inDets = inputPfx + "Details.tsv"
    outDets = outputPfx + "Details.tsv"
    nocallFile(inDets,outDets)

    
    
    
def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(inputPrefix='',outputPrefix='')
    parser.add_option('--inputPrefix', dest='inputPrefix',
                      help='Partial path to input files, e.g. /foo/ASM/CNVmain/Normal/copyCalls2000-post')
    parser.add_option('--outputPrefix', dest='outputPrefix',
                      help='Partial path to output files, e.g. /foo/ASM/CNVmain/Normal/nocalled-copyCalls2000-post')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.inputPrefix:
        parser.error('inputPrefix specification required')
    if not options.outputPrefix:
        parser.error('outputPrefix specification required')

    nocallCNVs(options.inputPrefix,options.outputPrefix)



if __name__ == '__main__':
    main()
