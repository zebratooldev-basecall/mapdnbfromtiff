#!/bin/env python
import os,sys
import optparse
from cgatools.reference import CrrFile
from cgatools.util.misc import openFileByExtension

chromMap = {}

headerTypePrio = [ 'fileformat',
                   'fileDate',
                   'center',
                   'source',
                   'source_GENOME_REFERENCE',
                   'source_GENE_ANNOTATIONS',
                   'source_DBSNP_BUILD',
                   'source_COSMIC',
                   'source_DGV_VERSION',
                   'source_MIRBASE_VERSION',
                   'source_PFAM_DATE',
                   'source_REPMASK_GENERATED_AT',
                   'source_SEGDUP_GENERATED_AT',
                   'reference',
                   'contig',
                   'phasing',
                   'INFO',
                   'ALT',
                   'FILTER',
                   'FORMAT',
                   'CHROM',
                   ]

def constructChrMap(crr):
    '''map supercontig names to an index for sorting'''
    superlist = crr.listChromosomes()
    n = 0
    global chromMap
    for s in superlist:
        t = s.name
        if t[:3] == "chr":
            t = t[3:]
        if t in chromMap:
            raise Exception("ABORT: Dup supercontig %s in crr file?" % s.name)
        chromMap[t] = n
        n += 1

def getHeaderType(headerLine):
    '''parse the "type" of a VCF header line ("fileformat", "INFO", "FORMAT", "FILTER", or "CHROM")'''
    if headerLine[0:2] == "##":
        headType = headerLine[2:].split('=')[0]
    else:
        if headerLine[0:1] != "#":
            raise Exception("ABORT: confusion about header line definition")
        headType = headerLine[1:].split('\t')[0]
    return headType


def getHeaderTypePriority(headType):
    '''convert a header type ("fileformat", "INFO", etc) into a sorting priority'''
    if headType not in headerTypePrio:
        raise Exception ("ABORT: header type unknown: " + headType)
    return headerTypePrio.index(headType)


def getHeaderID(headerLine):
    '''parse the "ID" from a VCF header line of type "INFO", "FORMAT", or "FILTER"'''
    w = headerLine.split('=')
    if len(w) < 3 or w[1] != "<ID":
        raise Excpetion("ABORT: Could not get ID out of header line:\n%s" % headerLine)
    return w[2].split(',')[0]
    

def matchingTypeChecks(aType,bType,a,b):
    '''check for conflicts between two VCF header lines of the same header type'''
    if aType == "fileformat":
        if a != b:
            raise Exception("ABORT: conflicting file formats:\n\t%s\t%s" % (a,b))
        else:
            return

    if aType == "reference":
        if a != b:
            raise Exception("ABORT: conflicting reference specifications:\n\t%s\t%s" % (a,b))
        else:
            return

    if aType == "CHROM":
        if a != b:
            raise Exception("ABORT: conflicting column header lines:\n\t%s\t%s" % (a,b))
        else:
            return

    if aType == "FORMAT" or aType == "FILTER" or aType == "INFO" or aType == "ALT" or aType == "contig":
        aID = getHeaderID(a)
        bID = getHeaderID(b)
        if aID == bID and a != b:
            raise Exception("ABORT: conflicting definitions:\n\t%s\t%s" % (a,b))
        else:
            return

    raise Exception("ABORT: header checking logic does not know about header type " + aType)


def compareHeaders(a,b):
    '''compare function for VCF header line sorting'''
    aType = getHeaderType(a)
    bType = getHeaderType(b)
    cmp = getHeaderTypePriority(aType) - getHeaderTypePriority(bType)
    if cmp != 0:
        return cmp
    if aType != bType:
        raise Exception("ABORT: header type priorities match but header types do not: %s and %s" % (aType,bType))
    matchingTypeChecks(aType,bType,a,b)
    if aType == 'contig':
        aID = chromMap[getHeaderID(a)]
        bID = chromMap[getHeaderID(b)]
        return aID-bID
    if a < b:
        return -1
    if a > b:
        return 1
    return 0


class VcfLine(object):
    '''VCF header line and index of input file the line comes from'''
    def __init__(self,line,index):
        self.line_ = line
        self.index_ = index


    def compareData(self,compareTo):
        w = self.line_.split('\t')
        x = compareTo.line_.split('\t')

        ## sort by chromosome order (based on crr file order)
        global chromMap
        chrA = chromMap[w[0]]
        chrB = chromMap[x[0]]
        if chrA != chrB:
            return chrA-chrB

        ## sort by start position
        posA = int(w[1])
        posB = int(x[1])
        if posA != posB:
            return posA - posB

        ## for duplicate lines, return 0 (ultimately suppressing duplication)
        if self.line_ == compareTo.line_:
            return 0

        ## retain sorting within a single file
        if self.index_ == compareTo.index_:
            return 1

        ## use lexical sorting between files
        if self.line_ < compareTo.line_:
            return -1
        if self.line_ > compareTo.line_:
            return 1

        raise Exception("Error in line ordering logic -- ABORT!")
        

def find_insertion_point(elem,elemlist):
    l = len(elemlist)
    if l > 5:
        raise Exception('ABORT: Naive list scanning and insertion may not be satisfactory; recode?')
    # scan for index of first element in list that is 'greater than' elem; we will insert before this point
    i = 0
    while i < l:
        # if lines are equal, return a special value that means 'skip this line' 
        if elem.compareData(elemlist[i]) == 0:
            return -1
        if elem.compareData(elemlist[i]) < 0:
            break
        i += 1
    return i

def openInputFiles(inputList):
    vcfs = []
    for i in inputList.split(','):
        f = openFileByExtension(i)
        vcfs += [ f ]
    return vcfs


def processTopOfFile(f,n,elemlist,headerLines):
    l = f.readline()
    insertpoint = -1
    while l and insertpoint < 0:
        if l[0]=='#':
            headerLines += [l]
        else:
            #print "from " + str(n) + " read " + l.rstrip()
            elem = VcfLine(l,n)
            insertpoint = find_insertion_point(elem,elemlist)
            if insertpoint >= 0:
                elemlist.insert(insertpoint,elem)
                #print "  insert at " + str(insertpoint)
                break
        l = f.readline()


def collectHeadersAndInitPQueue(inputList):
    vcfs = openInputFiles(inputList)
    headerLines = []
    elemlist = []
    n = 0
    for f in vcfs:
        processTopOfFile(f,n,elemlist,headerLines)
        n += 1
    return(vcfs,headerLines,elemlist)


def openOutputByExtension(fn):
    '''Open a file that may be compressed for writing.

    Returns the file-like object of appropriate type depending on the file
    extension (.bz2 and .gz are supported).'''

    _, ext = os.path.splitext(fn)
    if ext == '.bz2':
        import bz2
        return bz2.BZ2File(fn, 'w', 2*1024*1024)
    elif ext == '.gz':
        import gzip
        return gzip.GzipFile(fn, 'w')
    else:
        return open(fn, 'w')

def outputHeaders(headerLines,out):
    headerLines.sort(cmp=compareHeaders)
    prev = ''
    for l in headerLines:
        if l != prev:
            out.write(l)
            prev = l


def mergeDataLines(vcfs,elemlist,out,inputList):
    while len(elemlist) > 0:
        outelem = elemlist.pop(0)
        out.write(outelem.line_)
        idx = outelem.index_
        #print "wrote " + str(idx) + " wrote " + outelem.line_.rstrip()
        insertpoint = -1
        while insertpoint < 0:
            l = vcfs[idx].readline()
            if not l:
                break
            #print "from " + str(idx) + " read " + l.rstrip()
            elem =  VcfLine(l,idx)
            if elem.compareData(outelem) < 0:
                raise Exception('ABORT: Input VCF file ' + inputList.split(',')[idx] + ' not properly sorted; problem line:\n%s' % l)
            insertpoint = find_insertion_point(elem,elemlist)
            if insertpoint >= 0:
                elemlist.insert(insertpoint,elem)
                #print "  insert at " + str(insertpoint)
        
    
    
def mergeVCFs(inputList,outfile):
    (vcfs,headerLines,elemlist) = collectHeadersAndInitPQueue(inputList)
    o = openOutputByExtension(outfile)
    outputHeaders(headerLines,o)
    mergeDataLines(vcfs,elemlist,o,inputList)
    for f in vcfs:
        f.close()
    o.close()
    


def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(inputList='',crrFile='',outputFile='')
    parser.add_option('--vcf-inputs', dest='inputList',
                      help='comma-separated list of VCF files to merge')
    parser.add_option('--output',dest='outputFile',
                      help='output file name')
    parser.add_option('--reference-crr',dest='crrFile',
                      help='reference CRR file name')


    (options, args) = parser.parse_args()

    if not options.inputList:
        parser.error('vcf-inputs specification required')
    if not options.outputFile:
        parser.error('output specification required')
    if not options.crrFile:
        parser.error('reference-crr specification required')

    crr = CrrFile(options.crrFile)
    constructChrMap(crr)

    mergeVCFs(options.inputList,options.outputFile)


if __name__ == '__main__':
    main()
    
