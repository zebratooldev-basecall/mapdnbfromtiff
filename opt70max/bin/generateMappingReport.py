#!/usr/bin/python

import os, sys
import glob
try:
    import json
except Exception, e:
    import simplejson as json
import time
import string

def extractInfoOneField(samSummaryFile):
    mapCount = {}
    discordanceCount = {}
    fhIn = open(samSummaryFile, 'r')
    allLines = fhIn.readlines()
    fhIn.close()

    
    for line in allLines[1:3]:
        line = line.strip().split(',')
        mapCount[line[0]] = int(line[1])

    for line in allLines[9:]:
        line = line.strip()
        if line:
            line = line.split(',')
            try:
                discordanceCount[int(line[0])] = round(float(line[2]) * 100 / float(line[1]), 4)
            except ZeroDivisionError:
                discordanceCount[int(line[0])] = 0

        else:
            break
    return mapCount, discordanceCount

def transformHtml(mapDict, newDisData, prefix, htmlTemp, outHtmlReport):
    jsonData = {}

    mapTable = []
    mapTable.append(["mappingRate(%)", "C005R008","C005R032","C006R013","C012R008","C012R032", "Average"])
    for k in sorted(mapDict.keys()):
        row = [mapDict[k][i] for i in sorted(mapDict[k].keys())]
        fileterRow = map(float, filter(None, row))
        try:
            averageValue = round(sum(fileterRow) / len(fileterRow), 2)
        except ZeroDivisionError:
            averageValue = 0
        row.append(averageValue)
        row.insert(0, k)
        mapTable.append(row)
    jsonData['mapTable'] = json.dumps(mapTable)
    jsonData['disFigure'] = json.dumps(newDisData)

    reportTitle = 'BB Mapping Report of %s' % (prefix)
    jsonData['reportTitle'] = '"%s"' % reportTitle
    reportTime = time.strftime('%Y-%m-%d', time.localtime(time.time()))
    jsonData['reportTime'] = '"%s"' % reportTime

    fhIn = open(htmlTemp, 'r')
    template = fhIn.read()
    fhIn.close()
    # Construct a string template and replace json data
    newStr = string.Template(template)
    newStr = newStr.safe_substitute(jsonData)

    fhOut = open(outHtmlReport, 'w')
    fhOut.write(newStr)
    fhOut.close()

def parse_arguments(args):
    from optparse import  OptionParser
    parser = OptionParser()
    parser.add_option('--map-dir',help='the map dir', default='./',dest='map_dir')
    parser.add_option('--prefix',help='the prefix of run', default=None,dest='prefix')
    parser.add_option('--html-temp',help='the html template', default=None,dest='html_temp')

    options, args = parser.parse_args(args)
    return options.map_dir, options.prefix, options.html_temp

def main(argv):
    mapDir, prefix, htmlTemp = parse_arguments(argv)

    outHtmlReport = os.path.join(mapDir, '%s_mappingReport.html' % (prefix))
    fields = "C005R008,C005R032,C006R013,C012R008,C012R032".split(',')

    mapTable = {}
    disData = {}
    for laneDir in glob.glob(os.path.join(mapDir, 'GS*')):
        if not os.path.isdir(laneDir):
            continue
        lane = os.path.basename(laneDir)
        l = lane.split('-')[-1]
        disData[l] = {}
        tmpLaneMap = {}
        
        for field in fields:
            samSummaryFile = os.path.join(laneDir, field, '%s.%s.bt2.sam.summary.csv' % (lane, field))
            if not samSummaryFile:
                continue
            mapCount, discordanceCount = extractInfoOneField(samSummaryFile)
            try:
                tmpLaneMap[field] = round(float(mapCount['mappedDnbCount'])*100 / mapCount['inputDnbCount'], 2)
            except ZeroDivisionError, e:
                tmpLaneMap[filed] = 0
            except Error, e:
                raise e
            for k, v in discordanceCount.iteritems():
                if k in disData[l]:
                    disData[l][k].append(v)
                else:
                    disData[l][k] = [v]

        mapTable[l] = tmpLaneMap
        for k in disData[l]:
            if len(disData[l][k]) >= 1:
                filterData = disData[l][k]
                if 0 in mapTable[l].values():
                    filterData = filter(None, filterData)
                if filterData:
                    disData[l][k] = round(sum(filterData) / len(filterData), 4)
                else:
                    disData[l][k] = round(0.0, 4)


    newDisData = {}
    for laneDis in disData:
        tmpList = []
        posList = []
        for k in sorted(disData[laneDis].keys()):
            tmpList.append(disData[laneDis][k])
            posList.append(k)
        newDisData[laneDis] = tmpList
        if 'xTag' in newDisData:
            if posList != newDisData['xTag']:
                raise Exception, 'Position is not equal in %s' % laneDis
        else:
            newDisData['xTag'] = posList

    transformHtml(mapTable, newDisData, prefix, htmlTemp, outHtmlReport)
    

if __name__ == '__main__':
    main(sys.argv)



