#! /usr/bin/python

import sys
from optparse import OptionParser

MIN_COVERAGE_ERROR_VALUE = -2

parser = OptionParser()
parser.disable_interspersed_args()
parser.add_option("--output", "-o", default=None,
                  help="the output file name")
(options, args) = parser.parse_args()
if len(args) == 0:
    parser.print_help()
    raise "bad args"

header = None
coverage = None
maxC = 0
for arg in args:
    file = open(arg)
    line = file.readline()
    if header == None:
        header = line
        fields = header.split(",")
        coverage = [ ]
        for ii in xrange(len(fields)):
            coverage.append({})
    else:
        if header != line:
            raise "header mismatch"
    for line in file:
        fields = line.split(",")
        if len(fields) != len(coverage):
            raise "header does not match data"
        cv = int(fields[0])
        if cv > maxC:
            maxC = cv
        for ii in xrange(1, len(fields)):
            count = int(fields[ii])
            if cv in coverage[ii]:
                coverage[ii][cv] += count
            else:
                coverage[ii][cv] = count

out = sys.stdout
if options.output != None:
    out = open(options.output, "w")
out.write(header)
for ii in xrange(MIN_COVERAGE_ERROR_VALUE, maxC+1):
    hasData = False
    for jj in xrange(1, len(coverage)):
        if ii in coverage[jj] and coverage[jj][ii] != 0:
            hasData = True
    if not hasData:
        continue
    line = "%s" % ii
    for jj in xrange(1, len(coverage)):
        if ii in coverage[jj]:
            line += ",%d" % coverage[jj][ii]
        else:
            line += ",0"
    out.write(line+"\n")

