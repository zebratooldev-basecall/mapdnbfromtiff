#! /usr/bin/env python

import sys
from optparse import OptionParser
from os.path import join as pjoin, split as psplit
from glob import glob
import re

class bundle: pass

def getVersion(gg,ff):
    if ff is None:
        return None
    idx = gg.find('*')
    return ff[idx:-4]

GENOME_NAME_LIST = [ 'atcc-HCC1187/GS000000799-B36-*-ASM',
                     'atcc-HCC1187/GS000000799-B37-*-ASM',
                     'P032/GS00018-DNA_A01-B36-*-ASM',
                     'P032/GS00018-DNA_A01-B37-*-ASM',
                     'P032/GS00022-DNA_A01-B36-*-ASM',
                     'P032/GS00022-DNA_A01-B37-*-ASM',
                     'trio/GS19238-B36-*-ASM',
                     'trio/GS19239-B36-*-ASM',
                     'trio/GS19240-B36-*-ASM',
                     'trio/GS19240-B37-*-ASM',
                     'trio/GS19240B-B37-*-ASM',
                     ]

for gg in GENOME_NAME_LIST:
    if len(glob(gg)) > 1:
        raise Exception('len(glob("%s"))>1' % gg)

GENOME_LIST = []
for gg in GENOME_NAME_LIST:
    if len(glob(gg)) == 1:
        GENOME_LIST.append( (gg, glob(gg)[0]) )
    else:
        GENOME_LIST.append( (gg, None) )
VERSION = getVersion(GENOME_LIST[0][0], GENOME_LIST[0][1])

VERSION = None
for (gg,ff) in GENOME_LIST:
    version = getVersion(gg,ff)
    if version is not None:
        if VERSION is None:
            VERSION = version
        if version != VERSION:
            raise Exception('Version mismatch: %s %s %s' % (VERSION, version, ff))

DVERSION = VERSION.replace('_', '.')

class TrioStats:
    def __init__(self, trioFile):
        self.identical = 'N'
        self.compatible = 'N'
        self.incompatible = 'N'
        ff = open(trioFile)
        try:
            for line in ff:
                line = line.strip()
                if line == '':
                    return
                fields = line.split(',')
                if fields[1] == 'identical':
                    self.identical = int(fields[2])
                if fields[1] == 'compatible':
                    self.compatible = int(fields[2])
                if fields[1] == 'incompatible':
                    self.incompatible = int(fields[2])
        finally:
            ff.close()

class SnpDiffStats:
    def __init__(self, snpDiffFile):
        ff = open(snpDiffFile)
        try:
            thisBlock = False
            for line in ff:
                if line.startswith('Locus concordance by fraction'):
                    thisBlock = True
                if not thisBlock:
                    continue
                if line.startswith('total,'):
                    fields = line.strip().split(',')
                    self.discordance = float(fields[1])
                    self.nocall = float(fields[2])
                    return
        finally:
            ff.close()

class KnownXNovelStats:
    def __init__(self,knownXNovelFile):
        ff = open(knownXNovelFile)
        try:
            for line in ff:
                (self.known,self.novel) = map(int,line.split())
        finally:
            ff.close()

def checkAqc(workDir):
    ff = open(pjoin(workDir, 'WebReports', 'aqc.tsv'))
    failed = []
    for line in ff:
        if '' == line.strip():
            continue
        fields = line[:-1].split('\t')
        if fields[0] == 'name':
            failed = [ 0 ] * (len(fields)-3)
            continue
        if fields[1] not in [ '', '20-40_ccf100' ]:
            continue
        for ii in xrange(3, len(fields)):
            if fields[ii] not in [ 'OK', 'NA', 'NT' ]:
                failed[ii-3] += 1
    ff.close()
    return min(failed)

def checkAllAqc(workDirs):
    for (name,workDir) in workDirs:
        count = 'N'
        try:
            count = checkAqc(workDir)
        except:
            pass
        print ',%s failed AQC metrics,%s' % (name,count)

def checkTimings(workDir):
    ff = open(pjoin(workDir, 'WebReports', 'timings.csv'))
    header = ff.readline()
    (total, user) = (0.0,0.0)
    for line in ff:
        fields = line[:-1].split(',')
        total += float(fields[6])
        if fields[7] != 'Missing':
            user += float(fields[7])
    ff.close()
    return (total,user)

def checkAllTimings(workDirs):
    for (name,workDir) in workDirs:
        (total,user) = ('N','N')
        try:
            (total,user) = checkTimings(workDir)
            total = '%.1f' % total
            user = '%.1f' % user
        except:
            pass
        print ',%s total thread*hours,%s' % (name,total)
        print ',%s user thread*hours,%s' % (name,user)

def getWorkDir(name):
    for (ff,gg) in GENOME_LIST:
        if ff == name:
            return gg
    raise Exception('did not find workDir for name: '+ff)

def checkTrioStats(build, trioVersion):
    name = 'trio/GS19240-%s-*-ASM/%s' % (build,trioVersion)
    dirname = 'trio/GS19240-%s-%s-ASM/%s' % (build,VERSION,trioVersion)
    try:
        tstats = TrioStats(dirname + '/trio_stats_Variations20-40_ccf100.csv')
    except:
        tstats = bundle()
        tstats.identical = 'N'
        tstats.compatible = 'N'
        tstats.incompatible = 'N'
    print ',%s identical,%s' % (name,tstats.identical)
    print ',%s compatible,%s' % (name,tstats.compatible)
    print ',%s incompatible,%s' % (name,tstats.incompatible)

def checkSnpdiffStats(name, id):
    workDir = getWorkDir(name)
    try:
        sstats = SnpDiffStats(workDir + '/snpdiff/snpdiff_%s_stats_Variations20-40_ccf100.csv' % id)
    except:
        sstats = bundle()
        sstats.discordance = 'N'
        sstats.nocall = 'N'
    print ',%s %s discordance,%s' % (name, id, sstats.discordance)
    print ',%s %s nocall,%s' % (name, id, sstats.nocall)

def checkCnvNormalStats(name):
    workDir = getWorkDir(name)
    (cnvCount,cnvBases,cnvCountKnown,cnvBasesKnown) = ('N','N','N','N')
    try:
        ff = open(pjoin(workDir, 'ASM', 'CNVmain', 'Normal', 'cnvQCstats.csv'))
        for line in ff:
            fields = line[:-1].split(',')
            if fields[0] == 'number of called CNVs':
                cnvCount = fields[1]
            if fields[0] == 'total length of called CNVs':
                cnvBases = fields[1]
            if fields[0] == 'fraction of called CNVs similar to known CNVs':
                cnvCountKnown = fields[1]
            if fields[0] == 'fraction of called CNV bases that are in calls similar to known CNVs':
                cnvBasesKnown = fields[1]
        ff.close()
    except:
        pass
    print ',%s cnv count,%s' % (name,cnvCount)
    print ',%s cnv bases,%s' % (name,cnvBases)
    print ',%s cnv count known,%s' % (name,cnvCountKnown)
    print ',%s cnv bases known,%s' % (name,cnvBasesKnown)

def checkCnvTumorStats(name):
    workDir = getWorkDir(name)
    (normalizedNoise, numLevels, numSegsMergedAcrossCtgs) = ('N', 'N', 'N')
    try:
        ff = open(pjoin(workDir, 'ASM', 'CNVmain', 'Tumor', 'copyCalls100000-postReport.csv'))
        for line in ff:
            fields = line[:-1].split(',')
            if fields[0] == 'normalizedNoise':
                normalizedNoise = fields[1]
            if fields[0] == 'numLevels':
                numLevels = fields[1]
            if fields[0] == 'numSegsMergedAcrossCtgs':
                numSegsMergedAcrossCtgs = fields[1]
        ff.close()
    except:
        pass
    print ',%s normalizedNoise,%s' % (name,normalizedNoise)
    print ',%s numLevels,%s' % (name,numLevels)
    print ',%s numSegsMergedAcrossCtgs,%s' % (name,numSegsMergedAcrossCtgs)

def checkSvStats(name):
    workDir = getWorkDir(name)
    (total, known, ic, hcTotal, hcKnown, hcIc, senseKnown, hcNovel) = ( 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N' )
    try:
        ff = open(pjoin(workDir, 'JunctionsAnnotate', 'AnnotatedStats.csv'))
        for line in ff:
            fields = line[:-1].split(',')
            if fields[0] == 'junctionsTotal':
                total = int(fields[1])
            if fields[0] == 'junctionsKnown':
                known = int(fields[1])
            if fields[0] == 'junctionsInterChrom':
                ic = int(fields[1])
        ff.close()
        ff = open(pjoin(workDir, 'JunctionsAnnotate', 'AnnotatedFilteredStats.csv'))
        for line in ff:
            fields = line[:-1].split(',')
            if fields[0] == 'junctionsTotal':
                hcTotal = int(fields[1])
            if fields[0] == 'junctionsKnown':
                hcKnown = int(fields[1])
            if fields[0] == 'junctionsInterChrom':
                hcIc = int(fields[1])
        ff.close()
        senseKnown = hcKnown / float(known)
        hcNovel = 1.0 - hcKnown / float(hcTotal)
    except:
        pass
    print ',%s junctions total,%s' % (name,total)
    print ',%s junctions known,%s' % (name,known)
    print ',%s junctions high conf inter-chrom,%s' % (name,hcIc)
    print ',%s junctions hcKnown/known,%s' % (name,senseKnown)

def checkAnnotations(name):
    workDir = getWorkDir(name)
    keys = [ 'COMPATIBLE', 'DELETE', 'DELETE+', 'DISRUPT',
             'FRAMESHIFT', 'INSERT', 'INSERT+', 'MISSENSE',
             'MISSTART', 'NO-CHANGE', 'NONSENSE', 'NONSTOP',
             'UNKNOWN-INC', 'UNKNOWN-TR', 'UNKNOWN-VNC', ]
    stats = bundle()
    stats.OTHER = 0
    try:
        ff = open(pjoin(workDir, 'qual', 'gene-impact.csv'))
        for line in ff:
            fields = line[:-1].split(',')
            if fields[0] in keys:
                stats.__dict__[fields[0]] = int(fields[1])
            else:
                stats.OTHER += int(fields[1])
        ff.close()
    except:
        pass
    for key in keys:
        val = 'N'
        if key in stats.__dict__:
            val = stats.__dict__[key]
        print ',%s impact %s,%s' % (name, key, val)
    print ',%s impact other,%s' % (name, stats.OTHER)

def checkSomaticVariants(name, somaticOrError):
    workDir = getWorkDir(name)
    categories = [ 'snp', 'ins', 'del', 'sub' ]
    try:
        if somaticOrError == 'count':
            cdDir = glob(name + '/calldiff-v-norm')[0]
        elif somaticOrError == 'error':
            cdDir = glob(name + '/calldiff-v-replicate')[0]
        else:
            raise Exception('unknown option')
        ff = open(pjoin(cdDir, 'SomaticOutput.tsv'))
        hFields = ff.readline().rstrip('\r\n').split('\t')
        
        if hFields[-1] != 'SomaticScore' or hFields[8] != 'varType':
            raise Exception('bad header')
        somatic = {}
        for cat in categories:
            somatic[cat] = 0
        for line in ff:
            fields = line.split('\t')
            score = float(fields[-1])
            if score >= .05:
                somatic[fields[8]] += 1
    except:
        somatic = {}
        for cat in categories:
            somatic[cat] = 'N'
    for cat in categories:
        print ( ',%s somatic (%s) %s at 95%% relative sensitivity,%s' %
                (name,somaticOrError,cat,somatic[cat]) )
        

def checkMappingAqc(name):
    workDir = getWorkDir(name)
    myield = 'N'
    disc = 'N'
    try:
        ff = open(pjoin(workDir, 'WebReports', 'aqc.tsv'))
        for line in ff:
            fields = line[:-1].split('\t')
            if fields[0] == 'Gross mapping yield (Gb)':
                myield = fields[2]
            if fields[0] == 'Overall discordance':
                disc = fields[2]
        ff.close()
    except:
        pass
    print ',%s Gross mapping yield (Gb),%s' % (name,myield)
    print ',%s Overall discordance,%s' % (name,disc)

def checkVarCounts(name, id):
    workDir = getWorkDir(name)
    stats = bundle()
    stats.known = 'N'
    stats.novel = 'N'
    stats.novelRate = 'N'
    try:
        stats = KnownXNovelStats(pjoin(workDir, 'ASM', 'knownXnovel', 'knownXnew.20-40_ccf100.%s' % id))
        stats.novelRate = stats.novel / float(stats.known+stats.novel)
    except:
        pass
    print ',%s known %s,%s' % (name,id,stats.known)
    print ',%s novel %s,%s' % (name,id,stats.novel)
    print ',%s novel rate %s,%s' % (name,id,stats.novelRate)

def main():
    print 'Mapping,,%s' % DVERSION
    for name in [ 'trio/GS19240-B36-*-ASM', 'trio/GS19240-B37-*-ASM',
                  'P032/GS00022-DNA_A01-B36-*-ASM', 'P032/GS00022-DNA_A01-B37-*-ASM' ]:
        checkMappingAqc(name)

    print ''

    print 'AQC,,%s' % DVERSION
    checkAllAqc(GENOME_LIST)

    print ''

    print 'Timings,,%s' % DVERSION
    checkAllTimings(GENOME_LIST)

    print ''

    print 'Trio,,%s' % DVERSION
    checkTrioStats('B36', 'trio-v-baseline')
    checkTrioStats('B36', 'trio-nobaseline')
    checkTrioStats('B37', 'trio-v-baseline')

    print ''

    print 'Snpdiff,,%s' % DVERSION
    checkSnpdiffStats('trio/GS19240-B36-*-ASM', 'hapmap')
    checkSnpdiffStats('trio/GS19240-B36-*-ASM', 'infinium')

    print ''

    print 'Somatic variants,,%s' % DVERSION
    checkSomaticVariants('P032/GS00018-DNA_A01-B36-*-ASM', 'count')
    checkSomaticVariants('P032/GS00018-DNA_A01-B37-*-ASM', 'count')
    checkSomaticVariants('trio/GS19240B-B37-*-ASM', 'error')

    print ''

    print 'VariantCounts,,%s' % DVERSION
    for name in [ 'trio/GS19240-B36-*-ASM', 'trio/GS19240-B37-*-ASM',
                  'P032/GS00022-DNA_A01-B36-*-ASM', 'P032/GS00022-DNA_A01-B37-*-ASM' ]:
        for id in [ 'snp', 'ins', 'del' ]:
            checkVarCounts(name, id)

    print ''

    print 'Annotations,,%s' % DVERSION
    checkAnnotations('trio/GS19240-B36-*-ASM')
    checkAnnotations('trio/GS19240-B37-*-ASM')
    checkAnnotations('P032/GS00022-DNA_A01-B36-*-ASM')
    checkAnnotations('P032/GS00022-DNA_A01-B37-*-ASM')

    print ''

    print 'CNV,,%s' % DVERSION
    checkCnvNormalStats('trio/GS19240-B36-*-ASM')
    checkCnvNormalStats('trio/GS19240-B37-*-ASM')
    checkCnvNormalStats('P032/GS00022-DNA_A01-B36-*-ASM')
    checkCnvNormalStats('P032/GS00022-DNA_A01-B37-*-ASM')
    checkCnvTumorStats('P032/GS00018-DNA_A01-B36-*-ASM')
    checkCnvTumorStats('P032/GS00018-DNA_A01-B37-*-ASM')
    checkCnvTumorStats('atcc-HCC1187/GS000000799-B36-*-ASM')
    checkCnvTumorStats('atcc-HCC1187/GS000000799-B37-*-ASM')

    print ''

    print 'SV,,%s' % DVERSION
    checkSvStats('trio/GS19240-B36-*-ASM')
    checkSvStats('trio/GS19240-B37-*-ASM')
    checkSvStats('P032/GS00022-DNA_A01-B36-*-ASM')
    checkSvStats('P032/GS00022-DNA_A01-B37-*-ASM')
    checkSvStats('P032/GS00018-DNA_A01-B36-*-ASM')
    checkSvStats('P032/GS00018-DNA_A01-B37-*-ASM')
    checkSvStats('atcc-HCC1187/GS000000799-B36-*-ASM')
    checkSvStats('atcc-HCC1187/GS000000799-B37-*-ASM')

if __name__ == '__main__':
    main()
