#! /usr/bin/python

import sys
import xml.parsers.expat

path = []
mgDist = []
gaps = []
leftGaps = []
rightGaps = []
histogramMin = -1
data = ""

def initNewFile():
    global mgDist
    global gaps
    global leftGaps
    global rightGaps
    global histogramMin
    global data

    mgDist = []
    gaps = []
    leftGaps = []
    rightGaps = []
    histogramMin = -1
    data = ""

# 3 handler functions
def start_element(name, attrs):
    global path
    global data
    data = ""
    path.append(name)

def end_element(name):
    global mgDist
    global gaps
    global leftGaps
    global rightGaps
    global histogramMin
    global path
    global data
    mypath = str("/".join(path[1:]))
    if "mateGapDistribution/histogram_/item" == mypath:
        mgDist.append(float(data))
    elif "leftGapPairDistribution/frequencies_/item/first/gap" == mypath:
        gaps.append(int(data))
    elif "rightGapPairDistribution/frequencies_/item/first/gap" == mypath:
        gaps.append(int(data))
    elif "leftGapPairDistribution/frequencies_/item/second" == mypath:
        leftGaps.append( (gaps,float(data)) )
        gaps = []
    elif "rightGapPairDistribution/frequencies_/item/second" == mypath:
        # gaps are in order from clone end toward mate gap
        gaps.reverse()
        rightGaps.append( (gaps,float(data)) )
        gaps = []
    elif "mateGapDistribution/histogramMin_" == mypath:
        histogramMin = int(data)
    data = ""
    path.pop()

def char_data(thisData):
    global data

    data += str(thisData)

def writeSmallGaps(fn,gaps):
    outfile = open(fn, "w")
    gapCount = len(gaps[0][0])
    gaps.sort()
    header = "sequence:0-0 firstGap:0 gapCount:%d" % gapCount
    data = ""
    for (gt,freq) in gaps:
        header += ",gaps:%s" % (" ".join(map(str,gt)))
        data += ",%.8e" % freq
    outfile.write(header+"\n")
    outfile.write(data+"\n")
    outfile.close()


for arg in sys.argv[1:]:
    print "converting %s..." % arg
    initNewFile()

    p = xml.parsers.expat.ParserCreate()

    p.StartElementHandler = start_element
    p.EndElementHandler = end_element
    p.CharacterDataHandler = char_data

    infile = open(arg)
    p.ParseFile(infile)
    infile.close()

    # Write mate gap file.
    bn = arg
    if bn.endswith("-gaps.xml"):
        bn = bn[:-9]
    outfile = open(bn+"-gaps-M.csv","w")
    outfile.write("mg,freq\n")
    for ii in xrange(len(mgDist)):
        outfile.write("%d,%.8e\n" % (histogramMin+ii,mgDist[ii]))
    outfile.close()

    # Write small gaps files.
    writeSmallGaps(bn+"-gaps-L0.csv",leftGaps)
    writeSmallGaps(bn+"-gaps-R0.csv",rightGaps)

    # Write discordance file.
    infile = open(arg)
    outfile = open(bn+"-scoredist.xml","w")
    for line in infile:
        outfile.write(line)
        if line.strip() == "</scoreDistribution>":
            break
    outfile.write("</boost_serialization>\n")
    outfile.close()
    infile.close()
