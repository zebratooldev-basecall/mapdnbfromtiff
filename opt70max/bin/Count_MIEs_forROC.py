#!/usr/bin/env python

import os
import sys
import re
import getopt

##############################################################
def PrintDictionary(Dict, OUTFILE, DictName):
    outf = open( re.sub("[.]tsv", "", OUTFILE) + "_" + DictName + ".summary", 'w')
    for mykey in sorted(Dict.keys()):
        outf.write( "\t".join( [ str(x) for x in list(mykey) ] + [str(Dict[mykey])] ) + "\n")
        
##############################################################

def ExtractScores( format, data ):

    flag = 99999
    f = format.split(":")
    d = data.split(":")
    VAF = [ d[i] for i in range(len(d)) if f[i]=="HQ"  ][0].split(",")
    EAF = [ d[i] for i in range(len(d)) if f[i]=="EHQ" ][0].split(",")
    if EAF[0]==".": EAF0 = flag
    else:           EAF0 = int(EAF[0])
    if EAF[1]==".": EAF1 = flag
    else:           EAF1 = int(EAF[1])
    if VAF[0]==".": VAF0 = flag
    else:           VAF0 = int(VAF[0])
    if VAF[1]==".": VAF1 = flag
    else:           VAF1 = int(VAF[1])

    return ( EAF0, EAF1, VAF0, VAF1 )

##############################################################

def main():

    DEBUG = False
#    DEBUG = True

    # parse command line options and update variables
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:", [])
    except getopt.GetoptError:
        sys.exit(usage)

    for opt, arg in opts:
        if opt == "-h":
            sys.exit('Use me properly.')
        elif opt == '-i':
            INFILE = arg
           
    inf = open(INFILE, 'r')
    prevpos = ""
    prevoutline = ""
    VAFdict = {}
    EAFdict = {}
    
    while True:
        line1 = inf.readline()
        line2 = inf.readline()
        if '' == line1: break;
        if '' == line2: sys.exit("ERROR: Unexpected exit with an unpaired line already read in:\n" + line1)

        ## FILTER
        ## Don't want lines where the parents aren't fully called
        if "HasNoCalls=True" in line1: continue

        ## Parse out data !!!!!!!!!!!!!!!!!! this assumes an order: kid, mom, dad
        (allele1, variantID1, variant1, zygosity1, varType1, VAFscore1, EAFscore1, HasNoCalls1, Concordance1, chr1, pos1, ID1, ref1, alt1, qual1, filter1, info1, format1, kid1, mom1, dad1) = line1.split()
        (allele2, variantID2, variant2, zygosity2, varType2, VAFscore2, EAFscore2, HasNoCalls2, Concordance2, chr2, pos2, ID2, ref2, alt2, qual2, filter2, info2, format2, kid2, mom2, dad2) = line2.split()
        kid = re.sub("[|/]", "", re.sub(":.*", "", kid1))
        mom = re.sub("[|/]", "", re.sub(":.*", "", mom1))
        dad = re.sub("[|/]", "", re.sub(":.*", "", dad1))

        ## FILTER
        ## Want the parents to be hom-ref
        ## THIS DONESN'T WORK BECAUSE THERE WILL BE NO CONCORDANT CALLS (CHILD HAS TO ALSO BE HOM-REF, THIS ISN'T A VARIANT CALL AND DOSN'T HAVE A VARIANT SCORE AND ISN'T RECORDED IN THE VAR FILE)
        ##        if mom != "00": continue
        ##        if dad != "00": continue

        ## FILTER
        ## Want the kid to be fully called
        if zygosity1 == "half": continue

        ## Determine parameters
        ## TYPE
        types = re.sub(".*TYPE=","", info1)
        
        if zygosity1=="hom":
            vType = "_".join( sorted(set( [ re.sub("complex","sub", re.sub("mnp","sub",x)) for x in [varType1, varType2] if x != "ref" ] )) )
            zyg = "hom-"
        elif "het" in zygosity1:
            vType = "_".join( sorted(     [ re.sub("complex","sub", re.sub("mnp","sub",x)) for x in [varType1, varType2] if x != "ref" ] )  )
            zyg = "het-"
        else:
            sys.exit("ERROR: I shouldn't have any half calls!!!")
        ## I could also try to determine a secondary type here, but I won't since I won't use it anyway !!!!!
        if vType=="": vType = "ref"
        Type = zyg + vType
        
        ## CORDANCE
        if Concordance1=="BAD" or Concordance2=="BAD" or Concordance1=="bad" or Concordance2=="bad":
            Cordance = "BAD"
        else:
            Cordance = "GOOD"

        ## SCORES
        ## It doesn't matter which allele is bad, it just matters when this whole locus is no longer fully called, in parents and child
        ## you could maybe argue to not take the parents into account and to hold them as "fixed" with normal PASS filters !!!!!!!!!!
        ## you could also argue that if the GOOD allele is no-called at a lower score this doesn't matter because it's not the relevant *variant* score !!!!!!
        ## it's completely unclear from the documentation what it means to have a varScore on a ref call -- it says ref calls don't have varScores, but ref calls opposite variant calls *do* have a var score
        ( kEAFscore1, kEAFscore2, kVAFscore1, kVAFscore2 ) = ExtractScores( format1, kid1 )
        ( mEAFscore1, mEAFscore2, mVAFscore1, mVAFscore2 ) = ExtractScores( format1, mom1 )
        ( dEAFscore1, dEAFscore2, dVAFscore1, dVAFscore2 ) = ExtractScores( format1, dad1 )

        EAFscore = min( kEAFscore1, kEAFscore2, mEAFscore1, mEAFscore2, dEAFscore1, dEAFscore2)
        VAFscore = min( kVAFscore1, kVAFscore2, mVAFscore1, mVAFscore2, dVAFscore1, dVAFscore2)

        if DEBUG:
#        if "," in types:
#        if DEBUG and "ref" in Type:
            print line1
            print kVAFscore1, kEAFscore1, mVAFscore1, mEAFscore1, dVAFscore1, dEAFscore1
            print kVAFscore2, kEAFscore2, mVAFscore2, mEAFscore2, dVAFscore2, dEAFscore2
            print EAFscore, VAFscore
            print Type
                                    
        VAFdict[ (Type, Cordance, VAFscore) ] = VAFdict.get( (Type, Cordance, VAFscore), 0 ) + 1
        EAFdict[ (Type, Cordance, EAFscore) ] = EAFdict.get( (Type, Cordance, EAFscore), 0 ) + 1

    OUTFILE = re.sub("[.]vcf", "", INFILE)
    PrintDictionary(VAFdict, OUTFILE, "VAF")
    PrintDictionary(EAFdict, OUTFILE, "EAF")

    if DEBUG:
        print sorted(set([x for (x,y,z) in VAFdict.keys()]))

##############################################################
    
if __name__ == "__main__":
    main()

            
