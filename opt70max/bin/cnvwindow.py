#! /usr/bin/env python
from gbifile import GbiContig
from gbifile import GbiFile 
from refconstants import GlobalRefConstants

class ChrRange(object):
    def __init__(self,chr,begin,end):
        self.chr_ = chr
        self.begin_ = begin
        self.end_ = end

class ContigRange(object):
    def __init__(self,contig,begin,end):
        self.ctg_ = contig
        self.begin_ = begin
        self.end_ = end
        
class WholeGenomeCNVWindowIterator(object):
    def initialize(self,gbi,ignoreRanges,shiftSize,width):

        self.shift_ = shiftSize
        self.width_ = width

        # reformat ignore ranges into a simple list
        igRange = []
        i = 0
        for k in sorted(ignoreRanges.iterkeys()):
            igRange += [ ContigRange(int(k),int(ignoreRanges[k][0]),int(ignoreRanges[k][1])) ]
        nIR = len(igRange)
        
        self.contigs_ = []
        i = 0
        for c in gbi.contigs:
            while i < nIR and igRange[i].ctg_  < c.contig:
                i += 1
            if i == nIR or c.contig < igRange[i].ctg_:
                self.contigs_ += [ ChrRange(c.supername,c.offset,c.offset+c.length)]
            else:
                circ = "no"
                if c.circ:
                    circ = "yes"
                dlen = igRange[i].begin_ - c.offset
                d = ChrRange(c.supername,c.offset,igRange[i].begin_)
                e = ChrRange(c.supername,igRange[i].end_,c.offset+c.length)
                self.contigs_ += [d,e]

        self.numCtgChunks_ = len(self.contigs_)
        self.currChunk_ = 0
        self.currWindowBegin_ = -1
        self.currWindowEnd_ = -1
        self.currChr_ = "NULL"
        self.eof_ = False
        self.currentExtended_ = False

    def __init__(self):
        self.eof_ = True

    def computeNextWindowBegin(self):
        if self.currWindowBegin_ < 0:
            self.currChr_ = self.contigs_[self.currChunk_].chr_
            self.currWindowBegin_ = self.contigs_[self.currChunk_].begin_
        else:
            self.currWindowBegin_ = self.currWindowBegin_ + self.shift_ - (self.currWindowBegin_ % self.shift_)
            if self.currentExtended_:
                self.currWindowBegin_ += self.shift_
        return self.currWindowBegin_

    def computeWindowEnd(self):
        self.currWindowEnd_ = self.currWindowBegin_ - self.currWindowBegin_%self.shift_ + self.width_
        self.currentExtended_ = False
        if self.currWindowEnd_ - self.currWindowBegin_ < self.width_/2 :
            self.currWindowEnd_ += self.shift_
            self.currentExtended_ = True
        if self.contigs_[self.currChunk_].end_ - self.currWindowEnd_ < self.shift_/2:
            self.currWindowEnd_ = self.contigs_[self.currChunk_].end_
            self.currWindowBegin_ = -1
            self.currChunk_ += 1
            if self.currChunk_ >= self.numCtgChunks_:
                self.eof_ = True
        return self.currWindowEnd_

    def eof(self):
        return self.eof_
        
    def next(self):
        b = self.computeNextWindowBegin()
        e = self.computeWindowEnd()
        return ChrRange(self.currChr_,b,e)
        

if __name__ == '__main__':
    sampleType = "female"
    referenceRootDir = "/prod/pv-01/pipeline/REF"
    globalRefConstants = GlobalRefConstants(referenceRootDir)
    refConstants = globalRefConstants[sampleType]
    gbifilename = referenceRootDir + "/" + refConstants.refId + "/reference.gbi"
    gbi = GbiFile(gbifilename)
    cnvWins = WholeGenomeCNVWindowIterator()
    cnvWins.initialize(gbi,refConstants.cnvIgnoreRegions,100000,100000)
    while not cnvWins.eof():
        win = cnvWins.next()
        print win.chr_ + "\t" + str(win.begin_) + "\t" + str(win.end_)

