#!/usr/bin/env python

import sys

tag_matches = [
    'tag_matches.txt']
    #'cPal-10-1-10-tag-matches.txt',
    #'direct-2-12-11-tag-matches.txt',
    #'direct-2-12-13-tag-matches.txt']
    #'direct_1_2_21-24_tag_matches.txt',
    #'direct_1_2_22-23_tag_matches.txt',
    #'direct_1_2_22-24_tag_matches.txt',
    #'direct_1_3_tag_matches.txt',
    #'direct_1_4_tag_matches.txt',
    #'direct_1_5_tag_matches.txt',
    #'direct_2_2_25-26_tag_matches.txt']

def get_mismatch_positions(called_seq,ref_seq):
    pos_called = [0] * len(called_seq)
    pos_errors = [0] * len(called_seq)
    
    for x in range(len(called_seq)):
        if called_seq != "N":
            pos_called[x] = 1
            if called_seq[x] != ref_seq[x]:
                pos_errors[x] = 1

    return pos_called,pos_errors


def summarize_tag_match_file(file_path,sw):
    dnb_count,no_call_count,no_match_count,match_count = 0,0,0,0
    set_match_counts = {}
    tag_match_counts = {}
    mismatch_counts = {"0":0,"1":0}
    position_called = [0]*10
    position_errors = [0]*10

    sr = open(file_path,'r')
    sr.readline()
    for line in sr:
        fields = line.rstrip('\r\n').split('\t')
        # advance counters
        dnb_count += 1
        if fields[1] == "N": no_call_count += 1
        if fields[1] == "NA": no_match_count += 1

        # count tag sets, tags, and mis-matches.
        if not set_match_counts.has_key(fields[1]):
            set_match_counts[fields[1]] = 0
        set_match_counts[fields[1]] += 1
        if not tag_match_counts.has_key(fields[3]):
            tag_match_counts[fields[3]] = 0
        tag_match_counts[fields[3]] += 1
        if fields[1] != "N" and fields[1] != "NA" and len(fields[4])>0:
            match_count += 1
            if not mismatch_counts.has_key(fields[5]):
                mismatch_counts[fields[5]] = 0
            mismatch_counts[fields[5]] += 1
            pos_called,pos_errors = get_mismatch_positions(fields[0],fields[4])
            for x in range(len(pos_called)):
                position_called[x] += pos_called[x]
                position_errors[x] += pos_errors[x]

    sr.close()

    # write summary to file.
    sw.write('###########################################################################\n')   
    sw.write('# tag match stats\n')
    sw.write('tag_matches_file,%(file_path)s\n' % vars())
    sw.write('dnb_count,%(dnb_count)s\n' % vars())
    sw.write('no_call_count,%(no_call_count)s\n' % vars())
    sw.write('no_match_count,%(no_match_count)s\n' % vars())
    sw.write('match_count,%(match_count)s\n' % vars())
    sw.write('exact_match_count,%s\n\n' % mismatch_counts["0"])

    # write tag set counts to file.
    sw.write('# tag set match counts\n')
    sw.write('#set_id,count\n')
    for set_id in sorted(set_match_counts.keys()):
        sw.write('%s,%s\n' % (set_id,set_match_counts[set_id]))
    sw.write('\n')

    # write tag counts to file.
    sw.write('# tag match counts\n')
    sw.write('#tag_id,count\n')
    for tag_id in sorted(tag_match_counts.keys()):
        sw.write('%s,%s\n' % (tag_id,tag_match_counts[tag_id]))
    sw.write('\n')

    # write positional error counts.
    sw.write('# positional errors\n')
    sw.write('#position,called,error\n')
    for x in range(len(position_called)):
        sw.write('%s,%s,%s\n' % (x+1,position_called[x],position_errors[x]))
    sw.write('\n')

def compare_tag_match_files(file_path_1,file_path_2,sw):
    results = {}

    # open stream readers
    sr1 = open(file_path_1,'r')
    sr2 = open(file_path_2,'r')

    # deal with header lines.
    sr1.readline(); sr2.readline()

    # loop through files, index, and update count
    while True:
        line1 = sr1.readline()
        line2 = sr2.readline()

        if not line1 or not line2:
            break

        fields1 = line1.rstrip('\r\n').split('\t')
        fields2 = line2.rstrip('\r\n').split('\t')

        if not results.has_key(fields1[3]):
            results[fields1[3]] = {}
        if not results[fields1[3]].has_key(fields2[3]):
            results[fields1[3]][fields2[3]] = 0

        results[fields1[3]][fields2[3]] += 1

    # close input files.
    sr1.close()
    sr2.close()

    # write results to file.
    sw.write('###########################################################################\n')
    sw.write('# results of comparing two tag matches\n')
    sw.write('# file 1: %s\n' % file_path_1)
    sw.write('# file 2: %s\n' % file_path_2)
    for key1,results1 in results.items():
        for key2,results2 in results1.items():
            count = results[key1][key2]
            sw.write('%(key1)s,%(key2)s,%(count)s\n'%vars())
    sw.write('\n')

# main code section.
slidelane = sys.argv[1]

# open output.
sw = open('tag_match_stats.csv','w')
sw.write('# tag match stats for %s\n\n' % slidelane)

# loop through hard coded tag match files.
for tag_match_base in tag_matches:
    tag_match_file = '%s_%s' % (slidelane,tag_match_base)
    summarize_tag_match_file(tag_match_file,sw)

# compare tag match files, two at a time.
#compare_tag_match_files('%s_%s' % (slidelane,tag_matches[0]),'%s_%s' % (slidelane,tag_matches[1]),sw)
#compare_tag_match_files('%s_%s' % (slidelane,tag_matches[0]),'%s_%s' % (slidelane,tag_matches[2]),sw)
#compare_tag_match_files('%s_%s' % (slidelane,'direct_1_2_21-23_tag_matches.txt'),'%s_%s' % (slidelane,'direct_1_2_22-23_tag_matches.txt'),sw)
#compare_tag_match_files('%s_%s' % (slidelane,'direct_1_2_21-23_tag_matches.txt'),'%s_%s' % (slidelane,'direct_1_2_22-24_tag_matches.txt'),sw)
#compare_tag_match_files('%s_%s' % (slidelane,'direct_1_2_21-23_tag_matches.txt'),'%s_%s' % (slidelane,'direct_2_2_25-26_tag_matches.txt'),sw)
#compare_tag_match_files('%s_%s' % (slidelane,'cPal_1_10_tag_matches.txt'),'%s_%s' % (slidelane,'direct_1_2_21-24_tag_matches.txt'),sw)
#compare_tag_match_files('%s_%s' % (slidelane,'cPal_1_10_tag_matches.txt'),'%s_%s' % (slidelane,'direct_1_3_tag_matches.txt'),sw)
#compare_tag_match_files('%s_%s' % (slidelane,'cPal_1_10_tag_matches.txt'),'%s_%s' % (slidelane,'direct_1_4_tag_matches.txt'),sw)
#compare_tag_match_files('%s_%s' % (slidelane,'cPal_1_10_tag_matches.txt'),'%s_%s' % (slidelane,'direct_1_5_tag_matches.txt'),sw)

# close output.
sw.close()

