#!/usr/bin/env python

import sys
from subprocess import Popen, PIPE


def zopen(fn):
    return Popen([ 'zcat', fn ], stdout=PIPE).stdout


def processNanocallTsv(fn):
    ff = zopen(fn)

    header = ff.readline()[:-1]
    hFields = header.split('\t')

    # NOTE: This is crude but temporary. Files will be named differently in 2nd revision of this.
    if 'Mirage DNBs' in hFields:
        return processSingleCycleAdfReport(ff, hFields)
    else:
        return processMultiCycleAdfReport(ff, hFields)


def processMultiCycleAdfReport(ff, hFields):
    idxSlideName = hFields.index('slideName')
    idxFieldName = hFields.index('fieldName')
    idxDnbPos = hFields.index('DNB position')
    idxTotDnbs = hFields.index('Total DNBs')
    idxHighConcNeigh = hFields.index('High Concordance Neighbors')
    idxPcA = hFields.index('% Colored A')
    idxPcC = hFields.index('% Colored C')
    idxPcG = hFields.index('% Colored G')
    idxPcT = hFields.index('% Colored T')
    idxPc  = hFields.index('% Colored Total')

    totDnbs = 0
    totMirages = 0
    lane = None
    field = None

    pcCounts = {}
    pcSum = {}

    for line in ff:
        tokens = line.split('\t')

        if tokens[idxFieldName] != field:
            field = tokens[idxFieldName]
            if lane is None:
                lane = tokens[idxSlideName] + '-' + field[0:3]
            else:
                assert lane == tokens[idxSlideName] + '-' + field[0:3],\
                    "Unsupported mixed lanes in " + fn

            totDnbs += int(tokens[idxTotDnbs])
            totMirages += int(tokens[idxHighConcNeigh])

        position = int(tokens[idxDnbPos])
        ( pcA, pcC, pcG, pcT, pcColored) = \
            map(float, [ tokens[idxPcA], tokens[idxPcC], tokens[idxPcG], tokens[idxPcT], tokens[idxPc] ])

        if position not in pcCounts:
            pcCounts[position] = 0
            pcSum[position] = [ 0, 0, 0, 0, 0 ]

        pcCounts[position] += 1
        pcSum[position][0] += pcA
        pcSum[position][1] += pcC
        pcSum[position][2] += pcG
        pcSum[position][3] += pcT
        pcSum[position][4] += pcColored

    avgPctColoredByPosBase = {}
    for position in pcCounts:
        count = pcCounts[position]
        ( sumA, sumC, sumG, sumT, sumTot ) = pcSum[position]
        avgPctColoredByPosBase[position] = \
            ( sumA / count, sumC / count, sumG / count, sumT / count, sumTot / count)

    return ( lane, totDnbs, totMirages, avgPctColoredByPosBase )


def processSingleCycleAdfReport(ff, hFields):
    idxSlideName = hFields.index('slideName')
    idxFieldName = hFields.index('fieldName')
    idxTotDnbs = hFields.index('Total DNBs')
    idxMirageDnbs = hFields.index('Mirage DNBs')

    totDnbs = 0
    totMirages = 0
    lane = None
    field = None

    for line in ff:
        tokens = line.split('\t')

        if tokens[idxFieldName] != field:
            field = tokens[idxFieldName]
            if lane is None:
                lane = tokens[idxSlideName] + '-' + field[0:3]
            else:
                assert lane == tokens[idxSlideName] + '-' + field[0:3],\
                    "Unsupported mixed lanes in " + fn

            totDnbs += int(tokens[idxTotDnbs])
            totMirages += int(tokens[idxMirageDnbs])

    return ( lane, totDnbs, totMirages, None )


print 'freeform,adfLaneMetrics'
print 'laneName,totalDNBs,highConcordanceNeighbors'

pctColoredList = []
for fn in sys.argv[1:]:
    (lane, totDnbs, totMirages, avgPctColoredByPosBase) = processNanocallTsv(fn)
    if lane:
        print '%s,%d,%d' % ( lane, totDnbs, totMirages )
        if avgPctColoredByPosBase is not None:
            pctColoredList.append(( lane, avgPctColoredByPosBase ))

print
print 'freeform,adfLanePosMetrics'
print 'laneName,Position,Avg_Pct_Colored_A,Avg_Pct_Colored_C,Avg_Pct_Colored_G,Avg_Pct_Colored_T,Avg_Pct_Colored_Total'

if pctColoredList:
    for lane, avgPctColoredByPosBase in pctColoredList:
        for position in sorted(avgPctColoredByPosBase.keys()):
            ( pA, pC, pG, pT, pTot ) = avgPctColoredByPosBase[position]
            print '%s,%s,%.3f,%.3f,%.3f,%.3f,%.3f' % ( lane, position, pA, pC, pG, pT, pTot )
