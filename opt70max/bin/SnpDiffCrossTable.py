#! /usr/bin/env python

import sys
from os.path import split as psplit, join as pjoin

COUNTER_MODS = [ "allele",
                 "locus" ]

SNP_SETS = [ ("infinium","infinium_"),
             ("hapmap",""),
             ]

MEASURES = [ "nocall",
             "discordance" ]

LOCUS_CLASSIFICATIONS = [ "ref-ref",
                          "het-ref-alt",
                          "het-alt-alt",
                          "hom-alt-alt",
                          "total" ]

CALL_FILES = sys.argv[1:]

def getFractionOfTotal(result,mod,snpSetDN,measure,locusClass):
    val = None
    total = None
    for (mm,ss,ms,lc,data) in result:
        if mm == mod and ss == snpSetDN and ms == measure and lc == locusClass:
            return "%.5f%%" % (100.0 * float(data))
    raise Exception("data not found for %s,%s,%s,%s" % (mod,snpSetDN,measure,locusClass))


results = []
for callFile in CALL_FILES:
    result = []
    for (snpSetDN,snpSetFN) in SNP_SETS:
        (dir,cf) = psplit(callFile)
        fn = pjoin(dir,"snpdiff_%sstats_%s" % (snpSetFN,cf))
        ff = open(fn)
        headerFields = []
        mod = None
        for line in ff:
            line = line.strip()
            if line == "":
                mod = None
                continue
            if line.startswith("Locus concordance by fraction"):
                mod = "locus"
                continue
            if line.startswith("Allele concordance by fraction"):
                mod = "allele"
                continue
            if mod is None:
                continue
            if line.startswith(","):
                headerFields = line.split(",")
                continue
            fields = line.split(",")
            for ii in xrange(1, len(fields)):
                result.append( (mod,snpSetDN,headerFields[ii],fields[0],fields[ii]) )
        ff.close()
    results.append(result)


header = "By,SnpSet,Measure,SnpSetClass"
for callFile in CALL_FILES:
    header += ",%s" % callFile
print header
for mod in COUNTER_MODS:
    for (snpSetDN,snpSetFN) in SNP_SETS:
        for measure in MEASURES:
            for locusClass in LOCUS_CLASSIFICATIONS:
                line = "%s,%s,%s,%s" % (mod,snpSetDN,measure,locusClass)
                for ii in xrange(len(CALL_FILES)):
                    line += ",%s" % getFractionOfTotal(results[ii],mod,snpSetDN,measure,locusClass)
                print line
