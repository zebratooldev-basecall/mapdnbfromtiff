#!/usr/bin/env python
import os,sys
import optparse
import time
import re
import string

import csvparser
import cgatools.util.delimitedfile
from asmrptutil import getTableInt, getTableFloat, getDictValueOr0, floatDiv
from cgiutils import bundle


def sumCountByAttrFilter(data, attrs):
    result = 0
    for key in data:
        passesFilter = True
        for ii in xrange(len(attrs)):
            if ( attrs[ii] is not None ) and key[ii] != attrs[ii]:
                passesFilter = False
                break
        if passesFilter:
            result += data[key]
    return result


def genSomaticVarStats(rptFile, somaticMasterVarFn):

    masterVarFile = cgatools.util.delimitedfile.DelimitedFile(somaticMasterVarFn)
    masterVarFile.addField("reference")
    masterVarFile.addField("allele1Seq")
    masterVarFile.addField("allele2Seq")
    masterVarFile.addField("allele1XRef")
    masterVarFile.addField("allele2XRef")
    if masterVarFile.hasField("allele1ReadCount-N1"):
        masterVarFile.addField("allele1ReadCount-N1")
    else:
        masterVarFile.addField("allele1ReadCount-T1")
    if masterVarFile.hasField("allele2ReadCount-N1"):
        masterVarFile.addField("allele2ReadCount-N1")
    else:
        masterVarFile.addField("allele2ReadCount-T1")
    if masterVarFile.hasField("totalReadCount-N1"):
        masterVarFile.addField("totalReadCount-N1")
    else:
        masterVarFile.addField("totalReadCount-T1")
    masterVarFile.addField("somaticCategory")
    masterVarFile.addField("allele1VarFilter")
    masterVarFile.addField("allele2VarFilter")

    counts = {}
    VT = [ 'SNP', 'INS', 'DEL', 'SUB' ]

    for (reference,
         allele1Seq,
         allele2Seq,
         allele1XRef,
         allele2XRef,
         allele1ReadCount,
         allele2ReadCount,
         totalReadCount,
         somaticCategory,
         allele1VarFilter,
         allele2VarFilter) in masterVarFile:
        if (allele1Seq != reference) or (allele2Seq != reference):
            if somaticCategory != '':
                somCat = somaticCategory.upper()
                if somCat not in VT:
                    somCat = 'OTHER'
                known = (allele1XRef != "" or allele2XRef != "") and 'KNOWN' or ''
                highConf = (string.find(allele1VarFilter,"FET")==-1 and string.find(allele2VarFilter,"FET")==-1) and 'HIGH-CONF' or ''
                readCount = (allele1Seq != reference) and allele1ReadCount or allele2ReadCount
                readSupport = \
                    ((int(readCount) > 2) and (floatDiv(readCount, totalReadCount) > .05)) and \
                    'READ-SUPP' or ''
                attrs = ( known, highConf, readSupport, somCat )
                if attrs not in counts:
                    counts[attrs] = 0
                counts[attrs] += 1

    stats = []

    for varCat in VT + [ 'OTHER' ]:
        stats += [
            ( "Somatic %s count" % varCat,
              sumCountByAttrFilter(counts, ( None, None, None, varCat )) ),
            ( "High confidence somatic %s count" % varCat,
              sumCountByAttrFilter(counts, ( None, 'HIGH-CONF', None, varCat )) ),
            ( "Known somatic %s count" % varCat,
              sumCountByAttrFilter(counts, ( 'KNOWN', None, None, varCat )) ),
            ( "Known high confidence somatic %s count" % varCat,
              sumCountByAttrFilter(counts, ( 'KNOWN', 'HIGH-CONF', None, varCat )) ),
            ( "Count of somatic %s with read support in the normal > 2 and > 5%%" % varCat,
              sumCountByAttrFilter(counts, ( None, None, 'READ-SUPP', varCat )) ),
            ( "Count of high confidence somatic %s with read support in the normal > 2 and > 5%%" % varCat,
              sumCountByAttrFilter(counts, ( None, 'HIGH-CONF', 'READ-SUPP', varCat )) ),
        ]

    print >>rptFile, "freeform,somaticVarStats"
    for name, value in stats:
        print >>rptFile, "%s,%s" % (name, value)

def genSomaticCnvStats(rptFile, copyCallsRptFn, multiCopyCallsRptFn,
                       cnvQcStatsDipFn,cnvQcStatsNonDipFn,
                       multiCnvQcStatsDipFn, multiCnvQcStatsNonDipFn):
    single = csvparser.Data()
    singleDip = csvparser.Data()
    singleNonDip = csvparser.Data()
    multi = csvparser.Data()
    multiDip = csvparser.Data()
    multiNonDip = csvparser.Data()
    csvparser.loadFile(copyCallsRptFn, single)
    csvparser.loadFile(cnvQcStatsDipFn, singleDip),
    csvparser.loadFile(cnvQcStatsNonDipFn, singleNonDip),
    csvparser.loadFile(multiCopyCallsRptFn, multi)
    csvparser.loadFile(multiCnvQcStatsDipFn, multiDip)
    csvparser.loadFile(multiCnvQcStatsNonDipFn, multiNonDip)

    stats = [
        ( "Nondiploid levels",
            getTableInt(multi.cnvTumorStats100000, 'numLevels', 1) ),
        ( "Change in nondiploid levels vs. tumor-only analysis",
            getTableInt(multi.cnvTumorStats100000, 'numLevels', 1) - \
                getTableInt(single.cnvTumorStats100000, 'numLevels', 1) ),
        ( "Nondiploid segments",
            getTableInt(multiNonDip.CNV_QC_stats, 'number of called CNVs', 1) ),
        ( "Change in nondiploid segments vs. tumor-only analysis",
            getTableInt(multiNonDip.CNV_QC_stats, 'number of called CNVs', 1) - \
                getTableInt(singleNonDip.CNV_QC_stats, 'number of called CNVs', 1) ),
        ( "Diploid CNV segments",
            getTableInt(multiDip.CNV_QC_stats, 'number of called CNVs', 1) ),
        ( "Change in diploid CNV segments vs. tumor-only analysis",
            getTableInt(multiDip.CNV_QC_stats, 'number of called CNVs', 1) - \
                getTableInt(singleDip.CNV_QC_stats, 'number of called CNVs', 1) ),
        ( "Diploid CNV bases",
            getTableInt(multiDip.CNV_QC_stats, 'total length of called CNVs', 1) ),
        ( "Change in diploid CNV bases vs. tumor-only analysis",
            getTableInt(multiDip.CNV_QC_stats, 'total length of called CNVs', 1) - \
                getTableInt(singleDip.CNV_QC_stats, 'total length of called CNVs', 1) ),
    ]

    print >>rptFile, "freeform,somaticCnvStats"
    for name, value in stats:
        print >>rptFile, "%s,%s" % (name, value)

def genSomaticSvStats(rptFile, somaticAllJunctionsFn, somaticHighJunctionsFn, allSvEventsFn):
    stats = []

    allJctsCntPerEventId = {}

    svEventsFile = cgatools.util.delimitedfile.DelimitedFile(allSvEventsFn)
    svEventsFile.addField("EventId")
    svEventsFile.addField("RelatedJunctionIds", conv=lambda lst: len(lst.split(";")))

    for (eventId, relatedJctsCnt) in svEventsFile:
        allJctsCntPerEventId[eventId] = relatedJctsCnt

    svEventsFile.close()

    for (junctionsFn, confidence) in [ (somaticAllJunctionsFn, " "),
                                       (somaticHighJunctionsFn, " high confidence ") ]:
        somJctCnt = 0
        somJctInBaselineCnt = 0
        somJctKnownCnt = 0
        somJctMeiConsistentCnt = 0
        somJctLowScoredCnt = 0
        somJctsCntPerEventId = {}

        junctionsFile = cgatools.util.delimitedfile.DelimitedFile(junctionsFn)
        junctionsFile.addField("DiscordantMatePairAlignments")
        junctionsFile.addField("XRef")
        junctionsFile.addField("DeletedTransposableElement")
        junctionsFile.addField("FrequencyInBaselineGenomeSet")
        junctionsFile.addField("EventId")

        for (discordantMatePairAlignments,
             xRef,
             deletedTransposableElement,
             frequencyInBaselineGenomeSet,
             eventId) in junctionsFile:

            somJctCnt += 1
            if float(frequencyInBaselineGenomeSet) != 0:
                somJctInBaselineCnt += 1
            if xRef != "":
                somJctKnownCnt += 1
            if deletedTransposableElement != "":
                somJctMeiConsistentCnt += 1
            if int(discordantMatePairAlignments) < 15:
                somJctLowScoredCnt += 1
            if eventId not in somJctsCntPerEventId:
                somJctsCntPerEventId[eventId] = 0
            somJctsCntPerEventId[eventId] += 1

        junctionsFile.close()

        partlyNonSomEvntWithSomJctCnt = 0

        for eventId in somJctsCntPerEventId.keys():
            if allJctsCntPerEventId[eventId] > somJctsCntPerEventId[eventId]:
                partlyNonSomEvntWithSomJctCnt += 1

        stats += [
            ( "Somatic%sjunctions" % confidence, somJctCnt ),
            ( "Somatic%sjunctions that are in baseline" % confidence, somJctInBaselineCnt ),
            ( "Somatic%sjunctions that are known (DGV)" % confidence, somJctKnownCnt ),
            ( "Somatic%sjunctions that are consistent with an MEI polymorphism" % confidence, somJctMeiConsistentCnt ),
            ( "Somatic%sjunctions that have score < 15" % confidence, somJctLowScoredCnt ),
            ( "Events containing a somatic%sjunction" % confidence, len(somJctsCntPerEventId) ),
            ( "Partly non-somatic events containing a somatic%sjunction" % confidence, partlyNonSomEvntWithSomJctCnt ),
        ]

    print >>rptFile, "freeform,somaticSvStats"
    for name, value in stats:
        print >>rptFile, "%s,%s" % (name, value)


def genPostExpStandardSvStats(rptFile, allJunctionsFn, highJunctionsFn):
    stats = []

    for (junctionsFn, confidence) in [ (allJunctionsFn, " "),
                                       (highJunctionsFn, " high confidence ") ]:
        shortRangeStrandMismatchedCnt = 0

        junctionsFile = cgatools.util.delimitedfile.DelimitedFile(junctionsFn)
        junctionsFile.addField("LeftChr")
        junctionsFile.addField("LeftPosition", conv=int)
        junctionsFile.addField("LeftStrand")
        junctionsFile.addField("RightChr")
        junctionsFile.addField("RightPosition", conv=int)
        junctionsFile.addField("RightStrand")

        for (leftChr,
             leftPosition,
             leftStrand,
             rightChr,
             rightPosition,
             rightStrand) in junctionsFile:
            if leftChr == rightChr and leftStrand != rightStrand and rightPosition - leftPosition < 50000:
                shortRangeStrandMismatchedCnt += 1

        stats += [
            ( "Short-range strand mismatched%sjunctions" % confidence, shortRangeStrandMismatchedCnt ),
        ]

    print >>rptFile, "freeform,postExportStandardSvStats"
    for name, value in stats:
        print >>rptFile, "%s,%s" % (name, value)


def main():
    parser = optparse.OptionParser("usage: %prog [options]")
    parser.add_option("--output", dest="rptFn")
    parser.add_option("--somatic-master-var", dest="somaticMasterVarFn")
    parser.add_option("--single-copy-calls-rpt", dest="copyCallsRptFn")
    parser.add_option("--multi-copy-calls-rpt", dest="multiCopyCallsRptFn")
    parser.add_option("--single-cnv-qc-stats-diploid", dest="cnvQcStatsDipFn")
    parser.add_option("--single-cnv-qc-stats-nondiploid", dest="cnvQcStatsNonDipFn")
    parser.add_option("--multi-cnv-qc-stats-diploid", dest="multiCnvQcStatsDipFn")
    parser.add_option("--multi-cnv-qc-stats-nondiploid", dest="multiCnvQcStatsNonDipFn")
    parser.add_option("--all-junctions", dest="allJunctionsFn")
    parser.add_option("--high-junctions", dest="highJunctionsFn")
    parser.add_option("--somatic-all-junctions", dest="somaticAllJunctionsFn")
    parser.add_option("--somatic-high-junctions", dest="somaticHighJunctionsFn")
    parser.add_option("--all-sv-events", dest="allSvEventsFn")

    (opts, args) = parser.parse_args()
    if len(args) > 0:
        parser.error("unexpected arguments")

    rptFile = open(opts.rptFn, "w")

    genSomaticVarStats(rptFile, opts.somaticMasterVarFn)
    print >>rptFile
    genSomaticCnvStats(
        rptFile, opts.copyCallsRptFn, opts.multiCopyCallsRptFn,
        opts.cnvQcStatsDipFn, opts.cnvQcStatsNonDipFn,
        opts.multiCnvQcStatsDipFn, opts.multiCnvQcStatsNonDipFn)
    print >>rptFile
    genSomaticSvStats(
        rptFile, opts.somaticAllJunctionsFn, opts.somaticHighJunctionsFn, opts.allSvEventsFn)
    print >>rptFile
    genPostExpStandardSvStats(
        rptFile, opts.allJunctionsFn, opts.highJunctionsFn)

    rptFile.close()

if __name__ == '__main__':
    main()
