#!/bin/bash

R --no-save <<EOF
varFifty<-.0075
alphaFifty<-(.5*.5*.5)/varFifty-.5
betaFifty<-alphaFifty
pr<-seq(.005,1,.01)
quantilesFifty<-qbeta(pr,alphaFifty,betaFifty)
ratiosFifty<-quantilesFifty/(1-quantilesFifty)
for (p in seq(.01,.5,.01)){
     r<-p/(1-p)
     nu<-r*ratiosFifty
     quant<-nu/(1+nu)
     var<-var(quant)
     alphaP<-p*p*(1-p)/var-p
     betaP<-alphaP*(1-p)/p
     cat(sprintf("alphabeta[%f][%f] = [%f,%f]\n",varFifty,p,alphaP,betaP))
}
EOF

