#! /usr/bin/python

import sys
from optparse import OptionParser


def mergeRange(r1, r2):
    if r1[0] != r2[0]:
        raise "can't merge ranges from two different contigs"
    return (r1[0], min(r1[1],r2[1]), max(r1[2],r2[2]))


parser = OptionParser()
parser.disable_interspersed_args()
parser.add_option("--output", "-o", default=None,
                  help="the output file name")
parser.add_option("--reference", "-r", default=None,
                  help="the reference gbi file")
parser.add_option("--merge-dist", default=None, type=int,
                  help="merge ranges this distance or closer")
(options, args) = parser.parse_args()
if options.output == None:
    print "--output option is required"
    parser.print_help()
    raise "bad args"
if options.reference == None:
    print "--reference option is required"
    parser.print_help()
    raise "bad args"
if len(args) == 0:
    parser.print_help()
    raise "bad args"

header = None
ranges = None
for arg in args:
    file = open(arg)
    line = file.readline()
    if header == None:
        header = line
        headerfields = header.split(",")
        if len(headerfields) % 4 != 3:
            raise "bad header"
        ranges = []
        for ii in xrange(len(headerfields)/4 + 1):
            ranges.append([])
    else:
        if header != line:
            raise "header mismatch"
    for line in file:
        fields = line.split(",")
        if len(fields) != len(headerfields):
            raise "header does not match data"
        for ii in xrange(len(fields)/4 + 1):
            if "" == fields[4*ii]:
                continue
            (contig,begin,end) = map(int,fields[4*ii:4*ii+3])
            ranges[ii].append( (contig,begin,end) )

for ii in xrange(len(ranges)):
    rr = ranges[ii]
    if len(rr) > 0:
        rr.sort()
        rrNew = [rr[0]]
        for rng in rr[1:]:
            if rrNew[-1][0] == rng[0] and rrNew[-1][2] == rng[1]:
                rrNew[-1] = mergeRange(rrNew[-1], rng)
            elif ( options.merge_dist != None and
                   rrNew[-1][0] == rng[0] and
                   rrNew[-1][2] + options.merge_dist >= rng[1]):
                rrNew[-1] = mergeRange(rrNew[-1], rng)
            else:
                rrNew.append(rng)
        ranges[ii] = rrNew

maxRanges = 0
for rr in ranges:
    if len(rr) > maxRanges:
        maxRanges = len(rr)

out = open(options.output, "w")
out.write(header)
for ii in xrange(maxRanges):
    for jj in xrange(len(ranges)):
        if (0 != jj):
            out.write(",,")
        if len(ranges[jj]) <= ii:
            out.write(",,")
            continue
        (contig,begin,end) = ranges[jj][ii]
        out.write("%d,%d,%d" % (contig,begin,end))
    out.write("\n")

