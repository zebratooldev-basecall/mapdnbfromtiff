#!/usr/bin/python
import os,sys
import optparse
import numpy
from numpy import *

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(gapsDir='', modelDataDir='',refDir='',libID='', mappingDataDir='')
    parser.add_option('-g', '--gaps-dir', dest='gapsDir',
                      help='directory containing gaps data')
    parser.add_option('-r', '--ref-dir', dest='refDir',
                      help='directory containing reference/simulation data')
    parser.add_option('-l', '--lib-id', dest='libID',
                      help='ID of library')
    parser.add_option('-d', '--mapping-data-dir', dest='mappingDataDir',
                      help='directory that contains mapper output and intermediate covmodel files')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.mappingDataDir:
        parser.error('mapping-data-dir specification required')

    testdata = [
        "395,216,44,37,10,9,6,10,0,9,6,16,4,0,GACGTCCT,CTAGAGTA,GAGTCTCA,TGAGACAT,ACTTGCAG,GGGAAGGA,TCCTAATCAAGAGTCTCACCTGGGTACATGAACTTAGGAGGCAGCCTGGAGGGGGCATGTGAGACACTAG,,,GTCTGG,CCTCCA,,,",
        "358,165,44,38,13,7,11,4,0,9,10,10,6,0,AGGAGAAT,TACATTTC,GAACCCGG,GCAGGACA,TCATGCCA,AGCATTGA,TGACAAAGCATGAATAGCGCGGGGCAAGGACTACAGAATCCCTTGGAACCCGGGAACTCCGGTGAGATCA,GCCGA,,,,,ATTGAC,",
        "420,176,40,38,4,6,13,12,0,8,6,13,8,0,TTCTTTTG,GCTGTAGC,GCAGGCAC,GCACTTAC,GCTTCCGC,TAGTTGTT,GTTAGGTTTGTGTGTACTTCCAGGCCAGGTGGCTGTTTGCATGGGGCAGGCACCATAGAGGTAAGGTGCT,AGTGC,,,,,TTGTTA,",
        "428,246,48,42,8,9,11,7,0,8,10,12,5,0,AACCAGGA,CCCTAAGA,ACCGTGGG,GGTCCATG,CTACAGAC,AGGAGAGG,GAGGAACCAGCATGTTGGGGTCTAGGCCAATCCCTAGGACAGTGAACCGTGGGGGCCTCACCTGAGCTAC,GAGACT,,,,,GAGAGG,",
        "358,157,37,32,6,10,11,8,0,7,5,6,17,0,TATCAGTA,TCCCCCAT,TTGGTCTT,AAGCCCAG,CGTGAGAT,CTGCCTGA,CTGATTCCCTAGGTGGCAATGGCTAGCAGGGTCCCAGTATTTATTTTGGTCTTAGATCTTCACTTACGTG,TCTATA,,,,,GCCTGA,",
        "424,252,51,45,11,8,13,3,0,5,9,15,6,0,CTACCCAC,AGGGGAGC,GGATGGGA,CACCCGGA,GCAGGGCT,TGGACTGG,CCACAATTGGGGATGGGAAGACAGCCCAGAGGCAGCTGGAGGGAGGGGGCTGTTTCCAGCCACCCTGAGG,,,GTAGGG,CAGCCC,G,,",
        "352,213,47,41,9,9,12,5,0,6,13,7,9,0,ATTTGTCA,CTCCCCAT,GAAGGGCT,CCTCAGTG,GACCGTGG,TCTGGTGT,GTCACCTGAGGAAGGGCTAACTGAGCCGAGTGACCTGTGAAGCAGGAACCCCTTTTGCCCTCAGTCTCCC,,,CTGGTA,GGGTT,,,"
        ]

    dnblen = zeros(2000)
    dnbGCpcnt = zeros(101)
    armsGC = zeros(95)
    armsAG = zeros(95)
    leftseqA = zeros(46)
    leftseqC = zeros(46)
    leftseqG = zeros(46)
    leftseqT = zeros(46)
    rightseqA = zeros(46)
    rightseqC = zeros(46)
    rightseqG = zeros(46)
    rightseqT = zeros(46)
    leftend = {}
    rightend = {}
    leftAcuI = {}
    rightAcuI = {}
    leftEcoP15 = {}
    rightEcoP15 = {}

    inmapstats = open(os.path.join(options.mappingDataDir,'mappingStatsData.csv'),'r')
    for l in inmapstats:
        l = l.strip()

        (indist,incountGC,indnbGCbases,indnbAGbases,
         inleftArmA,inleftArmC,inleftArmG,inleftArmT,inleftArmN,
         inrightArmA,inrightArmC,inrightArmG,inrightArmT,inrightArmN,
         inleftFrgString,inrightFrgString,inleftAcuIString,inrightAcuIString,inleftEcoP15String,inrightEcoP15String,
         incalledbases,inleftgapI,inleftgapII,inleftgapIII,inrightgapI,inrightgapII,inrightgapIII,
         dummy) = l.split(",")

        dnblen[int(indist)]+=1
        dnbGCpcnt[int(100*float(incountGC)/float(indist))]+=1
        armsGC[int(indnbGCbases)]+=1
        armsAG[int(indnbAGbases)]+=1
        leftseqA[ int(inleftArmA) ]+=1
        leftseqC[ int(inleftArmC) ]+=1
        leftseqG[ int(inleftArmG) ]+=1
        leftseqT[ int(inleftArmT) ]+=1
        rightseqA[ int(inrightArmA) ]+=1
        rightseqC[ int(inrightArmC) ]+=1
        rightseqG[ int(inrightArmG) ]+=1
        rightseqT[ int(inrightArmT) ]+=1

        if inleftFrgString not in leftend:
            leftend[inleftFrgString]=0
        leftend[inleftFrgString] +=1

        if inrightFrgString not in rightend:
            rightend[inrightFrgString]=0
        rightend[inrightFrgString]+=1

        if inleftAcuIString not in leftAcuI:
            leftAcuI[inleftAcuIString]=0
        leftAcuI[inleftAcuIString]+=1

        if inrightAcuIString not in rightAcuI:
            rightAcuI[inrightAcuIString]=0
        rightAcuI[inrightAcuIString]+=1

        if inleftEcoP15String not in leftEcoP15:
            leftEcoP15[inleftEcoP15String]=0
        leftEcoP15[inleftEcoP15String]+=1

        if inrightEcoP15String not in rightEcoP15:
            rightEcoP15[inrightEcoP15String]=0
        rightEcoP15[inrightEcoP15String]+=1

    inmapstats.close()

    outmapsummary = open(os.path.join(options.mappingDataDir,'covStatsSummary.csv'),'w')
    for i in range(0,2000):
        print >> outmapsummary, "DNBLen,%d %d" % (i,dnblen[i])
    for i in range(0,101):
        print >> outmapsummary, "DNBGCpcnt,%d %d" % (i,dnbGCpcnt[i])
    for i in range(0,95):
        print >> outmapsummary, "armsGC,%d %d" % (i,armsGC[i])
    for i in range(0,95):
        print >> outmapsummary, "armsAG,%d %d" % (i,armsAG[i])
    for i in range(0,46):
        print >> outmapsummary, "leftarmA,%d %d" % ( i, leftseqA[i] )
    for i in range(0,46):
        print >> outmapsummary, "leftarmC,%d %d" % ( i, leftseqC[i] )
    for i in range(0,46):
        print >> outmapsummary, "leftarmG,%d %d" % ( i, leftseqG[i] )
    for i in range(0,46):
        print >> outmapsummary, "leftarmT,%d %d" % ( i, leftseqT[i] )
    for i in range(0,46):
        print >> outmapsummary, "rightarmA,%d %d" % ( i, rightseqA[i] )
    for i in range(0,46):
        print >> outmapsummary, "rightarmC,%d %d" % ( i, rightseqC[i] )
    for i in range(0,46):
        print >> outmapsummary, "rightarmG,%d %d" % ( i, rightseqG[i] )
    for i in range(0,46):
        print >> outmapsummary, "rightarmT,%d %d" % ( i, rightseqT[i] )
    for s in leftend.keys():
        print >> outmapsummary, "leftend,%s %d" % (s, leftend[s])
    for s in rightend:
        print >> outmapsummary, "rightend,%s %d" % (s, rightend[s])
    for s in leftAcuI:
        print >> outmapsummary, "leftAcuI,%s %d" % (s, leftAcuI[s])
    for s in rightAcuI:
        print >> outmapsummary, "rightAcuI,%s %d" % (s, rightAcuI[s])
    for s in leftEcoP15:
        print >> outmapsummary, "leftEcoP15,%s %d" % (s, leftEcoP15[s])
    for s in rightEcoP15:
        print >> outmapsummary, "rightEcoP15,%s %d" % (s, rightEcoP15[s])

    outmapsummary.close()

if __name__ == '__main__':
    main()
