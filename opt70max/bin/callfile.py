#! /usr/bin/python


FILE_HEADER = ( ">locus\tploidy\thaplotype\tchromosome\tbegin\tend\t" +
                "varType\treference\talleleSeq\ttotalScore\thapLink\txRef" )

######################################################################
# Call
######################################################################

class Call:
    def __init__(self, fields):
        self.fields = fields
        self.locus = int(fields[0])
        self.ploidy = 0
        if fields[1] != "?":
            self.ploidy = int(fields[1])
        self.haplotype = 0
        if fields[2] != "all":
            self.haplotype = int(fields[2])
        self.chromosome = fields[3]
        self.begin = int(fields[4])
        self.end = int(fields[5])
        self.vartype = fields[6]
        self.reference = fields[7]
        self.alleleSeq = fields[8]
        self.totalScore = 0
        if fields[9] != "":
            self.totalScore = int(fields[9])
        self.hapLink = fields[10]
        self.xRef = fields[11]
        self.sequence = self.alleleSeq

    def __str__(self):
        return "\t".join(self.fields)

    def hasNoCalls(self):
        return "?" in self.sequence or "N" in self.sequence

    def isAltCall(self):
        return not self.vartype in ["=", "ref", "ref-consistent", "no-call", "no-call-rc" ]

    def calledBases(self):
        if self.sequence == "=":
            return self.end - self.begin
        return len(self.sequence) - self.sequence.count("?") - self.sequence.count("N")


######################################################################
# Allele
######################################################################

class Allele:
    def __init__(self):
        self.calls = []

    def __str__(self):
        return "\n".join([str(call) for call in self.calls])

    def sequence(self):
        return "".join([call.sequence for call in self.calls])

    def refSequence(self):
        return "".join([call.reference for call in self.calls])

    def hasNoCalls(self):
        for call in self.calls:
            if call.hasNoCalls():
                return True
        return False

    def hasAltCalls(self):
        for call in self.calls:
            if call.isAltCall():
                return True
        return False

    def minScore(self):
        score = 999999
        for call in self.calls:
            score = min(call.totalScore, score)
        return score
    
    def calledBases(self):
        cb = 0
        for call in self.calls:
            cb += call.calledBases()
        return cb

    def varTypes(self):
        return [ call.vartype for call in self.calls ]


######################################################################
# Locus
######################################################################

class Locus:
    def __init__(self, id):
        self.id = id
        self.chromosome = None
        self.begin = 999999999
        self.end = 0
        self.alleles = []

    def __str__(self):
        return "\n".join([str(allele) for allele in self.alleles])

    def addCall(self, call):
        while len(self.alleles) < call.ploidy:
            self.alleles.append(Allele())
        if 0 == call.haplotype:
            for allele in self.alleles:
                allele.calls.append(call)
        else:
            self.alleles[call.haplotype-1].calls.append(call)
        self.chromosome = call.chromosome
        self.begin = min(self.begin, call.begin)
        self.end = max(self.end, call.end)

    def hasNoCalls(self):
        for allele in self.alleles:
            if allele.hasNoCalls():
                return True
        return False

    def hasAltCalls(self):
        for allele in self.alleles:
            if allele.hasAltCalls():
                return True
        return False

    def isCalledHomozygous(self):
        if self.hasNoCalls():
            return False
        sequence0 = self.alleles[0].sequence()
        for allele in self.alleles[1:]:
            if allele.sequence() != sequence0:
                return False
        return True

    def isCalledHeterozygous(self):
        if self.hasNoCalls():
            return False
        return not self.isCalledHomozygous()

    def isCalledAlternateOnly(self):
        if self.hasNoCalls():
            return False
        for allele in self.alleles:
            if allele.refSequence() == allele.sequence():
                return False
        return True

    def minScore(self):
        score = 999999
        for allele in self.alleles:
            score = min(allele.minScore(), score)
        return score

    def calledBases(self):
        cb = 0
        for allele in self.alleles:
            cb += allele.calledBases()
        return cb

    def varTypes(self):
        result = []
        for allele in self.alleles:
            result += allele.varTypes()
        return result



######################################################################
# LocusStream
######################################################################

class LocusStream:
    def __init__(self, filename):
        self.file = open(filename)
        self.currentComment = []
        self.nextComment = []
        self.currentLocus = None

    def close(self):
        self.file.close()

    def getComment(self):
        return '\n'.join(self.currentComment)

    def nextLocus(self):
        self.currentComment = self.nextComment
        self.nextComment = []
        while True:
            line = self.file.readline()
            if not line:
                break
            line = line[:-1]
            if line.startswith('#'):
                self.nextComment.append(line[1:])
                continue
            if len(line) == 0:
                continue
            fields = line.split("\t")
            if not fields[0].isdigit():
                if line != FILE_HEADER:
                    raise Exception("unrecognized line: " + line)
                self.currentComment = []
                self.nextComment = []
                continue
            if len(fields) != 12:
                raise Exception("wrong number of fields in call: " + line)
            call = Call(fields)
            if self.currentLocus is None:
                self.currentLocus = Locus(call.locus)
            if call.locus == self.currentLocus.id:
                self.currentComment += self.nextComment
                self.nextComment = []
                self.currentLocus.addCall(call)
                continue
            result = self.currentLocus
            self.currentLocus = Locus(call.locus)
            self.currentLocus.addCall(call)
            return result
        result = self.currentLocus
        self.currentLocus = None
        return result
