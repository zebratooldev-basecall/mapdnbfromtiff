#!/usr/bin/env python

import os
import sys
import re
import getopt
import bz2


purines    = ['A','G']
pyrimidine = ['C','T']
AllVarTypes = ['snp', 'ins', 'del', 'sub']

class TsvReader(object):
    def __init__(self, ff):
        self.ff = ff
        while True:
            line = self.ff.readline()
            if line.startswith('>'): break
        self.hFields = line.rstrip('\r\n').split('\t')
        
    def close(self):
        self.ff.close()
        
    def next(self):
        line = self.ff.readline()
        if '' == line:
            return None
        fields = line.rstrip('\r\n').split('\t')
        if len(fields) != len(self.hFields):
            raise Exception('field count mismatch')
        result = {}
        for (name,val) in zip(self.hFields, fields):
            result[name] = val
        result['WholeLine'] = line.rstrip()
        return result

##################################################################################################

def filter(line, mType, thresh, REGIONS):

    if not REGIONS == {} :
        chrLists = REGIONS[ line['chromosome'] ]
        begins = chrLists[0]
        ends   = chrLists[1]

        for i in range(len(begins)):
            if int(line['begin']) < begins[i]:
                break
        ## i is now at one region past the start, so go back one
        i -= 1
        if int(line['end']) > ends[i]:
            ## This variant is not fully contained within one region, filter it out
            return "Regions" # True


    if mType == 'PASS':
        if line['varFilter'] == 'PASS':
            return False
        elif not line['varFilter'] == '':
            return line['varFilter'] # True
        else:
            return "NonPASS" # True 
    else:
        try:
            if int(line[ 'varScore' + mType ]) >= thresh:
                if 'AMBIGUOUS' in line['varFilter']:
                    return "AMBIGUOUS" # True
                else:
                    return False
            else:
                return "VQLOW" # True
        except:
            ## non-numeric score value
            return "NonPASS" # True

##################################################################################################

def main():

    # parse command line options and update variables
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:p:f:r:", [])
    except getopt.GetoptError:
        sys.exit(usage)

    RegionsFile = ""
    FILTER = "PASS"

    for opt, arg in opts:
        if opt == "-h":
            sys.exit('Use me properly.')
        elif opt == '-i':
            INFILE = arg
        elif opt == '-o':
            OUTFILE = arg
        elif opt == '-p':
            PAR_file = arg
        elif opt == '-f':
            FILTER = arg
        elif opt == '-r':
            RegionsFile = arg

    # Reading PAR file
    parf = open(PAR_file, 'r')
    PAR = []
    for line in parf:
        (chrmNC,startNC,endNC,chrmDI,startDI,endDI) = line.rstrip().split(",")
        if chrmDI == 'chrX':
            PAR.append( (startDI,endDI) )
    parf.close()

    ## Read in regions
    REGIONS = {}
    if RegionsFile != "":
        rfile = open(RegionsFile, 'r')
        for line in rfile:
            (chrm, start, stop) = line.rstrip().split(",")
            if chrm == ">contig": continue
            chrLists = REGIONS.get( chrm, [ [], [] ] )
            chrLists[0].append(int(start))
            chrLists[1].append(int(stop))
            REGIONS[chrm] = chrLists
        rfile.close()

    ## Setup filtering thresholds
    if FILTER == 'PASS':
        mType = 'PASS'
        homScoreThresh = 0
        hetScoreThresh = 0
    else:
        (mType, homScoreThresh, hetScoreThresh) = FILTER.split(",")
        homScoreThresh = int(homScoreThresh)
        hetScoreThresh = int(hetScoreThresh)


    # Reading var file
    outf = open(OUTFILE, 'w')
    reader = ""
    if "bz2" in INFILE:
        print "Reading in bz2 file"
        reader = TsvReader( bz2.BZ2File(INFILE, 'r') )
    else:
        print "Reading in text file"
        reader = TsvReader( open(INFILE, 'r') )
    Counter = {}
    Ti = 0
    Tv = 0
    nextline = reader.next()
    while not nextline is None:
        PrintThis = False
        line1 = nextline
        nextline = reader.next()
        lines = [ line1 ]

        if line1['allele'] == 'all':
            if line1['varType'] == 'no-call' or  line1['varType'] == 'no-ref' or line1['varType'] == 'ref': continue

        while not nextline is None and nextline['>locus'] == line1['>locus']:
            lines += [ nextline ]
            nextline = reader.next()

        varType = []
        var = []

        ## Filtering
        ScoreThresh = hetScoreThresh
        initType = 'het'
        filteredCount = 0
        if len(lines)==2 and lines[0]['alleleSeq']==lines[1]['alleleSeq']:
            ScoreThresh = homScoreThresh
            initType = 'hom'
        for line in lines:
            var.append( line['alleleSeq'] )
            filter_value = filter(line, mType, ScoreThresh, REGIONS)
            if filter_value:
                varType.append( "no-call" )
                line['filter'] = True
                if filter_value != "AMBIGUOUS":
                    filteredCount += 1
            else:
                varType.append( line['varType'] )
                line['filter'] = False

        ## If my hom turned into a half, re-filter based on the non-hom threshold
        skip = False
        if initType == 'hom' and filteredCount == 1:
            for line in lines:
                if line['filter'] == False and filter(line, mType, hetScoreThresh, REGIONS):
                    skip = True
        if skip:
            continue

        ## Skip over filtered loci
        if  len([ i for i in varType if not i == "no-call" and not i == "ref" ]) == 0: continue

        ## Determine the location autosome, chrY, chrX-nonPAR
        if line1['chromosome'] in ["chr" + str(i) for i in range(1,22+1)]:
            loc = "auto"
        elif line1['chromosome'] == 'chrY':
            loc = 'chrY'
        elif line1['chromosome'] == 'chrX':
            loc = 'chrXnonPAR'
            for (start,stop) in PAR:
                for line in lines:
                    if int(line['begin']) < int(stop) and int(line['end']) > int(start):
                        loc = 'chrXPAR'
                        break
        else:
            loc = "other"


        novel = 'novel'
        for line in lines:
            ## Determine if this is novel (wrt the NEW dbSNP)
            if  "dbsnp" in line['xRef']: novel = 'known'

            ## Determine if this is Ti or Tv (SNPs only)
            if line['varType'] == 'snp' and not line['filter']:
                if len(line['reference']) > 1:
                    line['reference'], line['alleleSeq'] = [ x for x in zip(line['reference'], line['alleleSeq']) if x[0]!=x[1] ][0]
                if ( (line['reference'] in purines   ) and (line['alleleSeq'] in purines   ) or 
                     (line['reference'] in pyrimidine) and (line['alleleSeq'] in pyrimidine) ):
                    Ti += 1
                elif( (line['reference'] in purines   ) and (line['alleleSeq'] in pyrimidine)  or 
                      (line['reference'] in pyrimidine) and (line['alleleSeq'] in purines   ) ):
                    Tv += 1
                else:
                    print "ERROR!!!"
                    PrintThis = True

        # Deermine het / hom / half
        ## This is super annoying!!
        kind = "unknown"
        vtype = {}
        if len(varType) == 1:
            if lines[0]['ploidy']=='1':
                kind = "hemi"
                vtype[ varType[0] ] = 1
            # Sanity check
            else:
                print "ERROR?  1 varType but ploidy !=1"
                PrintThis = True
        elif len(varType) == 2:
            vtype[ [x for x in varType if x!="ref" and x != "no-call"][0] ] = 1
            if "no-call" in varType:
                kind = "half"
            elif "ref" in varType:
                kind = "het-ref"
            elif varType[0] == varType[1]:
                if var[0] == var[1]:
                    kind = "hom"
                else:
                    kind = "het-alt"          
            else:
                kind = "other"
                vtype[ [x for x in varType if x!="ref" and x != "no-call"][1] ] = 1
        else:
            if "no-call" in varType:
                kind = "half"
            else:
                kind = "other"
            for v in [x for x in varType if x!="ref" and x != "no-call"]:
                vtype[ v ] = vtype.get(v, 0) + 1

        ## Sanity check
        if kind == "unknown":
            print "ERROR? unknown kind"
            PrintThis = True
        if not bool(vtype):
            print "ERROR? empty dict"
            PrintThis = True
        if PrintThis:
            for line in lines:
                print line['WholeLine']

        hasSnp = False
        for v,count in vtype.iteritems():
            Counter[ (kind, loc, v, novel) ] = Counter.get( (kind, loc, v, novel), 0) + count
            if v == "snp": hasSnp = True
            if PrintThis:
                print  (kind, loc, v, novel, count)

        
    print Counter
    print Ti
    print Tv

    ## Calculate metrics
    SNP_total_count = sum([ Counter[x] for x in Counter.keys() if 'snp' in x[2] ])
    INS_total_count = sum([ Counter[x] for x in Counter.keys() if 'ins' in x[2] ])
    DEL_total_count = sum([ Counter[x] for x in Counter.keys() if 'del' in x[2] ])
    SUB_total_count = sum([ Counter[x] for x in Counter.keys() if 'sub' in x[2] ])
    SNPs_on_chrY    = sum([ Counter[x] for x in Counter.keys() if 'snp' in x[2] and 'chrY' in x] )
    Ins_del_SNP_ratio = float(INS_total_count + DEL_total_count) / SNP_total_count
    SNP_novel_rate = float(sum([ Counter[x] for x in Counter.keys() if 'snp' in x[2] and 'novel' == x[3]])) / SNP_total_count
    INS_novel_rate = float(sum([ Counter[x] for x in Counter.keys() if 'ins' in x[2] and 'novel' == x[3]])) / INS_total_count
    DEL_novel_rate = float(sum([ Counter[x] for x in Counter.keys() if 'del' in x[2] and 'novel' == x[3]])) / DEL_total_count
    SUB_novel_rate = float(sum([ Counter[x] for x in Counter.keys() if 'sub' in x[2] and 'novel' == x[3]])) / SUB_total_count
    SNP_het_hom_ratio               = (float(sum([ Counter[x] for x in Counter.keys() if 'snp' in x[2] and ('het-ref' == x[0] or 'het-alt' == x[0])])) / 
                                       float(sum([ Counter[x] for x in Counter.keys() if 'snp' in x[2] and 'hom' == x[0]])) )
    SNP_het_hom_ratio_for_AUTOSOMES = (float(sum([ Counter[x] for x in Counter.keys() if 'snp' in x[2] and ('het-ref' == x[0] or 'het-alt' == x[0]) and 'auto' == x[1] ])) / 
                                       float(sum([ Counter[x] for x in Counter.keys() if 'snp' in x[2] and 'hom' == x[0] and 'auto' == x[1]])) )
    try:
        SNP_het_hom_ratio_chrX_non_PAR  = (float(sum([ Counter[x] for x in Counter.keys() if 'snp' in x[2] and ('het-ref' == x[0] or 'het-alt' == x[0]) and 'chrXnonPAR' == x[1] ])) / 
                                           float(sum([ Counter[x] for x in Counter.keys() if 'snp' in x[2] and 'hom' == x[0] and 'chrXnonPAR' == x[1] ])) )
    except:
        SNP_het_hom_ratio_chrX_non_PAR  = 0
    INS_het_hom_ratio               = (float(sum([ Counter[x] for x in Counter.keys() if 'ins' in x[2] and ('het-ref' == x[0] or 'het-alt' == x[0])])) / 
                                       float(sum([ Counter[x] for x in Counter.keys() if 'ins' in x[2] and 'hom' == x[0]])) )
    DEL_het_hom_ratio               = (float(sum([ Counter[x] for x in Counter.keys() if 'del' in x[2] and ('het-ref' == x[0] or 'het-alt' == x[0])])) / 
                                       float(sum([ Counter[x] for x in Counter.keys() if 'del' in x[2] and 'hom' == x[0]])) )
    SUB_het_hom_ratio               = (float(sum([ Counter[x] for x in Counter.keys() if 'sub' in x[2] and ('het-ref' == x[0] or 'het-alt' == x[0])])) / 
                                       float(sum([ Counter[x] for x in Counter.keys() if 'sub' in x[2] and 'hom' == x[0]])) )
    INS_het_hom_ratio_for_AUTOSOMES = (float(sum([ Counter[x] for x in Counter.keys() if 'ins' in x[2] and ('het-ref' == x[0] or 'het-alt' == x[0]) and 'auto' == x[1] ])) / 
                                       float(sum([ Counter[x] for x in Counter.keys() if 'ins' in x[2] and 'hom' == x[0] and 'auto' == x[1]])) )
    DEL_het_hom_ratio_for_AUTOSOMES = (float(sum([ Counter[x] for x in Counter.keys() if 'del' in x[2] and ('het-ref' == x[0] or 'het-alt' == x[0]) and 'auto' == x[1] ])) / 
                                       float(sum([ Counter[x] for x in Counter.keys() if 'del' in x[2] and 'hom' == x[0] and 'auto' == x[1]])) )
    SUB_het_hom_ratio_for_AUTOSOMES = (float(sum([ Counter[x] for x in Counter.keys() if 'sub' in x[2] and ('het-ref' == x[0] or 'het-alt' == x[0]) and 'auto' == x[1] ])) / 
                                       float(sum([ Counter[x] for x in Counter.keys() if 'sub' in x[2] and 'hom' == x[0] and 'auto' == x[1]])) )

    SNP_Transitions_transversions   = float(Ti)/Tv

    FILTER = re.sub(",", "_",FILTER)
    outf.write( "SNP_total_count\t"                 + FILTER + "\t" + str(SNP_total_count) + "\n")
    outf.write( "INS_total_count\t"                 + FILTER + "\t" + str(INS_total_count) + "\n")
    outf.write( "DEL_total_count\t"                 + FILTER + "\t" + str(DEL_total_count) + "\n")
    outf.write( "SUB_total_count\t"                 + FILTER + "\t" + str(SUB_total_count) + "\n")
    outf.write( "SNPs_on_chrY\t"                    + FILTER + "\t" + str(SNPs_on_chrY) + "\n")
    outf.write( "Ins_del_SNP_ratio\t"               + FILTER + "\t" + str(Ins_del_SNP_ratio) + "\n")
    outf.write( "SNP_novel_rate\t"                  + FILTER + "\t" + str(SNP_novel_rate) + "\n")
    outf.write( "INS_novel_rate\t"                  + FILTER + "\t" + str(INS_novel_rate) + "\n")
    outf.write( "DEL_novel_rate\t"                  + FILTER + "\t" + str(DEL_novel_rate) + "\n")
    outf.write( "SUB_novel_rate\t"                  + FILTER + "\t" + str(SUB_novel_rate) + "\n")
    outf.write( "SNP_Transitions_transversions\t"   + FILTER + "\t" + str(SNP_Transitions_transversions) + "\n")
    outf.write( "SNP_het_hom_ratio\t"               + FILTER + "\t" + str(SNP_het_hom_ratio) + "\n")
    outf.write( "INS_het_hom_ratio\t"               + FILTER + "\t" + str(INS_het_hom_ratio) + "\n")
    outf.write( "DEL_het_hom_ratio\t"               + FILTER + "\t" + str(DEL_het_hom_ratio) + "\n")
    outf.write( "SUB_het_hom_ratio\t"               + FILTER + "\t" + str(SUB_het_hom_ratio) + "\n")
    outf.write( "SNP_het_hom_ratio_for_AUTOSOMES\t" + FILTER + "\t" + str(SNP_het_hom_ratio_for_AUTOSOMES) + "\n")
    outf.write( "INS_het_hom_ratio_for_AUTOSOMES\t" + FILTER + "\t" + str(INS_het_hom_ratio_for_AUTOSOMES) + "\n")
    outf.write( "DEL_het_hom_ratio_for_AUTOSOMES\t" + FILTER + "\t" + str(DEL_het_hom_ratio_for_AUTOSOMES) + "\n")
    outf.write( "SUB_het_hom_ratio_for_AUTOSOMES\t" + FILTER + "\t" + str(SUB_het_hom_ratio_for_AUTOSOMES) + "\n")
    outf.write( "SNP_het_hom_ratio_chrX_non_PAR\t"  + FILTER + "\t" + str(SNP_het_hom_ratio_chrX_non_PAR) + "\n")
    outf.close()

###################################################

if __name__ == "__main__":
    main()
