#! /usr/bin/env python

import os, subprocess, sys, wfclib.jsonutil
from os.path import join as pjoin, basename, splitext
from optparse import OptionParser
from cgiutils import bundle
from glob import glob


def doCall(args):
    print '"' + '" \\\n"'.join(args) + '"'
    sts = subprocess.call(args)
    if sts != 0:
        raise Exception('%s failed with sts %d' % (args[0], sts))


def optShellNameToPyName(name):
    result = []
    for cc in name:
        if '-' == cc:
            result.append('_')
        else:
            result.append(cc)
    return ''.join(result)


def getMostRecentStateFile(workDir, stage, subworkflow=None):
    if subworkflow is not None:
        stage += subworkflow
    fna = glob(pjoin(workDir, 'state', 'st.*.'+stage+'.*'))
    sfna = []
    for fn in fna:
        (id1,id2) = map(int,fn.split('.')[-2:])
        sfna.append( (id1,id2,fn) )
    sfna.sort()
    return sfna[-1][-1]


def requireParam(options, name):
    if options.__dict__[optShellNameToPyName(name)] is None:
        raise Exception('missing required parameter: %s' % name)


def main():
    parser = OptionParser('usage: %prog PARAMS')
    parser.disable_interspersed_args()
    parser.add_option('--asm-work-dir', default=None,
                      help='Work dir for AsmPrep stage of assembly, or common dir for multiple assemblies')
    parser.add_option('--subworkflow', default=None,
                      help='subworkflow suffix in case of multiple subworkflows')
    parser.add_option('--sleep-seconds', type=int, default=8*60*60,
                      help='Number of seconds to run sleep job and keep mapping servers running.')
    parser.add_option('--reports', default='',
                      help='Path to directory for MappingServer reports.')
    parser.add_option('--server', '-s', default=None,
                      help='host:port of the scheduler node')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        raise Exception('unexpected args')
    
    requireParam(options, 'asm-work-dir')
    
    #if os.path.abspath(os.getcwd()) == os.path.abspath(options.asm_work_dir):
    #    raise Exception ('--asm-work-dir should not coincide with current working directory');

    overrides = bundle()
    overrides.serveMappingsSleepSeconds = options.sleep_seconds
    overrides.serveMappingsReports = options.reports

    extraParams = []
    if options.server:
        extraParams += [ '-s', options.server ]

    name = 'ms-' + basename(options.asm_work_dir)
    
    if options.subworkflow :
        name        += options.subworkflow 
        extraParams += [ '--clone-subworkflow', options.subworkflow ]

    print '***NAME=', name

    doCall(['rat',
            'create',
            name ,
            'onestep.py',
            '-c',
            'ServeMappings',
            '--clone-state-file=%s' %
            getMostRecentStateFile(options.asm_work_dir, 'Asm', options.subworkflow),
            '-b',
            'ServeMappings',
            '-x',
            wfclib.jsonutil.dumps(overrides)
            ] + extraParams)


if __name__ == '__main__':
    main()
