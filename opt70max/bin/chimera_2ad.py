#! /usr/bin/python

import sys, os
from glob import glob


mapstats = {
	     "inputDnbCount":"input_dnbs", 
	     "searchedFullDnbCount":"full_searched_pct", 
	     "uniquelyPairedFullDnbCount":"full_mapped_uniq_pct",
#	     "bothArmsMappedUnpairedFullDnbCount":"fully_mapped_pct", 
	     "multiplyPairedFullDnbCount":"full_mapped_nonuniq_pct",
	     "mappedBaseCount":"bases_mapped",
	     "discTotalBaseCount":"discTotalBase",
	     "bothArmsMappedUnpairedFullDnbCount":"full_mapped_inconsistent_pct",
	     "notSearchedFullDnbCount":"full_arm_not_searched_pct",
	     "overflowedFullDnbCount":"full_arm_overflowed_pct",
	     "oneArmMappedFullDnbCount":"one_arm_only_unmapped_pct",
	     "neitherArmMappedFullDnbCount":"neither_arm_mapped_pct",
             "capturedGapFraction":"gaps_outside_bulk_range_pct",
	     "leftNotMappedHalfDnbCount":"left_not_mapped_half_pct",
	     "rightNotMappedHalfDnbCount":"right_not_mapped_half_pct",
	     "discDiscordantBaseCount":"discordance_rate"
	    }


for workdir in sys.argv[1:]:
    name = os.path.basename(workdir)

    fmate = open("%s/cs-mapStats.csv"%(workdir,), "w")
    header = ["slidelane","input_dnbs","full_searched_pct","full_mapped_uniq_pct","full_mapped_nonuniq_pct","bases_mapped","full_mapped_inconsistent_pct", "full_arm_not_searched_pct", "full_arm_overflowed_pct", "one_arm_only_unmapped_pct", "neither_arm_mapped_pct", "gaps_outside_bulk_range_pct", "left_not_mapped_half_pct", "right_not_mapped_half_pct","sample_size", "estranged_mate_pct", "disoriented_mate_pct", "disordered_mate_pct", "separated_mate_pct", "paired_mate_pct", "discordance_rate"]
    fmate.write(",".join(header)+"\n")
    
    ## added to adapt new wf
    fxml = glob('%s/collection.xml'%(workdir,))
    if len(fxml)==0: continue
    xml = open(fxml[0], 'r')
    for ln in xml:
        ln = ln.strip()
        if ln.startswith("<Slide"):
            slide = ln.split("=")[-1].strip(">").strip("\"")
        if ln.startswith("<Lane DNB"):
            xlane = ln.split(" ")[4].split("=")[-1].strip("\"")
    lane = slide+"-"+xlane

    fname = "%s/MAP/%s/reports/MappingStats.csv" %(workdir,lane)
    if not os.path.isfile(fname):
	print "No MappingStats.csv file exists for lane %s" % lane
	continue
    mstats = open(fname, 'r')
 
    parse = 0
    sum = 0
    tbase = 0; tdnb = 0
    for line in mstats:
	line = line.strip()
	if not line: continue

	if line.startswith("dict"):
	    fmate.write(lane)
	    parse = 1
	if parse:
	    metric, count = line.split(",")
            if metric.startswith("input"): 
		fmate.write(",%s" %count)
		tdnb = float(count)
	    elif metric in mapstats:
		if metric.startswith("mappedBaseCount"): 
		    fmate.write(",%s" %count)
                elif metric.startswith("discTotalBaseCount"):
                    tbase = float(count)
	        elif metric.startswith("captured"):
		    fmate.write(",%s" %count)
		elif metric.startswith("disc"): 
		    dpct = 100*float(count)/tbase
		    fmate.write(",%f" %dpct)
		else:
                    dpct = 100*float(count)/tdnb
		    fmate.write(",%f" %dpct)
	    elif metric.startswith("mate"):
	        if "Sample" in metric:
		    fmate.write(","+count)
		    size = float(count)
		else:	
	    	    pct = 100*float(count)/size
		    fmate.write(",%f" %pct)
		    sum+=pct

	if line.startswith("array"):
	    parse = 0
    if sum!=100: print "Something wrong with the lane %s" %lane
    fmate.write("\n")
