#!/usr/bin/env python

from optparse import OptionParser
from os.path import join as pjoin
from ConfigParser import ConfigParser
import os
import sys
import pymssql

"""
Upload results from Discordance Pattern workflow to dwProd.

"""
class DBLoader:

    def __init__(self,dbLogin):
        self.conn = None
        self.dbLogin=dbLogin
        self.rows = []
        self.accumulate=""
        self.insertion_count=0

    def connect(self,whichDB="idqc:dwProd"):
        self.conn = pymssql.connect(host=self.dbLogin.get(whichDB,'host'),
                               user=self.dbLogin.get(whichDB,'user'),
                               password=self.dbLogin.get(whichDB,'password'),
                               database=self.dbLogin.get(whichDB,'database'))
        if not self.conn:
            print "can't connect"
            os.sys.exit()

    def execute(self,query):
        cur = self.conn.cursor()
        cur.execute(query)
        self.rows = cur.fetchall()
        cur.close()
        return self.rows[0][0]

    def execute_no_result(self, query, debug):
        cur = self.conn.cursor()
        if not debug:
            #try :
            cur.execute(query)
            #except pymssql.OperationalError:
            #    pass
            #cur.close()
        else:
            print query
            print "debug run! no insertion!"

    def check_header(self, fn, header, expected):
        test = header.rstrip('\r\n')

        if test != ','.join(expected):
            print test
            print expected
            raise Exception('Header for %s file does not matched expected!' % fn)

    def commit_accumulated_insertions(self, debug):
        if self.insertion_count > 0:
            print self.accumulate
            self.execute_no_result(self.accumulate, debug)
            self.accumulate = ""
            self.conn.commit()
            self.insertion_count=0

    def insert_positional_discordance(self, lane, fn, debug):
        HEADER = ['position','totalBaseCount','discordantBaseCount','discordanceRate']

        # check for file.
        if not os.path.exists(fn):
            raise Esceiption('Positional discordance file does not exist! %s' % fn)
        print(fn)

        # read through file and insert records.
        ff = open(fn, 'r')
        header = ff.readline()

        # check header.
        self.check_header(fn, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            print(len(fields))
            if fields[3] != 'inf':
                self.accumulate += "INSERT INTO [bioinfo].dp_positional_discordance VALUES ('%s',%s,%s,%s,%s);\n" % \
                    (lane, fields[0], fields[1], fields[2], fields[3])
                self.insertion_count += 1
                if self.insertion_count > 49:
                    self.commit_accumulated_insertions(debug)
            else:
                print('insert_positional_discordance: Division by zero error! lane=%s position=%s' % (lane, fields[0]))
                print(fields)
        ff.close()
        self.commit_accumulated_insertions(debug)


    def insert_correlated_discordance(self, lane, fn, debug):
        HEADER = ['queryPosition,comparisonPosition,totalDiscBaseCount,correlatedDiscBaseCount,correlatedDiscordanceFraction']

        # check for file.
        if not os.path.exists(fn):
            raise Esceiption('Correlated positional discordance file does not exist! %s' % fn)

        # read through file and insert records.
        ff = open(fn, 'r')
        header = ff.readline()

        # check header.
        self.check_header(fn, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            if fields[4] != 'inf':
                self.accumulate += "INSERT INTO [bioinfo].dp_correlated_positional_discordance VALUES ('%s',%s,%s,%s,%s,%s);\n" % \
                    (lane, fields[0], fields[1], fields[2], fields[3],fields[4])
                self.insertion_count += 1
                if self.insertion_count > 49:
                    self.commit_accumulated_insertions(debug)
            else:
                print('insert_correlated_discordance: Division by zero error! lane=%s queryPosition=%s comparisonPosition=%s' % (lane, fields[0], fields[1]))
                print(fields)
        ff.close()
        self.commit_accumulated_insertions(debug)

    def insert_correlated_base_calls(self, lane, fn, debug):
        HEADER = ['queryPosition','comparisonPosition','totalDiscBaseCount','correlatedBaseCalls','correlatedBaseCallFraction','overAbundantCorrBaseCalls','fracOverAbundantCorrBaseCalls']

        # check for file.
        if not os.path.exists(fn):
            raise Esceiption('Correlated base call file does not exist! %s' % fn)

        # read through file and insert records.
        ff = open(fn, 'r')
        header = ff.readline()

        # check header.
        self.check_header(fn, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            if fields[4] != 'inf' and fields[6] != 'inf':
                self.accumulate += "INSERT INTO [bioinfo].dp_correlated_base_calls VALUES ('%s',%s,%s,%s,%s,%s,%s,%s);\n" % \
                    (lane, fields[0], fields[1], fields[2], fields[3], fields[4], fields[5], fields[6])
                self.insertion_count += 1
                if self.insertion_count > 49:
                    self.commit_accumulated_insertions(debug)
            else:
                print('insert_correlated_base_calls: Division by zero error! lane=%s queryPosition=%s comparisonPosition=%s' % (lane, fields[0], fields[1]))
                print(fields)
        ff.close()
        self.commit_accumulated_insertions(debug)

    def insert_confusion_matrices(self, lane, fn, debug):
        HEADER = ['position','callBase','refBase','count','fraction']

        # check for file.
        if not os.path.exists(fn):
            raise Esceiption('Positional confusion matrices file does not exist! %s' % fn)

        # read through file and insert records.
        ff = open(fn, 'r')
        header = ff.readline()

        # check header.
        self.check_header(fn, header, HEADER)

        # loop through file, read records, and perform inserts.
        for line in ff:
            fields = line.rstrip('\r\n').split(',')
            if fields[4] != 'inf':
                self.accumulate += "INSERT INTO [bioinfo].dp_confusion_matrices VALUES ('%s',%s,'%s','%s',%s,%s);\n" % \
                    (lane, fields[0], fields[1], fields[2], fields[3], fields[4])
                self.insertion_count += 1
                if self.insertion_count > 49:
                    self.commit_accumulated_insertions(debug)
            else:
                print('insert_confusion_matrices: Division by zero error! lane=%s position=%s callBase=%s refBase=%s' % (lane, fields[0], fields[1], fields[2]))
                print(fields)
        ff.close()
        self.commit_accumulated_insertions(debug)


def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-d', '--input-directory', help='path to directory containing discordance pattern output files', dest='input_directory')
    parser.add_option('-l', '--lane',            help='lane to run db uploader on',                                    dest='lane')
    parser.add_option('-c', '--db-user-config',  help='configuration file with db users and passwords',                dest='db_config_file')
    parser.add_option('-t', '--test',           help='debug upload tool, prints insert statements to log file',       dest='debug', action='store_true', default=False)

    # parse command line arguments.
    options, args = parser.parse_args(arguments[1:])

    return options.input_directory, options.lane, options.db_config_file, options.debug

def main(arguments):
    # parse command line arguments.
    inputDirectory, lane, db_config_file, debug = parse_arguments(arguments)

    # read in db user config file.
    dbUserConfig = ConfigParser()
    dbUserConfig.read(db_config_file)

    # initialize db loader.
    dbl = DBLoader(dbUserConfig)
    dbl.connect()

    # insert positional discordance.
    dbl.insert_positional_discordance(lane, pjoin(inputDirectory, lane, 'positional_discordance.csv'), debug)
    dbl.insert_correlated_discordance(lane, pjoin(inputDirectory, lane, 'correlated_discordance.csv'), debug)
    dbl.insert_correlated_base_calls(lane, pjoin(inputDirectory, lane, 'correlated_base_calls.csv'), debug)
    dbl.insert_confusion_matrices(lane, pjoin(inputDirectory, lane, 'positional_confusion_matrices.csv'), debug)

main(sys.argv)
