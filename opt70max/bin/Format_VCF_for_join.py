#!/usr/bin/env python

import os
import sys
import re
import getopt

def main():

    # parse command line options and update variables
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:", [])
    except getopt.GetoptError:
        sys.exit(usage)

    for opt, arg in opts:
        if opt == "-h":
            sys.exit('Use me properly.')
        elif opt == '-i':
            INFILE = arg
        elif opt == '-o':
            OUTFILE = arg
           
    inf = open(INFILE, 'r')
    outf = open(OUTFILE, 'w')

    for line in inf:

        if line.startswith("##"):
            continue

        (chr, pos, ID, ref, alt, qual, filter, info, format, kid, mom, dad) = line.split()
        if chr=="#CHROM":
            chr = "CHROM"
            outf.write( "\t".join( [chr, pos, ID, ref, alt, qual, filter, info, format, kid, mom, dad, "START", "STOP"] ) + "\n" )
            continue

        if not "TYPE" in info:
            varTypes = ['ref']
        else:
            varTypes = re.sub("TYPE=","", [x for x in info.split(';') if "TYPE=" in x][0]).split(",")
         
        ## Calculate start and stop position based on varType
        start = 0
        stop = 0
        pos = int(pos)
        if varTypes == ['other']:
            start = pos
            stop =  re.sub("END=","", [x for x in info.split(';') if "END=" in x][0])
        else:
            if   len([ x for x in varTypes if x in set([ "snp", "mnp", "ref"     ]) ]) > 0:
                start = pos-1
            elif len([ x for x in varTypes if x in set([ "del", "ins", "complex" ]) ]) >0:
                start = pos
            if   len([ x for x in varTypes if x in set([ "del", "mnp", "ref", "complex" ]) ]) > 0:
                stop = pos + len(ref) - 1
            elif len([ x for x in varTypes if x in set([ "snp", "ins"                   ]) ]) > 0:
                stop = pos

        if start==0 and stop==0:
            sys.exit("Unable to determine start and stop position")

        outf.write( "\t".join( [chr, str(pos), ID, ref, alt, qual, filter, info, format, kid, mom, dad, str(start), str(stop)] ) + "\n" )
            
###################################################

if __name__ == "__main__":
    main()
