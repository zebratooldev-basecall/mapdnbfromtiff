#! /usr/bin/env python

import sys
from cgatools.reference import CrrFile
from os.path import join as pjoin
from bz2 import BZ2File


refDir = sys.argv[1]
varType = sys.argv[2]

# Diversity testvariants file, for dbSNP frequency information.
freqFile = pjoin(refDir, 'diversity-unrelated-tv-b37.tsv.bz2')
crr = CrrFile(pjoin(refDir, 'reference.crr'))


def extendRange(crr,chromosome,begin,end,allele):
    if 0 == len(allele):
        return (chromosome,begin,end)

    MAX_EXTEND = 50

    # Circular suffix match to left.
    lSeq = crr.getSequence(chromosome, max(begin-MAX_EXTEND,0), begin)
    for lMatch in xrange(0, len(lSeq)):
        lCh = lSeq[len(lSeq)-lMatch-1]
        lAl = allele[ (MAX_EXTEND*len(allele)-lMatch-1) % len(allele) ]
        if lCh != lAl:
            break

    # Circular prefix match to right.
    cId = crr.getChromosomeId(chromosome)
    rSeq = crr.getSequence(chromosome, end,
                           min(end+MAX_EXTEND,crr.listChromosomes()[cId].length))
    for rMatch in xrange(0, len(rSeq)):
        rCh = rSeq[rMatch]
        rAl = allele[ rMatch % len(allele) ]
        if rCh != rAl:
            break

    return (chromosome, begin-lMatch, end+rMatch)


def fixdash(ss):
    if '-' == ss:
        return ''
    return ss


freq = {}
for vt in [ 'snp', 'ins', 'del', 'sub' ]:
    freq[vt] = {}
ff = BZ2File(freqFile)
hFields = ff.readline().rstrip('\r\n').split('\t')
xrid = hFields.index('xRef')
vtid = hFields.index('varType')
for line in ff:
    fields = line.rstrip('\r\n').split('\t')
    if '' == fields[xrid]:
        continue

    # Pick the first one, avoid double counting.
    db = fields[xrid].split(';')[0]
    if not db.startswith('dbsnp'):
        continue
    idx = db.find(':rs')
    if -1 == idx:
        continue
    db = db[idx+3:]

    counts = { '0':0, '1':0, 'N':0 }
    for field in fields[xrid+1:]:
        for ch in field:
            counts[ch] += 1
    sumcount = counts['0'] + counts['1'] + counts['N']
    if 4*counts['N'] > sumcount or sumcount < 20:
        continue

    freq[fields[vtid]][db] = float(counts['1']) / (sumcount-counts['N'])

ff.close()


fn = pjoin(refDir, 'dbSNP.tsv')
ff = open(fn)
while True:
    header = ff.readline()
    if header.startswith('>snpId') or header.startswith("snpId"):
        break
hFields = header.rstrip('\r\n').split('\t')
chrid = hFields.index('chromosome')
beginid = hFields.index('begin')
endid = hFields.index('end')
varid = hFields.index('variations')
refid = hFields.index('reference')
print 'chromosome\tbegin\tend\tfreq'
for line in ff:
    fields = line.rstrip('\r\n').split('\t')

    if fields[0] not in freq[varType]:
        continue

    vars = fields[varid].split('/')
    if len(vars) != 2:
        continue

    vars = map(fixdash, vars)
    refval = fixdash(fields[refid])
    if refval not in vars:
        continue

    if vars[0] == vars[1]:
        continue

    alt = vars[0]
    if alt == refval:
        alt = vars[1]

    chromosome = fields[chrid]
    if not chromosome.isdigit():
        continue
    chromosome = 'chr' + chromosome

    begin = int(fields[beginid])
    end = int(fields[endid])

    # only look at small var
    if end-begin > 10 or len(alt) > 10:
        continue

    if 'ins' == varType:
        (chromosome,begin,end) = extendRange(crr,chromosome,begin,end,alt)
        if begin > 0:
            begin -= 1
        end += 1

    elif 'del' == varType:
        (chromosome,begin,end) = extendRange(crr,chromosome,begin,end,refval)

    if begin == end:
        if begin > 0:
            begin -= 1
        end += 1

    print '%s\t%d\t%d\t%.2f' % (chromosome,begin,end,freq[varType][fields[0]])
