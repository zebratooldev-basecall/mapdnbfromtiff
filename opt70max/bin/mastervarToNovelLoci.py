#!/usr/bin/env python


import sys
import bz2
from optparse import OptionParser

def masterVarToLoci(infile,outbase):
    # loci file header
    # loci should be 1-based

    # store loci info as list [(chr, offset, ref, alt),...]
    outsnps = []
    outsubs = []

    fi = bz2.BZ2File(infile,'r')
    try:
        for line in fi:
            if line[0] == '#':  continue
            if line == '\n':  continue
            if line[0] == '>':  continue

            if 'dbsnp' not in line:
                line = line.rstrip().split('\t')
                chr, offset = line[2:4]
                offset = int(offset) + 1
                zygosity, vartype = line[5:7]
                print >> sys.stderr, chr, offset, zygosity, vartype
                if zygosity == 'het-ref' and vartype in set(['snp','sub']):
                    ref, alt1, alt2 = line[7:10]
                    try:
                        varFilter1,varFilter2 = line[14:16]
                    except:
                        print line
                        break

                    if varFilter1 == 'PASS' and varFilter2 == 'PASS':
                        if alt1 == ref:
                            alt = alt2
                        elif alt2 == ref:
                            alt = alt1
                        else:
                            print >> sys.stderr, 'neither allele matches ref. is this really a het call?'
                        output = (chr, offset, ref, alt)
                        if vartype == 'sub':
                            outsubs.append(output)
                        elif vartype == 'snp':
                            outsnps.append(output)

    finally:
        fi.close()

    subout = outbase + 'novel_het_sub_loci.tsv'
    snpout = outbase + 'novel_het_snp_loci.tsv'

    write_output(subout,outsubs)
    write_output(snpout,outsnps)

def write_output(outfile,outlist):
    fo = open(outfile,'w')
    try:
        fo.write('>chr\toffset\tref\talt\n')
        for outline in outlist:
            fo.write('\t'.join(map(str,outline)) + '\n')
    finally:
        fo.close()

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-i','--input-file', help='bz2-zipped evidence file from assembly pipeline', dest='infile')
    parser.add_option('-o','--output-base', help='base name for output files', dest='outbase')
    options,args = parser.parse_args(arguments[1:])

    return options.infile, options.outbase

def main(args):
    infile, outbase = parse_arguments(args)

    masterVarToLoci(infile, outbase)
    
if __name__=='__main__':
    main(sys.argv)
