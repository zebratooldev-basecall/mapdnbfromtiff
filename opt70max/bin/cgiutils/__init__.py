import os, sys, imp, subprocess, pwd
from indent import indent
from bundle import bundle
from command import Command, CommandPipeline, CommandError

class Abort(Exception):
    '''Raised when a command detects a fatal error'''

def wraplist(var, varname, etype=str):
    if isinstance(var, etype):
        return [var]
    elif isinstance(var, list):
        bads = [x for x in var if not isinstance(x, etype)]
        if not bads:
            return var
    else:
        bads = [var]
    raise TypeError('%s: expected %s or list of %s, found %s' %
                    (varname, etype.__name__, etype.__name__, type(bads[0]).__name__) )

def makedirs(loc):
    try:
        os.makedirs(loc)
    except OSError, e:
        if not os.path.isdir(loc):
            raise

def makedirsandsymlink(src, dest):
    '''If dest is not already a directory or a symlink to a directory,
    create dest as a symlink to src, which is itself created as a
    directory if it is not already a directory or symlink to a
    directory.'''
    if not os.path.isdir(dest):
        makedirs(src)
        makedirs(os.path.dirname(dest))
        os.symlink(src, dest)

def makeDirsAs(loc, userName):
    args = ['mkdir', '-p', loc]
    child = subprocess.Popen(args,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             **userSwitchParams(userName))
    stdout, stderr = child.communicate(None)
    if child.returncode != 0:
        raise RuntimeError('mkdir failed: ' + stderr)

def makeFileAs(loc, data, userName):
    import os, subprocess
    child = subprocess.Popen("cat > '%s'" % loc,
                             shell=True,
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             **userSwitchParams(userName))
    stdout, stderr = child.communicate(data)
    if child.returncode != 0:
        raise RuntimeError('file creation failed: ' + stderr)

def loadModule(codePath):
    assert codePath
    try:
        codeDir = os.path.dirname(codePath)
        codeFile = os.path.basename(codePath)
        moduleName,extension = os.path.splitext(codeFile)
        sys.path.append(codeDir)
        try:
            fin = open(codePath, 'r')
        except IOError,e:
            raise RuntimeError('cannot load module "%s": %s' % (codePath, str(e)))
        return  imp.load_source(moduleName, codePath, fin)
    finally:
        try: fin.close()
        except: pass

def writePidFile(fn):
    f = open(fn, 'w')
    print >>f, os.getpid()
    f.close()

def daemonize():
    """
    Detach a process from the controlling terminal and run it in the
    background as a daemon.

    See http://code.activestate.com/recipes/278731/
    """
    if os.name != 'posix':
        raise Exception('cannot run as daemon on this platform: ' + os.name)

    def toChild():
        """Fork and exit the original process"""
        try:
            pid = os.fork()
        except OSError, e:
            raise Exception('%s [%d]' % (e.strerror, e.errno))
        if pid != 0:
            os._exit(0)

    toChild() # fork once
    os.setsid()
    toChild() # fork twice

    # close all files
    for fd in range(0, 1024):
        try:
            os.close(fd)
        except OSError:
            pass

    # redirect input/output to nothing
    newStdIn = getattr(os, 'devnull', '/dev/null')
    os.open(newStdIn, os.O_RDWR)
    os.dup2(0, 1)
    os.dup2(0, 2)
