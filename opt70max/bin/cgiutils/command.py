#! /usr/bin/env python2.6

import errno, fcntl, os, signal, subprocess, sys
from cgiutils import bundle


SIGNAL_NAMES = dict((k, v) for v, k in signal.__dict__.iteritems() if v.startswith('SIG') and not v.startswith('SIG_'))


class CommandErrorData(object):
    """Contains information regarding the failure of a single Command.

    Members:
    message -- A message describing what happened.
    command -- The Command object that failed, or None if this object
               is not associated with any particular command.
    sts     -- The exit status of the command, or -SIGNUM if the command
               failed due to signal, or None if the command was never run."""

    def __init__(self, message, command, sts):
        self.message = message
        self.command = command # The Command object, or None if not associated with a Command.
        self.sts = sts # The exit status of the forked proc, or None if proc wasn't forked.


class CommandError(Exception):
    """Raised when a running of a Command or CommandPipeline fails.

    Has a message (string) member and an errors member, which is a
    list of CommandErrorData for each Command that failed."""

    def __init__(self, message, errors=[]):
        Exception.__init__(self, message)
        self.errors = errors


def getExceptionMessage(ee):
    return ee.__class__.__name__ + ': ' + str(ee)


def subprocess_setup(preexec_fn, fdToClose):
    # From http://www.chiark.greenend.org.uk/ucgi/~cjwatson/blosxom/2009-07-02-python-sigpipe.html
    # Python installs a SIGPIPE handler by default. This is usually not what
    # non-Python subprocesses expect.
    signal.signal(signal.SIGPIPE, signal.SIG_DFL)

    if preexec_fn is not None:
        preexec_fn()

    for fd in fdToClose:
        try:
            os.close(fd)
        except OSError, ee:
            if ee.errno != errno.EBADF:
                print >>sys.stderr, 'failed to close fd %d: %s' % (fd, getExceptionMessage(ee))
                os._exit(1)
        except Exception, ee:
            print >>sys.stderr, 'failed to close fd %s: %s' % (fd, getExceptionMessage(ee))
            os._exit(1)


try:
    # If ctypes is supported (python 2.5+), we can do this "fast"
    # version of determining if a fd is valid. Can really speed up
    # CommandPipeline.
    import ctypes
    rawfcntl = ctypes.cdll.LoadLibrary("libc.so.6").fcntl
    def is_valid_fd(fd):
        return -1 != rawfcntl(fd, fcntl.F_GETFL, 0)
except:
    def is_valid_fd(fd):
        try:
            fcntl.fcntl(fd, fcntl.F_GETFL)
            return True
        except IOError, ee:
            return False


class Command(object):
    """Like subprocess.Popen, but with subprocess redirection.

    Command is a class used to run commands. It supports the same
    keyword options as subprocess.Popen. Additionally, it supports the
    following arguments:

    keep_fds -- List of file descriptors to keep open in the child.
    ok_sts   -- List of command exit statuses to consider \"success\".
    pipein   -- Dictionary of key:Command or key:CommandPipeline for input process substitution.
    pipeout  -- Dictionary of key:Command or key:CommandPipeline for output process substitution.

    Also, stdin, stdout, and stderr may be strings, in which case they
    are interpreted as a filename to use as file redirection in the subprocess.

    Unlike Popen, a Command is not started immediately when the
    Command is constructed. Instead, you must use a Command as part of
    a CommandPipeline, or use Command().run() as a shorthand for
    CommandPipeline([Command()]).run(). If a Command or any subprocess
    redirection command exits with a status not considered a
    \"success\", an exception is thrown.

    Here are some example bash commands, and their equivalent with
    Command (except that Command is sensitive to errors in the
    subprocess redirection subprocess, while bash is not, and Command
    waits for subprocesses to finish, while bash may not):

    bash:   true
    python: Command('true').run()

    bash:   cat <(sort foo) >foo_sorted
    python: Command(['cat', '%(sortcmd)s'],
                    pipein={'sortcmd':Command(['sort', 'foo'])},
                    stdout='foo_sorted').run()

    bash:   tee plain.txt >(sort >sorted.txt) in.txt
    python: Command(['tee', 'plain.txt', '%(sortcmd)s'],
                    pipeout={'sortcmd':Command(['sort'], stdout='sorted.txt')},
                    stdin='in.txt').run()

    See CommandPipeline for more details.
    """

    def __init__(self, args, close_fds = True, keep_fds = [ 0, 1, 2],
                 ok_sts = [ 0, -signal.SIGPIPE], pipein = {}, pipeout = {}, **kwargs):
        self.args    = args
        self.close_fds = close_fds
        self.keep_fds = keep_fds
        self.ok_sts = ok_sts
        self.pipein  = self._sanitizePipeCommands(pipein)
        self.pipeout = self._sanitizePipeCommands(pipeout)
        self.kwargs = kwargs
        self.stdin  = None
        self.stdout = None
        self.stderr = None

    def run(self):
        CommandPipeline([self]).run()

    def isBad(self, sts):
        return sts not in self.ok_sts

    def _sanitizePipeCommands(self, dpipe):
        dpipe = dpipe.copy()
        for (key,commands) in dpipe.iteritems():
            if isinstance(commands, Command) or isinstance(commands, CommandPipeline):
                dpipe[key] = commands
            elif isinstance(commands, list) and [cmd for cmd in commands if isinstance(cmd, str)]:
                dpipe[key] = Command(commands)
            else:
                dpipe[key] = CommandPipeline(commands)
        return dpipe

    def _start(self, procs, errors, **kwargs):
        inProcs = {}
        outProcs = {}
        files = []
        try:
            # Subprocess redirection.
            for (key, cmd) in self.pipein.iteritems():
                cmd._start(procs, errors, stdout=subprocess.PIPE)
                inProcs[key] = procs[-1]
            for (key, cmd) in self.pipeout.iteritems():
                cmd._start(procs, errors, stdin=subprocess.PIPE)
                outProcs[key] = procs[-1]
            args = self._getSubstitutedCommand(inProcs, outProcs)

            # The main proc.
            try:
                kwargs = self._mergekwargs(kwargs)

                # Behavior is to keep only keep_fds if close_fds is
                # set. If there are subprocess redirections, we still
                # have to ensure the fds involved in that redirection
                # are not closed, so in that case we have to handle fd
                # closing ourselves.
                fdToClose = self._getFdToClose(inProcs, outProcs)

                # Wrap any user-defined preexec_fn in our own.
                preexec_fn = kwargs.get('preexec_fn')
                kwargs['preexec_fn'] = lambda : subprocess_setup(preexec_fn, fdToClose)

                cmddata = []
                if 'stdin' in kwargs and isinstance(kwargs['stdin'], str):
                    cmddata.append('stdin='+kwargs['stdin'])
                    self._ropen(files, kwargs, 'stdin')
                if 'stdout' in kwargs and isinstance(kwargs['stdout'], str):
                    cmddata.append('stdout='+kwargs['stdout'])
                    self._wopen(files, kwargs, 'stdout')
                if 'stderr' in kwargs and isinstance(kwargs['stderr'], str):
                    cmddata.append('stderr='+kwargs['stderr'])
                    self._wopen(files, kwargs, 'stderr')

                print >>sys.stderr, '+ starting command: %s %s' % (str(args), ' '.join(cmddata))
                proc = subprocess.Popen(args, **kwargs)
                self.stdin  = proc.stdin
                self.stdout = proc.stdout
                self.stderr = proc.stderr
                procs.append(bundle(proc=proc, command=self, args=args))

            except Exception, ee:
                import traceback
                traceback.print_exc()
                message = 'command failed: %s: %s' % (str(args),getExceptionMessage(ee))
                errors.append( CommandErrorData(message, self, None) )

        finally:
            # Close pipes and files we've opened for this Command.
            for (key,proc) in inProcs.iteritems():
                try:
                    proc.proc.stdout.close()
                except Exception, ee:
                    message = 'failed to close subprocess stdout: %s' % getExceptionMessage(ee)
                    errors.append( CommandErrorData(message, self, None) )
            for (key,proc) in outProcs.iteritems():
                try:
                    proc.proc.stdin.close()
                except Exception, ee:
                    message = 'failed to close subprocess stdin: %s' % getExceptionMessage(ee)
                    errors.append( CommandErrorData(message, self, None) )
            for (fn,fil) in files:
                try:
                    fil.close()
                except Exception, ee:
                    message = 'failed to close %s: %s' % (fn,getExceptionMessage(ee))
                    errors.append( CommandErrorData(message, self, None) )
            if 0 != len(errors):
                raise Exception(errors[-1].msg)

    def _ropen(self, files, kwargs, key):
        fn = kwargs[key]
        ff = open(fn)
        files.append( (fn, ff) )
        kwargs[key] = ff

    def _wopen(self, files, kwargs, key):
        fn = kwargs[key]
        if fn.startswith('>>'):
            ff = open(fn[2:], 'a')
        else:
            ff = open(fn, 'w')
        files.append( (fn, ff) )
        kwargs[key] = ff

    def _mergekwargs(self, kwargs):
        result = self.kwargs.copy()
        for (key,value) in kwargs.iteritems():
            assert key in [ 'stdin', 'stdout' ]
            if key in result:
                if key == 'stdin':
                    raise Exception('stdin override is illegal for CommandPipeline commands, except for first command')
                elif key == 'stdout':
                    raise Exception('stdout override is illegal for CommandPipeline commands, except for last command')
            result[key] = value
        return result

    def _getSubstitutedCommand(self, inProcs, outProcs):
        args = self.args
        if isinstance(args, str):
            return self._getSubstitutedCommandArg(args, inProcs, outProcs)
        return [ self._getSubstitutedCommandArg(arg, inProcs, outProcs) for arg in args ]

    def _getSubstitutedCommandArg(self, arg, inProcs, outProcs):
        dsub = {}
        for (key, proc) in inProcs.iteritems():
            dsub[key] = '/dev/fd/%d' % proc.proc.stdout.fileno()
        for (key, proc) in outProcs.iteritems():
            dsub[key] = '/dev/fd/%d' % proc.proc.stdin.fileno()
        if len(dsub) != 0:
            arg = arg % dsub
        return arg

    def _getFdToClose(self, inProcs, outProcs):
        if not self.close_fds:
            return []

        keep_fds = self.keep_fds[:]
        for proc in inProcs.itervalues():
            keep_fds.append(proc.proc.stdout.fileno())
        for proc in outProcs.itervalues():
            keep_fds.append(proc.proc.stdin.fileno())
        keep_fds = frozenset(keep_fds)

        # In the subprocess, if we close all but the keep_fds above,
        # we may close any pipes created by Popen() itself, which is
        # not desirable. Thus we must compute the set of fds to close
        # right now before Popen.
        fdToClose = []
        MAXFD = os.sysconf('SC_OPEN_MAX')
        for fd in xrange(0, MAXFD+1):
            if fd not in keep_fds and is_valid_fd(fd):
                fdToClose.append(fd)
        return fdToClose


class CommandPipeline(object):
    """Used to run a list of Commands as a command pipeline.

    CommandPipeline is a class used to run command pipelines. For
    example, the following is analogous to the bash (except a
    CommandPipeline throws an exception if any Command in the pipeline
    fails -- see ok_sts argument to Command()):

    bash:   cat in | sort >out
    python: CommandPipeline(Command(['cat', 'in']),
                            Command(['sort'], stdout='out')).run()

    One may also do pipe redirections to this python process, as in:

    pp = CommandPipeline([Command(['sort'], stdin=subprocess.PIPE),
                          Command(['tr', 'a', 'A'], stdout=subprocess.PIPE)])
    pp.start()
    pp.stdin.write('c\\na\\nb\\n')
    pp.stdin.close()
    result = pp.stdout.read()
    pp.wait()
    assert result == 'A\\nb\\nc\\n'

    Note that CommandPipeline().run() is equivalent to:

    pp = CommandPipeline()
    pp.start()
    pp.wait()

    The CommandPipeline.start() method starts the command pipeline,
    and the CommandPipeline.wait() method waits for the
    CommandPipeline to complete, and cleans up all subprocesses.
    """

    def __init__(self, commands):
        self.commands = []
        for command in commands:
            if isinstance(command, Command):
                self.commands.append(command)
            elif isinstance(command, CommandPipeline):
                self.commands.append(command)
            else:
                self.commands.append(Command(command))
        self.procs  = []
        self.errors = []
        self.stdin  = None
        self.stdout = None
        self.stderr = None

    def run(self):
        self.start()
        self.wait()

    def start(self): 
        if len(self.procs) != 0:
            raise Exception('CommandPipeline already running')
        self.procs  = []
        self.errors = []
        self.stdin  = None
        self.stdout = None
        self.stderr = None
        try:
            self._start(self.procs, self.errors)
        except Exception, ee:
            if len(self.errors) == 0:
                message = getExceptionMessage(ee)
                self.errors.append( CommandErrorData(message, None, None) )
            self.wait()
            assert False, 'Expected wait to fail due to errors.'

    def wait(self):
        for proc in reversed(self.procs):
            try:
                sts = proc.proc.wait()
                if proc.command.isBad(sts):
                    statusString = str(sts)
                    if -sts in SIGNAL_NAMES:
                        statusString += ' (%s)' % SIGNAL_NAMES[-sts]
                    message = 'command failed: %s: exit status %s' % (str(proc.args), statusString)
                    self.errors.append( CommandErrorData(message, proc.command, sts) )
            except Exception, ee:
                message = 'command failed: %s: %s' % (str(proc.args), getExceptionMessage(ee))
                self.errors.append( CommandErrorData(message, proc.command, None) )

        # Need to close all subprocess.PIPE that aren't internal to
        # the CommandPipeline.
        for proc in reversed(self.procs):
            if proc.proc.stdin is not None:
                try:
                    proc.proc.stdin.close()
                except Exception, ee:
                    message = 'failed to close subprocess stdin: %s' % getExceptionMessage(ee)
                    errors.append( CommandErrorData(message, self, None) )
            if proc.proc.stdout is not None:
                try:
                    proc.proc.stdout.close()
                except Exception, ee:
                    message = 'failed to close subprocess stdout: %s' % getExceptionMessage(ee)
                    errors.append( CommandErrorData(message, self, None) )
            if proc.proc.stderr is not None:
                try:
                    proc.proc.stderr.close()
                except Exception, ee:
                    message = 'failed to close subprocess stderr: %s' % getExceptionMessage(ee)
                    errors.append( CommandErrorData(message, self, None) )

        self.procs = []
        if len(self.errors) != 0:
            if 1 == len(self.errors):
                message = self.errors[0].message
            else:
                message = 'command pipeline failed:\n  %s' % '\n  '.join([error.message for error in self.errors])
            ee = CommandError(message, self.errors)
            raise ee

    def _start(self, procs, errors, **kwargs):
        prevProc = None
        try:
            for ii in xrange(len(self.commands)):
                command = self.commands[ii]

                cmdArgs = kwargs.copy()
                if prevProc is not None:
                    cmdArgs['stdin']  = procs[-1].proc.stdout
                if ii != len(self.commands)-1:
                    cmdArgs['stdout'] = subprocess.PIPE

                command._start(procs, errors, **cmdArgs)

                if 0 == ii:
                    self.stdin = procs[-1].proc.stdin
                if ii == len(self.commands)-1:
                    self.stdout = procs[-1].proc.stdout
                    self.stderr = procs[-1].proc.stderr
                if prevProc is not None:
                    prevProc.proc.stdout.close()
                if ii != len(self.commands)-1:
                    prevProc = procs[-1]

        finally:
            if prevProc is not None and prevProc.proc.stdout is not None:
                prevProc.proc.stdout.close()


def check_file(fn, contents):
    ff = open(fn)
    try:
        assert ff.read() == contents
    finally:
        ff.close()


def test_exception(cmd, message=None):
    try:
        cmd.run()
        assert False, 'expected command to fail'
    except Exception, ee:
        if message is not None:
            assert ee.args[0] == message, ee.args


def count_fd():
    MAXFD = os.sysconf('SC_OPEN_MAX')
    count = 0
    for fd in xrange(0, MAXFD+1):
        if is_valid_fd(fd):
            count += 1
    return count


def test_command_pipeline():
    import tempfile, shutil
    mydir = tempfile.mkdtemp()
    print >>sys.stderr, 'temp dir:', mydir
    try:
        os.chdir(mydir)

        fd_orig = count_fd()

        # simple commands
        Command(['echo', 'abc']).run()
        CommandPipeline([ ['echo', 'abc'] ]).run()

        assert fd_orig == count_fd()

        # simple command pipelines
        CommandPipeline([ ['echo', 'abc'],
                          Command(['tr', 'a', 'A'], stdout='abc.txt') ]).run()
        check_file('abc.txt', 'Abc\n')
        CommandPipeline([ Command(['cat'], stdin='abc.txt'),
                          Command(['tr', 'A', 'G']),
                          Command(['tr', 'c', 'D'], stdout='def.txt') ]).run()
        check_file('def.txt', 'GbD\n')

        assert fd_orig == count_fd()

        # simple input process substitution
        Command(['cat', '%(echo1)s', '%(echo2)s'],
                stdout='ghi.txt',
                pipein={'echo1':['echo', '1'],
                        'echo2':['echo', '2']}).run()
        check_file('ghi.txt', '1\n2\n')

        assert fd_orig == count_fd()

        # simple output process substitution
        Command(['tee', '%(cat1)s', '%(cat2)s'],
                stdin='ghi.txt',
                stdout='z.txt',
                pipeout={'cat1':Command(['cat'], stdout='y.txt'),
                         'cat2':CommandPipeline([Command(['cat'], stdout='x.txt')])}).run()
        check_file('x.txt', '1\n2\n')
        check_file('y.txt', '1\n2\n')
        check_file('z.txt', '1\n2\n')

        assert fd_orig == count_fd()

        # pipe to CommandPipeline
        pp = CommandPipeline([Command(['cat'], stdin=subprocess.PIPE, stdout='zz')])
        pp.start()
        pp.stdin.write('blah\n')
        pp.stdin.close()
        pp.wait()
        check_file('zz', 'blah\n')

        assert fd_orig == count_fd()

        # pipe to/from CommandPipeline
        pp = CommandPipeline([Command(['sort'], stdin=subprocess.PIPE),
                              Command(['tr', 'a', 'A'], stdout=subprocess.PIPE)])
        pp.start()
        pp.stdin.write('c\na\nb\n')
        pp.stdin.close()
        result = pp.stdout.read()
        pp.wait()
        assert result == 'A\nb\nc\n'

        assert fd_orig == count_fd()

        # check if SIGPIPE is ok
        aa = open('aa', 'w')
        for ii in xrange(10000):
            aa.write('abcdefhijklmnopqrstuvwxyz\n')
        aa.close()
        CommandPipeline([ ['cat', 'aa'],
                          ['head', '-1'] ]).run()

        assert fd_orig == count_fd()

        # some simple failures
        test_exception(CommandPipeline([ ['true'],
                                         ['false'] ]), "command failed: ['false']: exit status 1")
        test_exception(CommandPipeline([ ['false', '1'],
                                         ['false', '2'] ]), """command pipeline failed:
  command failed: ['false', '2']: exit status 1
  command failed: ['false', '1']: exit status 1""")

        assert fd_orig == count_fd()

        import time
        start = time.time()
        for ii in xrange(1000):
            Command(['true'], close_fds=False).run()
        print 'spawned 1000 procs in %.3f seconds' % (time.time()-start)
    finally:
        shutil.rmtree(mydir)

if __name__ == '__main__':
    test_command_pipeline()
