#!/usr/bin/python
import sys, os, logging, inspect, pdb
from cgiutils import bundle
import jsonutil

class Param(bundle):
    def __init__(self, name, p):
        self.name = name
        self.p = p

class Stage(bundle):
    def __init__(self, name, depends=None):
        self.name = name
        if depends:
            lst = [ x.strip() for x in depends.split(',') if x.strip() ]
            if lst:
                self.depends = ','.join(lst)
            else:
                self.depends = None
        else:
            self.depends = None

class Workflow(object):
    INIT = bundle(paramOverrides=[])

def shellEscape(ss):
    return ss.replace('"', '\\"')

def addShellArrayProlog(arrayParamValues, taskCount, lines, dereferenceArray=True):
    lines.append('#$ -t 1-%d' % taskCount)
    for key in arrayParamValues[0].keys():
        lines.append( key + '_vals=( "" ' +
                       ' '.join(['"' + shellEscape(p[key]) + '"' for p in arrayParamValues]) + \
                       ' )' )
        if dereferenceArray:
            lines.append( '%s=${%s_vals[$SGE_TASK_ID]}' % (key,key) )

def combineInitBundles(klass):
    assert Workflow in inspect.getmro(klass)
    state = bundle()
    bases = [ cls for cls in klass.__bases__ if
                Workflow in inspect.getmro(cls) and hasattr(cls, 'INIT') ]
    # Recurcively accumulate INITs of direct parents. Parents must not have
    # conflicts in values of INIT vars they add, so checkCompat is True.
    for i, base in enumerate(bases):
        try:
            state.rupdate(combineInitBundles(base), checkCompat=True, keyPath=base.__name__+'.INIT')
        except Exception, e:
            raise Exception(
                "%s\n  While combining bases of %s: %s.INIT --> %s.INIT" % (e, klass, base, bases[0:i]))
    # Update accumulated parents' INIT with child's INIT. Child can freely
    # override a parent's INIT var to whatever it wants, so checkCompat is False.
    if hasattr(klass, 'INIT'):
        state.rupdate(klass.INIT, checkCompat=False, keyPath=klass.__name__+'INIT')
    return state

def createWorkflowObject(workflowClass, state):
    wf = workflowClass()
    wf.__dict__.clear()
    wf.__dict__.update(state)
    return wf

def runStageCallback(workflowClass, methodName, input, debug=False, overridePrint=True):
    class LogWriter(object):
        def __init__(self, impl):
            self.impl = impl

        def write(self, text):
            if text[-1] == '\n':
                text = text[:-1]
            self.impl.log.info(text)

    from impldata import StageImplData

    wf = createWorkflowObject(workflowClass, input)
    impl = StageImplData(wf)

    method = getattr(wf, methodName)

    impl.log.info('starting callback: %s', methodName)
    if debug:
        pdb.runcall(method, impl)
    else:
        try:
            stdout, stderr = sys.stdout, sys.stderr
            try:
                if overridePrint:
                    sys.stdout, sys.stderr = LogWriter(impl), LogWriter(impl)
                method(impl)
            finally:
                sys.stdout, sys.stderr = stdout, stderr
        except Exception, e:
            impl.log.exception('callback failed: %s' % e)
            raise
    impl.log.info('callback completed successfully: %s', methodName)

    state = bundle(wf.__dict__)
    actions = bundle(files=impl.files, directories=list(impl.directories),
                     allocDirectories=impl.allocDirectories,
                     steps=impl.steps, services=impl.services,
                     notifications=impl.notifications)
    return (state, actions)


##########################################################################
# Functions strictly for workflow self-tests
##########################################################################


def _printBanner(ostr, text, lines = 1, width = 80):
    print >>ostr
    for i in range(lines):
        print >>ostr, '*' * 80
    print >>ostr, '*' + text.center(80)
    for i in range(lines):
        print >>ostr, '*' * 80


def _doTestStageCallback(workflowClass, method, startState, printStateDelta):
    import sys
    import jsonutil
    from copy import deepcopy

    startStateDeepCopy = deepcopy(startState)

    os = sys.stderr

    state, actions = runStageCallback(workflowClass, method, startState, overridePrint=False)

    if actions.steps:
        print >>os, '\n>>> STAGE ' + method + ' STEPS:'
        for s in actions.steps:
            print >>os, '>>>> step %s.%s -> %s' % (method, s.name, ', '.join(s.depends) or 'none')
            print >>os, '>>>>     [hog=%s, memory=%d, jobs=%d, wf tasks per grid task=%d]' % \
                ( s.hog, s.memory, len(s.arrayParamValues) or 1, s.workflowTasksPerGridTask )
            if s.arrayParamValues:
                print >>os, s.arrayParamValues
            print >>os, s.commands

    if actions.directories:
        print >>os, '\n>>> STAGE ' + method + ' DIRECTORIES:'
        for d in sorted(actions.directories):
            print >>os, '>>> %s dir: %s' % ( method, d )

    if actions.files:
        print >>os, '\n>>> STAGE ' + method + ' FILES:'
        for f in sorted(actions.files.keys()):
            print >>os, '>>> %s file: %s' % ( method, f )

    if actions.notifications:
        print >>os, '\n>>> STAGE ' + method + ' AAA NOTIFICATIONS:'
        for n in sorted(actions.notifications):
            print >>os, '>>> %s AAA notification: %s' % ( method, n )

    if printStateDelta:
        stateDiff = jsonutil.flatDiff(startStateDeepCopy, state, 'self')
        print >>os, '\n>>> STAGE ' + method + ' STATE DELTA:'
        print >>os, stateDiff.dumpsAdded('>>> ' + method + ' added state: %s = %s')
        print >>os, stateDiff.dumpsDeleted('>>> ' + method + ' deleted state: %s = %s')
        print >>os, stateDiff.dumpsChanged('>>> ' + method + ' changed state: %s = %s -> %s')

    return state


def testStageCallback(workflowClass, method, inpState, printStateDelta=True):
    logging.basicConfig(level=logging.DEBUG,
                        format='%(levelname)3s %(asctime)s.%(msecs)03d %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    ostr = sys.stderr

    _printBanner(ostr, 'TESTING STAGE %s.%s' % ( workflowClass.__name__, method ))

    if isinstance(inpState, str):
        inpState = jsonutil.loads(inpState)
    if 'cgiHome' in inpState:
        inpState.cgiHome = os.path.expandvars(inpState.cgiHome)

    startState = combineInitBundles(workflowClass)
    startState.rupdate(inpState)
    endState = _doTestStageCallback(workflowClass, method, startState, printStateDelta)

    return endState


class StageStateSrc:
    PREV_STAGE = 1
    STAGE_DEPS = 2


def testWorkflowCallbacks(
    workflowClass,
    overrideState="{}",
    emulatedStageOutputs={'': '{}'},
    stageStateSrc=StageStateSrc.PREV_STAGE,
    printStateDeltas=True,
    printWfInOutStates=True):

    ostr = sys.stderr

    wf = workflowClass
    wfName = wf.__name__
    if isinstance(overrideState, str):
        overrideState = jsonutil.loads(overrideState)
    if 'cgiHome' in overrideState:
        overrideState.cgiHome = os.path.expandvars(overrideState.cgiHome)

    logging.basicConfig(level=logging.DEBUG,
                        format='%(levelname)3s %(asctime)s.%(msecs)03d %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    _printBanner(ostr, 'TESTING WORKFLOW %s' % wfName, 2)

    print >>ostr, '\nLIST OF STAGES:\n'
    for st in wf.STAGES:
        print >>ostr, st.name, '->', st.depends

    startState = combineInitBundles(wf)
    startState.rupdate(overrideState)

    # Initialize stages' output states
    outStates = {
        'init': startState
    }

    # Print initial state (JSON)
    if printWfInOutStates:
        print >>ostr, '\nWORKFLOW ' + wfName + ' INIT JSON STATE:'
        print >>ostr, jsonutil.dumps(outStates['init'])

    # Print initial state in flattened format to set a reference for following deltas.
    if printStateDeltas:
        print >>ostr, '\n>>> WORKFLOW ' + wfName + ' INIT FLAT STATE:\n'
        initStateFlat = jsonutil.flatten(outStates['init'], 'self')
        for ( prop, val ) in sorted(initStateFlat):
            print >>ostr, '>>> %s init state: %s = %s' % ( wfName, prop, val )

    # Iterate over workflow's stages, feeding output state(s) of the source
    # stage(s) to the target stage.
    for idx, st in enumerate(wf.STAGES):
        _printBanner(ostr, 'TESTING STAGE %s.%s' % ( workflowClass.__name__, st.name ))

        # Identify source stages for the current stage. In PREV_STAGE mode, it's
        # the previous stage, or 'init' for the first stage. In STAGE_DEPS mode,
        # it's the stage's dependencies, or 'init' if the stage hase no dependencies.
        inpStateSrcs = None
        if stageStateSrc == StageStateSrc.PREV_STAGE:
            inpStateSrcs = (idx > 0) and [ wf.STAGES[idx - 1].name ] or [ 'init' ]
        elif stageStateSrc == StageStateSrc.STAGE_DEPS:
            inpStateSrcs = st.depends and st.depends.split(',') or [ 'init' ]
        else:
            raise Exception("Unrecognized stageStateSrc parameter")

        # Copy over the state(s) of the source stage(s) to the input state of the
        # current stage.
        inpState = bundle()
        for srcSt in inpStateSrcs:
            # Remove step specification, if present.
            srcSt = srcSt.split('.')[0].strip()
            inpState.rupdate(outStates[srcSt])

        # Run the stage's callback, or emulate the stage by copying over the
        # provided emulated output state.
        outState = None
        if st.name in emulatedStageOutputs:
            print >>ostr, 'WARNING: STAGE ' + st.name + ' IS BEING EMULATED\n'
            outState = inpState
            outState.rupdate(jsonutil.loads(emulatedStageOutputs[st.name]))
        else:
            outState = _doTestStageCallback(
                wf, 'impl' + st.name, inpState, printStateDeltas)
        # Save the state to the map of states for later use by dependent stages.
        outStates[st.name] = outState

    lastStageState = outStates[wf.STAGES[-1].name]

    _printBanner(ostr, 'DONE TESTING WORKFLOW ' + wfName, 2)

    # Print the final workflow state (last stage's state).
    if printWfInOutStates:
        print >>ostr, '\nWORKFLOW ' + wfName + ' OUTPUT JSON STATE:'
        print >>ostr, jsonutil.dumps(lastStageState)

    # Print the total workflow state flattened delta.
    if printStateDeltas:
        stateDiff = \
            jsonutil.flatDiff(overrideState, outStates[wf.STAGES[-1].name], 'self')
        print >>ostr, '\n>>> WORKFLOW ' + wfName + ' FLAT STATE DELTA:'
        print >>ostr, stateDiff.dumpsAdded('>>> ' + wfName + r' added state: %s = %s')
        print >>ostr, stateDiff.dumpsDeleted('>>> ' + wfName + r' deleted state: %s = %s')
        print >>ostr, stateDiff.dumpsChanged('>>> ' + wfName + r' changed state: %s = %s -> %s')

    return bundle(lastStageState)
