#! /usr/bin/env python

import optparse


def addColumn(infile, outfile, name, value):
    inf = open(infile)
    outf = open(outfile, "w")

    while True:
        line = inf.readline()
        if line[0] == ">":
            break
        outf.write(line)

    line = line.rstrip('\r\n')
    outf.write(line + '\t' + name + '\n')

    for line in inf:
        line = line.rstrip('\r\n')
        outf.write(line + '\t' + value + '\n')

    inf.close()
    outf.close()

    
def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.add_option('--input', default=None,
                      help='Input tsv file')
    parser.add_option('--output', default=None,
                      help='Output tsv file')
    parser.add_option('--name', default=None,
                      help='Name of additional column')
    parser.add_option('--value', default='',
                      help='Data value for additional column')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if options.input is None or options.output is None or options.name is None:
        parser.error('required params: input, output, name')

    addColumn(options.input, options.output, options.name, options.value)


if __name__ == '__main__':
    main()
