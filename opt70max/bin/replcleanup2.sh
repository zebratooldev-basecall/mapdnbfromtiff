#! /bin/bash

wd="$1"

cd "$wd"

for x in $(ls ASM/variations/annotated/Variations10-20_ccf100.tsv* \
    ASM/variations/annotated/VariationsVQHIGH.tsv* \
    ASM/variations/annotated/Variations20-40_ccf100.tsv* \
    $(find ASM/callrpt -name \*.csv) \
    ASM/coverage/*.csv ASM/coverage/overflow/* \
    ASM/range-*/VariationIntervals.csv ASM/range-*/NoCallIntervals.csv); do
  mkdir -p BAK/$(dirname $x)
  mv ${x} BAK/${x}
#   echo ${x}
done

set -x

rm -rf ASM
mv BAK/ASM ASM
rmdir BAK

for x in $(ls ASM/variations/annotated/VariationsVQHIGH.tsv \
    ASM/variations/annotated/Variations20-40_ccf100.tsv); do
  gzip -4 ${x}
done

rm -rf GAPS
rm -rf EXP/package/*/MAP
rm -rf profs/asm profs/map profs/asmsum profs/mapsum profs/varcall
rm -rf logs
# rm -rf MAP
