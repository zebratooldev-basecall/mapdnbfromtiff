#! /bin/bash

VER=$(echo trio/GS19240-B36-*-ASM)
VER=${VER#trio/GS19240-B36-}
VER=${VER%-ASM}

trio.py --output=$PWD/trio/GS19240-B36-${VER}-ASM/trio-v-baseline \
        --child=$PWD/trio/GS19240-B36-${VER}-ASM \
        --mother=/rnd/home/jbaccash/archive/qual/trio-baseline-36/var-GS00028-DNA_A01-ASM.tsv \
        --father=/rnd/home/jbaccash/archive/qual/trio-baseline-36/var-GS00028-DNA_B01-ASM.tsv
trio.py --output=$PWD/trio/GS19240-B36-${VER}-ASM/trio-nobaseline \
        --child=$PWD/trio/GS19240-B36-${VER}-ASM \
        --mother=$PWD/trio/GS19238-B36-${VER}-ASM/ASM/variations/annotated/Variations20-40_ccf100.tsv \
        --father=$PWD/trio/GS19239-B36-${VER}-ASM/ASM/variations/annotated/Variations20-40_ccf100.tsv

trio.py --output=$PWD/trio/GS19240-B37-${VER}-ASM/trio-v-baseline \
        --child=$PWD/trio/GS19240-B37-${VER}-ASM \
        --mother=/rnd/home/jbaccash/archive/qual/trio-baseline-37/var-GS00028-DNA_A01-ASM.tsv \
        --father=/rnd/home/jbaccash/archive/qual/trio-baseline-37/var-GS00028-DNA_B01-ASM.tsv \
        --include-range=build37

snpdiff.py --output=$PWD/trio/GS19240-B36-${VER}-ASM/snpdiff \
           --asm=$PWD/trio/GS19240-B36-${VER}-ASM \
           --genotype-files=/rnd/home/jbaccash/archive/qual/snpdiff-36/rel24/NA19240.unique-positions-noPAR.txt:/rnd/home/jbaccash/archive/qual/snpdiff-36/rel24/NA19240.unique-positions-noPAR.infinium.txt

# NA19240 Calldiff v baseline
mkdir $PWD/trio/GS19240-B36-${VER}-ASM/calldiff-v-baseline
rat run "/Proj/QA/Release/cgatools/1.3.0/1.3.0.9/binaries/bin/cgatools calldiff --reference=/rnd/home/jbaccash/archive/cgatools-stage/crr/build36.crr --output-prefix=$PWD/trio/GS19240-B36-${VER}-ASM/calldiff-v-baseline/ --variantsA=/rnd/home/jbaccash/archive/qual/trio-baseline-36/var-GS00028-DNA_C01-ASM.tsv --variantsB=$PWD/trio/GS19240-B36-${VER}-ASM/ASM/variations/annotated/Variations20-40_ccf100.tsv"
mkdir $PWD/trio/GS19240-B37-${VER}-ASM/calldiff-v-baseline
rat run "/Proj/QA/Release/cgatools/1.3.0/1.3.0.9/binaries/bin/cgatools calldiff --reference=/rnd/home/jbaccash/archive/cgatools-stage/crr/build37.crr --output-prefix=$PWD/trio/GS19240-B37-${VER}-ASM/calldiff-v-baseline/ --variantsA=/rnd/home/jbaccash/archive/qual/trio-baseline-37/var-GS00028-DNA_C01-ASM.tsv --variantsB=$PWD/trio/GS19240-B37-${VER}-ASM/ASM/variations/annotated/Variations20-40_ccf100.tsv"

# NA19240 Calldiff v replicate
mkdir $PWD/trio/GS19240B-B37-${VER}-ASM/calldiff-v-replicate
rat run "/Proj/QA/Release/cgatools/1.3.0/1.3.0.9/binaries/bin/cgatools calldiff --beta --reference=/rnd/home/jbaccash/archive/cgatools-stage/crr/build37.crr --output-prefix=$PWD/trio/GS19240B-B37-${VER}-ASM/calldiff-v-replicate/ --variantsA=$PWD/trio/GS19240B-B37-${VER}-ASM/EXP/package/GS00028-DNA_C01/ASM/var-GS19240B-B37-${VER}-ASM.tsv.bz2 --variantsB=$PWD/trio/GS19240-B37-${VER}-ASM/EXP/package/GS00028-DNA_C01/ASM/var-GS19240-B37-${VER}-ASM.tsv.bz2 --export-rootA=$PWD/trio/GS19240B-B37-${VER}-ASM/EXP/package/GS00028-DNA_C01 --export-rootB=$PWD/trio/GS19240-B37-${VER}-ASM/EXP/package/GS00028-DNA_C01 --reports=SuperlocusOutput,SuperlocusStats,LocusOutput,LocusStats,SomaticOutput"

# Genentech SomaticOutput B36 & B37
for build in 36 37; do

    asmt=GS00018-DNA_A01-B${build}-${VER}-ASM
    asmn=GS00022-DNA_A01-B${build}-${VER}-ASM
    mkdir -p $PWD/P032/${asmt}/calldiff-v-norm
    rat run "/Proj/QA/Release/cgatools/1.3.0/1.3.0.9/binaries/bin/cgatools calldiff --beta --reference=/rnd/home/jbaccash/archive/cgatools-stage/crr/build${build}.crr --output-prefix=$PWD/P032/${asmt}/calldiff-v-norm/ --variantsA=$PWD/P032/${asmt}/EXP/package/GS00018-DNA-A01/ASM/var-${asmt}.tsv.bz2 --variantsB=$PWD/P032/${asmn}/EXP/package/GS00022-DNA-A01/ASM/var-${asmn}.tsv.bz2 --export-rootA=$PWD/P032/${asmt}/EXP/package/GS00018-DNA-A01 --export-rootB=$PWD/P032/${asmn}/EXP/package/GS00022-DNA-A01 --reports=SuperlocusOutput,SuperlocusStats,LocusOutput,LocusStats,SomaticOutput"

done

for x in */*; do
    mkdir -p ${x}/qual
    y=$(echo ${x}/EXP/package/*/ASM/gene-*.tsv.bz2)
    rat run "bzcat ${y} | grep -v '^#' | grep -v '^$' | grep -v '^>' | awk -F'\t' '{print \$19}' | sort | uniq -c | awk '{print \$2\",\"\$1}' >${x}/qual/gene-impact.csv"
done
