#! /usr/bin/env python

"""
Use this program to aggregate output files from
3rd party program fastqc.  Input is a directory where all subdirectories
contain fastqc output.
"""

import sys
from optparse import OptionParser
import glob

def initialize_containers():
    global base_seqs ##base_seqs[posn] = {base:[frac1,frac2]}
    global base_gc ##base_gc[posn] = [gc%1, gc%2...]
    global seq_GC ## seq_GC[GCpct] = [seqCount1, seqCount2...]
    global overrep_seq ## overrep_seq[sequence] = {'count': count, 'posSrc' : source}
    global kmers ## kmers[kmer] = {obs/exp max posn :[obs/exp max, obs/exp max...]}
    global totalSeq

    totalSeq = 0
    base_seqs = {}
    base_gc = {}
    seq_GC = {}
    overrep_seq = {}
    kmers = {}

def write_kmers(outfile):
    fo = open(outfile,'w')
    try:
        fo.write('kmer,avg_observed_over_expected\n')
        # want the obs/exp averaged over all map files
        # assume number of map files is max of number of O/E for 
        # a given kmer
        obs = [ len(OEs) for OEs in kmers.values() ]
        totalobs = max(obs)
        for kmer, OEs in kmers.iteritems():
            avgOE = sum(OEs) / float(totalobs)
            fo.write('%s,%s\n'%(kmer,avgOE))
    finally:
        fo.close()

def write_overrep_seq(outfile):
    fo = open(outfile,'w')
    try:
        fo.write('sequence,count,totalSequences,pct,possibleSource\n')
        for sequence, values in overrep_seq.iteritems():
            count = values['count']
            src = values['posSrc']
            pctSeq = float(count) / float(totalSeq)
            fo.write('%s,%s,%s,%s,%s\n'%(sequence,count,totalSeq,pctSeq,src))
    finally:
        fo.close()

def write_seq_gc(outfile):
    gcpcts = [int(i) for i in seq_GC.keys()]
    fo = open(outfile,'w')
    try:
        fo.write('pctGC,dnbCount,totalDnbs,dnbPct\n')
        for gcpct in range(min(gcpcts),max(gcpcts) + 1):
            counts = seq_GC.get(gcpct,[0])
            totaldnbs = sum(counts)
            dnbpct = float(totaldnbs) / float(totalSeq)
            fo.write('%s,%s,%s,%s\n'%(gcpct,totaldnbs,totalSeq,dnbpct))
    finally:
        fo.close()

def write_base_gc(outfile):
    posns = [int(i) for i in base_gc.keys()]
    fo = open(outfile,'w')
    try:
        fo.write('Position,pctGC\n')
        for posn in range(min(posns),max(posns) + 1):
            gcs = base_gc.get(posn,0)
            if gcs != 0:
                gcpct = float(sum(gcs)) / float(len(gcs))
            else:
                gcpct = 0
            fo.write('%s,%s\n'%(posn,gcpct))
    finally:
        fo.close()

def write_base_seqs(outfile):
    posns = [int(i) for i in base_seqs.keys()]
    fo = open(outfile,'w')
    try:
        fo.write('Position,Base,Fraction\n')
        for posn in range(min(posns),max(posns) + 1):
            fracs = base_seqs.setdefault(posn,{})
            for base in ['A','T','G','C']:
                frac = fracs.get(base,[0])
                frac = sum(frac) / float(len(frac))
                fo.write('%s,%s,%s\n'%(posn,base,frac))
    finally:
        fo.close()

def skip_output(line,fi):
    while line.rstrip() != '>>END_MODULE':
        line = fi.next()

    return line, fi

def agg_base_seq(line, fi):
    while line.rstrip() != '>>END_MODULE':
        if line[0] == '#':
            line = fi.next()
        else:
            posn, g, a, t, c = line.split()
            posn = int(posn)
            base_seqs[posn] = base_seqs.setdefault(posn,{})
            for base in ['A','T','G','C']:
                base_seqs[posn][base] = base_seqs[posn].setdefault(base,[])
            base_seqs[posn]['G'].append(float(g))
            base_seqs[posn]['A'].append(float(a))
            base_seqs[posn]['T'].append(float(t))
            base_seqs[posn]['C'].append(float(c))
            line = fi.next()

    return line, fi

def agg_base_GC(line,fi):
    while line.rstrip() != '>>END_MODULE':
        if line[0] == '#':
            line = fi.next()
        else:
            posn, pct = line.split()
            posn = int(posn)
            base_gc[posn] = base_gc.setdefault(posn,[])
            base_gc[posn].append(float(pct))
            line = fi.next()

    return line, fi

def agg_seq_GC(line,fi):
    while line.rstrip() != '>>END_MODULE':
        if line[0] == '#':
            line = fi.next()
        else:
            GCpct, count = line.split()
            GCpct = int(GCpct); count = float(count)
            seq_GC[GCpct] = seq_GC.setdefault(GCpct,[])
            seq_GC[GCpct].append(count)
            line = fi.next()

    return line, fi

def agg_overrep_seq(line,fi):
    while line.rstrip() != '>>END_MODULE':
        if line[0] == '#':
            line = fi.next()
        else:
            line = line.rstrip().split('\t')
            seq, count, pct, source = line[:]
            overrep_seq[seq] = overrep_seq.setdefault(seq,{})
            overrep_seq[seq]['count'] = overrep_seq[seq].get('count',0) + int(count)
            overrep_seq[seq]['posSrc'] = source
            line = fi.next()

    return line, fi

def get_total_seq(line,fi):
    global totalSeq
    while line.rstrip() != '>>END_MODULE':
        if line[0] == '#':
            line = fi.next()
        else:
            line = line.rstrip().split('\t')
            if line[0] == 'Total Sequences':
                totalSeq += int(line[1])
            line = fi.next()

    return line,fi

def agg_kmer(line,fi):
    while line.rstrip() != '>>END_MODULE':
        if line[0] == '#':
            line = fi.next()
        else:
            line = line.split()
            kmer = line[0]
            OEoverall = float(line[2])
            kmers[kmer] = kmers.setdefault(kmer,[])
            kmers[kmer].append(OEoverall)
            line = fi.next()

    return line,fi

def parse_fastqcOut(fastqcFile):
    fi = open(fastqcFile)

    skipHeaders = set(['Per base sequence quality',
                'Per sequence quality scores',
                'Per base N content',
                'Sequence Length Distribution',
                'Sequence Duplication Levels'])
    
    try:
        for line in fi:
            if line[:2] == '>>':
                header = line.split('\t')[0][2:]
                # skip uninteresting stats
                if header in skipHeaders:
                    line, fi = skip_output(line,fi)
                elif header == 'Basic Statistics':
                    print 'Reading in %s data'%header
                    line = fi.next()
                    line,fi = get_total_seq(line,fi)
                elif header == 'Per base sequence content':
                    print 'Reading in %s data'%header
                    line = fi.next()
                    line, fi = agg_base_seq(line, fi)
                elif header == 'Per base GC content':
                    print 'Reading in %s data'%header
                    line = fi.next()
                    line, fi = agg_base_GC(line, fi)
                elif header == 'Per sequence GC content':
                    print 'Reading in %s data'%header
                    line = fi.next()
                    line, fi = agg_seq_GC(line,fi)
                elif header == 'Overrepresented sequences':
                    print 'Reading in %s data'%header
                    line = fi.next()
                    line, fi = agg_overrep_seq(line,fi)
                elif header == 'Kmer Content':
                    print 'Reading in %s data'%header
                    line = fi.next()
                    line, fi = agg_kmer(line,fi)

    finally:
        fi.close()

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-i','--input-directory',help='directory containing fastqc output subdirectories', dest='input_dir')

    # parse command line arguments.
    options,args = parser.parse_args(arguments[1:])

    return options.input_dir 

def main(arguments):
    # parse arguments
    inputDir = parse_arguments(arguments)

    # initialize containers
    initialize_containers()
    
    # parse individual fastq output files and add to containers
    for fn in glob.glob('%s/*_fastqc/fastqc_data.txt' % (inputDir, )):
        parse_fastqcOut(fn)
    
    # write out summarized data
    write_base_seqs('%s/base_sequences.csv' % (inputDir, ))
    write_base_gc('%s/gc_by_base.csv' % (inputDir, ))
    write_seq_gc('%s/gc_by_sequence.csv' % (inputDir, ))
    write_overrep_seq('%s/overrepresented_sequences.csv' % (inputDir, ))
    write_kmers('%s/kmers_OE.csv' % (inputDir, ))

if __name__ == '__main__':
    main(sys.argv)
