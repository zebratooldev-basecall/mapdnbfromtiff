#!/bin/env python
import os,sys
import optparse
from cnvwindow import WholeGenomeCNVWindowIterator
from gbifile import GbiFile 
from refconstants import GlobalRefConstants
#import scipy.stats.binom as binom
#import scipy.misc.comb as comb
import math

class outputClass(object):
    def __init__(self,outputpfx):
        self.countsForMAF = open(outputpfx + "-countsForMAF.tsv","w")


## silly values, overridden in main()
normalPvalueCutoff = 1.0
minNormalHetCount = 1000000
maxNormalHetCount = -1

n=-1
chromosomeCol = n
beginCol = n
endCol = n
zygosityCol = n
varTypeCol = n
referenceCol = n
allele1SeqCol = n
allele2SeqCol = n
allele1ReadCountCol = n
allele2ReadCountCol = n
allele1ReadCountOtherCol = n
allele2ReadCountOtherCol = n
totalReadCountCol = n

chromosome = ""
begin = -1
end = -1
zygosity = ""
varType = ""
reference = ""
allele1Seq = ""
allele2Seq = ""
allele1ReadCount = -1
allele2ReadCount = -1
totalReadCount = -1
allele1ReadCountOther = -1
allele2ReadCountOther = -1

pChr = ""
begin = None
winBegin = -1
winEnd = -1
hetCount = 0
allele1CountArray = []
allele2CountArray = []

numFields = 0

cnvWinIterator = WholeGenomeCNVWindowIterator()

def readHeader(w):
    n = 0
    for x in w:
        if x == "chromosome":
            global chromosomeCol
            chromosomeCol = n
        if x == "begin":
            global beginCol
            beginCol = n
        if x == "end":
            global endCol
            endCol = n
        if x == "zygosity":
            global zygosityCol
            zygosityCol = n
        if x == "varType":
            global varTypeCol
            varTypeCol = n
        if x == "reference":
            global referenceCol
            referenceCol = n
        if x == "allele1Seq":
            global allele1SeqCol
            allele1SeqCol = n
        if x == "allele2Seq":
            global allele2SeqCol
            allele2SeqCol = n
        if x == "allele1ReadCount":
            global allele1ReadCountCol
            allele1ReadCountCol = n
        if x == "allele2ReadCount":
            global allele2ReadCountCol
            allele2ReadCountCol = n
        if x == "totalReadCount":
            global totalReadCountCol
            totalReadCountCol = n
        if x == "allele1ReadCountOtherAsm":
            global allele1ReadCountOtherCol
            allele1ReadCountOtherCol = n
        if x == "allele2ReadCountOtherAsm":
            global allele2ReadCountOtherCol
            allele2ReadCountOtherCol = n
        n += 1
    global numFields
    numFields = n


    badFields = ''
    if chromosomeCol == -1:
        badFields += " chromosome"
    if beginCol == -1:
        badFields += " begin"
    if endCol == -1:
        badFields += " end"
    if zygosityCol == -1:
        badFields += " zygosity"
    if varTypeCol == -1:
        badFields += " varType"
    if referenceCol == -1:
        badFields += " reference"
    if allele1SeqCol == -1:
        badFields += " allele1Seq"
    if allele2SeqCol == -1:
        badFields += " allele2Seq"
    if allele1ReadCountCol == -1:
        badFields += " allele1ReadCount"
    if allele2ReadCountCol == -1:
        badFields += " allele2ReadCount"
    if totalReadCountCol == -1:
        badFields += " totalReadCount"
    if allele1ReadCountOtherCol == -1:
        badFields += " allele1ReadCountOtherAsm"
    if allele2ReadCountOtherCol == -1:
        badFields += " allele2ReadCountOtherAsm"

    if badFields != '':
        raise Exception( "Error: could not locate column(s) for" + badFields)


def choose(n,k):
    # return comb(n,k)
    m = n-k
    if k > m:
        t = m
        m = k
        k = t

    cmb = 1
    for i in range(m+1,n+1):
        cmb *= i
    for i in range(1,k+1):
        cmb /= i

    return cmb
    
def binomProb(n,a,p):
    return choose(n,a) * math.exp( float(a)*math.log(p) + float(n-a)*math.log(1.-p) )
                
def binom_interval(pval,n,p):
    a=0
    aprob = binomProb(n,a,p)
    b=n
    bprob = binomProb(n,b,p)
    tailCumul = 0
    minprob = min(aprob,bprob)
    while a < b and tailCumul+minprob <= pval:
        tailCumul += minprob
        if minprob == aprob:
            a += 1
            aprob = binomProb(n,a,p)
            if aprob == bprob:
                tailCumul += bprob
                b += 1
                bprob = binomProb(n,a,p)
            minprob = min(aprob,bprob)
        else:
            b -= 1
            bprob = binomProb(n,b,p)
            minprob = min(aprob,bprob)
    return [a,b]

def hetSnpLine(w):
    if len(w) != numFields:
        return False
    if w[varTypeCol] != "snp":
        return False
    if w[zygosityCol] != "het-ref":
        return False
    return True
    
def line_is_useful(w):
    if int(w[totalReadCountCol]) == 0:
        return False

    nreads = int(allele1ReadCount)+int(allele2ReadCount)

    if nreads < minNormalHetCount or nreads > maxNormalHetCount:
        return False

    #validRange = scipy.stats.binom.interval(normalPvalueCutoff,nreads,0.5)
    validRange = binom_interval(normalPvalueCutoff,nreads,0.5)
    if int(allele1ReadCount) < validRange[0] or  int(allele1ReadCount) > validRange[1]:
        return False

    return True


def readData(w):
    global chromosome
    chromosome = w[chromosomeCol]
    global begin
    begin = w[beginCol]
    global end
    end = w[endCol]
    global zygosity
    zygosity = w[zygosityCol]
    global varType
    varType = w[varTypeCol]
    global reference
    reference = w[referenceCol]
    global allele1Seq
    allele1Seq = w[allele1SeqCol]
    global allele2Seq
    allele2Seq = w[allele2SeqCol]
    global allele1ReadCount
    allele1ReadCount = w[allele1ReadCountCol]
    global allele2ReadCount
    allele2ReadCount = w[allele2ReadCountCol]
    global totalReadCount
    totalReadCount = w[totalReadCountCol]
    global allele1ReadCountOther
    allele1ReadCountOther = w[allele1ReadCountOtherCol]
    global allele2ReadCountOther
    allele2ReadCountOther = w[allele2ReadCountOtherCol]

def init_window(outputs):
    global pChr
    global chromosome
    global winBegin
    global winEnd

    pChr = chromosome

    global allele1CountArray
    allele1CountArray = []
    global allele2CountArray
    allele2CountArray = []           
    global hetCount
    hetCount = 0
    
    winEnd = winBegin
    while winEnd < int(begin):
        if cnvWinIterator.eof():
            raise Exception( "ERROR: CNV window iterator out of windows before end of masterVar file")
        win = cnvWinIterator.next()
        chromosome = win.chr_
        winBegin = win.begin_
        winEnd = win.end_
        if winEnd < int(begin):
            output_window(outputs)

def output_window(outputs):
    
    global winBegin

    countStr = ''
    l = len(allele1CountArray)
    for i in range(0,l-1):
        countStr += str(allele1CountArray[i]) + '/' + str(allele2CountArray[i]) + ';'
    if l > 0:
        countStr += str(allele1CountArray[l-1]) + '/' + str(allele2CountArray[i-1])

    outputs.countsForMAF.write("\t".join((chromosome,str(winBegin),str(winEnd),countStr))+"\n")

    winBegin = -1

def consumeData(w,outputs):
    if winBegin == -1:
        init_window(outputs)

    if zygosity == "het-ref":
        global hetCount
        global allele1CountArray
        global allele2CountArray
        hetCount += 1
        allele1CountArray += [allele1ReadCountOther]
        allele2CountArray += [allele2ReadCountOther]
    
def computeMAFinputs(masterVar,outputPfx):

    mv = open(masterVar)
    ok = False

    outputs = outputClass(outputPfx)
    
    for l in mv:
        w = l.rstrip().split("\t")
        if ok:
            if hetSnpLine(w):
                readData(w)
                if line_is_useful(w):
                    if chromosome != pChr or (begin != None and winEnd < int(begin)):
                        if pChr != "" and winBegin != None:
                            output_window(outputs)
                    consumeData(w,outputs)
                                  
        if w[0] == ">locus":
            ok = True
            readHeader(w)
        
    if pChr != "" and winBegin != None:
        output_window(outputs)
                
            
        

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(masterVar='',output='',windowWidth='100000',sampleType='',gbiFile='',
                        normalPvalCutoff='0.2',minNormal='10',maxNormal='200')
    parser.add_option('-m', '--master-var', dest='masterVar',
                      help='location of master var file')
    parser.add_option('-o', '--output-prefix', dest='outputPfx',
                      help='prefix for output file names')
    parser.add_option('--window-width',dest='windowWidth',
                      help='window width')
    parser.add_option('--sample-type',dest='sampleType',
                      help='sample type')
    parser.add_option('--reference-gbi',dest='gbiFile',
                      help='reference GBI file name')
    parser.add_option('--reference-root-dir', dest='referenceRootDir',
                      help='referenceRootDir of the workflow')
    parser.add_option('--normal-pvalue-cutoff',dest='normalPvalCutoff',
                      help='cutoff to exclude normal-genome het sites with allele bias strong enough to ' +
                           'give this p-value or less for a two-sided binomial test against the 50/50 null hypothesis')
    parser.add_option('--normal-min-read-count',dest='minNormal',
                      help='minimum number of reads in normal sample for a normal-het site to be included for MAF estimation')
    parser.add_option('--normal-max-read-count',dest='maxNormal',
                      help='maximum number of reads in normal sample for a normal-het site to be included for MAF estimation')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.masterVar:
        parser.error('master-var specification required')
    if not options.outputPfx:
        parser.error('output-prefix specification required')
    if not options.gbiFile:
        parser.error('reference-gbi specification required')
    if not options.sampleType:
        parser.error('sample-type specification required')

    globalRefConstants = GlobalRefConstants(options.referenceRootDir)
    refConstants = globalRefConstants[options.sampleType]
    gbi = GbiFile(options.gbiFile)
    global cnvWinIterator
    wwidth = int(options.windowWidth)
    cnvWinIterator.initialize(gbi,refConstants.cnvIgnoreRegions,wwidth,wwidth)

    global normalPvalueCutoff
    normalPvalueCutoff = float(options.normalPvalCutoff)

    global minNormalHetCount
    minNormalHetCount = int(options.minNormal)
    global maxNormalHetCount
    maxNormalHetCount = int(options.maxNormal)
    
    computeMAFinputs(options.masterVar,options.outputPfx)


if __name__ == '__main__':
    main()
