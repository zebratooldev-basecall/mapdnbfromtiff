#!/bin/env python
import os,sys
import optparse
from subprocess import call
from CircosInputs import CircosInputs 

def runCircos(workDir):
    os.chdir(workDir)
    # circos and misc perl junk currently in /rnd/home/ahalpern/Circos/circos-0.52/{bin,lib}

    try:
        retcode = call('cgicircos -conf main.conf', shell=True)
        if retcode < 0:
            raise Exception("Circos command was terminated by signal " + str(-retcode))
        else:
            if retcode > 0:
                raise Exception("Circos command returned " + str(retcode))

    except OSError, e:
        raise Exception("Execution failed: " + e)


def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(jcnFile='',masterVar='',lohSource='',
                        cnvDetails='',cnvSegments='',
                        somaticVar='',sampleType='',
                        confDir='',etcDir='',workDir='',
                        refDir='',plotType='',useScore=False)

    parser.add_option('--junction-file', dest='jcnFile',
                      help='junction (or junctiondiff, for somatic) file (high confidence)'),
    parser.add_option('--masterVar-file', dest='masterVar',
                      help='cgatools generatemastervar file'),
    parser.add_option('--calldiff-super-locus', dest='lohSource',
                      help='cgatools calldiff superlocus output file'),
    parser.add_option('--cnv-details', dest='cnvDetails',
                      help='CNV details file including LAF estimates'),
    parser.add_option('--cnv-segments', dest='cnvSegments',
                      help='CNV segments file'),
    parser.add_option('--somatic-var', dest='somaticVar',
                      help='cgatools somatic calldiff VariantOutput file'),
    parser.add_option('--sample-type', dest='sampleType',
                      help='sample type (male, female, male37, female37'),
    parser.add_option('--conf-dir', dest='confDir',
                      help='directory containing circos conf file templates'),
    parser.add_option('--ref-dir', dest='refDir',
                      help='reference (gbi-containing) directory'),
    parser.add_option('--plot-type',dest='plotType',
                      help='type of circos plot (somatic, normal, unmatched tumor)'),
    parser.add_option('--circos-etc', dest='etcDir',
                      help='directory containing circos "etc" files (colors, fonts,...)'),
    parser.add_option('--circos-output-dir', dest='workDir',
                      help='location of circos workfiles and final plot')
    parser.add_option('--plot-label', dest='plotLabel',
                      help='single-word label for plot')
    parser.add_option('--use-score-cutoff',dest='useScore',action='store_true',
                      help='use somatic score cutoff rather than somaticeQuality flag')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.jcnFile:
        parser.error('junction-file specification required')
    if options.jcnFile[-3:] == "bz2":
        parser.error('junction-file should not be compressed')

    if not options.masterVar:
        parser.error('masterVar-file specification required')
    if options.masterVar[-3:] != 'bz2':
        parser.error('master-var specification must have bz2 suffix (and be bz2 compressed)')

    if not options.cnvDetails:
        parser.error('cnv-details specification required')
    if options.cnvDetails[-3:] != 'bz2':
        parser.error('cnv-details specification must have bz2 suffix (and be bz2 compressed)')

    if not options.cnvSegments:
        parser.error('cnv-segments specification required')
    if options.cnvDetails[-3:] != 'bz2':
        parser.error('cnv-segments specification should not be compressed')

    if not options.sampleType:
        parser.error('sample-type specification required')
    if not options.confDir:
        parser.error('conf-dir specification required')
    if not options.etcDir:
        parser.error('circos-etc specification required')
    if not options.workDir:
        parser.error('circos-output-dir specification required')
    if not options.plotType or options.plotType not in ['somatic','normal','tumor']:
        parser.error('plot-type specification ("tumor", "normal" or "somatic") required')
    if not options.refDir:
        parser.error('ref-dir specification required')



    ci = CircosInputs()

    if options.plotType == 'somatic':
        ci.createSomaticJunctionInputs(options.jcnFile,options.workDir)
    else:
        ci.createInterChrJunctionInputs(options.jcnFile,options.workDir)

    ci.createSnpDensityInputs(options.masterVar,options.workDir)
    
    if options.plotType == 'somatic':
        if not options.lohSource:
            parser.error('calldiff-super-locus specification required')
        if options.lohSource[-3:] == "bz2":
            parser.error('calldiff-super-locus should not be compressed')
        ci.createLOHInputs(options.lohSource,options.workDir)

    ci.createLAFInputs(options.cnvDetails,options.workDir)

    if options.plotType in  ['somatic', 'tumor']:
        ci.createCNVLevelInputs(options.cnvSegments,options.workDir)
    else:
        ci.createCNVPloidyInputs(options.cnvSegments,options.workDir)
        
    if options.plotType == 'somatic':
        if not options.somaticVar:
            parser.error('somatic-var specification required')
        if options.somaticVar[-3:] != 'bz2':
            parser.error('somatic-var specification must have bz2 suffix (and be bz2 compressed)')
        ci.createSomaticVarTracks(options.somaticVar,options.workDir,options.useScore)

    ci.createCircosConfs(options.confDir,options.etcDir,options.workDir,
                         options.sampleType,options.plotType,options.refDir)

    ci.createCircosHtml(options.confDir,options.workDir,options.plotType,options.plotLabel)

    runCircos(options.workDir)


if __name__ == '__main__':
    main()
    
