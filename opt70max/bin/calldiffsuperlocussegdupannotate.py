#!/usr/bin/python

# usage: prog superlocus.tsv segdup.txt
# suitable repeat table in /Proj/Assembly/Shared/wormhole-annotations/build36/segdup.txt

from annotation import chromNo, loadAnnotation

def annotate(cdfn, repfn):
    rep = loadAnnotation(repfn, columns=(1, 2, 3, 4))
    f = open(cdfn, 'r')
    header = f.readline()
    print header[:-1] + '\tInSegDup\tSegDupBeg\tSegDupEnd\tName'

    while f:
        line = f.readline()
        if not line:
            break
        if not line.strip():
            continue
        _, chrom, beg, end = line.split('\t')[:4]
        chrom = chromNo(chrom)
        beg = int(beg) - 1
        end = int(end) + 1

        ints = rep.searchRange((chrom, beg, end))

        if ints:
            sd = ints[0]
            print '%s\tY\t%d\t%d\t%s' % (
                line[:-1], sd[1], sd[2], sd[3])
        else:
            print '%s\tN\t\t\t' % line[:-1]

import sys
annotate(sys.argv[1], sys.argv[2])