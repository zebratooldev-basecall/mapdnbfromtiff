#!/usr/bin/python

import sys
import os



'''

Script to run DeNovo17 assembly beginning at assembly level 3.



When this script starts, the current directory must contain a directory
Level1-Dnbs containing the DNBs to use for the run, in 200 batches.
Workflow Level1.py, which runs on the cluster, can be used to
prepare this directory.

It must also contain a symbolic link, name sharedMemory,
pointing to the shared memory directory (huge pages) to be used.

For a small test case, sharedMemory can instead by a regular directory,
in which case the data will be supported by disk space, not RAM.
In that case it will also be necessayr to reduce
LEVEL3_CONTEXT_LENGTH in DeNOvo17_Level3.hpp to 16 or so.

Pass a s a single argument the number of threads to be used.

'''



if len(sys.argv)!= 2:
    print 'Must be called with one argument.'
    print 'The argument specifies the number of threads.'
    sys.exit(1)
       
threadCount = sys.argv[1]



# Construct the fixed portion of the Assemble5 command.
baseCommand = 'Assemble5 ' + '--threads ' + threadCount + ' --command '



# Create a threadlogs directory if it does not already exist.
if not os.path.exists('threadLogs'):
    os.mkdir('threadLogs')



# Level3 assembly.
os.system(baseCommand + 'level1Load --batch 0,200')
os.system(baseCommand + 'level3CreatePass1')
os.system(baseCommand + 'level3CreatePass2')
os.system(baseCommand + 'level3CreatePass3')
os.system(baseCommand + 'level3CreatePass4')
os.system(baseCommand + 'level3CreatePass5')
os.system(baseCommand + 'level3CreatePass6')
os.system(baseCommand + 'level3CreatePass7')
os.system(baseCommand + 'level3CreatePass8')
os.system(baseCommand + 'level3CreatePass9')
os.system(baseCommand + 'level3CreatePass10')
os.system(baseCommand + 'level3Assemble')
os.system(baseCommand + 'level3RemoveRedundantContigs')
os.system(baseCommand + 'level3DumpNonRedundantContigs')
os.system(baseCommand + 'level3ComputeContigWells')



# Level 4 assembly.
os.system(baseCommand + 'level4CreateIndex')
os.system(baseCommand + 'level4Map')
os.system(baseCommand + 'level4ComputeMateDistribution')
os.system(baseCommand + 'level4ComputeOffsets')
os.system(baseCommand + 'level4Assemble')
os.system(baseCommand + 'level4AssembledContigsStatistics')
os.system(baseCommand + 'level4DumpAssembledContigs')

