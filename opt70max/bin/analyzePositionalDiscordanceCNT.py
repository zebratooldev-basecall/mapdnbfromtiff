#!/usr/bin/env python

import sys
import sequence
import datetime
import numpy as np
import glob

now = datetime.datetime.now()

from optparse import OptionParser
from os.path import join as pjoin
from dnbLib import *
from sequence import ChrSequence

CHROM_DIR = '/rnd/home/jyamtich/REF'
BASES = 'ACGT'

def initializeContainers(dnbLength):
    global maxDnbLength
    global posDiscSink
    global corrDiscSink
    global corrBaseCallsSink
    global posConfMatrix
    global posRefErrorSink
    global posCallErrorSink
    global gapDiffs
    global fullAdjSeqs
    global GCcontent
    global dups

    maxDnbLength = dnbLength
    # initialize containers for metric counts [x occurrences in y observations].
    gapDiffs = {} ##gapDiffs[difference in right and left gap length] = count
    fullAdjSeqs = {}  ##fullAdjSeqs[posn] = {A:count,T:count,G:count,C:count,N:count}
    posDiscSink = {}
    GCcontent = {} ##GCcontent[chr] = {'GC':count, 'AT':count, total : count}
    dups = {} ## dups = {'total': count, 'uniqs': set(seqs) }
    corrDiscSink = {}
    corrBaseCallsSink = {}
    posConfMatrix = {}
    posRefErrorSink = {}
    posCallErrorSink = {}
    for i in range(dnbLength):
        posDiscSink[i+1] = [0,0]
        corrDiscSink[i+1] = {}
        corrBaseCallsSink[i+1] = {}
        posConfMatrix[i+1] = {}
        posRefErrorSink[i+1] = {}
        posCallErrorSink[i+1] = {}
        for j in range(dnbLength):
            corrDiscSink[i+1][j+1] = [0,0]
            corrBaseCallsSink[i+1][j+1] = [0,0]
        for callBase in list(BASES):
            posConfMatrix[i+1][callBase] = {}
            posRefErrorSink[i+1][callBase] = [0,0]
            posCallErrorSink[i+1][callBase] = [0,0]
            for refBase in list(BASES):
                posConfMatrix[i+1][callBase][refBase] = 0


def log(string):
    now = datetime.datetime.now()

    print('%s: %s' % (now.strftime('%Y-%m-%d %H:%M:%S'),string))

def find_seq_dups(leftArm, rightArm):
    # calcualte DNB duplication rate
    seq = leftArm.sequence + rightArm.sequence
    dups['total'] = dups.get('total',0) + 1
    dups['uniq'] = dups.setdefault('uniq',set())
    dups['uniq'].add(seq)

def find_gc_content(leftArm, rightArm):
    # this function calculates per-DNB GC content for each chromosome (and total)
    seq = leftArm.sequence + rightArm.sequence
    mapping = leftArm.mappings[0]
    chr = mapping.chromosome
    
    GCcontent['total'] = GCcontent.setdefault('total',{})
    GCcontent[chr] = GCcontent.setdefault(chr,{})

    for base in seq:
        if base in 'GC':
            GCcontent['total']['GC'] = GCcontent['total'].get('GC',0) + 1
            GCcontent[chr]['GC'] = GCcontent[chr].get('GC',0) + 1
            GCcontent['total']['total'] = GCcontent['total'].get('total',0) + 1
            GCcontent[chr]['total'] = GCcontent[chr].get('total',0) + 1
        elif base in 'AT':
            GCcontent['total']['AT'] = GCcontent['total'].get('AT',0) + 1
            GCcontent[chr]['AT'] = GCcontent[chr].get('AT',0) + 1
            GCcontent['total']['total'] = GCcontent['total'].get('total',0) + 1
            GCcontent[chr]['total'] = GCcontent[chr].get('total',0) + 1

def find_discordant_bases(seq, mapping, ref, side):
    posDisc = [False] * len(seq)
    expRefBases = [''] * len(seq)

    offsets = mapping.dnbArm.dnb.dnbArchitecture.getOffsets(side, mapping.gaps)

    for i in range(len(seq)):
        expRefBases[i] = ref[offsets[i]]
        if seq[i] != 'N' and seq[i] != ref[offsets[i]]:
            posDisc[i] = True

    return posDisc, expRefBases

def find_gapDiffs(leftArm, rightArm):
    #This function will find the difference in gap size between arms within a dnb
    mapping = leftArm.mappings[0]
    gaps_left = mapping.gaps
    
    mapping = rightArm.mappings[0]
    gaps_right = mapping.gaps
    
    gapDiff = sum(gaps_left) - sum(gaps_right)
    gapDiffs[gapDiff] = gapDiffs.get(gapDiff,0) + 1

def find_adjacent_seq(leftArm, rightArm, chrSeq):
    # this function will pull the reference sequence from 10 bp up and downstream of each arm
    #   left Arm.
    mapping = leftArm.mappings[0]
    leftArmRef10up10down = chrSeq.get_sequence(mapping.chromosome, mapping.begin - 10, 
                                    mapping.begin + len(leftArm.sequence) + sum(mapping.gaps) + 10)
    if mapping.strand == '-':
        leftArmRef10up10down = sequence.reverse_complement(leftArmRef10up10down)

    #   right Arm.
    mapping = rightArm.mappings[0]
    rightArmRef10up10down = chrSeq.get_sequence(mapping.chromosome, mapping.begin - 10,
                                    mapping.begin + len(rightArm.sequence) + sum(mapping.gaps) + 10)
    if mapping.strand == '-':
        rightArmRef10up10down = sequence.reverse_complement(rightArmRef10up10down)
    
    modseq = leftArmRef10up10down[:10] + 'NNNNN' + leftArm.sequence + 'NNNNN' + \
             leftArmRef10up10down[-10:] + 'NNNNN' + rightArmRef10up10down[:10] + 'NNNNN' + \
             rightArm.sequence + 'NNNNN' + rightArmRef10up10down[-10:]

    for i in xrange(0,len(modseq)):
        fullAdjSeqs[i+1] = fullAdjSeqs.setdefault(i+1,{})
        fullAdjSeqs[i+1][modseq[i]] = fullAdjSeqs[i+1].get(modseq[i],0) + 1

def find_discordances(leftArm, rightArm, chrSeq):
    # this function will use dnb sequence and corresponding reference to find discordant positions.
    bases = list(leftArm.sequence) + list(rightArm.sequence)

    # get discordances and expanded reference bases.
    #   left Arm.
    mapping = leftArm.mappings[0]

    leftArmRef = chrSeq.get_sequence(mapping.chromosome, mapping.begin, 
                                    mapping.begin + len(leftArm.sequence) + sum(mapping.gaps))
    if mapping.strand == '-':
        leftArmRef = sequence.reverse_complement(leftArmRef)
    leftArmDiscPos, leftArmExpRefBases = find_discordant_bases(leftArm.sequence,
                                                             mapping, leftArmRef, leftArm.side)

    #   right Arm.
    mapping = rightArm.mappings[0]

    rightArmRef = chrSeq.get_sequence(mapping.chromosome, mapping.begin,
                                    mapping.begin + len(rightArm.sequence) + sum(mapping.gaps))
    if mapping.strand == '-':
        rightArmRef = sequence.reverse_complement(rightArmRef)
    rightArmDiscPos, rightArmExpRefBases = find_discordant_bases(rightArm.sequence, 
                                                               mapping, rightArmRef, rightArm.side)

    discordances = leftArmDiscPos + rightArmDiscPos
    expRefBases = leftArmExpRefBases + rightArmExpRefBases
    
    return bases, expRefBases, discordances

def process_disc_positions(bases, refBases, posDisc):
    # loop through positions and increment counters when appropriate.
    for i in range(len(bases)):
        iPos = i + 1
        if bases[i] == 'N': continue
        if not posConfMatrix[1].has_key(refBases[i]): continue

        # positional discordance
        posDiscSink[iPos][1] += 1
        posRefErrorSink[iPos][refBases[i]][1] += 1
        posCallErrorSink[iPos][bases[i]][1] += 1

        if posDisc[i]: 
            posDiscSink[iPos][0] += 1
            posRefErrorSink[iPos][refBases[i]][0] += 1
            posCallErrorSink[iPos][bases[i]][0] += 1

        # correlated discordance.
        if posDisc[i]:
            for j in range(len(bases)):
                if i == j: continue
                if bases[j] == 'N': continue
                jPos = j + 1
                corrDiscSink[iPos][jPos][1] += 1
                if posDisc[j] and posDisc.count(True)>1:
                    corrDiscSink[iPos][jPos][0] += 1

                # correlated base calls when base[i] is discordant.
                corrBaseCallsSink[iPos][jPos][1] += 1
                if bases[i] == bases[j]:
                    corrBaseCallsSink[iPos][jPos][0] += 1

        # confusion matrices.
        posConfMatrix[iPos][bases[i]][refBases[i]] += 1

def write_report(fn):
    out = open(fn, 'w')

    # positional discordance.
    out.write('array,discordancePerPosition\n')
    out.write('position,totalBaseCount,discordantBaseCount\n')
    for i in range(maxDnbLength):
        out.write('%d,%d,%d\n' % (i+1, posDiscSink[i+1][1], posDiscSink[i+1][0]))
    out.write('\n')

    # reference error counts.
    out.write('array,referenceErrorCounts\n')
    out.write('position,refBase,totalBaseCount,discordantBaseCount\n')
    for i in range(maxDnbLength):
        for refBase in list(BASES):
            out.write('%d,%s,%d,%d\n' % (i+1, refBase, posRefErrorSink[i+1][refBase][1], posRefErrorSink[i+1][refBase][0]))
    out.write('\n')

    # call base error counts.
    out.write('array,callBaseErrorCounts\n')
    out.write('position,callBase,totalBaseCount,discordantBaseCount\n')
    for i in range(maxDnbLength):
        for callBase in list(BASES):
            out.write('%d,%s,%d,%d\n' % (i+1, callBase, posCallErrorSink[i+1][callBase][1], posCallErrorSink[i+1][callBase][0]))
    out.write('\n')

    # correlated discordance.
    out.write('array,correlatedDiscPerPosition\n')
    out.write('position,otherPosition,totalDiscBaseCount,correlatedDiscBaseCount\n')
    for i in range(maxDnbLength):
        for j in range(maxDnbLength):
            out.write('%d,%d,%d,%d\n' % (i+1, j+1, corrDiscSink[i+1][j+1][1], corrDiscSink[i+1][j+1][0]))
    out.write('\n')

    # correlated Base Calls for discordant positions.
    out.write('array,correlatedBaseCallsForDiscPositions\n')
    out.write('position,otherPosition,totalDiscBaseCount,correlatedBaseCalls\n')
    for i in range(maxDnbLength):
        for j in range(maxDnbLength):
            out.write('%d,%d,%d,%d\n' % (i+1, j+1, corrBaseCallsSink[i+1][j+1][1], corrBaseCallsSink[i+1][j+1][0]))
    out.write('\n')

    # confusion matrices.
    out.write('array,positionalConfustionMatrices\n')
    out.write('position,callBase,refBase,count\n')
    for i in range(maxDnbLength):
        for callBase in list(BASES):
            for refBase in list(BASES):
                out.write('%d,%s,%s,%d\n' % (i+1, callBase, refBase, posConfMatrix[i+1][callBase][refBase]))
    out.write('\n')

    # gap differences
    out.write('dict,leftVsRightArmGapDifference\n')
    gapdiffs = gapDiffs.keys()
    for i in range(min(gapdiffs),max(gapdiffs)+1):
        count = gapDiffs.get(i,0)
        out.write('%d,%d\n' % (i,count))
    out.write('\n')

    #full adjacent sequences
    out.write('array,adjacentSequences\n')
    out.write('position,callBase,totalBaseCount\n')
    posns = fullAdjSeqs.keys()
    for i in range(min(posns),max(posns) + 1):
        for base in ['A','T','C','G','N']:
            count = fullAdjSeqs[i].get(base,0)
            out.write("%d,%s,%d\n"%(i,base,count))
    out.write('\n')

    # GC content
    out.write('array,GCcontentOfUniquelyMappedDNBs\n')
    out.write('chromosome,ATbases,GCbases,GCpctInUniquelyMappedDNBs\n')
    for chr, counts in GCcontent.iteritems():
        GCcount = counts.get('GC',0); ATcount = counts.get('AT',0)
        GCpct = float(GCcount) / float(counts['total'])
        out.write('%s,%s,%s,%s\n'%(chr,ATcount,GCcount,GCpct))
    out.write('\n')

    # sequence duplicates
    out.write('dict,sequenceDuplicationOfFullyUniquelyMappedDNBs\n')
    out.write('totalFullyUniquelyMappedDNBs,%s\n'%(dups['total'],))
    out.write('uniqueSequencesOfFullyUniquelyMappedDNBs,%s\n'%(len(dups['uniq']),))
    out.write('\n')

    out.close()

def find_map_files(mappingDir):
    mapFile = ''
    result = []
    log('searching for input files')

    for mapFile in glob.glob(pjoin(mappingDir, 'map','map-*.txt')):
        result.append(mapFile)

    if len(mapFile) == 0:
        raise Exception('No input files found! %s' % (pjoin(mappingDir, 'map','map-*.txt')))

    return result

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-i','--input-file',        help='text output file from TeraMap',               dest='input_file')
    parser.add_option('-o','--output-filename',   help='output report file name',                     dest='output_fn')
    parser.add_option('-d','--dnb-architecture',  help='string representation of dnb architecture',   dest='dnb_architecture', default='ad2_29_29')
    parser.add_option('-r','--reference-build',   help='UCSC name of the human reference build used', dest='reference_build', default='hg18')

    # parse command line arguments.
    options,args = parser.parse_args(arguments[1:])

    # check reference build compatibility.
    supportedBuilds = set(['hg18','hg19'])
    if not options.reference_build in supportedBuilds:
        raise Exception('Reference build %s not supported!' % options.reference_build)

    return options.input_file, options.output_fn, options.dnb_architecture, options.reference_build

def main(arguments):
    # parse arguments.
    inputFileName, outputFileName, dnbArchitecture, referenceBuild = parse_arguments(arguments)

    # create dnb architecture object to get dnb length.
    da = DnbArchitecture(DnbArchitectureStr = dnbArchitecture)

    # initialize module level counters.
    initializeContainers(da.leftArmLength() + da.rightArmLength())

    # load TeraMap output file.
    #   Currently the DnbLibCollection by default filters all non-unique & non-fully mapped dnbs.
    log('loading TeraMap file: %(inputFileName)s' % vars())
    dc = MappedDnbCollection(inputFileName)
    dc.loadDnbMappings(DnbArchitectureParam=dnbArchitecture, FullUniqueMappingsOnly=True)
    log('  - loaded dnb count: %d' % (len(dc.dnbs)))

    # index dnbs by chromosome.
    log('  - indexing dnbs by chromosome...')
    dc.indexByChromosome()

    # loop through chromosomes with dnb mappings.
    for chrName in dc.dnbsByChrom.keys():
        # skip chrM.
        if chrName == 'chrM': continue

        log('    - loading chromosome: %s' % chrName)
        chrFn = pjoin(CHROM_DIR, referenceBuild, chrName + '.fa')
        chrSeq = ChrSequence(chrFn)

        # loop through fully mapped unique dnbs.
        for dnb in dc.dnbsByChrom[chrName]:
            # process arms to get full dnb sequence, discordanct positions, and reference bases.
            bases, refBases, posDisc = find_discordances(dnb.leftArm, dnb.rightArm, chrSeq)

            # process arms to get reference sequence adjacent to mappings
            find_adjacent_seq(dnb.leftArm, dnb.rightArm, chrSeq)

            # increment counters for positional discordance, correlated discordance, etc.
            process_disc_positions(bases, refBases, posDisc)

            # get gap symmetry for fully mapped unique dnbs
            find_gapDiffs(dnb.leftArm, dnb.rightArm)

            # calculate GC content
            find_gc_content(dnb.leftArm, dnb.rightArm)

            # find sequence dups
            find_seq_dups(dnb.leftArm, dnb.rightArm)

    # write out report.
    write_report(outputFileName)

main(sys.argv)
