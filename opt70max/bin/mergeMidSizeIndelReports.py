#!/usr/bin/python
#######################################################################
import os, sys, string, time
from optparse import OptionParser


######################################################################

HEADER = '>Id'+'\t'+'LeftChr'+'\t'+'LeftPosition'+'\t'+'LeftStrand'+'\t'+'LeftLength'+'\t'+'RightChr'+'\t'+'RightPosition'+'\t'+'RightStrand'+'\t'+'RightLength'+'\t'+'StrandConsistent'+'\t'+'Interchromosomal'+'\t'+'Distance'+'\t'+'DiscordantMatePairAlignments'+'\t'+'JunctionSequenceResolved'+'\t'+'TransitionSequence'+'\t'+'TransitionLength'+'\t'+'LeftRepeatClassification'+'\t'+'RightRepeatClassification'+'\t'+'LeftGenes'+'\t'+'RightGenes'+'\t'+'XRef'+'\t'+'DeletedTransposableElement'+'\t'+'KnownUnderrepresentedRepeat'+'\t'+'FrequencyInBaselineGenomeSet'+'\t'+'AssembledSequence'

######################################################################

def runCommand ( cmd, echo, run ):
    
    if echo:
        print cmd
    
    rc = 0

    if run:
        rc = os.system(cmd);
        
    if rc != 0:
        print 'Error code: ',rc
        sys.exit(rc)
    

######################################################################


if __name__ == '__main__':
    
    usage = 'usage: %prog [options]'
    parser = OptionParser(usage=usage)
    parser.add_option ('--input-dir'   , '-i'  , dest = 'input_dir'   , default = '.'     ,                         help = 'input directory (default = current))');
    parser.add_option ('--output-file' , '-o'  , dest = 'output_file' , default = ''      ,                         help = 'output file (default = stdout)');
    parser.add_option ('--no-action'   , '-n'  , dest = 'no_action'   , default = False   , action = 'store_true' , help = 'no action - just echo the commands');
    parser.add_option ('--quiet'       , '-q'  , dest = 'quiet'       , default = False   , action = 'store_true' , help = 'quiet mode - do not echo the commands');
    parser.add_option ('--distance'    , '-d'  , dest = 'distance'    , default = 0       , type   = 'int',         help = 'distsance threshold for merging');
    parser.add_option ('--reference'   , '-r'  , dest = 'reference'   , default = ''      ,                         help = 'reference file');
    parser.add_option ('--pattern'     , '-p'  , dest = 'pattern'     , default = ''      ,                         help = 'reference file');
    
    (options, args) = parser.parse_args()
    if len(args) > 0:
        print 'Invalid argument(s):',string.join(args,' ')

    ##### print header ################################################
    
    command = 'echo ' + "'" + HEADER + "'"

    if len(options.output_file) != 0:
        command += ' >"' + options.output_file + '"'       

    runCommand( command, not options.quiet, not options.no_action );
    
    ##### merge reports ###############################################
    
    command = 'find "' + options.input_dir + '" | grep ' + options.pattern + ' | xargs grep -h -v LeftChr | sort -g'

    if len(options.output_file) != 0:
        command += ' >>"' + options.output_file + '"'       

    runCommand( command, not options.quiet, not options.no_action );

    if options.distance > 0:
        command = 'cp ' + options.output_file + ' ' + options.output_file + '.unfiltered'
        runCommand( command, not options.quiet, not options.no_action );
        
        command = 'svtools remove-duplicates --input ' + options.output_file + ' --output ' + options.output_file + ' --reference ' + options.reference + ' --distance ' + ('%d' % options.distance) 
        runCommand( command, not options.quiet, not options.no_action );

                
