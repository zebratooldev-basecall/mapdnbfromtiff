#!/bin/env python
import os,sys
import optparse
from cgatools.util import DelimitedFile
from cgatools.reference import CrrFile

def addNonDiploidSegmentsFields(segDF):
    segDF.addField('chr')
    segDF.addField('begin',conv=int)
    segDF.addField('end',conv=int)
    #segDF.addField('relativeCvg',sub={'N':'.'})
    segDF.addField('calledLevel',sub={'N':'.'})
    segDF.addField('levelScore')

def addNonDiploidDetailsFields(detDF):
    detDF.addField('chr')
    detDF.addField('begin',conv=int)
    detDF.addField('end',conv=int)
    detDF.addField('bestLAF',conv=float)
    detDF.addField('lowLAF',conv=float)
    detDF.addField('highLAF',conv=float)
    
def addDiploidDetailsFields(detDF):
    detDF.addField('chr')
    detDF.addField('begin',conv=int)
    detDF.addField('end',conv=int)
    #detDF.addField('avgNormalizedCvg',sub={'N':'.'})
    detDF.addField('gcCorrectedCvg',sub={'N':'.'})
    #detDF.addField('fractionUnique')
    #detDF.addField('relativeCvg',sub={'N':'.'})
    detDF.addField('calledPloidy', sub={'N':'.'})
    detDF.addField('calledCNVType',sub={'hypervariable':'.', 'invariant':'.'} )
    detDF.addField('ploidyScore')
    detDF.addField('CNVTypeScore')
    
def write_cnv_vcf_line(out,chrTdet,beginTdet,endTdet,tGCmean,nGCmean,
                       calledLevelTseg,levelScoreTseg,
                       calledLevelNseg,levelScoreNseg,
                       gcCorrectedCvgTdet,calledPloidyTdet,calledCNVTypeTdet,ploidyScoreTdet,CNVTypeScoreTdet,
                       gcCorrectedCvgNdet,calledPloidyNdet,calledCNVTypeNdet,ploidyScoreNdet,CNVTypeScoreNdet,
                       calledLevelNdSseg,levelScoreNdSseg,
                       bestLafNdSdet,lowLafNdSdet,highLafNdSdet,crrFile):

    chr = chrTdet
    if chr[0:3] == 'chr':
        chr = chrTdet[3:]


    refBase = crrFile.getSequence(chrTdet,beginTdet,beginTdet+1)
    
    if gcCorrectedCvgNdet == '.':
        baselineGC = '.'
    else:
        baselineGC = '%.3f' % (float(gcCorrectedCvgNdet)/nGCmean)

    if gcCorrectedCvgTdet == '.':
        nonbaselineGC = '.'
    else:
        nonbaselineGC = '%.3f' % (float(gcCorrectedCvgTdet)/nGCmean)
        
    baseline = '.:%s:%s:%s:%s:%s:%s:%s:.:.:.:.:.' % ( baselineGC,
                                                calledPloidyNdet, ploidyScoreNdet,
                                                calledCNVTypeNdet, CNVTypeScoreNdet,
                                                calledLevelNseg,levelScoreNseg)
    nonbaseline = '.:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s' % ( nonbaselineGC,
                                                         calledPloidyTdet, ploidyScoreTdet,
                                                         calledCNVTypeTdet, CNVTypeScoreTdet,
                                                         calledLevelTseg,levelScoreTseg,
                                                         calledLevelNdSseg,levelScoreNdSseg,
                                                         bestLafNdSdet,lowLafNdSdet,highLafNdSdet)


    out.write('%s\t%d\t.\t%s\t<CGA_CNVWIN>\t.\t.\tNS=2;CGA_WINEND=%d\tGT:CGA_GP:CGA_CP:CGA_PS:CGA_CT:CGA_TS:CGA_CL:CGA_LS:CGA_SCL:CGA_SLS:CGA_LAF:CGA_LLAF:CGA_ULAF\t%s\t%s\n' % 
              (chr,beginTdet+1,refBase,endTdet,baseline,nonbaseline) )

def computeMeanGcCvg(detailsFile):
    df = DelimitedFile(detailsFile)
    df.addField('gcCorrectedCvg')
    sum = 0.
    count = 0
    for gc in df:
        if gc[0] != 'N':
            sum += float(gc[0])
            count += 1
    return sum/float(count)

def write_cnv_vcf_header(out,baselineId,nonbaselineId):
    out.write('##fileformat=VCFv4.1\n' +
              '##ALT=<ID=CGA_CNVWIN,Description="Copy number analysis window">\n' +
              '##INFO=<ID=NS,Number=1,Type=Integer,Description="Number of Samples With Data">\n' +
              '##INFO=<ID=CGA_WINEND,Number=1,Type=Integer,Description="End of coverage window">\n' +
              '##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">\n' +
              '##FORMAT=<ID=CGA_GP,Number=1,Type=Float,Description="Depth of coverage for 2k window GC normalized to mean">\n' +
              '##FORMAT=<ID=CGA_CL,Number=1,Type=Float,Description="Nondiploid-model called level">\n' +
              '##FORMAT=<ID=CGA_LS,Number=1,Type=Integer,Description="Nondiploid-model called level score">\n' +
              '##FORMAT=<ID=CGA_CP,Number=1,Type=Integer,Description="Diploid-model called ploidy">\n' +
              '##FORMAT=<ID=CGA_PS,Number=1,Type=Integer,Description="Diploid-model called ploidy score">\n' +
              '##FORMAT=<ID=CGA_CT,Number=1,Type=String,Description="Diploid-model CNV type">\n' +
              '##FORMAT=<ID=CGA_TS,Number=1,Type=Integer,Description="Diploid-model CNV type score">\n' +
              '##FORMAT=<ID=CGA_SCL,Number=1,Type=Float,Description="Nondiploid-model somatic called level">\n' +
              '##FORMAT=<ID=CGA_SLS,Number=1,Type=Integer,Description="Nondiploid-model somatic called level score">\n' +

              '##FORMAT=<ID=CGA_LAF,Number=1,Type=Float,Description="Lesser allele fraction estimate, 100k window">\n' +
              '##FORMAT=<ID=CGA_LLAF,Number=1,Type=Float,Description="Lesser allele fraction lower bound, 100k window">\n' +
              '##FORMAT=<ID=CGA_ULAF,Number=1,Type=Float,Description="Lesser allele fraction upper bound, 100k window">\n' +

              ('#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t%s\t%s\n' % (baselineId,nonbaselineId) ) )


def makeVCF(baselineSampleId,nonbaselineSampleId,
            nonDiploidNonbaseline,diploidNonbaseline,
            nonDiploidBaseline,diploidBaseline,
            nonDiploidSomaticSeg,nonDiploidSomaticDet,
            outFile,crrFile):

    # notes about abbreviations in variable names:
    # 'nd' == nondiploid
    # 't' == 'T' == tumor == nonbaseline
    # 'n' == 'N' == normal == baseline
    # 'S' == somatic == analysis of nonbaseline normalized with baseline
    # 'det' == details file data
    # 'seg' == segments file data


    ndTseg = DelimitedFile(nonDiploidNonbaseline)
    addNonDiploidSegmentsFields(ndTseg)

    ndNseg = DelimitedFile(nonDiploidBaseline)
    addNonDiploidSegmentsFields(ndNseg)

    ndSseg = DelimitedFile(nonDiploidSomaticSeg)
    addNonDiploidSegmentsFields(ndSseg)

    ndSdet = DelimitedFile(nonDiploidSomaticDet)
    addNonDiploidDetailsFields(ndSdet)

    dTdet = DelimitedFile(diploidNonbaseline)
    addDiploidDetailsFields(dTdet)
    
    dNdet = DelimitedFile(diploidBaseline)
    addDiploidDetailsFields(dNdet)

    tGCmean = computeMeanGcCvg(diploidNonbaseline)
    nGCmean = computeMeanGcCvg(diploidBaseline)

    out = open(outFile,'w')
    write_cnv_vcf_header(out,baselineSampleId,nonbaselineSampleId)

    chrTdet = ''
    chrNseg = ''
    chrNdet = ''
    chrNdSseg = ''
    chrNdSdet = ''

    # loop over all segments of the non-diploid-model segments for the nonbaseline
    for (chrTseg,beginTseg,endTseg,calledLevelTseg,levelScoreTseg) in ndTseg:

       print 'working on ' + chrTseg + ' ' + str(beginTseg) + ' ' + str(endTseg)
       
       # while there are still details lines within the outer-loop segment;
       # non-baseline and baseline files should be synchronized, so we only test on one of them
       while chrTdet != chrTseg or endTdet < endTseg:
           # read in diploid details lines 
           (chrTdet,beginTdet,endTdet,gcCorrectedCvgTdet,calledPloidyTdet,calledCNVTypeTdet,ploidyScoreTdet,CNVTypeScoreTdet) = dTdet.next()
           (chrNdet,beginNdet,endNdet,gcCorrectedCvgNdet,calledPloidyNdet,calledCNVTypeNdet,ploidyScoreNdet,CNVTypeScoreNdet) = dNdet.next()

           # if necessary, read a new segment line from the other segment files
           if chrTseg != chrNseg or beginTdet >= endNseg:
               (chrNseg,beginNseg,endNseg,calledLevelNseg,levelScoreNseg) = ndNseg.next()
           if chrTseg != chrNdSseg or beginTdet >= endNdSseg:
               (chrNdSseg,beginNdSseg,endNdSseg,calledLevelNdSseg,levelScoreNdSseg) = ndSseg.next()

           # if necessary, read a new line from the somatic nondiploid segment file
           if chrTseg != chrNdSdet or beginTdet >= endNdSdet:
               (chrNdSdet,beginNdSdet,endNdSdet,bestLafNdSdet,lowLafNdSdet,highLafNdSdet) = ndSdet.next()

           # sanity check the various segments and windows: all others should either be contained in (if details)
           # or overlap (if segments) the primary segment
           if chrTdet != chrTseg or chrNseg != chrTseg or chrNdet != chrTseg or chrNdSseg != chrTseg \
              or beginTdet < beginTseg or beginNdet != beginTdet \
              or endTdet > endTseg or endNdet != endTdet or beginNseg >= endTdet or beginNdSseg >= endTdet \
              or  endTdet > endNseg or endNdSseg < endTdet:
               raise Exception('Trouble with file synchronization/iteration')

           # finally, write out a line for each smaller window
           write_cnv_vcf_line(out,chrTdet,beginTdet,endTdet,tGCmean,nGCmean,
                              calledLevelTseg,levelScoreTseg,
                              calledLevelNseg,levelScoreNseg,
                              gcCorrectedCvgTdet,calledPloidyTdet,calledCNVTypeTdet,ploidyScoreTdet,CNVTypeScoreTdet,
                              gcCorrectedCvgNdet,calledPloidyNdet,calledCNVTypeNdet,ploidyScoreNdet,CNVTypeScoreNdet,
                              calledLevelNdSseg,levelScoreNdSseg,
                              bestLafNdSdet,lowLafNdSdet,highLafNdSdet,crrFile)


        

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(baselineSampleId='',nonbaselineSampleId='',
                        nonDiploidNonbaseline='',diploidNonbaseline='',
                        nonDiploidBaseline='',diploidBaseline='',
                        nonDiploidSomaticSeg='',nonDiploidSomaticDet='',
                        outFile='', crrFile='')
    parser.add_option('--nondiploid-nonbaseline',dest='nonDiploidNonbaseline',
                      help='Nondiploid-model CNV segments file for non-baseline sample')
    parser.add_option('--diploid-nonbaseline',dest='diploidNonbaseline',
                      help='Diploid-model CNV details file for non-baseline sample')
    parser.add_option('--nondiploid-baseline',dest='nonDiploidBaseline',
                      help='Nondiploid-model CNV segments file for baseline sample')
    parser.add_option('--diploid-baseline',dest='diploidBaseline',
                      help='Diploid-model CNV details file for baseline sample')
    parser.add_option('--nondiploid-somatic-segments',dest='nonDiploidSomaticSeg',
                      help='Somatic nondiploid-model CNV segments file for non-baseline sample')
    parser.add_option('--nondiploid-somatic-details',dest='nonDiploidSomaticDet',
                      help='Somatic nondiploid-model CNV details file for non-baseline sample')
    parser.add_option('--baseline-sample-id',dest='baselineSampleId',
                      help='Sample ID for baseline')
    parser.add_option('--nonbaseline-sample-id',dest='nonbaselineSampleId',
                      help='Sample ID for non-baseline')
    parser.add_option('--output-file',dest='outFile',
                      help='name of file to contain output')
    parser.add_option('--crr-file',dest='crrFile',
                      help='Reference crr file')
    
    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.nonDiploidNonbaseline:
        parser.error('nondiploid-nonbaseline specification required')
    if not options.diploidNonbaseline:
        parser.error('diploid-nonbaseline specification required')
    if not options.nonDiploidBaseline:
        parser.error('nondiploid-baseline specification required')
    if not options.diploidBaseline:
        parser.error('diploid-baseline specification required')
    if not options.nonDiploidSomaticSeg:
        parser.error('nondiploid-somatic-segments specification required')
    if not options.nonDiploidSomaticDet:
        parser.error('nondiploid-somatic-details specification required')
    if not options.baselineSampleId:
        parser.error('baseline-sample-id specification required')
    if not options.nonbaselineSampleId:
        parser.error('nonbaseline-sample-id specification required')
    if not options.outFile:
        parser.error('output-file specification required')
    if not options.crrFile:
        parser.error('crr-file specification required')

    crrFile = CrrFile(options.crrFile)
    
    makeVCF(options.baselineSampleId,options.nonbaselineSampleId,
            options.nonDiploidNonbaseline,options.diploidNonbaseline,
            options.nonDiploidBaseline,options.diploidBaseline,
            options.nonDiploidSomaticSeg,options.nonDiploidSomaticDet,
            options.outFile,crrFile)


if __name__ == '__main__':
    main()

