#!/usr/bin/env python

import os, sys, re, glob
from score_converter import score_convertor 
from optparse import OptionParser

conv = score_convertor()
lines_to_skip = 36 

################################# Change Log ############################
'''
        2015_12_23  When the nanocall csv file is in error stat, the lines
                    will not include T C G A tags
'''

################################# Object #################################
class file_handle:
    def __init__(self,fn):
        try:
            self.ff = open(fn,'r')
        except:
            print 'cannot open files %s' % fn
            sys.exit(-1)
        for i in range(lines_to_skip): self.ff.readline()
        self.eof = False
    def close(self):
        self.ff.close()
    def read_lines(self):
        index,base,score = None, None, None
        line = self.ff.readline()
        if line is None or line == "":
            self.eof = True
            return index,base, score
        # Fix an error when it call N
        try:
            Index,Call,Score,TxR,Fit,Cy5,Cy3,T,C,G,A = line.split(',')
        except ValueError:
            Index,Call,Score,TxR,Fit,Cy5,Cy3 = line.split(',')

        index = int(Index)
        base = Call
        score = float(Score)
        return index,base, score

class nano_call_files:
    def __init__(self,files,extra_files):
        self.head_list = []
        self.extra_head_list = []
        for fn in files:
            self.head_list.append(file_handle(fn))
        for fn in extra_files:
            self.extra_head_list.append(file_handle(fn))
        self.eof=False
    def stack_one_dnb(self):
        index = []
        base = []
        score = []
        for fn in self.head_list:
            i,b,s = fn.read_lines()
            index.append(i)
            base.append(b)
            score.append(s)
        index=list(set(index))
        
        extrabase = []
        for fn in self.extra_head_list:
            i,b,s = fn.read_lines()
            extrabase.append(b)

        if len(index) != 1:
            print 'Multi-index happened! %s ' % ','.join(index)
            sys.exit(-1)
        index = index.pop(0)
        if index is None:
            self.eof = True
            return index,base,score,''

        base=''.join(base)
        #score=''.join(['A' for x in range(len(base))])
        score=''.join([str(x) for x in conv.digitize(score,return_phred=True)])
        if extrabase:
            extrabase=''.join(extrabase)
        else:
            extrabase=''
        
        return index,base,score,extrabase

def parse_arguments(args):
    parser = OptionParser()
    parser.add_option('--slide', help='slide barcode', default=None, dest='slide')
    parser.add_option('--lane', help='lane', default=None, dest='lane')
    parser.add_option('--nano-call-file-base', help='the parent dir holds the nanocall files', default=None, dest='dir_base')
    parser.add_option('--filed-id',help='field id', default=None, dest='field')
    parser.add_option('--cycle-map',help='input cycle map', default=None, dest='cycle_map')

    options, args = parser.parse_args(args)
    return options.slide, options.lane, options.dir_base, options.field, options.cycle_map

def main(argv):
    slide, lane, dir_base, field, cycle_map = parse_arguments(argv)

    files = []
    extra_files = []
    if cycle_map:
        temp_dict = {}
        for line in open(cycle_map).readlines():
            pos,path = line.rstrip('\r\n').split('\t')
            if '*' in pos: 
                extra_files.append(path)
                continue
            pos = int(pos)
            temp_dict[pos] = path
        files = [temp_dict[x] for x in sorted(temp_dict.keys())]
    else:
        slides_masked = slide.split('-').pop(0)[0:-3] + 'xxx' + '-FS3'
        path = os.path.join(dir_base,slides_masked,slide,'C*',lane,'%s.C*.%s_NanoCall.csv' % (slide,field) )
        files = sorted(glob.glob(path))

    sys.stderr.write("using these nanocall files: %s\n" % ("\n".join(files)) + "Extra files: %s\n" % ("\n".join(extra_files)))

    obj = nano_call_files(files,extra_files)
    counter = 0
    while True:
        index,base,score,extrabase = obj.stack_one_dnb()
        if index is None: break
        if extrabase:
            print "@index-%s-%d\n%s\n+\n%s" % (extrabase,index,base,score)
        else:
            print "@index-%d\n%s\n+\n%s" % (index,base,score)
        counter += 1
        #if counter == 1000: break
main(sys.argv)
