#!/usr/bin/python
import os, sys
from optparse import OptionParser

import numpy
import matplotlib, matplotlib.colors

LINUX_HOARD_ROOT = '/Proj'
WINDOWS_HOARD_ROOT = '///hoard'

def log10(n):
    import math
    return math.log(n) / math.log(10)

def extractImageLocation(filename):
    IN_PARAM_SPEC = 'input-dir='
    f = open(filename, 'r')
    for x in f:
        start = x.find(IN_PARAM_SPEC)
        if start != -1:
            start += len(IN_PARAM_SPEC)
            if x[start] == '"':
                end = x.find('"', start+1)
                if end == -1:
                    return None
            else:
                end = x.find(' ', start+1)
                if end == -1:
                    end = len(x)
            loc = x[start:end]
            if loc.startswith(LINUX_HOARD_ROOT):
                return loc[len(LINUX_HOARD_ROOT):]
    return None

def getImageFilenamePattern(lane):
    slide = int(lane.name[2:-8])
    return "/BaseCall/C%%02d/SLIDE%06d-%s-C%%02d-reduced.png" % (slide, lane.name[-3:])

def appendImageLinkHtml(htm, imageLocation, filenamePattern, cycle):
    if not imageLocation or not filenamePattern:
        return
    fn = imageLocation + (filenamePattern % (cycle, cycle))
    print >>htm, 'Image: '
    print >>htm, '<a href="file://%s%s" target="_blank">windows</a>' % (WINDOWS_HOARD_ROOT, fn)
    print >>htm, '<a href="file://%s%s" target="_blank">linux</a><p>' % (LINUX_HOARD_ROOT, fn)

discordanceColorMapData = \
    {'red':   ((0.0,  0.0, 0.0),
               (1.0,  1.0, 1.0)),
     'green': ((0.0,  0.0, 0.0),
               (0.0001,  0.0, 1.0),
               (1.0,  0.0, 0.0)),
     'blue':  ((0.0,  0.0, 0.0),
               (1.0,  0.0, 0.0))}
discordanceColorMap = matplotlib.colors.LinearSegmentedColormap('discmap', discordanceColorMapData, 256)

ratioColorMapData = \
    {'red':   ((0., 0, 0),
               (0.35, 0, 0),
               (0.66, 1, 1),
               (0.89,1, 1),
               (1, 0.5, 0.5)),
     'green': ((0., 0, 0),
               (0.125,0, 0),
               (0.39,1, 1),
               (0.61,1, 1),
               (0.91,0,0),
               (1, 0, 0)),
     'blue':  ((0.,0.,0.),
               (0.0001, 0.5, 0.5),
               (0.11, 1, 1),
               (0.275, 1, 1),
               (0.55,0, 0),
               (1, 0, 0))}
ratioColorMap = matplotlib.colors.LinearSegmentedColormap('ratiomap', ratioColorMapData, 256)

def writePng(htm, directory, filename):
    import matplotlib
    matplotlib.use('Agg')
    from pylab import savefig
    savefig(os.path.join(directory, filename + '.png'))
    print >>htm, '<img src="%s.png">' % filename


def writeOutput(collectionFile, directory, lane, data):
    import numpy
    import math
    import matplotlib
    matplotlib.use('Agg')
    from pylab import figure, subplot, title, colorbar, imshow, Normalize

    if not os.path.exists(directory):
        os.makedirs(directory)

    imageLocation = extractImageLocation(collectionFile)
    if imageLocation:
        filenamePattern = getImageFilenamePattern(lane)
    else:
        filenamePattern = None

    namePrefix = os.path.join(directory, lane.name + '-')
    htm = open(os.path.join(directory, lane.name + '-disc.html'), 'w')
    print >>htm, '<html>'
    print >>htm, '<title>%s</title>' % lane.name

    print >>htm, '<body>'
    print >>htm, 'lane: %s<p>' % lane.name
    print >>htm, 'collection: %s<p>' % collectionFile
    if imageLocation:
        print >>htm, 'images: %s%s<p>' % (LINUX_HOARD_ROOT, imageLocation)


    # overall DNB counts
    print >>htm, '<h3>Lane overview</h3>'
    figure(100, (10, 4))
    subplot(121)
    title('DNB count')
    imshow(data.dnbCount, interpolation='nearest', norm=Normalize(vmin=100), cmap=ratioColorMap)
    colorbar()

    # worst discordance
    subplot(122)
    title('worst disc.')
    worstDiscordance = numpy.amax(data.empiricalDiscordance, axis=0)
    imshow(worstDiscordance, interpolation='nearest', norm=Normalize(0.001,0.15), cmap=discordanceColorMap)
    colorbar()
    writePng(htm, directory, lane.name + '-overview')

    # overview of ratio
    print >>htm, '<h3>Min/max ratios across cycles</h3>'
    figure(101, (10, 4))
    subplot(121)
    title('max ratio')
    imshow(numpy.amax(data.discordanceRatio, axis=0),
           interpolation='nearest', cmap=ratioColorMap)
    colorbar()
    minRatio = numpy.amin(numpy.ma.masked_array(data.discordanceRatio, data.discordanceRatio==0), axis=0)
    subplot(122)
    title('min ratio')
    imshow(minRatio.filled(0), interpolation='nearest', cmap=ratioColorMap)
    colorbar()
    writePng(htm, directory, lane.name + '-ratio-overview')


    for ii in range(0, len(data.dataPointCount)):
        pos = ii + 1
        if pos in lane.cycleMap:
            cycle = lane.cycleMap[pos]
        else:
            cycle = 1000+pos
        print 'plotting pos %02d, C%02d' % (pos, cycle)
        print >>htm, '<h3>DNB position %02d, cycle C%02d</h3>' % (pos, cycle)
        appendImageLinkHtml(htm, imageLocation, filenamePattern, cycle)
        figure(ii, (14, 4))
        subplot(151)
        title('ratio')
        imshow(data.discordanceRatio[ii],
               interpolation='nearest',
               cmap=ratioColorMap,
               norm=Normalize(0.001,2.0))
        colorbar()
        subplot(152)
        title('log10(ratio)')
        imshow(numpy.log10(data.discordanceRatio[ii]),
               interpolation='nearest',
               cmap=ratioColorMap,
               norm=Normalize(-2.0,2.0))
        colorbar()
        subplot(153)
        title('actual')
        imshow(data.empiricalDiscordance[ii],
               interpolation='nearest',
               cmap=discordanceColorMap,
               norm=Normalize(0.001,0.1))
        colorbar()
        subplot(154)
        title('estimated')
        imshow(data.estimatedDiscordance[ii],
               interpolation='nearest',
               cmap=discordanceColorMap,
               norm=Normalize(0.001,0.1)
               )
        colorbar()
        subplot(155)
        title('est-unnorm')
        imshow(data.estimatedDiscordance[ii],
               interpolation='nearest',
               cmap=discordanceColorMap
               )
        colorbar()

        fn = '%s-c%02d' % (lane.name, cycle)
        writePng(htm, directory, fn)

    print >>htm, '</body><html>'
    htm.close()

def writeCycleOrderHtml(collectionFile, directory, lane):
    print 'writing cycle order file for:', lane.name
    imageLocation = extractImageLocation(collectionFile)
    if imageLocation:
        filenamePattern = getImageFilenamePattern(lane)
    else:
        filenamePattern = None
    htm = open(os.path.join(directory, lane.name + '-cycleorder.html'), 'w')
    print >>htm, '<html>'
    print >>htm, '<title>%s (Cycle Order)</title>' % lane.name

    print >>htm, '<body>'
    print >>htm, 'lane: %s<p>' % lane.name
    print >>htm, 'collection: %s<p>' % collectionFile
    if imageLocation:
        print >>htm, 'images: %s%s<p>' % (LINUX_HOARD_ROOT, imageLocation)

    cycleOrder = [ (cycle, pos) for (pos, cycle) in lane.cycleMap.items() ]
    cycleOrder.sort()

    for (cycle, pos) in cycleOrder:
        print >>htm, '<h3>Cycle C%02d, DNB position %02d</h3>' % (cycle, pos)
        appendImageLinkHtml(htm, imageLocation, filenamePattern, cycle)
        fn = '%s-c%02d' % (lane.name, cycle)
        print >>htm, '<img src="%s.png">' % fn

    print >>htm, '</body><html>'
    htm.close()

def readInputData(fileName):
    print 'reading data from:', fileName
    f = open(fileName, 'r')
    f.readline() # skip header
    points = []
    mcol = mrow = mcycle = 0
    for x in f:
        data = x.split(',')
        col = int(data[0][4:6])-1
        row = int(data[0][7:9])-1
        cycle = int(data[1])
        total = float(data[2])
        estimate = float(data[3])
        real = float(data[4])
        points.append( (cycle, col, row, total, estimate, real) )
        if col > mcol:
            mcol = col
        if row > mrow:
            mrow = row
        if cycle > mcycle:
            mcycle = cycle

    empiricalDiscordance = numpy.zeros( (mcycle+1, mrow+1, mcol+1),  dtype=float )
    estimatedDiscordance = numpy.zeros( (mcycle+1, mrow+1, mcol+1),  dtype=float )
    discordanceRatio = numpy.zeros( (mcycle+1, mrow+1, mcol+1),  dtype=float )
    dataPointCount = numpy.zeros( (mcycle+1, mrow+1, mcol+1) )
    dnbCount = numpy.zeros((mrow+1, mcol+1) )
    for (cycle, col, row, total, estimate, real) in points:
        if total != 0:
            empiricalDiscordance[cycle, row, col] = real / total
            estimatedDiscordance[cycle, row, col] = estimate / total
        if estimate != 0:
            discordanceRatio[cycle, row, col] = real / estimate
        dataPointCount[cycle, row, col] = total
        if total > dnbCount[row, col]:
            dnbCount[row, col] = total

    class Data:
        pass

    d = Data()
    d.dnbCount = dnbCount
    d.dataPointCount = dataPointCount
    d.discordanceRatio = discordanceRatio
    d.empiricalDiscordance = empiricalDiscordance
    d.estimatedDiscordance = estimatedDiscordance
    return d

def main():
    from dnbcollection import Collection

    parser = OptionParser('usage: %prog [options]')
    parser.add_option('-d', '--dnb-collection', dest='collection',
                      help="name of the DNB collection file")
    parser.add_option('--input', dest='input',
                      help="name of the input CSV file with per-cycle discordance data")
    parser.add_option('--output', dest='output',
                      help="output directory; html report will be created there")
    parser.add_option('--cycle-order-only', dest='cycleOrder', action='store_true',
                      help="write additional HTML file in cycle order")
    (options, args) = parser.parse_args()
    if len(args) != 0:
        raise Exception('unexpected arguments')

    if not options.collection or not options.input or not options.output:
        raise Exception('all parameters are mandatory (see help)')

    collection = Collection(options.collection)
    if len(collection.lanes) != 1:
        raise Exception('expected a one-lane collection')
    if not options.cycleOrder:
        data = readInputData(options.input)
        writeOutput(options.collection, options.output, collection.lanes[0], data)
    else:
        writeCycleOrderHtml(options.collection, options.output, collection.lanes[0])

if __name__ == '__main__':
#    try:
        main()
#    except Exception, msg:
#        print 'error: ', msg
#        sys.exit(1)
#    sys.exit(0)






