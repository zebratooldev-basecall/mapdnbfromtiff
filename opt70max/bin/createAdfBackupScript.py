#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
import glob
import re

# import pprint
# import cPickle as pickle

###### Document Decription
''' Create a backup shell for ADF directory to second storage.

    Structure of backup directory (Use GS800002219-DID as example):
    BackupRoot/
    |-- DID
    |   `-- GS800002xxx-DID
    |       `-- GS800002219-DID
    |           `-- ADF
    |               `-- GS66018-FS3-L04
    |                   |-- GS66018-FS3-L04.stat
    |                   |-- GS66018-FS3-L04_barcode_1
    |                   |   |-- GS66018-FS3-L04_barcode_1.calls
    |                   |   |-- GS66018-FS3-L04_barcode_1.scores
    |                   |   |-- GS66018-FS3-L04_barcode_1.tag-scores
    |                   |   |-- GS66018-FS3-L04_barcode_1.tags
    |                   |   |-- GS66018-FS3-L04_barcode_1.xml
    |                   |   `-- GS66018-FS3-L04_barcode_1.xml.bk
    |                   |-- barcodeList.txt
    |                   |-- cyclemap.csv
    |                   |-- laneinfo
    |                   |-- makeADF.json
    |                   |-- md5_check.log
    |                   |-- nanocall.tsv.gz
    |                   `-- performance.tsv.gz
    `-- slides
        `-- GS66xxx-FS3
            `-- GS66018-FS3
                `-- GS66018-FS3-L04 -> ../../../DID/GS800002xxx-DID/GS800002219-DID/ADF/GS66018-FS3-L04

    1. If paraList is given, return the shell in a scrat submiting mode. parameters will be stroage in paraList. Otherwise,
       return the text of command lines connected by "&&" for running separately.
    2. If DID can be a list file in format: "laneId\tDID", to separate lanes in different DID in a ADF folder. If DID is a string in format 'GS\d{9}-DID$', all lanes will be treat with same DID.

    Modified:
        Ver 1.1.0 2014.07.07: Removed umask parameter, use 'closseAccess' option(rsync --chmod=xxx) instead.
        Ver 1.1.1 2014.07.10: Add RSYNCED flag. Check md5 again only when rsync is really processed.
        Ver 1.2.0 2014.12.09: Change the type of value from string to list in pipeline.
        Ver 1.2.1 2015.03.31: Allow backup FASTQ directory instead of ADF.
        Ver 1.2.2 2015.06.10: Add support for 8-digit slide id.
'''

###### Version and Date
prog_version = '1.2.2'
prog_date = '2015.06.10'

###### Usage
usage = '''

     Version %s  by Vincent-Li  %s

     Usage: %s <adfDir> <backupDir> <DID> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################
def generateBackupScript(fromDir, toDir, did, paraList='', closeAccess=False, deleteFiles=False):
    
    lanePathList = glob.glob("%s/GS*-FS3-L*" % fromDir)
    if fromDir.endswith('/'):
        dirBaseName = os.path.basename(os.path.dirname(fromDir))
    else:
        dirBaseName = os.path.basename(fromDir)
    finalCommand = []

    for lanePath in lanePathList:

        ########### check the lanePath is symLink or not
        #### laneId eg: GS65925-FS3-L02 or GS12345678-FS3-L02
        laneId = os.path.basename(lanePath)

        #### slideId eg: GS65925-FS3 or GS12345678-FS3
        slideId = laneId[0:-4];

        #### slidePrefix eg: GS65xxx-FS3
        slidePrefix = slideId.replace(slideId[-7:], 'xxx-FS3')

        #### did eg: GS800002336-DID, didPrefix eg: GS800002xxx-DID
        if isinstance(did, dict):
            thisDid = did[laneId]
        else:
            thisDid = did
        didPrefix = thisDid.replace(thisDid[-7:], 'xxx-DID')

        ##### use regExp to check the format of input
        if not (re.match(r'GS\d{2}|\d{5}xxx-FS3$', slidePrefix) and re.match(r'GS\d{5}|\d{8}-FS3$', slideId) and re.match(r'GS\d{5}|\d{8}-FS3-L0\d', laneId)):
            raise ValueError, "[ERROR] invalid format of slidePrefix: %s, slideId: %s, laneId: %s" % (slidePrefix, slideId, laneId)

        ##### check DID format
        if not (re.match(r'GS\d{6}xxx-DID$', didPrefix) and re.match(r'GS\d{9}-DID$', thisDid)):
            raise ValueError, "[ERROR] invalid format of didPrefix: %s, did: %s" % (didPrefix, thisDid)

        ##### eg: AdfBackup/DID/GS800002xxx-DID/GS800002336-DID/ADF
        realBackupDir = "%s/DID/%s/%s/%s" % (toDir, didPrefix, thisDid, dirBaseName)
        realBackupLane = "%s/%s" % (realBackupDir, laneId)
        laneRelativePath = "../../../DID/%s/%s/%s/%s" % (didPrefix, thisDid, dirBaseName, laneId)

        ##### eg: AdfBackup/slides/GS65xxx-FS3/GS65925-FS3
        linkBackupDir = "%s/slides/%s/%s" % (toDir, slidePrefix, slideId)
        linkBackupLane = "%s/%s" % (linkBackupDir, laneId)

        md5File = "%s/md5_check.log" % realBackupLane

        deleteRenameLane = "%s-deleting" % lanePath

        if closeAccess:
            optionVal = '-rltp --chmod=u=rwX,g=rX,o= --delete'
        else:
            optionVal = '-rltp --chmod=u=rwX,go=rX --delete'

        command = []
        if isinstance(paraList, list):
            paraList.append({
                'REALBACKUPDIR': realBackupDir,
                'LINKBACKUPDIR': linkBackupDir,
                'LANEPATH': lanePath,
                'LINKBACKUPLANE': linkBackupLane,
                'LANERELATIVEPATH': laneRelativePath,
                'REAlBACKUPLANE': realBackupLane,
                'MD5FILE': md5File,
                'DELETERENAMELANE': deleteRenameLane
                })

        else:
            command.append('umask 022')
            command.append('mkdir -p %s' % realBackupDir)
            command.append('mkdir -p %s' % linkBackupDir)
            command.append('if [ -e %s ]; then chmod -R u+w %s; fi' % (realBackupLane, realBackupLane))
            command.append('if [ -e %s ] && [ ! -L %s ]; then export RSYNCED=1; rsync %s %s %s; fi' % (lanePath, lanePath, lanePath, optionVal, realBackupDir))
            command.append('if [ -L %s ]; then rm %s; fi' % (linkBackupLane, linkBackupLane))
            command.append('ln -s %s %s' % (laneRelativePath, linkBackupLane))
            command.append('cd %s' % realBackupLane)
            command.append('if [ "$RSYNCED" == "1" ]; then find -type f -exec md5sum {} \; | grep -v md5_check.log >%s; fi' % md5File)
            command.append('find -type d -exec chmod u-w {} \;')


            if deleteFiles == True:
                command.append('if [ -e %s ] && [ ! -L %s ]; then mv %s %s ; fi' % (lanePath, lanePath, lanePath, deleteRenameLane))
                command.append('if [ -L %s ]; then rm %s; fi' % (lanePath, lanePath))
                command.append('ln -s %s %s' % (realBackupLane, lanePath))
                command.append('if [ -e %s ]; then rm -r %s; fi' % (deleteRenameLane, deleteRenameLane))

            finalCommand.append(" && ".join(command))
    
    if isinstance(paraList, list):
        finalCommand.append('umask 022')
        finalCommand.append('mkdir -p ${REALBACKUPDIR}')
        finalCommand.append('mkdir -p ${LINKBACKUPDIR}')
        finalCommand.append('if [ -e ${REAlBACKUPLANE} ]; then chmod -R u+w ${REAlBACKUPLANE}; fi')
        finalCommand.append('if [ -e ${LANEPATH} ] && [ ! -L ${LANEPATH} ];then rsync %s ${LANEPATH} ${REALBACKUPDIR} && export RSYNCED=1; fi' % optionVal)
        finalCommand.append('if [ -L ${LINKBACKUPLANE} ]; then rm ${LINKBACKUPLANE}; fi')
        finalCommand.append('ln -s ${LANERELATIVEPATH} ${LINKBACKUPLANE}')
        finalCommand.append('cd ${REAlBACKUPLANE}')
        finalCommand.append('if [ "$RSYNCED" == "1" ]; then find -type f -exec md5sum {} \; | grep -v md5_check.log >${MD5FILE}; fi')
        finalCommand.append('find -type d -exec chmod u-w {} \;')

        if deleteFiles == True:
            finalCommand.append('if [ -e ${LANEPATH} ] && [ ! -L ${LANEPATH} ]; then mv ${LANEPATH} ${DELETERENAMELANE}; fi')
            finalCommand.append('if [ -L ${LANEPATH} ]; then rm ${LANEPATH}; fi')
            finalCommand.append('ln -s ${REAlBACKUPLANE} ${LANEPATH}')
            finalCommand.append('if [ -e ${DELETERENAMELANE} ]; then rm -r ${DELETERENAMELANE}; fi')
        return finalCommand

    return "\n".join(finalCommand)




        

##########################################################################
############################  BEGIN Function  ############################
##########################################################################


######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)
    ArgParser.add_argument("-d", "--delete", action="store_true", dest="delete", default=False, help="Delete raw data after backup. [%(default)s]")
    ArgParser.add_argument("-c", "--closeAccess", action="store_true", dest="closeAccess", default=False, help="Only allow owner and group member to access the data. [%(default)s]")

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 3:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (adfDir, backupRootDir, projectDID) = args

    ############################# Main Body #############################
    if os.path.exists(projectDID):
        didDict = {}
        ########### Open and read input file
        try:
            IN = open(projectDID, 'r')
        except Exception, e:
            raise e
        
        for line in IN:
            info = line.split()
            didDict[info[0]] = info[1]
        IN.close
        commandText = generateBackupScript(adfDir, backupRootDir, didDict, None, para.closeAccess, deleteFiles=para.delete)
    else:
        commandText = generateBackupScript(adfDir, backupRootDir, projectDID, None, para.closeAccess, deleteFiles=para.delete)
    print commandText

#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
