#! /usr/bin/python

import os
import sys

def openCGIInterface():
    CGIPath = os.environ.get('CGI_HOME') + "/bin"
    CGIHome = os.environ.get('CGI_HOME')
    
    if os.path.isfile(CGIPath + "/CGIPipeline.so") :
        sys.path.insert(0, CGIPath)
    else: 
        if os.path.isfile(CGIHome + "/CGIPipeline.DLL"):
           sys.path.insert(0, CGIHome)
        else:
           print "It appears that CGI_HOME was not set correctly aborting"
           sys.exit(-4)
    
    print "CGI Executables found in path " + CGIPath
    

openCGIInterface()
from CGIPipeline import *

