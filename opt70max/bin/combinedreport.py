#!/usr/bin/env python
import os,sys
import re
import optparse

import accessionlib
import driverlib

def system(cmd):
    sys.stdout.flush()
    sys.stderr.flush()
    sts = os.system(cmd)
    if sts != 0:
        raise RuntimeError('command execution failed with status: %d' % sts)

MUNGER_NAME='/Proj/Assembly/Shared/realData/Utilities/summarize_one_lane_both_arms-workflowver-newrptfmt-inclfull.csh'

def runReportMunger(locationList, destFile):
    first = True
    for x in locationList:
        if first:
            cmd = '%s %s printHeaders > %s' % (MUNGER_NAME, x, destFile)
            first = False
        else:
            cmd = '%s %s >> %s' % (MUNGER_NAME, x, destFile)
        system(cmd)

REPORT_NAME = '/reports/FullDnbSummaryMatches.csv'
MATE_LEN_END = 2000

def getMateInfo(mapLoc):
    f = open(mapLoc + REPORT_NAME, 'r')
    lines = f.readlines()
    f.close()
    lines = [x.strip() for x in lines]

    i = 0
    for i in range(len(lines)):
        if lines[i] == 'Mate distribution':
            break
    if i >= len(lines):
        raise RuntimeError('could not read mate report for ' + mapLoc)

    #import pdb; pdb.set_trace()

    i += 2
    data = [0] * MATE_LEN_END
    for dl in lines[i+2:]:
        if not dl:
            break
        x, cnt, pct = dl.split(',')
        data[int(x)] = float(pct)
    return data

def runMateMunger(mapObjList, destFile):
    data = [range(MATE_LEN_END)]
    for mo in mapObjList:
        data.append(getMateInfo(mo.location))

    f = open(destFile, 'w')

    print >> f, 'lane,' + ','.join([mo.lane for mo in mapObjList])
    print >> f, 'lib,' + ','.join([mo.library for mo in mapObjList])
    print >> f, 'sample,' + ','.join([mo.sample for mo in mapObjList])
    print >> f, ''

    for ii in range(MATE_LEN_END):
        items = [str(d[ii]) for d in data]
        print >>f, ','.join(items)

    f.close()

def main():
    parser = optparse.OptionParser('usage: %prog [options] MAP1 MAP2 MAP3')
    parser.set_defaults(inputAsm='',  rootDir='/Proj/Pipeline/Production_Data',
                        inputMaps=[])
    parser.add_option('-i', '--input', dest='inputAsm',
                      help='accession ID of the assembly to report on')
    parser.add_option('-r', '--root-dir', dest='rootDir',
                      help='use ROOTDIR as the base directory for the pipeline '
                           'object locations')
    parser.add_option('-o', '--output', dest='outputDir',
                      help='destination directory for the reports')
    (options, args) = parser.parse_args()

    if not os.path.exists(options.outputDir):
        os.mkdir(options.outputDir)

    cgiHome = driverlib.getCgiHome()
    print "loading assembly objects..."
    om = accessionlib.ObjectManager(options.rootDir, cgiHome)

    if options.inputAsm:
        if len(args) != 0:
            raise RuntimeError('either ASM object or MAP objects can be specified, but not both')
        asmobj = om.findObject(options.inputAsm)
        mapList = asmobj.mapList
    else:
        if len(args) == 0:
            raise RuntimeError('either ASM object or MAP objects must be specified')
        mapList = args

    mapObjList = [ om.findObject(x) for x in mapList ]
    locationList = [ om.location for om in mapObjList ]

    runReportMunger(locationList, os.path.join(options.outputDir, 'combined-map-report.csv'))
    runMateMunger(mapObjList, os.path.join(options.outputDir, 'combined-mate-report.csv'))

if __name__ == '__main__':
    import sys, traceback
    try:
        main()
        sys.exit(0)
    except SystemExit:
        pass
    except:
        last_type, last_value, last_traceback = sys.exc_info()
        traceback.print_exception(last_type, last_value, last_traceback)
        sys.exit(1)
