#!/bin/env python
import os,sys
import optparse
from cgatools.util import DelimitedFile
from cgatools.reference import CrrFile

def addNonDiploidDetailsFields(detDF):
    detDF.addField('chr')
    detDF.addField('begin',conv=int)
    detDF.addField('end',conv=int)
    #detDF.addField('relativeCvg',sub={'N':'.'})
    detDF.addField('calledLevel',sub={'N':'.'})
    detDF.addField('levelScore')


def addDiploidDetailsFields(detDF):
    detDF.addField('chr')
    detDF.addField('begin',conv=int)
    detDF.addField('end',conv=int)
    #detDF.addField('avgNormalizedCvg',sub={'N':'.'})
    detDF.addField('gcCorrectedCvg',sub={'N':'.'})
    #detDF.addField('fractionUnique')
    detDF.addField('relativeCvg',sub={'N':'.'})
    detDF.addField('calledPloidy', sub={'N':'.'})
    detDF.addField('calledCNVType',sub={'hypervariable':'.', 'invariant':'.', 'N':'.'} )
    detDF.addField('ploidyScore')
    detDF.addField('CNVTypeScore')

    
def write_single_normal_cnv_vcf_line(out,chr,beginWin,endWin,
                                     gcCorrectedCvg,relCvg,
                                     calledPloidy,calledCNVType,calledLevel,
                                     ploidyScore,cnvTypeScore,levelScore,
                                     GCmean,crrFile):

    refBase = crrFile.getSequence(chr,beginWin,beginWin+1)
    
    if chr[0:3] == 'chr':
        chr = chr[3:]

    if gcCorrectedCvg == '.':
        GC = '.'
    else:
        GC = '%.3f' % (float(gcCorrectedCvg)/GCmean)

    line = '.:%s:%s:%s:%s:%s:%s:%s:%s' % ( GC,relCvg,
                                           calledPloidy, ploidyScore,
                                           calledCNVType, cnvTypeScore,
                                           calledLevel, levelScore)

    out.write('%s\t%d\t.\t%s\t<CGA_CNVWIN>\t.\t.\tNS=1;CGA_WINEND=%d\tGT:CGA_GP:CGA_NP:CGA_CP:CGA_PS:CGA_CT:CGA_TS:CGA_CL:CGA_LS\t%s\n' % 
              (chr,beginWin+1,refBase,endWin,line) )


def computeMeanGcCvg(detailsFile):
    df = DelimitedFile(detailsFile)
    df.addField('gcCorrectedCvg')
    sum = 0.
    count = 0
    for gc in df:
        if gc[0] != 'N':
            sum += float(gc[0])
            count += 1
    return sum/float(count)


def write_cnv_vcf_header(out,sampleId):
    out.write('##fileformat=VCFv4.1\n' +
              '##ALT=<ID=CGA_CNVWIN,Description="Copy number analysis window">\n' +
              '##INFO=<ID=NS,Number=1,Type=Integer,Description="Number of Samples With Data">\n' +
              '##INFO=<ID=CGA_WINEND,Number=1,Type=Integer,Description="End of coverage window">\n' +
              '##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">\n' +
              '##FORMAT=<ID=CGA_GP,Number=1,Type=Float,Description="Depth of coverage for 2k window GC normalized to mean">\n' +
              '##FORMAT=<ID=CGA_NP,Number=1,Type=Float,Description="Coverage for 2k window, GC-corrected and normalized relative to copy-number-corrected multi-sample baseline">\n' +
              '##FORMAT=<ID=CGA_CL,Number=1,Type=Float,Description="Nondiploid-model called level">\n' +
              '##FORMAT=<ID=CGA_LS,Number=1,Type=Integer,Description="Nondiploid-model called level score">\n' +
              '##FORMAT=<ID=CGA_CP,Number=1,Type=Integer,Description="Diploid-model called ploidy">\n' +
              '##FORMAT=<ID=CGA_PS,Number=1,Type=Integer,Description="Diploid-model called ploidy score">\n' +
              '##FORMAT=<ID=CGA_CT,Number=1,Type=String,Description="Diploid-model CNV type">\n' +
              '##FORMAT=<ID=CGA_TS,Number=1,Type=Integer,Description="Diploid-model CNV type score">\n' +
              ('#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t%s\n' % (sampleId) ) )


def makeVCF(sampleId,
            diploidDetails,
            nondiploidDetails,
            outFile,crrFile):

    GCmean = computeMeanGcCvg(diploidDetails)

    dipdetails = DelimitedFile(diploidDetails)
    addDiploidDetailsFields(dipdetails)
    nondipdetails = DelimitedFile(nondiploidDetails)
    addNonDiploidDetailsFields(nondipdetails)

    out = open(outFile,'w')
    write_cnv_vcf_header(out,sampleId)

    chrDip = ''
    begDip = ''
    endDip = ''
    
    for (chrNondip,begNondip,endNondip,calledLevel,levelScore) in nondipdetails:

        while chrDip != chrNondip or endDip < endNondip:

            (chrDip,beginDip,endDip,gcCorrectedCvg,relCvg,calledPloidy,calledCNVType,ploidyScore,cnvTypeScore) = dipdetails.next()

            if chrDip != chrNondip or begDip < begNondip or endDip > endNondip:
                raise Exception('Trouble with file synchronization/iteration')

            write_single_normal_cnv_vcf_line(out,chrDip,beginDip,endDip,
                                             gcCorrectedCvg,relCvg,
                                             calledPloidy,calledCNVType,calledLevel,
                                             ploidyScore,cnvTypeScore,levelScore,
                                             GCmean,crrFile)

        

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(sampleId='',nondiploidDetails='',diploidDetails='',
                        outFile='', crrFile='')
    parser.add_option('--nondiploid-details',dest='nondiploidDetails',
                      help='Diploid-model CNV details file for single sample')
    parser.add_option('--diploid-details',dest='diploidDetails',
                      help='Diploid-model CNV details file for single sample')
    parser.add_option('--sample-id',dest='sampleId',
                      help='Sample ID')
    parser.add_option('--output-file',dest='outFile',
                      help='name of file to contain output')
    parser.add_option('--crr-file',dest='crrFile',
                      help='Reference crr file')
    
    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.diploidDetails:
        parser.error('diploid-details specification required')
    if not options.nondiploidDetails:
        parser.error('nondiploid-details specification required')
    if not options.sampleId:
        parser.error('sample-id specification required')
    if not options.outFile:
        parser.error('output-file specification required')
    if not options.crrFile:
        parser.error('crr-file specification required')

    crrFile = CrrFile(options.crrFile)
    
    makeVCF(options.sampleId,options.diploidDetails,options.nondiploidDetails,
            options.outFile,crrFile)


if __name__ == '__main__':
    main()
