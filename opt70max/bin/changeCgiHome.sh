#!/bin/bash

##### Parameters
cgiHomeDir=$1
##### Description
description="Execute this shell with command '. $0 <path>'
  to change the envirmental varible CGI_HOME, LD_LIBRARY_PATH 
  and PYTHONPATH to <path>."

##### Usage
if [ $# != 1 ]
then
    echo -e "==============================================================\n"
    echo -e "  Version 1.1.0  Vincent-Li  2015.06.03\n"
    echo -e "  $description\n"
    echo -e "  Usage: . $0 <path> >STDOUT"
    echo -e "=============================================================="
    exit
fi

##### Global VARs

##### Functions
### Set CGI_HOME, LD_LIBRARY_PATH and PYTHONPATH to cgiDir, need removepath()
setcgi() {
    cgiDir=$1
    oldCgi=$CGI_HOME
    if [ -d $cgiDir ]
    then
        removepath LD_LIBRARY_PATH $oldCgi
        removepath PYTHONPATH $oldCgi
        removepath PATH $oldCgi
        export CGI_HOME=$cgiDir
        export LD_LIBRARY_PATH=$CGI_HOME/bin:$LD_LIBRARY_PATH
        export PATH=$CGI_HOME/bin:$PATH
        export PYTHONPATH=$CGI_HOME/bin:$PYTHONPATH
        echo "CGI_HOME: $CGI_HOME"
    else
        echo "No such a directory: $cgiDir"
    fi
}

### remove specific dir in envirmental varible
removepath() {
    var=$1
    val=${!1}
    cgi=$2
    new_val=""
    
    if [ -z "$cgi" ] || [ -z "$val" ]
    then
        return
    fi
    for i in `echo $val |sed 's/:/ /g'`
    do
        if [[ ! "$i" =~ "$cgi" ]]
        then
            new_val="$new_val:$i"
        fi
    done
    export $var=$new_val
}

##### Main
if [ "$CGI_HOME" != "$cgiHomeDir" ]
then
    setcgi $cgiHomeDir
fi

################## God's in his heaven, All's right with the world. ##################
