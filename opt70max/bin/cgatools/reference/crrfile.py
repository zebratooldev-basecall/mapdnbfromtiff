#!/usr/bin/env python
#
# Copyright 2010 Complete Genomics, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you
# may not use this file except in compliance with the License. You
# may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied. See the License for the specific language governing
# permissions and limitations under the License.

import bisect
from cgatools.util import error, BinaryDataFile

class CrrFile(object):
    '''Genome reference in CRR format'''

    class _PublicChrInfo(object):
        # Despite the name, users only see this as an anonymous object
        def __init__(self, ci):
            self.name = ci.name
            self.length = ci.length
            self.circular = ci.circular
            self.id = ci.id

    def __init__(self, fn):
        '''Open a CRR reference file'''

        self._file = BinaryDataFile(fn)
        self._version = -1
        self._chr = []
        self._readMetadata()
        self._nameToId = {}
        for i, c in enumerate(self._chr):
            self._nameToId[c.name] = i

    def close(self):
        '''Free the OS resources used by this object.

        It's safe to call this method multiple times.
        '''
        self._file.close()

    def getChromosomeId(self, s):
        '''Return integer chromosome id for a given chromosome name.'''
        try:
            return self._nameToId[s]
        except KeyError:
            error('not a valid chromosome name: %s', s)

    def listChromosomes(self):
        '''Return a list of chromosome information objects.
        The information object has the following attributes:
            - name: str
            - length: int
            - circular: bool
            - id: int
        '''
        return [ CrrFile._PublicChrInfo(x) for x in self._chr ]

    def getSequence(self, chrid, begin, end):
        '''Return the reference sequence string for a given region.

        Parameters:
            - chrid: integer zero-based chromosome number or string name
              of the chromosome.
            - begin: start of the region; zero-based offset of the first
              base to be included into the result string.
            - end: end of the region; zero based offset of the first base
              to be NOT included into the result string.
        '''
        if isinstance(chrid, str):
            chrid = self.getChromosomeId(chrid)
        elif chrid > len(self._chr):
            error('chromosome id out of range: %d', chrid)
        c = self._chr[chrid]
        s = []
        for i in range(begin, end):
            s.append(c.getUnambigousBase(self._file, i))
        return c.applyAmbiguity(s, begin)

    _BASE_ARRAY = ['A', 'C', 'G', 'T']
    _HEADER_FORMAT = '>4sLQQQ'

    class _AmbRegion(object):
        '''Ambiguity region in the reference'''
        def __init__(self, f):
            ''' Reads ambiguity information from the current position in the file'''
            self.code = f.readBytes(1)
            self.offset = f.readZCInt()
            self.length = f.readZCInt()

        def __cmp__(self, x):
            return cmp(self.offset, x)

    class _ChrInfo(object):
        '''Internal information for a single chromosome'''
        def __init__(self, id, f):
            ''' Reads chromosome information from the current position in the file'''
            self.id = id
            self.name = f.readStr()
            self.circular = f.readBool()
            self.file_offset = f.readZCInt()
            self.digest = f.readBytes(16)
            self.length = f.readZCInt()
            self.amb = []
            amb_count = f.readZCInt()
            for _ in range(amb_count):
                self.amb.append(CrrFile._AmbRegion(f))

        def _fixCircularPos(self, pos):
            '''Attempt to normalize the position for circular chromosomes and
               check that the coordinate is valid'''
            if pos < 0 or pos >= self.length:
                if self.circular:
                    if pos < 0:
                        pos += self.length
                    if pos >= self.length:
                        pos -= self.length
                    if 0 <= pos < self.length:
                        return pos
                self._badPos(pos)
            else:
                return pos

        def _badPos(self, pos):
            error('position %d out of range for %s:[0, %d)', pos, self.name, self.length)

        def getUnambigousBase(self, f, pos):
            '''Get the base at given offset, ignoring the ambiguity mask'''
            pos = self._fixCircularPos(pos)
            offset = pos / 4;
            shift = 6 - ( (pos & 0x3) << 1 )
            b = f.getMmapedByte(self.file_offset + offset)
            return CrrFile._BASE_ARRAY[ (b >> shift) & 0x3 ]

        def applyAmbiguity(self, bases, begin):
            '''Given a list of single bases and the offset in the chromosome, replace
               the bases at ambiguous positions with the appropriate ambiguity codes,
               and return the result as a joined string of bases'''
            begin = self._fixCircularPos(begin)
            end = begin + len(bases)

            if end > self.length:   # handle circular contigs
                if not self.circular:
                    self._badPos(end)
                prefix = self.length - begin
                return self.applyAmbiguity(bases[:prefix], begin) + \
                       self.applyAmbiguity(bases[prefix:], 0)

            pos = bisect.bisect_left(self.amb, begin)
            if pos > 0:
                pos -= 1
            while pos < len(self.amb):
                ar = self.amb[pos]
                bases_begin = max(0, ar.offset - begin)
                if bases_begin >= len(bases):
                    break
                bases_end = min(ar.offset + ar.length - begin, len(bases))
                for x in range(bases_begin, bases_end):
                    bases[x] = ar.code
                pos += 1
            return ''.join(bases)

    def _readMetadata(self):
        '''Read header and chromosome table from the file'''
        self._file.seek(0)
        magic, self._version, _, _, chr_table_offset = \
            self._file.readStruct(CrrFile._HEADER_FORMAT)
        if magic != 'CRR\n':
            error('failed to open reference %s: not a CRR file', self._filename)
        if self._version != 1:
            error('failed to open reference %s: unknown version %d',
                   self._filename, self._version)
        self._file.seek(chr_table_offset)
        self._readChrTable()

    def _readChrTable(self):
        '''Read chromosome table from the file'''
        chr_count = self._file.readZCInt()
        for cc in range(chr_count):
            self._chr.append(CrrFile._ChrInfo(cc, self._file))

class ChromosomeIdConverter(object):
    '''Field converter suitable for use with DelimitedFile that converts
    string chromosome id to the integer id using the specified CrrFile
    object.

    See L{CrrFile.getChromosomeId}
    '''

    def __init__(self, crr):
        '''Create converter that will use a given CrrFile.'''
        self._crr = crr

    def __call__(self, strid):
        '''Make this object callable as a function that takes string
        chromosome name and returns integer chromosome id.'''
        return self._crr.getChromosomeId(strid)

