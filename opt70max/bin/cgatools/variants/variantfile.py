#!/usr/bin/env python
#
# Copyright 2010 Complete Genomics, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you
# may not use this file except in compliance with the License. You
# may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied. See the License for the specific language governing
# permissions and limitations under the License.

from cgatools.util import allIdentical, DelimitedFile

class Call(object):
    '''Corresponds to a single line of a variant file.'''

    UNKNOWN_PLOIDY = 0  #: Special value for calls with unknown ploidy
    ALL_HAPLOTYPES = 0  #: Special value for calls that span all haplotypes

    #: Set of different no-call subtypes
    NO_CALL_VAR_TYPES = frozenset(['no-call', 'no-call-rc', 'no-call-ri', 'no-ref'])

    def __init__(self):
        '''Internal use only.'''
        self.locus = 0 #: integer locus id
        self.ploidy = 0 #: integer ploidy value or Call.UNKNOWN_PLOIDY if reference sequence is not known for the locus
        self.allele = 0 #: allele this call belongs to, one-based
        self.chromosome = '' #: chromosome name
        self.begin = 0 #: start of the span of the call on the reference
        self.end = 0 #: end of the span of the call on the reference
        self.varType = '' #: variant type string, e.g. 'snp' or 'no-call'
        self.reference = '' #: reference sequence as string, or '=' for pure-ref calls
        self.alleleSeq = '' #: allele sequence as string, or '=' for pure-ref calls
        self.totalScore = 0 #: integer score for the call; zero when score is undefined
        self.hapLink = 0 #: haplotype linkage field
        self.xRef = '' #: cross-reference field.

    def getAlleleSequence(self, crr_file):
        '''Returns true called sequence, retrieving it from the reference
        file if necessary'''
        if self.alleleSeq == '=':
            return crr_file.getSequence(self.chromosome, self.begin, self.end)
        else:
            return self.alleleSeq

    def _similar(self, a):
        return (self.chromosome == a.chromosome and
                self.begin == a.begin and
                self.end == a.end and
                self.varType == a.varType and
                self.alleleSeq == a.alleleSeq)


class Allele(object):
    '''Corresponds to a single allele within a locus in a variant file.

    Public fields:
        - calls: list of all calls of this allele (see L{Call})
    '''

    def __init__(self):
        '''Internal use only'''
        self.calls = []

    def hasNoCalls(self):
        '''Return true if the allele contains any no-calls.'''
        for c in self.calls:
            if c.varType in Call.NO_CALL_VAR_TYPES:
                return True
        return False

    def getSequence(self, crr_file=None):
        '''Returns the called sequence of the allele.

        If CRR file is given, the reference sequence for reference calls is
        extracted from the reference file. Otherwise, the sequence in the
        variation file is returned as is, and the reference sequence may be
        returned as '='.
        '''
        if crr_file is None:
            return self._getPseudoSequence()
        else:
            return ''.join([c.getAlleleSequence(crr_file) for c in self.calls])

    def _getPseudoSequence(self):
        return ''.join([c.alleleSeq for c in self.calls])

class Locus(object):
    '''Corresponds to a single locus in a variant file.'''
    def __init__(self):
        '''Internal use only'''
        self.id = -1 #: integer id of the locus
        self.calls = [] #: list of all calls of this locus (see L{Call})
        self.alleles = [] #: list of all alleles (see L{Allele})
        self.chromosome = None #: chromosome name
        self.begin = 1 #: start of the span of this locus
        self.end = 0 #: end of the span of this locus

    def getPloidy(self):
        '''Returns the ploidy of this locus.

        See L{Call.ploidy}'''
        return self.calls[0].ploidy

    def getPloidyStr(self):
        '''Returns string representation of the ploidy in the variant file
        format.'''
        p = self.getPloidy()
        return p and str(p) or '?'

    def hasNoCalls(self):
        '''Return true if the locus has any no-called sequence'''
        for c in self.calls:
            if c.varType in Call.NO_CALL_VAR_TYPES:
                return True
        return False

    def isNoCallLocus(self):
        '''Return true if the locus is entirely no-called'''
        return len(self.calls) == 1 and self.calls[0].varType in Call.NO_CALL_VAR_TYPES

    def isRefCallLocus(self):
        '''Return true if the locus is entirely called reference'''
        return len(self.calls) == 1 and self.calls[0].varType == 'ref'

    def getZygosityClassification(self):
        '''Return the zygosity of the locus.

        The returned zygosity string can be one of:
            - 'no-call': if any no-calls are present
            - 'ref': if the locus is a pure reference call
            - 'hap': if the locus is haploid
            - 'het': if the locus is fully called and the alleles differ
            - 'hom': if the locus is fully called and the alleles are the same
        '''
        if self.hasNoCalls():
            return 'no-call'
        if self.isRefCallLocus():
            return 'ref'
        if len(self.alleles) < 2:
            return 'hap'
        seq = [a._getPseudoSequence() for a in self.alleles]
        if allIdentical(seq):
            return 'hom'
        else:
            return 'het'

    def getClassification(self):
        '''Return locus classification similar to the one used by calldiff tool'''

        if self.isNoCallLocus():
            return 'no-call'
        fully_called_alleles = [a for a in self.alleles if not a.hasNoCalls()]
        if not fully_called_alleles:
            return 'no-call'
        if len(fully_called_alleles) != len(self.alleles):
            het_prefix = 'no-call-'
        elif len(self.alleles) == 1:
            het_prefix = 'hap-'
        else:
            seq = [a._getPseudoSequence() for a in fully_called_alleles]
            if allIdentical(seq):
                het_prefix = 'hom-'
            else:
                het_prefix = 'het-'
        call = None
        for a in fully_called_alleles:
            calls = [c for c in a.calls if c.varType != 'ref']
            if not calls:
                continue
            if len(calls) > 1:
                return het_prefix + 'other'
            if call is None:
                call = calls[0]
            elif not call._similar(calls[0]):
                return het_prefix + 'other'
        if call is None:
            return het_prefix + 'ref'
        else:
            return het_prefix + call.varType

    def __str__(self):
        '''Returns string representation of the locus for debugging.'''
        seq = [a._getPseudoSequence() for a in self.alleles]
        return '%s:%d-%d, alleles: [%s]' % (
            self.chromosome, self.begin, self.end, ', '.join(seq) )

    def _initFromCalls(self):
        alleleCount = self.calls[0].ploidy
        if alleleCount == Call.UNKNOWN_PLOIDY:
            self.chromosome = self.calls[0].chromosome
            self.begin = self.calls[0].begin
            self.end = self.calls[-1].end
            return
        else:
            for allele in range(alleleCount):
                a = Allele()
                for c in self.calls:
                    if c.allele == (allele+1) or c.allele == Call.ALL_HAPLOTYPES:
                        a.calls.append(c)
                self.alleles.append(a)
        a = self.alleles[0]
        self.chromosome = a.calls[0].chromosome
        self.begin = a.calls[0].begin
        self.end = a.calls[-1].end


class VariantFile(object):
    '''Parse Complete Genomics variant file.

    Iterable object that supports locus-by-locus traversal of the variant
    file.

    For example::
        f = VariantFile(fileName)
        for loc in f:
            if loc.getClassification() == 'het-snp':
                print loc.chromosome, loc.begin, loc.end
        f.close()
    '''

    def __init__(self, filename=None, fileobj=None, delimiter='\t'):
        '''Open the file and prepare to iterate through the loci.

        The file can be specified by name, or an opened file-like object
        can be passed, in which case the file name, if specified, will be
        used only in error messages.

        File object will be closed when the VariantFile is closed.
        '''

        self._f = None
        self._f = DelimitedFile(filename, fileobj, delimiter)
        self._addFieldParsers()
        self._readCall()

    def close(self):
        '''Free the OS resources used by this object.

        It's safe to call this method multiple times.
        '''
        if hasattr(self, '_f') and self._f is not None:
            self._f.close()
            self._f = None

    def getMetadata(self):
        '''Return the headers of the file as DelimitedFileMetadata dictionary.

        See L{cgatools.util.DelimitedFile.getMetadata}
        '''
        return self._f.getMetadata()

    def __del__(self):
        '''Call L{close}.'''
        self.close()

    def _readCall(self):
        c = Call()
        try:
            c.locus, c.ploidy, c.allele, c.chromosome, c.begin, c.end, c.varType, \
            c.reference, c.alleleSeq, c.totalScore, c.hapLink, c.xRef = self._f.next()
            if c.varType == 'ref-consistent':
                if c.alleleSeq == '?':
                    c.varType = 'no-call'
                else:
                    c.varType = 'no-call-rc'
            self._nextCall = c
        except StopIteration:
            self._nextCall = None

    def __iter__(self):
        '''Allow locus-by-locus iteration through variant files.'''
        return self

    def next(self):
        '''Return the next locus in the file.'''
        if self._nextCall is None:
            raise StopIteration
        loc = Locus()
        loc.calls.append(self._nextCall)
        loc.id = self._nextCall.locus
        while True:
            self._readCall()
            if self._nextCall is None:
                break
            if self._nextCall.locus != loc.id:
                break
            loc.calls.append(self._nextCall)
        loc._initFromCalls()
        return loc

    def _addFieldParsers(self):
        f = self._f
        f.addField('locus', conv=int)
        f.addField('ploidy', conv=int, sub={'?':Call.UNKNOWN_PLOIDY})
        if f.hasField('allele'):
            f.addField('allele', conv=int, sub={'all':Call.ALL_HAPLOTYPES})
        else:
            f.addField('haplotype', conv=int, sub={'all':Call.ALL_HAPLOTYPES})
        f.addField('chromosome')
        f.addField('begin', conv=int)
        f.addField('end', conv=int)
        f.addField('varType', sub={'=':'ref', 'delins':'sub', 'ref-inconsistent':'no-call-ri'})
        f.addField('reference')
        f.addField('alleleSeq')
        f.addField('totalScore', conv=int, sub={'':0})
        f.addField('hapLink', optional=True, sub={None:''})
        f.addField('xRef', optional=True, sub={None:''})

