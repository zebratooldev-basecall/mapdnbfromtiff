import unittest
from cStringIO import StringIO
from cgatools.variants import *

DATA1 = \
'''
#ASSEMBLY_ID\tsim110
#COSMIC\tCOSMIC v48
#DBSNP_BUILD\tTEST-SMOKETEST1_03-REF
#FORMAT_VERSION\t1.5
#GENERATED_AT\t2010-Oct-19 09:59:44.593716
#GENERATED_BY\tdbsnptool
#GENOME_REFERENCE\tTEST-SMOKETEST1_03-REF
#SAMPLE\tSimImager-Sample
#SOFTWARE_VERSION\t$Change: 33924 $ Oct 19 2010 09:47:09
#TYPE\tVAR-ANNOTATION

>locus\tploidy\tallele\tchromosome\tbegin\tend\tvarType\treference\talleleSeq\ttotalScore\thapLink\txRef
1\t2\t1\tOneMBpTestWithSegDups\t0\t2\tno-call\tTT\t?\t\t\t
1\t2\t1\tOneMBpTestWithSegDups\t2\t9\tref\tTAACAGA\tTAACAGA\t28\t\t
1\t2\t1\tOneMBpTestWithSegDups\t9\t10\tno-call-rc\tA\tN\t98\t\t
1\t2\t1\tOneMBpTestWithSegDups\t10\t23\tref\tAAATGAGCATGGA\tAAATGAGCATGGA\t27\t\t
1\t2\t2\tOneMBpTestWithSegDups\t0\t23\tno-call\tTTTAACAGAAAAATGAGCATGGA\t?\t\t\t
2\t2\tall\tOneMBpTestWithSegDups\t23\t248\tref\t=\t=\t\t\t
3\t?\tall\tOneMBpTestWithSegDups\t248\t249\tno-ref\t=\t?\t\t\t
'''

DATA2 = \
'''
>locus,ploidy,allele,chromosome,begin,end,varType,reference,alleleSeq,totalScore,hapLink,xRef
1,?,all,chr1,0,100,no-ref,=,?,,,
2,1,1,chr1,100,101,snp,A,T,123,,
3,2,all,chr1,101,200,ref,=,=,,,
4,2,1,chr1,200,201,ref,C,C,191,,
4,2,2,chr1,200,201,no-call-rc,C,N,191,,
5,2,all,chr1,201,300,ref,=,=,,,
6,2,1,chr1,300,301,del,G,,156,,dbsnp.129:rs60652689
6,2,2,chr1,300,301,del,G,,156,,dbsnp.129:rs60652689
7,2,all,chr1,301,400,ref,=,=,,,
8,2,1,chr1,400,400,ins,,G,156,,
8,2,2,chr1,400,400,ref,,,156,,
9,2,1,chr1,400,401,snp,A,G,156,,
9,2,2,chr1,400,401,snp,A,T,156,,
'''

class TestVariant(unittest.TestCase):
    def testBasic(self):
        f = VariantFile('test-data', fileobj=StringIO(DATA1))
        mtd = f.getMetadata()
        loci = [ x for x in f ]
        f.close()
        self.assertEqual(mtd.getFormatVersion(), (1, 5))
        self.assertEqual(mtd['TYPE'], 'VAR-ANNOTATION')
        self.assertEqual(len(loci), 3)
        self.assertEqual([int(x.id) for x in loci], [1, 2, 3])
        self.assertEqual([x.getPloidy() for x in loci], [2, 2, Call.UNKNOWN_PLOIDY])
        self.assertEqual([x.hasNoCalls() for x in loci], [True, False, True])
        self.assertEqual([x.isNoCallLocus() for x in loci], [False, False, True])
        self.assertEqual([x.isRefCallLocus() for x in loci], [False, True, False])
        self.assertEqual([x.getZygosityClassification() for x in loci], ['no-call', 'ref', 'no-call'])
        self.assertEqual([x.getClassification() for x in loci], ['no-call', 'hom-ref', 'no-call'])
    
    def testClassification(self):
        f = VariantFile('test-data', fileobj=StringIO(DATA2), delimiter=',')
        loci = [x for x in f]
        f.close()
        self.assertEqual([x.getZygosityClassification() for x in loci], 
                         ['no-call', 'hap', 'ref', 'no-call', 'ref', 'hom', 'ref', 
                          'het', 'het'])
        self.assertEqual([x.getClassification() for x in loci], 
                         ['no-call', 'hap-snp', 'hom-ref', 'no-call-ref', 'hom-ref', 
                          'hom-del', 'hom-ref', 'het-ins', 'het-other'])
    
    def testSequence(self):
        f = VariantFile('test-data', fileobj=StringIO(DATA2), delimiter=',')
        loci = [x for x in f if x.getPloidy() != Call.UNKNOWN_PLOIDY]
        f.close()
        self.assertEqual([x.alleles[0].getSequence() for x in loci], 
                         ['T', '=', 'C', '=', '', '=', 'G', 'G'])
        self.assertEqual([x.alleles[1].getSequence() for x in loci if x.getPloidy() == 2],
                              ['=', 'N', '=', '', '=', '',  'T']) 
        
        
        
        
        