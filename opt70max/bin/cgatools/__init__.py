#!/usr/bin/env python
#
# Copyright 2010 Complete Genomics, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you
# may not use this file except in compliance with the License. You
# may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied. See the License for the specific language governing
# permissions and limitations under the License.

'''Python APIs to access various Complete Genomics file formats.

B{BETA RELEASE}

Currently we provide APIs that allow to:
    - access CRR reference file (L{cgatools.reference.CrrFile})
    - parse tab-separated text format used by various Complete Genomics
      data files (L{cgatools.util.DelimitedFile})
    - parse variant files (L{cgatools.variant.VariantFile})

The code requires Python 2.4.3-2.7.x (Python 3 is currently not supported).

The APIs can be used by adding the python/ subdirectory of the cgatools
installation to the PYTHONPATH environment variable. Example Python scripts
are installed to that directory as well, and can be run directly from there
without modifying the environment.
'''