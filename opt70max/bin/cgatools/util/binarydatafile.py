#!/usr/bin/env python
#
# Copyright 2010 Complete Genomics, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you
# may not use this file except in compliance with the License. You
# may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied. See the License for the specific language governing
# permissions and limitations under the License.

import os, struct, mmap
from misc import error

class BinaryDataFile(object):
    '''Helper object with methods to access CGA binary data file.

    Supports both file-like and memory-mapped access. Provides methods to
    read certain data types in the encoding used by cgatool C++ code.

    All readXXX methods read data from the current position in the file
    and move the current file pointer. The L{getMmapedByte} method reads
    a single byte using memory mapping, and doesn't affect the current file
    pointer.
    '''

    def __init__(self, fn):
        '''Open the data file with a given name.'''
        self._filename = fn
        self._handle = -1
        self._memory = None
        self._handle = os.open(fn, os.O_RDONLY + getattr(os, 'O_BINARY', 0))
        file_size = os.fstat(self._handle).st_size
        self._memory = mmap.mmap(self._handle, file_size, access=mmap.ACCESS_READ)

    def close(self):
        '''Free the OS resources used by this object.

        It's safe to call this method multiple times.
        '''
        if hasattr(self, '_memory') and self._memory is not None:
            self._memory.close()
            self._memory = None
        if self._handle >= 0:
            os.close(self._handle)
            self._handle = -1

    def __del__(self):
        '''Call L{close}.'''
        self.close()

    def name(self):
        '''Return name of the file.'''
        return self._filename

    def handle(self):
        '''Return OS handle of the file.'''
        return self._handle

    def seek(self, offset):
        '''Move current file pointer to the specified offset relative to
        the start of the file.'''
        os.lseek(self._handle, offset, 0)

    def readZCInt(self):
        '''Read zero-compressed integer.'''
        val = 0
        while True:
            temp = self.readBytes(1)
            b = ord(temp[0])
            val = val | (b & 0x7f)
            if b & 0x80 == 0:
                return val
            val = val << 7

    def readStr(self):
        '''Read string.'''
        length = self.readZCInt()
        return self.readBytes(length)

    def readBool(self):
        '''Read boolean value.'''
        s = self.readBytes(1)
        if ord(s) == 0:
            return False
        else:
            return True

    def readBytes(self, length):
        '''Read sequence of bytes.'''
        s = os.read(self._handle, length)
        if len(s) != length:
            error('failed to read reference %s: unexpected end of file', self._filename)
        return s

    def readStruct(self, format):
        '''Read data according to format string (see module struct)'''
        size = struct.calcsize(format)
        s = self.readBytes(size)
        return struct.unpack(format, s)

    def getMmapedByte(self, ofs):
        '''Read single byte using memory mapping.'''
        return ord(self._memory[ofs])

