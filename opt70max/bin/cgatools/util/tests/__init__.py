import unittest
from cStringIO import StringIO
from cgatools.util import *

class TestMisc(unittest.TestCase):
    def testAllIdentical(self):
        self.assertTrue(allIdentical([]))
        self.assertTrue(allIdentical([False]))
        self.assertTrue(allIdentical([1, 1, 1, 1]))
        self.assertFalse(allIdentical([1, 2]))
        self.assertFalse(allIdentical([1, "1"]))

class TestDelimitedFile(unittest.TestCase):
    def testBasic(self):
        text = '#FOO,BAR\n>x,Y\n 10,20\n30 , 40'
        f = DelimitedFile(fileobj=StringIO(text), delimiter=',')
        self.assertEqual(f.getMetadata()['FOO'], 'BAR')
        self.assertEqual(len(f.getMetadata().keys()), 1)
        f.addField('x', conv=int)
        f.addField('y')
        self.assertRaises(Exception, f.addField, 'z')
        f.addField('z', optional=True)
        count = 0
        for x, y, z in f:
            if count == 0:
                self.assertEqual(x, 10)
                self.assertEqual(y, '20')
            else:
                self.assertEqual(x, 30)
                self.assertEqual(y, '40')
            self.assertTrue(z is None)
            count += 1
        self.assertEqual(count, 2)
        f.close()
    
    def testVersion(self):
        text = '#FORMAT_VERSION\t1.5\n>x\ty\n10\t20\n'
        f = DelimitedFile(fileobj=StringIO(text))
        self.assertEqual( f.getMetadata().getFormatVersion(), (1, 5) )
        f.close()
        
    def testConv(self):
        text = '>x,y,z,w\n1,2,3,4'
        f = DelimitedFile(fileobj=StringIO(text), delimiter=',')
        f.addField('x')
        f.addField('y', conv=int)
        f.addField('z', conv=float)
        f.addField('w', conv=lambda x: int(x) + 1)
        t = f.next()
        self.assertEqual(t, ['1', 2, 3, 5])
        self.assertTrue(isinstance(t[2], float))
        self.assertRaises(StopIteration, f.next)
        f.close()
        
    def testSubst(self):
        text = '>X,y,z\n?,\n'
        f = DelimitedFile(fileobj=StringIO(text), delimiter=',')
        f.addField('x', sub={'?':10})
        f.addField('Y', sub={'':20})
        f.addField('z', optional=True, sub={None:30})
        t = f.next()
        self.assertEqual(t, [10, 20, 30])
        self.assertRaises(StopIteration, f.next)        
        f.close()        
            
            
        

