#!/usr/bin/env python
#
# Copyright 2010 Complete Genomics, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you
# may not use this file except in compliance with the License. You
# may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied. See the License for the specific language governing
# permissions and limitations under the License.
import os, sys, traceback

def error(fmt, *args):
    '''Format and raise a RuntimeError'''
    raise RuntimeError(fmt % args)

def allIdentical(lst):
    '''Return true if all items of a given list are identical'''
    return len(lst) < 2 or lst.count(lst[0]) == len(lst)

def openFileByExtension(fn):
    '''Open a text file that may be compressed.

    Returns the file-like object of appropriate type depending on the file
    extension (.bz2 and .gz are supported).'''

    _, ext = os.path.splitext(fn)
    if ext == '.bz2':
        import bz2
        return bz2.BZ2File(fn, 'r', 2*1024*1024)
    elif ext == '.gz':
        import gzip
        return gzip.GzipFile(fn, 'r')
    else:
        return open(fn, 'r')

def mainFunctionWrapper(function):
    '''Call a given function and suppress annoying "broken pipe" messages.

    This is useful for simple command-line utilities that are often invoked
    with stdout piped to another program. Full python traceback is replaced
    with a simple single-line error message, and the error messages are
    suppressed entirely for Ctrl-C and the cases when the next program in
    the pipeline exits early (e.g. "head -100").
    '''
    def err(e):
        if sys.stderr:
            prog = os.path.basename(sys.argv[0])
            print >>sys.stderr, '%s: %s: %s' % (prog, e.__class__.__name__, e)

    def silentClose(f):
        try: f.close()
        except: pass

    def closestd():
        silentClose(sys.stdin)
        silentClose(sys.stdout)
        silentClose(sys.stderr)

    try:
        function()
    except SystemExit:
        raise
    except KeyboardInterrupt:
        closestd()
        raise SystemExit(1)
    except IOError, e:
        if e.filename is None and e.errno == 32:
            closestd()
            raise SystemExit(1)
        else:
            err(e)
            closestd()
            raise SystemExit(1)
    except Exception, e:
        err(e)
        closestd()
        raise SystemExit(1)

