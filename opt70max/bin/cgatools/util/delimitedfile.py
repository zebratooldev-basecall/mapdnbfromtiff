#!/usr/bin/env python
#
# Copyright 2010 Complete Genomics, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you
# may not use this file except in compliance with the License. You
# may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied. See the License for the specific language governing
# permissions and limitations under the License.

from misc import error, openFileByExtension
from cgiutils import bundle


class DelimitedFileMetadata(dict):
    '''Stores header values of a CG export file as a dictionary.
    @undocumented: __init__
    '''

    def __init__(self, fn):
        self._filename = fn

    def getFormatVersion(self):
        '''Return CGI format version for this metadata set, as a (major, minor) tuple'''

        try:
            v = self['FORMAT_VERSION']
        except KeyError:
            try:
                v = self['VERSION']
            except KeyError:
                error('file %s has no version header', self._filename)
        try:
            major, minor = [ int(x) for x in v.split('.') ]
        except:
            error('file %s contains bad version header value: %s', self._filename, v)
        if minor >= 1000 or minor < 0 or major < 0:
            error('file %s contains bad version header value: %s', self._filename, v)
        return (major, minor)

class DelimitedFile(object):
    '''Parse files delimited in the way found in a typical CG export file.

    Example::
        f = DelimitedFile('test.txt')
        f.addField('id', conv=int)
        f.addField('chromosome', sub={'':'unknown'})

        for id, chrom in f:
            print '%d, %s' % (id, chrom)
    '''

    def __init__(self, filename=None, fileobj=None, delimiter='\t'):
        '''Open the file and prepare to iterate through the records.

        The file can be specified by name, or an opened file-like object
        can be passed, in which case the file name, if specified, will be
        used only in error messages.

        File object will be closed when the DelimitedFile is closed.
        '''
        self._delimiter = delimiter
        self._fileOpen = False
        if fileobj is None:
            self._filename = filename
            self._f = openFileByExtension(self._filename)
        elif fileobj is not None:
            self._filename = (filename is None) and 'unknown' or filename
            self._f = fileobj
        else:
            error('either file name or a file-like object must be given')
        self._fileOpen = True

        self._fields = []
        self._metadata = DelimitedFileMetadata(self._filename)
        self._line = ''
        self._lineno = 0
        self._readHeader()

    def close(self):
        '''Free the OS resources used by this object.

        It's safe to call this method multiple times.
        '''
        if hasattr(self, '_fileOpen') and self._fileOpen:
            self._fileOpen = False
            if hasattr(self._f, 'close'):
                self._f.close()

    def __del__(self):
        '''Call L{close}.'''
        self.close()

    def getMetadata(self):
        '''Return the headers of the file as DelimitedFileMetadata
        dictionary.'''
        return self._metadata

    def addField(self, name, optional=False, sub={}, conv=None):
        '''Register a field parser.

        Parameters:
            - name: header string for this field; case insensitive
            - optional: True if it's ok for the field to not be present in
              the file or in one of the data lines
            - sub: dictionary that maps special values in the file to the
              values to be returned by the parser; to provide a default for
              an optional field, use sub={None:value}
            - conv: converter to apply to the string found in the file
              before returning it, e.g. conv=int; this can be any callable
              object
        '''
        try:
            pos = self._headersLower.index(name.lower())
        except:
            if not optional:
                error('field "%s" not found in file %s', name, self._filename)
            else:
                pos = -1
        field = DelimitedFile._Field()
        field.name = name
        field.optional = optional
        field.exceptions = sub
        field.converter = conv
        field.position = pos
        self._fields.append(field)

    def hasField(self, name):
        '''Return true if the file has a specified field.

        Header names are not case sensitive.
        '''
        return name.lower() in self._headersLower

    def __iter__(self):
        '''Allow line-by-line iteration through delimited files.'''
        return self

    def next(self):
        '''If at least one explicit field has been registered using addField(),
        return the next line as a list of registered field values.
        Otherwise, return a dict where the keys are the field names as
        read from the file header, and the values are corresponding values
        from the current line.'''
        line = self._f.readline()
        #if not line:
        if not line or line == '\x00':
            raise StopIteration()

        self._line = line
        self._lineno += 1

        data = [ x.strip() for x in line.split(self._delimiter) ]

        result = None
        if self._fields:
            result = []
            for f in self._fields:
                if f.position < 0 or f.position >= len(data):
                    if f.optional:
                        v = None
                    else:
                        error('field "%s" in not found in file %s (line %d)',
                            f.name, self._filename, self._lineno)
                else:
                    v = data[f.position]
                if v in f.exceptions:
                    result.append(f.exceptions[v])
                    continue
                if f.converter:
                    try:
                        result.append(f.converter(v))
                    except Exception, e:
                        error('failed to convert field "%s" in file %s (line %d): %s',
                            f.name, self._filename, self._lineno, e)
                else:
                    result.append(v)
        else:
            if len(data) != len(self._headers):
                error('line %d contains incorrect number of values: %s\nexpected one value per header: %s' %
                    (self._lineno, data, self._headers))
            result = dict(zip(self._headers, data))

        return result

    def getCurLine(self):
        return self._line

    def getCurLineNo(self):
        return self._lineno

    class _Field(object):
        pass

    def _readHeader(self):
        while True:
            line = self._f.readline()
            self._lineno += 1
            if not line:
                error('header line not found in file %s (currently at line %d)',
                      self._filename, self._lineno)
            if line[0] == '>':
                break
            elif line[0] == '#':
                if self._delimiter in line:
                    try:
                        k, v  = [ x.strip() for x in line[1:].split(self._delimiter) ]
                    except Exception, e:
                        error('failed to parse header "%s" in file %s (line %d): %s',
                              line, self._filename, self._lineno, e)

                else:
                    k = line[1:].strip()
                    v = ''
                self._metadata[k] = v
        self._headers = [ x.strip() for x in line[1:].split(self._delimiter) ]
        self._headersLower = [ x.lower() for x in self._headers ]
