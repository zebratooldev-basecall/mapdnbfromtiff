#!/usr/bin/python
import sys, os, re, string
from threading import Thread
import subprocess
from optparse import OptionParser

##################### Gobal settings #####################
threads = []

def int2bin(n, count=10):
    return "".join([str((n >> y) & 1) for y in range(count-1, -1, -1)])

def base_translate(string_):
    return string_.translate(string.maketrans("ACGT","TGCA"))

def get_cycle_id(path):
    return os.path.basename(path).split('.')[1]

##################### objects #####################
class file_header:
    def __init__(self,fn,dump_sam,simple_text,has_extra_cycles=False):
        self.dump_sam = None
        if dump_sam:
            self.dump_sam = open(dump_sam,'w')
        self.simple_text = None 
        if simple_text:
            self.simple_text = open(simple_text,'w')


        if fn == '-':
            self.ff = sys.stdin
        else:
            self.ff = open(fn,'r')
        while True:
            line = self.ff.readline()
            if self.dump_sam: self.dump_sam.write(line)

            if line.startswith('@'): 
                continue
            else:
                self.lastLine = line
                index = line.split('\t')[0]
                self.lastIndex = index
                break

    def next(self):
        temp = []
        if self.lastLine: temp.append(self.lastLine)

        while True:
            line = self.ff.readline()

            if line == None or line == "": 
                self.lastLine = None
                break
            if self.dump_sam: self.dump_sam.write(line)

            index = line.split('\t')[0]

            if index == self.lastIndex:
                temp.append(line)
            else:
                self.lastLine = line
                self.lastIndex = index
                break
        
        if temp:
            obj = sam_object(temp)
            if self.simple_text: self.simple_text.write(obj.get_simple_text()+'\n')
            return obj
        else:
            return None

    def close(self):
        self.ff.close()
        if self.dump_sam: self.dump_sam.close()
        if self.simple_text: self.simple_text.close() 

class CIGAR:
    def __init__(self,string):
        self.string = string
    def mismatches(self):
        list_ = []
        seg = [a for a in re.split(r'([A-Z][a-z]*)', self.string) if a] 
        counter = 0
        for item in seg:
            len_ = 1 
            try:
                len_ = int(item)
                for i in range(len_): list_.append('.')
            except:
                list_.append(item)
            counter += len_
        return list_

class alignment:
    def __init__(self,line):
        line = line.split('\t')
        self.index = int(line[0].split('-').pop())
        self.extrabases = "" 
        if len(line[0].split('-')) > 2:
            self.extrabases =  ",".join([ i for i in line[0].split('-')[1] ])
        self.flag = int2bin(int(line[1]))
        self.isMapped = bool( 1 - int(self.flag[-3]))
        self.isReversed = bool( int(self.flag[-5]))
        self.seq = line[9]
        self.isUnique = self.isMapped
        self.addiction_info = {}
        self.chr = line[2].replace('chr','')
        self.pos = line[3]

        ############# obtain the addiction information #############
        for seg in line[11:]:
            seg = seg.split(':')
            key = ":".join(seg[0:2])
            value = seg[-1]
            self.addiction_info[key] = value
        
        ############# Catch the mismatch information #############
        if 'MD:Z' in self.addiction_info.keys():
            self.mismatches = CIGAR(self.addiction_info['MD:Z']).mismatches()
        else:
            if self.isMapped:
                self.mismatches = ['.'] * len(self.seq)
            else:
                self.mismatches = ['*'] * len(self.seq)
        
        ############# Set align score and uniqueness #############
        self.align_score = 0
        if 'AS:i' in self.addiction_info.keys(): self.align_score = int(self.addiction_info['AS:i'])
        if 'XS:i' in self.addiction_info.keys(): self.isUnique = False
        #print self.index, self.isMapped

    def print_mismatches(self):
        return " ".join([str(self.isUnique), str(self.isReversed), self.index, self.seq] + [str(x) for x in self.mismatches])+"\n"

    def rev_comp(self):
        self.mismatches = [ base_translate(x) for x in self.mismatches[::-1] ]
        self.seq = base_translate(self.seq[::-1])

    def get_sequence_base(self,pos):
        if pos >= len(self.mismatches):
            print 'the position is out of range %d' % pos
            print 'the mismatches array:', self.mismatches
        base = self.mismatches[pos]
        if base == '.': 
            base = self.seq[pos]
        return base 

    def get_simple_text(self):
        call = [] 
        refbase = [] 
        for i in range(len(self.mismatches)):
            call.append(self.seq[i])
            if self.mismatches[i] == '.':
                refbase.append(self.seq[i])
            else:
                refbase.append(self.mismatches[i])
        return ','.join(call+refbase+[self.extrabases])

class sam_object:
    def __init__(self,big_list):
        self.isUnique = False
        self.isMapped = False
        self.align = None
        self.isReversed = False

        if len(big_list) == 1:
            line =big_list.pop(0).rstrip('\t\n')
            align = alignment(line)
            self.isUnique = align.isMapped
            self.align = align
        else:
            best_as = None
            best_align = None
            for line in big_list:
                align = alignment(line)
                if best_as == None and best_align == None:
                    best_align = align
                    best_as = align.align_score
                elif align.align_score > best_as:
                    best_align = align
                    best_as = align.align_score
            self.align = best_align
        self.isMapped  = self.align.isMapped
        self.isReversed = self.align.isReversed
        self.index = int(self.align.index)

        if self.isReversed:
            self.align.rev_comp()

    def get_sequence_base(self,pos):
        return self.align.get_sequence_base(pos)

    def get_simple_text(self): 
        Index = str(self.align.index)
        RefBase,Strand,Chr,Pos,Uniq = '*','*','*','*','*'

        if self.isMapped:
            if self.isReversed: 
                Strand = '-'
            else: 
                Strand = '+'
            if self.isUnique: 
                Uniq = 'U'
            else:
                Uniq = 'M'
        return ','.join([Index,Strand,Uniq,self.align.get_simple_text()])

##################### Thread Function #####################
def threadFunction(inputSam,readlength,outputSummary):
    script='''process-sam.py -l %d -f %s > %s ''' % (readlength, inputSam, outputSummary)
    subprocess.call([script], shell=True)

##################### Main Function #####################
def parse_arguments(args):
    parser = OptionParser()
    parser.add_option('--sink-paths',help='the path log files to sink files', default=None, dest='sinklog')
    parser.add_option('--sam',help='the path to sam files', default=None, dest='sam')
    parser.add_option('--output-dir',help='the output dir', default='./',dest='output_dir')
    parser.add_option('--dump-sam-file',help='dump the samfile from stdin', default=None,dest='dump_sam')
    parser.add_option('--simple-base-file',help='write an simple map file', default=None,dest='simple_text')

    options, args = parser.parse_args(args)
    return options.sinklog, options.sam, options.output_dir, options.dump_sam, options.simple_text

def main(argv):
    sinklog, sam, output_dir, dump_sam, simple_text = parse_arguments(argv)

    sinklist = {}
    extra_cycles = {}
    for line in open(sinklog).readlines():
        pos, path = line.rstrip('\r\n').split('\t')
        if '*' in pos:
            pos = int(pos.replace('*',''))
            extra_cycles[pos] = path
        else:
            sinklist[int(pos)] = path

    big_dict = {}
    if dump_sam:
        print '#output sam: %s' % dump_sam
        fh = file_header(sam, dump_sam, simple_text, bool(extra_cycles))
    else:
        fh = file_header(sam,False, simple_text, bool(extra_cycles))
    if simple_text:
        if extra_cycles:
            fh.simple_text.write( ','.join(
                ['Index,Strand,Uniq'] + 
                [ "Call_%s" % get_cycle_id(sinklist[pos]) for pos in sorted(sinklist.keys())] + 
                [ "RefBase_%s" % get_cycle_id(sinklist[pos]) for pos in sorted(sinklist.keys())] + 
                [ "EX_%s" % get_cycle_id(extra_cycles[pos]) for pos in sorted(extra_cycles.keys())] 
            ) + '\n')
        else:
            fh.simple_text.write( ','.join(
                ['Index,Strand,Uniq'] + 
                [ "Call_%s" % get_cycle_id(sinklist[pos]) for pos in sorted(sinklist.keys())] + 
                [ "RefBase_%s" % get_cycle_id(sinklist[pos]) for pos in sorted(sinklist.keys())]
            ) + '\n')

    line_count = 0
    while True:
        obj = fh.next()
        if obj is None: break
        big_dict[obj.index] = obj
        line_count += 1
        if line_count % 100000 == 0 :
            print "Read %d alignment from sam file" % line_count
    print 'Finish reading'
    fh.close()

    print 'Start to output'
    max_index = line_count
    print 'Totally we recieved mapping information of %d DNBs' % line_count

    if dump_sam:
        print 'folk one thread to run Shane\'s SAM stats'
        t = Thread(target = threadFunction, args=(dump_sam, len(sinklist), dump_sam+'.summary.csv')) 
        threads.append(t)
        t.start()

    nanocall_fh = {}
    for pos in sorted(sinklist.keys()):
        bn = os.path.basename(sinklist[pos]).replace('.csv','_wRefBase.csv')
        nanocall_fh[pos] = {}
        nanocall_fh[pos]['in'] = open(sinklist[pos],'r')
        nanocall_fh[pos]['out'] = open(os.path.join(output_dir,bn),'w')
        line_counter = 0
        while True:
            line = nanocall_fh[pos]['in'].readline()
            line_counter += 1
            if line_counter <= 35: 
                nanocall_fh[pos]['out'].write(line) 
                continue
            if line_counter == 36:
                nanocall_fh[pos]['out'].write( ','.join([
                    line.rstrip('\r\n'),
                    'RefBase',
                    'Strand',
                    'Uniq'
                    ]) + '\n') 
                break

    big_dict = [ big_dict[x] for x in sorted(big_dict.keys())]

    while len(big_dict) > 0:
        obj = big_dict.pop(0)

        for pos in nanocall_fh.keys():
            Index,Call,Score,TxR,Fit,Cy5,Cy3,T,C,G,A = [None]*11
            while True:
                line = nanocall_fh[pos]['in'].readline()
                try:
                    Index,Call,Score,TxR,Fit,Cy5,Cy3,T,C,G,A = line.rstrip('\r\n').split(',')
                except ValueError:
                    Index,Call,Score,TxR,Fit,Cy5,Cy3 = line.rstrip('\r\n').split(',')
                    T = C = G = A = '0'

                Index = int(Index)
                if Index == obj.index:
                    break
                elif Index < obj: 
                    continue
                else:
                    Index = None

            if Index is None: continue
            RefBase,Strand,Chr,Pos,Uniq = '*','*','*','*','*'
            if Index >= max_index:
                nanocall_fh[pos]['in'].close()
                nanocall_fh[pos]['out'].close()
                break

            if obj.isMapped:
                RefBase = obj.get_sequence_base(pos)
                Strand = '+'
                if obj.isReversed: Strand = '-'
                Chr = obj.align.chr
                Pos = obj.align.pos 
                Uniq = {True: 'U', False: 'M'} [obj.isUnique]

            nanocall_fh[pos]['out'].write( ','.join([
                str(Index),Call,Score,TxR,Fit,Cy5,Cy3,T,C,G,A,
                RefBase,Strand,Uniq
                ]) + '\n')

    for t in threads: 
        t.join()

main(sys.argv)

