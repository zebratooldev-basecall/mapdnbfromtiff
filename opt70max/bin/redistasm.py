#! /usr/bin/env python

import os
import datetime

SG_QUEUE_COUNT = 3
BIG_JOB_SLOT_COUNT = 100

class SlotCount:
    def __init__(self):
        self.free = 0
        self.used = 0

    def __repr__(self):
        return "(free=%d,used=%d)" % (self.free,self.used)

class WaitingJob:
    def __init__(self,id,slots):
        self.id = id
        self.slots = slots

    def __repr__(self):
        return "(id=%d,slots=%d)" % (self.id,self.slots)

print "Starting at", datetime.datetime.now()

qSlotCounts = {}
waitingBigJobs = []

ii = SG_QUEUE_COUNT
while ii > 0:
    qSlotCounts["sg%02d.q" % ii] = SlotCount()
    ii -= 1

for line in os.popen("qstat -u \* -f | tail -n +3"):
    if line.startswith('-'):
        continue
    fields = line.split()
    if line.startswith(' '):
        if fields[4] == 'qw' and int(fields[7]) > BIG_JOB_SLOT_COUNT:
            waitingBigJobs.append( WaitingJob(int(fields[0]), int(fields[7])) )
    elif len(fields) == 5:
        qq = fields[0][:fields[0].find('@')]
        if qq in qSlotCounts:
            used = int(fields[2][:fields[2].find('/')])
            tot = int(fields[2][fields[2].find('/')+1:])
            if 0 == used:
                qSlotCounts[qq].free += tot
            else:
                qSlotCounts[qq].used += tot

print 'qSlotCounts =',qSlotCounts
print 'waitingBigJobs =',waitingBigJobs

for job in waitingBigJobs:
    ii = SG_QUEUE_COUNT
    while ii > 0:
        qq = "sg%02d.q" % ii
        if qq in qSlotCounts and qSlotCounts[qq].free >= job.slots:
            print "Setting hard job queue for %d to %s" % (job.id,qq)
            for line in os.popen("qalter -q %s %d" % (qq,job.id)):
                print line,
            del qSlotCounts[qq]
            break
        ii -= 1

print "Finished at", datetime.datetime.now()
