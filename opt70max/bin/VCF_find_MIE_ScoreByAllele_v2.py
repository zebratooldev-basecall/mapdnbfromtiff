#!/usr/bin/env python

## This code no longer tried to match the listvariant/testvariant code, but instead tries to
## not only identify MIEs, but figure out exactly which variant is an MIE (if there are multiple on each line)
## NOTE: Only code for autosomes is implemented
## NOTE: completely no-called and completly ref-called lines are skipped
## NOTE: lines where the child is completely no-called are skipped (can't find MIEs here anyway)
## NOTE: lines where the child is hom-ref have no scores, but these CAN be MIEs, so all scores are set to 0 (this is what has changed from v1 to v2)
## NOTE: each locus is included twice (once for each allele), so in some cases just counting up "BAD" calls can double-count the same locus (eg. parents are both hom-ref, child is alt/alt)
## NOTE: GOOD plus HasNoCalls=True really means "consistent"

## The output format is:
## v1/v2 (first or second variant in the child)
## call (the variant number call in the child)
## variant (the actual variant call in the child, can be ".")
## zygosity (het-ref, hom, etc.)
## varType (snp, ins, etc.)
## VAF score for child
## EAF score for child
## HasNoCalls=True/False (do the parents contain any no-calls?)
## GOOD/BAD/good/bad (GOOD/BAD are confident calls where it's clear if this variant is an MIE, good/bad it's unclear which variant is the error,
##                    assume the higher scoring one is "good" and the lower scoreing one is "bad".  For the score use the sum of the VAF and EAF scores, which is a hack!)
## Original VCF line

import os
import sys
import re
import getopt

#############################################################################################################################
#############################################################################################################################

def test_autosomes(dad, mom, kid, types, scores, alts, newline, outf):

    # variant 1/2 could have come from mom/dad
    v1M = (kid[0] in mom) or (kid[0]==".") or ("." in mom)
    v1D = (kid[0] in dad) or (kid[0]==".") or ("." in dad)
    v2M = (kid[2] in mom) or (kid[2]==".") or ("." in mom)
    v2D = (kid[2] in dad) or (kid[2]==".") or ("." in dad)

    v1string=""
    v2string=""

    zygosity=""
    if kid[0]==kid[2]:
        if kid[0]==".":              zygosity="no-call"
        else:                        zygosity="hom"
    elif kid[0]=="." or kid[2]==".": zygosity="half"
    elif kid[0]=="0" or kid[2]=="0": zygosity="het-ref"
    else:                            zygosity="het-alt"


    HasNoCalls = "." in  mom or "." in dad
    if kid[0]==".":
        v1base = "v1\t" + kid[0] + "\t" + "."               + "\t" + zygosity + "\t" + "."                + "\t" + scores[0] + "\t" + scores[2] + "\tHasNoCalls=" + str(HasNoCalls)
    else:
        v1base = "v1\t" + kid[0] + "\t" + alts[int(kid[0])] + "\t" + zygosity + "\t" + types[int(kid[0])] + "\t" + scores[0] + "\t" + scores[2] + "\tHasNoCalls=" + str(HasNoCalls)

    if kid[2]==".":
        v2base = "v2\t" + kid[2] + "\t" + "."               + "\t" + zygosity + "\t" + "."                + "\t" + scores[1] + "\t" + scores[3] + "\tHasNoCalls=" + str(HasNoCalls)
    else:
        v2base = "v2\t" + kid[2] + "\t" + alts[int(kid[2])] + "\t" + zygosity + "\t" + types[int(kid[2])] + "\t" + scores[1] + "\t" + scores[3] + "\tHasNoCalls=" + str(HasNoCalls)

    ## deal with variant 1 only
    if v1M and v1D:
        ## In both, this is definitely ok
        v1string = v1base + "\tGOOD\t" + newline
    elif not v1M and not v1D:                                                                              
        ## in neither, this is definitely bad
        v1string = v1base + "\tBAD\t" + newline
    elif kid[0]==".":                                                                                  
        ## no-called                                                                                    
        v1string="n/c"                                                                                          
                                                                                                                            
    ## deal with variant 2 only
    if v2M and v2D:                                                                                      
        ## In both, this is definitely ok                                          
        v2string = v2base + "\tGOOD\t" + newline
    elif not v2M and not v2D:                                                                              
        ## in neither, this is definitely bad
        v2string = v2base + "\tBAD\t" + newline
    elif kid[2]==".":
        ## no-called
        v2string="n/c"

    ## both have an allele from only one parents - are they the right ones?
    if v1string=="" and v2string=="":
        if (v1M and v2D) or (v1D and v2M):
            ## both are good
            v1string = v1base + "\tGOOD\t" + newline
            v2string = v2base + "\tGOOD\t" + newline
        else:
            ## can't do anything wiht this if it's unscored...
            #if "." in scores:
            #    v1string = v1base + "\tunknown\t" + newline
            #    v2string = v2base + "\tunknown\t" + newline
            #else:
            ## only one of these is correct, assume it's the lower scoring one, but mark as less confident
            ## This score sum is a hack!!!!
            v1score = float(scores[0]) + float(scores[2])
            v2score = float(scores[1]) + float(scores[3])
            if v1score >  v2score:
                v1string = v1base + "\tgood\t" + newline
                v2string = v2base + "\tbad\t" + newline
            else:
                v1string = v1base + "\tbad\t" + newline
                v2string = v2base + "\tgood\t" + newline

    ## Only one of them was questionable, but since the other is known, this is also fine
    if v1string=="":
        v1string = v1base + "\tGOOD\t" + newline
    if v2string=="":
        v2string = v2base + "\tGOOD\t" + newline


    outf.write(v1string)
    outf.write(v2string)
        
#############################################################################################################################
#############################################################################################################################

def main():

    # parse command line options and update variables
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:", [])
    except getopt.GetoptError:
        sys.exit(usage)

    for opt, arg in opts:
        if opt == "-h":
            sys.exit('Use me properly.')
        elif opt == '-i':
            INFILE = arg
        elif opt == '-o':
            OUTBASE = arg
            
    inf = open(INFILE, 'r')
    outf = open(OUTBASE+"_MIEsScoredByAlleleType_v2.vcf", 'w')
    counter = 0
    for line in inf:
        if line.startswith('#'): continue

        ## !!!!!!!!!!!!!!!! check teh order of kid/mom/dad here!!!!
        [chr, pos, id, ref, alt, qual, filt, info, format, kid, mom, dad] = line.split()
        d = re.sub(":.*","",dad)
        m = re.sub(":.*","",mom)
        k = re.sub(":.*","",kid)

        # Skip completely no-called lines
        maxvar = max(d[0],d[len(d)-1],m[0],m[len(m)-1],k[0],k[len(k)-1])
        if maxvar == '.': continue
        if maxvar=="0": continue

        counter += 1
        #if counter > 100: break

        ################################### chrX
        if line.startswith("X"):
            continue

        ########################### chrY
        elif line.startswith("Y"):
            continue
        ########################### chrM
        elif line.startswith("M"):
            continue
        ################################### autosomes
        else:
            # skip lines where the kid is no-called; these can't be MIEs
            if k[0]=="." and k[2]==".": continue
            
            type = re.sub(".*TYPE=","",info)
            types = ["ref"] + type.split(",")
            alts  =  [ref]  + alt.split(",")
            # for lines where the kid is hom-ref; these don't have scores; set all score to 0
            if k[0]=="0" and k[2]=="0": 
                scores = ["0", "0", "0", "0"]
            else:
                scores = kid.split(":")
                scores = [ scores[i].split(",") for i,x in enumerate( format.split(":") ) if x=="HQ" or x=="EHQ"]
                scores = [ item for sublist in scores for item in sublist ]

            if len(scores) != 4:
                print "Expected scores but didn't find any, all scores set to 0 for this line:\t" + line
                scores = ["0", "0", "0", "0"]

            test_autosomes(d, m, k, types, scores, alts, line, outf)


if __name__ == "__main__":
    main()
        
