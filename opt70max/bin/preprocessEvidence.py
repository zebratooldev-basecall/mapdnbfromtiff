#!/usr/bin/env python

'''
Use this program to convert evidence files
from assembly to a format where cgatools join
can link evidence with a locus file.

For now, assume ad2_28_28 read structure.
'''

import sys
import bz2
from optparse import OptionParser

def preprocess_evidence(infile,outbase):
    # set name of output file based on input
    filebase = infile.split('.')[:-2]
    filebase = '.'.join(filebase)
    filebase = filebase.split('/')[-1]
    outfile = outbase + filebase + '_preprocessed.tsv'

    fi = bz2.BZ2File(infile,'r')
    fo = open(outfile,'w')
    try:
        for line in fi:
            if line[0] == '#':  continue
            if line == '\n':  continue
            # write header line
            if line[0] == '>':  
                line = line.rstrip()
                line += '\trefBegin\trefEnd\n'
                fo.write(line)
                continue

            line = line.rstrip().split('\t')
            refOffset = int(line[11])
            refBegin = refOffset - 1 ## Vcf is 1-based, everything CGI 0-based
            refEnd = refBegin + 40 ## strand doesn't matter here
            # make refEnd longer than a DNB arm for now.  Still a hack.
            line.append(refBegin)
            line.append(refEnd)
            fo.write('\t'.join(map(str,line)) + '\n')

    finally:
        fi.close()
        fo.close()

def parse_arguments(arguments):
    parser = OptionParser()

    parser.add_option('-i','--input-file', help='bz2-zipped evidence file from assembly pipeline', dest='infile')
    parser.add_option('-o','--output-base', help='base name for output files', dest='outbase')
    options,args = parser.parse_args(arguments[1:])

    return options.infile, options.outbase

def main(args):
    infile, outbase = parse_arguments(args)

    preprocess_evidence(infile, outbase)
    
if __name__=='__main__':
    main(sys.argv)
