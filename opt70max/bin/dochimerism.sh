#! /bin/bash

for workdir in "$@"; do
    workdir="$1"
    collection="${workdir}/collection.xml"
    gaps="${workdir}/allgaps"
    reference=/prod/pv-01/pipeline/REF/HUMAN-M_06-REF/reference.gbi

    for libdir in ${workdir}/GAPS/GS*; do
        LIB=${libdir##*/}
        CHDM=${libdir}/libmap/libmap-biggest-chunk.chdm

        LocalScan \
            --dump-env \
            --dump-timings \
            --mode="explore-unmapped-dnb-structure" \
            --reference="${reference}" \
            --dnb-collection="${collection}" \
            --gaps-dir="${gaps}" \
            --chdm="${CHDM}" \
            --output="chimera-output-${LIB}.txt" \
            --sample-size="30000" \
            --sequence="CTGCAG" \
            --sequence="CTGAAG" \
            --sequence="CTTCAG" \
            --sequence="CAGCAG" \
            --sequence="CTGCTG"

        localScanExploreCounter.py \
            --input="chimera-output-${LIB}.txt" \
            --output="chimera-stats-${LIB}.csv" \
            --library="${LIB}"

    done

done

