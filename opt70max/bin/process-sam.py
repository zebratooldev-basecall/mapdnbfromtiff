#!/usr/bin/env python

import os
import sys
import copy
import sequence
from optparse import OptionParser

class SamRecord(object):
    def __init__(self, fields):
        # mandatory fields
        i = 0
        self.qname  = fields[i]; i += 1 # Query template NAME
        self.flag   = int(fields[i]); i += 1 # bitwise FLAG
        self.rname  = fields[i]; i += 1 # Reference sequence NAME
        self.pos    = int(fields[i]); i += 1 # 1-based leftmost mapping POSition
        self.strand = '+'
        self.mapq   = fields[i]; i += 1 # MAPping Quality
        self.cigar  = fields[i]; i += 1 # CIGAR string
        self.rnext  = fields[i]; i += 1 # Ref. name of the mate/next read
        self.pnext  = fields[i]; i += 1 # Position of the mate/next read
        self.tlen   = fields[i]; i += 1 # ovserved Template LENgth
        self.seq    = fields[i]; i += 1 # segment SEQuence
        self.qual   = fields[i]; i += 1 # ASCII of Phred-scaled base QUALity+33

        # check flag to determine if alignment is to the negative strand.
        if self.flag & 16:
            self.strand = '-'

            # rix reverse complemented sequence.
            self.seq = sequence.reverse_complement(self.seq)

        # optional fields
        self.opt_fields = {}
        while i < len(fields):
            tokens = fields[i].split(':')
            if tokens[1] == 'i':
                self.opt_fields[tokens[0]] = int(tokens[2])
            else:
                self.opt_fields[tokens[0]] = tokens[2]
            i += 1

        # set discordances
        self.__set_positional_mismatches()

        
    def __set_positional_mismatches(self):
        # create list to contain reference bases for mismatches.
        # if list contains a 'False' value the base matches reference,
        # else the list contains the reference base.
        self.discordant_bases = [ False ] * len(self.seq)
        self.discordant_base_count = 0

        # if not optional fields contain mismatch string then exit
        if not self.opt_fields.has_key('MD'):
            return

        # if mismatch string contains a deletion ('^') then skip for now.
        if self.opt_fields['MD'].find('^') >= 0:
            return

        # parse discordance
        m = ''; toks = []
        mm_string = self.opt_fields['MD']
        for i in range(len(mm_string)):
            if mm_string[i].isdigit():
                m = m + mm_string[i]
            else:
                if len(m)>0: toks.append(m)
                toks.append(mm_string[i])
                m = ''
        if len(m)>0: toks.append(m)

        # set reference bases.
        i = -1
        for j in range(len(toks)):
            if toks[j].isdigit():
                i += int(toks[j])
            else:
                i += 1
                self.discordant_bases[i] = toks[j]
                self.discordant_base_count += 1

        # if read is mapped to the minus strand, reverse the order.
        if self.strand == '-':
            self.discordant_bases.reverse()

class SamReader(object):
    def __init__(self, fn):
        self.fn = fn
        self.ff = open(fn, 'r')
        while True:
            line = self.ff.readline()
            if line.startswith('@'): continue
            self.lastline = line.rstrip('\r\n').split('\t')
            break

    def nextline(self):
        if self.lastline == None:
            return None
        line = self.ff.readline()
        lastline = self.lastline
        if not line:
            self.lastline = None
        else:
            self.lastline = line.rstrip('\r\n').split('\t')
        return lastline

    def close(self):
        self.ff.close()

    def next(self):
        fields = self.nextline()
        if fields == None:
            return None
        smr = SamRecord(fields)
        dnb = smr.qname
        mappings = [smr]
        while True:
            if self.lastline == None: break
            if self.lastline[0] != dnb: break
            mappings.append(SamRecord((self.nextline())))

        return mappings

# Option parsing
parser = OptionParser()
parser.add_option("-l", "--length", type="int", dest="readlength", help="Length of the reads")
parser.add_option("-f", "--samfile", dest="samfile", help="Sam (output of bowtie2) file to be processed")
(opts, args) = parser.parse_args()

fp = opts.samfile
rl = opts.readlength

dnbCount = 0
mapped = 0
uniqMapped = 0
disc = { 'call':0, 'mismatch':0 }
bases = { 'A':0, 'C':0, 'G':0, 'T':0 }
posDisc = []
baseComp = []
cigars = {}
discPattern = []
discPerDnb = [0]*rl
for i in range(rl):
    posDisc.append(copy.deepcopy(disc))
    baseComp.append(copy.deepcopy(bases))
    discPattern.append([])
    for j in range(rl):
        discPattern[i].append(copy.deepcopy(disc))
        

sr = SamReader(fp)
while True:
    mappings = sr.next()
    if mappings == None: break
    dnbCount += 1
    if mappings[0].rname == '*': continue
    mapped += 1
    if len(mappings) == 1:
        uniqMapped += 1
    smr = mappings[0]
    discordances = 0
    for i in range(len(smr.seq)):
        if smr.seq[i] == 'N': continue
        baseComp[i][smr.seq[i]] += 1
        posDisc[i]['call'] += 1
        if smr.discordant_bases[i]:
            discordances += 1
            posDisc[i]['mismatch'] += 1
            for j in range(rl):
                if i == j: continue
                discPattern[i][j]['call'] += 1
                if smr.seq[i] == smr.seq[j]:
                    discPattern[i][j]['mismatch'] += 1
    if not cigars.has_key(smr.cigar):
        cigars[smr.cigar] = 1
    else:
        cigars[smr.cigar] += 1
    discPerDnb[discordances] += 1
sr.close()

print('dict,map')
print('inputDnbCount,%d' % dnbCount)
print('mappedDnbCount,%d' % mapped)
print('uniquelyMappedCount,%d' % uniqMapped)
print('discordantBases,%d' % sum([ x['mismatch'] for x in posDisc ]))
print('mappedBases,%d' % sum([ x['call'] for x in posDisc ]))
print('')
print('array,discordancePerPosition')
print('position,totalBaseCount,discordantBaseCount')
for i in range(rl):
    print('%d,%d,%d' % (i+1, posDisc[i]['call'], posDisc[i]['mismatch']))
print('')
print('array,discordancesPerDnb')
print('discordantBaseCount,dnbCount')
for i in range(rl):
    print('%d,%d' % (i, discPerDnb[i]))
print('')
print('array,baseComposition')
print('position,callA,callC,callG,callT')
for i in range(rl):
    print('%d,%d,%d,%d,%d' % (i+1, baseComp[i]['A'], baseComp[i]['C'], baseComp[i]['G'], baseComp[i]['T']))
print('')
print('array,cigarStrs')
for cigar,count in cigars.items():
    print('%s,%d' % (cigar,count))
print('')
print('array,discordancePattern')
print('queryPosition,comparisonPosition,discordantBases,matchingBaseCalls')
for i in range(rl):
    for j in range(rl):
        if i == j: continue
        print('%d,%d,%d,%d' % (i+1,j+1,discPattern[i][j]['call'],discPattern[i][j]['mismatch']))

