#! /usr/bin/env python

import os, subprocess, sys, wfclib.jsonutil
from os.path import join as pjoin, basename, splitext
from optparse import OptionParser
from cgiutils import bundle
from glob import glob


def doCall(args):
    print '"' + '" \\\n"'.join(args) + '"'
    sts = subprocess.call(args)
    if sts != 0:
        raise Exception('%s failed with sts %d' % (args[0], sts))


def optShellNameToPyName(name):
    result = []
    for cc in name:
        if '-' == cc:
            result.append('_')
        else:
            result.append(cc)
    return ''.join(result)


def getMostRecentStateFile(workDir, stage, subworkflow=None):
    if subworkflow is not None:
        stage += subworkflow
    fna = glob(pjoin(workDir, 'state', 'st.*.'+stage+'.*'))
    sfna = []
    for fn in fna:
        (id1,id2) = map(int,fn.split('.')[-2:])
        sfna.append( (id1,id2,fn) )
    sfna.sort()
    return sfna[-1][-1]


def requireParam(options, name):
    if options.__dict__[optShellNameToPyName(name)] is None:
        raise Exception('missing required parameter: %s' % name)


def main():
    parser = OptionParser('''usage: %prog PARAMS
    
    Creates visualizations in $PWD/html directory. This spawns a
    workflow where the current directory is the workDir, so run this
    in an empty directory. The only required params are asm-work-dir
    and locus-file.

    The locus file is a comma-separated file with no header, and it is
    expected to have the following comma-separated fields on each line:

        LeftChromosome  - Chromosome of the left-flanking reference sequence.
        LeftOffset      - Offset of the left-flanking reference sequence.
        LeftDirection   - Direction of the left-flanking reference sequence (typically F for forward).
        RightChromosome - Chromosome of the right-flanking reference sequence.
        RightOffset     - Offset of the right-flanking reference sequence.
        RightDirection  - Direction of the right-flanking reference sequence (typically B for backward).
        HtmlComment     - An html comment string.
        Allele1         - The first non-reference allele to visualize (reference is implied).
        Allele2 ...     - The second non-reference allele to visualize.

    Some examples:

        One-base visualization of a snp:
            chr1,143700873,F,chr1,143700874,B,dbsnp:rs1539654,A
        One-base visualization of a deletion:
            chr1,143700873,F,chr1,143700874,B,my del,
        Junction visualization:
            chr1,143700873,F,chr1,124076989,F,,ACAGGACTAGAAATAC
        Visualization of all possible snps at a locus:
            chr1,143700873,F,chr1,143700874,B,dbsnp:rs1539654:A,A,C,G,T
    ''')

    parser.disable_interspersed_args()
    parser.add_option('--asm-work-dir', default=None,
                      help='Work dir for AsmPrep stage of assembly.')
    parser.add_option('--subworkflow', default=None,
                      help='subworkflow suffix in case of multiple subworkflows')
    parser.add_option('--locus-file', default=None,
                      help='Path to locus definition file.')
    parser.add_option('--node-list', default=None,
                      help='Mapping server node list (default spawns new mapping servers).')
    parser.add_option('--call-files', default=None,
                      help='Colon-separated list of call files to display at the top of visualizations.')
    parser.add_option('--seed', type=int, default=12345,
                      help='Random number generator seed.')
    parser.add_option('--sample-size', type=int, default=100,
                      help='Number of loci to visualize (chosen at random from locus-file).')
    parser.add_option('--phased-fragment-map-prefix', type=str, default=None,
                      help='Phased fragment map, for determining haplotype of LFR DNBs.')
    parser.add_option('--server', '-s', default=None,
                      help='host:port of the scheduler node')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        raise Exception('unexpected args')

    requireParam(options, 'asm-work-dir')
    requireParam(options, 'locus-file')
    
    #if os.path.abspath(os.getcwd()) == os.path.abspath(options.asm_work_dir):
    #    raise Exception ('--asm-work-dir should not coincide with current working directory');

    overrides = bundle()
    overrides.asmWorkDir = os.path.abspath(options.asm_work_dir)
    overrides.locusFile = os.path.abspath(options.locus_file)
    overrides.seed = options.seed
    overrides.sampleSize = options.sample_size
    if options.node_list is not None:
        overrides.nodeList = options.node_list
    if options.call_files is not None:
        overrides.callFiles = options.call_files
    if options.phased_fragment_map_prefix is not None:
        overrides.lfrPhasedFragmentMapPrefix = options.phased_fragment_map_prefix

    extraParams = []
    if options.server:
        extraParams += [ '-s', options.server ]
        
    name = 'vis-' + basename(options.asm_work_dir)
    
    if options.subworkflow :
        name        += options.subworkflow 
        extraParams += [ '--clone-subworkflow', options.subworkflow ]
        overrides.asmWorkDir = os.path.join(overrides.asmWorkDir, options.subworkflow)

    doCall(['rat',
            'create',
            name,
            'onestep.py',
            '-c',
            'OneStepVisualize',
            '--clone-state-file=%s' %
            getMostRecentStateFile(options.asm_work_dir, 'Asm', options.subworkflow),
            '-b',
            'Visualize',
            '-x',
            wfclib.jsonutil.dumps(overrides)
            ] + extraParams)


if __name__ == '__main__':
    main()
