#!/usr/bin/python

"""

"""

__author__ = "Anushka Brownley"
__version__ = "$RCSId$"
__date__ = "$Date$"
__copyright__ = "Copyright (c) 2008 Complete Genomics"
__license__ = "Proprietary"

import os.path
import sys
import getopt
import shutil
import re
from datetime import datetime, date, time

HOST="vip-app-pgcw3:8080"
APPNAME="dpm"

def usage(script):
    print "\nThis script takes a csv file containing slide-lane-cycle LIMS override data and runs the "
    print "RS workflow for each entry in the input file with the corresponding override parameters.\n"
    print script
    print "    -h, --help          <arg>   outputs this help menu"
    print "    -c, --csv           <arg>   comma-delimited RS request form"
    print "    [-p, --prod-cluster         flag to run processes on production cluster]"
    print "    [-v, --sw-version   <arg>   CGI HOME sw version to run"
    print "    [-w, --work-dir     <arg>   work directory: default /Proj/Shared/ReedSolomon/GridRuns]"
    print "    [-o, --out-dir      <arg>   output directory: default /Proj/Shared/ReedSolomon]"
    print "    [-r, --rs-dyeswap   <arg>   dye swap information for RS analysis]"
    print "    [-b, --bypass-dbload        hold data from being loaded into the database"
    print "    [-f, --field-list   <arg>   list of fields to print DNB sequence data]"
    print "    [-s, --run-class    <arg>   workflow class in ReedSolomonAnalysis.py to run (default: ReedSolomonAnalysisFromLIMS)]"
    print "    [-d, --dpm-host     <arg>   dpm override (default: production dpm)]"
    print "    [-l, --lims-host    <arg>   lims host override (default: production lims)]"
    print "    [-a, --lims-api-wf  <arg>   lims workflow override (default: production lims api]"
    print '    [-g, --adf-override <arg>   adf workflow parameter override, the content will be put into "adf":{} by the script]\n'

def main(argv):
    script = argv[0]
    curPath = os.path.dirname(os.path.abspath(script))
    argv = argv[1:]
    inputCSV = None
    dpmOverride = None
    limsOverride = None
    limsWFOverride = None
    runWFClass = None
    dbload = True
    prodCluster = False
    fields = ''
    removeLST = []
    keepWorkDir = False
    RSWF = curPath+"/../etc/workflows/onestep/ReedSolomonAnalysis.py"
    rootWorkDir = "/Proj/Shared/ReedSolomon/GridRuns"
    outputDir = "/Proj/Shared/ReedSolomon"
    rsDyeSwap = ''
    swVersion = None
    adfOverride = None
    
    try:
        opts, args = getopt.getopt(argv, "h:c:pv:w:o:r:bf:s:d:l:a:g:", ["help=", "csv=", "prod-cluster", "work-dir=", "out-dir=", "field-list=", "rs-dyeswap=", "bypass-dbload","dpm-host=", "lims-host=", "lims-wf=", "run-stage=", "adf-override="])
    except getopt.GetoptError:
        usage(script)
        sys.exit(1)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(script)
        elif opt in ("-c", "--csv"):
            inputCSV = arg
        elif opt in ("-w", "--work-dir"):
            rootWorkDir = arg
            keepWorkDir = True
        elif opt in ("-o", "--out-dir"):
            outputDir = arg
        elif opt in ("-v", "--sw-version"):
            swVersion = arg
        elif opt in ("-r", "--rs-dyeswap"):
            rsDyeSwap = arg
        elif opt in ("-d", "--dpm-host"):
            dpmOverride = arg
        elif opt in ("-l", "--lims-host"):
            limsOverride = arg
        elif opt in ("-a", "--lims-wf"):
            limsWFOverride = arg
        elif opt in ("-f", "--field-list"):
            fields = arg
        elif opt in ("-s", "--run-class"):
            runWFClass = arg
        elif opt in ("-b", "--bypass-dbload"):
            dbload = False
        elif opt in ("-p", "--prod-cluster"):
            prodCluster = True
        elif opt in ("-g", "--adf-override"):
            adfOverride = arg
    
    if inputCSV is None:
        print "Please enter input csv file."
        usage(script)
        sys.exit(1)
        
    if not(os.path.isfile(inputCSV)):
        raise Exception("Input csv file does not exist: " + inputCSV)
        
    if not(os.path.isdir(rootWorkDir)):
        raise Exception("Work directory does not exist: " + rootWorkDir)
    
    if not(os.path.isdir(outputDir)):
        raise Exception("Output directory does not exist: " + outputDir)
    
    # read input csv file
    inFH = open(inputCSV, "r")
    print "Read input file"
    matrix = []
    for line in inFH:
        matrix.append(line.rstrip().rsplit(","))
    
    inFH.close()
    limsDNBPos = []
    probeOffset = []
    anchorLST = []
    positionLST = []
    for row in range (0, len(matrix)):
        limsDNBPos.append(matrix[row][0])
        probeOffset.append(matrix[row][1])
        anchorLST.append(matrix[row][2])
        positionLST.append(matrix[row][3])
    
    # read header lines
    for col in range(4,len(matrix[0])):
        # get run parameters from input file
        slide = matrix[0][col]
        lane = matrix[1][col]
        if slide.rstrip() == '' or lane.rstrip() == '':
            continue
        
        fieldLST = fields.rstrip().rsplit(",")
        for i in range (0,len(fieldLST)):
            if fieldLST[i].rstrip() == "":
                fieldLST.pop(i)
                continue
            
            l = re.compile('^L0[0-9]C[0-9][0-9]R[0-9][0-9]$')
            if l.match(fieldLST[i]) == None:
                newID = lane+fieldLST[i]
                if l.match(newID) == None:
                    raise Exception("Field name is incorrectly formated: " + fieldLST[i])
                else:
                    fieldLST[i] = newID
            else:
                fieldLST[i] = lane+fieldLST[i][3:9]
        
        fields = ",".join(fieldLST)
        
        #validate slide and lane name
        s = re.compile('GS[0-9][0-9][0-9][0-9][0-9]-FS3')
        if s.match(slide) == None:
            raise Exception("Slide name incorrectly formatted"+slide)
        
        l = re.compile('L0[0-9]')
        if l.match(lane) == None:
            raise Exception("Lane name incorrectly formatted"+lane)
        
        if keepWorkDir:
            workDir = rootWorkDir
        else:
            date = str(datetime.now()).replace(" ","_").replace(":","-")
            workDir = rootWorkDir + "/" + slide + "_" + lane + "_" + date
            makeDir(workDir)
        
        print "Processing " + slide + "-" + lane
        
        # retrieve accession list for slide from DPM
        slideFile = slide+"_accessions.list"
        if not os.path.isfile(slideFile):
            removeLST.append(slideFile)
            if dpmOverride == None:
                os.system("curl -s \"http://vip-app-pgcw3:8080/dpm/rs/dpm/findAccessions?name="+slide+"*\" | grep String | cut -d \">\" -f 2 | cut -d \"<\" -f 1 > "+slideFile)
            else:
                os.system("curl -s \""+dpmOverride+"/findAccessions?name="+slide+"*\" | grep String | cut -d \">\" -f 2 | cut -d \"<\" -f 1 > "+slideFile)
                
        preferredCycles = []
        cycleLST = []
        for row in range(2,len(matrix)):
            if col < len(matrix[row]):
                if matrix[row][col].find(";") > -1:
                    cycleDataLST = matrix[row][col].split(";")
                    rsDyeSwap += matrix[row][0]+";"+";".join(cycleDataLST[1:])+","
                    matrix[row][col] = cycleDataLST[0]
                
                cycleLST.append(matrix[row][col])
            
        uniqueCyclesLST = list(set(cycleLST))
        uniqueCycleCount = 1
        
        # Set up queryLIMS override parameters
        rsaOverride = "\"rsa\" : {\"outDir\": \""+outputDir+"\", \"dye-swap\": \""+rsDyeSwap.rstrip(",")+"\""
        if fields == '':
            if dbload:
                rsaOverride += "}"
            else:
                rsaOverride += ", \"dbload\": \"false\"}"
        else:
            rsaOverride += ", \"fields\": \""+fields+"\", \"dbload\": \"false\", \"gridArrSize\": \"1\"}"
        
        if limsOverride == None:
            override = "'{\"queryLIMS\" : { \"lane\":\""+slide+"-"+lane+"\" }, "+rsaOverride
        else:
            override = "'{\"queryLIMS\" : { \"lane\":\""+slide+"-"+lane+"\", \"server\":\""+limsOverride+"\", \"workflow\":\""+limsWFOverride+"\" }, "+rsaOverride
        
        # Setup mergeCycles override parameters
        if len(uniqueCyclesLST) > 1:
            override += ", \"input\" : {\"mergeCycles\" : [{\"lane\" : \"" + slide + "-" + lane + "\",\"cycles\" : [{"
        
        for row in range (2, len(matrix)):
            
            if col >= len(matrix[row]):
                continue
            
            if matrix[row][col].strip() == '':
                continue
                
            anchor = anchorLST[row]
            pos = positionLST[row]
            
            if len(matrix[row][col]) == 2:
                cycle = "C"+matrix[row][col]
            else:
                cycle = "C0"+matrix[row][col]
            
            pathFile = slide+"_"+cycle+"_"+lane+".path"
            os.system("grep " + slide + " " + slideFile + " | grep " + lane + " | grep " + cycle + " > " + pathFile)
            pathFH = open(pathFile, "r")
            accession = pathFH.readline().rstrip()
            pathFH.close()
            if accession.rstrip() == "":
                raise Exception("ERROR: no accession was returned for " + slide + " " + lane + " " + cycle)
            
            if dpmOverride == None:
                os.system("curl -s \"http://vip-app-pgcw3:8080/dpm/rs/dpm/getPaths?accessions="+accession+"&clientType=native%2Funix\" | grep value | cut -d \">\" -f 2 | cut -d \"<\" -f 1 > " + pathFile)
            else:
                os.system("curl -s \""+dpmOverride+"/getPaths?accessions="+accession+"&clientType=native%2Funix\" | grep value | cut -d \">\" -f 2 | cut -d \"<\" -f 1 > " + pathFile)
            
            pathFH = open(pathFile, "r")
            path = pathFH.readline().rstrip()
            pathFH.close()
            if path.rstrip() == "":
                raise Exception("ERROR: no path was returned for " +slide + "-" + lane + "-" + cycle)
                    
            os.system("rm "+pathFile)
            tempDCT = {}
            tempDCT["isCompleted"] = "true"
            tempDCT["anchorName"] = "\""+anchor+"\""
            tempDCT["isBiological"] = "true"
            tempDCT["position"] = "\""+pos+"\""
            tempDCT["quality"] = 100.0
            tempDCT["number"] = matrix[row][col]
            tempDCT["location"] = "\""+path+"\""
                
            keyTotal = len(tempDCT.keys())
            keyCount = 1
            preferredCycles.append(tempDCT["number"].rstrip())
            for dataType in tempDCT.keys():
                override += "\"" + dataType + "\": " + str(tempDCT[dataType])
                if keyCount < keyTotal:
                    override += ","
                else:
                    if uniqueCycleCount == len(uniqueCyclesLST)-1:
                        override += "}]"
                    else:
                        override += "},{"
                
                keyCount += 1
             
            uniqueCycleCount += 1
        
        adfParamList = []
        
        if len(uniqueCyclesLST) > 1:
            preferredCyclesInAdf = '"preferredCycles" : {"%s-%s":[%s]} ' % (slide,lane,",".join(preferredCycles))
            adfParamList.append(preferredCyclesInAdf) 
            override += "} ]"
            
        override += "}"

        if adfOverride:
            adfParamList.append(adfOverride) 

        if len(adfParamList):
            override += ", \"adf\": {%s} " % (",".join(adfParamList))
            
        override += "}'"        
        if runWFClass == None:
            ratCommand = "rat create RS-" + slide + "-" + lane + " " + RSWF + " -c ReedSolomonAnalysisFromLIMS -x "+override+" -w " + workDir
        else:
            ratCommand = "rat create RS-" + slide + "-" + lane + " " + RSWF + " -c " + runWFClass + " -x "+override+" -w " + workDir
        
        if not swVersion == None:
            ratCommand += " --cgi-home="+swVersion

        if not prodCluster:
            ratCommand += "  -s lxv-testscrat01:8080"

        os.system(ratCommand)
        print "Submitted " + slide + " " + lane
        
    removeData(removeLST)
    print "Done!\n"
 
################################ 
def removeData(removeLST):
    for rmfile in removeLST:
        if os.path.isfile(rmfile):
            os.remove(rmfile)
        elif os.path.isdir(rmfile):
            os.rmdir(rmfile)
        
################################
def makeDir(dirPath):
    if not(os.path.isdir(dirPath)):
        os.mkdir(dirPath, 0775)
        
################################    
if __name__ == "__main__":
    main(sys.argv)
