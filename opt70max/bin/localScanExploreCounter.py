#!/usr/bin/env python
import os,sys
import optparse

# These are classes classified based on arm-level information. 
prettyClassMap = { 'base-call-errors' : 'base-call-errors',
                   'too-many-nocalls' : 'too-many-nocalls',
                   'rare-gaps' : 'rare-gaps',
                   'missed-by-mapper' : 'missed-by-mapper'
                   }

def getPrettyClass(clz, arm):
    className = 'other'
    # No conversion (arm level class)
    if clz in prettyClassMap:
        className = prettyClassMap[clz]
    # Class that needs to be coverted
    # Extract subread indices (strIndices) and the category
    if (clz.count('mismapread') > 0 or clz.count('badread') > 0):
        numElement = clz.count('mismapread') + clz.count('badread')
        str1 = clz.replace('mismapread', 'B')
        str1 = str1.replace('badread', 'C')
        if arm == 'R':
            str1 = str1.replace('3', '0')
            str1 = str1.replace('4', '1')
            str1 = str1.replace('5', '2')
        className = ''
        myList = ['0', '1', '2']
        for ind in myList:
            charIndex = str1.find(ind);
            if charIndex == -1:
                className = className + 'A'
            else:
                className = className + str1[charIndex-1]
    return className

if __name__ == '__main__':
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(readCount=3)
    parser.add_option('-i', '--input',
                      help='name of the source file produced by LocalScan')
    parser.add_option('-o', '--output',
                      help='name of the output file')
    parser.add_option('-l', '--library',
                      help='name of library')
    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    countByClass = {}
    for arm in [ 'L', 'R' ]:
        for clz in prettyClassMap.values() + [ 'other' ]:
            countByClass.setdefault('%s,%s' % (arm,clz), 0)
    sumL = 0
    sumR = 0
    qualDnbs = 0
    qualLMap = 0
    qualRMap = 0
    unmappedQualDnbs = 0
    ff = open(options.input)
    for line in ff:
        if line.startswith('DnbsPassedQualFilter:'):
            qualDnbs = int(line.split()[-1])
        if line.startswith('LMapPassedQualFilter:'):
            qualLMap = int(line.split()[-1])
        if line.startswith('RMapPassedQualFilter:'):
            qualRMap = int(line.split()[-1])
        if line.startswith('UnmappedDnbsPassedQualFilter: '):
            unmappedQualDnbs = int(line.split()[-1])
        if not ( line.startswith('GS') or line.startswith('SI') ):
            continue
        fields = line.split(',')
        otherArm = fields[1]
        clz = fields[5]
        if clz == 'unknown':
            continue
        clz = getPrettyClass(clz, otherArm)
        key = otherArm+','+clz
        countByClass.setdefault(key, 0)
        countByClass[key] += 1
        if otherArm == 'L':
            sumL += 1
        else:
            sumR += 1
    ff.close()

    out = open(options.output, 'w')
    out.write('freeform,Chimerism-%s\n' % options.library)
    out.write('BadArm,Classification,Count,PctOfSampledDnbs,ExtrapolatedPctOfTotalDnbs\n')
    keys = []
    keys = sorted(countByClass.keys())
    for key in keys:
        if 0 == qualDnbs:
            extrapolated = '0.00%'
        else:
            pct = float(qualDnbs-qualLMap) / qualDnbs
            sumT = sumL
            if key.startswith('R'):
                sumT = sumR
                pct = float(qualDnbs-qualRMap) / qualDnbs
            if 0 == sumT:
                extrapolated = '0.00%'
            else:
                extrapolated = '%.2f%%' % (100.0 * countByClass[key] / sumT * pct)
        if 0 == sumL+sumR:
            pctOfTot = '0.00%'
        else:
            pctOfTot = '%.2f%%' % (100.0*countByClass[key]/(sumL+sumR))
        out.write('%s,%d,%s,%s\n' %
                  (key, countByClass[key], pctOfTot, extrapolated))
    out.close()
