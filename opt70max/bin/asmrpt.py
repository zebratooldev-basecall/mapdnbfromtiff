#!/usr/bin/env python
import os,sys
import re
import shutil
import optparse
import matplotlib
matplotlib.use('Agg')
import numpy
import mako.template, mako.lookup, mako.exceptions

import dnbcollection
import gbifile
import accessionlib
import driverlib
import csvparser

from asmrptutil import setDef, findByName

def findCallRptInputs(asmDir):
    rdir = os.path.join(asmDir, 'callrpt')
    if not os.path.isdir(rdir):
        return []
    inputs = []
    for f in os.listdir(rdir):
        if not f.startswith('annotation') and f.endswith('.csv'):
            inputs.append('asm,,' + os.path.join(rdir,f))
            print "   ",f
    return inputs

class Timings(object):
    USER   = re.compile(r'^User   time\:\s*(?P<value>\d+\.\d+) sec$')
    SYSTEM = re.compile(r'^System time\:\s*(?P<value>\d+\.\d+) sec$')
    RECLAIMS = re.compile(r'^Page reclaims\:\s*(?P<value>\d+)$')
    FAULTS = re.compile(r'^Page faults\:\s*(?P<value>\d+)$')
    ELAPSED = re.compile(r'^Program elapsed time\:\s*(?P<h>\d\d)\:(?P<m>\d\d)\:(?P<s>\d+\.\d+)$')
    ELAPSED2 = re.compile(r'^(step elapsed time in seconds:\s*|(master )?execution failed after )(?P<value>\d+)( seconds.*)?$')
    HEAPSIZE = re.compile(r'^Max heap size:\s*(?P<value>\d+).*$')
    SLOTS = re.compile(r'^task slots:\s*(?P<value>\d+)$')

    def __init__(self):
        self.taskCount = 0
        self.slots = 1
        self.user = 0.0
        self.system = 0.0
        self.reclaims = 0
        self.faults = 0
        self.wall = 0.0
        self.wall2 = 0.0
        self.maxHeap = 0

    def __iadd__(self, a):
        self.taskCount += a.taskCount
        self.slots = max(self.slots, a.slots)
        self.user += a.user
        self.system += a.system
        self.reclaims += a.reclaims
        self.faults += a.faults
        self.wall += a.wall
        self.wall2 += a.wall2
        self.maxHeap = max(self.maxHeap, a.maxHeap)
        return self

    def parse(self, line):
        m = Timings.USER.match(line)
        if m:
            self.user += float(m.group('value'))
            return
        m = Timings.SYSTEM.match(line)
        if m:
            self.system += float(m.group('value'))
            return
        m = Timings.RECLAIMS.match(line)
        if m:
            self.reclaims += float(m.group('value'))
            return
        m = Timings.FAULTS.match(line)
        if m:
            self.faults += float(m.group('value'))
            return
        m = Timings.ELAPSED.match(line)
        if m:
            self.wall += 3600.0 * float(m.group('h'))
            self.wall += 60.0 * float(m.group('m'))
            self.wall += float(m.group('s'))
            return
        m = Timings.ELAPSED2.match(line)
        if m:
            self.wall2 += float(m.group('value'))
        m = Timings.HEAPSIZE.match(line)
        if m:
            self.maxHeap = max(self.maxHeap, int(m.group('value')))
        m = Timings.SLOTS.match(line)
        if m:
            self.slots = max(self.slots, int(m.group('value')))

    def parseEnd(self):
        self.wall *= self.slots
        self.wall2 *= self.slots

def readTimesFromLog(fn):
    print 'loading from: '+fn
    f = open(fn, 'r')

    try:
        t = Timings()
        t.taskCount += 1

        for line in f:
            try:
                t.parse(line)
            except:
                pass

        t.parseEnd()
        return t
    finally:
        f.close()

def getLogFileNames(dir, subdir, result):
    # Note: the straightforward implementation of this function using
    # os.walk() is too slow, presumably because it has to check each
    # file to see if it's a directory.
    fileExpr = re.compile(r'.*\.([^/]+)\.o\d+(\.\d+)?$')
    scriptExpr = re.compile(r'.*\.script$')
    files = os.listdir(os.path.join(dir,subdir))
    for fn in files:
        m = fileExpr.match(fn)
        if m:
            result.append(os.path.join(subdir, fn))
            continue
        m = scriptExpr.match(fn)
        if m:
            continue
        if os.path.isdir(os.path.join(dir,subdir,fn)):
            getLogFileNames(dir, os.path.join(subdir, fn), result)

def readLogs(dir, result):
    files = []
    getLogFileNames(dir, "", files)
    progressStep = max(100, len(files)//20)
    progress = 0
    nextMessage = progress + progressStep

    exprs = {
        'stepExpr'    : re.compile(r'^(?P<wid>[^/]+)\.(?P<stage>[^/.]+)\.(?P<step>[^/.]+)\.o\d+(\.0)?$'),
        'taskExpr'    : re.compile(r'^(?P<wid>[^/]+)\.(?P<stage>[^/.]+)\.(?P<step>[^/.]+)/(?P=wid)\.(?P=stage)\.(?P=step)\.o\d+\.(?P<task>\d+)$'),
        'sgStepExpr'  : re.compile(r'^(?P<wid>[^/]+)\.(?P<stage>[^/.]+)\.(?P<step>[^/.]+)/(?P=wid)\.(?P=stage)\.(?P=step)\.o\d+$'),
        'subStepExpr' : re.compile(r'^(?P<wid>[^/]+)\.(?P<stage>[^/.]+)\.(?P<step>[^/.]+)/(?P=wid)\.(?P=stage)\.(?P<substep>[^/.]+)\.o\d+$'),
        'subTaskExpr' : re.compile(r'^(?P<wid>[^/]+)\.(?P<stage>[^/.]+)\.(?P<step>[^/.]+)/(?P=wid)\.(?P=stage)\.(?P<substep>[^/.]+)/(?P=wid)\.(?P=stage)\.(?P=substep)\.o\d+\.(?P<task>\d+)$'),
        }

    gridOverheadTimes = {}
    for fn in files:
        for (exprType,expr) in exprs.items():
            m = expr.match(fn)
            if m:
                break
        if not m:
            continue
        kv = m.groupdict()
        wid     = kv['wid']
        stage   = kv['stage']
        step    = kv['step']
        substep = ''
        if 'substep' in kv:
            substep = kv['substep']
        elif exprType == 'sgStepExpr':
            substep = 'GridOverhead'
        task = None
        if 'task' in kv:
            task = int(kv['task'])
        ffn = os.path.join(dir, fn)
        times = readTimesFromLog(ffn)
        result.append( (wid,stage,step,substep,task,times) )
        if substep == 'GridOverhead':
            if ( (wid,stage,step) not in gridOverheadTimes or
                 gridOverheadTimes[(wid,stage,step)].wall2 < times.wall2 ):
                gridOverheadTimes[(wid,stage,step)] = times
        progress += 1
        if progress == nextMessage:
            nextMessage = progress + progressStep
            print '        read %d logs out of %d' % (progress, len(files))
    for (wid,stage,step,substep,task,times) in result:
        if substep != '' and substep != 'GridOverhead' and (wid,stage,step) in gridOverheadTimes:
            gridOverheadTimes[(wid,stage,step)].wall2 -= times.wall2

def loadTimings(collection, options):
    timings = []
    readLogs(options.logDir, timings)
    collection.timings = timings

def findMapData(om, mapList):
    inputs = []
    collections = []
    gapObjects = {}

    for m in mapList:
        print "   ", m,
        mobj = om.findObject(m)
        path = os.path.join(mobj.location, 'reports', 'MappingStats.csv')
        if os.path.exists(path):
            inputs.append('lane,%s,%s,%s' % (mobj.lane, path, mobj.name))
            print ": found mapping data",

        fobj = om.findObject(mobj.adf)
        collections.append(os.path.join(fobj.location, fobj.name + '.xml'))

        if mobj.adf.endswith('FDF'):
            path = os.path.join(fobj.location, 'stats', 'final-reports.csv')
            if os.path.exists(path):
                inputs.append('lane,%s,%s,%s' % (mobj.lane, path, mobj.name))
                print ": found filter data",

        print

        gobj = om.findObject(mobj.gaps)
        path = os.path.join(gobj.location, 'enzyme-data.csv')
        if os.path.exists(path):
            gapObjects[mobj.library] = path

    for lib, fn in gapObjects.items():
        if os.path.exists(fn):
            print '    LIB %s: found enzyme data' % lib
            inputs.append('lib,%s,%s' % (lib, fn))

    return (inputs, collections)

def fillOptionsForMap(cgiHome, om, options):
    if not options.outputDir:
        raise RuntimeError('output parameter must be set when using --input-map')
    collFile = os.path.join(options.outputDir, 'collection.xml')
    options.collectionFile = collFile

    inputs, collections = findMapData(om, options.inputMap)

    inparams = ' '.join(['-i %s' % coll for coll in collections])
    system( '%s %s -o %s' % ('makeCollection', inparams, collFile) )

    setDef(options, 'inputDataFiles', inputs)

    setDef(options, 'templateDir', os.path.join(cgiHome,
                                                'etc', 'reports', 'templates'))
    templateList = ['mapping.html']
    setDef(options, 'templates', templateList)

def fillOptionsForAsm(cgiHome, om, options):
    asmObj = om.findObject(options.inputAsm)
    asmDir = asmObj.location
    refDir = om.findObject(asmObj.ref).location

    setDef(options, 'outputDir', os.path.join(asmDir, 'WebReports'))
    setDef(options, 'collectionFile', os.path.join(asmDir, 'collection.xml'))
    setDef(options, 'reference', os.path.join(refDir, 'reference.gbi'))
    setDef(options, 'templateDir', os.path.join(cgiHome,
                                                'etc', 'reports', 'templates'))

    inputs, collections = findMapData(om, asmObj.mapList)
    inputs += findCallRptInputs(asmDir)
    setDef(options, 'inputDataFiles', inputs)

    templateList = ['coverage.html', 'mapping.html', 'summary.html', 'aqc.html', 'callrpt.html', 'cqc.html']
    setDef(options, 'templates', templateList)
    setDef(options, 'coverageRoot', os.path.join(asmDir, 'coverage'))
    setDef(options, 'reportsCoverageRoot', os.path.join(asmDir, 'coverage'))

def fillDefaults(options):
    setDef(options, 'collectionFile', 'collection.xml')
    setDef(options, 'output', 'reports')
    if 'CGI_HOME' in os.environ and os.environ['CGI_HOME']:
        templateDirDefault = os.path.join(os.environ['CGI_HOME'],
                                          'etc', 'reports', 'templates')
    else:
        templateDirDefault = '.'
    setDef(options, 'templateDir', templateDirDefault)

def parseInputParams(collection, inputs):
    """Returns list of (object, filename) tuples"""
    r = []
    for x in inputs:
        tokens = x.split(',')
        if len(tokens) == 3:
            objt, name, fileName = tokens
            srcObjName = None
        elif len(tokens) == 4:
            objt, name, fileName, srcObjName = tokens
        else:
            raise RuntimeError('bad format in input file parameter: ' + x)

        if objt == 'asm':
            if name:
                raise RuntimeError('name must be empty for asm: ' + x)
            r.append((collection, fileName))
        elif objt == 'lib':
            r.append((findByName(collection.libraries, name), fileName))
        elif objt == 'lane':
            lobj = findByName(collection.lanes, name)
            r.append((lobj, fileName))
        else:
            raise RuntimeError('object type not recognized (must be asm, lib or lane): ' + x)
    return r

def loadFiles(collection, files):
    for obj, fileName in files:
        try:
            csvparser.loadFile(fileName, obj)
        except csvparser.ParseError, e:
            print 'warning: failed reading:', fileName
            print e

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(inputAsm='',  rootDir=accessionlib.DEFAULT_ROOT_DIR,
                        inputDataFiles=[], templates=[],previoustemplates=[])
    parser.add_option('-i', '--input', dest='inputAsm',
                      help='accession ID of the assembly to report on')
    parser.add_option('--input-map', dest='inputMap', action='append',
                      help='accession ID of the mapping object to report on; '
                           'this parameter may be specified multiple times')
    parser.add_option('-r', '--root-dir', dest='rootDir',
                      help='use ROOTDIR as the base directory for the pipeline '
                           'object locations')
    parser.add_option('--project-id', dest='projectId',
                      help='project ID of the assembly to report on')
    parser.add_option('--tumor', dest='tumor',
                      help='tumor or normal status of the sample')
    parser.add_option('--sample-type', dest='sampleType',
                      help='self.sampleType from the assembly workflow')
    parser.add_option('--gender', dest='gender',
                      help='manifested gender of the sample')
    parser.add_option('--coverage-level',dest='coverageLevel',
                      help='self.input.coverageLevel from the assembly workflow')
    parser.add_option('--assembly-id', dest='assemblyId',
                      help='assembly ID')
    parser.add_option('--part-number', dest='partNumber',
                      help='part number of assembly')
    parser.add_option('--reference-build', dest='referenceBuild',
                      help='NCBI reference build')
    parser.add_option('--assembly-DID', dest='assemblyDID',
                      help='DID of assembly')
    parser.add_option('--assembly-comment', dest='assemblyComment',
                      help='input comment for assembly')
    parser.add_option('--assembly-path', dest='assemblyPath',
                      help='path to root of assembly')
    # ------------------------------------------------------------------- #
    parser.add_option('--TargetRegionsFile', dest='TargetRegionsFile',
                      help="The target regions files, if none is provided, then \"None\"")
    # ------------------------------------------------------------------- #
    parser.add_option('-o', '--output', dest='outputDir',
                      help='[debug] destination directory for the reports')
    parser.add_option('--dnb-collection', dest='collectionFile',
                      help="[debug] merged collection that was used for the assembly")
    parser.add_option('--reference', dest='reference',
                      help="[debug] the reference gbi file that was used for mapping and assembly")
    parser.add_option('--template-dir', dest='templateDir',
                      help='[debug] directory that contains report templates')
    parser.add_option('--input-file', dest='inputDataFiles', action='append',
                      metavar='level,name,file-name',
                      help="[debug] load data from CSV file, attaching it to an object; "
                           "level must be 'asm', 'lib' or 'lane'; name must be empty for "
                           "'asm'-level data. This parameter may be specified multiple times")
    parser.add_option('--template', dest='templates', action='append', metavar='name',
                      help="[debug] render only the specified template. This parameter "
                           "may be specified multiple times")
    parser.add_option('--previous-template', dest='previoustemplates', action='append', metavar='name',
                      help="[debug] add the specified templates to index.html (if index.html is a template)")
    parser.add_option('--coverage-root', dest='coverageRoot',
                      help="[debug] the root directory for coverage data (typically, the coverage "
                           "subdirectory of the assembly directory)")
    parser.add_option('--reports-coverage-root', dest='reportsCoverageRoot',
                      help="[debug] the root directory for coverage data output by the Reports stage "
                           "(typically, the coverage subdirectory of the assembly directory)")
    parser.add_option('--aqc-metrics-root', dest='aqcMetricsRoot',
                      help="[debug] the root directory for input AQC metrics validation (typically, the"
                           "etc/config/AQC subdirectory of the assembly installation directory")
    parser.add_option('--cqc-metrics-root', dest='cqcMetricsRoot',
                      help="[debug] the root directory for input CQC metrics validation (typically, the"
                           "etc/config/CQC subdirectory of the assembly installation directory")
    parser.add_option('--dw-root', dest='dwRoot',
                      help="[debug] the root directory for output data warehouse metrics "
                           "(typically, the \"dw\" subdirectory of the WebReports directory)")
    parser.add_option('--log-dir', dest='logDir',
                      help="[debug] the logs directory")
    parser.add_option('--receive-date', dest='receiveDate', 
                      help="[debug] the date the sample was received")
    parser.add_option('--sample-source', dest='sampleSource', 
                      help="[debug] the source of the sample")
    parser.add_option('--customer-sample-id', dest='customerSampleId', 
                      help="[debug] the sample ID provided by the customer")
    parser.add_option('--library-control-cls-ids', dest='controlSampleClsIds',
                      help="[debug] the CLS IDs of the control samples")
    parser.add_option('--library-control-assembly-ids', dest='controlSampleAsmIds',
                      help="[debug] the assembly IDs of the control samples")

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if options.inputAsm or options.inputMap:
        cgiHome = driverlib.getCgiHome()
        print "loading assembly objects..."
        om = accessionlib.ObjectManager(options.rootDir, cgiHome)
        if options.inputAsm:
            fillOptionsForAsm(cgiHome, om, options)
        else:
            fillOptionsForMap(cgiHome, om, options)
    else:
        fillDefaults(options)

    if not os.path.exists(options.outputDir):
        os.mkdir(options.outputDir)

    print "loading collection..."
    collection = dnbcollection.Collection(options.collectionFile)

    print "loading reference..."
    if options.reference:
        reference = gbifile.GbiFile(options.reference)

    print "reading input data..."
    files = parseInputParams(collection, options.inputDataFiles)
    loadFiles(collection, files)

    if 'timings.html' in options.templates:
        print 'loading timings...'
        loadTimings(collection, options)

    # ------------------------------------------------------------------- #
    collection.TargetRegionsFile = options.TargetRegionsFile
    # ------------------------------------------------------------------- #

    print "formatting reports..."
    lookup = mako.lookup.TemplateLookup(directories=[options.templateDir])

    # __file__ is not defined in python 2.4 when using pdb 
    thisFile = None
    try: 
        thisFile = __file__
    except NameError:
        print 'IF YOU ARE USING PDB, THE __FILE__ VAR IS UNDEFINED'
        print 'Trying to get it from CGI_HOME'
        try:
            cgiHome = os.environ['CGI_HOME']
            thisFile = cgiHome + '/bin/asmrpt.py'
            print 'Got __file__ name from CGI_HOME'
            print thisFile
        except NameError:
            print 'Unable to get __file__ name from CGI_HOME'

    renderParams = {'collection':collection,
                    'options':options,
                    'outputDir':options.outputDir,
                    'creatorScript':os.path.realpath( thisFile )}
    if options.reference:
        renderParams['reference'] = reference

    for fn in options.templates:
        exn = False
        try:
            template = lookup.get_template(fn + '.mako')

            report = template.render(**renderParams)

            # If one exists, copy the style sheet to the same location as the
            # HTML file.        
            if os.path.exists(options.templateDir + '/' + fn + ".css"):
                shutil.copyfile(options.templateDir + '/' + fn + ".css", \
                                options.outputDir + '/' + fn + ".css")

            if os.path.exists(options.templateDir + '/' + fn + ".jpg"):
                shutil.copyfile(options.templateDir + '/' + fn + ".jpg", \
                                options.outputDir + '/' + fn + ".jpg")

            if os.path.exists(options.templateDir + '/' + fn + ".png"):
                shutil.copyfile(options.templateDir + '/' + fn + ".png", \
                                options.outputDir + '/' + fn + ".png")

        except:
            report = mako.exceptions.html_error_template().render()
            exn = True

        out = open(os.path.join(options.outputDir, fn), 'w')
        out.write(report)
        out.close()
        if exn:
            raise RuntimeError('failed to render '+fn+' -- look at generated html file to see what went wrong')

if __name__ == '__main__':
    main()
    
