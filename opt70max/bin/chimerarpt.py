#!/usr/bin/env python

import sys
from os.path import basename
from glob import glob

# Updated in Jan 2014, to make the report applicable to 2ad structure

KEYS = [ 'L too-many-nocalls',
         'L rare-gaps',
         'L missed-by-mapper',
         'L base-call-errors',
         'L AAB', 'L AAC',
         'L ABA', 'L ABB', 'L ABC',
         'L ACA', 'L ACB', 'L ACC',
         'L BAA', 'L BAB', 'L BAC',
         'L BBA', 'L BBB', 'L BBC',
         'L BCA', 'L BCB', 'L BCC',
         'L CAA', 'L CAB', 'L CAC',
         'L CBA', 'L CBB', 'L CBC',
         'L CCA', 'L CCB', 'L CCC',
         'L other',
         'R too-many-nocalls',
         'R rare-gaps',
         'R missed-by-mapper',
         'R base-call-errors',
         'R AAB', 'R AAC',
         'R ABA', 'R ABB', 'R ABC',
         'R ACA', 'R ACB', 'R ACC',
         'R BAA', 'R BAB', 'R BAC',
         'R BBA', 'R BBB', 'R BBC',
         'R BCA', 'R BCB', 'R BCC',
         'R CAA', 'R CAB', 'R CAC',
         'R CBA', 'R CBB', 'R CBC',
         'R CCA', 'R CCB', 'R CCC',
         'R other'
         ]

def getEmptyData():
    result = []
    for key in KEYS:
        result.append('N')
    result.append('N')
    result.append('N')
    return result

libdata = {}
for workdir in sys.argv[1:]:

    fxml = glob('%s/collection.xml' % (workdir,)) ## added to adapt new wf
    if len(fxml)==0: continue
    xml = open(fxml[0], 'r')
    for ln in xml:
        ln = ln.strip()
        if ln.startswith("<Slide"): 
            slide = ln.split("=")[-1].strip(">").strip("\"")
        if ln.startswith("<Lane DNB"):
            xlane = ln.split(" ")[4].split("=")[-1].strip("\"")
    wlane = slide+"-"+xlane

    libs = glob('%s/GAPS/GS*'%(workdir,))
    if len(libs) != 1:
        continue
    lib = basename(libs[0]) 
    
    fn = glob('%s/ASM/callrpt/chimera-stats-*.csv'%(workdir,))
    if 1 != len(fn):
        libdata['%s,%s' % (wlane,lib)] = getEmptyData()
        continue
    ff = open(fn[0])
    try:
        header1 = ff.readline()
        header2 = ff.readline()
        sumdnbs = 0
        res = ['0.00%'] * len(KEYS)
    
        for line in ff:
            fields = line[:-1].split(',')
            key = fields[0] + ' ' + fields[1]
            #print('key=' + key)
            value = fields[4]
            indx = KEYS.index(key)
            if indx < 0:
                 raise 'key mismatch: %s' % (key)
            res[indx] = fields[4]
            sumdnbs += int(fields[2])
    finally:
        ff.close()

    if len(res) != len(KEYS):
        raise 'bad count %s' % wlane
    if sumdnbs < 29000:
        libdata['%s,%s' % (wlane,lib)] = getEmptyData()
    else:
        ff = open(glob('%s/ASM/callrpt/chimera-output-*.txt'%(workdir,))[0])
        dnbs = int(ff.readline().split()[1])
        mapL = int(ff.readline().split()[1])
        mapR = int(ff.readline().split()[1])
        ff.close()
        # convert string 'x.xx%' to float number, like 25.41% to 0.2541
        vv = [ float(rr[:-1]) / 100.0 for rr in res ]
        
        ## Old code for dnb with structure issues
        ##res = [ '%.2f%%' % (100.0* (1 -
        #                            (1-vv[0]-vv[1]-vv[3]-vv[4]-vv[8])*
        #                            (1-vv[10]-vv[11]-vv[13]-vv[14]-vv[18]) ) ) ] + res
        
        # NEW: extrapolated dnbs with bad gaps (at least one subread being classified as "mismapped")
        # mismapread is repesented as "B" in the A, B, C, grouping
        dnbBadGaps_L =  vv[4] +  sum(vv[6:9]) + vv[10] + sum(vv[12:21]) + vv[22] + sum(vv[24:27]) + vv[28]
        dnbBadGaps_R = vv[35] +sum(vv[37:40]) + vv[41] + sum(vv[43:52]) + vv[53] + sum(vv[55:58]) + vv[59]
        res = [ '%.2f%%' % (100.0* (1 - (1 - dnbBadGaps_L)*(1 - dnbBadGaps_R ) )) ] + res
        res = [ '%.2f%%' % (100.0 - 100.0 * float(mapL+mapR)/(2*dnbs)) ] + res
        res = [ '%.2f%%' % (100.0 * float(mapL+mapR)/(2*dnbs)) ] + res
        libdata['%s,%s' % (wlane,lib)] = res

workdir = sys.argv[1]
out = open('%s/chimerarpt.csv'%(workdir,), 'w')
out.write(','.join(['Lane,CLS'] + [ 'MappingYieldHighScoringDnbs',
                                    '1-MappingYieldHighScoringDnbs',
                                    'extrapolated dnbs with bad gaps'] + KEYS) + '\n')
        
for lib in libdata:
    out.write(','.join([lib] + libdata[lib]) + '\n')

out.close()
