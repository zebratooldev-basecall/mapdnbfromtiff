#! /bin/bash

VERSION=$(which Assemble1 | awk -F/ '{print $(NF-3)}' | sed 's/[.]/_/g')

# Female T+N:
# /rnd/home/jbaccash/archive/tn/atcc-HCC1187/

# for build in 36 37; do

#     asm=GS000000798-B${build}-${VERSION}-ASM
#     mkdir -p atcc-HCC1187/${asm}
#     cp -lLR /rnd/home/jbaccash/archive/tn/atcc-HCC1187/ADF/GS000000798-ASM/ atcc-HCC1187/${asm}/ADF
#     rat create ${asm} onestep.py -c OneStepFromAdf -w $PWD/atcc-HCC1187/${asm} \
#         -x '{ "input": {"referenceBuild":"'${build}'", "tumor":"no"} }'

# done

for build in 36 37; do

    asm=GS000000799-B${build}-${VERSION}-ASM
    mkdir -p atcc-HCC1187/${asm}
    cp -lLR /rnd/home/jbaccash/archive/tn/atcc-HCC1187/ADF/GS000000799-ASM/ atcc-HCC1187/${asm}/ADF
    rat create ${asm} onestep.py -c OneStepFromAdf -w $PWD/atcc-HCC1187/${asm} \
        -x '{ "input": {"referenceBuild":"'${build}'", "tumor":"yes"} }'

done

# Male T+N:
# /rnd/home/jbaccash/archive/tn/P032/

for build in 36 37; do

    asm=GS00022-DNA_A01-B${build}-${VERSION}-ASM
    mkdir -p P032/${asm}
    cp -lLR /rnd/home/jbaccash/archive/tn/P032/ADF/GS00022-DNA_A01 P032/${asm}/ADF
    rat create ${asm} onestep.py -c OneStepFromAdf -w $PWD/P032/${asm} \
        -x '{ "input": {"referenceBuild":"'${build}'", "tumor":"no"} }'

done

for build in 36 37; do

    asm=GS00018-DNA_A01-B${build}-${VERSION}-ASM
    mkdir -p P032/${asm}
    cp -lLR /rnd/home/jbaccash/archive/tn/P032/ADF/GS00018-DNA_A01 P032/${asm}/ADF
    rat create ${asm} onestep.py -c OneStepFromAdf -w $PWD/P032/${asm} \
        -x '{ "input": {"referenceBuild":"'${build}'", "tumor":"yes"} }'

done

# NA12878
# /rnd/home/jbaccash/archive/replicates/NA12878/

# for build in 36 37; do

#     asm=GS00646-CLS_E04-B${build}-${VERSION}-ASM
#     mkdir -p NA12878/${asm}
#     cp -lLR /rnd/home/jbaccash/archive/replicates/NA12878/ADF/GS00646-CLS_E04/ NA12878/${asm}/ADF
#     rat create ${asm} onestep.py -c OneStepFromAdf -w $PWD/NA12878/${asm} \
#         -x '{ "input": {"referenceBuild":"'${build}'", "tumor":"no"} }'

# done

# for build in 36 37; do

#     asm=GS00650-CLS_G06-B${build}-${VERSION}-ASM
#     mkdir -p NA12878/${asm}
#     cp -lLR /rnd/home/jbaccash/archive/replicates/NA12878/ADF/GS00650-CLS_G06/ NA12878/${asm}/ADF
#     rat create ${asm} onestep.py -c OneStepFromAdf -w $PWD/NA12878/${asm} \
#         -x '{ "input": {"referenceBuild":"'${build}'", "tumor":"no"} }'

# done

# Trio:
# /rnd/home/jbaccash/archive/trio/NA19240/bb-only

for build in 36 37; do

    asm=GS19240-B${build}-${VERSION}-ASM
    mkdir -p trio/${asm}
    cp -lLR /rnd/home/jbaccash/archive/trio/NA19240/bb-only/ADF/child trio/${asm}/ADF
    rat create ${asm} onestep.py -c OneStepFromAdf -w $PWD/trio/${asm} \
        -x '{ "input": {"referenceBuild":"'${build}'", "tumor":"no"} }'

done

# for build in 36 37; do
for build in 36; do

    asm=GS19238-B${build}-${VERSION}-ASM
    mkdir -p trio/${asm}
    cp -lLR /rnd/home/jbaccash/archive/trio/NA19240/bb-only/ADF/mother trio/${asm}/ADF
    rat create ${asm} onestep.py -c OneStepFromAdf -w $PWD/trio/${asm} \
        -x '{ "input": {"referenceBuild":"'${build}'", "tumor":"no"} }'

done

# for build in 36 37; do
for build in 36; do

    asm=GS19239-B${build}-${VERSION}-ASM
    mkdir -p trio/${asm}
    cp -lLR /rnd/home/jbaccash/archive/trio/NA19240/bb-only/ADF/father trio/${asm}/ADF
    rat create ${asm} onestep.py -c OneStepFromAdf -w $PWD/trio/${asm} \
        -x '{ "input": {"referenceBuild":"'${build}'", "tumor":"no"} }'

done

# NA19240 replicate

for build in 37; do

    asm=GS19240B-B${build}-${VERSION}-ASM
    mkdir -p trio/${asm}
    cp -lLR /rnd/home/jbaccash/archive/replicates/NA19240/ADF/GS00662-CLS_F11 trio/${asm}/ADF
    rat create ${asm} onestep.py -c OneStepFromAdf -w $PWD/trio/${asm} \
        -x '{ "input": {"referenceBuild":"'${build}'", "tumor":"no"} }'

done
