#! /usr/bin/python

import sys
import os
import re
import datetime
from optparse import OptionParser

class Contig:
    def __init__(self,fields):
        self.contig = int(fields[0])
        self.length = int(fields[1])
        self.circ = fields[2]
        self.offset = int(fields[3])
        self.superid = int(fields[4])
        self.supername = fields[5]
        if self.supername.startswith("'"):
            self.supername = self.supername[1:]
        if self.supername.endswith("'"):
            self.supername = self.supername[:-1]


def readContigs(reference):
    int_re = re.compile("[0-9]+")
    contigs = []
    for line in os.popen("gbi -l %s" % reference):
        fields = line.split()
        if len(fields) == 6 and int_re.match(fields[0]) and int_re.match(fields[1]):
            contigs.append(Contig(fields))
    return contigs


def runcmd(cmd):
    print "cmd:",cmd
    sts = os.system(cmd)
    if 0 != sts:
        raise "command failed: %s" % cmd


def grokConfig(config, key):
    file = open(config)
    try:
        for line in file:
            line = line.strip()
            line = line.split("#")[0]
            if line.find("=") != -1:
                fields = line.split("=",2)
                if fields[0].strip() == key:
                    return fields[1].strip()
        return None
    finally:
        file.close()


def hasCoverageSummary(config):
    reports = grokConfig(config, "reports")
    if reports is None or "CoverageSummary" in reports.split(","):
        return True
    return False


def hasCoverageRange(config):
    report = grokConfig(config, "range-report")
    if report != None:
        return True
    return False


def covrptRanges(contigs, chunk_size):
    ranges = []
    for contig in contigs:
        start = 0
        while start < contig.length:
            end = start + options.chunk_size
            if contig.length < end:
                end = contig.length
            ranges.append( (contig,start,end) )
            start += chunk_size
    return ranges


def mergeChrs(contigs):
    chrs = []
    for contig in contigs:
        if 0 == len(chrs) or chrs[-1] != contig.supername:
            chrs.append(contig.supername)
    return chrs


def runJobs(contigs, options):
    if not os.path.exists(options.log):
        os.mkdir(options.log)
    if not os.path.exists(options.results):
        os.mkdir(options.results)
    for contig in contigs:
        dirname = "%s/%s" % (options.results, contig.supername)
        if not os.path.exists(dirname):
            os.mkdir(dirname)

    ranges = covrptRanges(contigs, options.chunk_size)
    chrs = mergeChrs(contigs)
    if not options.merge_only:
        runcmd( ("qscript.py %s --config=%s "+
                 " --chunk-size=%d"+
                 " --log=%s"+
                 " --results=%s"+
                 " --covrpt-batch=\\$SGE_TASK_ID"+
                 " --tstamp=%s"+
                 " | qsub -N covrpt-%s -t 1-%d -o %s -l job_version=2 -l virtual_free=2G") %
                (sys.argv[0],
                 options.config, options.chunk_size,
                 options.log, options.results, options.tstamp,
                 options.tstamp, len(ranges),
                 options.log) )
    if hasCoverageSummary(options.config) or hasCoverageRange(options.config):
        runcmd( ("qscript.py %s --config=%s "+
                 " --chunk-size=%d"+
                 " --log=%s"+
                 " --results=%s"+
                 " --merge-batch=\\$SGE_TASK_ID"+
                 " --tstamp=%s"+
                 " | qsub -N covmerge-%s -t 1-%d -o %s -hold_jid covrpt-%s -l job_version=2 -l virtual_free=2G") %
                (sys.argv[0],
                 options.config, options.chunk_size,
                 options.log, options.results, options.tstamp,
                 options.tstamp, len(chrs),
                 options.log, options.tstamp) )
        runcmd( ("qscript.py %s --config=%s "+
                 " --chunk-size=%d"+
                 " --log=%s"+
                 " --results=%s"+
                 " --merge-final"+
                 " --tstamp=%s"+
                 " | qsub -N covmerge-final-%s -o %s -hold_jid covmerge-%s -l job_version=2 -l virtual_free=2G") %
                (sys.argv[0],
                 options.config, options.chunk_size,
                 options.log, options.results, options.tstamp,
                 options.tstamp, options.log, options.tstamp) )


def runCovrpt(contigs, options):
    (contig,start,end) = covrptRanges(contigs, options.chunk_size)[options.covrpt_batch-1]
    runcmd( ("covrpt --config %s --output-base-name=%s/%s/%d,%d,%d,%d "+
             " --range=%d,%d,%d,%d") %
            (options.config, options.results, contig.supername,
             contig.contig,start,contig.contig,end,
             contig.contig,start,contig.contig,end) )


def runMerge(contigs, options):
    chr = mergeChrs(contigs)[options.merge_batch-1]
    ranges = covrptRanges(contigs, options.chunk_size)
    if hasCoverageSummary(options.config):
        runcmd( ("mergeCoverageSummary.py -o %s/%s/%sCoverageSummary.csv "+
                 " %s/%s/[0-9]*,[0-9]*,[0-9]*,[0-9]*CoverageSummary.csv") %
                (options.results, chr, chr, options.results, chr) )
    if hasCoverageRange(options.config):
        runcmd( ("mergeCoverageRange.py --reference=%s "+
                 " -o %s/%s/%sCoverageRangeDetail.csv "+
                 " %s/%s/[0-9]*,[0-9]*,[0-9]*,[0-9]*CoverageRangeDetail.csv") %
                (grokConfig(options.config, "reference"),
                 options.results, chr, chr, options.results, chr) )


def runFinalMerge(contigs, options):
    chrs = mergeChrs(contigs)
    if hasCoverageSummary(options.config):
        inputArray = []
        for chr in chrs:
            inputArray.append( "%s/%s/%sCoverageSummary.csv" % (options.results, chr, chr) )
        inputs = " ".join(inputArray)
        runcmd( "mergeCoverageSummary.py -o %s/allCoverageSummary.csv %s" %
                (options.results, inputs) )    
    if hasCoverageRange(options.config):
        inputArray = []
        for chr in chrs:
            inputArray.append( "%s/%s/%sCoverageRangeDetail.csv" % (options.results, chr, chr) )
        inputs = " ".join(inputArray)
        runcmd( ("mergeCoverageRange.py --reference=%s -o %s/allCoverageRangeDetail.csv %s") %
                (grokConfig(options.config, "reference"),
                 options.results, inputs) )    


parser = OptionParser()
parser.disable_interspersed_args()

parser.add_option("--config", default=None,
                  help="the covrpt config file name")
parser.add_option("--chunk-size", type=int, default=1000000,
                  help="the length of genome a single task examines")
parser.add_option("--merge-only", action="store_true", default=False,
                  help="don't run the initial covrpt runs")
parser.add_option("--log", default="log",
                  help="the log directory name")
parser.add_option("--results", default="results",
                  help="the results directory name")

# these options are used internally
parser.add_option("--tstamp", default=None,
                  help="the timestamp when this run was started")
parser.add_option("--covrpt-batch", type=int, default=None,
                  help="the task id for this covrpt task")
parser.add_option("--merge-batch", type=int, default=None,
                  help="the task id for this merge task")
parser.add_option("--merge-final", action="store_true", default=False,
                  help="do the final merge job")

(options, args) = parser.parse_args()
if options.config == None:
    print "--config option is required"
    parser.print_help()
    raise "bad args"
if options.tstamp == None:
    options.tstamp = datetime.datetime.now().isoformat().replace(":","-").replace(".","-")

print sys.argv

contigs = readContigs(grokConfig(options.config, "reference"))
if options.merge_final:
    runFinalMerge(contigs, options)
elif options.merge_batch != None:
    runMerge(contigs, options)
elif options.covrpt_batch != None:
    runCovrpt(contigs, options)
else:
    runJobs(contigs, options)
