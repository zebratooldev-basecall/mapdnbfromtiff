#!/usr/bin/python
import os,sys
from gzip import GzipFile
import optparse
import numpy
import matplotlib
matplotlib.use('Agg')
import pylab as P
from glob import glob
import mako.template, mako.lookup, mako.exceptions


def loccmp(x,y):
    if(x[0] !=  y[0]):
        if( x[0] < y[0] ):
            return -1
        else:
            return 1
    else:
        return -1


def cnv_plot(width,offsets,smoothdir,calldetails,levelsFile,outfilebase,asmid,tumor,useNorm,skipCalls,plotChr):

    wid = int(width)
    k = int( wid / 1000 )

    chrlinesx = []
    chrlinesy = []
    f_offset = open(offsets,'r')
    offsets = {}
    offsetlist = []
    for l in f_offset:
        lw = l.split()
        offsets[lw[0]] = int(lw[1])
        offsetlist.append( (int(lw[1]), lw[0]) )
        chrlinesx += [  int(lw[1]),  int(lw[1]),  int(lw[1]) ]
        chrlinesy += [ 0, 250, 0 ]
    f_offset.close()
    offsetlist.sort(loccmp)


    N = 0
    levels=[]
    if levelsFile != '' and os.path.exists(levelsFile):
        f_levels = open(levelsFile)
        for l in f_levels:
            (lev,n,sigma) = l.split()
            if ( int(n) != N ) :
                print "ERROR: levels file indexing not as expected"
                sys.exit(-1)
            levels.append(float(lev))
            N += 1
        f_levels.close()

    calls = []
    normed = []
    gccvg = []
    levelmap = {}
    if calldetails != '' and os.path.exists(calldetails):

        # get data from CNV details file
        
        f_calls = open(calldetails)
        igvfile = open(outfilebase+'.igv','w')
        igvfile.write( "Chromosome	Start	End	Feature	GCCov	NormCov	CallCov\n" )
        n=0
        

        # column header will be parsed to figure out which columns contain the interesting data
        callCol = -1
        normCol = -1
        gcCol = -1
        # intoData will be true once past the column header
        intoData = False

        
        for l in f_calls:

            if len(l.rstrip()) == 0:
                continue
                
            lw = l.rstrip().split('\t')


            # for a tumor, build level to mean coverage map
            if lw[0][0:5] == "#MEAN":
                nl = int(lw[0].split('_')[-1])
                levelmap[lw[1]] = float(levels[nl])

            # parse column header to figure out which columns are relevant
            if lw[0][0] == '>':
                for i in range(0,len(lw)):
                    if lw[i] == "calledPloidy" or lw[i] == "calledLevel":
                        callCol = i
                    if lw[i] == "avgNormalizedCvg":
                        normCol = i
                    if lw[i] == "gcCorrectedCvg":
                        gcCol = i

                # make sure we found the expected columns
                if callCol == -1:
                    print "Could not find call column in header line:\n" + l
                    sys.exit(-1)
                if normCol == -1:
                    print "Could not find normed coverage column in header line:\n" + l
                    sys.exit(-1)
                if gcCol == -1:
                    print "Could not find gc corrected coverage column in header line:\n" + l
                    sys.exit(-1)

                # record that we are past this column header line
                intoData = True
                continue
                
            # if a regular data line ...
            if intoData:

                # if we only want one chr, and this isn't it, skip this line
                if plotChr != '' and lw[0] != plotChr:
                    continue

                
                oset=offsets[lw[0]]
                midpoint = int( ( int(lw[1]) + int(lw[2]) ) / 2 )
                if plotChr!='':
                    oset=0

                # if not a no-call, get called level
                if lw[callCol] != "N":
                    lev = 0
                    if (tumor):
                        lev = levelmap[lw[callCol]]
                    else:
                        lev = levels[int(lw[callCol])]

                    calls.append(  ( oset+midpoint, lev ) )
                    igvfile.write( "%s\t%d\t%d\tCNV_%d\t%s\t%s\t%f\n" % ( lw[0],int(lw[1]),int(lw[2])-1,n,lw[gcCol],lw[normCol],lev ) )
                # otherwise, use a dummy value
                else:
                    calls.append(  ( oset+int(lw[1]), -10 ) )
                    igvfile.write( "%s\t%d\t%d\tCNV_%d\t%s\t%s\t%f\n" % ( lw[0],int(lw[1]), int(lw[2])-1,n,lw[gcCol],lw[normCol],-10 ) )

                # core plotting data ...
                if lw[normCol] != 'N':
                    normed.append( (oset+midpoint,float(lw[normCol])) )
                else:
                    normed.append( (oset+midpoint,-10) ) 
                if lw[gcCol] != 'N':
                    gccvg.append( (oset+midpoint,float(lw[gcCol])) )
                else:
                    gccvg.append( (oset+midpoint,-10) )

            n += 1
        f_calls.close()

    calls.sort(loccmp)
    normed.sort(loccmp)
    gccvg.sort(loccmp)
    
    callsoffset = []

    callsval = []
    for call in calls:
        callsoffset.append(call[0])
        callsval.append(call[1])

    cvgoffset = []
    cvgval = []

    # if we already have data, store in its final location
    if gccvg:
        if useNorm:
            for cov in normed:
                cvgoffset.append(cov[0])
                cvgval.append(cov[1])
        else:
            for cov in gccvg:
                cvgoffset.append(cov[0])
                cvgval.append(cov[1])
    # else we didn't read the smoothed coverage data, we need to get data from smoothed coverage files
    else:
        cvg = []
        cvgfiles=glob(smoothdir + '/*tsv.gz')
        zipped = 1
        if not cvgfiles:
            cvgfiles=glob(smoothdir + '/*tsv')
            zipped = 0
        for c in cvgfiles:
            print "loading:", c
            if zipped:
                cvgfile = GzipFile(c,'r')
            else:
                cvgfile = open(c,'r')
            header = cvgfile.readline()
            if header[0] != '>' :
                print "ERROR: header line not found in " + c
                sys.exit(-1)
            
            w = header.rstrip().split('\t')
            if w[0] != ">chr" or w[1] != "begin" or w[2] != "end":
                print "ERROR: misformed header for " + c
                sys.exit(-1)

            smoothedGCcol = -1
            n = -1
            for word in w:
                n += 1
                if word == "gcCorrectedCoverage":
                    smoothedGCcol = n
                    break
            if smoothedGCcol == -1:
                print "ERROR: could not find gcCorrectedCoverage in header for " + c
                sys.exit(-1)
                
            for l in cvgfile:
                lw = l.split()
                if plotChr != '' and lw[0] != plotChr:
                    continue
                oset=offsets[lw[0]]
                if plotChr!='':
                    oset=0
                midpoint = int((int(lw[1])+int(lw[2]))/2)
                cvg.append ( ( oset + midpoint, float(lw[smoothedGCcol]) ) )
            cvgfile.close()

        cvg.sort(loccmp)

        for cov in cvg:
            cvgoffset.append(cov[0])
            cvgval.append(cov[1])
        


    if not cvgoffset:
        print "ERROR: no data in the input file(s)."
        fig = P.figure(figsize=(10, 5))
        fig.suptitle("ERROR: no data in the input file(s).")
        fig.savefig(outfilebase+'.png')
        P.close(fig)
        return

    finalcvg = []
    finalcall = []
    finalloc = []
    if k < 20:
        numSame = 0
        numSkipped = 0
        prevlvl=-1
        I = len(cvgoffset)
        if I != len(cvgval) or (len(callsval) > 0 and (I != len(callsval) or I != len(callsoffset))):
            print "ERROR: List lengths not equal"
            sys.exit(-1)
        for i in range(0,I):
            if len(callsval) > 0 and callsval[i] != prevlvl:
                numSame = 0
            if len(callsval) > 0:
                prevlvl = callsval[i]
            if numSame > 10 and numSame != 10 * int (numSame/10):
                numSame += 1
                numSkipped += 1
                continue
            numSame += 1
            finalcvg.append(cvgval[i])
            finalloc.append(cvgoffset[i])
            if len(callsval) > 0:
                finalcall.append(callsval[i])
        print "final numskipped = %d\n" % numSkipped
    else:
        finalcvg = cvgval
        finalcall = callsval
        finalloc = cvgoffset

    fig = P.figure(figsize=(10, 5))
    fig.suptitle("%s genome coverage and called coverage levels" % asmid )

    xlabel = 'genome position (+10M before each chr)'
    if plotChr != '':
        xlabel = 'position in ' + plotChr
    ylabel = 'coverage'

    ax = fig.add_subplot(1,1,1)
    ax.grid(alpha=0.2,linewidth=1)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    line, = ax.plot(chrlinesy, xdata = chrlinesx, color='black')

    lines = []
    point, = ax.plot(finalcvg, 'ro', xdata = finalloc, markersize=2,markeredgewidth=0,color='red')
    lines.append(point)

    if len(finalcall) != 0 and not skipCalls:
        point, = ax.plot(finalcall, 'ro', xdata = finalloc, markersize=2,markeredgewidth=0,color='blue')
        lines.append(point)

    if plotChr == '':
        prev = 0
        prevname=''
        for off in offsetlist:
            if prev > 0:
                ax.annotate(prevname[3:], xy=((prev+off[0])/2-20000000,150), fontsize=8 )
            prev=off[0]
            prevname=off[1]

        ax.annotate(prevname[3:],xy=((prev+cvgoffset[-1])/2-20000000,150), fontsize=8 )

    if plotChr == '':
        ax.set_ylim(-11,150)
        ax.set_xlim(0,4000000000)
    else:
        ax.set_ylim(-11,150)
        chrBeg=0
        chrEnd=5000000000
        for k in offsets.keys():
            if(offsets[k]>offsets[plotChr] and offsets[k]-offsets[plotChr] < chrEnd):
                chrEnd=offsets[k]-offsets[plotChr]
        ax.set_xlim(chrBeg,chrEnd)

    lineNames = [ 'coverage' ]
    if len(finalcall)>0 and not skipCalls:
        lineNames += ['as called']

    leg = ax.legend( lines, lineNames, loc='upper right', labelspacing = 0.2)
    leg.get_frame().set_fill(False)
    for t in leg.get_texts():
        t.set_fontsize('small')    # the legend text fontsize
    for l in leg.get_lines():
        l.set_linewidth(1.5)  # the legend line width

    fig.savefig(outfilebase+'.png')
    P.close(fig)




def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(width='',offsets='',smoothdir='',calldetails='',levelsFile='',outfilebase='',asmid='',tumor=False,useNorm=False, skipCalls=False,plotChr='')
    parser.add_option('--window-width', dest='width',
                      help='width of sliding window averaging')
    parser.add_option('--offsets', dest='offsets',
                      help='file containing starting positions of chromosomes')
    parser.add_option('--smooth-dir', dest='smoothdir',
                      help='directory containing smoothed coverage files')
    parser.add_option('--call-details', dest='calldetails',
                      help='file containing CNV Details')
    parser.add_option('--levels-file', dest='levelsFile',
                      help='file containing final coverage levels for HMM states')
    parser.add_option('-o', '--outfile-base', dest='outfilebase',
                      help='prefix for intermediate and output files')
    parser.add_option('-t', '--tumor', dest='tumor', action='store_true',
                      help='tumor file format for details file')
    parser.add_option('-n', '--use-normed', dest='useNorm', action='store_true',
                      help='plot normalized coverage rather than just GC corrected')
    parser.add_option('-s', '--skip-calls', dest='skipCalls', action='store_true',
                      help='do not plot calls')
    parser.add_option('-p', '--plot-chr', dest='plotChr',
                      help='plot only given chromosome')
    parser.add_option('--asm-id', dest='asmid',
                      help='assembly ID')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.width:
        parser.error('window-width specification required')
    if not options.offsets:
        parser.error('offsets specification required')
    if not options.smoothdir and not options.calldetails:
        parser.error('source of coverage data required: smooth-dir or calldetails')
    if options.useNorm and not options.calldetails:
        parser.error('call-details required if use-normed specified')
    if not options.outfilebase:
        parser.error('outfile-base specification required')
    if not options.asmid:
        parser.error('asm-id specification required')

    cnv_plot(options.width,options.offsets,options.smoothdir,options.calldetails,options.levelsFile,options.outfilebase, options.asmid,options.tumor,options.useNorm,options.skipCalls,options.plotChr)



if __name__ == '__main__':
    main()

