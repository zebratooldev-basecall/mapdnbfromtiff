#!/bin/tcsh -efx

cd $1

set n=`zcat *tsv.gz | grep -v '>' | wc -l`
set first=`echo *tsv.gz | awk '{print $1}'`
set wgtSumCol=`zcat $first | awk 'NR==1{ws=-1;for(i=1;i<=NF;i++)if($i=="weightSumCoverage"){ws=i;break};print ws}'`

if ( $wgtSumCol == "-1" ) then
   echo Could not find weightSumCoverage header column
   exit -1
endif

set median=`zcat  *tsv.gz | grep -v '>' | awk -v wsc=$wgtSumCol '{print $wsc}' | sort -n | awk -v n=$n 'NR==int(n/2){print}'`

foreach f (*tsv.gz)
  set chr=`echo $f | awk -F_  '{print $1}'`
  zcat $f | grep -v '>' | awk -v m=$median -v wsc=$wgtSumCol '$7>=0{print $2,$wsc/m*2}'  > ${chr}.profile
end
