import os

from driverlib import Error

class GbiContig:
    def __init__(self,fields):
        self.contig = int(fields[0])
        self.length = int(fields[1])
        self.circ = False
        if fields[2] == "no":
            self.circ = False
        elif fields[2] == "yes":
            self.circ = True
        else:
            raise Error("Failed to parse gbi output")
        self.offset = int(fields[3])
        self.superid = int(fields[4])
        self.supername = fields[5]
        if self.supername.startswith("'"):
            self.supername = self.supername[1:]
        if self.supername.endswith("'"):
            self.supername = self.supername[:-1]

class GbiFile(object):
    def __init__(self, filename):
        self.contigs = []
        self.filename = filename
        file = os.popen('gbi -l "%s"' % filename)
        try:
            # Second line should be GBI header:
            line = file.next() and file.next()
            stdGbiHeader = ['id', 'length', 'circ', 'offset', 'origId', 'name']
            if line.strip().split() != stdGbiHeader:
                raise Error("Unexpected header in GBI input '%s': %s" % (filename, line))

            # Read contig info
            for line in file:
                prefields = line.strip().split("'")
                if len(prefields) != 3:
                    raise Error("Wrong number of pre-fields in GBI: %d; line: %s; file: %s" % \
                        (len(prefields), prefields, filename))
                fields = prefields[0].strip().split() + [ prefields[1] ]
                if len(fields) != 6:
                    raise Error("Wrong number of fields in GBI: %d; line: %s; file: %s" % \
                        (len(prefields), fields, filename))

                self.contigs.append(GbiContig(fields))
        finally:
            file.close()

    def listChromosomes(self):
        result = []
        for contig in self.contigs:
            if contig.supername not in result:
                result.append(contig.supername)
        return result

    def superToContig(self, chr, pos):
        for contig in self.contigs:
            if ( contig.supername == chr and
                 contig.offset <= pos and
                 pos <= contig.offset+contig.length ):
                return (contig.contig, pos-contig.offset)
        raise Error("no contig position for %s,%d" % (chr,pos))

    def superContigs(self):
        result = []
        for contig in self.contigs:
            if contig.supername not in result:
                result.append(contig.supername)
        return result
