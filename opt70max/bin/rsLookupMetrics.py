#! /usr/bin/python

import sys
import os
import csv

from datetime import datetime, date, time

######################################################################
# Metrics
######################################################################

class Metrics:
    def __init__(self,DNB_Len,Num_RS_Codes):
        
        self.DNB_Len = DNB_Len                                  # number of positions sequenced in DNB
        self.Num_RS_Codes = Num_RS_Codes                        # number of total Reed Solomon codes in DNB
        self.RS_Code_Len = 10                                   # length of Reed Solomon code
        self.numCallRates = 20                                  # number of callrates at which to calculate accuracy
        self.criticalCutoffLST = [float(0.5),float(0.6),float(0.7),float(0.8),float(0.9)]                  # list of Callrate critical cutoffs
        self.criticalCutoffDCT = {}                             # dict with accuracy data for each cutoff
        for i in self.criticalCutoffLST:
            self.criticalCutoffDCT[i] = []
        
        # total and per position confusion matracies at various cutoffs
        self.obsBaseLST = ['A','T','G','C']
        self.expBaseLST = ['A','T','G','C']
        self.confMatrixCutoffLST = [float(0.1),float(0.9)]
        self.totalConfMatrixDCT = {}
        self.confMatrixPerPosDCT = {}
        for cutoff in self.confMatrixCutoffLST:
            self.totalConfMatrixDCT[cutoff] = ConfusionMatrix(self.expBaseLST,self.obsBaseLST)
            self.confMatrixPerPosDCT[cutoff] = []
            for i in range(0,self.DNB_Len):
                self.confMatrixPerPosDCT[cutoff].append(ConfusionMatrix(self.expBaseLST,self.obsBaseLST))
            
        # data for full DNB counts
        self.totalAltDNBs = 0                                   # total number of observed RS codes that are not part of the 20-mer set
        self.totalVerticalDuplicateDNBs = 0                     # total vertical duplicates
        self.totalHorizontalDuplicateDNBs = 0                   # total horizontal duplicates
        self.totalPartialMappedDNBs = 0                         # total number of DNBs where some of the segments are good data but don't map to RS codes
        self.totalPartialCalledDNBs = 0                         # total number of DNBs where some but not all of the segments have nocalls
        self.totalDNBs = 0                                      # total number of DNBs in lane
        self.totalRawMappedDNBs = 0                             # total number of DNBs where all segments have raw mappings to RS codes
        self.totalCorrectedDNBs = 0                             # total number of DNBs where all segments have either error corrected or raw mappings to RS codes
        self.totalNoCallDNBs = 0                                # total number of DNBs with all segments having >1 no-calls
        self.totalUnmappedDNBs = 0                              # total number of DNBs with all segments unmapped to RS codes
        
        # count data per 10mer
        self.totalRawMappedDNBsLST = [0] * self.Num_RS_Codes       # total number of observed DNBs with raw mappings for each RS code
        self.totalCorrectedDNBsLST = [0] * self.Num_RS_Codes       # total number of observed DNBs with error corrected matching for each RS codes
        self.totalNoCallDNBsLST = [0] * self.Num_RS_Codes          # total number of DNBs with > 1 no-calls for each RS code
        self.totalUnmappedDNBsLST = [0] * self.Num_RS_Codes        # total number of unmapped DNBs for each RS code
        
        # data for posistional DNB raw and corrected matches
        self.fiveMerNoCallCountDCT = {}
        self.obsRawCodeCountPerPos = [0] * self.DNB_Len         # mapped counts for each position without error correction
        self.correctedCodeCountPerPos = [0] * self.DNB_Len      # mapped counts for each position with error correction
        self.aveScoreDNBMappedDCT = {}                          # DNB counts based on mapping type, position, and score
        self.aveScoreDNBMappedDCT['total'] = {}
        self.aveScoreDNBMappedDCT['rawmapped'] = {}
        self.aveScoreDNBMappedDCT['corrected'] = {}
        for i in range(0,self.DNB_Len):
            self.aveScoreDNBMappedDCT['total'][i] = {}          # total number of matches per score excluding no-calls & non-mapping reads
            self.aveScoreDNBMappedDCT['rawmapped'][i] = {}
            self.aveScoreDNBMappedDCT['corrected'][i] = {}
        self.sortedScoreLST = []
        
    def incrementTotalDNBs(self,count):
        self.totalDNBs += count
        
    def incrementTotalNoCallDNBs(self, count):
        self.totalNoCallDNBs += count
        
    def incrementTotalAltDNBs(self, count):
        self.totalAltDNBs += count
        
    def incrementTotalPartialMappedDNBs(self, count):
        self.totalPartialMappedDNBs += count
        
    def incrementTotalPartialCalledDNBs(self, count):
        self.totalPartialCalledDNBs += count
        
    def incrementUnmappedDNBs(self,count):
        self.totalUnmappedDNBs += count
        
    def incrementTotalRawMappedDNBs(self,count):
        self.totalRawMappedDNBs += count
        
    def incrementTotalCorrectedDNBs(self,count):
        self.totalCorrectedDNBs += count
        
    def incrementTotalNoCallDNBSegment(self,count,RS_Code_Index):
        self.totalNoCallDNBsLST[RS_Code_Index] += count
        
    def incrementUnmappedDNBSegment(self,count,RS_Code_Index):
        self.totalUnmappedDNBsLST[RS_Code_Index] += count
        
    def incrementTotalRawMappedDNBSegment(self,count,RS_Code_Index):
        self.totalRawMappedDNBsLST[RS_Code_Index] += count
        
    def incrementTotalCorrectedDNBSegment(self,count,RS_Code_Index):
        self.totalCorrectedDNBsLST[RS_Code_Index] += count
        
    def incrementFiveMerNoCallCount(self,count,pos):
        if pos in self.fiveMerNoCallCountDCT.keys():
            self.fiveMerNoCallCountDCT[pos] += count
        else:
            self.fiveMerNoCallCountDCT[pos] = count
    
    def printHeader(self,FH,comment):
        FH.write("SW Version,")
        FH.write("Cycle Set,")
        FH.write("Slide,")
        FH.write("Lane,")
        FH.write("Field,")
        FH.write("Row,")
        FH.write("Col,")
        FH.write("Total,")
        FH.write("%No Called,")
        FH.write("%Unmapped,")
        FH.write("%Partial Called,")
        FH.write("%Partial Mapped,")
        FH.write("%Mapped Raw,")
        FH.write("%Mapped With Error Correction,")
        FH.write("%Raw Error Per Pos,")
        FH.write("Ave EE,")

        for cutoff in self.criticalCutoffLST:
            FH.write(str(cutoff) + "critical ave %error,")
            
        # 10-mer related metrics
        for i in range(0,self.Num_RS_Codes):
            FH.write("%No Called 10mer" + str(i+1) + ",")
            FH.write("%Unmapped 10mer" + str(i+1) + ",")
            FH.write("%Raw Mapped 10mer" + str(i+1) + ",")
            FH.write("%Total Mapped 10mer" + str(i+1) + ",")
            FH.write("%Raw Error 10mer" + str(i+1) + ",")
            FH.write("Ave EE 10mer" + str(i+1) + ",")
            for cutoff in self.criticalCutoffLST:
                FH.write(str(cutoff) + "critical ave %error 10mer" + str(i+1) + ",")
            
        # average call rate vs accuracy data
        for i in range(0,self.numCallRates):
            callRate = float(5*(i+1))/100.0
            FH.write("Callrate " + str(callRate) + "-Accuracy,")
        
        rawErrorStr = ''
        eeStr = ''
        callRateStr = ''
        # per position  raw error rate and effective error rate
        for i in range(0,self.DNB_Len):
            callRateStr1 = ''
            accuracyStr = ''
            index = self._convertTo80bp(i+1)
            for cr in range(0,self.numCallRates):
                callRate = float(5*(cr+1))/100.0
                accuracyStr += "DP" + str(callRate)+"AC pos"+str(index)+","
                callRateStr1 += "DP" + str(callRate)+"CR pos"+str(index)+","
                
            areaStr = ''
            for i in range(0,len(self.criticalCutoffLST)-1):
                areaStr += "Area-"+str(self.criticalCutoffLST[i])+"-"+str(self.criticalCutoffLST[i+1])+"pos "+str(index)+","
                
            callRateStr += callRateStr1 + accuracyStr+areaStr
            rawErrorStr += "%Raw Error pos"+str(index)+","
            eeStr += "EE pos"+str(index)+","
        
        FH.write(rawErrorStr + eeStr)
        
        # per position critical error rates at all cutoffs
        for cutoff in self.criticalCutoffLST:
            for i in range(0,self.DNB_Len):
                index = self._convertTo80bp(i+1)   
                FH.write(str(cutoff)+"critical %error pos"+str(index)+",")
        
        # per position call rate vs accuracy data
        FH.write(callRateStr)
        
        # per position confusion matrix
        for cutoff in self.confMatrixCutoffLST:
            for i in range(0,self.DNB_Len):
                index = self._convertTo80bp(i+1)
                self.confMatrixPerPosDCT[cutoff][i].printConfusionMatrixHeader(FH,str(cutoff) + ' ' + str(index))
                
            
            # total confusion matrix
            self.totalConfMatrixDCT[cutoff].printConfusionMatrixHeader(FH,str(cutoff) + ' ' + 'Total')
        
        FH.write(comment+"\n")
            
    def printMetrics(self,FH,swVersion,slide,lane,field,row,col,cycleStr,DBFH,comment):
        percNoCalled = float(self.totalNoCallDNBs) / float(self.totalDNBs) * 100.0
        numCalled = self.totalDNBs - self.totalNoCallDNBs
        
        percUnmapped = "N/A"
        percPartialCall = "N/A"
        percPartialMap = "N/A"
        percRawMap = "N/A"
        percCorrected = "N/A"
        percHorizDup = "N/A"
        percVertDup = "N/A"
        percRawError = "N/A"
        if numCalled > 0:
            percUnmapped = float(self.totalUnmappedDNBs) / float(numCalled) * 100.0
            percRawMap = float(self.totalRawMappedDNBs) / float(numCalled) * 100.0
            percCorrected = float(self.totalCorrectedDNBs) / float(numCalled) * 100.0
            percPartialMap = float(self.totalPartialMappedDNBs) / float(numCalled) * 100.0
            percPartialCall = float(self.totalPartialCalledDNBs) / float(numCalled) * 100.0
            dupSearchSpace = self.totalRawMappedDNBs + self.totalCorrectedDNBs + self.totalPartialMappedDNBs + self.totalPartialCalledDNBs
            if dupSearchSpace > 0:
                percHorizDup = float(self.totalHorizontalDuplicateDNBs) / float(dupSearchSpace) * 100.0
                percVertDup = float(self.totalVerticalDuplicateDNBs) / float(dupSearchSpace) * 100.0

        FH.write(str(swVersion)+",")
        FH.write(str(cycleStr)+",")
        FH.write(str(slide)+",")
        FH.write(str(lane)+",")
        FH.write(str(field)+",")
        FH.write(str(row)+",")
        FH.write(str(col)+",")
        FH.write(str(self.totalDNBs)+",")
        FH.write(str(percNoCalled)+",")
        FH.write(str(percUnmapped)+",")
        FH.write(str(percPartialCall)+",")
        FH.write(str(percPartialMap)+",")
        FH.write(str(percRawMap)+",")
        FH.write(str(percCorrected)+",")
        
        self._printDBMetrics(DBFH,field)
        
        self._printCallRateAccuracyErrorRates(FH,DBFH)
        
        # print confusion matrix data for each score cutoff
        for cutoff in self.confMatrixCutoffLST:
            for i in range(0,self.DNB_Len):
                self.confMatrixPerPosDCT[cutoff][i].printConfusionMatrix(FH)
            self.totalConfMatrixDCT[cutoff].printConfusionMatrix(FH)
        
        FH.write(comment+"\n")
        
    def _printCallRateAccuracyErrorRates(self,FH,DBFH):
        callrateLST = [0] * self.numCallRates
        accuracyLST = [0] * self.numCallRates
        aveCallRatePerPerc = [0] * self.numCallRates
        aveAccuracyPerPerc = [0] * self.numCallRates
        effectiveErrorLST = [0] * self.DNB_Len
        rawErrorLST = [0] * self.DNB_Len
        
        # calculate accuracy based on raw and corrected mapped read counts
        eeStr = ''
        segmentStr = ''
        posRawErrorStr = ''
        posAccuracyStr = ''
        for posIndex in range(0,self.DNB_Len):
            dnbIndex = posIndex+1
            tenMerNum = (self._convertTo80bp(dnbIndex) - 1) / 10
            
            # calculate callrate vs accuracy
            for percIndex in range (0,self.numCallRates):
                callRate = 5*(percIndex+1)
                (obsRawCodeCount, correctedCodeCount, totalCodeCount) = self._getMappedCodeCount(posIndex,callRate)
                if posIndex in self.fiveMerNoCallCountDCT.keys():
                    mappedCount = self.totalDNBs-self.fiveMerNoCallCountDCT[posIndex]
                else:
                    mappedCount = self.totalDNBs-self.totalNoCallDNBsLST[tenMerNum]
                    
                if obsRawCodeCount + correctedCodeCount > 0 and mappedCount > 0:
                    
                    callrateLST[percIndex] = (float(obsRawCodeCount) + float(correctedCodeCount)) / float(mappedCount)
                    accuracyLST[percIndex] = float(obsRawCodeCount) / (float(obsRawCodeCount) + float(correctedCodeCount))
                    aveAccuracyPerPerc[percIndex] += accuracyLST[percIndex]
                    aveCallRatePerPerc[percIndex] += callrateLST[percIndex]
                else:
                    callrateLST[percIndex] = 'N/A'
                    accuracyLST[percIndex] = 'N/A'
            
            # print positional error rates
            effectiveErrorLST[posIndex] = self._calculateEffectiveError(callrateLST,accuracyLST)
            eeStr += str(effectiveErrorLST[posIndex])+","
            rawErrorLST[posIndex] = self._printErrorData(DBFH,posIndex,effectiveErrorLST[posIndex])
            posRawErrorStr += str(rawErrorLST[posIndex])+","
            
            # store critical cutoff accuracy measures for each position
            for critCutoff in self.criticalCutoffLST:
                index = int(critCutoff)/5 - 1
                if callrateLST[percIndex] == 'N/A':
                    self.criticalCutoffDCT[critCutoff].insert(posIndex,'N/A')
                else:
                    (criticalCutoffCR,inserIndex) = self._calcCriticalCutoffCR(accuracyLST,callrateLST,critCutoff)
                    if criticalCutoffCR == 'N/A':
                        self.criticalCutoffDCT[critCutoff].insert(posIndex,'N/A')
                    else:
                        self.criticalCutoffDCT[critCutoff].insert(posIndex,(1-criticalCutoffCR)*100)
            
            # print positional accuracy data
            posAccuracyStr += self._printPosAccuracyData(DBFH,accuracyLST,callrateLST,posIndex)
            
            # calculate metrics per 10-mer
            if dnbIndex == self.RS_Code_Len/2 or dnbIndex == self.DNB_Len:
                start = dnbIndex-(self.RS_Code_Len/2)
                stop = dnbIndex
                if sum(self.correctedCodeCountPerPos[start:stop]) + sum(self.obsRawCodeCountPerPos[start:stop]) > 0:
                    posRawErrorRate = float(sum(self.correctedCodeCountPerPos[start:stop])) / float(sum(self.obsRawCodeCountPerPos[start:stop]) + sum(self.correctedCodeCountPerPos[start:stop])) * 100
                else:
                    posRawErrorRate = "N/A"
                
                aveEE = self._calculateAve(effectiveErrorLST[start:stop])
                segmentStr += self._printTenMerData(tenMerNum,posRawErrorRate,aveEE,DBFH)
                for cutoff in self.criticalCutoffLST:
                    segmentStr += self._calculateAve(self.criticalCutoffDCT[cutoff][start:stop])+","
            elif dnbIndex % self.RS_Code_Len == 5:
                start = dnbIndex-self.RS_Code_Len
                stop = dnbIndex
                if sum(self.correctedCodeCountPerPos[start:stop]) + sum(self.obsRawCodeCountPerPos[start:stop]) > 0:
                    posRawErrorRate = float(sum(self.correctedCodeCountPerPos[start:stop])) / float(sum(self.obsRawCodeCountPerPos[start:stop]) + sum(self.correctedCodeCountPerPos[start:stop])) * 100.0
                else:
                    posRawErrorRate = "N/A"
                
                aveEE = self._calculateAve(effectiveErrorLST[start:stop])
                segmentStr += self._printTenMerData(tenMerNum,posRawErrorRate,aveEE,DBFH)
                for cutoff in self.criticalCutoffLST:
                    segmentStr += self._calculateAve(self.criticalCutoffDCT[cutoff][start:stop])+","
        
        # calculate average raw error rate for all positions
        rawErrorLST=filter ( lambda b: b != 'N/A', rawErrorLST)
        aveRawEStr = self._calculateAve(rawErrorLST) + ","
        
        # calculate average effective error rate for all positions
        aveEEStr = self._calculateAve(effectiveErrorLST) + ","
        
        # generate all critical cutoffs average data string 
        criticalCutoffStr = ''
        aveCriticalCutoffStr = ''
        for cutoff in self.criticalCutoffLST:
            criticalCutoffStr += ",".join(["%s" % a for a in self.criticalCutoffDCT[cutoff]]) + ","
            aveCriticalCutoffStr += self._calculateAve(self.criticalCutoffDCT[cutoff]) + ","                
            
        # generate callrate vs accuracy data string 
        accuracyStr = ''
        for index in range(0,self.numCallRates):
            percentile = 5*(index+1)
            ave = float(aveAccuracyPerPerc[index]) / float(self.DNB_Len)
            if ave == 0:
                ave = 'N/A'
            accuracyStr += str(ave)+","
        
        # print all data
        FH.write(aveRawEStr + aveEEStr + aveCriticalCutoffStr + segmentStr + accuracyStr + posRawErrorStr + eeStr + criticalCutoffStr + posAccuracyStr)
        
    def _calculateAve(self,dataLST):
        dataLST=filter ( lambda b: b != 'N/A', dataLST)
        if len(dataLST) == 0:
            aveEffectiveError = 'N/A'
        else:
            aveEffectiveError = float(sum(dataLST)) / len(dataLST)
            if aveEffectiveError == 1:
                aveEffectiveError = 'N/A'
            
        return str(aveEffectiveError)
    
    def _getMappedCodeCount(self,pos,callRate):
        # descending ordered scores list
        sortedScoreLST = sorted(self.aveScoreDNBMappedDCT['total'][pos])
        sortedScoreLST.reverse()
        
        # total calls for position and lower and upper bound of total calls to use for percentile
        totalDataDNBs = sum([i for i in self.aveScoreDNBMappedDCT['total'][pos].values()])
        maxDNBsToUse = float(totalDataDNBs) * float(callRate) / 100.0

        totalDNBs = 0
        rawMappedDNBs = 0
        correctedDNBs = 0
        
        # find set of data that correspond to the given percentile
        for i in range(0,len(sortedScoreLST)):
            if totalDNBs + self.aveScoreDNBMappedDCT['total'][pos][sortedScoreLST[i]] < maxDNBsToUse:
                totalDNBs += self.aveScoreDNBMappedDCT['total'][pos][sortedScoreLST[i]]
                if sortedScoreLST[i] in self.aveScoreDNBMappedDCT['rawmapped'][pos]:
                    rawMappedDNBs += self.aveScoreDNBMappedDCT['rawmapped'][pos][sortedScoreLST[i]]
                    
                if sortedScoreLST[i] in self.aveScoreDNBMappedDCT['corrected'][pos]:
                    correctedDNBs += self.aveScoreDNBMappedDCT['corrected'][pos][sortedScoreLST[i]]
            else:
                DNBsNeeded = maxDNBsToUse - totalDNBs
                totalDNBs += DNBsNeeded
                percOfCurrentBin = float(DNBsNeeded) / float(self.aveScoreDNBMappedDCT['total'][pos][sortedScoreLST[i]])
                if sortedScoreLST[i] in self.aveScoreDNBMappedDCT['rawmapped'][pos]:
                    rawMappedDNBs += int((self.aveScoreDNBMappedDCT['rawmapped'][pos][sortedScoreLST[i]] * percOfCurrentBin))
                
                if sortedScoreLST[i] in self.aveScoreDNBMappedDCT['corrected'][pos]:
                    correctedDNBs += int((self.aveScoreDNBMappedDCT['corrected'][pos][sortedScoreLST[i]] * percOfCurrentBin))
                break
                
        return (rawMappedDNBs, correctedDNBs, totalDNBs)
    
    def _calculateEffectiveError(self,callrateLST,accuracyLST):
        minCallRate = 5*(0+1)
        maxCallRate = 5*(self.numCallRates+1)
        area = self._calculateAreaUnderCurve(callrateLST,accuracyLST)
        callrateLST=filter ( lambda b: b != 'N/A', callrateLST)
        if len(callrateLST) > 1:
            callrateRange = callrateLST[len(callrateLST)-1] - callrateLST[0]
            return 100* (1-(area/callrateRange))
        else:
            return 'N/A'
    
    def _calculateAreaUnderCurve(self,callrateLST,accuracyLST):
        area = 0
        if not len(accuracyLST) == len(callrateLST):
            print "ERROR: the accuracy and callrate lists must have the same amount of data\n"
            print "Accuracy: ",
            print accuracyLST
            print "CallRate: ",
            print callrateLST
            sys.exit()
        
        for index in range(0,len(accuracyLST)-1):
            if accuracyLST[index+1] == 'N/A' or accuracyLST[index] == 'N/A':
                continue
                
            area += (callrateLST[index+1] - callrateLST[index]) * accuracyLST[index]
        
        return area
    
    def _calcCriticalCutoffCR(self,accuracyLST,callrateLST,critCutoff):
        for i in range(0,len(callrateLST)-1):
            if callrateLST[i] == 'N/A' or callrateLST[i+1] == 'N/A':
                continue
                
            if float(callrateLST[i]) < float(critCutoff) and float(callrateLST[i+1]) > float(critCutoff):
                x1 = callrateLST[i]
                x2 = callrateLST[i+1]
                y1 = accuracyLST[i]
                y2 = accuracyLST[i+1]
                if y1 == y2:
                    return (y1,i+1)
                    
                m = (y1-y2)/(x1-x2)
                b = y1-(m*x1)
                return (m*critCutoff+b,i+1)
        
        return ('N/A',-1)
    
    ### PRINT REPORTING AND DATABASE LOADING INFORMATION
    def _printDBMetrics(self,DBFH,field):
        
        # print RSMetrics Data
        DBFH.write("RSMetrics,")
        DBFH.write(str(field)+",")
        DBFH.write(str(self.totalDNBs)+",")
        DBFH.write(str(self.totalNoCallDNBs)+",")
        DBFH.write(str(self.totalUnmappedDNBs)+",")
        DBFH.write(str(self.totalPartialCalledDNBs)+",")
        DBFH.write(str(self.totalPartialMappedDNBs)+",")
        DBFH.write(str(self.totalRawMappedDNBs)+",")
        DBFH.write(str(self.totalCorrectedDNBs)+",")
        DBFH.write(str(self.totalHorizontalDuplicateDNBs) + ",")
        DBFH.write(str(self.totalVerticalDuplicateDNBs)+"\n")
        
    def _printTenMerData(self,index,rawError,EE,DBFH):
        numCalled = self.totalDNBs - self.totalNoCallDNBsLST[index]
        percNoCall = float(self.totalNoCallDNBsLST[index]) / float(self.totalDNBs) * 100.0
        if numCalled == 0:
            percUnmapped = "N/A"
            percRawMapped = "N/A"
            percTotalMapped = "N/A"
            posRawErrorRate = "N/A"
        else:
            percUnmapped = float(self.totalUnmappedDNBsLST[index]) / float(numCalled) * 100.0
            percRawMapped = float(self.totalRawMappedDNBsLST[index]) / float(numCalled) * 100.0
            percTotalMapped = float(self.totalCorrectedDNBsLST[index]+self.totalRawMappedDNBsLST[index])/ float(numCalled) * 100.0
        
        DBFH.write("TenMerMetrics,")
        DBFH.write(str(index+1)+",")
        DBFH.write(str(self.totalNoCallDNBsLST[index])+",")
        DBFH.write(str(self.totalUnmappedDNBsLST[index])+",")
        DBFH.write(str(self.totalRawMappedDNBsLST[index])+",")
        DBFH.write(str(self.totalCorrectedDNBsLST[index])+",")
        DBFH.write(str(rawError)+",")
        DBFH.write(str(EE)+"\n")
        
        
        return str(percNoCall)+","+str(percUnmapped)+","+str(percRawMapped)+","+str(percTotalMapped)+","+str(rawError)+","+str(EE)+","
    
    def _printErrorData(self,DBFH,pos,EE):
        DBFH.write("PositionMetrics,")
        DBFH.write(str(self._convertTo80bp(pos+1))+","+str(EE)+",")
        
        # calculate raw error rate for each position
        if self.obsRawCodeCountPerPos[pos] + self.correctedCodeCountPerPos[pos] > 0:
            rawError = float(self.correctedCodeCountPerPos[pos]) / float(self.obsRawCodeCountPerPos[pos] + self.correctedCodeCountPerPos[pos]) * 100.0
            DBFH.write(str(rawError)+",")
            return rawError
        else:
            DBFH.write("N/A,")
            return 'N/A'
        
        
    def _printPosAccuracyData(self,DBFH,accuracyLST,callrateLST,posIndex):
        # calculate area 1 metrics
        areaAccLST = []
        areaCRLST = []
        areaAccLST.extend(accuracyLST)
        areaCRLST.extend(callrateLST)
        
        # add critical cutoff accuracy values
        for cutoff in self.criticalCutoffLST:
            (accuracy,index) = self._calcCriticalCutoffCR(areaAccLST,areaCRLST,cutoff)
            if not index == -1:
                areaAccLST.insert(index,accuracy)
                areaCRLST.insert(index,cutoff)
        
        # find max accuracy bound at 0.5 callrate
        critMaxCRIndex = self._getIndex(areaCRLST, float(0.5))
        
        areaStr = ''
        for i in range(0,len(self.criticalCutoffLST)-1):
            if critMaxCRIndex == -1:
                areaStr += 'N/A,'
                continue
            
            # if call rate boundaries don't exist, don't calculate area
            lowIndex = self._getIndex(areaCRLST,float(self.criticalCutoffLST[i]))
            highIndex = self._getIndex(areaCRLST,float(self.criticalCutoffLST[i+1]))
            if lowIndex == -1 or highIndex == -1:
                areaStr += 'N/A,'
                continue
            
            # if max accuracy within callrate boundaries is > 0.5 critical accuracy don't calculate area
            highAcc = max(areaAccLST[lowIndex:highIndex+1])
            if highAcc > areaAccLST[critMaxCRIndex]:
                areaStr += 'MAX,'
                continue
            
            area = self._calculateAreaUnderCurve(areaCRLST[lowIndex:highIndex+1],areaAccLST[lowIndex:highIndex+1])
            if area == 0:
                areaStr += 'N/A,'
            else:
                areaStr += str((areaAccLST[critMaxCRIndex]*(self.criticalCutoffLST[i+1]-self.criticalCutoffLST[i])) - area)+","
        
        DBFH.write(str(len(self.criticalCutoffLST))+",")
        DBFH.write(",".join(["%s" % a for a in self.criticalCutoffLST])+",")
        DBFH.write(areaStr)
        
        dbAccLST = []
        dbCRLST = []
        dbAccLST.extend(accuracyLST)
        dbCRLST.extend(callrateLST)
        for cutoff in self.criticalCutoffLST:
            if self.criticalCutoffDCT[cutoff][posIndex] == "N/A":
                dbCRLST.insert(0,"N/A")
                dbAccLST.insert(0,self.criticalCutoffDCT[cutoff][posIndex])
            else:
                acc = 1-(self.criticalCutoffDCT[cutoff][posIndex]/100)
                dbCRLST.insert(0,cutoff)
                dbAccLST.insert(0,acc)
                
        DBFH.write(",".join(["%s" % a for a in dbAccLST])+"," + ",".join(["%s" % a for a in dbCRLST]) + "\n")
        return ",".join(["%s" % a for a in callrateLST])+","+",".join(["%s" % a for a in accuracyLST])+","+areaStr
            
    def _convertTo80bp(self,val):
        if val > 5:
            val +=5
        
        if val > 70:
            val +=5
        
        return val
    
    def _getIndex(self,arr,value):
        for i in range(0,len(arr)):
            if arr[i] == value:
                return i
        
        return -1
            
######################################################################
# FieldMetrics
######################################################################

class FieldMetrics(Metrics):
    def __init__(self, DNB_Len, Num_RS_Codes, numRows, numCols):
        Metrics.__init__(self,DNB_Len,Num_RS_Codes)
        self.numRows = numRows
        self.numCols = numCols
        
        # tracking duplicates
        self.verticalDuplicates = [''] * (self.numRows)
        self.horizontalDuplicates = [''] * (self.numRows)
    
    ####  DNB DUPLICATION DATA
    
    def calculateDuplication(self, dnbIndex, seq, fieldIndex, fieldSize):
        dnbFieldIndex = dnbIndex - (fieldIndex / fieldSize)
        self._checkVerticalDuplication(dnbFieldIndex, seq)
        self._checkHorizontalDuplication(dnbFieldIndex, seq)
        
    def setDuplicate(self, dnbIndex, fieldIndex, fieldSize):
        dnbFieldIndex = dnbIndex - (fieldIndex / fieldSize)
        vertIndex = dnbIndex % self.numRows
        self.verticalDuplicates[vertIndex] = ''
        horizIndex = dnbIndex % self.numRows
        self.horizontalDuplicates[horizIndex] = ''

    def _checkVerticalDuplication(self, dnbIndex, code):
        vertIndex = dnbIndex % self.numRows
        if vertIndex < self.numRows-1:
            self.verticalDuplicates[vertIndex] = code
            
        if vertIndex > 0:
            if self.verticalDuplicates[vertIndex-1] == code and not code == '':
                self.totalVerticalDuplicateDNBs += 1
    
    def _checkHorizontalDuplication(self, dnbIndex, code):
        horizIndex = dnbIndex % self.numRows
        compareCode = self.horizontalDuplicates[horizIndex]
        self.horizontalDuplicates[horizIndex] = code        
        if dnbIndex >= self.numCols:
            if compareCode == code and not code == '':
                self.totalHorizontalDuplicateDNBs += 1
    
    ### STATISTICAL COUNTS AND CALCULATIONS
    
    def incrementAveScoreCount(self,scoreLST,obsSeq,expSeq):
        for i in range(0,len(obsSeq)):
            if obsSeq[i] == 'N' or expSeq[i] == 'N':
                continue
                
            currPosScore = scoreLST[i]
            for cutoff in self.confMatrixCutoffLST:
                if int(currPosScore) >= cutoff*200:
                    self.totalConfMatrixDCT[cutoff].incrementObsExpCount(expSeq[i],obsSeq[i],1)
                    self.confMatrixPerPosDCT[cutoff][i].incrementObsExpCount(expSeq[i],obsSeq[i],1)
                
            if currPosScore in self.aveScoreDNBMappedDCT['total'][i]:
                self.aveScoreDNBMappedDCT['total'][i][currPosScore] += 1
            else:
                self.aveScoreDNBMappedDCT['total'][i][currPosScore] = 1
           
            if obsSeq[i] == expSeq[i]:
                self.obsRawCodeCountPerPos[i] += 1
                if currPosScore in self.aveScoreDNBMappedDCT['rawmapped'][i]:
                    self.aveScoreDNBMappedDCT['rawmapped'][i][currPosScore] += 1
                else:
                    self.aveScoreDNBMappedDCT['rawmapped'][i][currPosScore] = 1
            else:
                self.correctedCodeCountPerPos[i] += 1
                if currPosScore in self.aveScoreDNBMappedDCT['corrected'][i]:
                    self.aveScoreDNBMappedDCT['corrected'][i][currPosScore] += 1
                else:
                    self.aveScoreDNBMappedDCT['corrected'][i][currPosScore] = 1
        
        
    ### FIELD ONLY DATABASE INFO
    
    def printDBConfMatrixData(self,DBFH):
        for cutoff in self.confMatrixCutoffLST:
            for i in range(0,self.DNB_Len):
                self.confMatrixPerPosDCT[cutoff][i].printDBConfusionMatrix(DBFH,cutoff,i)
    
######################################################################
# LaneMetrics
######################################################################

class LaneMetrics(Metrics):
    
    def incrementTotalVerticalDuplicateDNBs(self, count):
        self.totalVerticalDuplicateDNBs += count
    
    def incrementTotalHorizontalDuplicateDNBs(self, count):
        self.totalHorizontalDuplicateDNBs += count
        
    def incrementRawCodeCountPerPos(self, pos, count):
        self.obsRawCodeCountPerPos[int(pos)] += count
    
    def incrementCorrectedCodeCountPerPos(self, pos, count):
        self.correctedCodeCountPerPos[int(pos)] += count
        
    # INCREMENT LANE LEVEL DATA FROM FIELD DATA
    def incrementLaneData(self, fieldData):
        self.incrementTotalDNBs(fieldData.totalDNBs)
        self.incrementTotalNoCallDNBs(fieldData.totalNoCallDNBs)
        self.incrementUnmappedDNBs(fieldData.totalUnmappedDNBs)
        self.incrementTotalRawMappedDNBs(fieldData.totalRawMappedDNBs)
        self.incrementTotalCorrectedDNBs(fieldData.totalCorrectedDNBs)
        self.incrementTotalAltDNBs(fieldData.totalAltDNBs)
        self.incrementTotalPartialMappedDNBs(fieldData.totalPartialMappedDNBs)
        self.incrementTotalPartialCalledDNBs(fieldData.totalPartialCalledDNBs)
        self.incrementTotalVerticalDuplicateDNBs(fieldData.totalVerticalDuplicateDNBs)
        self.incrementTotalHorizontalDuplicateDNBs(fieldData.totalHorizontalDuplicateDNBs)
        
        for code in range(0, self.Num_RS_Codes):
            self.incrementTotalRawMappedDNBSegment(fieldData.totalRawMappedDNBsLST[code], code)
            self.incrementTotalCorrectedDNBSegment(fieldData.totalCorrectedDNBsLST[code], code)
            self.incrementTotalNoCallDNBSegment(fieldData.totalNoCallDNBsLST[code], code)
            self.incrementUnmappedDNBSegment(fieldData.totalUnmappedDNBsLST[code], code)
            
        for pos in range(0,len(fieldData.obsRawCodeCountPerPos)):
            self.incrementRawCodeCountPerPos(pos,fieldData.obsRawCodeCountPerPos[pos])
            self.incrementCorrectedCodeCountPerPos(pos,fieldData.correctedCodeCountPerPos[pos])
            for base1 in self.expBaseLST:
                for base2 in self.obsBaseLST:
                    for cutoff in self.confMatrixCutoffLST:
                        self.totalConfMatrixDCT[cutoff].incrementObsExpCount(base1,base2,fieldData.confMatrixPerPosDCT[cutoff][pos].obsExpDCT[base1][base2])
                        self.confMatrixPerPosDCT[cutoff][pos].incrementObsExpCount(base1,base2,fieldData.confMatrixPerPosDCT[cutoff][pos].obsExpDCT[base1][base2])
                    
        for maptype in ('total','rawmapped','corrected'):
            for pos in range(0,len(fieldData.obsRawCodeCountPerPos)):
                scoreLST = fieldData.aveScoreDNBMappedDCT[maptype][pos]
                for score in scoreLST:
                    if score in self.aveScoreDNBMappedDCT[maptype][pos]:
                        self.aveScoreDNBMappedDCT[maptype][pos][score] += fieldData.aveScoreDNBMappedDCT[maptype][pos][score]
                    else:
                        self.aveScoreDNBMappedDCT[maptype][pos][score] = fieldData.aveScoreDNBMappedDCT[maptype][pos][score]
        
        for pos in fieldData.fiveMerNoCallCountDCT.keys():
            self.incrementFiveMerNoCallCount(fieldData.fiveMerNoCallCountDCT[pos],pos)
        
    def printLaneData(self, FH):
        FH.write(str(self.totalDNBs)+",")
        FH.write(str(self.totalNoCallDNBs)+",")
        FH.write(str(self.totalUnmappedDNBs)+",")
        FH.write(str(self.totalRawMappedDNBs)+",")
        FH.write(str(self.totalCorrectedDNBs)+",")
        FH.write(str(self.totalAltDNBs)+",")
        FH.write(str(self.totalPartialMappedDNBs)+",")
        FH.write(str(self.totalPartialCalledDNBs)+",")
        FH.write(str(self.totalVerticalDuplicateDNBs)+",")
        FH.write(str(self.totalHorizontalDuplicateDNBs)+"\n")

        # For each 10-mer print raw mapped, corrected mapped, no-call, and unmapped counts
        for code in range(0,self.Num_RS_Codes):
            FH.write("Code " + str(code) + ",")
            FH.write(str(self.totalRawMappedDNBsLST[code]) + ",")
            FH.write(str(self.totalCorrectedDNBsLST[code]) + ",")
            FH.write(str(self.totalNoCallDNBsLST[code]) + ",")
            FH.write(str(self.totalUnmappedDNBsLST[code]) + "\n")

        # print the following information for each position:
        # Pos #
        # conf matrix
        # best conf matrix
        # raw map count, corrected map count
        # total mapped per score
        # raw mapped per score
        # corrected mapped per score
        for pos in range(0,self.DNB_Len):
            FH.write("Pos " + str(pos) + "\n")
            for cutoff in self.confMatrixCutoffLST:
                self.confMatrixPerPosDCT[cutoff][pos].printConfusionMatrixCounts(FH)
                FH.write("\n")
            
            FH.write(str(self.obsRawCodeCountPerPos[int(pos)])+","+str(self.correctedCodeCountPerPos[int(pos)])+"\n")
            for maptype in ('total','rawmapped','corrected'):
                scoreLST = self.aveScoreDNBMappedDCT[maptype][pos]
                for score in scoreLST:
                    FH.write(str(score)+":"+str(self.aveScoreDNBMappedDCT[maptype][pos][score])+",")
                FH.write("\n")
        
        FH.write("FiveMer Pos Nocalls\n")
        for pos in self.fiveMerNoCallCountDCT.keys():
            FH.write(str(pos)+":"+str(self.fiveMerNoCallCountDCT[pos])+",")
            
        FH.write("\n")
    
    def readLaneData(self,FH):
        # read aggregate dnb data
        lineLST = FH.readline().rstrip().rsplit(",")
        self.incrementTotalDNBs(int(lineLST[0]))
        self.incrementTotalNoCallDNBs(int(lineLST[1]))
        self.incrementUnmappedDNBs(int(lineLST[2]))
        self.incrementTotalRawMappedDNBs(int(lineLST[3]))
        self.incrementTotalCorrectedDNBs(int(lineLST[4]))
        self.incrementTotalAltDNBs(int(lineLST[5]))
        self.incrementTotalPartialMappedDNBs(int(lineLST[6]))
        self.incrementTotalPartialCalledDNBs(int(lineLST[7]))
        self.incrementTotalVerticalDuplicateDNBs(int(lineLST[8]))
        self.incrementTotalHorizontalDuplicateDNBs(int(lineLST[9]))
        
        # read 10-mer info
        for code in range(0,self.Num_RS_Codes):
            lineLST = FH.readline().rstrip().rsplit(",")
            self.incrementTotalRawMappedDNBSegment(int(lineLST[1]), code)
            self.incrementTotalCorrectedDNBSegment(int(lineLST[2]), code)
            self.incrementTotalNoCallDNBSegment(int(lineLST[3]), code)
            self.incrementUnmappedDNBSegment(int(lineLST[4]), code)
        
        # read per position information
        for pos in range(0,self.DNB_Len):
            # pos number
            line = FH.readline()
            
            # conf matrix
            for cutoff in self.confMatrixCutoffLST:
                lineLST = FH.readline().rstrip().rsplit(",")
                for data in lineLST:
                    if data == '':
                        continue
                    (base1, base2, count) = data.split(":")
                    self.confMatrixPerPosDCT[cutoff][pos].incrementObsExpCount(base1,base2,int(count))
                    self.totalConfMatrixDCT[cutoff].incrementObsExpCount(base1,base2,int(count))
                
            # read per position raw and corrected mapped read count
            lineLST = FH.readline().rstrip().rsplit(",")
            self.incrementRawCodeCountPerPos(pos, int(lineLST[0]))
            self.incrementCorrectedCodeCountPerPos(pos, int(lineLST[1]))
            
            # read per position score based mapping counts
            for maptype in ('total','rawmapped','corrected'):
                lineLST = FH.readline().rstrip().rsplit(",")
                for data in lineLST:
                    if data == '':
                        continue
                    (score, count) = data.split(":")
                    if int(score) in self.aveScoreDNBMappedDCT[maptype][pos].keys():
                        self.aveScoreDNBMappedDCT[maptype][pos][int(score)] += int(count)
                    else:
                        self.aveScoreDNBMappedDCT[maptype][pos][int(score)] = int(count)
        
        header = FH.readline()
        line = FH.readline()
        lineLST = line.strip().split(",")
        for data in lineLST:
            if data == '':
                continue
            (pos,nocall) = data.split(":")
            self.incrementFiveMerNoCallCount(int(nocall),int(pos))
            
######################################################################
# LookupTable
######################################################################

class LookupTable:
    def __init__(self, lookupCSV, lookupCSV20bp,DNB_Len):
        self.RS_Code_Len = 10
        self.DNB_Len = DNB_Len
        
        # Load 10-mer lookup table
        self.lookupTableDCT = {}
        inputFH = open(lookupCSV, "r")
        print "Loading " + lookupCSV
        for line in inputFH:
            (key,value) = line.rstrip().split(",")
            self.lookupTableDCT[key.upper()] = value.upper()
        inputFH.close()
        
        # Load 20-mer lookup for full and partial seq
        self.full20BpLookupTableDCT = {}
        self.partial20BpLookupTableDCT = {}
        inputFH = open(lookupCSV20bp, "r")
        print "Loading " + lookupCSV20bp
        for line in inputFH:
            seq10bp = line[0:10]
            self.full20BpLookupTableDCT[seq10bp]= line.rstrip()
            seq10bp = line[10:20]
            self.full20BpLookupTableDCT[seq10bp]= line.rstrip()
        
        inputFH.close()
        
    # search for 20-mer RS code with 10bp
    def seqLookup20bp(self,seq):
        if seq in self.full20BpLookupTableDCT:
            return [seq,self.full20BpLookupTableDCT[seq]]
        else:
            return [seq,'']
    
    # search for RS code with error correction
    def seqLookup(self,seq):
        code = self.lookupTableDCT[seq]
        if code.find('*') > -1:
            return ['','']
        else:
            return self.seqLookup20bp(code)
    
######################################################################
# ConfusionMatrix
######################################################################

class ConfusionMatrix:
    def __init__(self,expLST,obsLST):
        # data for confusion matrix
        self.obsExpDCT = {}
        if not 'total' in obsLST:
            obsLST.append('total')
            
        self.expLST = expLST
        self.obsLST = obsLST
        for expect in expLST:
            self.obsExpDCT[expect] = {}
            for observed in obsLST:
                self.obsExpDCT[expect][observed] = 0
    
    def incrementObsExpCount(self,expStr,obsStr,count):
        if obsStr == 'total':
            return
        
        if obsStr == 'N':
            return
        
        for i in range (0,len(obsStr)):
            self.obsExpDCT[expStr][obsStr] += count
            self.obsExpDCT[expStr]['total'] += count
    
    def printConfusionMatrixHeader(self,FH,pos):
        for base1 in self.expLST:
            for base2 in self.expLST:
                if base2 == 'total':
                    continue
                FH.write(str(pos) + " " + str(base1)+"->"+str(base2)+",")
            
    def printConfusionMatrix(self,FH):
        for base1 in self.expLST:
            for base2 in self.obsLST:
                if base2 == 'total':
                    continue
                    
                if self.obsExpDCT[base1]['total'] == 0:
                    FH.write('N/A,')
                else:
                    percObsExp = float(self.obsExpDCT[base1][base2]) / float(self.obsExpDCT[base1]['total']) * 100.0
                    FH.write(str(percObsExp)+",")

    def printDBConfusionMatrix(self,DBFH,cutoff,pos):
        DBFH.write("RSConfMatrix,"+str(cutoff)+","+str(pos)+",")
        for base1 in self.expLST:
            for base2 in self.obsLST:
                if base2 == 'total':
                    continue
                    
                if self.obsExpDCT[base1]['total'] == 0:
                    DBFH.write('N/A,')
                else:
                    DBFH.write(base1+":"+base2+":"+str(self.obsExpDCT[base1][base2])+",")
                    
        DBFH.write("\n")
                    
    def printConfusionMatrixCounts(self,FH):
        outString = ''
        for base1 in self.expLST:
            for base2 in self.obsLST:
                FH.write(base1+":"+base2+":"+str(self.obsExpDCT[base1][base2])+",")
    
