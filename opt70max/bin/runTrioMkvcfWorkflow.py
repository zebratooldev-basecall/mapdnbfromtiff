#!/usr/bin/env python

import os
import sys
from optparse import OptionParser
try:
    import json
except:
    import simplejson as json

#WORKFLOW_LOCATION = "/home/rtruty/builds/rtruty_lx1_trio_2.5.0/inst/opt70max/etc/workflows/onestep/TrioMkvcf.py"
WORKFLOW_LOCATION = "TrioMkvcf.py"

def runTrioMkvcfWorkflow(workFlowName, cgi_home,workDir, overrides, start_stage):
    workflow_loc = WORKFLOW_LOCATION
    
    if start_stage!=None:
        start_stage_option = "-b %s" % start_stage
    else:
        start_stage_option = ""
        
    if cgi_home!=None:
        cgi_home_option = "-h %s" % cgi_home
    else:
        cgi_home_option = ""
            
    command = "rat create %(workFlowName)s %(workflow_loc)s %(cgi_home_option)s -c %(workFlowName)s %(start_stage_option)s -w %(workDir)s -x '%(overrides)s' -s lxv-testscrat01:8080" % vars()
    print "RUNNING: %s" % command
    sys.stdout.flush()
    os.system(command)

################################################################################################################
            
def parseArguments(arguments):
    parser = OptionParser()
    
    parser.add_option("--cgi-home",default=None,help="Use this CGI_HOME (rather than $CGI_HOME)")
    parser.add_option("-w","--workDir",default=None,help="Work directory (i.e. output directory)")
    parser.add_option("-a","--asmList",default=None,help="File containing ASMs (headers: ASMDIR, GROUP, NAME, GENDER, OUTPUT-NAME); tab separated")
    parser.add_option('-o', '--outfile',default=None,help="Output file (will contain summary results for all trios)")
    parser.add_option("-r","--reference",default=None,help="Male reference file (crr format)")
    parser.add_option("-f","--filter",default=None,help="Filter out variants that do not have this varFilter value (typically 'varFilter!=PASS'),  use 'ROC' to generate unfiltered ROC curves")
    parser.add_option("--regions",default=None, help="Report MIEs that overlap with regions in this bed file")
    parser.add_option("--invertRegions",default=False,action="store_true",help="Rather than report MIEs that overlap with regions in this bed file, instead report MIEs that do not overlap with regions in this bed file")
    parser.add_option("--onlyPhased",default=False,action="store_true",help="Filter variant files to only consider variants that are phased")
    parser.add_option("--dont-clean",default=False,action="store_true",help="If true, don't clean up intermediate files. If false, leave only summary results (default: False)")
    parser.add_option("-b","--start-stage",default=None,help="first stage of workflow not to skip (optional)")
    
    options,args = parser.parse_args(arguments)
    
    if [options.workDir,options.outfile,options.asmList,options.reference].count(None)>0:
        parser.print_help()
        sys.exit(1)
        
    return options.start_stage,options.dont_clean,options.cgi_home,options.workDir,options.outfile,options.asmList,options.reference,options.filter,options.invertRegions,options.onlyPhased,options.regions
    
################################################################################################################
    
if __name__=="__main__":

    start_stage, dont_clean, cgi_home, workDir, outfilename, asmList, reference, varFilter, invertRegions, onlyPhased, regionsFile = parseArguments(sys.argv[1:])
        
    #initialize json obj
    obj = {}
    
    #top level options
    obj["outfile"] = os.path.abspath(outfilename)
    obj["asmList"] = os.path.abspath(asmList)
    obj["reference"] = os.path.abspath(reference)
    obj["onlyPhased"] = onlyPhased    

    if dont_clean:
        obj["clean"] = False
    else:
        obj["clean"] = True
        
        
    if varFilter!=None and varFilter!="ROC":
        obj["varFilter"] = varFilter
    if regionsFile!=None:
        obj["regionsFile"] = regionsFile
        obj["invertRegions"] = invertRegions

    if varFilter=="ROC" and not regionsFile==None:
        workFlowName = "trioMkvcf_regions_ROC"
    elif varFilter=="ROC":
        workFlowName = "trioMkvcf_ROC"
    elif not regionsFile==None:
        workFlowName = "trioMkvcf_regions"
    elif onlyPhased:
        workFlowName = "trioMkvcf_OnlyPhased"
    else:
        workFlowName = "trioMkvcf"

    # Additional variables that I'll use when running
    obj["mkvcf"] = {}
    obj["mkvcf"]["outfiles"] = []
    obj["vcfannotated"] = {}
    obj["vcfannotated"]["outfiles"] = []
    obj["mies"] = {}
    obj["mies"]["outfiles"] = []
    obj["count"] = {}
    obj["count"]["sum2Files"] = []
        
    overrides = json.dumps(obj)
    runTrioMkvcfWorkflow(workFlowName, cgi_home, workDir, overrides, start_stage)

    
