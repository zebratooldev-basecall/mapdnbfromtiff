#!/usr/bin/env python

import sys, os
import optparse

def createOutputFile(name, content, status=None):
    if status:
        status='-'+status
    else:
        status = ''
    os.system("echo %s >%s"%(content, name+status))

def renameOutputFile(name, status):
    status='-'+status
    os.rename(name, name+status)
    
class CompareMethod(object):
    def __init__(self, methodName = 'default'):
        self.method = methodName
        
    def reportError(self, errMsg=None):
        if not errMsg: 
            errMsg = "Error in compare method: %s" % self.method
        createOutputFile(self.outFileName,errMsg,"FAILED")
        raise Exception(errMsg)
    
    def setOptions(self, options):
        self.options = options
        self.outFileName = options.output
        self.resultCode = 0
        
    def compare(self):
        raise Exception("the method is not implemented")

    def copy(self):
        cmd = "mkdir -p %s;cp -f %s %s>%s"% (os.path.dirname(self.options.inputGold),self.options.input, self.options.inputGold, self.outFileName) 
        self.resultCode = os.system( cmd )

    def processResult(self):
        if self.resultCode>0:
            renameOutputFile(self.outFileName,"FAILED")
        else:
            renameOutputFile(self.outFileName,"OK")
        return self.resultCode
    
class CompareMethodDummy(CompareMethod):
    def __init__(self):
        CompareMethod.__init__(self, 'dummy')
    def compare(self):
        createOutputFile(self.options.output,"dummyCompare")
        
class CompareMethodBinDiff(CompareMethod):
    def __init__(self):
        CompareMethod.__init__(self, 'bindiff')
    def compare(self):
        cmd = 'diff %s %s >%s'% (self.options.input, self.options.inputGold, self.outFileName) 
        self.resultCode = os.system( cmd )

class CompareMethodDiffCollections(CompareMethod):
    def __init__(self):
        CompareMethod.__init__(self, 'diffCollections')
    def compare(self):
        cmd = 'compareCollections -d %s %s -s >%s'% (self.options.input, self.options.inputGold, self.outFileName) 
        self.resultCode = os.system( cmd )
    def copy(self):
        cmd = "mkdir -p %s;cp -f %s %s>%s"% (os.path.dirname(self.options.inputGold),os.path.splitext(self.options.input)[0]+'.*', os.path.dirname(self.options.inputGold),self.outFileName) 
        self.resultCode = os.system( cmd )

comparatorList = [
    CompareMethodBinDiff(),
    CompareMethodDiffCollections(),
    CompareMethodDummy(), 
]

comparators = {}

for c in comparatorList: 
    comparators[c.method] = c 

parser = optparse.OptionParser("usage: %prog [options]")
parser.add_option("-i", "--input-file", dest="input",
                  help="input file or directory")
parser.add_option("-g", "--input-gold", dest="inputGold",
                  help="gold input file or directory")
parser.add_option("-o", "--output", dest="output",
                  help="output file")
parser.add_option("-m", "--method", dest="method",
                  help="Currently supported methods: "+str(comparators.keys()))
parser.add_option("-e", "--no-error", dest="noError",
                  help="Don't report error code for failed tests. Return 0 for all the executable error codes except 255.")
parser.add_option("-c", "--copy-gold", dest="copyGold",
                  help="Copy gold data instead of compare")

(options, args) = parser.parse_args()
if len(args) != 0:
    parser.error("unexpected arguments: "+args)

if comparators.has_key(options.method):
    comparator = comparators[options.method]
    comparator.setOptions(options)
    if options.copyGold:
        comparator.copy()
    else:
        comparator.compare()
    result = comparator.processResult()
    if result>255:
        result = result / 256 
    if options.noError and result<255: 
        sys.exit(0)
    else:
        sys.exit(result)
else:
    errMsg = "Error in parameters:compare method not found: %s" % options.method
    createOutputFile(options.output,errMsg,"FAILED")
    raise Exception(errMsg)
