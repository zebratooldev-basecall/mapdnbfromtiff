#!/usr/bin/env python

import os
import sys
import re
import getopt

def main():

    # parse command line options and update variables
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:b:", [])
    except getopt.GetoptError:
        sys.exit(usage)

    for opt, arg in opts:
        if opt == "-h":
            sys.exit('Use me properly.')
        elif opt == '-i':
            INFILE = arg
        elif opt == '-b':
            BARCODEFILE = arg
        elif opt == '-o':
            OUTFILE = arg
           
    outf = open(OUTFILE, 'w')
    outf.write( "\t".join( ["lane", "unexpectedLow", "unexpectedHigh", "unexpectedFrac", "lowFrac", "undecodedFrac"] )  + "\n" )

    ## Read in lane <-> barcode table
    GetBarcodes = {}
    try:
        for line in open(BARCODEFILE, 'r'):
            (lane, barcodes) = line.rstrip().split()
            GetBarcodes[ lane ] = barcodes
    except:
        print "No expected barcodes input\n"

    unexpectedLowCumm  = 0
    unexpectedHighCumm = 0
    unexpectedTotCumm  = 0
    lowTotCumm         = 0
    decodedTagsCumm    = 0
    undecodedTagsCumm  = 0
    totalTagsCumm      = 0
                
    for infile in INFILE.split(","):
        inf = open(infile, 'r')
        lane = os.path.basename(infile).split(".")[0]

        barcodeTotal = {}
        maxTotal = 0
            
        line = inf.readline()
        if not line: break
        words = line.split(",")

        while words[0] != 'totalTags': 
            line = inf.readline()
            if not line: break
            words = line.split(",")
        totalTags = int(words[1])

        while words[0] != 'decodedTags': 
            line = inf.readline()
            if not line: break
            words = line.split(",")
        decodedTags = int(words[1])

        while words[0] != 'undecodedTags': 
            line = inf.readline()
            if not line: break
            words = line.split(",")
        undecodedTags = int(words[1])
        while 1==1:
            line = inf.readline()
            if not line: break
            words = line.split(",")
            if words[0]=='array':
                if words[1].rstrip()=="tagSetMatches": 
                    break
        line = inf.readline()
        line = inf.readline()
        barcodes = GetBarcodes.get( lane, "" ).split(",")
        while line !="":
            if line.rstrip() == "": break
            (bid, correct, corrected, nocall, total) = line.split(",")
            barcodeTotal[ bid ] = total
            maxTotal = max( maxTotal, int(total) )
            line = inf.readline()
        inf.close()

        unexpectedHigh = 0
        unexpectedLow = 0
        unexpectedTot = 0
        lowTot = 0
        for bid,tot in barcodeTotal.items():
            tot = int(tot)
            if bid in barcodes:
                if tot < maxTotal*0.25:
                    unexpectedLow += 1
            else:
                if tot > maxTotal*0.25:
                    unexpectedHigh += 1
                unexpectedTot += tot
            if tot < maxTotal*0.25:
                lowTot += tot

        unexpectedFrac      = float(unexpectedTot) / decodedTags
        lowFrac             = float(lowTot) / decodedTags
        undecodedFrac       = float(undecodedTags) / totalTags
        outf.write( "\t".join( [lane, str(unexpectedLow), str(unexpectedHigh), str(unexpectedFrac), str(lowFrac), str(undecodedFrac)] )  + "\n" )
        
        unexpectedLowCumm  += unexpectedLow
        unexpectedHighCumm += unexpectedHigh
        unexpectedTotCumm  += unexpectedTot
        lowTotCumm         += lowTot
        decodedTagsCumm    += decodedTags  
        undecodedTagsCumm  += undecodedTags
        totalTagsCumm      += totalTags    

    unexpectedFracCumm = float(unexpectedTotCumm) / decodedTagsCumm
    lowFracCumm = float(lowTotCumm) / decodedTagsCumm
    undecodedFracCumm  = float(undecodedTagsCumm) / totalTagsCumm
    outf.write( "\t".join( ['all', str(unexpectedLowCumm), str(unexpectedHighCumm), str(unexpectedFracCumm), str(lowFracCumm), str(undecodedFracCumm)] )  + "\n" )
    outf.close()

    outf = open( re.sub(".tsv","_all.tsv",OUTFILE), 'w' )
    outf.write("Number of barcodes at unexpectedly low (<25% max) rates",  str(unexpectedLowCumm) )
    outf.write("Number of barcodes at unexpectedly high (>25% max) rates", str(unexpectedHighCumm) )
    outf.write("Fraction of barcodes from unexpected barcodes", str(unexpectedFracCumm) )
    outf.write("Fraction of barcodes at low rates (<25% of max)", str(lowFracCumm) )
    outf.write("Fraction of barcodes that are undecoded", str(undecodedFracCumm) )
    outf.close()

###################################################

if __name__ == "__main__":
    main()
