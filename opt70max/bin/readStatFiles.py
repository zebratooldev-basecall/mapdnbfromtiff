#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
try:
    import json
except:
    import simplejson as json


# import pprint
# import cPickle as pickle

###### Document Decription
'''  '''

###### Version and Date
prog_version = '1.0.0'
prog_date = '2014.03.25'

###### Usage
usage = '''

     Version %s  by Vincent-Li  %s

     Usage: %s <statFile> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class readStat(object):
    """ Object to read statistics file created by pipeline.

            Usage:
                readList: Sections in this list will be recorded. []
                skipList: Sections in this list wiil be skiped. []
                typeList: Only recored specific types of section. [dict, array, matrix]

            1) Read and record (Initialization):
            dataObj = readStat(fileToRead, readList, skipList, typeList)

            2) Count section number:
            len(dataObj) # return recored section number

            3) get specific section by name
            dataObj.get(key) # structure relative to selected section. (dict or list)

            4) convert to JSON format
            dataObj.dumps(indent, sectionName) # if sectionName is not given, dump all sections.

            5) convert matrix to dict format
            dataObj.formatMatrix(sectionName, keyCategory)
            example:
            [
                [lane, GS65924-FS3-L06, GS65924-FS3-L07],
                [left, 1, 2],
                [right, 3, 4]
            ]

            dataObj.formatMatrix(sectionName, lane)
            # will be converted to:
            {
                GS65924-FS3-L06:
                    {
                        left: 1,
                        right: 3
                    },
                GS65924-FS3-L07:
                    {
                        left: 2,
                        right: 4
                    }
            }

    """

    def read(self, filename):
        try:
            if filename[-3:] == '.gz':
                fileHandle = os.popen('gzip -dc %s' % filename, 'r', 100000000)
            elif filename[-4:] == '.bz2':
                fileHandle = os.popen('bzip2 -dc %s' % filename, 'r', 100000000)
            else:
                fileHandle = open(filename, 'r', 100000000)
        except IOError, error:
            raise error
        
        allowType = ['dict', 'array', 'matrix']
        type = None
        name = None
        container = []
        for line in fileHandle:

            strLine = line.strip()

            ##### skip comment and empty line
            if line.startswith('#') or strLine == '':
                continue

            ##### main loop
            lineList = strLine.split(',')

            if lineList[0] in allowType:
                if type != None:
                    yield type, name, container
                type = lineList[0]
                name = lineList[1]
                container = []
            else:
                container.append(lineList)

        fileHandle.close()
        if type != None:
            yield type, name, container
            
    def get(self, key):
        if key in self.data:
            return self.data[key]
        else:
            return None

    def dumps(self, indent=4, section='all'):
        if section == 'all':
            target = self.data
        else:
            target = self.data[section]

        return json.dumps(target, indent=indent)

    def formatMatrix(self, key, keyCategory):
        if self.struct[key] != 'matrix':
            return None

        tmpList = [{}] * len(self.data[key][0])
        newDict = {}
        for row in self.data[key]:
            if row[0] == keyCategory:
                for i in xrange(1, len(row)):
                    newDict[row[i]] = tmpList[i]
            else:
                for i in xrange(1, len(row)):
                    tmpList[i][row[0]] = row[i]
        return newDict



    def record(self, fileName, readSet=set(), skipSet=set(), typeSet=set()):
        for type, name, container in self.read(fileName):

            ####### skip section in certain cases
            if readSet and name not in readSet:
                continue
            elif skipSet and name in skipSet:
                continue
            elif typeSet and type not in typeSet:
                continue

            if type == 'dict':
                self.data[name] = {}
                for x in container:
                    self.data[name][x[0]] = x[1]
            elif type == 'array':
                self.data[name] = []
                headList = container[0]
                if len(container) > 1:
                    for x in container[1:]:
                        self.data[name].append(dict(zip(headList, x)))
            elif type == 'matrix':
                self.data[name] = container
            else:
                raise Exception, 'Undefined type: %s' % type

            self.struct[name] = type

            ####### optimization of readSet
            if len(self) == len(readSet):
                break
    
    def __init__(self, fileName, readList=[], skipList=[], typeList=[]):
        self.data = {}
        self.struct = {}
        self.record(fileName, readSet=set(readList), skipSet=set(skipList), typeSet=set(typeList))

    def __str__(self):
        return str(self.data)

    def __len__(self):
        return len(self.data)
        

##########################################################################
############################  BEGIN Function  ############################
##########################################################################


######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)
    ArgParser.add_argument("-r", "--readSet", action="store", dest="readStr", metavar="SET", help="Only specific section will be read and preserved. [%(default)s]")
    ArgParser.add_argument("-s", "--skipSet", action="store", dest="skipStr", metavar="SET", help="Section in the set will be skiped. [%(default)s]")
    ArgParser.add_argument("-t", "--type", action="store", dest="typeStr", default='dict,array,matrix', metavar="NAME", help="Only specific type of data structure will be preserved. [%(default)s]")
    ArgParser.add_argument("-d", "--dump", action="store_true", dest="dump", default=False, help="output result in JSON instead of normal print. [%(default)s]")

    (para, args) = ArgParser.parse_known_args()
    

    if len(args) != 1:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (statFile,) = args

    ############################# Main Body #############################
    if para.readStr:
        readList = para.readStr.split(',')
    else:
        readList = []

    if para.skipStr:
        skipList = para.skipStr.split(',')
    else:
        skipList = []
    
    if para.typeStr:
        typeList = para.typeStr.split(',')
    else:
        typeList = []
    
    
    

    dataObj = readStat(statFile, readList, skipList, typeList)
    if para.dump:
        print dataObj.dumps()
    else:
        print dataObj


#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################