#!/usr/bin/env python

import os
import sys
from optparse import OptionParser

def parse_arguments(args):
    parser = OptionParser()

    parser.add_option('-i', '--input-file', help='Input file from getnanodnb', default=None, dest='input')
    parser.add_option('-o', '--output-file', help='Output file', default=None, dest='output')

    options, args = parser.parse_args(args)

    # make sure input and output files are set.
    if options.input == None or options.output == None:
        parser.print_help()
        print('both input and output arguments are required!')
        sys.exit(1)

    return options.input, options.output

def load_calls(file):
    # make sure file exists.
    if not os.path.isfile(file):
        print('file does not exist "%s"!' % file)
        sys.exit(1)

    # read in file.
    calls = []
    index = 0
    ff = open(file, 'r')
    for line in ff:
        fields = line.rstrip('\r\n').split(',')
        calls.append((index,fields[1],fields[2],fields[3],fields[4]))
        index += 1
    ff.close()

    return calls

def parseDnbSummaryFileName(filepath):
    filename = os.path.split(filepath)[1]
    tokens = filename.split('.')
    slide  = tokens[0]
    lane   = int(tokens[1][1:3])
    column = int(tokens[1][4:7])
    row    = int(tokens[1][8:11])

    return slide, lane, column, row

def calc_concordance(calls, slide, lane, column, row, out_file):
    HEADER = ['slide','lane','column','row','percentile','concordance','call_rate']

    # open file
    sw = open(out_file, 'w')
    sw.write(','.join(HEADER) + '\n')

    # create sorted lists of dnbs.
    cyc1 = sorted(calls, key=lambda call: call[2], reverse=True) # sort cycle 1 by score.
    cyc2 = sorted(calls, key=lambda call: call[4], reverse=True) # sort cycle 2 by score.

    # loop through fractions of the data, find intersection, and calculate concordance.
    cyc1set = set()
    cyc2set = set()
    seenIndexes = set()
    lowerIndex, match, total = 0, 0, 0
    for i in range(100):
        # calculate upper bounds of stripe.
        upperIndex = len(calls) * (i+1)/100
        for j in range(lowerIndex, upperIndex):
            # add dnb indexes to individual sets.
            cyc1set.add(cyc1[j][0])
            cyc2set.add(cyc2[j][0])
            lowerIndex += 1

        # find intersection.
        intrsct = cyc1set.intersection(cyc2set)
        newIndexes = intrsct.difference(seenIndexes)

        # count matching calls.
        for index in list(newIndexes):
            if calls[index][1] == 'N' or calls[index][3] == 'N':
                continue
            if calls[index][1] == calls[index][3]:
                match += 1
            total += 1

        percentile = i+1
        concordance = float(match)/float(total)
        call_rate = float(total)/float(len(calls))

        # add nexIndexes to seen index set.
        seenIndexes = seenIndexes.union(intrsct)

        sw.write('%(slide)s,%(lane)d,%(column)d,%(row)d,%(percentile)d,%(concordance)f,%(call_rate)f\n' % vars())

    sw.close()

def main(arguments):
    # parse command line arguments.
    in_file, out_file = parse_arguments(arguments[1:])

    # parse slide, lane, field, and cycle from input file name
    slide, lane, column, row = parseDnbSummaryFileName(in_file)

    # read in base calls and scores.
    calls = load_calls(in_file)

    # calc_concordance
    calc_concordance(calls, slide, lane, column, row, out_file)

main(sys.argv)
