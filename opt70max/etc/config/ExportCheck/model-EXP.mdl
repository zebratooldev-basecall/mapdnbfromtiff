###############################################################################
###############################################################################
#                   DEFINES AND PREDICATES SECTION
###############################################################################
###############################################################################


###############################################################################
# Defines that must be defined by the calling code.
###############################################################################

# Determines flavor of file masks to use for certain IDs (like ASSEMBLY etc.),
# when a concrete define for that ID is not provided.
# Supported values:
# - GENERIC: use masks that will accomodate actual use in both production and
# R&D (both small nightly tests and other tests).
# - PRODUCTION: use more stringent masks that reflect the current actual use in
# production. This is to catch any deviations from the "norm" in production.
# - SMALL_TEST: use more stringent masks that reflect the current actual use in
# R&D nightly small tests. This is just to test the scheme with more stringent
# masks, and also catch any changes in file naming conventions in small tests.
@require    MASKS_TO_USE
@if "$MASKS_TO_USE" not in [ 'GENERIC', 'PRODUCTION', 'SMALL_TEST' ]
    @error "MASKS_TO_USE must be one of: 'GENERIC', 'PRODUCTION', 'SMALL_TEST'
@endif

# Number of genomes in the assembly
@require    N_GENOMES

# List of chromosomes expected to be found in each genome of the assembly.
# Normally something like [ chr1, chr2, ..., chr22, chrX, chrM ], but can
# include unusual names like OneMBpTestWithSegDups for small tests.
@require    CHROMOSOMES

# EXP type: can be "EXP" or "EXP_R" for Rogue, "EXP" only for all others.
@require    EXP_TYPE

@if "$EXP_TYPE" not in [ "EXP", "EXP_R" ]
    @error "Unexpected EXP_TYPE: " + $EXP_TYPE
@endif


###############################################################################
# Defines that can be reasonably defaulted or deduced from the mandatory ones
# (but still can be overriden by the calling code).
###############################################################################

# IS_REMOTE enables checking of a remote EXP on S3 or elsewhere:
# instead of trying to find each element (file or dir) on disk, the model will
# use the file information passed to CheckDirsVsModels as element-to-properties
# map argument elementProps, which is exposed for each asserted element here
# as $elem_props (see below).
# NOTE: This way was chosen over just passing a map with file properties as
# another define, because that would cause the entire map to print in every
# interpolated predicate in the output log.
@default    IS_REMOTE   False

# Check the contents of externalSampleId file against the expected
# external sample ID extracted from $EXTERNAL_SAMPLE_ID_MAP.
@default    CHECK_EXTERNAL_SAMPLE_ID    False

@if $CHECK_EXTERNAL_SAMPLE_ID
    # Map of internal CGI sample IDs onto external sample IDs (e.g. for TARGET)
    @require    EXTERNAL_SAMPLE_ID_MAP
    @require    ENCRYPTION_PASS
@endif

# Caller can allow TARGET PILOT: adds TARGET PILOT's special assembly ID to the mask
@default    ALLOW_TARGET_PILOT  False

# Is EXP expected to contain read_* and mapping_* files? 'None' means "we don't know".
@default    INCLUDE_READS_AND_MAPPINGS      None

# Is EXP expected to contain tagLFR* files? 'None' means "we don't know".
@default    INCLUDE_LFR_TAGS                None

# Is EXP expected to contain mobileElementInsertions* files? 'None' means "we don't know".
@default    INCLUDE_MEI_REPORTS             None

# Is EXP expected to contain a clinical report (CQReport*.pdf)? Require this one at least
# for the initial clinical period just to be sure it's not missed.
@require    CLINICAL_SAMPLE_INFO

# Number of compared genome pairs. Pairs are directional: A-to-B and B-to-A
# are counted as 2 pairs.
# For single-genome, this yields 0.
# For multi-genome, 'A' is compared against all the other (N - 1) genomes,
# and each of the other (N - 1) genomes is compared to 'A'.
@default    N_COMPARED      2*($N_GENOMES-1)

# Number of somatic (non-baseline) genomes.
# For single-genome, this yields 0.
# For multi-genome, every genome except 'A' is treated as somatic.
@default    N_SOMATIC       ($N_GENOMES-1)

# Maximum size of a file in export dirs.
# The default of 5 GB is the current S3 limitation.
@default    MAX_FILE_SIZE   5*(1024**3)


###############################################################################
# Generate patching scripts.
###############################################################################

@default    GEN_PATCH_SCRIPTS   False
@if $GEN_PATCH_SCRIPTS
    @require DID_TO_PATCH
@endif


###############################################################################
# Alias longer names to shorter ones for more compact file count expressions
# in assertions.
###############################################################################

@define     N                   $N_GENOMES
@define     CN                  $N_COMPARED
@define     SN                  $N_SOMATIC

@if isinstance($CHROMOSOMES, dict)
    @define     CHR_COUNTS      $CHROMOSOMES
@elif isinstance($CHROMOSOMES, list)
    @define     CHR_COUNTS      dict([ (c, $N) for c in $CHROMOSOMES ])
@endif


###############################################################################
# Regular expressions for defining parts of file paths.
###############################################################################

# Assembly ID
@if "$MASKS_TO_USE" == "PRODUCTION"
    # The only three flavors of ASM ID currently expected in production.
    @define     ASSEMBLY_STD    r"GS\d{9}-ASM"                              # "GS123456789-ASM" (assembly)
    @define     ASSEMBLY_REASM  r"GS([0-9]{5}|[0-9]{8})-DNA(_\w\d\d)+_\d{3,4}_\d{2}-ASM"  # "GS12345-DNA_G01_A02-220-37-ASM" (reassembly)
    @define     ASSEMBLY_TARGET r"[A-Z]{6}_\d{3,4}-ASM"                     # "PXVAQS_210-ASM" (TARGET pilot)
    @if $ALLOW_TARGET_PILOT
        @default    ASSEMBLY    r"($ASSEMBLY_STD|$ASSEMBLY_REASM|$ASSEMBLY_TARGET)"
    @else
        @default    ASSEMBLY    r"($ASSEMBLY_STD|$ASSEMBLY_REASM)"
    @endif
@else
    # Default to a generic mask that will accept almost any alphanumeric value.
    # NOTE: Continuously accept [-\w] characters into ASSEMBLY as long as the following
    # sequence does not constitute a comparison suffix. This makes ASSEMBLY precise:
    # ${ASSEMBLY} will match "GS00001-ASM", but not "GS00001-ASM-T1".
    @default    ASSEMBLY        r"(((?!-[TN]\d)[-\w])+)"    # Any alphanumeric string not ending in comparison suffix.
@endif

# Sample ID
@if "$MASKS_TO_USE" == "SMALL_TEST"
    @default    SAMPLE          r"(SimImager-Sample[A-Z]?)" # "SimImager-Sample" or "SimImager-SampleA"
@else
    @default    SAMPLE          r"(GS([0-9]{5}|[0-9]{8})-DNA(_\w\d\d)?)"  # "GS12345-DNA_G01"
@endif

# Slide ID
@if "$MASKS_TO_USE" == "PRODUCTION"
    @default    SLIDE           r"(GS([0-9]{5}|[0-9]{8})-FS3)"            # "GS12345-FS3" etc.
@elif "$MASKS_TO_USE" == "SMALL_TEST"
    @default    SLIDE           r"(SIM-SLIDE-\d+)"          # "GS12345-FS3" etc.
@else
    @default    SLIDE           r"([-\w]+)"                 # "GS12345-FS3" etc.
@endif

# Library ID
@if "$MASKS_TO_USE" == "SMALL_TEST"
    @default    CLS             r"(SI([0-9]{5}|[0-9]{8})-CLS)"            # "SI00001-CLS"
@else
    @default    CLS             r"(GS([0-9]{5}|[0-9]{8})-CLS(_\w\d\d)?)"  # "GS00001-CLS" or "GS000001-CLS_D12"
@endif

# Lane ID
@default        LANE            r"(L\d{2,3})"               # "L02"

# Comparison and multi-comparison suffixes (assembly ID modifiers)
@if $CN > 0
    @default    TN              r"(-[TN]\d)"                # "-T1"
    @default    TNTN            r"($TN)+"                   # "-N1-T1", "-N1-T1-T2", etc.
@else
    @default    TN              r"\b"                       # word boundary
    @default    TNTN            r"\b"                       # word boundary
@endif


###############################################################################
# Predicates for checking file properties.
###############################################################################

@if $IS_REMOTE
    @predicate  is_dir              $elem_props['type'] == 'dir'
    @predicate  is_file             $elem_props['type'] == 'file'
    @predicate  file_size           $elem_props['size']
    @predicate  file_first_8_bytes  $elem_props['firstBytes'][0:8]
@else
    # Top EXP or EXP-R dir. Used only for file property checks. File count checks are asserted
    # directly in the processing code (checkDirVsModel.py)
    @require    ROOT_DIR
    @predicate  is_dir              os.path.isdir("${ROOT_DIR}/$elem")
    @predicate  is_file             os.path.isfile("${ROOT_DIR}/$elem")
    @predicate  file_size           os.path.getsize("${ROOT_DIR}/$elem")
    @predicate  file_first_8_bytes  open("${ROOT_DIR}/$elem").read(8)
@endif

@if "$EXP_TYPE" == "EXP_R"
    @predicate  is_encryption_ok    $file_first_8_bytes == "Salted__" ~~ $cp_matching
@else
    @predicate  is_encryption_ok    $file_first_8_bytes != "Salted__" ~~ $cp_matching
@endif

@if $MAX_FILE_SIZE > 0
    @predicate  is_size_ok          0 < $file_size < $MAX_FILE_SIZE ~~ $cp_matching
@else
    @predicate  is_size_ok          0 < $file_size ~~ $cp_matching
@endif

###############################################################################
# Special predicate to decrypt the contents of a remote externalSampleId and
# compare it with the expected value(s) passed in as EXT_SAMPLE_MAP.
###############################################################################

@if $CHECK_EXTERNAL_SAMPLE_ID
    @execute    import subprocess
    @predicate  openssl_cmd         [ 'openssl', 'enc', '-d', '-aes-256-cbc', '-salt', '-pass', '${ENCRYPTION_PASS}' ]
    @predicate  openssl_pipe        subprocess.Popen($openssl_cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    @predicate  actual_ext_sample   $openssl_pipe.communicate(input=$elem_props['firstBytes'])[0]
    @predicate  expected_ext_sample $EXTERNAL_SAMPLE_ID_MAP[$match_groups['sample']]
    @predicate  is_ext_sample_ok    $actual_ext_sample==$expected_ext_sample
@else
    @define     actual_ext_sample   "'<not checked>'"
    @define     expected_ext_sample "'<not checked>'"
    @define     is_ext_sample_ok    True
@endif


###############################################################################
# Printable actions for patching found problems.
###############################################################################

# copyFile, copyMissingFiles and copyExtSampleId are just function call stubs
# that will be printed to the output when a corresponding predicate in front
# or '~~' operator (see below) yields False. The code that has to do subsequent
# patching needs to define these functions to perform a corresponding patching
# action, and process the patching script printed here.
# An example of such code is workflows/onestep/stagingPatch.py.
# Alternatively, the output can be manually reworked into a shell script to do
# the same.

@if $GEN_PATCH_SCRIPTS
    @predicate  cp_matching             "copyFile('$DID_TO_PATCH', r'$elem')"
    @predicate  cp_missing              "copyMissingFiles('$DID_TO_PATCH', r'$mask', $elems)"
    @predicate  cp_external_sample_id   "copyExtSampleId('$DID_TO_PATCH', r'$elem', '''$expected_ext_sample''')"
@else
    @predicate  cp_matching             ""
    @predicate  cp_missing              ""
    # In non-patching mode, provide improved error output for externalSampleId:
    @predicate  cp_external_sample_id   $actual_ext_sample + '!=' + $expected_ext_sample
@endif


###############################################################################
# Root package dirs.
###############################################################################

# For remote, we start one level higher than local, for 2 purposes:
# 1) To make sure there are no extraneous unexpected assemblies under the target DID
# 2) For the code below to be simpler: since we remove the "package" level when we
# upload an EXP object, we would have to set PACKAGE to "", which would invalidate
# paths like "${PACKAGE}/${SAMPLE}..." and require paths like "${PACKAGE}${SAMPLE}"
# instead - not a huge problem, but looks better as it is.

@if $IS_REMOTE
    @if "$EXP_TYPE" == "EXP"
        @define PACKAGE     "${ASSEMBLY}"
    @elif "$EXP_TYPE" == "EXP_R"
        @define PACKAGE     "${ASSEMBLY}/EXP"
        @define BAM_PACKAGE "${ASSEMBLY}/MAP-${ASSEMBLY}"
    @endif
@else
    @if "$EXP_TYPE" == "EXP"
        @define PACKAGE     "package"
    @elif "$EXP_TYPE" == "EXP_R"
        @define PACKAGE     "EXP"
        @define BAM_PACKAGE "MAP-${ASSEMBLY}"
    @endif
@endif


###############################################################################
###############################################################################
#                            MAIN SECTION
###############################################################################
###############################################################################


###############################################################################
# Directory assertions.
###############################################################################

@if $IS_REMOTE
    "${ASSEMBLY}" : $is_dir && $count==1
@endif

"${PACKAGE}" : $is_dir && $count==1
"${PACKAGE}/${SAMPLE}" : $is_dir && $count==$N
"${PACKAGE}/${SAMPLE}/ASM" : $is_dir && $count==$N
"${PACKAGE}/${SAMPLE}/ASM/CNV" : $is_dir && $count==$N
"${PACKAGE}/${SAMPLE}/ASM/EVIDENCE" : $is_dir && $count==$N
"${PACKAGE}/${SAMPLE}/ASM/EVIDENCE-${ASSEMBLY}${TN}" : $is_dir && $count==$CN
"${PACKAGE}/${SAMPLE}/ASM/REF" : $is_dir && $count==$N
"${PACKAGE}/${SAMPLE}/ASM/REPORTS" : $is_dir && $count==$N
"${PACKAGE}/${SAMPLE}/ASM/SV" : $is_dir && $count==$N

@if $INCLUDE_MEI_REPORTS
    "${PACKAGE}/${SAMPLE}/ASM/MEI" : $is_dir && $count==$N
@elif $INCLUDE_MEI_REPORTS is None
    "${PACKAGE}/${SAMPLE}/ASM/MEI" : $is_dir && $count>=0
@else
    "${PACKAGE}/${SAMPLE}/ASM/MEI" : $count==0
@endif

"${PACKAGE}/${SAMPLE}/LIB" : $is_dir && $count==$N
"${PACKAGE}/${SAMPLE}/LIB/${CLS}" : $is_dir && $count>=$N

@if "$EXP_TYPE" == "EXP"
    @if $INCLUDE_READS_AND_MAPPINGS or $INCLUDE_LFR_TAGS
        "${PACKAGE}/${SAMPLE}/MAP" : $is_dir && $count==$N
        "${PACKAGE}/${SAMPLE}/MAP/${SLIDE}-${LANE}" : $is_dir && $count>=$N
    @elif $INCLUDE_READS_AND_MAPPINGS is None and $INCLUDE_LFR_TAGS is None
        "${PACKAGE}/${SAMPLE}/MAP" : $is_dir && $count>=0
        "${PACKAGE}/${SAMPLE}/MAP/${SLIDE}-${LANE}" : $is_dir && $count>=0
    @else
        "${PACKAGE}/${SAMPLE}/MAP" : $count==0
        "${PACKAGE}/${SAMPLE}/MAP/${SLIDE}-${LANE}" : $count==0
    @endif
@elif "$EXP_TYPE" == "EXP_R"
    "${BAM_PACKAGE}" : $is_dir && $count==1
    "${BAM_PACKAGE}/BAM" : $is_dir && $count==1
    "${BAM_PACKAGE}/BAM/${SAMPLE}" : $is_dir && $count==$N
@endif


###############################################################################
# File assertions:
###############################################################################

"${PACKAGE}/README.*\.txt" : $is_file && $is_size_ok && $count==1 ~~ $cp_missing
"${PACKAGE}/manifest\.all" : $is_file && $is_size_ok && $count==1 ~~ $cp_missing

@if "$EXP_TYPE" == "EXP_R"
    "${PACKAGE}/(?P<sample>${SAMPLE})/externalSampleId-(?P=sample)" : $is_file && $file_size==48 && $is_encryption_ok && $count==$N ~~ $cp_missing && $is_ext_sample_ok ~~ $cp_external_sample_id
@endif

"${PACKAGE}/${SAMPLE}/version" : $is_file && $is_size_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/idMap-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $count==($CN>0 and $N or 0) ~~ $cp_missing

"${PACKAGE}/${SAMPLE}/ASM/dbSNPAnnotated-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing

"${PACKAGE}/${SAMPLE}/ASM/CNV/cnvDetailsDiploidBeta-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/CNV/cnvDetailsNondiploidBeta-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/CNV/cnvSegmentsDiploidBeta-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/CNV/cnvSegmentsNondiploidBeta-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/CNV/depthOfCoverage_100000-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/CNV/somaticCnvDetailsDiploidBeta-${ASSEMBLY}${TN}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$SN ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/CNV/somaticCnvDetailsNondiploidBeta-${ASSEMBLY}${TN}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$SN ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/CNV/somaticCnvSegmentsDiploidBeta-${ASSEMBLY}${TN}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$SN ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/CNV/somaticCnvSegmentsNondiploidBeta-${ASSEMBLY}${TN}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$SN ~~ $cp_missing

"${PACKAGE}/${SAMPLE}/ASM/EVIDENCE/correlation-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing

@for chr in $CHR_COUNTS.keys()
    "${PACKAGE}/${SAMPLE}/ASM/EVIDENCE/evidenceDnbs-${chr}-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$CHR_COUNTS["$chr"] ~~ $cp_missing
    "${PACKAGE}/${SAMPLE}/ASM/EVIDENCE/evidenceIntervals-${chr}-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$CHR_COUNTS["$chr"] ~~ $cp_missing

    @if $CHR_COUNTS["$chr"] == $N
        # Single genome and same gender multi-genome.
        "${PACKAGE}/${SAMPLE}/ASM/EVIDENCE-${ASSEMBLY}${TN}/evidenceDnbs-${chr}-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$CN ~~ $cp_missing
        "${PACKAGE}/${SAMPLE}/ASM/EVIDENCE-${ASSEMBLY}${TN}/evidenceIntervals-${chr}-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$CN ~~ $cp_missing
    @else
        # Mixed gender multi-genome.
        # TODO: A more precise count check.
        "${PACKAGE}/${SAMPLE}/ASM/EVIDENCE-${ASSEMBLY}${TN}/evidenceDnbs-${chr}-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && ($CHR_COUNTS["$chr"]-1) <= $count <= max($CN, $CHR_COUNTS["$chr"]) ~~ $cp_missing
        "${PACKAGE}/${SAMPLE}/ASM/EVIDENCE-${ASSEMBLY}${TN}/evidenceIntervals-${chr}-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && ($CHR_COUNTS["$chr"]-1) <= $count <= max($CN, $CHR_COUNTS["$chr"]) ~~ $cp_missing
    @endif

    "${PACKAGE}/${SAMPLE}/ASM/REF/coverageRefScore-${chr}-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$CHR_COUNTS["$chr"] ~~ $cp_missing
@endfor

"${PACKAGE}/${SAMPLE}/ASM/gene-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/geneVarSummary-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/masterVarBeta-${ASSEMBLY}${TNTN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing

@if "$EXP_TYPE" == "EXP"
    # NOTE: This doesn't exist in EXP-R (TARGET) just because it's new and not merged there (yet)
    # - not because it's somehow specific to non-TARGET genomes.
    "${PACKAGE}/${SAMPLE}/ASM/vcfBeta-${ASSEMBLY}${TN}\.vcf\.bz2" : $is_file && $is_size_ok && $count==$N ~~ $cp_missing
@endif

@if $INCLUDE_MEI_REPORTS
    "${PACKAGE}/${SAMPLE}/ASM/MEI/mobileElementInsertionsBeta-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
    "${PACKAGE}/${SAMPLE}/ASM/MEI/mobileElementInsertionsRefCountsBeta-${ASSEMBLY}${TN}\.png" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
    "${PACKAGE}/${SAMPLE}/ASM/MEI/mobileElementInsertionsROCBeta-${ASSEMBLY}${TN}\.png" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
@elif $INCLUDE_MEI_REPORTS is None
    "${PACKAGE}/${SAMPLE}/ASM/MEI/mobileElementInsertionsBeta-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count>=0
    "${PACKAGE}/${SAMPLE}/ASM/MEI/mobileElementInsertionsRefCountsBeta-${ASSEMBLY}${TN}\.png" : $is_file && $is_size_ok && $is_encryption_ok && $count>=0
    "${PACKAGE}/${SAMPLE}/ASM/MEI/mobileElementInsertionsROCBeta-${ASSEMBLY}${TN}\.png" : $is_file && $is_size_ok && $is_encryption_ok && $count>=0
@else
    "${PACKAGE}/${SAMPLE}/ASM/MEI/mobileElementInsertionsBeta.*\.tsv" : $count==0
    "${PACKAGE}/${SAMPLE}/ASM/MEI/mobileElementInsertionsRefCountsBeta.*\.png" : $count==0
    "${PACKAGE}/${SAMPLE}/ASM/MEI/mobileElementInsertionsROCBeta.*\.png" : $count==0
@endif

"${PACKAGE}/${SAMPLE}/ASM/ncRNA-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing

"${PACKAGE}/${SAMPLE}/ASM/REPORTS/circos-${ASSEMBLY}${TN}\.html" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/circos-${ASSEMBLY}${TN}\.png" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/circosLegend\.png" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/coverage-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/coverageByGcContent-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/coverageCoding-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/coverageByGcContentCoding-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/indelLength-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/indelLengthCoding-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/somaticCircos-${ASSEMBLY}${TN}${TN}\.html" : $is_file && $is_size_ok && $is_encryption_ok && $count==$SN ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/somaticCircos-${ASSEMBLY}${TN}${TN}\.png" : $is_file && $is_size_ok && $is_encryption_ok && $count==$SN ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/somaticCircosLegend\.png" : $is_file && $is_size_ok && $is_encryption_ok && $count==$SN ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/substitutionLength-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/REPORTS/substitutionLengthCoding-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing

"${PACKAGE}/${SAMPLE}/ASM/somaticVcfBeta-${ASSEMBLY}${TN}${TN}\.vcf\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$SN ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/summary-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing

"${PACKAGE}/${SAMPLE}/ASM/SV/allJunctionsBeta-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/SV/allSvEventsBeta-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/SV/evidenceJunctionClustersBeta-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/SV/evidenceJunctionDnbsBeta-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/SV/highConfidenceJunctionsBeta-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/SV/highConfidenceSvEventsBeta-${ASSEMBLY}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/SV/somaticAllJunctionsBeta-${ASSEMBLY}${TN}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$SN ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/ASM/SV/somaticHighConfidenceJunctionsBeta-${ASSEMBLY}${TN}${TN}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count==$SN ~~ $cp_missing

"${PACKAGE}/${SAMPLE}/ASM/var-${ASSEMBLY}${TN}\.tsv\.bz2" : $is_file && $is_size_ok && $is_encryption_ok && $count==$N ~~ $cp_missing

"${PACKAGE}/${SAMPLE}/LIB/${CLS}/lib_DNB_${CLS}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count>=$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/LIB/${CLS}/lib_gaps_L0_${CLS}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count>=$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/LIB/${CLS}/lib_gaps_L1_${CLS}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count>=0 ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/LIB/${CLS}/lib_gaps_M_${CLS}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count>=$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/LIB/${CLS}/lib_gaps_R0_${CLS}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count>=$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/LIB/${CLS}/lib_gaps_R1_${CLS}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count>=0 ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/LIB/${CLS}/lib_gaps_rollup_L_${CLS}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count>=$N ~~ $cp_missing
"${PACKAGE}/${SAMPLE}/LIB/${CLS}/lib_gaps_rollup_R_${CLS}\.tsv" : $is_file && $is_size_ok && $is_encryption_ok && $count>=$N ~~ $cp_missing

@if "$EXP_TYPE" == "EXP"
    @if $INCLUDE_READS_AND_MAPPINGS
        "${PACKAGE}/${SAMPLE}/MAP/${SLIDE}-${LANE}/mapping_${SLIDE}-${LANE}_\d{3}\.tsv\.bz2" : $is_file && $is_size_ok && $count>=$N ~~ $cp_missing
        "${PACKAGE}/${SAMPLE}/MAP/${SLIDE}-${LANE}/reads_${SLIDE}-${LANE}_\d{3}\.tsv\.bz2" : $is_file && $is_size_ok && $count>=$N ~~ $cp_missing
    @elif $INCLUDE_READS_AND_MAPPINGS is None
        "${PACKAGE}/${SAMPLE}/MAP/${SLIDE}-${LANE}/mapping_${SLIDE}-${LANE}_\d{3}\.tsv\.bz2" : $is_file && $is_size_ok && $count>=0
        "${PACKAGE}/${SAMPLE}/MAP/${SLIDE}-${LANE}/reads_${SLIDE}-${LANE}_\d{3}\.tsv\.bz2" : $is_file && $is_size_ok && $count>=0
    @else
        "${PACKAGE}/${SAMPLE}/MAP/${SLIDE}-${LANE}/mapping.*\.tsv\.bz2" : $count==0
        "${PACKAGE}/${SAMPLE}/MAP/${SLIDE}-${LANE}/reads.*\.tsv\.bz2" : $count==0
    @endif

    @if $INCLUDE_LFR_TAGS
        "${PACKAGE}/${SAMPLE}/MAP/${SLIDE}-${LANE}/tagLFR_${SLIDE}-${LANE}_\d{3}\.tsv\.bz2" : $is_file && $is_size_ok && $count>=1 ~~ $cp_missing
    @elif $INCLUDE_LFR_TAGS is None
        "${PACKAGE}/${SAMPLE}/MAP/${SLIDE}-${LANE}/tagLFR_${SLIDE}-${LANE}_\d{3}\.tsv\.bz2" : $is_file && $is_size_ok && $count>=0
    @else
        "${PACKAGE}/${SAMPLE}/MAP/${SLIDE}-${LANE}/tagLFR.*\.tsv\.bz2" : $count==0
    @endif

    @if $IS_REMOTE
        "${PACKAGE}/manifest\.all\.sig" : $is_file && $is_size_ok && $count==1 ~~ $cp_missing
    @endif
@else
    @for chr in $CHR_COUNTS.keys()
        "${BAM_PACKAGE}/BAM/${SAMPLE}/${SAMPLE}_${chr}\.bam\.md5" : $is_file && $is_size_ok && $count==$CHR_COUNTS["$chr"] ~~ $cp_missing
        "${BAM_PACKAGE}/BAM/${SAMPLE}/${SAMPLE}_${chr}\.bam\.split\.\d+" : $is_file && $is_size_ok && $count>=$CHR_COUNTS["$chr"] ~~ $cp_missing
    @endfor

    "${BAM_PACKAGE}/BAM/${SAMPLE}/${SAMPLE}_notMapped\.bam\.md5" : $is_file && $is_size_ok && $count==$N ~~ $cp_missing
    "${BAM_PACKAGE}/BAM/${SAMPLE}/${SAMPLE}_notMapped\.bam\.split\.\d+" : $is_file && $is_size_ok && $count>=$N ~~ $cp_missing

    "${BAM_PACKAGE}/manifest\.all" : $is_file && $is_size_ok && $count==1 ~~ $cp_missing

    @if $IS_REMOTE
        "${BAM_PACKAGE}/manifest\.all\.sig" : $is_file && $is_size_ok && $count==1 ~~ $cp_missing
    @endif
@endif

# This one is special for staging in the sense that it is expected to be absent when
# staging is initially run; it is supposed to be copied to the appropriate location by
# the clinical supervisor after reviewing. Therefore, place this check last to draw attention.
# Also, since this is the ultimate sign-off criterium, it makes sense to fix all the other
# problems in the staged EXP before copying the CQ report.
@if $CLINICAL_SAMPLE_INFO
    @for sampleInfo in $CLINICAL_SAMPLE_INFO
        @define sampleId        $sampleInfo['sampleId']
        @define expectedCount   $sampleInfo['isClinical'] and 1 or 0
        "${PACKAGE}/${sampleId}/ASM/CQReport-${ASSEMBLY}${TN}.pdf" : $is_file && $is_size_ok && $count==$expectedCount ~~ $cp_missing
    @endfor
@endif

#".*" : $count==0
