#!/usr/bin/python
import os, sys, glob
from cgiutils import bundle
from wfclib import Workflow, Stage, Param
from nanocall import Nanocall
from mapreport import Report
from clean import Clean

class MappedDnbsFromTiff(Nanocall, Report, Clean):
    STAGES = [
               Stage('Nanocall'),
               Stage('MapAndProcess','Nanocall'),
               #Stage('Sample','MapAndProcess'),
               #Stage('Upload','MapAndProcess'),
               Stage('Report', 'MapAndProcess'),
               Stage('Clean', 'Report')
               #Stage('Email', 'Sample,Report')
               #Stage('Email', 'Sample,Upload')
             ]

    INIT = bundle(
        input = bundle(
            lanes = [],
            cycles = [],
            requestor = None,
            email = None,
            fields = "C005R008,C005R032,C006R013,C012R008,C012R032".split(','), 
            species = "Human",
            sample_fraction = 1.0
            ),
        tif = bundle(
            fluorConfigs=bundle(
                custom = bundle(
                    FITC = 'C',
                    Cy3 = 'A',
                    TxRd = 'T',
                    Cy5 = 'G'
                    ),
                cPAL = bundle(
                    FITC = 'T',
                    Cy3 = 'G',
                    TxRd = 'C',
                    Cy5 = 'A'
                    )
                )
            ),
        ref = bundle(
            Human = '/prod/pv-01/research/st_read/F13ZOOYJSY1380/zhaofuxiang/pipeline/REF/BOWTIE2-REF/hg19.fa',
            Ecoli = '/prod/pv-01/research/st_read/F13ZOOYJSY1380/zhaofuxiang/pipeline/REF/BOWTIE2-REF/Ecoli.fa'
            ),
        bowtie2 = '$CGI_HOME/bin/bowtie2',
        )

    def implMapAndProcess(self,impl):
        slide_dir = impl.fn(self.workDir,'slides')
        map_dir = impl.fn(self.workDir,'MAPS')
        if not os.path.isdir(map_dir): os.mkdir(map_dir)

        species = self.input.species
        ref = self.ref[species]

        for lane in self.input.lanes:
            slide_id_masked = lane.split('-').pop(0)[0:-3] + 'xxx' + '-FS3'
            slide_id = '-'.join(lane.split('-')[0:2])
            lane_id = lane.split('-')[-1]

            lane_dir = impl.fn(map_dir,lane)
            if not os.path.isdir(lane_dir): os.mkdir(lane_dir)

            ########## find the cycles and make the diretories
            for field in self.input.fields:
                nc_files = {}
                ########## find the file
                path = impl.fn(slide_dir,slide_id_masked,slide_id,'C*',lane_id,'*%s_NanoCall.csv' % field)
                for ncf in glob.glob(path):
                    cycle = int(os.path.basename(ncf).split('.')[1].replace('C',''))
                    nc_files[cycle] = ncf

                if len(nc_files.keys()) != len(self.input.cycles):
                    impl.abort('The len of nc_files (%d) is not equal to input.cycles (%d)' % (len(nc_files.keys()), len(self.input.cycles)) )

                ########## make the directory
                field_dir = impl.fn(map_dir,lane,field)
                if not os.path.isdir(field_dir): os.mkdir(field_dir)
                #write the cycle-map 
                pos = 0
                log = open(impl.fn(field_dir,'cycle2position.map.tsv'),'w')
                for i in self.input.map_cycles:
                    log.write("%d\t%s\n" % (pos, nc_files[i]) )
                    pos += 1
                for j in self.input.extra_cycles:
                    log.write("%d*\t%s\n" % (pos, nc_files[j]) )
                    pos += 1
                log.close()

        ########## Generate the script
        nc2fq_Params = Param('nc2fq_Params', [
            ('cycle-map',impl.fn(map_dir,'${LANE}','${FIELD}','cycle2position.map.tsv'))
            ])
        nc2fq_cmd = impl.command('nanocall2fastq.py',nc2fq_Params)

        bt_Params = Param('bt_Params', [
            ('time',''),
            ('threads',8),
            ('phred33',''),
            ('score-min','L,0,-0.15'),
            ('end-to-end',''),
            ('','-x %s' % ref),
            ('','-N 0'),
            ('','-L 15'),
            ('','-i S,1,0.40'),
            ('','-D 15'),
            ('','-q'),
            ('','-k 28'),
            ('','-U -')
            ])
        bt_cmd = impl.command(self.bowtie2,bt_Params)

        sam2map_Params = Param('sam2map_Params', [
            ('sam','-'),
            ('sink-paths',impl.fn(map_dir,'${LANE}','${FIELD}','cycle2position.map.tsv')),
            ('dump-sam',impl.fn(map_dir,'${LANE}','${FIELD}', '${LANE}.${FIELD}.bt2.sam' )),
            ('output-dir',impl.fn(map_dir,'${LANE}','${FIELD}')),
            ('simple-base-file',impl.fn(map_dir,'${LANE}','${FIELD}','simple_txt.csv'))
            ])
        sam2map_cmd = impl.command('sam2map_file.py',sam2map_Params)

        cmd = nc2fq_cmd + ' | \\\n' + bt_cmd + ' | \\\n' + sam2map_cmd
        
        ########## Set array parameters
        map_and_process_ArrayParams = []
        for lane in self.input.lanes:
            for field in self.input.fields:
                map_and_process_ArrayParams.append({
                        'LANE' : lane,
                        'FIELD' : field
                    })

        impl.step('map_and_process',
            commands = cmd,
            arrayParamValues = map_and_process_ArrayParams,
            resources = bundle(memorymb = 1024, cpu=1200)
            )

    def implSample(self,impl):
        if self.input.sample_fraction != 1.0:
            path = impl.fn (self.workDir,'MAPS','${LANE}','${FIELD}','simple_txt.csv')
            output_file = impl.fn (self.workDir,'MAPS','${LANE}','${FIELD}','simple_txt.sampled.csv')
            cmd = 'cat %s | awk \' NR==1 || rand() <= %f {print $0}\' > %s ' % (path, float(self.input.sample_fraction), output_file)

            sample_ArrayParams = []
            for lane in self.input.lanes:
                for field in self.input.fields:
                    sample_ArrayParams.append({
                            'LANE' : lane,
                            'FIELD' : field
                        })

            impl.step('sample', commands = cmd, arrayParamValues=sample_ArrayParams)

            sorted_file = impl.fn (self.workDir,'MAPS','${LANE}','${FIELD}','simple_txt.sampled.sorted.csv')
            cmd = 'cat %s | perl -e \'print scalar <>, sort {$a<=>$b} <>;\' > %s ' % (output_file,sorted_file)
            impl.step('sort', commands = cmd, arrayParamValues=sample_ArrayParams, depends=['sample'] ) 

            cmd = '''head -35 ${INPUT_FILE} > ${OUTPUT_FILE}\njoin -t, -o 1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14  <(awk \'NR>35\' ${INPUT_FILE}) ${SORTED_FILE} >> ${OUTPUT_FILE}\n''' 
            aligned_ArrayParams = []
            for lane in self.input.lanes:
                for field in self.input.fields:
                    # map_cycles 
                    sorted_file = impl.fn(self.workDir,'MAPS',lane,field,'simple_txt.sampled.sorted.csv')
                    for nc_files_wRefBase in glob.glob(impl.fn(self.workDir,'MAPS',lane,field,'*_wRefBase.csv')):
                        aligned_ArrayParams.append({
                                'INPUT_FILE' : nc_files_wRefBase,
                                'SORTED_FILE' : sorted_file,
                                'OUTPUT_FILE': nc_files_wRefBase.replace('.csv','.sampled.csv')
                            })
            impl.step('align_map_cycles', commands = cmd, arrayParamValues=aligned_ArrayParams, depends=['sample','sort'] ) 

            cmd = '''head -35 ${INPUT_FILE} > ${OUTPUT_FILE}\njoin -t, -o 1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11 <(awk \'NR>35\' ${INPUT_FILE}) ${SORTED_FILE} >> ${OUTPUT_FILE}\n''' 
            aligned_ArrayParams = []
            for lane in self.input.lanes:
                for field in self.input.fields:
                    # extra_cycles 
                    for line in open(impl.fn(self.workDir,'MAPS',lane,field,'cycle2position.map.tsv')).readlines():
                        pos,path = line.rstrip('\r\n').split('\t')
                        if not '*' in pos: continue
                        aligned_ArrayParams.append({
                                'INPUT_FILE' : path,
                                'SORTED_FILE' : sorted_file,
                                'OUTPUT_FILE':  impl.fn(self.workDir,'MAPS',lane,field,os.path.basename(path).replace('.csv','.sampled.csv'))
                            })
            if aligned_ArrayParams:
                impl.step('align_extra_cycles', commands = cmd, arrayParamValues=aligned_ArrayParams, depends=['sample','sort'] )             

    def implUpload(self,impl):
        cmd = 'sbs_se_mapping_uploader.py'
        uploadParams = Param('upload_Parms', [
            ('workDir', self.workDir),
            ('requestor', self.input.requestor),
            ('email', self.input.email)
            ])
        impl.step('upload', commands = impl.command(cmd,uploadParams) )

#    def implEmail(self,impl):
#        FROMEMAIL = 'no-reply@completegenomics.com'
#        subject = '[DNB tools notification] Your job (%s) is done!' % (os.path.basename(self.workDir))
#        content = ''' Your job is done!\n All your files could be found here: %s/MAPS/ ''' % self.workDir
#
#        msg = 'From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n' % (FROMEMAIL, self.input.email, subject)
#        msg += content
#
#        import smtplib
#        s = smtplib.SMTP('localhost')
#        s.sendmail(FROMEMAIL, self.input.email, msg)
#        s.quit()
