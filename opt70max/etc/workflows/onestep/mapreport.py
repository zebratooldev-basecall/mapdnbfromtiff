#!/usr/bin/python
import os, sys, glob
from cgiutils import bundle
from wfclib import Workflow, Stage, Param
try:
    import json
except:
    import simplejson as json

class Report(Workflow):
    STAGES = [
                Stage('Report')
            ]
    INIT = bundle(
            input = bundle(
                lanes = []
                ))

    def implReport(self,impl):
        map_dir = impl.fn(self.workDir, 'MAPS')
        if not os.path.isdir(map_dir): impl.abort('There is not exists MAPS directory.')

        lane = self.input.lanes[0]
        lane = lane.split('-')
        prefix = '-'.join(lane[:2])
        htmlTemp = os.path.join(self.cgiHome, 'etc', 'reports', 'templates', 'mappingReport.html')

        cmd = 'generateMappingReport.py'
        reportParams = Param('report_Params', [
            ('map-dir', map_dir),
            ('prefix', prefix),
            ('html-temp', htmlTemp)
            ])
        impl.step('generate_report',
                commands = impl.command(cmd, reportParams)
                )

        '''
        # generate a file controling sending email
        json_data = {}
        json_data["prefix"] = prefix
        json_data["map_dir"] = map_dir
        json_data["from_dir"] = "no-reply@completegenomics.com"
        json_data["to_dir"] = self.input.email
        json_data["attachment"] = impl.fn(map_dir, '%s_mappingReport.html' % prefix)

        #dest_dir = '/prod/pv-01/research/st_read/F13ZOOYJSY1380/zhaofuxiang/crontab/bb_mapped_from_tiff/Info'
        dest_dir = r'/rnd/home/prodqc/dnb-mapped-from-tiff/Info' 
        if not os.path.exists(dest_dir):
            os.makedirs(dest_dir)
        dest_file = impl.fn(dest_dir, os.path.basename(self.workDir))

        fh_out = open(dest_file, 'w')
        fh_out.write(json.dumps(json_data, indent=2))
        fh_out.close()
        '''

class Email(Workflow):
    STAGES = [
                Stage('Email') 
            ]
    INIT = bundle(
            input = bundle(
                lanes = [],
                email = None
                )
            )
    def implEmail(self,impl):
        map_dir = impl.fn(self.workDir, 'MAPS')
        if not os.path.isdir(map_dir): impl.abort('There is not exists MAPS directory.')

        reportList = glob.glob(os.path.join(map_dir, '*.html'))
        if len(reportList) != 1: impl.abort('There is not exists html report or more than one report.')
        report = reportList[0]

        subject = '%s_MappingReport' % ('-'.join(self.input.lanes[0].split('-')[:2]))
        content = r"Hi All:<br/>Your job is done!<br/>All your files could be found here: %s/MAPS/<br/> " % self.workDir
        content = 'hello'

        cmd = 'sendMail.py'
        emailParams = Param('email_Params', [
            ('from-addr', 'no-reply@completegenomics.com'),
            ('to-addr', self.input.email),
            ('subject', subject),
            ('content', content)
            #('attachment', report)
            ])
        impl.step('email', commands = impl.command(cmd,emailParams))


