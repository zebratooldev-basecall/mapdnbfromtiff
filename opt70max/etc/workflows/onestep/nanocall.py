#!/usr/bin/env python

import os
import glob
import wfclib.jsonutil
from cgiutils import bundle
from wfclib import Workflow, Stage, Param

'''Workflows for measuring cycle to cycle concordance'''
class Nanocall(Workflow):
    STAGES = [ 
        Stage('Nanocall'),
        Stage('Xplot', 'Nanocall'),
        Stage('FindNanoCallFiles', 'Nanocall'),
        Stage('CopyCyclesToInput', 'FindNanoCallFiles')
    ]

    INIT = bundle(input = bundle(), tif = bundle())
    INIT.tif.lanes = []
    INIT.tif.fieldListConfig = 'fieldListConfig-BBHD3.tsv'
    INIT.tif.nanocallConfigFileName = 'fp_fieldLevel.config'
    INIT.tif.nanocallBinaryPath = '/Proj/QA/NightlyBuilds/ImagePipeline/0.0.1/0.0.1.633/opt-ip/bin'
    INIT.tif.stagedLocations = [
        '/prod/pv-01/pipeline/',
        '/prod/pv-02/pipeline/',
        '/prod/pv-03/pipeline/',
        '/prod/pv-04/pipeline/',
        '/prod/pv-05/pipeline/',
        '/prod/pv-01/offline/',
        '/prod/pv-02/offline/',
        '/prod/pv-03/offline/',
        '/prod/pv-04/offline/',
        '/prod/pv-05/offline/',
        '/rnd/Proj/Pipeline/'
    ]
    INIT.tif.fluorConfigs = bundle()
    INIT.tif.fluorConfigs.standard = bundle(
        TxR='C',
        FITC='T',
        Cy5='A',
        Cy3='G'
    )

    def updateCycleLocations(self, impl, lanes):
        '''Determine slidelane cycle data from a list of staging directories'''
        for lane in lanes:
            for cycle in lane.cycles:
                # skip if this is a test.
                if self.workDir == "/fake":
                    cycle.location = "/fake/slides/%s/%s/%s/%s" % (lane.slideIdMasked, lane.slideId, cycle.cycle, lane.laneId)

                # if cycle location does not exist, attempt to find it.
                if not os.path.isdir(cycle.location):
                    # store invariant part of the slidelane cycle location (ex. 'slides/GS45xxx-FS3/GS45123-FS3/C01/L01')
                    ## also needs to work for eight digit barcodes GS12345678-FS3
                    cycleLocationInvariant = "/".join([
                        'slides',
                        lane.slideId[:-7]+'xxx'+lane.slideId[-4:],
                        lane.slideId,
                        cycle.cycle,
                        lane.laneId
                    ])
                    impl.log.info('Looking for slidelane cycle directory %s' % cycleLocationInvariant)

                    # loop through known staging directories.
                    found = False
                    for stagedLocation in self.tif.stagedLocations:
                        # create full path to putative slidelane cycle data.
                        tmpCycleLocation = stagedLocation + cycleLocationInvariant

                        # if location exists then store and move on to next cycle.
                        if os.path.isdir(tmpCycleLocation):
                            cycle.location = tmpCycleLocation
                            found = True
                            break

                    # if not found then throw an error.
                    if not found:
                        impl.abort('Unable to find cycle directory for slidelane %s cycle %s' % (lane.slidelane, cycle.cycle))

    def extractCpmLaneData(self, impl):
        # check to see if cycles with fluor configuration are specified in input.cycles.
        cycleConfigs = {}
        if hasattr(self.input, 'cycles'):
            for cycle in self.input.cycles:
                cycleConfigs[cycle.number] = cycle.fluorConfig
            
        for inputLane in self.input.cpm.lanes:
            # create object to store lane meta data.
            lane = bundle(
                laneId             = 'L%s' % inputLane.laneId,
                slideId            = inputLane.slideId,
                slidelane          = '%s-L%s' % (inputLane.slideId, inputLane.laneId),
                slideNumber        = int(inputLane.slideId[2:-4]),
                laneNumber         = int(inputLane.laneId),
                slideIdMasked      = '%sxxx%s' % (inputLane.slideId[:-7], inputLane.slideId[-4:]),
                cycles             = []
            )

            for inputCycle in inputLane.cycles:
                # create object to store cycle metadata.
                cycleName = 'C%02d' % inputCycle.number
                cycle = bundle(
                    number         = inputCycle.number,
                    cycle          = cycleName,
                    fluorConfig    = 'standard',
                    output         = impl.mkdir(self.workDir, 'slides', lane.slideIdMasked, lane.slideId, cycleName, lane.laneId),
                    nanocallConfig = impl.fn(self.workDir, 'slides', lane.slideIdMasked, lane.slideId, cycleName, lane.laneId, 'nanocall.config')
                )

                # set cycle location.
                if hasattr(inputCycle, 'location'):
                    cycle.location = inputCycle.location
                else:
                    cycle.location = ''

                # update cycle config if specified.
                if cycleConfigs.has_key(cycle.number):
                    cycle.fluorConfig = cycleConfigs[cycle.number]

                # add cycle to lane collection.
                lane.cycles.append(cycle)

            # add lane to tiff lane collection.
            self.tif.lanes.append(lane)

    def extractInputLaneData(self, impl):
        for inputLane in self.input.lanes:
            # create object to store lane meta data.
            lane = bundle(
                laneId          = inputLane[-3:],
                slideId         = inputLane[:-4],
                slidelane       = inputLane,
                slideNumber     = int(inputLane[2:-8]),
                laneNumber      = int(inputLane[-2:]),
                slideIdMasked   = inputLane[:-11]+'xxx'+inputLane[-8:-4],
                cycles          = []
            )
            for inputCycle in self.input.cycles:
                # create object to store cycle meta data.
                cycleName = 'C%02d' % inputCycle.number
                cycle = bundle(
                    number         = inputCycle.number,
                    cycle          = cycleName,
                    location       = '',
                )

                # add nanocall specific inforation if "fluorConfig" exists.
                if hasattr(inputCycle, 'fluorConfig'):
                    cycle.fluorConfig    = inputCycle.fluorConfig
                    cycle.output         = impl.mkdir(self.workDir, 'slides', lane.slideIdMasked, lane.slideId, cycleName, lane.laneId)
                    cycle.nanocallConfig = impl.fn(self.workDir, 'slides', lane.slideIdMasked, lane.slideId, cycleName, lane.laneId, 'nanocall.config')

                # add cycle object to lane
                lane.cycles.append(cycle)

            # add lane to self data.
            self.tif.lanes.append(lane)

    def loadFieldNames(self, impl):
        fields = []

        # get path to field name config file.
        fieldNameConfigFile = ''
        if hasattr(self.input, 'fieldListConfig'):
            fieldNameConfigFile = self.input.fieldListConfig
        elif hasattr(self.tif, 'fieldListConfig'):
            fieldNameConfigFile = self.tif.fieldListConfig
        else:
            fieldNameConfigFile = impl.fn(self.cgiHome, 'etc', 'config' , 'nanocall', self.tif.fieldListConfig)
        self.tif.fieldListConfig = fieldNameConfigFile
        impl.log.info('field name list config files set to "%s"' % self.tif.fieldListConfig)

        # read in fields.
        impl.log.info('Reading in field list config file...')
        try:
            sr = open(fieldNameConfigFile, 'r')
            # skip header
            sr.readline()
            for line in sr:
                data = line.rstrip('\r\n').split('\t')
                field = bundle(
                                col  = data[0],
                                row  = data[1],
                                name = data[2]
                        )
                fields.append(field)
            sr.close()
        except:
            impl.abort('Failure in reading field name list config file (%s)!' % fieldNameConfigFile)

        return fields

    def indexFieldsByColumn(self, impl, fields):
        fieldsByColumn = bundle()

        for field in fields:
            if not hasattr(fieldsByColumn, field.col):
                fieldsByColumn[field.col] = []
            fieldsByColumn[field.col].append(field)

        return fieldsByColumn

    def implNanocall(self, impl):
        # create top level nanocall directory.
        impl.mkdir(self.workDir, 'slides')

        # path to fp config file
        nanocallConfig = ''
        if hasattr(self.input, 'nanocallConfigFP'):
            nanocallConfig = self.input.nanocallConfigFP
        else:
            nanocallConfig = impl.fn(self.cgiHome, 'etc', 'config' , 'nanocall', self.tif.nanocallConfigFileName)

        # gather lane and cycle metadata.
        # lane cycle data can be from QueryLIMS (self.input.cpm) or in self.input.
        if self.workDir != "/fake":
            if hasattr(self.input, 'lanes'):
                impl.log.info('looking for lane cycle data in input.lanes...')
                self.extractInputLaneData(impl)
            elif hasattr(self.input, 'cpm'):
                impl.log.info('looking for lane cycles in query lims output...')
                self.extractCpmLaneData(impl)
            else:
                # we did not find lanes in input or input.cpm!
                impl.abort('no lane data found!')

            # find slidelane cycle data directories.
            self.updateCycleLocations(impl, self.tif.lanes)

        # setup nanocall array arguments/params.
        nanocallArrayParams = []
        for lane in self.tif.lanes:
            for cycle in lane.cycles:
                # concatenate base to fluorophore assignment.
                if not hasattr(self.tif.fluorConfigs, cycle.fluorConfig):
                    impl.abort('Unknown fluorophore configuration %s' % cycle.fluorConfig)
                fc = self.tif.fluorConfigs[cycle.fluorConfig]
                bases = fc['TxRd']+fc['FITC']+fc['Cy5']+fc['Cy3']

                # add array parameters.
                nanocallArrayParams.append({
                    'SLIDENUMBER' : str(lane.slideNumber),
                    'LANE'        : str(lane.laneNumber),
                    'CYCLE'       : str(cycle.number),
                    'LOCATION'    : cycle.location,
                    'OUTDIR'      : cycle.output,
                    'CONFIG'      : cycle.nanocallConfig,
                    'CONFIGBASES' : bases
                })

        # set up command string.
        cmds = []

        # copy nanocall config file to cycle output directory.
        cmds.append('cp -f %s ${CONFIG}' % nanocallConfig)

        # append base to fluorophore assignment string.
        cmds.append('echo -e "# Sequence Order (corresponding to TxRed, FITC, Cy5, Cy3)\\nmodparam.nanocall.baseOrder =${CONFIGBASES}\\n" >> ${CONFIG}')

        # build nanocall command.
        cmds.append('processor offline -p ${CONFIG} -t -s ${SLIDENUMBER} -c ${CYCLE} -l ${LANE} -i ${LOCATION} -o ${OUTDIR}')
        #cmds.append('%s/processor offline -p ${CONFIG} -t -s ${SLIDENUMBER} -c ${CYCLE} -l ${LANE} -i ${LOCATION} -o ${OUTDIR}' % self.input.nanocallBinaryPath)

        # create step.
        impl.step('processor', commands=cmds, arrayParamValues=nanocallArrayParams, memory=10, concurrencyLimit=200)

    def implGetNanoDnb(self, impl):
        # create top level directory.
        impl.mkdir(self.workDir, 'dnbs')

        # gather slide and cycle meta data.
        if hasattr(self.input, 'lanes'):
            self.extractInputLaneData(impl)
        else:
            # we did not find lanes in input or input.cpm!
            impl.abort('no lane data found!')

        # find slidelane cycle data directories.
        self.updateCycleLocations(impl, self.tif.lanes)

        # gather fields from field name config file.
        fields = self.loadFieldNames(impl)
        fieldsByColumn = self.indexFieldsByColumn(impl, fields)
        impl.log.info('%d columns and %d fields found in field list config file' % (len(fieldsByColumn), sum([ len(n) for n in fieldsByColumn.values() ])))

        # loop through slides and columns and create a step for each iteration.
        lastStepName = []
        for lane in self.tif.lanes:
            # set slide variables.
            slideNumber = str(lane.slideNumber)
            laneNumber = str(lane.laneNumber)
            impl.log.info('creating steps for %s...' % lane.slidelane)

            # build cycle param list.
            cycleList = []
            for cycle in lane.cycles:
                cycleList.append(str(cycle.number))
                cycleList.append(cycle.location)
            cycleParams = ' '.join([ item for item in cycleList ])
            impl.log.info('cycle/directory pairs set to %s' % cycleParams)

            for column,fields in fieldsByColumn.items():
                impl.log.info('  - creating step for column %s' % column)
                # create output directory
                colName = 'C%03d' % int(column)
                outDir = impl.mkdir(self.workDir, 'dnbs', lane.slidelane, colName)
                stepName = '%s_%s' % (lane.slidelane, colName)

                # array parameters.
                getNanoDnbArrayParams = []

                # loop through fields.
                for field in fields:
                    # append array parameters
                    getNanoDnbArrayParams.append({
                        'FIELD' : lane.laneId+field.name
                    })

                # build the getnanodnb command.
                cmd = [ 'getnanodnb %(outDir)s %(slideNumber)s ${FIELD} %(cycleParams)s' % vars() ]

                # set step.
                impl.step(name = stepName,
                          depends = lastStepName,
                          commands = cmd,
                          arrayParamValues = getNanoDnbArrayParams,
                          concurrencyLimit = 200)


    def ConcFromGetNanoDnb(self, impl):
        # setup top level directory
        impl.mkdir(self.workDir, 'conc')

        # gather fields and index by column
        fields = self.loadFieldNames(impl)
        fieldsByColumn = self.indexFieldsByColumn(impl, fields)

        # loop through slides and field columns to create a step for each iteration.
        lastStepName = []
        for lane in self.tif.lanes:
            impl.log.info('building scripts for concordance analysis of %s...' % lane.slidelane)

            # check that only two cycles were entered for concordance analysis.
            if not len(lane.cycles) == 2:
                impl.log.info('expecting two cycles for concordance analysis, found %d!' % len(lane.cycles))
                impl.abort('un-expected number of cycles discovered for concordance analysis!')

            
            

    def implXplot(self, impl):
        # setup xplot array parameters of nanocall directories.
        xplotArrayParams = []
        for lane in self.tif.lanes:
            for cycle in lane.cycles:
                # add array parameter.
                xplotArrayParams.append({
                    'NCDIR' : cycle.output
                })

        # set up command  string
        cmd = 'xplotFromNanocall.R ${NCDIR} ${NCDIR}'

        # create xplot step.
        impl.step('xplot', commands = cmd, arrayParamValues=xplotArrayParams, memory=1, concurrencyLimit=200)

    def getFieldLevelMetrics(self, fn):
        results = {}

        # read in nanocall file.
        ff = open(fn, 'r')
        for line in ff:
            if line.startswith('Version'): continue
            if line.startswith('Per Base'): break
            fields = line.rstrip('\r\n').split(',')
            if len(fields) < 2: continue
            results[fields[0]] = fields[1]
        ff.close()

        return results

    def implFindNanoCallFiles(self, impl):
        '''find the paths to all output files after running nanocall'''
        # loop through the lanes, cycles, and fields of working directory.
        # collect paths and basecalling metrics.
        for lane in self.tif.lanes:
            impl.log.info('looking for nanocall csv files for lane %s' % lane.slidelane)
            for cycle in lane.cycles:
                impl.log.info('  - cycle %s' % cycle.cycle)
                # if this is a test run exit loop.
                if self.workDir == "/fake": continue

                # create container for fields.
                cycle.fields = []
                fileSearchPattern = os.path.join(cycle.output, '%s.%s.%s*_NanoCall.csv' % (lane.slideId, cycle.cycle, lane.laneId))
                impl.log.info('    - file search patter: %s' % fileSearchPattern)
                for nanoCallFile in glob.glob(fileSearchPattern):
                    fieldName = os.path.split(nanoCallFile)[1][-21:-13]
                    fieldMetrics = self.getFieldLevelMetrics(nanoCallFile)
                    fd = bundle(
                        fieldId = fieldName,
                        nanoCallFile = nanoCallFile,
                        regStatus = fieldMetrics['Registration Status'],
                        patternId = fieldMetrics['Pattern ID'],
                        bic = fieldMetrics['Percent BaseCall Information Content'],
                        fit = fieldMetrics['Fit Score']
                    )
                    cycle.fields.append(fd)

                # loop through fields and stor max bic.
                cycle.bic = max([ float(field.bic) for field in cycle.fields ])

    def implCopyCpmCycleData(self, impl):
        '''update cpm.lane meta data from location and quality from nanocall results'''
        # if cpm cycle data is not present then skip the following steps.
        if not hasattr(self.input, 'cpm'): return
        if not hasattr(self.input.cpm, 'lanes'): return

        # loop through lane cycles in tif.lanes.
        for lane in self.tif.lanes:
            for cycle in lane.cycles:
                # if this is a test run then skip loop.
                if self.workDir == "/fake": continue

                # if cpm lane.cycle exists then update it's location and quality value.
                for cpmlane in self.input.cpm.lanes:
                    slidelane = '%s-L%s' % (cpmlane.slideId, cpmlane.laneId)
                    # short cut loop if slidelanes do not match.
                    if slidelane != lane.slidelane: continue

                    # find matching cycle
                    for cpmcycle in cpmlane.cycles:
                        # short cut loop if cycles do not match.
                        if cpmcycle.number != cycle.number:continue

                        # update values.
                        cpmcycle.location = cycle.output
                        cpmcycle.quality  = cycle.bic

    def implCopyCyclesToInput(self, impl):
        '''copy new cycle location and information to input.cpm for makeADF'''
        self.input.cpm = bundle(
            libraries = [],
            lanes     = []
        )

        # create fake library entry.
        library = bundle(
            project   = "001",
            clsName   = "GS99999-CLS_A01",
            did       = "GS000000001-DID",
            sample    = "GS99999-DNA_A01",
            sid       = "GS99999-DNA_A01.1",
            structure = "LS999999",
            anchors   = [],
            adaptors  = []
        )
        anchor = bundle(
            adaptor        = "AdA",
            offsetSequence = "",
            reach          = 0,
            orientation    = "3 Prime",
            name           = "AdA_3T"
        )
        adaptor = bundle(
            sequence = "TTT",
            number   = 1,
            name     = "AdA"
        )
        library.anchors.append(anchor)
        library.adaptors.append(adaptor)

        # add library to state object.
        self.input.cpm.libraries.append(library)

        # copy slidelane cycles from tif to input.cpm.
        for lane in self.tif.lanes:
            lane_data = bundle(
                clsName = 'GS99999-CLS_A01',
                sid     = 'GS99999-DNA_A01.1',
                laneId  = lane.laneId[-2:],
                slideId = lane.slideId,
                cycles  = []
            )
            for cycle in lane.cycles:
                cycle_data = bundle(
                    isCompleted=True,
                    anchorName='AdA_3T',
                    number=cycle.number,
                    location=cycle.output,
                    isBiological=True,
                    position='A013%02d' % cycle.number,
                    quality=cycle.bic
                )
                lane_data.cycles.append(cycle_data)

            # add lane to state object.
            self.input.cpm.lanes.append(lane_data)

    def implNanocallToFastq(self, impl):
        '''create fastq file from nanocall csv files'''
        # create top level directory.
        impl.mkdir(self.workDir, 'fastq')

        # build list of fields
        field_names = set([])
        for lane in self.tif.lanes:
            for cycle in lane.cycles:
                for field in cycle.fields:
                    field_names.add(field.fieldId)

        # create string representation of cycle list.
        cycle_list_str = ','.join([ str(c.number) for c in self.input.cycles ])

        # determine memory required.
        requiredMemory = int(len(self.input.cycles) * 150)

        # create nanocall to fastq command line parameters collection.
        nanocallToFastqParams = Param('NanocallToFastq.createFastq', [
            ('source-directory', impl.fn(self.workDir, 'slides')),
            ('output-filename', '${OUTPUT_FN}'),
            ('slide-barcode', '${SLIDE_BARCODE}'),
            ('lane', '${LANE}'),
            ('field-name', '${FIELD_NAME}'),
            ('cycle-list', cycle_list_str)
            ])

        # compile array parameters.
        nanocallToFastqArrayParams = []
        for lane in self.tif.lanes:
            # create ouput subdirectory.
            impl.mkdir(self.workDir, 'fastq', lane.slidelane)
            for field_name in field_names:
                nanocallToFastqArrayParams.append({
                    'OUTPUT_FN': impl.fn(self.workDir, 'fastq', lane.slidelane, '%s.%s.fastq' % (lane.slidelane, field_name)),
                    'SLIDE_BARCODE': lane.slideId,
                    'LANE': str(lane.laneNumber),
                    'FIELD_NAME': field_name
                    })

        # create nanocall to fastq step.
        impl.step('createFastq',
                  commands = impl.command('nanocall_to_fastq.py', nanocallToFastqParams),
                  arrayParamValues = nanocallToFastqArrayParams,
                  resources = bundle(cpu=100,memorymb=requiredMemory,nfsio=450))


class GetNanoDnb(Nanocall):
    STAGES = [
        Stage('GetNanoDnb')
    ]

#---------------------------------------testing--------------------------------------------------------
_testInput = \
'''
{
  "input": {
    "lanes" : [ "GS79533-FS3-L01" ],
    "cycles": [ 
      { "number": 1, "fluorConfig": "standard" },
      { "number": 2, "fluorConfig": "standard" },
      { "number": 3, "fluorConfig": "standard" },
      { "number": 4, "fluorConfig": "standard" },
      { "number": 5, "fluorConfig": "standard" },
      { "number": 6, "fluorConfig": "standard" },
      { "number": 7, "fluorConfig": "standard" },
      { "number": 8, "fluorConfig": "standard" },
      { "number": 9, "fluorConfig": "standard" },
      { "number": 10, "fluorConfig": "standard" },
      { "number": 11, "fluorConfig": "standard" }
    ],
    "fieldListConfig": "/rnd/home/sgiles/source/sgiles_2.5.0/bio/config/nanocall/fieldListConfig-BBHD3.tsv"
  },
  "adf": {
    "checkSampleId": false
  },
  "workDir": "/fake",
  "cgiHome": "/fake/home"
}
'''

if __name__ == '__main__':
    import sys, copy
    from wfclib import testWorkflowCallbacks

    if len(sys.argv) == 1:
        testInput = wfclib.jsonutil.loads(_testInput)
        testWorkflowCallbacks(Nanocall, testInput)
        testWorkflowCallbacks(GetNanoDnb, testInput)
    elif sys.argv[1] == 'Nanocall':
        testInput = wfclib.jsonutil.loads(_testInput)
        testWorkflowCallbacks(Nanocall, testInput)
    elif sys.argv[1] == 'GetNanoDnb':
        testInput = wfclib.jsonutil.loads(_testInput)
        testWorkflowCallbacks(GetNanoDnb, testInput)


