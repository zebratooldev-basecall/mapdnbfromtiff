#!/usr/bin/python
import os, sys, glob
from cgiutils import bundle
from wfclib import Workflow, Stage, Param
import subprocess
import shutil
import glob

class Clean(Workflow):
    STAGES = [
                Stage('Clean')
            ]

    def implClean(self, impl):
        slide_dir = impl.fn(self.workDir,'slides')
        map_dir = impl.fn(self.workDir,'MAPS')
        if os.path.exists(slide_dir): shutil.rmtree(slide_dir)

        lanes_dir = filter(lambda x: os.path.isdir(x), glob.glob(os.path.join(map_dir, '*')))
        for lane_dir in lanes_dir:
            shutil.rmtree(lane_dir)

