#!/usr/bin/python
import os, sys, glob
from cgiutils import bundle
from wfclib import Workflow, Stage, Param

class RunCopy(Workflow):
    STAGES = [
                Stage('RunCopy')
            ]

    INIT = bundle(
        input = bundle(
            lanes = [],
            cycles = [],
            requestor = None,
            email = None,
            fields = "C005R008,C005R032,C006R013,C012R008,C012R032".split(','), 
            species = "Human",
            sample_fraction = 1.0
            ),
        tif = bundle(
            fluorConfigs=bundle(
                custom = bundle(
                    FITC = 'C',
                    Cy3 = 'A',
                    TxRd = 'T',
                    Cy5 = 'G'
                    ),
                cPAL = bundle(
                    FITC = 'T',
                    Cy3 = 'G',
                    TxRd = 'C',
                    Cy5 = 'A'
                    )
                )
            ),
        ref = bundle(
            Human = '/prod/pv-01/research/st_read/F13ZOOYJSY1380/zhaofuxiang/pipeline/REF/BOWTIE2-REF/hg19.fa',
            Ecoli = '/prod/pv-01/research/st_read/F13ZOOYJSY1380/zhaofuxiang/pipeline/REF/BOWTIE2-REF/Ecoli.fa'
            ),
        bowtie2 = '$CGI_HOME/bin/bowtie2',
        )

    def implRunCopy(self,impl):
        impl.log.info( "test")
        impl.log.info( type(self.input) )
        # new_bundle = self.input.rcopy()
        new_bundle = self.input
        impl.log.info( type(new_bundle) )
        print type(new_bundle)


